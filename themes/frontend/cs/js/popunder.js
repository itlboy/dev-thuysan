function popunder(popList) {
//    $.cookie('count', null);
    
    var date = new Date();
    var url;
    var urls;
    var params;
	
    var checkCount;
    var count = $.cookie('count');
    if (!count) count = 1;
    else count = parseInt(count);
    
    $.each(popList, function(key, row) {
        
        checkCount = ','+row['popcount']+',';
        if(checkCount.indexOf(','+count+',') >= 0) {
            params = 'width='+row['width']+',height='+row['height']+'';
            params += row['position'];
            if (row['window'] == '0')
                params += 'toolbar=yes, location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,copyhistory=yes';
			
            urls = [row['url']];
            url = urls[Math.floor(Math.random() * urls.length)];
            window.open(url, row['name'], params).blur();
            window.focus();
        }
    });
    // Set time for Cookie
    date.setTime(date.getTime() + (4 * 60 * 60 * 1000));
    $.cookie('count', ++count, { expires: date });
}