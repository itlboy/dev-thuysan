    <div class="fix_width">
        <?php $this->widget('bootstrap.widgets.TbCarousel', array(
            'items'=>array(
                array('image' => Yii::app()->theme->baseUrl . '/images/slide/01.jpg', 'label'=>'First Thumbnail label', 'caption'=>'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.'),
                array('image' => Yii::app()->theme->baseUrl . '/images/slide/02.jpg', 'label'=>'First Thumbnail label', 'caption'=>'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.'),
                array('image' => Yii::app()->theme->baseUrl . '/images/slide/03.jpg', 'label'=>'First Thumbnail label', 'caption'=>'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.'),
                array('image' => Yii::app()->theme->baseUrl . '/images/slide/04.jpg', 'label'=>'First Thumbnail label', 'caption'=>'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.'),
                array('image' => Yii::app()->theme->baseUrl . '/images/slide/05.jpg', 'label'=>'First Thumbnail label', 'caption'=>'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.'),
                array('image' => Yii::app()->theme->baseUrl . '/images/slide/06.jpg', 'label'=>'First Thumbnail label', 'caption'=>'Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.'),
            ),
        )); ?>
    </div>

    <link rel="canonical" href="<?php echo Yii::app()->getBaseUrl(true); ?>" />
    <div class="fix_width body">
        <div class="fl col_s1" style="width: 200px;">                        

            <!-- User Panel -->
            <?php $this->widget('application.modules.account.widgets.WidgetLoginForm'); ?>
            <!-- END User Panel -->
        </div>
        <div class="col_s1_backgound"></div>
        
        <div class="fl col_s4">
            <div class="padding">
                <h2 class="heading_s1">Download LetCS</h2>
                <p>
                    Tải phiên bản LetCS mới nhất <a href="http://forum.let.vn/thread-243-letcs-chinh-thuc-mo-cua-hoat-dong-lai/1" target="_blank" title="Tải phiên bản LetCS mới nhất">tại đây</a>
                </p>
                <h2 class="heading_s1">LetCS Server</h2>
                <div style="margin-left: 15px;">
                    <?php $this->widget('bootstrap.widgets.TbMenu', array(
                        'type'=>'list',
                        'htmlOptions'=>array('class'=>'user-info'),
                        'items'=>array(
                            array('label'=>'Server Public'),
                            array('label'=>'public 1: 210.245.90.238:27015', 'icon'=>'icon-minus', 'url'=> 'javascript:void(0);'),
                            array('label'=>'public 2: 210.245.90.238:27016', 'icon'=>'icon-minus', 'url'=> 'javascript:void(0);'),
                        )
                    )); ?>
                </div>
                <h2 class="heading_s1">LetCS ChangeLog</h2>
                <div style="margin-left: 15px;">
                    <?php $this->widget('bootstrap.widgets.TbMenu', array(
                        'type'=>'list',
                        'htmlOptions'=>array('class'=>'user-info'),
                        'items'=>array(
                            array('label'=>'02/03/2013 - Version 1001:'),
                            array('label'=>'Đổi giao diện Launcher Đổi giao diện Launcher Đổi giao diện Launcher', 'icon'=>'icon-minus', 'url'=> 'javascript:void(0);'),
                            array('label'=>'15/02/2013 - Version 1000:'),
                            array('label'=>'Bắt đầu khởi tạo game LetCS v.1000', 'icon'=>'icon-minus', 'url'=> 'javascript:void(0);'),
                        )
                    )); ?>
                </div>
            </div>
        </div>
        <div class="col_s4_backgound"></div>
    </div>

<!--            <div class="heading_s1 margin_top">Thành viên</div>-->
<!--            --><?php //$this->widget('bootstrap.widgets.TbButtonGroup', array(
//                'buttons'=>array(
//                    array('label'=>'Đại gia', 'url'=>'#', 'active'=>true),
//                    array('label'=>'Top comment', 'url'=>'#'),
//                    array('label'=>'Thành viên mới', 'url'=>'#'),
//                ),
//            )); ?>
<!--                    --><?php //for ($i = 0; $i < 5; $i++): ?>
<!---->
<!--                    --><?php //endfor; ?>

