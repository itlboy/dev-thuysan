<?php
$memberConfig = KitAccountConfig::getField(Yii::app()->user->id,'cs_vip');
if(!empty($memberConfig)) {
    if(strtotime($memberConfig) > time()) {
        $vipText = letDate::fuzzy_span(strtotime($memberConfig)) . ' VIP';
    } else {
        $vipText = 'Đã hết hạn VIP';
    }
} else $vipText = 'Chưa nạp VIP';
?>
<script type="text/javascript">
</script>
<div style="float: left; width: 230px;" class="well">
    <div style="margin: 10px 0 0 15px;"><img src="<?php echo KitAccount::getAvatar(Yii::app()->user->id,'medium'); ?>" alt="img" /></div>
    <div>
        <?php $this->widget('bootstrap.widgets.TbMenu', array(
                'type'=>'list',
                'items'=>array(
                    array('label'=>'TÀI KHOẢN: ' . Yii::app()->user->name),
        //                array('label'=>'Home', 'icon'=>'home', 'url'=>'#', 'active'=>true),
                    array('label'=>'Trang chủ', 'icon'=>'home', 'url'=>'http://cs.let.vn', 'linkOptions'=>array('target'=>'_blank')),
                    array('label'=>'Nạp VIP', 'icon'=>'star', 'url'=>'javascript:void(0);', 'linkOptions'=>array('onclick'=>'alert("Mời bạn vào website http://cs.let.vn, đăng nhập và nạp VIP!");')),
                    array('label'=>'Thoát', 'icon'=>'off', 'url'=>Yii::app()->createUrl('cs/launcher/logout')),
                    array('label'=>'NHÂN VẬT ' . $vipText),
                ),
        )); ?>
    </div>
    
    <?php $this->widget('application.modules.cs.widgets.frontend.launcherCharacter'); ?>
</div>
<!--<div style="float: right; width: 220px;">
    <ul class="list_s1">
        <?php for ($i = 0; $i < 8; $i++): ?>
        <li><a href="javascript:void(0);">Những chiêu cướp 'khó đỡ' ở Sài Gòn</a> <span class="time">11:57 17/2/2013</span></li>
        <?php endfor; ?>
    </ul>
</div>-->
<div style="clear: both;"></div>
