<div class="well" style="float: left; width: 230px;">
    <?php /** @var BootActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'loginForm',
        'action' => Yii::app()->createUrl('//cs/frontend/launcher/login'),
        'htmlOptions'=>array(
            'class'=>'',
            'style'=>'width:230px;',
            'onSubmit' => "js:ajaxLoginForm(jQuery(this).attr('id'), jQuery(this).attr('action')); return false"
        ),
    )); ?>
    <?php echo $form->textFieldRow($model, 'username', array('class'=>'span3')); ?>
    <?php echo $form->passwordFieldRow($model, 'password', array('class'=>'span3')); ?>
    <br />
    <?php // echo $form->checkboxRow($model, 'checkbox'); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'icon'=>'ok', 'label'=>'Đăng nhập')); ?>
    <?php $this->endWidget(); ?>
    
    <ul class="nav nav-list">
        <li><a href="http://id.let.vn/home.let" target="_blank"><i class="icon-user"></i> Đăng ký</a></li>
        <li><a href="http://id.let.vn/account/default/forgot.let" target="_blank"><i class="icon-cog"></i> Tìm lại mật khẩu</a></li></ul>       
    <div id="login-msg" style="display:none;text-align: center;"></div>
</div>
<!--<div style="float: right; width: 220px;">
    <ul class="list_s1">
        <?php for ($i = 0; $i < 8; $i++): ?>
        <li><a href="javascript:void(0);">Những chiêu cướp 'khó đỡ' ở Sài Gòn</a> <span class="time">11:57 17/2/2013</span></li>
        <?php endfor; ?>
    </ul>
</div>-->
<div style="clear: both;"></div>

<script>
function ajaxLoginForm(form, url) {
    jQuery.ajax({
        'url':url,
        'type':'POST',
        'data':jQuery('#'+form).serialize(),
//        'dataType':'JSON',
        'beforeSend':function() {
            jQuery('#login-msg').html('<img src="http://phim.let.vn/themes/frontend/letphim/images/ajax-loader.gif" />').show();
        },
        'error':function() {
            location.reload();
        },
        'success':function(data) {
            if (data == 'success') {
                location.reload();
            } else {                
                alert('Truy cập không thành công. Xin hãy thử lại...');
                jQuery('#login-msg').html('').hide();
            }
        }
    });
}
</script>
