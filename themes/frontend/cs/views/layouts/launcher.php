<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <title><?php echo isset($this->pageTitle) ? CHtml::encode($this->pageTitle) : Yii::app()->name; ?></title>
<?php
    $cs = Yii::app()->getClientScript();
//    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/templates.css?version='.$version);
//    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/style_1.css?version='.$version);

    $cs->registerCoreScript('jquery');
    $cs->registerCoreScript('cookie');
?>
</head>
<body>
<style>
body {background: #081534;}
form {margin: 0 0 10px}
.well {padding: 5px;}
.character {font-size: 11px; line-height: 15px;}
.character .row_info span {padding: 3px 15px;}
.character .row_info.focus {margin-bottom: 5px;}
.character .row_info.focus span {font-weight: bold; font-size: 13px;}

.list_s1 {list-style: none; margin: 0;}
.list_s1 li {line-height: 16px; margin-bottom: 5px; padding-left: 20px; background: url('<?php echo Yii::app()->theme->baseUrl; ?>/images/arrow.png') 0 3px no-repeat;}
.list_s1 .time {font: italic 10px Tahoma; color: #ccc;}
</style>

<!--    <div style="padding: 10px; width: 480px; border: 1px #ccc solid; margin: 10px;">-->
<div style="margin: 5px 12px; width: 472px;">
    <?php echo $content; ?>
</div>
</body>
</html>
