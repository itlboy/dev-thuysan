<?php $host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ''; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo isset($this->pageTitle) ? CHtml::encode($this->pageTitle) : Yii::app()->name; ?></title>
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <meta name="verify-maudich" content="e778f649fdf9333cd712d3dc287658196bcc7acc" />
<?php
    $version = 4;
    $cs = Yii::app()->getClientScript();
    $cs->registerCssFile('http://assets.let.vn/css/youtube_core.css');
    $cs->registerCssFile('http://assets.let.vn/css/youtube_guide.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/templates.css?version='.$version);
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/style_1.css?version='.$version);
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/cs.css?version='.$version);

    $cs->registerCoreScript('jquery');
    $cs->registerCoreScript('cookie');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/jquery.lazyload.mini.js', CClientScript::POS_HEAD);
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/let.js?version='.$version, CClientScript::POS_END);
    $cs->registerScriptFile(Yii::app()->request->baseUrl.'/js/functions.js?version='.$version, CClientScript::POS_END);
?>
	<!--[if lte IE 7]>
		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ie.css" type="text/css" />
	<![endif]-->
</head>
<!--<body onclick="popunder(popList);">-->
<body>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/vi_VN/all.js#xfbml=1";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Header -->

    <div class="fix_width header">
        <div style="position:relative;">
            <div class="logo fl" onclick="window.location = '<?php echo Yii::app()->getBaseUrl(true); ?>'"></div>
            <div class="well fl" style="margin: 15px 0 0 5px; font-size: 12px;">
                <div>Hỗ trợ các vấn đề trong game: <a href="ymsgr:sendim?dyotangid_foem&amp;m=Xin chào Nova, mình cần hỗ trợ về game!" title="Chat với Nova để được hỗ trợ các vấn đề trong game" rel="nofollow">Nova <img src="http://opi.yahoo.com/online?u=dyotangid_foem&amp;m=g&amp;t=5&amp;l=us" width="12" height="12" /></a></div>
                <div>Hỗ trợ tài khoản, thanh toán: <a href="ymsgr:sendim?letnguago&amp;m=Xin chào Ngựa Gỗ, mình cần hỗ trợ các vấn đề về tài khoản và thanh toán!" title="Chat với Ngựa Gỗ để được hỗ trợ về các vấn đề liên quan tài khoản và thanh toán!" rel="nofollow">Ngựa Gỗ <img src="http://opi.yahoo.com/online?u=letnguago&amp;m=g&amp;t=5&amp;l=us" width="12" height="12" /></a></div>
            </div>
            <div class="link">                
                <?php $this->widget('application.modules.account.widgets.WidgetLoginModal', array(
                    'view' => 'WidgetLoginModal_cs',
                )); ?>
            </div>
            <div class="nguago"></div>

        </div>
        <div class="topmenu">
            <?php
            $this->widget('bootstrap.widgets.TbNavbar', array(
                'fixed'=>false,
                'brand' => false,
                'collapse'=>true, // requires bootstrap-responsive.css
                'items'=>array(
                    array(
                        'class'=>'bootstrap.widgets.TbMenu',
                        'items'=>array(
                            array('label'=>'Trang chủ', 'url' => Yii::app()->getBaseUrl(true)),
							array('label'=>'Diễn đàn', 'url'=> 'http://forum.let.vn/forum-37-letcs/'),
							array('label'=>'Download', 'url'=> 'http://forum.let.vn/thread-243-letcs-chinh-thuc-mo-cua-hoat-dong-lai/1'),
//							array('label'=>'Top 10', 'url'=> Yii::app()->createUrl('phim-de-cu')),
                        ),
                    ),
//                    array(
//                        'class'=>'bootstrap.widgets.TbMenu',
//                        'htmlOptions'=>array('class'=>'pull-right'),
//                        'items'=>array(
//                            array('label'=>'"Vãi lúa"', 'url'=>'http://vailua.vn?utm_source=phim.let.vn&utm_medium=topmenu&utm_campaign=vailua'),
//                        ),
//                    ),
                ),
            ));
            ?>           
        </div>

    </div>
    <!-- END Header -->
    <!-- Body -->
    <?php echo $content; ?>
    <!-- END Body -->
    <!-- Footer -->
    <div class="footer">
        <div class="fix_width">
<!--            <div class="fl" style="">
                <ul class="nav nav-pills">
                    <li class=""><a rel="nofollow" href="#">Điều khoản sử dụng</a></li>
                    <li class=""><a rel="nofollow" href="#">Chính sách riêng tư</a></li></ul>
            </div>-->
            <div class="fr info" style="">
                <div style="font-weight: bold;">Bản quyền thuộc về iLet Media</div>
                <div>Website được xây dựng trên hệ thống <span class="labelinfo">Letkit v1.0 Beta</span></div>
                <div>Website đang trong quá trình xây dựng và thử nghiệm. Mọi chi tiết xin liên hệ</div>
                <div><span class="labelinfo">[M]</span> 0988.631988 - 0908.631988 | <span class="labelinfo">[E]</span> nguago@let.vn | <span class="labelinfo">[Y!M]</span> letnguago</div> 
           </div>
        </div>
    </div>
    <!-- END Footer -->
   
    <div class="loading"></div>
    <script type="text/javascript">
    $(document).ready(function() {
        $('.loading').ajaxStart(function() {
            $(this).show();
        }).ajaxStop(function (){
            $(this).hide(1000);
        })
    });
    </script>

    <script type="text/javascript">
    $(function() {
        $('img.lazy').lazyload({
            effect : 'fadeIn'
        });
        $('img.lazy2').lazyload({
            effect : 'fadeIn'
        });
    });
    var root_url = '<?php echo Yii::app()->getBaseUrl(true); ?>';
    </script>
</body>
</html>