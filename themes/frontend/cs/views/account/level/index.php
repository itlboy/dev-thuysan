<div class="fix_width body">
<div class="fl col_s1" style="width: 200px;">

    <!-- User Panel -->
    <?php $this->widget('application.modules.account.widgets.WidgetLoginForm'); ?>
    <!-- END User Panel -->

    <!-- Menu -->
    <?php $this->widget('category.widgets.frontend.treeCategory', array(
    'module' => 'film',
    'view' => 'treeCategory_letphim',
)); ?>
    <?php
    $banner_from = strtotime('2012-08-08 00:00:00');
    $banner_to = strtotime('2012-09-08 23:59:59');
    $banner1 = ($banner_from < time() AND time() < $banner_to) ? TRUE : FALSE;
    ?>
    <div class="ct-ads" style="margin-top: 10px;">
        <?php if($banner1): ?>
        <!--                Dat quang cao phia duoi danh muc-->
        <?php else: ?>
        <a href="https://docs.google.com/spreadsheet/ccc?key=0AukxH5-6U2AIdGZfSzlwcnVvcUtMMzhncGthOVRiaEE#gid=0" target="_blank">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/ads/default/Left_banner_200x200.jpg" width="200px" height="200px" />
        </a>
        <?php endif; ?>
    </div>

</div>
<div class="col_s1_backgound"></div>

<?php $attributes = KitAccountStats::model()->attributeNames();
    $attributesNew = array();
    foreach($attributes as $key => $attribute)
        if ($attribute != 'user_id'){
            $attributesNew[] = $attribute;
        };
?>
<div class="fl col_s2" style="width: 770px;">
    <div class="padding clearfix">
        <h2 style="font-weight: bold;font-size: 20px; text-align: center">Thông tin cấp độ của bạn</h2>
        <?php if(!empty($level)): ?>
        <table cellpadding="0" cellspacing="0" class="items table table-bordered table-level">
            <thead>
                <tr>
                    <th align="center">Điều kiện </th>
                    <th align="center">Thông tin của bạn</th>
                    <th align="center">Cấp độ tiếp theo</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $configs = json_decode($level['config'],true);
                for($i = 0; $i < count($attributesNew); $i++){
                    if(!isset($configs[$i]['field'])){
                        $configs[$i] = array(
                            'field' => $attributesNew[$i],
                            'comparing' => '',
                            'conditions' => 0,
                        );
                    }
                }

                $nextConfigs = json_decode($nextlevel['config'],true);
                for($i = 0; $i < count($attributesNew); $i++){
                    if(!isset($nextConfigs[$i]['field'])){
                        $nextConfigs[$i] = array(
                            'field' => $attributesNew[$i],
                            'comparing' => '',
                            'conditions' => 0,
                        );
                    }
                }
                ?>
                <?php foreach($attributesNew as $key => $attribute): ?>
            <tr>
                <td width="150">
                    <?php echo $attribute; ?>
                </td>
                <td>
                    <?php echo number_format(KitAccountStats::getField(Yii::app()->user->id,$attribute),0,',','.'); ?>
                </td>
                <td>
                    <?php foreach($nextConfigs as $config): ?>
                    <?php echo ($config['field'] == $attribute) ?  $config['comparing'].' '.number_format($config['conditions'],0,',','.') : ''; ?>
                    <?php endforeach; ?>
                </td>

            </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php endif; ?>
    </div>
    <div class="padding">
        <h2 style="font-weight: bold;font-size: 20px; text-align: center">Danh sách cấp độ</h2>
        <table class="items table table-bordered table-level" style="background-color: white">
            <thead>

                <th colspan="2" style="text-align: center">Cấp độ</th>
                <?php foreach($attributesNew as $attribute): ?>
                <th ><?php echo $attribute; ?></th>
                <?php endforeach; ?>
            </thead>
            <tbody>
                <?php foreach($data as $item): ?>
                <?php
                    $configs = json_decode($item['config'],true);
                    for($i = 0; $i < count($attributesNew); $i++){
                        if(!isset($configs[$i]['field'])){
                            $configs[$i] = array(
                                'field' => $attributesNew[$i],
                                'comparing' => '',
                                'conditions' => 0,
                            );
                        }
                    }
                ?>
                <tr class="odd">
                    <td width="1%"><img src="<?php echo KitAccountLevel::getField($item['id'],'image_url') ?>" width="25" /></td>
                    <td style="width: 60px" ><?php echo $item['title']; ?></td>
                    <?php foreach($attributesNew as $attribute): ?>
                        <?php foreach($configs as $config): ?>
                            <?php echo ($config['field'] == $attribute) ? '<td align="center">' . $config['comparing'].' '.number_format($config['conditions'],0,',','.') . '</td>' : ''; ?>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </tr>
                <?php endforeach ?>

            </tbody>
        </table>
    </div>
    <div class="col_s2_backgound" style="border-radius: 0px;width: 770px"></div>
</div>


</div>