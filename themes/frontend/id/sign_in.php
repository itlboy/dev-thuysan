<?php include './_header2.php'; ?>
    <body class='contrast-grass-green sign-in contrast-background'>
        <div id='wrapper'>
            <div class='application'>
                <div class='application-content'>
                    <a href="sign_in.php"><div class='icon-heart'></div>
                        <span>LetID</span>
                    </a>
                </div>
            </div>
            <div class='controls'>
                <div class='caret'></div>
                <div class='form-wrapper'>
                    <h1 class='text-center'>Đăng nhập</h1>
                    <form accept-charset="UTF-8" action="index.php" method="get" /><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                    <div class='row-fluid'>
                        <div class='span12 icon-over-input'>
                            <input class="span12" id="email" name="email" placeholder="E-mail" type="text" value="" />
                            <i class='icon-user muted'></i>
                        </div>
                    </div>
                    <div class='row-fluid'>
                        <div class='span12 icon-over-input'>
                            <input class="span12" id="password" name="password" placeholder="Password" type="password" value="" />
                            <i class='icon-lock muted'></i>
                        </div>
                    </div>
                    <label class="checkbox" for="remember_me"><input id="remember_me" name="remember_me" type="checkbox" value="1" />
                        Ghi nhớ đăng nhập
                    </label>
                    <button class="btn btn-block" name="button" type="submit">Đăng nhập</button>
                    </form>
                    <div class='text-center'>
                        <hr class='hr-normal' />
                        <a href="forgot_password.php">Quên mật khẩu?</a>
                    </div>
                </div>
            </div>
            <div class='login-action text-center'>
                <a href="sign_up.php"><i class='icon-user'></i>
                    Bạn chưa có tài khoản LetID? Hãy <strong>Đăng ký</strong> ngay bây giờ!
                </a>
            </div>
        </div>
    </body>
<?php include './_footer2.php'; ?>