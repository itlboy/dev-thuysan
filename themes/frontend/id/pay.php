<?php include './_header.php'; ?>
            <section id='content'>
                <div class='container-fluid'>
                    <div class='row-fluid' id='content-wrapper'>
                        <div class='span12'>
                            <div class='page-header'>
                                <h1 class='pull-left'>
                                    <i class='icon-credit-card'></i>
                                    <span>Nạp <?php echo KitCmsConfig::getValue('currency_gold_name'); ?></span>
                                </h1>
                                <div class='pull-right'>
                                    <ul class='breadcrumb'>
                                        <li>
                                            <a href="index.php" title="Tổng quan"><i class='icon-dashboard'></i></a>
                                        </li>
                                        <li class='separator'>
                                            <i class='icon-angle-right'></i>
                                        </li>
                                        <li class='active'>Nạp <?php echo KitCmsConfig::getValue('currency_gold_name'); ?></li>
                                    </ul>
                                </div>

                            </div>

                        </div>
                    </div>

                    <div class='row-fluid'>
                        <div class="span12 box box-bordered">
                            <div class="box-header box-header-small" style="font-size:12px;background-color:#fff;">
                                <div class='row-fluid'>
                                    <div class="span6">
                                        <h4>NẠP QUA THẺ CÀO</h4>
                                        <div>
                                            <ul class="unstyled">
                                                <li><i class="icon-ok text-green"></i> Tiện lợi, nhiều mệnh giá khác nhau</li>
                                                <li><i class="icon-ok text-green"></i> Dễ dàng mua được ở mọi nơi</li>
                                                <li><i class="icon-ok text-green"></i> Tỉ lệ quy đổi: 100.000 VND = 100 <?php echo KitCmsConfig::getValue('currency_gold_name'); ?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="span6">
                                        <h4>Chọn nhanh loại thẻ cào</h4>
                                        <div class='row-fluid'>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class='row-fluid'>
                        <div class="span12 box box-bordered">
                            <div class="box-header box-header-small" style="font-size:12px;background-color:#fff;">
                                <div class='row-fluid'>
                                    <div class="span6">
                                        <h4>THẺ VISA, MASTER VÀ NGÂN HÀNG NỘI ĐỊA</h4>
                                        <div>
                                            <ul class="unstyled">
                                                <li><i class="icon-ok text-green"></i> Không mất phí thanh toán</li>
                                                <li><i class="icon-ok text-green"></i> Thanh toán nhanh gọn và có thể sử dụng dịch vụ ngay sau khi thanh toán.</li>
                                                <li><i class="icon-ok text-green"></i> Thẻ của quý khách phải được kích hoạt chức năng thanh toán trực tuyến hoặc đã đăng ký Internet Banking.</li>
                                                <li><i class="icon-ok text-green"></i> Tỉ lệ quy đổi: 100.000 VND = 103 <?php echo KitCmsConfig::getValue('currency_gold_name'); ?> (Khuyến mại 3% giá trị thanh toán).</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="span6">
                                        <h4>Chọn nhanh thẻ</h4>
                                        <div class='row-fluid'>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                        </div>
                                        <div class='row-fluid' style="margin-top: 10px;">
                                            <div class="span2" style="background: #18b2e6;"></div>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                        </div>
                                        <div class='row-fluid' style="margin-top: 10px;">
                                            <div class="span2" style="background: #18b2e6;"></div>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                            <div class="span2" style="background: #18b2e6;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
<?php include './_footer.php'; ?>