<!DOCTYPE html>
<html>
    <head>
        <title>LetID</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' />

        <!--[if lt IE 9]>
        <script src='assets/javascripts/html5shiv.js' type='text/javascript'></script>
        <![endif]-->
        <link href='assets/stylesheets/bootstrap/bootstrap.css' media='all' rel='stylesheet' type='text/css' />
        <link href='assets/stylesheets/bootstrap/bootstrap-responsive.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / jquery ui -->
        <link href='assets/stylesheets/jquery_ui/jquery-ui-1.10.0.custom.css' media='all' rel='stylesheet' type='text/css' />
        <link href='assets/stylesheets/jquery_ui/jquery.ui.1.10.0.ie.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / switch buttons -->
        <link href='assets/stylesheets/plugins/bootstrap_switch/bootstrap-switch.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / xeditable -->
        <link href='assets/stylesheets/plugins/xeditable/bootstrap-editable.css' media='all' rel='stylesheet' type='text/css' />
        <link href='assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / wysihtml5 (wysywig) -->
        <link href='assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / jquery file upload -->
        <link href='assets/stylesheets/plugins/jquery_fileupload/jquery.fileupload-ui.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / full calendar -->
        <link href='assets/stylesheets/plugins/fullcalendar/fullcalendar.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / select2 -->
        <link href='assets/stylesheets/plugins/select2/select2.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / mention -->
        <link href='assets/stylesheets/plugins/mention/mention.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / tabdrop (responsive tabs) -->
        <link href='assets/stylesheets/plugins/tabdrop/tabdrop.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / jgrowl notifications -->
        <link href='assets/stylesheets/plugins/jgrowl/jquery.jgrowl.min.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / datatables -->
        <link href='assets/stylesheets/plugins/datatables/bootstrap-datatable.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / dynatrees (file trees) -->
        <link href='assets/stylesheets/plugins/dynatree/ui.dynatree.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / color picker -->
        <link href='assets/stylesheets/plugins/bootstrap_colorpicker/bootstrap-colorpicker.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / datetime picker -->
        <link href='assets/stylesheets/plugins/bootstrap_datetimepicker/bootstrap-datetimepicker.min.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / daterange picker) -->
        <link href='assets/stylesheets/plugins/bootstrap_daterangepicker/bootstrap-daterangepicker.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / flags (country flags) -->
        <link href='assets/stylesheets/plugins/flags/flags.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / slider nav (address book) -->
        <link href='assets/stylesheets/plugins/slider_nav/slidernav.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / fuelux (wizard) -->
        <link href='assets/stylesheets/plugins/fuelux/wizard.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / flatty theme -->
        <link href='assets/stylesheets/light-theme.css' id='color-settings-body-color' media='all' rel='stylesheet' type='text/css' />
        <!-- / demo -->
        <link href='assets/stylesheets/demo.css' media='all' rel='stylesheet' type='text/css' />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>
    <body class='contrast-grass-green '>
        <header>
            <div class='navbar'>
                <div class='navbar-inner'>
                    <div class='container-fluid'>
                        <a class='brand' href='index.html'>
                            <i class='icon-heart-empty'></i>
                            <span class='hidden-phone'>Let ID</span>
                        </a>
                        <a class='toggle-nav btn pull-left' href='#'>
                            <i class='icon-reorder'></i>
                        </a>
                        <ul class='nav pull-right'>
                            <li class='dropdown light only-icon'>
                                <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
                                    <i class='icon-adjust'></i>
                                </a>
                                <ul class='dropdown-menu color-settings'>
                                    <li class='color-settings-body-color'>
                                        <div class='color-title'>Body color</div>
                                        <a data-change-to='assets/stylesheets/light-theme.css' href='#'>
                                            Light
                                            <small>(default)</small>
                                        </a>
                                        <a data-change-to='assets/stylesheets/dark-theme.css' href='#'>
                                            Dark
                                        </a>
                                        <a data-change-to='assets/stylesheets/dark-blue-theme.css' href='#'>
                                            Dark blue
                                        </a>
                                    </li>
                                    <li class='divider'></li>
                                    <li class='color-settings-contrast-color'>
                                        <div class='color-title'>Contrast color</div>
                                        <a href="#" data-change-to="contrast-red"><i class='icon-adjust text-red'></i>
                                            Red
                                            <small>(default)</small>
                                        </a>
                                        <a href="#" data-change-to="contrast-blue"><i class='icon-adjust text-blue'></i>
                                            Blue
                                        </a>
                                        <a href="#" data-change-to="contrast-orange"><i class='icon-adjust text-orange'></i>
                                            Orange
                                        </a>
                                        <a href="#" data-change-to="contrast-purple"><i class='icon-adjust text-purple'></i>
                                            Purple
                                        </a>
                                        <a href="#" data-change-to="contrast-green"><i class='icon-adjust text-green'></i>
                                            Green
                                        </a>
                                        <a href="#" data-change-to="contrast-muted"><i class='icon-adjust text-muted'></i>
                                            Muted
                                        </a>
                                        <a href="#" data-change-to="contrast-fb"><i class='icon-adjust text-fb'></i>
                                            Facebook
                                        </a>
                                        <a href="#" data-change-to="contrast-dark"><i class='icon-adjust text-dark'></i>
                                            Dark
                                        </a>
                                        <a href="#" data-change-to="contrast-pink"><i class='icon-adjust text-pink'></i>
                                            Pink
                                        </a>
                                        <a href="#" data-change-to="contrast-grass-green"><i class='icon-adjust text-grass-green'></i>
                                            Grass green
                                        </a>
                                        <a href="#" data-change-to="contrast-sea-blue"><i class='icon-adjust text-sea-blue'></i>
                                            Sea blue
                                        </a>
                                        <a href="#" data-change-to="contrast-banana"><i class='icon-adjust text-banana'></i>
                                            Banana
                                        </a>
                                        <a href="#" data-change-to="contrast-dark-orange"><i class='icon-adjust text-dark-orange'></i>
                                            Dark orange
                                        </a>
                                        <a href="#" data-change-to="contrast-brown"><i class='icon-adjust text-brown'></i>
                                            Brown
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class='dropdown medium only-icon widget'>
                                <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
                                    <i class='icon-rss'></i>
                                    <div class='label'>5</div>
                                </a>
                                <ul class='dropdown-menu'>
                                    <li>
                                        <a href='#'>
                                            <div class='widget-body'>
                                                <div class='pull-left icon'>
                                                    <i class='icon-user text-success'></i>
                                                </div>
                                                <div class='pull-left text'>
                                                    John Doe signed up
                                                    <small class='muted'>just now</small>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class='divider'></li>
                                    <li>
                                        <a href='#'>
                                            <div class='widget-body'>
                                                <div class='pull-left icon'>
                                                    <i class='icon-inbox text-error'></i>
                                                </div>
                                                <div class='pull-left text'>
                                                    New Order #002
                                                    <small class='muted'>3 minutes ago</small>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class='divider'></li>
                                    <li>
                                        <a href='#'>
                                            <div class='widget-body'>
                                                <div class='pull-left icon'>
                                                    <i class='icon-comment text-warning'></i>
                                                </div>
                                                <div class='pull-left text'>
                                                    America Leannon commented Flatty with veeery long text.
                                                    <small class='muted'>1 hour ago</small>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class='divider'></li>
                                    <li>
                                        <a href='#'>
                                            <div class='widget-body'>
                                                <div class='pull-left icon'>
                                                    <i class='icon-user text-success'></i>
                                                </div>
                                                <div class='pull-left text'>
                                                    Jane Doe signed up
                                                    <small class='muted'>last week</small>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class='divider'></li>
                                    <li>
                                        <a href='#'>
                                            <div class='widget-body'>
                                                <div class='pull-left icon'>
                                                    <i class='icon-inbox text-error'></i>
                                                </div>
                                                <div class='pull-left text'>
                                                    New Order #001
                                                    <small class='muted'>1 year ago</small>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class='widget-footer'>
                                        <a href='#'>All notifications</a>
                                    </li>
                                </ul>
                            </li>
                            <li class='dropdown dark user-menu'>
                                <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
                                    <img alt='Ngựa Gỗ' height='23' src='assets/images/avatar.jpg' width='23' />
                                    <span class='user-name hidden-phone'>Ngựa Gỗ</span>
                                    <b class='caret'></b>
                                </a>
                                <ul class='dropdown-menu'>
                                    <li>
                                        <a href='user_profile.php'>
                                            <i class='icon-user'></i>
                                            Thông tin cá nhân
                                        </a>
                                    </li>
                                    <li>
                                        <a href='javascript:void(0);'>
                                            <i class='icon-cog'></i>
                                            Thiết lập
                                        </a>
                                    </li>
                                    <li class='divider'></li>
                                    <li>
                                        <a href='sign_in.php'>
                                            <i class='icon-signout'></i>
                                            Thoát
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <form accept-charset="UTF-8" action="search_results.php" class="navbar-search pull-right hidden-phone" method="get" /><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                        <button class="btn btn-link icon-search" name="button" type="submit"></button>
                        <input autocomplete="off" class="search-query span2" id="q_header" name="q" placeholder="Search..." type="text" value="" />
                        </form>
                    </div>
                </div>
            </div>
        </header>
        <div id='wrapper'>
            <div id='main-nav-bg'></div>
            <nav class='' id='main-nav'>
                <div class='navigation'>
                    <div class='search'>
                        <form accept-charset="UTF-8" action="search_results.html" method="get">
                        <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                        <div class='search-wrapper'>
                            <input autocomplete="off" class="search-query" id="q" name="q" placeholder="Search..." type="text" value="" />
                            <button class="btn btn-link icon-search" name="button" type="submit"></button>
                        </div>
                        </form>
                    </div>
                    <?php include '_menu.php'; ?>
                </div>
            </nav>
