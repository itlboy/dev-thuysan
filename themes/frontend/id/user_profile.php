<?php include './_header.php'; ?>
            <section id='content'>
                <div class='container-fluid'>
                    <div class='row-fluid' id='content-wrapper'>
                        <div class='span12'>
                            <div class='page-header'>
                                <h1 class='pull-left'>
                                    <i class='icon-user'></i>
                                    <span>Thông tin cá nhân</span>
                                </h1>
                                <div class='pull-right'>
                                    <ul class='breadcrumb'>
                                        <li>
                                            <a href="index.php" title="Tổng quan"><i class='icon-dashboard'></i></a>
                                        </li>
                                        <li class='separator'>
                                            <i class='icon-angle-right'></i>
                                        </li>
                                        <li class='active'>Thông tin cá nhân</li>
                                    </ul>
                                </div>
                            </div>
                            <div class='row-fluid'>
                                <div class='span3 box'>
                                    <div class='box-content'>
                                        <img alt="230x230&amp;text=photo" src="http://placehold.it/230x230&amp;text=Avatar" />
                                    </div>
                                </div>
                                <div class='span9 box'>
                                    <div class='box-content box-double-padding'>
                                        <form class='form' style='margin-bottom: 0;'>
                                        <fieldset>
                                            <div class='span4'>
                                                <div class='lead'>
                                                    <i class='icon-signin text-contrast'></i>
                                                    Thông tin tài khoản
                                                </div>
                                                <small class='muted'>Đây là những thông tin quan trọng liên quan trực tiếp đến tài khoản của bạn. Hãy nhập chính xác nếu muốn thay đổi !</small>
                                            </div>
                                            <div class='span7 offset1'>
                                                <div class='control-group'>
                                                    <label class='control-label'>Tên truy cập</label>
                                                    <div class='controls'>
                                                        <input class='span12' placeholder='nguago' type='text' disabled="disabled" />
                                                    </div>
                                                </div>
                                                <div class='control-group'>
                                                    <label class='control-label'>E-mail</label>
                                                    <div class='controls'>
                                                        <input class='span12' id='email' placeholder='Nhập E-mail' type='text' />
                                                    </div>
                                                </div>
                                                <hr class='hr-normal' />
                                                <div class='control-group'>
                                                    <div class='controls'>
                                                        <label class='checkbox'>
                                                            <input data-target='#change-password' data-toggle='collapse' id='changepasswordcheck' type='checkbox' value='option1' />
                                                            Bạn có muốn thay đổi mật khẩu?
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class='collapse' id='change-password'>
                                                    <div class='control-group'>
                                                        <label class='control-label'>Mật khẩu cũ</label>
                                                        <div class='controls'>
                                                            <input class='span12' id='password_old' placeholder='Nhập mật khẩu cũ' type='password' />
                                                        </div>
                                                    </div>
                                                    <div class='control-group'>
                                                        <label class='control-label'>Mật khẩu mới</label>
                                                        <div class='controls'>
                                                            <input class='span12' id='password_new' placeholder='Nhập mật khẩu mới' type='password' />
                                                        </div>
                                                    </div>
                                                    <div class='control-group'>
                                                        <label class='control-label'>Xác nhận mật khẩu mới</label>
                                                        <div class='controls'>
                                                            <input class='span12' id='password_confirmation' placeholder='Xác nhận mật khẩu mới' type='password' />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <hr class='hr-normal' />
                                        <fieldset>
                                            <div class='span4 box'>
                                                <div class='lead'>
                                                    <i class='icon-barcode text-contrast'></i>
                                                    Thông tin xác thực
                                                </div>
                                                <small class='muted'>Đây là những thông tin rất quan trọng, nó giúp bạn lấy lại tài khoản dễ dàng khi bị đánh cắp. Thông tin này chỉ có thể thay đổi 1 lần!</small>
                                            </div>
                                            <div class='span7 offset1'>
                                                <div class='control-group'>
                                                    <label class='control-label'>Số chứng minh nhân dân</label>
                                                    <div class='controls'>
                                                        <input class='span12' name='fullname' placeholder='Nhập số CMND' type='text' />
                                                    </div>
                                                </div>
                                                <div class='control-group'>
                                                    <label class='control-label'>Ngày cấp</label>
                                                    <div class='controls'>
                                                        <div class="input-append datepicker">
                                                            <input class='span12' name='ngaycap' type='text' />
                                                            <span class="add-on"><i class="icon-th"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class='control-group'>
                                                    <label class='control-label'>Nơi cấp</label>
                                                    <div class='controls'>
                                                        <input class='span12' name='fullname' placeholder='Nhập nơi cấp CMND' type='text' />
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <hr class='hr-normal' />
                                        <fieldset>
                                            <div class='span4 box'>
                                                <div class='lead'>
                                                    <i class='icon-user text-contrast'></i>
                                                    Thông tin cá nhân
                                                </div>
                                                <small class='muted'>Đây là những thông tin cá nhân. Nhập đầy đủ là cách bạn có thể mở rộng quan hệ bạn bè.</small>
                                            </div>
                                            <div class='span7 offset1'>
                                                <div class='control-group'>
                                                    <label class='control-label'>Họ và tên</label>
                                                    <div class='controls'>
                                                        <input class='span12' name='fullname' placeholder='Nhập tên thật của bạn' type='text' />
                                                    </div>
                                                </div>
                                                <div class='control-group'>
                                                    <label class='control-label'>Tên hiển thị</label>
                                                    <div class='controls'>
                                                        <input class='span12' name='displayname' placeholder='Tên mà người khác nhìn thấy bạn' type='text' />
                                                    </div>
                                                </div>
                                                <hr />
                                                <div class='control-group'>
                                                    <label class='control-label'>Chữ ký</label>
                                                    <div class='controls'>
                                                        <textarea class='autosize span12' id='bio' placeholder='Nhập chữ ký'></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <div class='form-actions' style='margin-bottom: 0;'>
                                            <div class='text-right'>
                                                <div class='btn btn-primary btn-large'>
                                                    <i class='icon-save'></i>
                                                    Save
                                                </div>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
<?php include './_footer.php'; ?>