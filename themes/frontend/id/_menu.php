                    <ul class='nav nav-stacked'>
                        <li class='active'>
                            <a href='index.php'>
                                <i class='icon-dashboard'></i>
                                <span>Tổng quan</span>
                            </a>
                        </li>
                        <li class=''>
                            <a href='user_profile.php'>
                                <i class='icon-info-sign'></i>
                                <span>Thông tin cá nhân</span>
                            </a>
                        </li>
                        <li class=''>
                            <a href='user_avatar.php'>
                                <i class='icon-user'></i>
                                <span>Thay đổi avatar</span>
                            </a>
                        </li>
                        <li class=''>
                            <a class='dropdown-collapse' href='javascript:void(0);'>
                                <i class='icon-credit-card'></i>
                                <span>Nạp <?php echo KitCmsConfig::getValue('currency_gold_name'); ?></span>
                                <i class='icon-angle-down angle-down'></i>
                            </a>
                            <ul class='nav nav-stacked'>
                                <li class=''>
                                    <a href='pay.php'>
                                        <i class='icon-caret-right'></i>
                                        <span>Nạp <?php echo KitCmsConfig::getValue('currency_gold_name'); ?></span>
                                    </a>
                                </li>
                                <li class=''>
                                    <a href='pay_history.php'>
                                        <i class='icon-caret-right'></i>
                                        <span>Lịch sử nạp tiền</span>
                                    </a>
                                </li>
                                <li class=''>
                                    <a href='gold_history.php'>
                                        <i class='icon-caret-right'></i>
                                        <span>Lịch sử quy đổi</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
