<?php LetIdentityId::checkLogin(); ?>
<!DOCTYPE html>
<html>
    <head>
        <title>LetID</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' />

        <?php $baseUrlTheme = Yii::app()->theme->baseUrl; ?>

        <!--[if lt IE 9]>
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/html5shiv.js' type='text/javascript'></script>
        <![endif]-->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/bootstrap/bootstrap.css' media='all' rel='stylesheet' type='text/css' />
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/bootstrap/bootstrap-responsive.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / jquery ui -->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/jquery_ui/jquery-ui-1.10.0.custom.css' media='all' rel='stylesheet' type='text/css' />
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/jquery_ui/jquery.ui.1.10.0.ie.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / switch buttons -->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/plugins/bootstrap_switch/bootstrap-switch.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / xeditable -->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/plugins/xeditable/bootstrap-editable.css' media='all' rel='stylesheet' type='text/css' />
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / wysihtml5 (wysywig) -->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/plugins/common/bootstrap-wysihtml5.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / jquery file upload -->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/plugins/jquery_fileupload/jquery.fileupload-ui.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / full calendar -->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/plugins/fullcalendar/fullcalendar.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / select2 -->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/plugins/select2/select2.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / mention -->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/plugins/mention/mention.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / tabdrop (responsive tabs) -->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/plugins/tabdrop/tabdrop.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / jgrowl notifications -->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/plugins/jgrowl/jquery.jgrowl.min.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / datatables -->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/plugins/datatables/bootstrap-datatable.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / dynatrees (file trees) -->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/plugins/dynatree/ui.dynatree.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / color picker -->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/plugins/bootstrap_colorpicker/bootstrap-colorpicker.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / datetime picker -->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/plugins/bootstrap_datetimepicker/bootstrap-datetimepicker.min.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / daterange picker) -->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/plugins/bootstrap_daterangepicker/bootstrap-daterangepicker.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / flags (country flags) -->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/plugins/flags/flags.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / slider nav (address book) -->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/plugins/slider_nav/slidernav.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / fuelux (wizard) -->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/plugins/fuelux/wizard.css' media='all' rel='stylesheet' type='text/css' />
        <!-- / flatty theme -->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/light-theme.css' id='color-settings-body-color' media='all' rel='stylesheet' type='text/css' />
        <!-- / demo -->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/demo.css' media='all' rel='stylesheet' type='text/css' />
        
        <!--/ Let id        -->
        <link href='<?php echo $baseUrlTheme; ?>/assets/stylesheets/let_id.css' media='all' rel='stylesheet' type='text/css' />

        <!-- / jquery -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/jquery/jquery.min.js' type='text/javascript'></script>
        <!-- / jquery mobile events (for touch and slide) -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/mobile_events/jquery.mobile-events.min.js' type='text/javascript'></script>
        <!-- / jquery migrate (for compatibility with new jquery) -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/jquery/jquery-migrate.min.js' type='text/javascript'></script>
        <!-- / jquery ui -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/jquery_ui/jquery-ui.min.js' type='text/javascript'></script>
        <!-- / bootstrap -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/bootstrap/bootstrap.min.js' type='text/javascript'></script>
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/flot/excanvas.js' type='text/javascript'></script>
        <!-- / sparklines -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/sparklines/jquery.sparkline.min.js' type='text/javascript'></script>
        <!-- / flot charts -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/flot/flot.min.js' type='text/javascript'></script>
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/flot/flot.resize.js' type='text/javascript'></script>
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/flot/flot.pie.js' type='text/javascript'></script>
        <!-- / bootstrap switch -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/bootstrap_switch/bootstrapSwitch.min.js' type='text/javascript'></script>
        <!-- / fullcalendar -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/fullcalendar/fullcalendar.min.js' type='text/javascript'></script>
        <!-- / datatables -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/datatables/jquery.dataTables.min.js' type='text/javascript'></script>
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/datatables/jquery.dataTables.columnFilter.js' type='text/javascript'></script>
        <!-- / wysihtml5 -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/common/wysihtml5.min.js' type='text/javascript'></script>
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/common/bootstrap-wysihtml5.js' type='text/javascript'></script>
        <!-- / select2 -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/select2/select2.js' type='text/javascript'></script>
        <!-- / color picker -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/bootstrap_colorpicker/bootstrap-colorpicker.min.js' type='text/javascript'></script>
        <!-- / mention -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/mention/mention.min.js' type='text/javascript'></script>
        <!-- / input mask -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/input_mask/bootstrap-inputmask.min.js' type='text/javascript'></script>
        <!-- / fileinput -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/fileinput/bootstrap-fileinput.js' type='text/javascript'></script>
        <!-- / modernizr -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/modernizr/modernizr.min.js' type='text/javascript'></script>
        <!-- / retina -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/retina/retina.js' type='text/javascript'></script>
        <!-- / fileupload -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/fileupload/tmpl.min.js' type='text/javascript'></script>
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/fileupload/load-image.min.js' type='text/javascript'></script>
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/fileupload/canvas-to-blob.min.js' type='text/javascript'></script>
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/fileupload/jquery.iframe-transport.min.js' type='text/javascript'></script>
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/fileupload/jquery.fileupload.min.js' type='text/javascript'></script>
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/fileupload/jquery.fileupload-fp.min.js' type='text/javascript'></script>
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/fileupload/jquery.fileupload-ui.min.js' type='text/javascript'></script>
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/fileupload/jquery.fileupload-init.js' type='text/javascript'></script>
        <!-- / timeago -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/timeago/jquery.timeago.js' type='text/javascript'></script>
        <!-- / slimscroll -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/slimscroll/jquery.slimscroll.min.js' type='text/javascript'></script>
        <!-- / autosize (for textareas) -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/autosize/jquery.autosize-min.js' type='text/javascript'></script>
        <!-- / charCount -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/charCount/charCount.js' type='text/javascript'></script>
        <!-- / validate -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/validate/jquery.validate.min.js' type='text/javascript'></script>
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/validate/additional-methods.js' type='text/javascript'></script>
        <!-- / naked password -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/naked_password/naked_password-0.2.4.min.js' type='text/javascript'></script>
        <!-- / nestable -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/nestable/jquery.nestable.js' type='text/javascript'></script>
        <!-- / tabdrop -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/tabdrop/bootstrap-tabdrop.js' type='text/javascript'></script>
        <!-- / jgrowl -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/jgrowl/jquery.jgrowl.min.js' type='text/javascript'></script>
        <!-- / bootbox -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/bootbox/bootbox.min.js' type='text/javascript'></script>
        <!-- / inplace editing -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/xeditable/bootstrap-editable.min.js' type='text/javascript'></script>
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/xeditable/wysihtml5.js' type='text/javascript'></script>
        <!-- / ckeditor -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/ckeditor/ckeditor.js' type='text/javascript'></script>
        <!-- / filetrees -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/dynatree/jquery.dynatree.min.js' type='text/javascript'></script>
        <!-- / datetime picker -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/bootstrap_datetimepicker/bootstrap-datetimepicker.js' type='text/javascript'></script>
        <!-- / daterange picker -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/bootstrap_daterangepicker/moment.min.js' type='text/javascript'></script>
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/bootstrap_daterangepicker/bootstrap-daterangepicker.js' type='text/javascript'></script>
        <!-- / max length -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/bootstrap_maxlength/bootstrap-maxlength.min.js' type='text/javascript'></script>
        <!-- / dropdown hover -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/bootstrap_hover_dropdown/twitter-bootstrap-hover-dropdown.min.js' type='text/javascript'></script>
        <!-- / slider nav (address book) -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/slider_nav/slidernav-min.js' type='text/javascript'></script>
        <!-- / fuelux -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/plugins/fuelux/wizard.js' type='text/javascript'></script>
        <!-- / flatty theme -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/nav.js' type='text/javascript'></script>
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/tables.js' type='text/javascript'></script>
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/theme.js' type='text/javascript'></script>
        <!-- / demo -->
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/demo/jquery.mockjax.js' type='text/javascript'></script>
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/demo/inplace_editing.js' type='text/javascript'></script>
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/demo/charts.js' type='text/javascript'></script>
        <script src='<?php echo $baseUrlTheme; ?>/assets/javascripts/demo/demo.js' type='text/javascript'></script>
        
        
    </head>
    <body class='contrast-grass-green '>
        <header>
            <div class='navbar'>
                <div class='navbar-inner'>
                    <div class='container-fluid'>
                        <a class='brand' href='<?php echo Yii::app()->getBaseUrl(true); ?>'>
                            <i class='icon-heart-empty'></i>
                            <span class='hidden-phone'>Let ID</span>
                        </a>
                        <a class='toggle-nav btn pull-left' href='#'>
                            <i class='icon-reorder'></i>
                        </a>
                        <!--                        <form action="search_results.php" class="navbar-search pull-left hidden-phone" method="get" style="margin-left: 10px;">
                                                    <button class="btn btn-link icon-search" name="button" type="submit"></button>
                                                    <input autocomplete="on" class="search-query span2" name="q" placeholder="Search..." type="text" value="" />
                                                </form>-->
                        <ul class='nav pull-right'>
                            <!--                            <li class='dropdown medium only-icon widget'>
                                                            <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
                                                                <i class='icon-rss'></i>
                                                                <div class='label'>5</div>
                                                            </a>
                                                            <ul class='dropdown-menu'>
                                                                <li>
                                                                    <a href='#'>
                                                                        <div class='widget-body'>
                                                                            <div class='pull-left icon'>
                                                                                <i class='icon-user text-success'></i>
                                                                            </div>
                                                                            <div class='pull-left text'>
                                                                                John Doe signed up
                                                                                <small class='muted'>just now</small>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li class='divider'></li>
                                                                <li>
                                                                    <a href='#'>
                                                                        <div class='widget-body'>
                                                                            <div class='pull-left icon'>
                                                                                <i class='icon-inbox text-error'></i>
                                                                            </div>
                                                                            <div class='pull-left text'>
                                                                                New Order #002
                                                                                <small class='muted'>3 minutes ago</small>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li class='divider'></li>
                                                                <li>
                                                                    <a href='#'>
                                                                        <div class='widget-body'>
                                                                            <div class='pull-left icon'>
                                                                                <i class='icon-comment text-warning'></i>
                                                                            </div>
                                                                            <div class='pull-left text'>
                                                                                America Leannon commented Flatty with veeery long text.
                                                                                <small class='muted'>1 hour ago</small>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li class='divider'></li>
                                                                <li>
                                                                    <a href='#'>
                                                                        <div class='widget-body'>
                                                                            <div class='pull-left icon'>
                                                                                <i class='icon-user text-success'></i>
                                                                            </div>
                                                                            <div class='pull-left text'>
                                                                                Jane Doe signed up
                                                                                <small class='muted'>last week</small>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li class='divider'></li>
                                                                <li>
                                                                    <a href='#'>
                                                                        <div class='widget-body'>
                                                                            <div class='pull-left icon'>
                                                                                <i class='icon-inbox text-error'></i>
                                                                            </div>
                                                                            <div class='pull-left text'>
                                                                                New Order #001
                                                                                <small class='muted'>1 year ago</small>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </li>
                                                                <li class='widget-footer'>
                                                                    <a href='#'>All notifications</a>
                                                                </li>
                                                            </ul>
                                                        </li>-->
                            <li class=''>
                                <?php echo KitCmsConfig::getValue('currency_gold_name'); ?>: <?php echo KitAccountStats::getField(Yii::app()->user->id, 'gold_current'); ?>
                            </li>
                            <li class='dropdown medium only-icon widget'>
                                <a class='dropdown-toggle' data-toggle='dropdown' href='#'>Hỗ trợ trực tuyến <b class='caret'></b></a>
                                <ul class='dropdown-menu'>
                                    <li>
                                        <div class='widget-body'>
                                            <div class='pull-left icon' style="font-size: 40px; width: 50px;">
                                                <i class='icon-phone text-success'></i>
                                            </div>
                                            <div class='pull-left text' style="width: 200px;">
                                                <div><strong>Hỗ trợ qua điện thoại</strong></div>
                                                <div>04 36884989</div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class='divider'></li>
                                    <li>
                                        <div class='widget-body'>
                                            <div class='pull-left icon' style="font-size: 40px; width: 50px;">
                                                <i class='icon-envelope text-success'></i>
                                            </div>
                                            <div class='pull-left text' style="width: 200px;">
                                                <div><strong>Hỗ trợ qua Email</strong></div>
                                                <div><a href="mailto:hotro@let.vn" rel="nofollow">hotro@let.vn</a></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class='divider'></li>
                                    <li>
                                        <div class='widget-body'>
                                            <div class='pull-left icon' style="font-size: 40px; width: 50px;">
                                                <i class='icon-user-md text-success'></i>
                                            </div>
                                            <div class='pull-left text' style="width: 200px;">
                                                <div><strong>Hỗ trợ qua chat</strong></div>
                                                <div>
                                                    <a href="skype:let_hotro?chat" rel="nofollow">Skype</a> |
                                                    <a href="ymsgr:sendim?let_hotro" rel="nofollow">Yahoo</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class='divider'></li>

                                </ul>
                            </li>
                            <li class='dropdown dark user-menu'>
                                <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
                                    <?php 
                                        $user = KitAccount::model()->findByPk(Yii::app()->user->id);
                                        $avatarUrl = KitAccount::getAvatarUrl('medium', $user->avatar); 
                                    ?>
                                    <img alt='<?php echo $user->username; ?>' height='23' src='<?php echo $avatarUrl; ?>' width='23' />
                                    <span class='user-name hidden-phone'><?php echo Yii::app()->user->name; ?></span>
                                    <b class='caret'></b>
                                </a>
                                <ul class='dropdown-menu'>
                                    <li>
                                        <a href='<?php echo Yii::app()->createUrl('account/profile/edit'); ?>'>
                                            <i class='icon-user'></i>
                                            Thông tin cá nhân
                                        </a>
                                    </li>
                                    <li>
                                        <a href='javascript:void(0);'>
                                            <i class='icon-cog'></i>
                                            Thiết lập tài khoản
                                        </a>
                                    </li>
                                    <li class='divider'></li>
                                    <li>
                                        <a href='<?php echo Yii::app()->createUrl('currency/pay'); ?>'>
                                            <i class='icon-certificate'></i>
                                            Nạp <?php echo KitCmsConfig::getValue('currency_gold_name'); ?>
                                        </a>
                                    </li>
                                    <li class='divider'></li>
                                    <li>
                                        <a href='<?php echo $this->createUrl('//account/frontend/id/logout'); ?>'>
                                            <i class='icon-signout'></i>
                                            Thoát
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        <div id='wrapper'>
            <div id='main-nav-bg'></div>
            <nav class='' id='main-nav'>
                <div class='navigation'>
                    <div class='search'>
                        <form accept-charset="UTF-8" action="search_results.html" method="get">
                            <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                            <div class='search-wrapper'>
                                <input autocomplete="off" class="search-query" id="q" name="q" placeholder="Search..." type="text" value="" />
                                <button class="btn btn-link icon-search" name="button" type="submit"></button>
                            </div>
                        </form>
                    </div>
                    <?php $this->renderPartial('//_block/_menu'); ?>
                </div>
            </nav>
            <?php echo $content; ?>
            <div class="pull-right" style="margin-right: 20px;">
                <div style="text-align: right;">Trụ sở: D19 Hồng Châu, khu Du lịch sinh thái Hà Hải, Lê Thanh Nghị, Hải Dương</div>
                <div style="text-align: right;">Giao dịch: Phòng 808, chung cư An Lạc, Phùng Khoang, Từ Liêm, Hà Nội</div>
            </div>;
        </div>
    </body>
</html>