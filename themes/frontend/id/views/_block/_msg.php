<?php if (isset($msg)): ?>
    <?php foreach ($msg as $type => $arrayMsg): ?>
        <div class='alert alert-<?php echo $type; ?>'>
            <a class='close' data-dismiss='alert' href='#'>&times;</a>
            <?php foreach ($arrayMsg as $row): ?>
                <div><i class='icon-chevron-right'></i> <?php echo $row; ?></div>
            <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
