<?php
$module = Yii::app()->controller->module->id;
$controller = Yii::app()->controller->id;
$action = Yii::app()->controller->action->id;
//echo $module . '<br />' . $controller . '<br />' . $action; 
if ($module == 'cms' AND $controller == 'frontend/default')
    $active = 'dashboard';
elseif ($module == 'account' AND $controller == 'frontend/profile' AND $action == 'edit')
    $active = 'profile';
elseif ($module == 'account' AND $controller == 'frontend/profile' AND $action == 'changeemail')
    $active = 'changeemail';
elseif ($module == 'account' AND $controller == 'frontend/profile' AND $action == 'changephone')
    $active = 'changephone';
elseif ($module == 'account' AND $controller == 'frontend/password')
    $active = 'password';
elseif ($module == 'currency' AND $controller == 'frontend/pay')
    $active = 'currency';
elseif ($module == 'currency' AND $controller == 'frontend/exchange')
    $active = 'exchange';
elseif ($module == 'currency' AND $controller == 'frontend/creditcardwallet')
    $active = 'creditcardwallet';
else
    $active = '';
?>
<ul class='nav nav-stacked'>
    <li class='<?php if ($active == 'dashboard'): ?>active<?php endif; ?>'>
        <a href='<?php echo Yii::app()->getBaseUrl(true); ?>'>
            <i class='icon-dashboard'></i>
            <span>Tổng quan</span>
        </a>
    </li>
    <li class='<?php if ($active == 'profile'): ?>active<?php endif; ?>'>
        <a href='<?php echo Yii::app()->createUrl('account/profile/edit'); ?>'>
            <i class='icon-info-sign'></i>
            <span>Thông tin cá nhân</span>
        </a>
    </li>
    <li class='<?php if ($active == 'changeemail'): ?>active<?php endif; ?>'>
        <a href='<?php echo Yii::app()->createUrl('account/profile/changeEmail'); ?>'>
            <i class='icon-envelope'></i>
            <span>Đổi email</span>
        </a>
    </li>
    <li class='<?php if ($active == 'changephone'): ?>active<?php endif; ?>'>
        <a href='<?php echo Yii::app()->createUrl('account/profile/changePhone'); ?>'>
            <i class='icon-mobile-phone'></i>
            <span>Đổi số điện thoại</span>
        </a>
    </li>
    <li class='<?php if ($active == 'password'): ?>active<?php endif; ?>'>
        <a class='dropdown-collapse<?php if ($active == 'password'): ?> in<?php endif; ?>' href='javascript:void(0);'>
            <i class='icon-lock'></i>
            <span>Quản lý mật khẩu</span>
            <i class='icon-angle-down angle-down'></i>
        </a>
        <ul class='nav nav-stacked<?php if ($active == 'password'): ?> in<?php endif; ?>'>
            <li class='<?php if ($active == 'password' AND $action == 'password1'): ?>active<?php endif; ?>'>
                <a href='<?php echo Yii::app()->createUrl('account/password/password1'); ?>'>
                    <i class='icon-caret-right'></i>
                    <span>Mật khẩu cấp 1</span>
                </a>
            </li>
            <li class='<?php if ($active == 'password' AND $action == 'password2'): ?>active<?php endif; ?>'>
                <a href='<?php echo Yii::app()->createUrl('account/password/password2'); ?>'>
                    <i class='icon-caret-right'></i>
                    <span>Mật khẩu cấp 2</span>
                </a>
            </li>
        </ul>
    </li>
    <li class='<?php if ($active == 'currency'): ?>active<?php endif; ?>'>
        <a class='dropdown-collapse<?php if ($active == 'currency'): ?> in<?php endif; ?>' href='javascript:void(0);'>
            <i class='icon-certificate'></i>
            <span>Nạp <?php echo KitCmsConfig::getValue('currency_gold_name'); ?></span>
            <i class='icon-angle-down angle-down'></i>
        </a>
        <ul class='nav nav-stacked<?php if ($active == 'currency'): ?> in<?php endif; ?>'>
            <li class='<?php if ($active == 'currency' AND ($action == 'index' OR $action == 'card')): ?>active<?php endif; ?>'>
                <a href='<?php echo Yii::app()->createUrl('currency/pay'); ?>'>
                    <i class='icon-caret-right'></i>
                    <span>Nạp <?php KitCmsConfig::getValue('currency_gold_name'); ?></span>
                </a>
            </li>
            <li class='<?php if ($active == 'currency' AND $action == 'historypay'): ?>active<?php endif; ?>'>
                <a href='<?php echo Yii::app()->createUrl('currency/pay/historypay'); ?>'>
                    <i class='icon-caret-right'></i>
                    <span>Lịch sử nạp tiền</span>
                </a>
            </li>
            <li class='<?php if ($active == 'currency' AND $action == 'historygold'): ?>active<?php endif; ?>'>
                <a href='<?php echo Yii::app()->createUrl('currency/pay/historygold'); ?>'>
                    <i class='icon-caret-right'></i>
                    <span>Lịch sử quy đổi</span>
                </a>
            </li>
        </ul>
    </li>
    <li class='<?php if ($active == 'exchange'): ?>active<?php endif; ?>'>
        <a href='<?php echo Yii::app()->createUrl('currency/exchange/index'); ?>'>
            <i class='icon-exchange'></i>
            <span>Quy đổi <?php echo KitCmsConfig::getValue('currency_gold_name'); ?></span>
        </a>
    </li>
    <li class='<?php if ($active == 'creditcardwallet'): ?>active<?php endif; ?>'>
        <a href='<?php echo Yii::app()->createUrl('currency/CreditCardWallet'); ?>'>
            <i class='icon-credit-card'></i>
            <span>Ví đựng thẻ</span>
        </a>
    </li>
    <li class='<?php if ($active == 'config'): ?>active<?php endif; ?>'>
        <a href='<?php echo Yii::app()->createUrl('account/profile/password'); ?>'>
            <i class='icon-cog'></i>
            <span>Thiết lập tài khoản</span>
        </a>
    </li>
    <li class='<?php if ($active == 'help'): ?>active<?php endif; ?>'>
        <a href='<?php echo Yii::app()->createUrl('account/profile/password'); ?>'>
            <i class='icon-question'></i>
            <span>Hỏi đáp</span>
        </a>
    </li>
</ul>
