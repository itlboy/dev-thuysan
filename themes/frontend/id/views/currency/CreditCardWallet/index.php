<section id='content'>
    <div class='container-fluid'>
        <div class='row-fluid' id='content-wrapper'>
            <div class='span12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-credit-card'></i>
                        <span>Ví đựng thẻ</span>
                    </h1>
                    <div class='pull-right'>
                        <ul class='breadcrumb'>
                            <li>
                                <a href="<?php echo Yii::app()->getBaseUrl(true); ?>" title="Tổng quan"><i class='icon-dashboard'></i></a>
                            </li>
                            <li class='separator'><i class='icon-angle-right'></i></li>
                            <li class='active'>Ví đựng thẻ</li>
                        </ul>
                    </div>
                </div>

                <div class='row-fluid' style="margin-bottom: 10px;">
                    <div class="btn-group pull-right">
                        <a class="btn btn-success" href="javascript:void(0);">Tạo thẻ ngân hàng</a>
                    </div>
                </div>
                <div class='row-fluid'>
                    <div class='span12 box bordered-box blue-border' style='margin-bottom:0;'>
                        <div class='box-header blue-background'>
                            <div class='title'>Ví đựng thẻ của bạn</div>
                        </div>
                        <div class='box-content box-no-padding'>
                            <table class='table table-striped' style='margin-bottom:0;'>
                                <thead>
                                    <tr>
                                        <th>Loại thẻ</th>
                                        <th>Số thẻ</th>
                                        <th>Tên chủ thẻ</th>
                                        <th>Ngày hết hạn/phát hành</th>
                                        <th>Trạng thái</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Vietcombank</td>
                                        <td>XXXX-XXXX-XXXX-8815014</td>
                                        <td>LUONG VAN THANH</td>
                                        <td>06/2010</td>
                                        <td>
                                            <span class='label label-success'>Hoạt động</span>
                                        </td>
                                        <td>
                                            <div class='text-right'>
                                                <a class='btn btn-success btn-mini' href='#'>
                                                    <i class='icon-edit'></i>
                                                </a>
                                                <a class='btn btn-danger btn-mini' href='#'>
                                                    <i class='icon-remove'></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>
</section>
