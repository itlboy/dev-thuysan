<section id='content'>
    <div class='container-fluid'>
        <div class='row-fluid' id='content-wrapper'>
            <div class='span12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-exchange'></i>
                        <span>Quy đổi <?php KitCmsConfig::getValue('currency_gold_name'); ?></span>
                    </h1>
                    <div class='pull-right'>
                        <ul class='breadcrumb'>
                            <li>
                                <a href="<?php echo Yii::app()->getBaseUrl(true); ?>" title="Tổng quan"><i class='icon-dashboard'></i></a>
                            </li>
                            <li class='separator'><i class='icon-angle-right'></i></li>
                            <li class='active'>Quy đổi <?php KitCmsConfig::getValue('currency_gold_name'); ?></li>
                        </ul>
                    </div>
                </div>
                
                <?php $this->renderPartial('//_block/_msg', array('msg' => $msg)); ?>
                
                <?php foreach ($services as $service):?>
                <div class='row-fluid' onclick="window.location = '<?php echo Yii::app()->createUrl('currency/exchange/service', array('code' => $service['code'])); ?>'" style="cursor: pointer;">
                    <div class="span12 box box-bordered">
                        <div class="box-header box-header-small" style="font-size:12px;background-color:#fff;">
                            <div class='row-fluid'>
                                <div class="span2">
                                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/tmp/pham_nhan_tu_tien_300x300.jpg"/>
                                </div>
                                <div class="span6">
                                    <h4 style="text-transform: uppercase;"><?php echo $service['title']; ?></h4>
                                    <div>
                                        <ul class="unstyled">
                                            <li><i class="icon-ok text-green"></i> Tỉ lệ quy đổi: 1 <?php KitCmsConfig::getValue('currency_gold_name'); ?> = 10 Vàng</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>

            </div>
        </div>

    </div>
</section>
