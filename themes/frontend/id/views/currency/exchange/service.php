<section id='content'>
    <div class='container-fluid'>
        <div class='row-fluid' id='content-wrapper'>
            <div class='span12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-exchange'></i>
                        <span>Quy đổi <?php echo KitCmsConfig::getValue('currency_gold_name'); ?></span>
                    </h1>
                    <div class='pull-right'>
                        <ul class='breadcrumb'>
                            <li>
                                <a href="<?php echo Yii::app()->getBaseUrl(true); ?>" title="Tổng quan"><i class='icon-dashboard'></i></a>
                            </li>
                            <li class='separator'><i class='icon-angle-right'></i></li>
                            <li class='active'>Quy đổi <?php echo KitCmsConfig::getValue('currency_gold_name'); ?></li>
                        </ul>
                    </div>
                </div>
                
                <?php $this->renderPartial('//_block/_msg', array('msg' => $msg)); ?>
                
                <div class='row-fluid'>
                    <div class='span12 box'>
                        <div class='box-content box-double-padding'>
                            <?php $form = $this->beginWidget('CActiveForm', array(
                                'id'=>'formExchange',
                                'enableAjaxValidation'=>FALSE,
                                'enableClientValidation'=>true,
                                'htmlOptions' => array('name' => 'formExchange', 'class' => 'form', 'style' => 'margin-bottom: 0')
                            )); ?>
                                <fieldset>
                                    <div class='span7'>
                                        <div class='control-group'>
                                            Số <?php echo KitCmsConfig::getValue('currency_gold_name'); ?> bạn đang có: <?php echo KitAccountStats::getField(Yii::app()->user->id, 'gold_current'); ?>
                                            <hr />
                                            <label class='control-label'>Chọn số <?php echo KitCmsConfig::getValue('currency_gold_name'); ?> để chuyển sang dịch vụ: <?php echo $service['title']; ?></label>
                                            <div class='controls'>
                                                <?php $arrGold = array(10,20,50,100,200); ?>
                                                <select name="gold" class="span12">
                                                    <?php foreach ($arrGold as $gold): ?>
                                                    <option value="10"><?php echo $gold; ?> <?php echo KitCmsConfig::getValue('currency_gold_name'); ?> = <?php echo KitCurrencyService::exchangeGoldToService($gold, $service['rate']) . ' ' . $service['unit']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <button class='btn btn-primary' type="submit" name="submit">
                                                <i class='icon-exchange'></i>
                                                Chuyển đổi
                                            </button>
                                        </div>
                                    </div>
                                </fieldset>
                            <?php $this->endWidget(); ?>
                        </div>
                    </div>                    
                </div>

            </div>
        </div>

    </div>
</section>
