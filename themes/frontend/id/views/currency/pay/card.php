<section id='content'>
    <div class='container-fluid'>
        <div class='row-fluid' id='content-wrapper'>
            <div class='span12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-certificate'></i>
                        <span>Nạp <?php echo KitCmsConfig::getValue('currency_gold_name'); ?> qua thẻ</span>
                    </h1>
                    <div class='pull-right'>
                        <ul class='breadcrumb'>
                            <li>
                                <a href="<?php echo Yii::app()->getBaseUrl(true); ?>" title="Tổng quan"><i class='icon-dashboard'></i></a>
                            </li>
                            <li class='separator'><i class='icon-angle-right'></i></li>
                            <li><?php echo KitCmsConfig::getValue('currency_gold_name'); ?></li>
                            <li class='separator'><i class='icon-angle-right'></i></li>
                            <li>Nạp <?php echo KitCmsConfig::getValue('currency_gold_name'); ?></li>
                            <li class='separator'><i class='icon-angle-right'></i></li>
                            <li class='active'>Nạp <?php echo KitCmsConfig::getValue('currency_gold_name'); ?> qua thẻ</li>
                        </ul>
                    </div>
                </div>


                <?php $this->renderPartial('//_block/_msg', array('msg' => $msg)); ?>

                <div class='row-fluid'>
                    <div class='span12 box'>
                        <div class='box-content box-double-padding'>
                            <?php
                            $form = $this->beginWidget('CActiveForm', array(
                                'id' => 'payform',
                                'enableAjaxValidation' => FALSE,
                                'enableClientValidation' => true,
                                'htmlOptions' => array('name' => 'payform', 'class' => 'form', 'style' => 'margin-bottom: 0')
                            ));
                            ?>
                            <input type="hidden" name="type" value="<?php echo $type; ?>" />
                            <fieldset>
                                <div class='span7'>
                                    <div class='control-group'>
                                        <label class='control-label'>Chọn loại thẻ</label>
                                        <div class="btn-group">
                                            <button class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                                                <?php echo ucfirst($type); ?>
                                            </button>
                                            <button class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <?php foreach ($providers as $provider => $providersCode): ?>
                                                <li><a href="<?php echo Yii::app()->createUrl('currency/pay/card', array('type' => $provider)); ?>"><?php echo ucfirst($provider); ?></a></li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <hr class='hr-normal' />
                                    <div class='control-group'>
                                        <label class='control-label'>Mã số thẻ</label>
                                        <div class='controls'>
                                            <input type="text" name="card_number" placeholder="Mã số nằm trong phần giấy bạc" class="span12" />
                                        </div>
                                        <label class='control-label'>Serial</label>
                                        <div class='controls'>
                                            <input type="text" name="serial_number" placeholder="Serial của thẻ" class="span12" />
                                        </div>
                                    </div>
                                </div>
                                <div class='span5'>
                                    <div class='lead'>
                                        <i class='icon-info-sign text-contrast'></i>
                                        Hướng dẫn
                                    </div>
                                    <ul class="unstyled">
                                        <li><i class="icon-ok text-green"></i>
                                            Mật khẩu cấp 1 dùng để đăng nhập vào khu vực cá nhân và các dịch vụ khác từ Let.vn
                                        </li>
                                        <li><i class="icon-ok text-green"></i>
                                            Dùng mật khẩu cấp 2 hoặc mật khẩu cấp 1 hiện tại có thể đổi được mật khẩu cấp 1
                                        </li>
                                        <li><i class="icon-ok text-green"></i>
                                            Dùng số điện thoại đã đăng ký có thể đổi được mật khẩu cấp 1 <br />
                                            Dùng số điện thoại: <a data-toggle="tooltip" title="Bắt buộc gửi tin nhắn từ số điện thoại này thì mới được chấp nhận" href="javascript:void(0);">0988631988</a> để soạn tin: <br />
                                            <a data-toggle="tooltip" title="mk là mã tin nhắn đổi mật khẩu" href="javascript:void(0);">mk</a> <a data-toggle="tooltip" title="234564 là id của bạn" href="javascript:void(0);">234564</a> <a data-toggle="tooltip" title="Điền mật khẩu cấp 1 mới mà bạn muốn đổi" href="javascript:void(0);">{mật khẩu cấp 1}</a> gửi <a data-toggle="tooltip" title="Giá dịch vụ là 15.000đ" href="javascript:void(0);">8771</a>
                                        </li>
                                    </ul>
                                </div>
                            </fieldset>
                            <hr class='hr-normal' />
                            <div class='form-actions' style='margin-bottom: 0;'>
                                <div class='text-right'>
                                    <button class='btn btn-primary btn-large' type="submit" name="submit">
                                        <i class='icon-save'></i>
                                        Lưu
                                    </button>
                                </div>
                            </div>
<?php $this->endWidget(); ?>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</section>
