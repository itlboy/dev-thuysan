<section id='content'>
    <div class='container-fluid'>
        <div class='row-fluid' id='content-wrapper'>
            <div class='span12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-certificate'></i>
                        <span>Nạp <?php echo KitCmsConfig::getValue('currency_gold_name'); ?></span>
                    </h1>
                    <div class='pull-right'>
                        <ul class='breadcrumb'>
                            <li>
                                <a href="<?php echo Yii::app()->getBaseUrl(true); ?>" title="Tổng quan"><i class='icon-dashboard'></i></a>
                            </li>
                            <li class='separator'><i class='icon-angle-right'></i></li>
                            <li><?php echo KitCmsConfig::getValue('currency_gold_name'); ?></li>
                            <li class='separator'><i class='icon-angle-right'></i></li>
                            <li class='active'>Nạp <?php echo KitCmsConfig::getValue('currency_gold_name'); ?></li>
                        </ul>
                    </div>
                </div>
                
                <?php $this->renderPartial('//_block/_msg', array('msg' => $msg)); ?>
                
                <div class='row-fluid'>
                    <div class="span12 box box-bordered">
                        <div class="box-header box-header-small" style="font-size:12px;background-color:#fff;">
                            <div class='row-fluid'>
                                <div class="span6">
                                    <h4>NẠP QUA THẺ CÀO</h4>
                                    <div>
                                        <ul class="unstyled">
                                            <li><i class="icon-ok text-green"></i> Tiện lợi, nhiều mệnh giá khác nhau</li>
                                            <li><i class="icon-ok text-green"></i> Dễ dàng mua được ở mọi nơi</li>
                                            <li><i class="icon-ok text-green"></i> Tỉ lệ quy đổi: 100.000 VND = 100 <?php echo KitCmsConfig::getValue('currency_gold_name'); ?></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="span6">
                                    <h4>Chọn nhanh loại thẻ cào</h4>
                                    <?php
                                    $rowPerPage = 3;
                                    $padding_top = 10;
                                    $backgroundPositionNext = -60;
                                    $i = 0;
                                    ?>
                                    <div class="row-fluid">
                                        <?php foreach ($providers as $provider => $providersCode): ?>
                                        <div class="span<?php echo 12/$rowPerPage; ?> box-quick-link" style="background: #ccc;">
                                            <div style="width: 100%; margin: 0 auto;">
                                                <a href='<?php echo Yii::app()->createUrl('currency/pay/card', array('type' => $provider)); ?>'>
                                                    <div class="header">
                                                        <div class="" style="background: url('<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/pay_telco.png') center <?php echo $padding_top + ($backgroundPositionNext * $i);?>px no-repeat; width: 100%; height: 57px;">&nbsp;</div>
                                                    </div>
                                                    <div class="content"><?php echo ucfirst($provider); ?></div>
                                                </a>
                                            </div>
                                        </div>
                                    <?php if ($i % $rowPerPage == ($rowPerPage - 1)): ?>
                                    </div>
                                    <div class="row-fluid" style="margin-top: 10px;">
                                    <?php endif; ?>
                                    <?php $i++; ?>
                                    <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class='row-fluid'>
                    <div class="span12 box box-bordered">
                        <div class="box-header box-header-small" style="font-size:12px;background-color:#fff;">
                            <div class='row-fluid'>
                                <div class="span6">
                                    <h4>THẺ VISA, MASTER VÀ NGÂN HÀNG NỘI ĐỊA</h4>
                                    <div>
                                        <ul class="unstyled">
                                            <li><i class="icon-ok text-green"></i> Không mất phí thanh toán</li>
                                            <li><i class="icon-ok text-green"></i> Thanh toán nhanh gọn và có thể sử dụng dịch vụ ngay sau khi thanh toán.</li>
                                            <li><i class="icon-ok text-green"></i> Thẻ của quý khách phải được kích hoạt chức năng thanh toán trực tuyến hoặc đã đăng ký Internet Banking.</li>
                                            <li><i class="icon-ok text-green"></i> Tỉ lệ quy đổi: 100.000 VND = 103 <?php echo KitCmsConfig::getValue('currency_gold_name'); ?> (Khuyến mại 3% giá trị thanh toán).</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="span6">
                                    <?php
                                        $cs = Yii::app()->clientScript;
                                        //ad js for payment process
                                        $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/javascripts/payment.js', CClientScript::POS_END);
                                    ?>
                                    <h4>Chọn Ngân hàng</h4>
                                    
                                    <?php
                                    $rowPerPage = 4;
                                    $padding_top = 5;
                                    $backgroundPositionNext = -44;
                                    $i = 0;
                                    ?>
                                    <div class="row-fluid bankList">
                                        <?php foreach ($banks as $bankCode => $bank): ?>
                                        <div class="span<?php echo 12/$rowPerPage; ?> box-quick-link bank">
                                            <div style="width: 100%; margin: 0 auto;">
                                                <a href='javascript:void(0);' rel="<?php echo $bankCode; ?>" rev="<?php echo $bank; ?>">
                                                    <div class="header" style="height: 44px;">
                                                        <div class="" style="background: url('<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/pay_bank.png') center <?php echo $padding_top + ($backgroundPositionNext * $i);?>px no-repeat; width: 100%; height: 44px;">&nbsp;</div>
                                                    </div>
                                                    <div class="content"><?php echo ucfirst($bank); ?></div>
                                                </a>
                                            </div>
                                        </div>
                                    <?php if ($i % $rowPerPage == ($rowPerPage - 1)): ?>
                                    </div>
                                    <div class="row-fluid" style="margin-top: 10px;">
                                    <?php endif; ?>
                                    <?php $i++; ?>
                                    <?php endforeach; ?>
                                    </div>

                                    
                                    <?php $form = $this->beginWidget('CActiveForm', array(
                                        'id'=>'payForm',
                                        'action' => Yii::app()->createAbsoluteUrl('currency/pay/makeRequestOnePay'),
                                        'enableAjaxValidation'=>FALSE,
                                        'enableClientValidation'=>true,
                                        'htmlOptions' => array('name' => 'payForm', 'onsubmit' => 'return $.checkPayForm(this);', 'class' => 'form', 'style' => 'margin-bottom: 0')
                                    )); ?>
                                    
                                    <div class="banks">
                                        <div class='row-fluid'>
                                        <?php 
                                        ?>
                                            <input type="hidden" name="slectedBankId" id="slectedBankId" value="" />
                                            <input type="hidden" name="slectedBankName" id="slectedBankName" value="" />
                                        </div>
                                    </div>
                                    
                                    <div class='control-group'>
                                        <h4>Số tiền muốn nạp</h4>
                                        <div class='controls'>
                                            <?php echo CHtml::dropDownList('vpc_Amount', '', OnePay::$allowValues); ?>
                                            <span class="currencyLabel"> VND </span>
                                        </div>
                                    </div>
                                    
                                    <div class='form-actions' style='margin-bottom: 0;'>
                                        <label id="tips" class="tips" style="display: none;"></label>
                                        <div class='text-left pull-left'>
                                            <a class='btn btn-large' href="http://202.9.84.88/documents/payment/guideVN.jsp?logos=at" target="_blank" title="Hướng dẫn nạp qua thẻ ngân hàng">
                                                Hướng dẫn
                                            </a>
                                        </div>
                                        <div class='text-right pull-right'>
                                            <button class='btn btn-large btn-primary' type="submit" name="submit">
                                                Nạp qua thẻ Ngân hàng
                                            </button>
                                        </div>
                                    </div>
                                    <?php $this->endWidget(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>
</section>
