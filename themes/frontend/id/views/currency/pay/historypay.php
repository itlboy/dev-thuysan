<section id='content'>
    <div class='container-fluid'>
        <div class='row-fluid' id='content-wrapper'>
            <div class='span12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-certificate'></i>
                        <span>Lịch sử nạp tiền</span>
                    </h1>
                    <div class='pull-right'>
                        <ul class='breadcrumb'>
                            <li>
                                <a href="<?php echo Yii::app()->getBaseUrl(true); ?>" title="Tổng quan"><i class='icon-dashboard'></i></a>
                            </li>
                            <li class='separator'><i class='icon-angle-right'></i></li>
                            <li><?php echo KitCmsConfig::getValue('currency_gold_name'); ?></li>
                            <li class='separator'><i class='icon-angle-right'></i></li>
                            <li class='active'>Lịch sử nạp tiền</li>
                        </ul>
                    </div>
                </div>

                <div class='row-fluid' style="margin-bottom: 10px;">
                    <div class='pull-right'>
                        <div class='btn-group'>
                            <a href="#" class="btn btn-white hidden-phone">Last month</a>
                            <a href="#" class="btn btn-white">Last week</a>
                            <a href="#" class="btn btn-white ">Today</a>
                            <a href="#" class="btn btn-white" id="daterange"><i class='icon-calendar'></i>
                                <span class='hidden-phone'>Custom</span>
                                <b class='caret'></b>
                            </a>
                        </div>
                    </div>
                </div>
                <div class='row-fluid'>
                    <div class='span12 box bordered-box blue-border' style='margin-bottom:10px;'>
                        <div class='box-header blue-background'>
                            <div class='title'>Lịch sử nạp tiền</div>
                            <div class='actions'>
                                <a href="#" class="btn box-remove btn-mini btn-link"><i class='icon-remove'></i>
                                </a>
                                <a href="#" class="btn box-collapse btn-mini btn-link"><i></i>
                                </a>
                            </div>
                        </div>
                        <div class='box-content box-no-padding'>
                            <table class='table table-striped' style='margin-bottom:0;'>
                                <thead>
                                    <tr>
                                        <th>Thời gian</th>
                                        <th>Mã giao dịch</th>
                                        <th>Số tiền nạp</th>
                                        <th>Nhận được</th>
                                        <th>Hình thức</th>
                                        <th>Trạng thái</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($models as $model) : ?>
                                        <tr>
                                            <td><?php echo date('d/m/Y H:i', strtotime($model->time)); ?></td>
                                            <td><?php echo $model->checknum; ?></td>
                                            <td><?php echo number_format($model->money, 0, ',', '.'); ?>đ</td>
                                            <td><?php echo number_format($model->gold, 0, ',', '.'); ?> <?php echo KitCmsConfig::getValue('currency_gold_name'); ?></td>
                                            <td><?php echo $model->payment; ?></td>
                                            <td>
                                                <span class='label label-success'>Thành công</span>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class='pagination pagination-centered'>
                        <?php
                        $this->widget('CLinkPager', array(
                            'pages' => $pages,
                            'cssFile' => FALSE,
                            'htmlOptions' => array('class' => ''),
                            'firstPageLabel' => '&laquo;',
                            'prevPageLabel' => '&lt;',
                            'nextPageLabel' => '&gt;',
                            'lastPageLabel' => '&raquo;',
                            'hiddenPageCssClass' => 'disabled',
                            'selectedPageCssClass' => 'active',
                        ));
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
