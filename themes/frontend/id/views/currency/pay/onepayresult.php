<section id='content'>
    <div class='container-fluid'>
        <div class='row-fluid' id='content-wrapper'>
            <div class='span12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-certificate'></i>
                        <span>Nạp <?php echo KitCmsConfig::getValue('currency_gold_name'); ?></span>
                    </h1>
                    <div class='pull-right'>
                        <ul class='breadcrumb'>
                            <li>
                                <a href="<?php echo Yii::app()->getBaseUrl(true); ?>" title="Tổng quan"><i class='icon-dashboard'></i></a>
                            </li>
                            <li class='separator'><i class='icon-angle-right'></i></li>
                            <li><?php echo KitCmsConfig::getValue('currency_gold_name'); ?></li>
                            <li class='separator'><i class='icon-angle-right'></i></li>
                            <li class='active'>Nạp <?php echo KitCmsConfig::getValue('currency_gold_name'); ?></li>
                        </ul>
                    </div>
                </div>
                
                <?php $this->renderPartial('//_block/_msg', array('msg' => $msg)); ?>
                
            </div>
        </div>
    </div>
</section>
