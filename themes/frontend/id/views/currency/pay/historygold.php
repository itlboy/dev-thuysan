<section id='content'>
    <div class='container-fluid'>
        <div class='row-fluid' id='content-wrapper'>
            <div class='span12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-certificate'></i>
                        <span>Lịch sử quy đổi</span>
                    </h1>
                    <div class='pull-right'>
                        <ul class='breadcrumb'>
                            <li>
                                <a href="<?php echo Yii::app()->getBaseUrl(true); ?>" title="Tổng quan"><i class='icon-dashboard'></i></a>
                            </li>
                            <li class='separator'><i class='icon-angle-right'></i></li>
                            <li><?php echo KitCmsConfig::getValue('currency_gold_name'); ?></li>
                            <li class='separator'><i class='icon-angle-right'></i></li>
                            <li class='active'>Lịch sử quy đổi</li>
                        </ul>
                    </div>
                </div>

                <div class='row-fluid'>
                    <div class='span12 box bordered-box purple-border' style='margin-bottom:20px;'>
                        <div class='box-header purple-background'>
                            <div class='title'>Lịch sử quy đổi</div>
                            <div class='actions'>
                                <a href="#" class="btn box-remove btn-mini btn-link"><i class='icon-remove'></i>
                                </a>
                                <a href="#" class="btn box-collapse btn-mini btn-link"><i></i>
                                </a>
                            </div>
                        </div>
                        <div class='box-content box-no-padding'>
                            <table class='table table-striped' style='margin-bottom:0;'>
                                <thead>
                                    <tr>
                                        <th>Thời gian</th>
                                        <th>Mã giao dịch</th>
                                        <th>Nhận được</th>
                                        <th>Trạng thái</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($models as $model) : ?>
                                        
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class='pagination pagination-centered'>
                        <?php
                        $this->widget('CLinkPager', array(
                            'pages' => $pages,
                            'cssFile' => FALSE,
                            'htmlOptions' => array('class' => ''),
                            'firstPageLabel' => '&laquo;',
                            'prevPageLabel' => '&lt;',
                            'nextPageLabel' => '&gt;',
                            'lastPageLabel' => '&raquo;',
                            'hiddenPageCssClass' => 'disabled',
                            'selectedPageCssClass' => 'active',
                        ));
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
