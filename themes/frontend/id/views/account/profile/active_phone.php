<section id='content'>
    <div class='container-fluid'>
        <div class='row-fluid' id='content-wrapper'>
            <div class='span12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-lock'></i>
                        <span>Kích hoạt số điện thoại</span>
                    </h1>
                    <div class='pull-right'>
                        <ul class='breadcrumb'>
                            <li>
                                <a href="<?php echo Yii::app()->getBaseUrl(true); ?>" title="Tổng quan"><i class='icon-dashboard'></i></a>
                            </li>
                            <li class='separator'>
                                <i class='icon-angle-right'></i>
                            </li>
                            <li>Thông tin cá nhân</li>
                            <li class='separator'>
                                <i class='icon-angle-right'></i>
                            </li>
                            <li class='active'>Kích hoạt số điện thoại</li>
                        </ul>
                    </div>
                </div>
                
                <?php $this->renderPartial('//_block/_msg', array('msg' => $msg)); ?>

                <div class='row-fluid'>
                    <div class='span12 box'>
                        Bạn chưa có số điện thoại, hãy kích hoạt số điện thoại của bạn bằng cách soạn tin nhắn theo cú pháp: <strong>LET KH <?php echo $model->id2; ?></strong> gửi tới 6746<br/>
<!--                        <i>Trong đó:</i> ID2 là mật khẩu cấp 2 của bạn.<br/>-->
                        
                        <?php 
                         //Make id2
//                            $id2 = substr(md5($model->id), 0, 3);
//                            $randomStr = Common::randomString(3);
//                            $id2 = $id2.$randomStr;
                        ?>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</section>
