<section id='content'>
    <div class='container-fluid'>
        <div class='row-fluid' id='content-wrapper'>
            <div class='span12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-info-sign'></i>
                        <span>Thông tin cá nhân</span>
                    </h1>
                    <div class='pull-right'>
                        <ul class='breadcrumb'>
                            <li>
                                <a href="<?php echo Yii::app()->getBaseUrl(true); ?>" title="Tổng quan"><i class='icon-dashboard'></i></a>
                            </li>
                            <li class='separator'>
                                <i class='icon-angle-right'></i>
                            </li>
                            <li class='active'>Thông tin cá nhân</li>
                        </ul>
                    </div>
                </div>
                <div class='row-fluid'>
                    <div class='span3 box'>
                        <div id="avatar" class='box-content avatar'>
                            <?php 
                                $cs = Yii::app()->clientScript;
                                $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/javascripts/ajax_upload_file.js', CClientScript::POS_END);
                                $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/javascripts/jquery_block_ui.js', CClientScript::POS_END);
                                $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/assets/javascripts/edit_profiles.js', CClientScript::POS_END);

                                // Add colorbox
                                $cs->registerScriptFile(Yii::app()->theme->baseUrl."/assets/colorbox/jquery.colorbox.js", CClientScript::POS_END);
                                $cs->registerCssFile(Yii::app()->theme->baseUrl."/assets/colorbox/colorbox.css");
                                
                                $avatarUrl = KitAccount::getAvatarUrl('large', $model->avatar, 'http://placehold.it/230x230&amp;text=Avatar');
                            ?>
                            <img id="img-avatar" alt="<?php echo $model->username; ?>" src="<?php echo $avatarUrl; ?>" />
                            <div id="avatar-options" class='btn-group hide'>
                                <button class='btn dropdown-toggle' data-delay='300' data-hover='dropdown' data-toggle='dropdown'>
                                    Tùy chọn
                                    <span class='caret'></span>
                                </button>
                                <ul class='dropdown-menu options'>
                                    <li>
                                        <form id="frmAvatar" action="<?php echo Yii::app()->createUrl('account/profile/ajaxUploadAvatar') ; ?>">
                                            <div class="file_browse_wrapper"></div>
                                        </form>
                                    </li>
                                    <li class='divider'></li>
                                    <?php 
                                        if($model->avatar):
                                            $editAvatarAction = Yii::app()->createUrl('account/profile/editAvatar')."?size=large&fileName=".$model->avatar;
                                    ?>
                                    <li>
                                        <a href='javascript:void(0);' title="Chỉnh sửa ảnh đại diện" onclick="$.editAvatar(this);" rel="<?php echo $editAvatarAction; ?>">
                                            <i class="icon-crop"></i><span> Thay đổi vùng hiển thị<span>
                                        </a>
                                    </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class='span9 box'>
                        <div class='box-content box-double-padding'>
                            
                            <?php $this->renderPartial('//_block/_msg', array('msg' => $msg)); ?>
                            
                            <?php $form = $this->beginWidget('CActiveForm', array(
                                'id'=>'editProfile',
                                'enableAjaxValidation'=>FALSE,
                                'enableClientValidation'=>true,
                                'focus'=>array($model,'username'),
                                'htmlOptions' => array('name' => 'editProfile', 'class' => 'form', 'style' => 'margin-bottom: 0')
                            )); ?>
                                <fieldset>
                                    <div class='span4'>
                                        <div class='lead'>
                                            <i class='icon-signin text-contrast'></i>
                                            Thông tin tài khoản
                                        </div>
                                        <small class='muted'>Đây là những thông tin quan trọng liên quan trực tiếp đến tài khoản của bạn. Hãy nhập chính xác nếu muốn thay đổi !</small>
                                    </div>
                                    <div class='span7 offset1'>
                                        <div class='control-group'>
                                            <label class='control-label'>Tên truy cập</label>
                                            <div class='controls'>
                                                <?php echo $form->textField($model, 'username', array('class' => 'span12', 'placeholder' => $model->username, 'disabled' => 'disabled')); ?>
                                            </div>
                                        </div>
                                        <div class='control-group'>
                                            <label class='control-label'>E-mail (<a href="<?php echo Yii::app()->createUrl('account/profile/changeEmail'); ?>">Thay đổi</a>)</label>
                                            <!--<label class='control-label' style="font-size: 11px; font-style: italic;">Bạn đã thay đổi email nhưng chưa xác nhận - Gửi lại mã xác nhận vào email</label>-->
                                            <div class='controls'>
                                                <?php echo $form->textField($model, 'email', array('class' => 'span12', 'placeholder' => $model->email, 'disabled' => 'disabled')); ?>
                                            </div>
                                        </div>
                                        <div class='control-group'>
                                            <label class='control-label'>Số điện thoại (<a href="<?php echo Yii::app()->createUrl('account/profile/changePhone'); ?>">Thay đổi</a>)</label>
                                            <div class='controls'>
                                                <?php echo $form->textField($model, 'phone', array('class' => 'span12', 'placeholder' => $model->phone, 'disabled' => 'disabled')); ?>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <hr class='hr-normal' />
                                <fieldset>
                                    <div class='span4 box'>
                                        <div class='lead'>
                                            <i class='icon-barcode text-contrast'></i>
                                            Thông tin xác thực
                                        </div>
                                        <small class='muted'>Đây là những thông tin rất quan trọng, nó giúp bạn lấy lại tài khoản dễ dàng khi bị đánh cắp. Thông tin này chỉ có thể thay đổi 1 lần!</small>
                                    </div>
                                    <div class='span7 offset1'>
                                        <div class='control-group'>
                                            <label class='control-label'>Số chứng minh nhân dân</label>
                                            <div class='controls'>
                                                <?php 
                                                    if(!empty($model->idcard))
                                                        echo $form->textField($model, 'idcard', array('class' => 'span12', 'placeholder' => $model->idcard, 'disabled' => 'disabled')); 
                                                    else
                                                        echo $form->textField($model, 'idcard', array('class' => 'span12 numbersOnly', 'placeholder' => 'Nhập số CMND')); 
                                                ?>
                                            </div>
                                        </div>
                                        <div class='control-group'>
                                            <label class='control-label'>Ngày cấp</label>
                                            <div class='controls'>
                                                <div class="input-append datepicker">
                                                    <?php 
                                                        if(!empty($model->birthprovince))
                                                            echo $form->textField($model, 'birthprovince', array('class' => 'span12', 'disabled' => 'disabled', 'data-format' => 'dd/MM/yyyy','placeholder' => $model->birthprovince));  
                                                        else
                                                            echo $form->textField($model, 'birthprovince', array('class' => 'span12', 'data-format' => 'dd/MM/yyyy','placeholder' => 'Nhập ngày cấp CMND'));  
                                                    ?>
                                                    <span class="add-on"><i class="icon-th"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class='control-group'>
                                            <label class='control-label'>Nơi cấp</label>
                                            <div class='controls'>
                                                <?php 
                                                    if(!empty($model->resideprovince))
                                                        echo $form->textField($model, 'resideprovince', array('disabled' => 'disabled', 'class' => 'span12', 'placeholder' => $model->resideprovince));  
                                                    else
                                                        echo $form->textField($model, 'resideprovince', array('class' => 'span12', 'placeholder' => 'Nhập nơi cấp CMND'));  
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <hr class='hr-normal' />
                                <fieldset>
                                    <div class='span4 box'>
                                        <div class='lead'>
                                            <i class='icon-user text-contrast'></i>
                                            Thông tin cá nhân
                                        </div>
                                        <small class='muted'>Đây là những thông tin cá nhân. Nhập đầy đủ là cách bạn có thể mở rộng quan hệ bạn bè.</small>
                                    </div>
                                    <div class='span7 offset1'>
                                        <div class='control-group'>
                                            <label class='control-label'>Họ và tên</label>
                                            <div class='controls'>
                                                <?php echo $form->textField($model, 'fullname', array('class' => 'span12', 'placeholder' => 'Nhập tên thật của bạn')); ?>
                                            </div>
                                        </div>
                                        <div class='control-group'>
                                            <label class='control-label'>Tên hiển thị</label>
                                            <div class='controls'>
                                                <?php echo $form->textField($model, 'displayname', array('class' => 'span12', 'placeholder' => 'Tên mà người khác nhìn thấy bạn')); ?>
                                            </div>
                                        </div>
                                        <div class='control-group'>
                                            <label class='control-label'>Sinh nhật</label>
                                            <div class='controls'>
                                                <div class="input-append datepicker">
                                                    <?php echo $form->textField($model, 'birthday', array('class' => 'span12', 'placeholder' => 'Chọn ngày tháng', 'data-format' => 'dd/MM/yyyy')); ?>
                                                    <span class="add-on"><i class="icon-th"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class='control-group'>
                                            <label class='control-label'>Địa chỉ</label>
                                            <div class='controls'>
                                                <?php echo $form->textField($model, 'address', array('class' => 'span12', 'placeholder' => 'Nhập địa chỉ của bạn')); ?>
                                            </div>
                                        </div>
                                        <div class='control-group'>
                                            <label class='control-label'>Nick Yahoo</label>
                                            <div class='controls'>
                                                <?php echo $form->textField($model, 'yahoo', array('class' => 'span12', 'placeholder' => 'Nhập Nick yahoo')); ?>
                                            </div>
                                        </div>
<!--                                        <div class='control-group'>
                                            <label class='control-label'>Chữ ký</label>
                                            <div class='controls'>
                                                <textarea class='autosize span12' id='bio' placeholder='Nhập chữ ký'></textarea>
                                            </div>
                                        </div>-->
                                    </div>
                                </fieldset>
                                <div class='form-actions' style='margin-bottom: 0;'>
                                    <div class='text-right'>
                                        <button class='btn btn-primary btn-large' type="submit" name="submit">
                                            <i class='icon-save'></i>
                                            Lưu
                                        </button>
                                    </div>
                                </div>
                            <?php $this->endWidget(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
