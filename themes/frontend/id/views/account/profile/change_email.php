<section id='content'>
    <div class='container-fluid'>
        <div class='row-fluid' id='content-wrapper'>
            <div class='span12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-envelope'></i>
                        <span>Thay đổi Email</span>
                    </h1>
                    <div class='pull-right'>
                        <ul class='breadcrumb'>
                            <li>
                                <a href="<?php echo Yii::app()->getBaseUrl(true); ?>" title="Tổng quan"><i class='icon-dashboard'></i></a>
                            </li>
                            <li class='separator'>
                                <i class='icon-angle-right'></i>
                            </li>
                            <li>Thông tin cá nhân</li>
                            <li class='separator'>
                                <i class='icon-angle-right'></i>
                            </li>
                            <li class='active'>Thay đổi email</li>
                        </ul>
                    </div>
                </div>
                
                <?php $this->renderPartial('//_block/_msg', array('msg' => $msg)); ?>

                <div class='row-fluid'>
                    <div class='span12 box'>
                        <div class='box-content box-double-padding'>
                            <script type="text/javascript">
                            $(document).ready(function() {
                                // Tooltip
                                $('a[data-toggle="tooltip"]').tooltip();
                            });
                            </script>
                            <?php $form = $this->beginWidget('CActiveForm', array(
                                'id'=>'editPassword',
                                'enableAjaxValidation'=>FALSE,
                                'enableClientValidation'=>true,
                                'focus'=>array($model, 'email2'),
                                'htmlOptions' => array('name' => 'changeEmail', 'class' => 'form', 'style' => 'margin-bottom: 0')
                            )); ?>
                                <input type="hidden" name="formality" id="formality" value="password1" />
                                <fieldset>
                                    <div class='span7'>
                                        <div class='control-group' id="email1">
                                            <label class='control-label'>Email hiện tại</label>
                                            <div class='controls'>
                                                <?php echo $form->textField($model, 'email', array('class' => 'span12', 'placeholder' => $model->email, 'disabled' => 'disabled')); ?>
                                            </div>
                                        </div>
                                        <div class='control-group' id="email2">
                                            <label class='control-label'>Email thay thế</label>
                                            <div class='controls'>
                                                <?php echo $form->textField($model, 'email2', array('autocomplete'=>'off', 'class' => 'span12', 'placeholder' => "Nhập vào email thay thế")); ?>
                                            </div>
                                        </div>
                                        <div class='control-group' id="email2">
                                            <label class='control-label'>Mật khẩu cấp 2</label>
                                            <div class='controls'>
                                                <?php echo $form->passwordField($model, 'password2', array('autocomplete'=>'off', 'class' => 'span12', 'placeholder' => "Nhập vào mật khẩu cấp 2")); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='span5'>
                                        <div class='lead'>
                                            <i class='icon-info-sign text-contrast'></i>
                                            Hướng dẫn
                                        </div>
                                        <ul class="unstyled">
                                            <li><i class="icon-ok text-green"></i>
                                                Bước 1: Nhập vào email bạn muốn thay thế.
                                            </li>
                                            <li><i class="icon-ok text-green"></i>
                                                Bước 2: Nhập mật khẩu cấp 2 của bạn.
                                            </li>
                                            <li><i class="icon-ok text-green"></i>
                                                Bước 3: Click chuột vào nút "Lưu" sau đó tiến hành kiểm tra hòm thư của email cũ & email mới thay thế để xác nhận.
                                            </li>
                                        </ul>
                                    </div>
                                </fieldset>
                                <hr class='hr-normal' />
                                <div class='form-actions' style='margin-bottom: 0;'>
                                    <div class='text-right'>
                                        <button class='btn btn-primary btn-large' type="submit" name="submit">
                                            <i class='icon-save'></i>
                                            Lưu
                                        </button>
                                    </div>
                                </div>
                            <?php $this->endWidget(); ?>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</section>
