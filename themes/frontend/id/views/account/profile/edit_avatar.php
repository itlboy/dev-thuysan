<?php
$cs = Yii::app()->clientScript;
$cs->registerCssFile(Yii::app()->theme->baseUrl."/assets/stylesheets/edit_image.css");

$cropAreaW = 2*$image['sizeW'];
$cropAreaH = 2*$image['sizeH'];
$selector = array(
        'centered' => true,
        'borderColor' => '#FF0000',
        'borderColorHover' => '#FDB314',
        'infoFontSize'=>11,
        'infoFontColor'=>'red',
        'showPositionsOnDrag'=>true,
        'showDimetionsOnDrag'=>true,
        'w'=> $image['sizeW'],
        'h'=> $image['sizeH'],
        'aspectRatio' => true,
    	'maxWidth'=>$image['sizeW'],
		'maxHeight'=>$image['sizeH'],
);

list($width, $height) = @getimagesize($image['src']);
$this->widget('application.widgets.cropzoom.JCropZoom', array(
            'id' => 'cropZoom1',
            'cropTriggerId'=> 'btnCrop',
			'options'=>array(
                'bgColor' => '#ABABAB',
                'border'=>'2px solid #F17830',
                'marginLeft'=>'auto',
                'width'=> $cropAreaW,
                'height'=> $cropAreaH,
                'zoomSteps'=>0,
				'selector'=>$selector,
                'expose' => array(
                    'slidersOrientation' => 'horizontal', //vertical | horizontal
                    'zoomElement' => '#zoom',
                    'rotationElement' => '#rot',
                    'elementMovement' => '#movement' ,
                ),
			),
			'containerId'=>'crop_container',
			'callbackUrl' => $this->createUrl('profile/handleCropZoom')."?imageSize=".$image['size']."&fileName=".$image['fileName'],
            'onServerHandled' => 'js:function(result){
				parent.$.afterCroped(result);
             }',
            'image'=>array(
				'source'=>$image['src'],
				'width' =>$width,
				'height' => $height,
                'minZoom' =>1,
                'startZoom'=>30,
                'maxZoom' =>500,
                'useStartZoomAsMinZoom' => false,
                'snapToContainer' => true,
                'centered' => true,
			   )
		  ));
?>

<div id="imageEditAread" class="clearfix">
    <div id="crop_container"></div>
    <div class="clearfix"></div>
    <div id="crop-buttons" >
        <a id="btnCrop" class="" onclick="$(this).addClass('loading');" href="javascript:void(0);" >Cắt ảnh</a>
        <a id="restore-image" class="" href="javascript:void(0);" onclick="cropZoom1.restore();">Khôi phục</a>
        <a id="btn-cancle" class="" href="javascript:void(0);" onclick="parent.$.colorbox.close();">Bỏ qua</a>
    </div>
    <div class="clearfix"></div>
    <div id="ultilities">
        <div id="sliders">
            <div id="zoom" class="clearfix"></div>
            <div id="rot"></div>
        </div>
        <div id="movement"></div>
    </div>
</div>