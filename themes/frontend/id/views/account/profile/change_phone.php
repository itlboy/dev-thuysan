<section id='content'>
    <div class='container-fluid'>
        <div class='row-fluid' id='content-wrapper'>
            <div class='span12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-mobile-phone'></i>
                        <span>Thay đổi số điện thoại</span>
                    </h1>
                    <div class='pull-right'>
                        <ul class='breadcrumb'>
                            <li>
                                <a href="<?php echo Yii::app()->getBaseUrl(true); ?>" title="Tổng quan"><i class='icon-dashboard'></i></a>
                            </li>
                            <li class='separator'>
                                <i class='icon-angle-right'></i>
                            </li>
                            <li>Thông tin cá nhân</li>
                            <li class='separator'>
                                <i class='icon-angle-right'></i>
                            </li>
                            <li class='active'>Thay đổi số điện thoại</li>
                        </ul>
                    </div>
                </div>
                
                <?php $this->renderPartial('//_block/_msg', array('msg' => $msg)); ?>

                <div class='row-fluid'>
                    <div class='span12 box'>
                        <div class='box-content box-double-padding'>
                            <script type="text/javascript">
                            $(document).ready(function() {
                                // Tooltip
                                $('a[data-toggle="tooltip"]').tooltip();
                            });
                            </script>
                            <?php $form = $this->beginWidget('CActiveForm', array(
                                'id'=>'editPassword',
                                'enableAjaxValidation'=>FALSE,
                                'enableClientValidation'=>true,
                                'focus'=>array($model, 'phone2'),
                                'htmlOptions' => array('name' => 'changePhone', 'class' => 'form', 'style' => 'margin-bottom: 0')
                            )); ?>
                                <input type="hidden" name="formality" id="formality" value="password1" />
                                <fieldset>
                                    <div class='span7'>
                                        <div class='control-group' id="phone1">
                                            <label class='control-label'>Số điện thoại hiện tại</label>
                                            <div class='controls'>
                                                <?php echo $form->textField($model, 'phone', array('class' => 'span12', 'placeholder' => $model->phone, 'disabled' => 'disabled')); ?>
                                            </div>
                                        </div>
                                        <div class='control-group' id="phone2">
                                            <label class='control-label'>Số điện thoại thay thế</label>
                                            <div class='controls'>
                                                <?php echo $form->textField($model, 'phone2', array('autocomplete'=>'off', 'class' => 'span12', 'placeholder' => "Nhập vào số điện thoại thay thế")); ?>
                                            </div>
                                        </div>
                                        <div class='control-group' id="phone2">
                                            <label class='control-label'>Mã OTP</label>
                                            <div class='controls'>
                                                <?php echo $form->textField($model, 'otp_code', array('class' => 'span12', 'placeholder' => "Nhập vào mã OTP")); ?>
                                            </div>
                                        </div>
                                        <div class='control-group' id="email2">
                                            <label class='control-label'>Mật khẩu cấp 2</label>
                                            <div class='controls'>
                                                <?php echo $form->passwordField($model, 'password2', array('autocomplete'=>'off', 'class' => 'span12', 'placeholder' => "Nhập vào mật khẩu cấp 2")); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='span5'>
                                        <div class='lead'>
                                            <i class='icon-info-sign text-contrast'></i>
                                            Hướng dẫn
                                        </div>
                                        <ul class="unstyled">
                                            <li><i class="icon-ok text-green"></i>
                                                Bước 1: Nhập vào số điện thoại bạn muốn thay thế.
                                            </li>
                                            <li><i class="icon-ok text-green"></i>
                                                Bước 2: Gửi tin nhắn theo cú pháp <strong>LET OTP</strong> gửi tới 6746. Sau đó nhập mã OTP nhận được vào phần Mã OTP.
                                            </li>
                                            <li><i class="icon-ok text-green"></i>
                                                Bước 3: Nhập vào mật khẩu cấp 2.
                                            </li>
                                            <li><i class="icon-ok text-green"></i>
                                                Bước 4: Click chuột vào nút "Lưu" để lưu lại thông tin.
                                            </li>
                                        </ul>
                                    </div>
                                </fieldset>
                                <hr class='hr-normal' />
                                <div class='form-actions' style='margin-bottom: 0;'>
                                    <div class='text-right'>
                                        <button class='btn btn-primary btn-large' type="submit" name="submit">
                                            <i class='icon-save'></i>
                                            Lưu
                                        </button>
                                    </div>
                                </div>
                            <?php $this->endWidget(); ?>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</section>
