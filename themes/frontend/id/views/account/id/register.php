<body class='contrast-grass-green sign-up contrast-background'>
    <div id='wrapper'>
        <div class='application'>
            <div class='application-content'>
                <a href="<?php echo Yii::app()->getBaseUrl(true); ?>"><div class='icon-heart'></div>
                    <span>LetID</span>
                </a>
            </div>
        </div>
        <div class='controls'>
            <div class='caret'></div>
            <div class='form-wrapper'>
                <h1 class='text-center'>Đăng ký</h1>

                <?php $form = $this->beginWidget('CActiveForm', array(
                    'enableAjaxValidation' => false,
                    'focus' => array($model, 'email')
                )); ?>
                <?php //echo $form->errorSummary($model); ?>
                <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>

                <?php if (Yii::app()->user->hasFlash('success')) : ?>                    
                <div class='row-fluid' id="message">                        
                    <div class="alert alert-info"><div><i class="icon-chevron-right"></i> <?php echo Yii::app()->user->getFlash('success'); ?></div></div>
                    <?php header('refresh:3; url=' . Yii::app()->createUrl('//account/frontend/id/success')); ?>
                </div>
                <?php endif; ?>

                <?php if (!empty($model->errors)) : ?>                    
                <div class='row-fluid' id="message">                        
                    <div class="alert alert-error">
                        <div><?php echo $form->errorSummary($model); ?></div>
                    </div>
                </div>
                <?php endif; ?>

                <div class='row-fluid'>
                    <div class='span12 icon-over-input'>
                        <?php echo $form->textField($model, 'username', array('class' => 'span12', 'placeholder' => $model->getAttributeLabel('username'))); ?>
                        <?php // echo $form->error($model, 'username'); ?>
                        <i class='icon-user muted'></i>
                    </div>
                </div>
                <div class='row-fluid'>
                    <div class='span12 icon-over-input'>
                        <?php echo $form->textField($model, 'email', array('class' => 'span12', 'placeholder' => $model->getAttributeLabel('email'))); ?>
                        <?php // echo $form->error($model, 'email'); ?>
                        <i class='icon-user muted'></i>
                    </div>
                </div>
                <div class='row-fluid'>
                    <div class='span12 icon-over-input'>
                        <?php echo $form->passwordField($model, 'password', array('class' => 'span12', 'placeholder' => $model->getAttributeLabel('password'))); ?>
                        <?php // echo $form->error($model, 'password'); ?>
                        <i class='icon-lock muted'></i>
                    </div>
                </div>
                <div class='row-fluid'>
                    <div class='span12 icon-over-input'>
                        <?php echo $form->passwordField($model, 'password_repeat', array('class' => 'span12', 'placeholder' => $model->getAttributeLabel('password_repeat'))); ?>
                        <?php // echo $form->error($model, 'password_repeat'); ?>
                        <i class='icon-lock muted'></i>
                    </div>
                </div>

                <?php if (CCaptcha::checkRequirements()): ?>
                <div class='row-fluid'>
                    <div class='span12 icon-over-input'>
                        <?php echo CHtml::activeTextField($model, 'verifyCode', array('placeholder' => 'Nhập mã xác nhận bên cạnh')); ?>
                        <?php $this->widget('CCaptcha', array('clickableImage' => true, 'showRefreshButton' => false)); ?>
                        <?php // echo $form->error($model, 'verifyCode'); ?>
                    </div>
                </div>
                <?php endif; ?>

                <div class='row-fluid'>
                    <div class='span12 icon-over-input'>
                        <label class="checkbox" for="agreement">
                            <?php echo $form->checkBox($model, 'agreement'); ?>
                            Tôi đồng ý với
                            <a href="#" class="text-contrast">điều khoản sử dụng</a>
                        </label>
                        <?php // echo $form->error($model, 'agreement'); ?>
                    </div>
                </div>

                <button class="btn btn-block" name="button" type="submit">Đăng ký</button>
                <?php $this->endWidget(); ?>

                <div class='text-center'>
                    <hr class='hr-normal' />
                    <a href="<?php echo Yii::app()->homeUrl; ?>"><i class='icon-chevron-left'></i>
                        Bạn có tài khoản rồi? Hãy đăng nhập!
                    </a>
                </div>
            </div>
        </div>
        <div class='login-action text-center'>
            <a href="forgot_password.php"><i class='icon-lock'></i>
                Quên mật khẩu?
            </a>
        </div>
    </div>
</body>
