<script type="text/javascript">
    function login (loginButton) {
        $(loginButton).addClass('disabled').html('Đang tải...');
        var email = $('#email').val();
        var password = $('#password').val();
        $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createUrl('account/id/login') ?>',
            dataType: 'json',
            data: {email: email, password: password}
        }).done(function(msg) {
            if (msg.result == 'success') {
                location.reload();                
            } else {
                $(loginButton).removeClass('disabled').html('Đăng nhập');
                $('#message').show().html('<div class="alert alert-error"><div><i class="icon-chevron-right"></i> ' + msg.message + '</div></div>');
            }

        });
    }
</script>
<body class='contrast-grass-green sign-in contrast-background'>
    <div id='wrapper'>
        <div class='application'>
            <div class='application-content'>
                <a href="<?php echo Yii::app()->getBaseUrl(true); ?>"><div class='icon-heart'></div>
                    <span>LetID</span>
                </a>
            </div>
        </div>
        
        <div class='controls'>
            <div class='caret'></div>
            <div class='form-wrapper'>
                <h1 class='text-center'>Đăng nhập</h1>
                
                <?php $form = $this->beginWidget('CActiveForm', array(
                    'enableAjaxValidation' => false,
                    'focus' => array($model, 'email')
                )); ?>
                
                <div class='row-fluid' id="message" style="display: none;"></div>
                
                <div class='row-fluid'>
                    <div class='span12 icon-over-input'>
                        <?php echo $form->textField($model, 'email', array('id' => 'email', 'class' => 'span12', 'placeholder' => 'Tên truy cập hoặc Email')); ?>
                        <?php echo $form->error($model, 'email'); ?>
                        <i class='icon-user muted'></i>        
                    </div>
                </div>
                <div class='row-fluid'>
                    <div class='span12 icon-over-input'>
                        <?php echo $form->passwordField($model, 'password', array('id' => 'password', 'class' => 'span12', 'placeholder' => $model->getAttributeLabel('password'))); ?>
                        <?php echo $form->error($model, 'password'); ?>
                        <i class='icon-lock muted'></i>
                    </div>
                </div>
                <label class="checkbox" for="remember_me">
                    <?php echo $form->checkbox($model, 'rememberMe', array('id' => 'remember_me')); ?>
                    Ghi nhớ đăng nhập
                </label>
                
                <?php if ($model->message != NULL) : ?>
                <div class='row-fluid'>
                    <?php echo $model->message; ?>
                </div>
                <?php endif;?>
                
                <button class="btn btn-block" name="button" type="button" onclick="login(this);">Đăng nhập</button>
                <?php $this->endWidget(); ?>
                
                <?php if (isset($_GET['fb'])) : ?>
                <div class='text-center'>
                    <hr class='hr-normal' />
                    <?php $this->widget('ext.hoauth.widgets.HOAuth', array(
                        'route' => '//account/frontend/oauth'
                    )); ?>
                </div>
                <?php endif; ?>
                
                <div class='text-center'>
                    <hr class='hr-normal' />
                    <a href="forgot_password.php">Quên mật khẩu?</a>
                </div>
            </div>
        </div>
        <div class='login-action text-center'>
            <a href="<?php echo $this->createUrl('//account/frontend/id/register'); ?>"><i class='icon-user'></i>
                Bạn chưa có tài khoản LetID? Hãy <strong>Đăng ký</strong> ngay bây giờ!
            </a>
        </div>        
    </div>
</body>
