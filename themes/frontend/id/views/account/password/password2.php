<section id='content'>
    <div class='container-fluid'>
        <div class='row-fluid' id='content-wrapper'>
            <div class='span12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-lock'></i>
                        <span>Đổi mật khẩu cấp 2</span>
                    </h1>
                    <div class='pull-right'>
                        <ul class='breadcrumb'>
                            <li>
                                <a href="<?php echo Yii::app()->getBaseUrl(true); ?>" title="Tổng quan"><i class='icon-dashboard'></i></a>
                            </li>
                            <li class='separator'>
                                <i class='icon-angle-right'></i>
                            </li>
                            <li>Quản lý mật khẩu</li>
                            <li class='separator'>
                                <i class='icon-angle-right'></i>
                            </li>
                            <li class='active'>Đổi mật khẩu cấp 2</li>
                        </ul>
                    </div>
                </div>
                
                <?php $this->renderPartial('//_block/_msg', array('msg' => $msg)); ?>
                
                <div class='row-fluid'>
                    <div class='span12 box'>
                        <div class='box-content box-double-padding'>
                            <script type="text/javascript">
                            $(document).ready(function() {
                                // Tooltip
                                $('a[data-toggle="tooltip"]').tooltip();
                            });
                            </script>
                            <?php $form = $this->beginWidget('CActiveForm', array(
                                'id'=>'changePassword2',
                                'enableAjaxValidation'=>FALSE,
                                'enableClientValidation'=>TRUE,
                                'focus'=>array($model, 'password2'),
                                'htmlOptions' => array('name' => 'changePassword2', 'class' => 'form', 'style' => 'margin-bottom: 0')
                            )); ?>
                                <fieldset>
                                    <div class='span7'>
                                        <div class='control-group'>
                                            <label class='control-label'>Mật khẩu cấp 2 hiện tại</label>
                                            <div class='controls'>
                                                <?php echo $form->passwordField($model, 'password2', array('autocomplete'=>'off', 'class' => 'span12', 'placeholder' => "Nhập mật khẩu cấp 2 hiện tại.")); ?>
                                            </div>
                                        </div>
                                        <div class='control-group'>
                                            <label class='control-label'>Mật khẩu mới</label>
                                            <div class='controls'>
                                                <?php echo $form->passwordField($model, 'new_password2', array('autocomplete'=>'off', 'class' => 'span12', 'placeholder' => "Nhập mật khẩu cấp 2 mới.")); ?>
                                            </div>
                                            <label class='control-label'>Xác nhận mật khẩu mới</label>
                                            <div class='controls'>
                                                <?php echo $form->passwordField($model, 'new_password2_confirm', array('autocomplete'=>'off', 'class' => 'span12', 'placeholder' => "Nhập lại mật khẩu cấp 2 mới.")); ?>
                                            </div>
                                        </div>
                                        <hr class='hr-normal' />
                                        <div class='control-group'>
                                            <script type="text/javascript">
                                                $.ajaxSendConfirm =  function(obj){
                                                    var $url = $(obj).attr('rel');
                                                    if($url.length > 0){
                                                        $.ajax({
                                                            type: 'post',
                                                            url: $url,
                                                            data: {name:'letid'},
                                                            beforeSend:  function() {
                                                                $('#statusSendMail').html('<i class="icon-2x icon-spinner icon-spin"></i>');
                                                            },
                                                            success: function(resultText){
                                                                var $result = $.parseJSON(resultText)
                                                                if($result.success){
                                                                    //$('#statusSendMail').html($result.message);
                                                                    bootbox.dialog($result.message, [
                                                                        {
                                                                            label: "Ok",
                                                                            "class": "btn-danger"
                                                                        }
                                                                    ]);
                                                                    $('#statusSendMail').html('');
                                                                }else{
                                                                    bootbox.dialog($result.message, [
                                                                        {
                                                                            label: "Ok",
                                                                            "class": "btn-danger"
                                                                        }
                                                                    ]);
                                                                    $('#statusSendMail').html('');
                                                                }
                                                            }
                                                        });
                                                    }
                                                }
                                            </script>
                                            <?php $ajaxSendMailUrl = Yii::app()->createUrl('account/password/ajaxSendMail');?>
                                            <label class='control-label'>Mã xác nhận Email ( <a data-toggle="tooltip" title="Click chuột vào đây để hệ thống gửi mã xác nhận vào email của bạn." href="javascript:void(0);" onclick="$.ajaxSendConfirm(this);" rel="<?php echo $ajaxSendMailUrl; ?>" style="text-decoration: underline;"> Gửi mã xác nhận vào Email cho tôi </a> ) <span id="statusSendMail"></span></label>
                                            <div class='controls'>
                                                <?php echo $form->textField($model, 'email_code', array('class' => 'span12', 'placeholder' => "Nhập mã xác nhận đã gửi trong email của bạn vào đây.")); ?>
                                            </div>
                                        </div>
                                        <hr class='hr-normal' />
                                        <div class='control-group'>
                                            <label class='control-label'>Mã xác nhận điện thoại - OTP</label>
                                            <div class='controls'>
                                                <?php echo $form->textField($model, 'otp_code', array('class' => 'span12', 'placeholder' => "Nhập mã OTP đã gửi trong điện thoại của bạn vào đây.")); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='span5'>
                                        <div class='lead'>
                                            <i class='icon-info-sign text-contrast'></i>
                                            Hướng dẫn
                                        </div>
                                        <ul class="unstyled">
                                            <li><i class="icon-ok text-green"></i>
                                                Bước 1: Nhập vào mật khẩu cấp 2 hiện tại của bạn
                                            </li>
                                            <li><i class="icon-ok text-green"></i>
                                                Bước 2: Nhập vào mật khẩu cấp 2 mới của bạn
                                            </li>
                                            <li><i class="icon-ok text-green"></i>
                                                Bước 3: Nhập lại mật khẩu cấp 2 mới để xác nhận
                                            </li>
                                            <li><i class="icon-ok text-green"></i>
                                                Bước 4: Click chuột vào "Gửi mã xác nhận vào Email cho tôi" sau đó kiểm tra hòm thư để lấy mã xác nhận Email.
                                            </li>
                                            <li><i class="icon-ok text-green"></i>
                                                Bước 5: Soạn tin nhắn từ số điện thoại <a data-toggle="tooltip" title="Bắt buộc gửi tin nhắn từ số điện thoại này thì mới được chấp nhận" href="javascript:void(0);"> <?php echo $model->phone; ?> </a> theo cú pháp: <strong>LET OTP</strong> gửi <a data-toggle="tooltip" title="Giá dịch vụ là 15.000đ" href="javascript:void(0);">6746</a> để nhận mã OTP.
                                            </li>
                                        </ul>
                                    </div>
                                </fieldset>
                                <hr class='hr-normal' />
                                <div class='form-actions' style='margin-bottom: 0;'>
                                    <div class='text-right'>
                                        <button class='btn btn-primary btn-large' type="submit" name="submit">
                                            <i class='icon-save'></i>
                                            Lưu
                                        </button>
                                    </div>
                                </div>
                            <?php $this->endWidget(); ?>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</section>
