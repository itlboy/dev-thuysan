<section id='content'>
    <div class='container-fluid'>
        <div class='row-fluid' id='content-wrapper'>
            <div class='span12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-lock'></i>
                        <span>Tạo mật khẩu cấp 2</span>
                    </h1>
                    <div class='pull-right'>
                        <ul class='breadcrumb'>
                            <li>
                                <a href="<?php echo Yii::app()->getBaseUrl(true); ?>" title="Tổng quan"><i class='icon-dashboard'></i></a>
                            </li>
                            <li class='separator'>
                                <i class='icon-angle-right'></i>
                            </li>
                            <li>Quản lý mật khẩu</li>
                            <li class='separator'>
                                <i class='icon-angle-right'></i>
                            </li>
                            <li class='active'>Tạo mật khẩu cấp 2</li>
                        </ul>
                    </div>
                </div>
                
                <?php $this->renderPartial('//_block/_msg', array('msg' => $msg)); ?>
                
                <div class='row-fluid'>
                    <div class='span12 box'>
                        <div class='box-content box-double-padding'>
                            <script type="text/javascript">
                            $(document).ready(function() {
                                // Tooltip
                                $('a[data-toggle="tooltip"]').tooltip();
                            });
                            </script>
                            <?php $form = $this->beginWidget('CActiveForm', array(
                                'id'=>'makePassword2',
                                'enableAjaxValidation'=>FALSE,
                                'enableClientValidation'=>TRUE,
                                'focus'=>array($model, 'password2'),
                                'htmlOptions' => array('name' => 'makePassword2', 'class' => 'form', 'style' => 'margin-bottom: 0')
                            )); ?>
                                <fieldset>
                                    <div class='span7'>
                                        <div class='control-group'>
                                            <label class='control-label'>Mật khẩu cấp 2</label>
                                            <div class='controls'>
                                                <?php echo $form->passwordField($model, 'password2', array('autocomplete'=>'off', 'class' => 'span12', 'placeholder' => "Nhập mật khẩu cấp 2.")); ?>
                                            </div>
                                            <label class='control-label'>Xác nhận mật khẩu cấp 2</label>
                                            <div class='controls'>
                                                <?php echo $form->passwordField($model, 'password2_confirm', array('autocomplete'=>'off', 'class' => 'span12', 'placeholder' => "Nhập lại mật khẩu cấp 2.")); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='span5'>
                                        <div class='lead'>
                                            <i class='icon-info-sign text-contrast'></i>
                                            Hướng dẫn
                                        </div>
                                        <ul class="unstyled">
                                            <li><i class="icon-ok text-green"></i>
                                                Bước 1: Nhập vào mật khẩu cấp 2
                                            </li>
                                            <li><i class="icon-ok text-green"></i>
                                                Bước 2: Nhập lại mật khẩu cấp 2 để xác nhận.
                                            </li>
                                            <li><i class="icon-ok text-green"></i>
                                                Bước 3: Click chuột vào nút "Lưu" để lưu lại mật khẩu cấp 2 của bạn.
                                            </li>
                                        </ul>
                                    </div>
                                </fieldset>
                                <hr class='hr-normal' />
                                <div class='form-actions' style='margin-bottom: 0;'>
                                    <div class='text-right'>
                                        <button class='btn btn-primary btn-large' type="submit" name="submit">
                                            <i class='icon-save'></i>
                                            Lưu
                                        </button>
                                    </div>
                                </div>
                            <?php $this->endWidget(); ?>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</section>
