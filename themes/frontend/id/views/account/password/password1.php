<section id='content'>
    <div class='container-fluid'>
        <div class='row-fluid' id='content-wrapper'>
            <div class='span12'>
                <div class='page-header'>
                    <h1 class='pull-left'>
                        <i class='icon-lock'></i>
                        <span>Đổi mật khẩu cấp 1</span>
                    </h1>
                    <div class='pull-right'>
                        <ul class='breadcrumb'>
                            <li>
                                <a href="<?php echo Yii::app()->getBaseUrl(true); ?>" title="Tổng quan"><i class='icon-dashboard'></i></a>
                            </li>
                            <li class='separator'>
                                <i class='icon-angle-right'></i>
                            </li>
                            <li>Quản lý mật khẩu</li>
                            <li class='separator'>
                                <i class='icon-angle-right'></i>
                            </li>
                            <li class='active'>Đổi mật khẩu cấp 1</li>
                        </ul>
                    </div>
                </div>
                
                <?php $this->renderPartial('//_block/_msg', array('msg' => $msg)); ?>

                <div class='row-fluid'>
                    <div class='span12 box'>
                        <div class='box-content box-double-padding'>
                            <script type="text/javascript">
//                            $(document).ready(function() {
//                                // Chon hinh thuc thay doi bang mat khau cap 1 hay cap 2
//                                $('.choose_formality').click(function() {
//                                    var formality = $(this).val();
//                                    if (formality == 'password2') {
//                                        $('#password1').hide();
//                                        $('#password2').show();
//                                    } else {
//                                        formality == 'password1';
//                                        $('#password1').show();
//                                        $('#password2').hide();
//                                    }
//                                    // Thay doi hien thi nut bam
//                                    $('.choose_formality').removeClass('btn-success');
//                                    $(this).addClass('btn-success');
//                                    
//                                    // Thay doi gia tri trong input hidden de xac dinh dang dung hinh thuc nao
//                                    $('#formality').val(formality);
//                                });
//                                
//                                // Tooltip
//                                $('a[data-toggle="tooltip"]').tooltip();
//                            });
                            </script>
                            <?php $form = $this->beginWidget('CActiveForm', array(
                                'id'=>'editPassword',
                                'enableAjaxValidation'=>FALSE,
                                'enableClientValidation'=>true,
                                'htmlOptions' => array('name' => 'editPassword', 'class' => 'form', 'style' => 'margin-bottom: 0')
                            )); ?>
                                <fieldset>
                                    <div class='span7'>
                                        <div class='control-group' id="password1">
                                            <label class='control-label'>Mật khẩu cấp 1 hiện tại</label>
                                            <div class='controls'>
                                                <input type="password" name="password" placeholder="Nhập mật khẩu cấp 1 hiện tại" class="span12" />
                                            </div>
                                        </div>
                                        <div class='control-group'>
                                            <label class='control-label'>Mật khẩu mới</label>
                                            <div class='controls'>
                                                <input type="password" name="newpassword" placeholder="Nhập mật khẩu cấp 1 mới" class="span12" />
                                            </div>
                                            <label class='control-label'>Xác nhận mật khẩu mới</label>
                                            <div class='controls'>
                                                <input type="password" name="newpassword_confirm" placeholder="Nhập lại mật khẩu cấp 1 mới để xác nhận" class="span12" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class='span5'>
                                        <div class='lead'>
                                            <i class='icon-info-sign text-contrast'></i>
                                            Hướng dẫn
                                        </div>
                                        <ul class="unstyled">
                                            <li><i class="icon-ok text-green"></i>
                                                Mật khẩu cấp 1 dùng để đăng nhập vào khu vực cá nhân và các dịch vụ khác từ Let.vn
                                            </li>
                                            <li><i class="icon-ok text-green"></i>
                                                Dùng mật khẩu cấp 2 hoặc mật khẩu cấp 1 hiện tại có thể đổi được mật khẩu cấp 1
                                            </li>
                                            <li><i class="icon-ok text-green"></i>
                                                Dùng số điện thoại đã đăng ký có thể đổi được mật khẩu cấp 1 <br />
                                                Dùng số điện thoại: <a data-toggle="tooltip" title="Bắt buộc gửi tin nhắn từ số điện thoại này thì mới được chấp nhận" href="javascript:void(0);">0988631988</a> để soạn tin: <br />
                                                <a data-toggle="tooltip" title="mk là mã tin nhắn đổi mật khẩu" href="javascript:void(0);">mk</a> <a data-toggle="tooltip" title="234564 là id của bạn" href="javascript:void(0);">234564</a> <a data-toggle="tooltip" title="Điền mật khẩu cấp 1 mới mà bạn muốn đổi" href="javascript:void(0);">{mật khẩu cấp 1}</a> gửi <a data-toggle="tooltip" title="Giá dịch vụ là 15.000đ" href="javascript:void(0);">8771</a>
                                            </li>
                                        </ul>
                                    </div>
                                </fieldset>
                                <hr class='hr-normal' />
                                <div class='form-actions' style='margin-bottom: 0;'>
                                    <div class='text-right'>
                                        <button class='btn btn-primary btn-large' type="submit" name="submit">
                                            <i class='icon-save'></i>
                                            Lưu
                                        </button>
                                    </div>
                                </div>
                            <?php $this->endWidget(); ?>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</section>
