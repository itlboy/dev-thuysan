<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Let Games</title>
        <link rel="stylesheet" href="plugins/jquery.mobile/jquery.mobile-1.3.1.min.css" />
        <!--<link rel="stylesheet" href="../../_assets/css/jqm-demos.css">-->
        <!--<link rel="shortcut icon" href="../../favicon.ico">-->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
        <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
        <!--<script src="../../_assets/js/index.js"></script>-->
        <script src="plugins/jquery.mobile/jquery.mobile-1.3.1.min.js"></script>
        <style>
            /* Adjust the width of the left reveal menu.
            Copy all CSS from jquery.mobile.panel.css and delete the properties other than width, left, right and transform.
            Then delete the selectors/rules for display modes (reveal/push/overlay) and the position (left/right) that you don't use.
            If you don't use fixed toolbars on your page you can delete those selectors as well.
            Narrow the scope of the selectors to prevent other panels being affected by the overrides. */
            #demo-page #left-panel.ui-panel {
                width: 15em;
            }
            #demo-page #left-panel.ui-panel-closed {
                width: 0;
            }
            #demo-page .ui-panel-position-left.ui-panel-display-reveal {
                left: 0;
            }
            #demo-page .ui-panel-content-wrap-position-left.ui-panel-content-wrap-open,
            .ui-panel-dismiss-position-left.ui-panel-dismiss-open {
                left: 15em;
                right: -15em;
            }
            #demo-page .ui-panel-animate.ui-panel-content-wrap-position-left.ui-panel-content-wrap-open.ui-panel-content-wrap-display-reveal {
                left: 0;
                right: 0;
                -webkit-transform: translate3d(15em,0,0);
                -moz-transform: translate3d(15em,0,0);
                transform: translate3d(15em,0,0);
            }

            /* Combined listview collapsible menu. */
            /* Unset negative margin bottom on the listviews. */
            #left-panel .ui-panel-inner > .ui-listview { margin-bottom: 0; }
            /* Unset top and bottom margin on collapsible set. */
            #left-panel .ui-collapsible-set { margin: 0; }
            /* The first collapsible contains the collapsible set. Make it fit exactly in the collapsible content. */
            #left-panel .ui-panel-inner > .ui-collapsible > .ui-collapsible-content { padding-top: 0; padding-bottom: 0;  border-bottom: none; }
            /* Remove border top if a collapsible comes after a listview. */
            #left-panel .ui-panel-inner  > .ui-collapsible > .ui-collapsible-heading .ui-btn,
            #left-panel .ui-collapsible.ui-first-child .ui-collapsible-heading .ui-btn { border-top: none; }
            /* Give the first collapsible heading the same padding, i.e. same height, as the list items. */
            #left-panel .ui-collapsible-heading .ui-btn-inner { padding: .7em 40px .7em 15px; }
            /* Give the other collapsible headings the same padding and font-size as the list divider. */
            #left-panel .ui-collapsible-set .ui-collapsible-heading .ui-btn-inner { padding: .5em 40px .5em 15px; font-size: 14px; }

            /* Styling of the close button in both panels. */
            #demo-page .ui-panel-inner > .ui-listview .ui-first-child {
                background: #eee;
            }

            /* Reveal menu shadow on top of the list items */
            #demo-page .ui-panel-display-reveal {
                -webkit-box-shadow: none;
                -moz-box-shadow: none;
                box-shadow: none;
            }
            #demo-page .ui-panel-content-wrap-position-left {
                -webkit-box-shadow: -5px 0px 5px rgba(0,0,0,.15);
                -moz-box-shadow: -5px 0px 5px rgba(0,0,0,.15);
                box-shadow: -5px 0px 5px rgba(0,0,0,.15);
            }

            /* Use the ui-body class of your page theme (ui-body-d in this demo) to set a background image.
            This class will be added to the content wrapper, while the page itself gets the same background
            as the panel before opening the panel. */
            #demo-page .ui-body-d {
                background-image: url(../../_assets/img/bg-pattern.png);
                background-repeat: repeat-x;
                background-position: left bottom;
            }
            
            .ui-panel-content-fixed-toolbar-open.ui-panel-content-fixed-toolbar-position-left {left: -32px !important;}
        </style>
    </head>
    <body>
        <div data-role="page" id="demo-page" data-theme="d">
            <div data-role="header" data-theme="b">
                <h1><a href="index.php">Let Games</a></h1>
                <a href="#left-panel" data-icon="bars" data-iconpos="notext" data-shadow="false" data-iconshadow="false">Menu</a>
                <!--<a href="index.php" data-icon="home" data-iconpos="notext" data-direction="reverse">Home</a>-->
                <a href="#" data-icon="search" data-iconpos="notext" data-rel="dialog" data-transition="fade">Search</a>
            </div><!-- /header -->

