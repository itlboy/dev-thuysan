<div data-role="content" data-theme="c">	
    <div class="content-primary">	                
        <ul data-role="listview" data-inset="true" data-split-icon="gear">
            <?php
            $this->widget('application.modules.game.widgets.frontend.game_list', array(
                'where' => 'focus = 1',
                'order_by' => 'sorder DESC',
                'limit' => 10,
                'view' => 'game_status',
                'view_title' => 'BOX GAME FOCUS',
            ));

            $this->widget('application.modules.game.widgets.frontend.game_list', array(
                'where' => 'hot = 1',
                'order_by' => 'sorder DESC',
                'limit' => 10,
                'view' => 'game_status',
                'view_title' => 'BOX GAME HOT',
            ));
            ?>
        </ul>
    </div><!--/content-primary -->		
</div><!-- /content -->

