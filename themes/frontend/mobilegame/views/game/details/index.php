<?php
$prices = array(
    '500' => '6046',
    '1000' => '6146',
    '2000' => '6246',
    '3000' => '6346',
    '4000' => '6446',
    '5000' => '6546',
    '10000' => '6646',
    '15000' => '6746',
);
?>
<div data-role="content" data-theme="c">
    <div>
        <?php
        echo CHtml::link('Trang chủ', Yii::app()->homeUrl);
        if (!empty($category))
            echo ' / ' . CHtml::link($category[0]['name'], $category[0]['url']);
        echo ' / ' . $data['title'];
        ?>
    </div>
    
    <?php if(isset($message['success'])) :?>
        <div class="message"> <?php echo $message['success']; ?> </div>
    <?php elseif (isset($message['error'])):?>
        <div class="messageError"> <?php echo $message['error']; ?> </div>
    <?php endif; ?>
    
    <?php if ($data['file'] !== NULL) : ?>
    <?php /* <div><a href="<?php echo MFile::autoPathView($data['file'], 'game', 'file'); ?>" data-role="button" data-icon="arrow-d" data-iconpos="left" data-theme="e">Download</a></div> */ ?>
    <!--<div><a href="<?php echo $data['url'] . '?download=1'; ?>" data-role="button" data-icon="arrow-d" data-iconpos="left" data-theme="e">Download</a></div>-->
    <?php endif; ?>
    
    <h1><?php echo $data['title']; ?></h1>    
    <a href="<?php echo $data['url'] . '?download=1'; ?>" data-role="button" data-theme="e">Tải ngay</a>
    <div style="margin-bottom: 15px; text-align: center;">Hoặc soạn tin: LET DL <?php echo $data['id']; ?> gửi <?php echo $prices[$data['price']]; ?></div>
    <img src="<?php echo Common::getImageUploaded('game/large/' . $data['image']); ?>" alt="<?php echo $data['title']; ?>" style="width:100%" />
    
    <?php
    Yii::import('application.vendors.libs.grabVideo');
    echo grabVideo::player($data['video'], '100%', 300);
    ?>
    <h4><?php echo $data['content']; ?></h4>
    <div>
        <table data-role="table" id="movie-table" data-mode="reflow" class="ui-responsive table-stroke">
            <thead>
                <tr>
                    <th data-priority="1">Hệ điều hành</th>
                    <th data-priority="2">Thể loại</th>
                    <th data-priority="3">Giới thiệu</th>
                    <th data-priority="3">Giá</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th><?php echo CHtml::link(KitGame::model()->parseOperatingSystemOptions($data['operating_system']), array('//game/frontend/os/index', 'id' => $data['operating_system'])); ?></th>
                    <td>
                        <?php
                        if (!empty($category)) {
                            $catLink = array();
                            foreach ($category as $row) {
                                $catLink[] = CHtml::link($row['name'], $row['url'], array('title' => $row['name']));
                            }
                            echo implode(', ', $catLink);
                        }
                        ?>                        
                    </td>
                    <td><?php echo $data['intro']; ?></td>
                    <td><?php echo number_format($data['price'], 0, ',', '.'); ?> VND</td>
                </tr>
            </tbody>
        </table>

    </div>      
</div><!-- /content -->

<?php
if (isset($category[0]['id'])) {
    $this->widget('application.modules.game.widgets.frontend.game_list', array(
        'category' => $category[0]['id'],
        'where' => 't.id != "'.intval($data['id']).'" AND price = "'.intval($data['price']).'"',
        'order_by' => 'rand()',
        'item' => intval($data['id']),
        'limit' => 5,
        'view' => 'game_related',
        'view_title' => 'GAME LIÊN QUAN',
    ));
}

