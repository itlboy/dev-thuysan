<!-- Top menu -->
<div class="grid_24 topMenu padding_bottom">
    <div class="topmenu_s1 clearfix">
        <div class="topmenu_s1_in">
            <div class="topmenu_s1_chn">
                <ul class="topmenu_s1_chn_ul">
                    <li><a href="<?php echo Yii::app()->getBaseUrl(true); ?>" title="">Trang chủ</a></li>
                    <li><a href="http://phim.let.vn" title="" class="active">Phim</a></li>
<!--                    <li><a href="http://cuoi.let.vn" title="">Cười +</a><span class="topmenu_s1_icon_hot">hot</span></li>
                    <li><a href="http://me2.vn" title="">Chợ tình</a></li>-->
                    <li class="last"><a href="htp://forum.let.vn" title="">Diễn đàn</a></li>
                </ul>
            </div>
            <div class="topmenu_s1_pro">
                <ul class="topmenu_s1_pro_ul">
<!--                    <li><a href="http://id.let.vn" title="Đăng ký tham gia let.vn">Đăng ký</a></li>
                    <li><a href="javascript:void(0);" title="Đăng nhập">Đăng nhập</a></li>--> 
               </ul>
            </div>
        </div>
    </div>
    
    <div class="topmenu_s2 clearfix">
        <ul class="list">
            <li><a href="<?php echo Yii::app()->createUrl('film/search/index'); ?>" title="">Phim mới</a></li>
            <li><a href="<?php echo Yii::app()->createUrl('film/search/index', array('hot' => 1)); ?>" title="">Phim HOT</a></li>
            <li><a href="<?php echo Yii::app()->createUrl('film/search/index', array('length_1' => 1)); ?>" title="">Phim bộ</a></li>
            <li><a href="<?php echo Yii::app()->createUrl('film/search/index', array('length_0' => 1)); ?>" title="">Phim lẻ</a></li>
            <li><a href="<?php echo Yii::app()->createUrl('film/search/index', array('theater' => 1)); ?>" title="">Phim chiếu rạp</a></li>
            <li class="last"><a href="javascript:void(0);" title="">Thể loại phim</a>
                <div class="submenu">
                    <?php $this->widget('category.widgets.frontend.treeCategory', array(
                        'module' => 'film',
                        'view' => 'treeCategory_letphimNew',
                    )); ?>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- END Top menu -->

            
<div>
    <?php
    // Top menu
    $banners = array(
        array(
            'from' => strtotime('2013-04-26 00:00:00'),
            'to' => strtotime('2013-05-27 23:59:59'),
            'source' => Yii::app()->theme->baseUrl . '/ads/onec.swf',
            'url' => '',
            'source_default' => Yii::app()->theme->baseUrl . '/ads/default/Top_banner_480x100.jpg',
            'url_default' => 'https://docs.google.com/spreadsheet/ccc?key=0AukxH5-6U2AIdGZfSzlwcnVvcUtMMzhncGthOVRiaEE#gid=0',
            'type' => 'flash',
            'class' => 'fl',
            'width' => '480',
            'height' => '100',
        ),
        array(
            'from' => strtotime('2013-01-09 00:00:00'),
            'to' => strtotime('2013-01-30 23:59:59'),
            'source' => 'http://ad.thaisonmedia.com/easy/topright_480x100_phim.let.vn.html',
            'url' => '',
            'source_default' => Yii::app()->theme->baseUrl . '/ads/default/Top_banner_480x100.jpg',
            'url_default' => 'https://docs.google.com/spreadsheet/ccc?key=0AukxH5-6U2AIdGZfSzlwcnVvcUtMMzhncGthOVRiaEE#gid=0',
            'type' => 'iframe',
            'class' => 'fr',
            'width' => '480',
            'height' => '100',
        ),
    );
    ?>
    <?php $this->renderPartial('//blocks/ads', array(
        'banners' => $banners,
    )); ?>
</div>

