<div class="grid_24 padding_bottom">
    <div class="box_s1 clearfix border_red bg_white">
        <div class="clearfix">
            <div class="grid_2 alpha title">Kết quả của từ khóa: <?php echo $this->options['keyword']; ?></div>
        </div>
        <div class="mainContent">
            <!-- Video list -->
            <div class="grid_24 alpha omega list_video_s1">
                <div class="list maggin_left_20">
                	<?php foreach($rows as $key => $item): ?>
                        <?php $item["url"] = 'http://phim.let.vn' . $item["url"]; ?>
                        <div class="item clearfix<?php if ($key % 6 == 5): ?> last<?php endif; ?>">
                            <div class="thumb mouseover">
                                <a title="" href="<?php echo $item["url"]; ?>">
                                    <img alt="" src="<?php echo Common::getImageUploaded('film/medium/' . $item['image']); ?>" width="130" height="160" />
                                </a>
                            </div>
                            <div class="item_txtbox">
                                <h6>
                                    <a class="item_title" title="<?php echo $item["title"]; ?>" href="<?php echo $item["url"]; ?>"><?php echo $item["title"]; ?></a>
                                </h6>
                                <p><span><a target="_blank" href="<?php echo $item["url"]; ?>" class="gray" title="<?php echo $item["title_english"]; ?>"><?php echo $item["title_english"]; ?></a></span></p>
                                <p>
                                    <span class="icon_play"><s title="">&nbsp;</s><em><?php echo number_format($item['view_count'],0,'.',','); ?></em></span>
                                </p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                
                <?php if (count($rows) > 0) : ?>
                <div class="list maggin_left_20">
                    <?php echo CHtml::link('Xem tất cả', array('//cms/frontend/search/index', 'module' => 'film', 'keyword' => $this->options['keyword'])); ?>
                </div> 
                <?php endif; ?>
            </div>
            <!-- END Video list -->
        </div>
    </div>
</div>
