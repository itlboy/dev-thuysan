<div class="grid_24 padding_bottom">
    <div class="box_s1 clearfix border_red bg_white">
        <div class="clearfix">
            <div class="grid_2 alpha title">Kết quả tìm Film: <?php echo $this->options['keyword']; ?></div>
        </div>
        <div class="mainContent">
            <!-- Video list -->
            <div class="grid_24 alpha omega list_video_s1">
                <?php
                $width = 190;
                $height = 190;
                ?>
                <div style="text-align: right; margin: -10px 0 5px 0;">Tìm thấy <?php echo $total; ?> kết quả.</div>
                <div id="widget-comment-wrapper">

                    <!-- Video list -->
                    <div class="list_video_s1">
                        <div class="list maggin_left_20">  
                            <div class="comment-items">
                                <?php foreach ($rows as $i => $row): ?>   
                                    <?php $row["url"] = 'http://phim.let.vn' . $row["url"]; ?>
                                    <div class="comment-item item clearfix<?php if ($i % 6 == 5): ?> last<?php endif; ?>">
                                        <div class="thumb mouseover">
                                            <a title="" href="<?php echo $row['url']; ?>">
                                                <img alt="" src="<?php echo Common::getImageUploaded('film/medium/' . $row['image']); ?>" width="130" height="160" />
                                            </a>
                                        </div>
                                        <div class="item_txtbox">
                                            <h6>
                                                <a class="item_title" title="" href="<?php echo $row['url']; ?>"><?php echo $row['title']; ?></a>
                                            </h6>
                                            <p><span><a target="_blank" href="<?php echo $row['url']; ?>" class="gray"><?php if (isset($row['title_english']) AND $row['title_english'] !== '' AND $row['title_english'] !== $row['title']) echo '(' . $row['title_english'] . ')'; ?></a></span></p>
                                            <p>
                                                <span class="icon_play"><s title="">&nbsp;</s><em><?php echo number_format($row['view_count'],0,'.',','); ?></em></span>
                                            </p>
                                        </div>
                                    </div>
                                <?php endforeach; ?>

                                <a href="javascript:;" style="display:none" class="comment-url" rel="<?php echo $url; ?>">URL</a>
                                <a href="javascript:;" style="display:none" class="comment-offset" rel="<?php echo $offset; ?>">OFFSET</a>
                            </div>

                        </div>
                    </div>
                    <!-- END Video list -->

                    <div class="comment-load"></div>
                    <div class="comment-finish"></div>
                </div>
                   
            </div>
            <!-- END Video list -->
        </div>
    </div>
</div>
