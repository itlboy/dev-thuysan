<div class="fix_width body">
    <div class="fl col_s1" style="width: 200px;">
        <?php $this->renderPartial('//classiads/blocks/_leftcol'); ?>
        <?php $this->renderPartial('//blocks/ads_left'); ?>
    </div>
    <div class="col_s1_backgound"></div>
    <div class="fl col_s2" style="width: 770px;">
        <div class="padding clearfix" style="padding: 10px;">
            <?php $this->renderPartial('//classiads/blocks/_breadcrumb',array(
                'breadcrumb' => array(
                    'Rao' => Yii::app()->createUrl('cho-tinh'),
                    $details['name'] => $details['url'],
                )
            )); ?>
            <div class="clearfix" style="margin-bottom: 10px;">
                <a href="<?php echo Yii::app()->createUrl('cho-tinh'); ?>" class="btn-mini btn-primary btn fl" style="" >Tất cả tin</a>
                <?php if(!Yii::app()->user->isGuest): ?>
                <a   href="<?php echo Yii::app()->createUrl('cho-tinh/'.Yii::app()->user->id); ?>" class="btn-mini btn-primary btn fl" style=" margin-left: 5px;" >Tin của bạn</a>
                <?php endif; ?>

                <?php if(Yii::app()->user->isGuest): ?>
                <a rel="nofollow" data-toggle="modal" href="#loginModal"  class="btn-mini btn-primary btn fr" >Đăng tin</a>
                <?php else: ?>
                <a href="<?php echo Yii::app()->createUrl('cho-tinh/dang-tin'); ?>"  class="btn-mini btn-primary btn fr" >Đăng tin</a>
                <?php endif; ?>
            </div>
            <?php $this->widget('classiads.widgets.frontend.classiads_lastest',array(
                'view' => 'classiads_lastest_let',
                'limit' => 20,
                'user_id' => isset($_GET['user_id']) ? letArray::get($_GET,'user_id') : NULL,
                'category' => !empty($category) ? $category : NULL,
            )); ?>
        </div>
        <div class="col_s2_backgound" style="border-radius: 0px;width: 770px"></div>
    </div>
</div>