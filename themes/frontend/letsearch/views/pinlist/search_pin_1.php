<div class="grid_24 padding_bottom">
    <div class="box_s1 clearfix border_red bg_white">
        <div class="clearfix">
            <div class="grid_2 alpha title">Kết quả tìm Film: <?php echo $this->keyword; ?></div>
        </div>
        <div class="mainContent">
            <!-- Video list -->
            <div class="grid_24 alpha omega list_video_s1">
                <div class="list maggin_left_20">
                	<?php foreach($rows as $key => $row): ?>
                        <div class="item clearfix<?php if ($key % 6 == 5): ?> last<?php endif; ?>">
                            <div class="thumb mouseover">
                                <?php if ($row['type'] == 'video'): ?>
                                <?php
                                Yii::import('application.vendors.libs.grabVideo');
                                echo grabVideo::youtubePlayer($row['video'], 120, 90);
                                ?>
                                <?php elseif ($row['type'] == 'image'): ?>
                                <a href="<?php echo $row['url']; ?>" class="PinImage ImgLink" target="_blank">
                                <img src="<?php echo Common::getImageUploaded('pin/medium/' . $row['image']); ?>" alt="<?php echo $row['title']; ?>" class="PinImageImg" />
                                </a>
                                <?php else: ?>
                                <?php endif; ?>
                            </div>
                            <div class="item_txtbox">
                                <h6>
                                    <a class="item_title" title="<?php echo $row["title"]; ?>" href="<?php echo $row["url"]; ?>"><?php echo $row["title"]; ?></a>
                                </h6>
                                <p>
                                    <span class="icon_play"><s title="">&nbsp;</s><em><?php echo $row['view_count'];?></em></span>
                                    <span class="icon_comment"><s title="">&nbsp;</s><em><?php echo $row['comment_count'];?></em></span>
                                </p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

                <?php if (count($rows) > 0) : ?>
                <div class="list maggin_left_20">
                    <?php echo CHtml::link('Xem tất cả', array('//cms/frontend/search/index', 'module' => 'pin', 'keyword' => $this->keyword)); ?>
                </div>
                <?php endif; ?>
            </div>
            <!-- END Video list -->
        </div>
    </div>
</div>
