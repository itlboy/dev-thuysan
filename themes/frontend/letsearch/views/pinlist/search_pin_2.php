<div class="grid_24 padding_bottom">
    <div class="box_s1 clearfix border_red bg_white">
        <div class="clearfix">
            <div class="grid_2 alpha title">Kết quả tìm Pin: <?php echo $this->keyword; ?></div>
        </div>
        <div class="mainContent">
            <!-- Video list -->
            <div class="grid_24 alpha omega list_video_s1">
                <?php
                $width = 190;
                $height = 190;
                ?>
                <div style="text-align: right; margin: -10px 0 5px 0;">Tìm thấy <?php echo $total; ?> kết quả.</div>
                <div id="pin-list">

                    <!-- Video list -->
                    <div class="list_video_s1">
                        <div class="list maggin_left_20">  
                            <div class="pin-items">
                                <?php foreach ($rows as $i => $row): ?>                        
                                    <div class="pin-item item clearfix<?php if ($i % 6 == 5): ?> last<?php endif; ?>">
                                        <div class="thumb mouseover">                                            
                                            <?php if ($row['type'] == 'video'): ?>
                                            <?php
                                            Yii::import('application.vendors.libs.grabVideo');
                                            echo grabVideo::youtubePlayer($row['video'], 120, 90);
                                            ?>
                                            <?php elseif ($row['type'] == 'image'): ?>
                                            <a href="<?php echo $row['url']; ?>" class="PinImage ImgLink" target="_blank">
                                            <img src="<?php echo Common::getImageUploaded('pin/medium/' . $row['image']); ?>" alt="<?php echo $row['title']; ?>" class="PinImageImg" />
                                            </a>
                                            <?php else: ?>
                                            <?php endif; ?>
                                        </div>
                                        <div class="item_txtbox">
                                            <h6>
                                                <a class="item_title" title="" href="<?php echo $row['url']; ?>"><?php echo $row['title']; ?></a>
                                            </h6>
                                            <p><span><a target="_blank" href="<?php echo $row['url']; ?>" class="gray"><?php if (isset($row['title_english']) AND $row['title_english'] !== '' AND $row['title_english'] !== $row['title']) echo '(' . $row['title_english'] . ')'; ?></a></span></p>
                                            <p>
                                                <span class="icon_play"><s title="">&nbsp;</s><em><?php echo $row['view_count'];?></em></span>
                                                <span class="icon_comment"><s title="">&nbsp;</s><em><?php echo $row['comment_count'];?></em></span>
                                            </p>
                                        </div>
                                    </div>
                                <?php endforeach; ?>

                                <a href="javascript:;" style="display:none" class="pin-url" rel="<?php echo $url; ?>">URL</a>
                                <a href="javascript:;" style="display:none" class="pin-offset" rel="<?php echo $offset; ?>">OFFSET</a>
                            </div>

                        </div>
                    </div>
                    <!-- END Video list -->

                    <div class="pin-load"></div>
                    <div class="pin-finish"></div>
                </div>
                   
            </div>
            <!-- END Video list -->
        </div>
    </div>
</div>
