<?php $host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ''; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title><?php echo isset($this->pageTitle) ? CHtml::encode($this->pageTitle) : Yii::app()->name; ?></title>
        <link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
        <meta name="ROBOTS" content="index, follow" />
        <!--<meta http-equiv="refresh" content="900" />-->
        <meta name="google-site-verification" content="P5DkQ2snxl62jA_Bhe49Kgv4Gxq9fsloWRK-yt6DM1E" />

		<?php
            $version = 4;
            $cs = Yii::app()->getClientScript();
            $cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/reset.css?version='.$version);
            $cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/grid.css?version='.$version);
            $cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/let.css?version='.$version);
            $cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/letphim.css?version='.$version);
            $cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/letcuoi.css?version='.$version);
            $cs->registerCoreScript('jquery');
            $cs->registerCoreScript('cookie');
            $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/let.js', CClientScript::POS_HEAD);
            $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/film.js', CClientScript::POS_HEAD);
            $cs->registerMetaTag('-zWqXqeIf4eqyqG67qc7YbZZJs6FibkbZ_XKxmk7hx4','google-site-verification');
        ?>
        
        <script>
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/vi_VN/all.js#xfbml=1";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
        
        <!-- Place this tag in your head or just before your close body tag. -->
        <script type="text/javascript" src="https://apis.google.com/js/plusone.js">
          {lang: 'vi'}
        </script>
        <!-- Place this tag where you want the +1 button to render. -->
    </head>
    <!--[if IE 6 ]><body class="ie6 ie"><![endif]-->
    <!--[if IE 7 ]><body class="ie7 ie"><![endif]-->
    <!--[if IE 8 ]><body class="ie8 ie"><![endif]-->
    <!--[if !IE]>-->
    <body>
        <?php if (isset($_GET['debug'])): ?>
        <div class="debug"></div>
        <?php endif; ?>
        <div class="letMenu">
            <div class="container_24" style="height: 38px;">
                <div class="grid_24 clearfix" style="height: 38px;">
                    <div class="home">
                        <a href="http://let.vn">
                            <s class="icon_home">&nbsp;</s>
                        </a>
                    </div>
                    <ul class="nav">
                        <li class="first"><a href="http://phim.let.vn">Phim</a></li>
<!--                        <li><a href="http://cuoi.let.vn">Cười +</a></li>
                        <li><a href="http://me2.vn">Chợ tình</a></li>-->
                        <li><a href="http://forum.let.vn">Diễn đàn</a></li>
                    </ul>
                    <div class="fr likefacebook" style="line-height: 50px; display: none;">
                        <div class="fb-like" data-href="http://www.facebook.com/trollervietnam" data-send="true" data-layout="button_count" data-width="620" data-show-faces="false"></div>
                    </div>
                    <div class="fr likefacebook" style="line-height: 43px; color: #fff; margin-right: 10px; display: none;">
                        Like để ủng hộ Let Phim nha !
                    </div>
                </div>
            </div>
        </div>
        <div class="container_24 clearfix">
            
            <!-- Header -->
            <div class="grid_24 letHeader">
                <div class="grid_6 alpha logo">
                    <a title="Về trang chủ let.vn" href="<?php echo Yii::app()->getBaseUrl(true); ?>">let.vn</a>
                </div>
                

                <!-- Search box -->
                <div class="grid_10 search_box">
                    <div class="search_box_extra">
                        <form action="<?php echo Yii::app()->createUrl('//tim-kiem'); ?>">
                            <div>
                                <?php
                                $searchValue = 'all';
                                $searchModule = array(
                                    'all' => 'Tất cả',
                                    'film' => 'Film',
                                    'pin' => 'Pin',
                                );
                                foreach ($searchModule as $key => $value) {
                                    if (isset($_GET['module']) AND $_GET['module'] == $key) {
                                        $searchValue = $key;                                 
                                        break;
                                    }
                                }
                                echo CHtml::radioButtonList('module', $searchValue, $searchModule, array(
                                    'separator' => '',
                                    'template' => '<span>{input} {label}</span>'));
                                ?>
                            </div>
                            <div>
                                <input placeholder="Tìm kiếm..." type="text" name="keyword" value="<?php echo (isset($_GET['keyword'])) ? $_GET['keyword'] : ''; ?>" />
                                <input type="submit" value="Tìm" />
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END Search box -->

                <!-- Search box -->
<!--                <div class="grid_10 search_box">
                    <?php // $this->renderPartial('//blocks/searchbox'); ?>
                    <div class="search_box_extra">
                        <ul class="search_box_keyword">
                            <li><a href="javascript:void(0);">Đàm Vĩnh Hưng</a></li>
                            <li><a href="javascript:void(0);">Phương Thanh</a></li>
                            <li><a href="javascript:void(0);">HKT</a></li>
                        </ul>
                    </div>
                </div>-->
                <!-- END Search box -->

                <!-- Cần code lại cho phù hợp với mục đích -->
<!--                <div class="grid_4 omega">
					<div>
                    	
                    </div>
                </div>-->
                <!-- END Cần code lại cho phù hợp với mục đích -->
            </div>
            <!-- END Header -->
            <?php echo $content; ?>
        </div>
        <div class="letFooter">
<!--            <div class="search_box">
                <div class="container_24 clearfix">
                    <div class="grid_10">
                        <?php // $this->renderPartial('//blocks/searchbox'); ?>
                    </div>
                </div>
            </div>
-->            
            <div class="container_24 clearfix info">
<!--                <div class="grid_24">
                    <div style="font-weight: bold;">Bản quyền thuộc về iLet Media</div>
                    <div>Website được xây dựng trên hệ thống <span class="labelinfo">Letkit v1.0</span></div>
                    <div>Website đang trong quá trình xây dựng và thử nghiệm. Mọi chi tiết xin liên hệ:</div>
                    <div><span class="labelinfo">[M]</span> 0988.631988 - 0908.631988 | <span class="labelinfo">[E]</span> nguago@let.vn | <span class="labelinfo">[Y!M]</span> letnguago</div>
                </div>-->
           </div>
        </div>
        <div id="fb-root"></div>
        
        <img border="0" src="http://whos.amung.us/swidget/nguagoletvn" width="0" height="0" />

        <!--- Popup Ads by Ambient Digital --->
        <script type="text/javascript">
        var _abd = _abd || [];
        _abd.push(["1344417668","Popup","1344418189"]);
        </script>
        <script src="http://media.adnetwork.vn/js/adnetwork.js" type="text/javascript"></script>
        <noscript><a href="http://track.adnetwork.vn/247/adServerNs/zid_1344418189/wid_1344417668/" target="_blank"><img src="http://delivery.adnetwork.vn/247/noscript/zid_1344418189/wid_1344417668/" /></a></noscript>
        <!--- Popup Ads by Ambient Digital --->

        <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-12411612-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        </script>

    </body>
</html>