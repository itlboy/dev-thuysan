<?php
//$this->renderPartial('//blocks/topmenu');

$this->widget('film.widgets.frontend.searchNew', array(
    'view' => 'search_film_1',
    'limit' => 12,
    'options' => $options
));

$this->widget('application.modules.pin.widgets.frontend.pinlist', array(
    'view' => 'search_pin_1',
    'limit' => 12,
    'keyword' => $options['keyword']
));

