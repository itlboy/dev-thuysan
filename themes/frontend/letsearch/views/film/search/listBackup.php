<?php
    // Get list Category
    $category = KitCategory::getListInParent('film');
    $category = KitCategory::treatment($category);
    if (! isset($options['length_0']) AND ! isset($options['length_1']))
        $options['length_0'] = $options['length_1'] = 1;
    $options['category'] = isset($options['category']) ? $options['category'] : 0;
?>
<script type="text/javascript">
$(document).ready(function(){
    // submenu
    $('.list_category .item').mouseover(function(){
        $(this).addClass('hover');
    }).mouseout(function(){
        $(this).removeClass('hover');
    });

	// Press enter
//	$(".filter input").keydown(function(e){
//		if (e.which == 13) {submitSearch ('let_search'); return false;}
//	});
});

function changeValue (idInput, valueInput) {
    idInput = 'let_search_'+idInput;
    jQuery('#' + idInput).val(valueInput);
    submitSearch ('let_search');
}

function changeValueOrderBy (order_by, direction) {
    jQuery('#let_search_order_by').val(order_by);
    jQuery('#let_search_direction').val(direction);
    submitSearch ('let_search');
	
}

function submitSearch (idForm) {
    var queryString = $('#' + idForm).serialize();
    jQuery.ajax({
        url: '<?php echo Yii::app()->createUrl('film/search/list'); ?>',
        type: 'GET',
        data: queryString,
        success: function(data){
            jQuery('#result_viewlist').html(data);
        }
    });
	return false;
}

function resetSearch (idForm) {
    jQuery.ajax({
        url: '<?php echo Yii::app()->createUrl('film/search/list'); ?>',
        type: 'GET',
        success: function(data){
            jQuery('#result_viewlist').html(data);
        }
    });
}
</script>
<form name="let_search" id="let_search" onsubmit="submitSearch('let_search'); return false;">
<div class="filter">
    <div class="btn-toolbar">
        <!-- Category -->
        <input type="hidden" name="category" id="let_search_category" value="<?php echo isset($options['category']) ? $options['category'] : ''; ?>" />
        <?php
        $items = array();
        $items[] = array(
            'label' => '-- Tất cả thể loại --',
            'url' => 'javascript:void(0)',
            'itemOptions' => array('onclick'=>'changeValue("category", "")'),
        );
        $root = 'Tất cả thể loại';
        foreach ($category as $key => $value) {
            $items[] = array(
                'label' => $value['name'],
                'url' => 'javascript:void(0)',
                'itemOptions' => array('onclick'=>'changeValue("category", '.$value['id'].')'),
            );
            if ((int)$value['id'] == (int)$options['category'])
                $root = $value['name'];
        }
        $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'type'=>'', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'buttons'=>array(
                array('label' => $root, 'items' => $items),
            ),
        )); ?>
        <!-- END Category -->
        
        <!-- Country --
        <input type="hidden" name="country" id="let_search_country" value="" />
        <?php
//        $root = 'Quốc gia';
//
//        $this->widget('bootstrap.widgets.TbButtonGroup', array(
//            'type'=>'', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
//            'buttons'=>array(
//                array('label' => $root, 'items'=>array(
//                    array('label'=>'Hàn Quốc', 'url'=>'#'),
//                    array('label'=>'Nhật Bản', 'url'=>'#'),
//                )),
//            ),
//        ));
		 ?>
        <!-- END Country -->
        
        <!-- Options -->
        <?php
        $list = array(
            'length_1' => 'Phim bộ',
            'length_0' => 'Phim lẻ',
            'hot' => 'HOT',
            'theater' => 'Chiếu rạp',
            'promotion' => 'Đề cử',
        );
        foreach ($list as $key => $value) {
            if (isset($options[$key]) AND $options[$key] == '1') {
                $buttons[] = array ('label' => $value, 'htmlOptions' => array('onclick'=>'changeValue("'.$key.'", 0)'), 'active' => TRUE);
            } else {
                $buttons[] = array ('label' => $value, 'htmlOptions' => array('onclick'=>'changeValue("'.$key.'", 1)'),);
                $options[$key] = 0;
            }
            echo '<input type="hidden" name="'.$key.'" id="let_search_'.$key.'" value="'.$options[$key].'" />';
        }
        $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'type' => '',
            'toggle' => 'checkbox', // 'checkbox' or 'radio'
            'buttons' => $buttons,
        )); ?>
        <!-- END Options -->
        
        <!-- Order by -->
        <input type="hidden" name="order_by_direction" id="let_search_order_by_direction" value="<?php echo isset($options['order_by_direction']) ? $options['order_by_direction'] : 'lastest'; ?>" />
        <?php
        if (isset($options['order_by_direction']) AND $options['order_by_direction'] == 'oldest') $root = 'Cũ nhất';
        elseif (isset($options['order_by_direction']) AND $options['order_by_direction'] == 'random') $root = 'Phim ngẫu nhiên';
        else $root = 'Mới nhất';
        
        $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'type'=>'', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
            'htmlOptions'=>array('class'=>'pull-right'),
            'buttons'=>array(
                array('label' => $root, 'items'=>array(
                    array('label'=>'Mới nhất', 'url'=>'javascript:void(0);', 'itemOptions' => array('onclick'=>'changeValue("order_by_direction", "lastest")')),
                    array('label'=>'Cũ nhất', 'url'=>'javascript:void(0);', 'itemOptions' => array('onclick'=>'changeValue("order_by_direction", "oldest")')),
//                    array('label'=>'Bình chọn nhiều nhất', 'url'=>'javascript:void(0);', 'itemOptions' => array('onclick'=>'changeValue("order_by", "id DESC")')),
//                    array('label'=>'Bình chọn ít nhất', 'url'=>'javascript:void(0);', 'itemOptions' => array('onclick'=>'changeValue("order_by", "id DESC")')),
//                    array('label'=>'Comment nhiều nhất', 'url'=>'javascript:void(0);', 'itemOptions' => array('onclick'=>'changeValue("order_by", "id DESC")')),
//                    array('label'=>'Comment ít nhất', 'url'=>'javascript:void(0);', 'itemOptions' => array('onclick'=>'changeValue("order_by", "id DESC")')),
//                    array('label'=>'Nhiều lượt xem nhất', 'url'=>'javascript:void(0);', 'itemOptions' => array('onclick'=>'changeValue("order_by", "id DESC")')),
//                    array('label'=>'Ít lượt xem nhất', 'url'=>'javascript:void(0);', 'itemOptions' => array('onclick'=>'changeValue("order_by", "id DESC")')),
                    array('label'=>'Phim ngẫu nhiên', 'url'=>'javascript:void(0);', 'itemOptions' => array('onclick'=>'changeValue("order_by_direction", "random")')),
                )),
            ),
        )); ?>
        <!-- END Order by -->
    </div>    
    <div class="form-search">
        <input class="input-normal" placeholder="Nhập từ khóa..." name="keyword" type="text" onchange="submitSearch('let_search');" value="<?php echo isset($options['keyword']) ? $options['keyword'] : ''; ?>" />
        <button class="btn btn-info" type="button" onclick="submitSearch('let_search')"><i class="icon-search"></i> Tìm kiếm</button>
        <button class="btn btn-danger" type="button" onclick="resetSearch()"><i class="icon-remove"></i> Làm lại bộ lọc</button>
    </div>
</div>
</form>
                
<div class="list_category clearfix">
    <?php $this->widget('film.widgets.frontend.searchNew', array('options' => $options)); ?>
</div>