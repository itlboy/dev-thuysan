<?php
if (isset($data['seo_url']) AND $data['seo_url'] !== NULL AND $data['seo_url'] !== '') $title = $data['seo_url'];
else $title = $data['title'] . '-' . $data['title_english'];
$data['url'] = 'http://phim.let.vn' . $data['url'];
?>

<?php $this->renderPartial('//blocks/topmenu'); ?>

<?php
	if(!empty($episode)){
		$this->widget('film.widgets.frontend.episodeDetails', array(
			'id' => $episode,
			'item_title' => $title,
		));
	}
?>
<div class="grid_16">
    <div class="grid_8 alpha maggin_bottom_10">
        <div class="fb-like" data-href="<?php echo $data['url']; ?>" data-send="true" data-layout="button_count" data-width="620" data-show-faces="false"></div>
    </div>
    <div class="grid_8 omega maggin_bottom_10">
        <div class="social_button right clearfix">
            <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $data['url']; ?>" target="_blank" class="facebook"><span></span></a>
            <a href="" target="_blank" class="twitter"><span></span></a>
            <a href="https://plus.google.com/share?url=<?php echo $data['url']; ?>" target="_blank" class="googleplus"><span></span></a>
        </div>
    </div>

	<!-- Lượt xem -->
    <?php $random = 'r'.rand(0, 999999999); ?>
    <?php $view_count = (isset($stats)) ? $stats->view_total : $data['view_count']; ?>
    <div class="grid_16 alpha omega maggin_bottom_15">
        <div class="grid_8 push_8 omega film_stats clearfix" title="Có <?php echo number_format($view_count,0,',','.'); ?> lượt xem">
            <a href="javascript:void(0)" onclick="$('#<?php echo $random; ?>').show(); $(this).addClass('active');" title="" class="button_stats mouseover fr"><span></span></a> <?php echo Common::convertIntToText($view_count); ?> lượt xem
        </div>
    </div>

	<?php if(isset($stats)): ?>
    <div class="grid_16 alpha omega maggin_bottom_15" id="<?php echo $random; ?>" style="display: none;">
        <a class="close">×</a>
        <div class="box_s1 clearfix bg_white padding_20">
            <div style="line-height: 20px;">
                <div class="maggin_bottom_10"><strong style="font-size: 17px;">Số liệu thống kê của phim</strong></div>
                <ul>
                    <li>Đăng bởi <strong><?php echo $user['username']; ?></strong> vào lúc <?php echo (!empty($data['created_time']) AND isset($data['created_time'])) ? date('d/m/Y H:i',strtotime($data['created_time'])) : ''; ?> (<?php echo letDate::fuzzy_span(strtotime($data['created_time'])); ?>)</li>
                    <li>Truy cập trong ngày: <?php echo number_format($stats->view_intoday,0,',','.'); ?></li>
                    <li>Truy cập trong tuần: <?php echo number_format($stats->view_inweek,0,',','.'); ?></li>
                    <li>Truy cập trong tháng: <?php echo number_format($stats->view_inmonth,0,',','.'); ?></li>
                    <li>Truy cập trong năm: <?php echo number_format($stats->view_inyear,0,',','.'); ?></li>
                    <li>Tất cả truy cập: <?php echo number_format($stats->view_total,0,',','.'); ?></li>
                </ul>
            </div>
        </div>
    </div>
	<?php endif; ?>
<?php
	$this->widget('film.widgets.frontend.episodeList', array(
		'item_id' => !empty($data['id']) ? $data['id'] : '',
		'item_title' => $title,
		'episode_current' => !empty($episode) ? $episode : 'null',
	));
?>

    <div class="grid_16 alpha omega maggin_bottom_15">
        <h1 class="title"><?php echo $data['title']; ?></h1>
        <?php if ($data['title'] !== $data['title_english']): ?>
        <h2 class="title gray maggin_bottom_10"><?php echo $data['title_english']; ?></h2>
        <?php endif; ?>
        
        <div class="grid_16 alpha omega maggin_bottom_15">
            <div class="grid_8 alpha gray">Đăng <?php echo letDate::fuzzy_span(strtotime($data['created_time'])); ?> bởi <a href="javascript:void(0);"><?php echo $user['username']; ?></a></div>
        </div>

        <div class="grid_7 alpha">
            <img src="<?php echo Common::getImageUploaded('film/large/' . $data['image']); ?>" width="250" />
        </div>
        <div class="grid_9 omega film_info background">
        	<?php
			$film_info = array();
			
			// Tập phim
			if(!empty($data['episode_display']))
				$film_info['Tập'] = $data['episode_display'];
			
			// IMDB
			if (!empty($data['imdb_point']))
				$film_info['IMDB'] = $data['imdb_point'];
				
			// Thể loại
			foreach ($category as $row) {
				$cat_link[] = '<a property="v:title" rel="v:url" href="' . $row['url'] . '" title="' . $row['name'] . '">' . $row['name'] . '</a>';
			}
			$film_info['Thể loại'] = implode(', ',$cat_link);
			
			// Diễn viên
			if (!empty($data['actor'])) {
				$actors = explode(',',$data['actor']);
				$arrActor = array();
				if(!empty($actors)){
					foreach($actors as $actor){
						$arrActor[] = '<a href="javascript:;" ><span itemprop="actors">'.$actor.'</span></a>';
					}
				}
				$film_info['Diễn viên'] = implode(',',$arrActor);
			}
			
			// Đạo diễn
			if (!empty($data['director'])) {
				$directors = explode(',',$data['director']);
				$arrDirector = array();
				if(!empty($directors)){
					foreach($directors as $director){
						if(!empty($director)){
							$director = preg_replace('/\((.*)\)/','',$director);
							$arrDirector[] = '<a href="javascript:;" ><span itemprop="director">'.$director.'</span></a>';
						}
					}
				}
				$film_info['Đạo diễn'] = implode(',',$arrDirector);
			}
			
			// Năm sản xuất
			if (!empty($data['year']))
				$film_info['Năm'] = $data['year'];
				
			// Thời lượng
			if (!empty($data['duration']))
				$film_info['Thời lượng'] = $data['duration'];
				
			// Quốc gia
			if (!empty($data['country']))
				$film_info['Quốc gia'] = $data['country'];
				
			// Giới thiệu
			if (!empty($data['intro']))
				$film_info['Giới thiệu'] = $data['intro'];
				
			$i = 0;
			?>
        	<?php foreach ($film_info as $label => $content): ?>
			<?php $i++; ?>
            <div class="row bg_<?php echo ($i % 2) + 1; ?> clearfix"><div class="grid_2 omega label"><?php echo $label; ?>:</div><div class="grid_7 alpha"><?php echo $content; ?></div></div>
            <?php endforeach; ?>
        </div>
    </div>

    <?php
    $arrCategory = array();
    foreach ($category as $row) {
        $arrCategory[] = $row['id'];
    }
    $this->widget('film.widgets.frontend.film_other',array(
		'title' => 'Phim cùng thể loại',
        'limit' => 8,
        'category' => $arrCategory,
    ));
    ?>

    <div class="grid_16 alpha omega maggin_bottom_15">
        <div class="box_s1 clearfix bg_white padding_20">
            <div class="fb-comments" data-href="<?php echo $data['url']; ?>" data-width="590" data-num-posts="10"></div>
        </div>
    </div>
</div>
<div class="grid_8" style="text-align: right;">
</div>

<?php if(KitAccount::getCheckFilmUnlimit() == FALSE AND Yii::app()->request->getParam('episode', NULL) !== NULL):?>
<!-- Code Ambient Digital -->
    <!--- Video Ad by Ambient Digital --->
    <script type="text/javascript" src="http://media.adnetwork.vn/js/jwplayer.js"></script>
    <script type="text/javascript">
    var _abd = _abd || [];
    _abd.push(["1344417668","Video","1344418247","divVideo","770","440"]);
    </script>
    <script src="http://media.adnetwork.vn/js/adnetwork.js" type="text/javascript"></script>
    <noscript><a href="http://track.adnetwork.vn/247/adServerNs/zid_1344418247/wid_1344417668/" target="_blank"><img src="http://delivery.adnetwork.vn/247/noscript/zid_1344418247/wid_1344417668/" /></a></noscript>
    <!--- Video Ad by Ambient Digital --->
<!-- END Code Ambient Digital -->
<?php endif; ?>
