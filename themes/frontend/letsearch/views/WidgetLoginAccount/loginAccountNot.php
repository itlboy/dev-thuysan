<style type="text/css">
</style>
<div class="form-login">
    <?php
    $model = new KitAccount();
    $form = $this->beginWidget('CActiveForm', array(
        'action' => Yii::app()->createUrl('account/default/login'),
        'id' => 'inlineForm',
        'htmlOptions' => array('class' => 'well',
        ),
    ));
    ?>
    <?php echo $form->textField($model, 'username', array('class' => 'input-small', 'id' => 'Kit_Acc')); ?>
    <?php echo $form->passwordField($model, 'password', array('class' => 'input-small', 'id' => 'Kit_Pass')); ?>
    <?php echo CHtml::submitButton('Log in'); ?>
    <br />
    <?php echo $form->checkBox($model, 'checkbox'); ?><label for="KitAccount_checkbox" style="margin-left: 20px;">Lưu mật khẩu</label>
    <a href="<?php echo Yii::app()->createUrl('account/default/forgot'); ?>" style="margin-left: 20px;">Forgot your password?</a>
    <?php if (Yii::app()->user->hasFlash('error')): ?>
        <div style="color: red; font-size: 11px; text-align: center;"><?php echo Yii::app()->user->getFlash('error'); ?></div>
    <?php endif; ?>
<?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.form-login .btn').live('click', function() {
            var user = $('#Kit_Acc').val();
            var pass = $('#Kit_Pass').val();
            if (user == '' || user == 'Username' || pass == '' || pass == 'Password') {
                alert('Bạn chưa nhập tài khoản hoặc mật khẩu');
                return false;
            }
        });
    });

</script>