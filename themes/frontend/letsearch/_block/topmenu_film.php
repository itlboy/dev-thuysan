<!-- Top menu -->
<div class="grid_24 topMenu padding_bottom">
    <div class="topmenu_s1 clearfix">
        <div class="topmenu_s1_in">
            <div class="topmenu_s1_chn">
                <ul class="topmenu_s1_chn_ul">
                    <li><a href="javascript:void(0):" title="">Trang chủ</a></li>
                    <li><a href="javascript:void(0):" title="" class="active">Phim</a></li>
                    <li><a href="javascript:void(0):" title="">Cười</a></li>
                    <li><a href="javascript:void(0):" title="">Diễn đàn</a></li>
                    <li><a href="list_film.php" title="">Danh sách phim</a></li>
                    <li class="last"><a href="list_pin.php" title="">Vui cười</a><span class="topmenu_s1_icon_hot">hot</span></li>
                </ul>
            </div>
            <div class="topmenu_s1_pro">
                <ul class="topmenu_s1_pro_ul">
                    <li><a href="javascript:void(0):" title="">Nạp VIP</a></li>
                    <li><a href="javascript:void(0):" title="">Đăng bài</a></li>
                </ul>
            </div>
        </div>
    </div>
    
    <div class="topmenu_s2 clearfix">
        <ul class="list">
            <li><a href="javascript:void(0):" title="">Phim hot</a></li>
            <li><a href="javascript:void(0):" title="">Phim bộ</a></li>
            <li><a href="javascript:void(0):" title="">Phim lẻ</a></li>
            <li><a href="javascript:void(0):" title="">Phim mới ra</a></li>
            <li class="last"><a href="list_film.php" title="">Phim chiếu rạp</a></li>
        </ul>
    </div>
</div>
<!-- END Top menu -->
