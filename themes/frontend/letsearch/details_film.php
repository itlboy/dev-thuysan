<?php include './header.php'; ?>
<?php include './_block/topmenu_film.php'; ?>
<div class="grid_16">
    <div class="grid_8 alpha maggin_bottom_10">
        <div class="fb-like" data-href="http://phim.let.vn" data-send="true" data-layout="button_count" data-width="620" data-show-faces="false"></div>
    </div>
    <div class="grid_8 omega maggin_bottom_10">
        <div class="social_button right clearfix">
            <a class="facebook"><span></span></a>
            <a class="twitter"><span></span></a>
            <a class="googleplus"><span></span></a>
        </div>
    </div>

    <?php $random = 'r'.rand(0, 999999999); ?>
    <div class="grid_16 alpha omega maggin_bottom_15">
        <div class="grid_8 push_8 omega film_stats clearfix" title="Có 38.523 lượt xem">
            <a href="javascript:void(0)" onclick="$('#<?php echo $random; ?>').show(); $(this).addClass('active');" title="" class="button_stats mouseover fr"><span></span></a> 38 ngàn lượt xem
        </div>
    </div>

    <div class="grid_16 alpha omega maggin_bottom_15" id="<?php echo $random; ?>" style="display: none;">
        <a class="close">×</a>
        <div class="box_s1 clearfix bg_white padding_20">
            <div style="line-height: 20px;">
                <div class="maggin_bottom_10"><strong style="font-size: 17px;">Số liệu thống kê của phim</strong></div>
                <ul>
                    <li>Đăng bởi <strong>gamegacon</strong> vào lúc 19/04/2013 04:37 (2 ngày trước)</li>
                    <li>Truy cập trong ngày: 6179</li>
                    <li>Truy cập trong tuần: 12668</li>
                    <li>Truy cập trong tháng: 12668</li>
                    <li>Truy cập trong năm: 12668</li>
                    <li>Tất cả truy cập: 12668</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="grid_16 alpha omega maggin_bottom_15">
        <div class="box_s1 clearfix bg_white padding_20">
            <?php include './_block/episode_list.php'; ?>
        </div>
    </div>

    <div class="grid_16 alpha omega maggin_bottom_15">
        <h1 class="title">Bố Ơi! Mình Đi Đâu Thế?? Tập 10</h1>
        <h2 class="title gray maggin_bottom_10">Dad, Where Are You Going?</h2>
        
        <div class="grid_16 alpha omega maggin_bottom_15">
            <div class="grid_8 alpha gray">Đăng 18 ngày trước bởi <a href="javascript:void(0);">gamegacon</a></div>
        </div>

        
        <div class="grid_7 alpha">
            <img src="http://data.let.vn/uploads/film/large/2013/04/19/bo-oi-minh-di-dau-the_287635.jpg" width="250" />
        </div>
        <div class="grid_9 omega film_info background">
            <div class="row bg_1 clearfix"><div class="grid_2 omega label">Tập:</div><div class="grid_7 alpha">2/4</div></div>
            
            <div class="row bg_2 clearfix"><div class="grid_2 omega label">IMDB:</div><div class="grid_7 alpha">8.5</div></div>
            
            <div class="row bg_1 clearfix"><div class="grid_2 omega label">Diễn viên:</div><div class="grid_7 alpha">
                <a href="javascript:;"><span itemprop="actors">Sung Dong-il</span></a>,<a href="javascript:;"><span itemprop="actors"> Kim Sung-joo</span></a>,<a href="javascript:;"><span itemprop="actors"> Kim Sung-joo</span></a>,<a href="javascript:;"><span itemprop="actors"> Song Jong-gook</span></a>,<a href="javascript:;"><span itemprop="actors"> Yoon Min-soo</span></a>
            </div></div>
            
            <div class="row bg_2 clearfix"><div class="grid_2 omega label">Thể loại:</div><div class="grid_7 alpha">
                <a property="v:title" rel="v:url" href="/hai-huoc-c46.let" title="Hài hước">Hài hước</a>, <a property="v:title" rel="v:url" href="/gia-dinh-c47.let" title="Gia đình">Gia đình</a>, <a property="v:title" rel="v:url" href="/tam-ly-c50.let" title="Tâm lý">Tâm lý</a>, <a property="v:title" rel="v:url" href="/truyen-hinh-c59.let" title="Truyền hình">Truyền hình</a>, <a property="v:title" rel="v:url" href="/than-tuong-c78.let" title="Thần tượng">Thần tượng</a>
            </div></div>
            
            <div class="row bg_1 clearfix"><div class="grid_2 omega label">Giới thiệu:</div><div class="grid_7 alpha">
                Dad! Where Are You Going?” là một chương trình truyền hình thực tế, trong đó những người cha nổi tiếng cùng con mình thực hiện các chuyến đi đến khắp các địa điểm ở Hàn Quốc. Thông qua các chuyến đi này, những người cha nổi tiếng và các con mình sẽ có cơ hội hiểu và thân thiết với nhau hơn.
            </div></div>
        </div>
    </div>

    <div class="grid_16 alpha omega maggin_bottom_15">
        <div class="box_s1 clearfix border_red bg_white">
            <div class="clearfix">
                <div class="grid_15 alpha title">Phim cùng thể loại</div>
            </div>
            <div class="mainContent">
                <!-- Video list -->
                <div class="grid_16 alpha omega list_video_s1">
                    <ul class="list maggin_left_20">
                        <?php for ($i = 0; $i < 4; $i++): ?>
                            <li class="item clearfix<?php if ($i % 4 == 3): ?> last<?php endif; ?>">
                                <div class="thumb mouseover">
                                    <a title="" href="details_film.php">
                                        <img alt="" src="http://data.let.vn/uploads/film/medium/2013/04/17/bau-troi-tinh-yeu_628814.jpg" width="130" height="160" />
                                    </a>
                                </div>
                                <div class="item_txtbox">
                                    <h6>
                                        <a class="item_title" title="" href="details_film.php">Tân câu chuyện cảnh sát</a>
                                    </h6>
                                    <p><span><a target="_blank" href="" class="gray">The police</a></span></p>
                                    <p>
                                        <span class="icon_play"><s title="">&nbsp;</s><em>48,613</em></span>
                                        <span class="icon_comment"><s title="">&nbsp;</s><em>19</em></span>
                                    </p>
                                </div>
                            </li>
                        <?php endfor; ?>
                    </ul>
                </div>
                <!-- END Video list -->
            </div>
        </div>
    </div>

    <div class="grid_16 alpha omega maggin_bottom_15">
        <div class="box_s1 clearfix bg_white padding_20">
            <div class="fb-comments" data-href="http://phim.let.vn/xem-phim-bo-oi-minh-di-dau-the-dad-where-are-you-going-online-film-3751.let" data-width="590" data-num-posts="10"></div>
        </div>
    </div>
</div>
<div class="grid_8" style="text-align: right;">
    <object width="310" height="250">
        <param name="movie" value="http://ds.serving-sys.com/BurstingRes//Site-29089/Type-2/47d224c7-9990-4b18-bb34-ec377619ed03.swf"></param>
        <param name="allowFullScreen" value="true"></param>
        <param name="allowscriptaccess" value="always"></param>
        <embed src="http://ds.serving-sys.com/BurstingRes//Site-29089/Type-2/47d224c7-9990-4b18-bb34-ec377619ed03.swf" type="application/x-shockwave-flash" width="310" height="250" allowscriptaccess="always" allowfullscreen="true"></embed>
    </object>
</div>

<?php include './footer.php'; ?>