function clearForm($element) {
    jQuery('#' + $element).find(':input').each(function() {
        switch(this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'text':
            case 'textarea':
                jQuery(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
        }
    });

    jQuery('input[name=filter_length][value=all]').attr('checked', 'checked');
    jQuery('input[name=filter_type][value=all]').attr('checked', 'checked');
}

function submitSearch(idForm, urlSearch) {
    var queryString = $('#' + idForm).serialize();
    jQuery.ajax({
        url: urlSearch,
        type: 'GET',
        data: queryString,
        success: function(data){
            jQuery('#result_viewlist').html(data);
        }
    });
    return false;
}

function resetSearch(idForm, urlSearch) {
    clearForm(idForm);
    submitSearch(idForm, urlSearch);
}
