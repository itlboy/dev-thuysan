<?php
if (isset($data['seo_url']) AND $data['seo_url'] !== NULL AND $data['seo_url'] !== '') $title = $data['seo_url'];
else $title = $data['title'] . '-' . $data['title_english'];
?>
    <link rel="canonical" href="<?php echo $data['url']; ?>" />
    <div class="fix_width body">
        <div class="fl col_s1" style="width: 200px;">
            
            <!-- User Panel -->
            <?php $this->widget('application.modules.account.widgets.WidgetLoginForm'); ?>
            <!-- END User Panel -->

            <!-- Menu -->
            <?php $this->widget('category.widgets.frontend.treeCategory', array(
                'module' => 'film',
                'view' => 'treeCategory_letphim',
            )); ?>

            <?php $this->renderPartial('//blocks/ads_left'); ?>
            <!-- END Menu -->
            
        </div>
        <div class="col_s1_backgound"></div>
        
        <div class="fl col_s4" style="width: 770px;">
            <?php $this->widget('application.modules.pin.widgets.frontend.pinfilter', array(
				'title' => 'Ảnh vailua ngẫu nhiên',
				'order' => 'RAND()',
				'limit' => 12,
				'cache_time' => 60 * 60 * 15, // 15 phút
                'view' => 'pinfilter_letphim',
			)); ?>


<!--                --><?php //if ($episode !== NULL): ?>
<!--                     Details Film-->
<!--                    --><?php
//                    $this->widget('film.widgets.frontend.episodeDetails', array(
//                        'id' => $episode,
//                        'item_title' => $title,
//                    )); ?>
<!--                --><?php //endif; ?>
                <?php
//                
//                $link = 'https://picasaweb.google.com/lh/photo/oLb1wikeW75BUCYthty5JGdsRkDU9D-GZvm2P4XELMM';
//                $link = 'http://clip.vn/watch/MV-Sac-Moi-Em-Hong-Minh-Hang,h9FN';
//                $link = 'http://www.2shared.com/video/rgNEW23R/becauseyoulive.html';
//                $link = 'http://www.dailymotion.com/video/xk9dhz_b3sour0part3_travel';
//                $link = 'http://www.youtube.com/watch?v=D7n_AsUw3nQ&feature=youtu.be';
//                $link = 'http://www.youtube.com/v/D7n_AsUw3nQ';
//                $link = 'http://youtu.be/D7n_AsUw3nQ';
//                echo grabVideo::player($link, 770, 440);
                ?>
<!--                <object height="440" width="770">
                    <param value="http://www.youtube.com/v/SaWI6pA6IV8&amp;hl=vi_VN&amp;feature=player_embedded&amp;version=3" name="movie" />
                    <param value="true" name="allowFullScreen" />
                    <param value="always" name="allowScriptAccess" />
                    <param name="wmode" value="opaque" />
                    <embed height="440" width="770" allowscriptaccess="always" allowfullscreen="true" type="application/x-shockwave-flash" src="http://www.youtube.com/v/SaWI6pA6IV8&amp;hl=vi_VN&amp;feature=player_embedded&amp;version=3" wmode="opaque"thoo />
                </object>-->
            
            <!-- Tap phim -->


<!--            --><?php //$this->widget('film.widgets.frontend.episodeList_ajax', array(
//                'item_id' => $data['id'],
//                'item_title' => $title,
//                'episode_current' => $episode,
//            )); ?>
<!--            -->
            <div id="div-episode">
                <?php
                    if($episode == NULL AND !empty($episode_default)){
                        $episode = $episode_default['id'];
                    }
                    if(!empty($episode)){
                        $this->widget('film.widgets.frontend.episodeDetails', array(
                            'id' => $episode,
                            'item_title' => $title,
                        ));
                    }
                    $this->widget('film.widgets.frontend.episodeList', array(
                        'item_id' => !empty($data['id']) ? $data['id'] : '',
                        'item_title' => $title,
                        'episode_current' => !empty($episode) ? $episode : 'null',
                    ));
                ?>
            </div>
<!--            <script type="text/javascript">-->
<!--                var title = '--><?php //echo $title; ?><!--';-->
<!--                var id_item = '--><?php //echo !empty($data['id']) ? $data['id'] : ''; ?><!--';-->
<!--                var episode_current = '--><?php //echo !empty($episode) ? $episode : 'null'; ?><!--';-->
<!--                $(document).ready(function(){-->
<!--                    $('#div-episode').html('<img style="margin:10px" src="--><?php //echo Yii::app()->theme->baseUrl; ?><!--/images/loading.gif" />');-->
<!--                    $.get('--><?php //echo Yii::app()->createUrl('film/ajax/episode') ?><!--',-->
<!--                        {-->
<!--                            title: title,-->
<!--                            id_item: id_item,-->
<!--                            episode_current: episode_current-->
<!--                        },-->
<!--                        function(data){-->
<!--                            $('#div-episode').html(data);-->
<!--                        }-->
<!--                    );-->
<!--                });-->
<!--            </script>-->
            <div class="padding" itemscope itemtype="http://schema.org/Movie" >
                <div class="clearfix">
                    <!-- Action -->
                    <div id="watch-actions">
                        <div id="watch-actions-right">
                            <span class="watch-view-count">
                                <strong><?php echo (isset($stats)) ? number_format($stats->view_total,0,'.',',') : number_format($data['view_count'], 0, ',', '.'); ?></strong>
                            </span>
                            <button onclick="return showStats(this);"   title="Hiển thị số liệu thống kê của video" type="button"  id="watch-insight-button" class="yt-uix-tooltip yt-uix-tooltip-reverse yt-uix-button yt-uix-button-default yt-uix-tooltip yt-uix-button-empty" data-button-action="yt.www.watch.actions.stats" role="button"><span class="yt-uix-button-icon-wrapper"><img class="yt-uix-button-icon yt-uix-button-icon-watch-insight" src="//s.ytimg.com/yt/img/pixel-vfl3z5WfW.gif" alt="Hiển thị số liệu thống kê của video"></span></button>
                        </div>
                        <span id="watch-like-unlike" class="yt-uix-button-group " data-button-toggle-group="optional">
                            <button like="<?php echo (intval($like['like']) === 1 ) ? 0 : 1; ?>" like_id="<?php echo  !empty($like['id']) ? $like['id'] : 0 ; ?>" default="1" item="<?php echo $data['id'] ?>" follow="<?php echo !empty($like['follow']) ? $like['follow'] : 0; ?>" onclick=";return false;" title="Tôi thích video này" type="button" class="start yt-uix-tooltip-reverse  yt-uix-button yt-uix-button-default yt-uix-tooltip button-like <?php echo (intval($like['like']) === 1 ) ? 'yt-uix-button-toggled' : ''; ?>" id="watch-like" data-button-toggle="true" data-button-action="yt.www.watch.actions.like" role="button" data-tooltip-text="Tôi thích video này"><span class="yt-uix-button-icon-wrapper"><img class="yt-uix-button-icon yt-uix-button-icon-watch-like" src="//s.ytimg.com/yt/img/pixel-vfl3z5WfW.gif" alt="Tôi thích video này"></span><span class="yt-uix-button-content">Thích </span></button>
                            <button like="<?php echo (intval($like['like']) === 2 ) ? 0 : 2; ?>" like_id="<?php echo  !empty($like['id']) ? $like['id'] : 0 ; ?>" default="2" item="<?php echo $data['id'] ?>" follow="<?php echo !empty($like['follow']) ? $like['follow'] : 0; ?>" onclick=";return false;" title="Tôi không thích video này" type="button" class="end yt-uix-tooltip-reverse  yt-uix-button yt-uix-button-default yt-uix-tooltip yt-uix-button-empty button-like <?php echo (intval($like['like']) === 2 ) ? 'yt-uix-button-toggled' : ''; ?>" id="watch-unlike" data-button-toggle="true" data-button-action="yt.www.watch.actions.unlike" role="button" data-tooltip-text="Tôi không thích video này"><span class="yt-uix-button-icon-wrapper"><img class="yt-uix-button-icon yt-uix-button-icon-watch-unlike" src="//s.ytimg.com/yt/img/pixel-vfl3z5WfW.gif" alt="Tôi không thích video này"></span></button>
                            <button like="<?php echo !empty($like['like']) ? $like['like'] : 0; ?>" like_id="<?php echo  !empty($like['id']) ? $like['id'] : 0 ; ?>"  item="<?php echo $data['id'] ?>" follow="<?php echo (intval($like['follow']) == 1) ? 0 : 1; ?>" role="button" data-button-action="yt.www.watch.actions.addto" title="Theo dõi" onclick=";return false;" class="yt-uix-tooltip-reverse yt-uix-button yt-uix-button-default yt-uix-tooltip button-follow <?php echo (intval($like['follow']) == 1) ? 'yt-uix-button-toggled' : ''; ?>" type="button" data-tooltip-text="Theo dõi"><span class="yt-uix-button-content"><span class="addto-label sp-label">Theo dõi</span> </span></button>
                        </span>

<!--                        <ul id="watch-description-extra-info">-->
<!--                            <li>-->
<!--                                <div class="video-extras-sparkbars">-->
<!--                                    <div style="width: 98.0696311617%" class="video-extras-sparkbar-likes"></div>-->
<!--                                    <div style="width: 1.93036883833%" class="video-extras-sparkbar-dislikes"></div>-->
<!--                                </div>-->
<!--                                  <span class="video-extras-likes-dislikes">-->
<!--                                    <span class="likes">2,845</span> lượt thích, <span class="dislikes">56</span> lượt không thích-->
<!--                                  </span>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                        <button onclick=";return false;" title="Chia sẻ hoặc nhúng video này" type="button" class="yt-uix-tooltip-reverse yt-uix-button yt-uix-button-default yt-uix-tooltip" id="watch-share" data-button-action="yt.www.watch.actions.share" role="button" data-tooltip-text="Chia sẻ hoặc nhúng video này"><span class="yt-uix-button-content">Chia sẻ </span></button>-->

                    </div>
                    <!-- END Action -->
<!--                    Box Stats-->
                    <div class="box-stats clearfix" style="display: none">
                        <div class="box-child">
                            <?php if(isset($stats)): ?>
                            <h3>Số liệu thống kê của phim </h3>
                            <ul>
                                <li>Đăng bởi <strong><?php echo $user['username']; ?></strong> vào lúc <?php echo (!empty($data['created_time']) AND isset($data['created_time'])) ? date('d/m/Y H:i',strtotime($data['created_time'])) : ''; ?> (<?php echo letDate::fuzzy_span(strtotime($data['created_time'])); ?>)</li>
                                <li>Truy cập trong ngày: <?php echo $stats->view_intoday; ?></li>
                                <li>Truy cập trong tuần: <?php echo $stats->view_inweek; ?></li>
                                <li>Truy cập trong tháng: <?php echo $stats->view_inmonth; ?></li>
                                <li>Truy cập trong năm: <?php echo $stats->view_inyear; ?></li>
                                <li>Tất cả truy cập: <?php echo $stats->view_total; ?></li>
                            </ul>
                            <?php endif; ?>
                        </div>
                    </div>


                    <div class="fl" style="width: 400px;">
                        <p id="watch-uploader-info"><span id="eow-date" class="watch-video-date"><?php echo letDate::fuzzy_span(strtotime($data['created_time'])); ?></span>
                            bởi <a target="_blank" href="http://forum.let.vn/space-username-<?php echo $user['username']; ?>.html" class="yt-user-name author" rel="author" dir="ltr"><?php echo $user['username']; ?></a>
                        </p>
                        <div style="margin-bottom: 10px;">
                            <div class="fb-like" data-href="http://www.facebook.com/pages/V&#xe3;i-l&#xfa;a/373533156073060" data-send="true" data-width="450" data-show-faces="true"></div>
                        </div>

                    </div>
                    <div class="fr" style="width: 200px;">
                        <!-- Like and Dislike -->
                        <?php
                            $like = !empty($stats['like_total']) ? $stats['like_total'] : 0;
                            $dislike = !empty($stats['dislike_total']) ? $stats['dislike_total'] : 0;
                            $total = $like + $dislike;
                            if(!empty($total)){
                                $point = round(($like/$total)*10,1);
                            } else {
                                $point = 0;
                            }
                        ?>
                        <ul id="watch-description-extra-info">
                            <li>
                                <div class="video-extras-sparkbars">
                                    <div style="width: <?php echo (!empty($like) AND !empty($total)) ? ($like/$total)*100 : 50; ?>%" class="video-extras-sparkbar-likes"></div>
                                    <div style="width: <?php echo (!empty($dislike) AND !empty($total)) ? ($dislike/$total)*100 : 50; ?>%" class="video-extras-sparkbar-dislikes"></div>
                                </div>
                                  <span class="video-extras-likes-dislikes" itemtype="http://schema.org/AggregateRating" itemscope itemprop="aggregateRating">
                                        <span class="likes"><?php echo $like; ?></span> lượt thích,
                                        <span class="dislikes"><?php echo $dislike ?></span> lượt không thích
                                        <span itemprop="ratingValue" style="display: none"><?php echo $point; ?></span>
                                        <span itemprop="bestRating" style="display: none">10</span>
                                        <span itemprop="ratingCount" style="display: none"><?php echo !empty($stats['comment_total']) ? (int)$stats['comment_total']+$total : $total; ?></span>
                                  </span>
                            </li>
                        </ul>

                        <!-- END Like and Dislike -->
                    </div>
                </div>
                <div>
				<?php
				$host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
				if ($host !== 'phimle.vn') {
//					Yii::app()->user->setFlash('info', '
//						<div>- Bạn có thể xem phim <a target="_blank" href="http://phimle.vn' . $data['url'] . '">' . $data['title'] . ' (' . $data['title_english'] . ')</a> tại <strong style="color:#F00;">phimle.vn</strong> mà <strong style="color:#F00;">không có popup, banner quảng cáo hay bất cứ sự làm phiền nào</strong></div>
//					');
                    $this->widget('bootstrap.widgets.TbAlert');
					Yii::app()->user->setFlash('info', '
                    <div>- <strong style="color:#F00;">04/04/2013</strong> Let Phim thời gian vừa qua xuất hiện 1 số lỗi lạ. Nếu bạn phát hiện thấy lỗi xin vui lòng gửi về <a href="http://forum.let.vn/thread-170-bao-loi-tren-trang-let-phim/1" target="_blank"><span style="color:#F00;">NƠI BÁO LỖI</span></a> để Let Phim được hoàn thiện và nâng cao chất lượng phục vụ. Xin cám ơn!</div>
					<div>- <strong style="color:#F00;">09/11/2012</strong> Các tài khoản đã <a href="http://id.let.vn/currency/pay/index.let?type=film_unlimit&payment_type=baokim&utm_source=phim.let.vn&utm_medium=message&utm_campaign=pay" target="_blank"><span style="color:#F00;">GIA HẠN</span></a> sẽ không còn bị hiện quảng cáo.</div>
					<!--<div>- <strong style="color:#F00;">20/10/2012</strong> Let Phim thu phí một số phim lẻ. Để xem phim không giới hạn, bạn cần <a target="_blank" rel="nofollow" class="btn btn-primary btn-mini" href="http://id.let.vn/currency/pay/index.let?type=film_unlimit&payment_type=card">gia hạn tài khoản</a> <span style="color:#F00; ">chỉ với 1.000đ/ngày</span></div>-->
                    <div>- <strong style="color:#F00;">05/10/2012</strong> Ra mắt ứng dụng <a href="http://phim.let.vn/cho-tinh.let?utm_source=phim.let.vn&utm_medium=message&utm_campaign=chotinh"><span style="color:#F00; ">CHỢ TÌNH</span></a> và <a href="http://phim.let.vn/cuoc-hen/sap-dien-ra.let?utm_source=phim.let.vn&utm_medium=message&utm_campaign=chotinh"><span style="color:#F00; ">HẸN HÒ</span></a></div>
                    <div>- <strong style="color:#F00;">20/09/2012</strong> Let Phim bỏ các hình thức quảng cáo mang đến sự khó chịu cho người dùng như Popup, banner quảng cáo 2 bên.</div>
					');
					$this->widget('bootstrap.widgets.TbAlert');
				}
				?>
                </div>
                <h1 class="object_title text_shadow_s1" data-title="<?php echo $data['title'].' ('.$data['title_english'].')'; ?>">
                   <a href="<?php echo Yii::app()->getBaseUrl(true); ?>/tag/<?php echo str_replace('-','_',strtolower(Common::convertStringToUrl($data['title']))); ?>" ><span itemprop="name"><?php echo $data['title']; ?></span></a>
                    <a href="<?php echo Yii::app()->getBaseUrl(true); ?>/tag/<?php echo str_replace('-','_',strtolower(Common::convertStringToUrl($data['title_english']))); ?>">(<?php echo $data['title_english']; ?>)</a>
                </h1>
                <div class="film_details clearfix">
                    <div class="images fl">
                        <div class="thumb"><img class="loaded2" itemprop="image" src="<?php echo Common::getImageUploaded('film/large/' . $data['image']); ?>" width="250" height="370"  original="<?php echo Common::getImageUploaded('film/large/' . $data['image']); ?>" style="display: inline; " /></div>
                    </div>
                    <div class="item_content fr">
                        <?php if(!empty($data['episode_display'])): ?>
                            <div class="row row2" style="margin-bottom: 5px">
                                <strong>Tập: </strong> <?php echo $data['episode_display']; ?>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($data['actor'])): ?>
                            <?php
                            $actors = explode(',',$data['actor']);
                            $arrActor = array();
                            if(!empty($actors)){
                                foreach($actors as $actor){
                                    $arrActor[] = '<a href="javascript:;" ><span itemprop="actors">'.$actor.'</span></a>';
                                }
                            }
                            ?>
                            <div class="row row2" style="margin-bottom: 5px" ><strong>Diễn viên:</strong> <?php echo implode(',',$arrActor); ?></div>
                        <?php endif;?>
                        <?php if (!empty($data['director'])): ?>
                            <?php
                            $directors = explode(',',$data['director']);
                            $arrDirector = array();
                            if(!empty($directors)){
                                foreach($directors as $director){
                                    if(!empty($director)){
                                        $director = preg_replace('/\((.*)\)/','',$director);
                                        $arrDirector[] = '<a href="javascript:;" ><span itemprop="director">'.$director.'</span></a>';
                                    }
                                }
                            }
                            ?>
                            <div class="row row1" style="margin-bottom: 5px" ><strong>Đạo diễn:</strong> <?php echo implode(',',$arrDirector); ?></div>
                        <?php endif;?>
                        <?php if (!empty($category)): ?>
                        <div class="row row1" style="margin-bottom: 5px">
                            <strong>Thể loại:</strong>
                            <?php

                                foreach ($category as $row) {
                                    $cat_link[] = '<a property="v:title" rel="v:url" href="' . $row['url'] . '" title="' . $row['name'] . '">' . $row['name'] . '</a>';
                                }
                                echo implode(', ',$cat_link);
                            ?>

                        </div>
                        <?php endif;?>
                        <!--Diem IMDB-->
                        <?php if (!empty($data['imdb_point'])): ?>
                        <div class="row row1" style="margin-bottom: 5px" >
                            <strong>IMDB: </strong> <span ><?php echo $data['imdb_point']; ?></span>/<span >10</span> điểm
                        </div>
                        <?php endif;?>

                        <!--Nam phat hanh-->
                        <?php if (!empty($data['year'])): ?>
                        <div class="row row1" style="margin-bottom: 5px" >
                            <strong>Năm phát hành: </strong> <span ><?php echo $data['year']; ?></span>
                        </div>
                        <?php endif;?>

                        <!--Thoi luong phim-->
                        <?php if (!empty($data['duration'])): ?>
                        <div class="row row1" style="margin-bottom: 5px" >
                            <strong>Thời lượng: </strong> <span ><?php echo $data['duration']; ?></span> phút
                        </div>
                        <?php endif;?>

                        <!-- Quoc Gia-->
                        <?php if (!empty($data['country'])): ?>
                        <div class="row row1" style="margin-bottom: 5px" >
                            <strong>Quốc gia: </strong> <span ><?php echo $data['country']; ?></span>
                        </div>
                        <?php endif;?>



                        <?php if (!empty($data['intro'])): ?><div class="row row2" ><?php echo $data['intro']; ?></div><?php endif;?>

                        <!-- Shareaholic-->
                        <?php
//                        $cs = Yii::app()->getClientScript();
//                        $cs->registerCssFile('http://assets.let.vn/css/shareaholic/sprite.css');
                        ?>
<!--                        <div class="shr_class shareaholic-show-on-load"></div>
                        <script type="text/javascript">
                        var SHRSB_Settings = {"shr_class":{"src":"<?php //echo $row['url']; ?>","link":"","service":"5,7,304,313","apikey":"0747414f47c2b684cf5480c36b2478689","localize":true,"shortener":"bitly","shortener_key":"","designer_toolTips":true,"tip_bg_color":"black","tip_text_color":"white","twitter_template":"${title} - ${short_link} via @Shareaholic"}};
                        var SHRSB_Globals = {"perfoption":"1"};
                        </script>
                        <script type="text/javascript">
                        (function() {
                        var sb = document.createElement("script"); sb.type = "text/javascript";sb.async = true;
                        sb.src = ("https:" == document.location.protocol ? "https://dtym7iokkjlif.cloudfront.net" : "http://cdn.shareaholic.com") + "/media/js/jquery.shareaholic-publishers-sb.min.js";
                        var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(sb, s);
                        })();
                        </script>-->
                        <!-- END Shareaholic-->

                    </div>
                </div>
            </div>
            <?php if(KitAccount::getCheckFilmUnlimit() == FALSE): ?>
            <div class="ct-ads" style="margin: 10px 0px;text-align:center">
                <?php
                $banner_from = strtotime('2013-01-18 00:00:00');
                $banner_to = strtotime('2012-12-18 23:59:59');
                $banner = ($banner_from < time() AND time() < $banner_to) ? TRUE : FALSE;
                ?>
                <?php if($banner): ?>
                <div style="text-align: center; background: #FFF;">
                    <!-- Begin Novanet -->
                    <script type="text/javascript" src="http://static.novanet.vn/client.js?z=457"></script>
                    <!-- End Novanet -->
                </div>
                <?php endif; ?>
                    
                <?php
                $banner_from = strtotime('2012-08-08 00:00:00');
                $banner_to = strtotime('2012-09-08 23:59:59');
                $banner = ($banner_from < time() AND time() < $banner_to) ? TRUE : FALSE;
                ?>
                <?php if($banner): ?>
<!--                Dat quang cao phia duoi thong tin phim-->
                <?php else: ?>
                <a href="https://docs.google.com/spreadsheet/ccc?key=0AukxH5-6U2AIdGZfSzlwcnVvcUtMMzhncGthOVRiaEE#gid=0" target="_blank">
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/ads/default/Below_Film_Info_750x100.jpg" width="750" height="100" style="margin-bottom: 5px;"/>
                </a>
                <?php endif; ?>
            </div>
            <?php endif; ?>
        </div>
        <div class="fr col_s3" style="width: 300px; margin-top: 10px;">
            <?php
            $this->widget('film.widgets.frontend.film_lastest',array(
                'limit' => 10,
                'type' => 'hot',
                'view' => 'list_style_2',
                'tabs' => array(
                    array('label'=>'Phim hot', 'active'=>true,'htmlOptions' => array(
                        'data-link' => Yii::app()->createUrl('film/ajax/lastest', array('limit' => 10, 'type' => 'hot', 'view' => 'list_style_2')),
                        'rel' => 'nofollow',
                    )),
                    array('label'=>'Xem nhiều','htmlOptions' => array(
                        'data-link' => Yii::app()->createUrl('film/ajax/lastest', array('limit' => 10, 'type' => 'view', 'view' => 'list_style_2')),
                        'rel' => 'nofollow',
                    )),
                ),
            ));
            ?>
            <?php
            $this->widget('film.widgets.frontend.film_lastest',array(
                'limit' => 5,
                'type' => 'all',
                'view' => 'list_style_2',
                'tabs' => array(
                    array('label'=>'Phim mới', 'active'=>true,'htmlOptions' => array(
                        'data-link' => Yii::app()->createUrl('film/ajax/lastest', array('limit' => 5, 'type' => 'all', 'view' => 'list_style_2')),
                        'rel' => 'nofollow',
                    )),
                    array('label'=>'Phim tập','htmlOptions' => array(
                        'data-link' => Yii::app()->createUrl('film/ajax/lastest', array('limit' => 5, 'type' => 'many', 'view' => 'list_style_2')),
                        'rel' => 'nofollow',
                    )),
                    array('label'=>'Phim lẻ','htmlOptions' => array(
                        'data-link' => Yii::app()->createUrl('film/ajax/lastest', array('limit' => 5, 'type' => 'few', 'view' => 'list_style_2')),
                        'rel' => 'nofollow',
                    )),
                ),
            ));
            ?>

        </div>
        <div class="col_s3_backgound"></div>
        <div class="fr col_s2" style="width: 465px;">
            <div class="padding" style="margin-top: 8px; margin-right:10px;">
                <?php // $this->renderPartial('_comment', array('data' => $data)); ?>
                <div class="fb-comments" data-href="http://phim.let.vn<?php echo $data['url'];?>" data-width="445" data-num-posts="10"></div>
            </div>
            
        </div>
        <div class="col_s2_backgound"></div>
    </div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.button-follow').live('click',function(){
            var obj = $(this);
            var film_id = $(this).attr('item');
            var df = $(this).attr('default');
            var like = $(this).attr('like');
            var follow = $(this).attr('follow');
            $.ajax({
                type : 'POST',
                url : '<?php echo Yii::app()->createUrl('/film/ajax/like'); ?>',
                data :{is_ajax:'follow',item:film_id,like:like,follow:follow},
                success:function(data){
                    eval('data = '+data);
                    if(data.status == 'true')
                    {
                        if(follow == 1)
                        {
                            obj.addClass('yt-uix-button-toggled');
                            obj.attr('follow',0);
                            $('.button-like').attr('follow',1)
                        } else {
                            obj.removeClass('yt-uix-button-toggled')
                            obj.attr('follow',1);
                            $('.button-like').attr('follow',0)
                        }
                    } else if(data.status == 'login')
                    {
                        alert('Bạn chưa đăng nhập !');
                    }
                }
            });

        });
        $('.button-like').live('click',function(){
            var obj = $(this);
            var film_id = $(this).attr('item');
            var df = $(this).attr('default');
            var like = $(this).attr('like');
//            var id = $(this).attr('like_id');
            var follow = $(this).attr('follow');
            $.ajax({
                type : 'POST',
                url : '<?php echo Yii::app()->createUrl('/film/ajax/like'); ?>',
                data :{is_ajax:'like',item:film_id,like:like,follow:follow},
                success:function(data){
                    eval('data = '+data);
                    if(data.status == 'true')
                    {
                        reset();
                        $('.button-like').removeClass('yt-uix-button-toggled');
                        obj.addClass('yt-uix-button-toggled');
                        if(like == 0)
                        {
                            obj.attr('like',df);
                            obj.removeClass('yt-uix-button-toggled');
                        } else {
                            if(obj.attr('id') != 'watch-unlike'){
                                $('.button-follow').addClass('yt-uix-button-toggled');
                            }
                            obj.attr('like',0);
                        }
                    } else if(data.status == 'login')
                    {
                        alert('Bạn chưa đăng nhập !');
                    }
                }
            });

        });
        function reset()
        {
            $('.button-like').each(function(){
                $(this).attr('like',$(this).data('default'));
            });
        }



    });
    function showStats(obj){
        $(document).ready(function(){
            if(!$(obj).attr('stats')){
                $(obj).addClass('yt-uix-button-toggled');
                $(obj).attr('stats','stats');
                $('.box-stats').slideDown();
            } else {
                $(obj).removeAttr('stats');
                $(obj).removeClass('yt-uix-button-toggled');
                $('.box-stats').slideUp();
            }
        });
    }
</script>

    <?php if(KitAccount::getCheckFilmUnlimit() == FALSE AND Yii::app()->request->getParam('episode', NULL) !== NULL):?>
    <!-- Code Ambient Digital -->
        <!--- Video Ad by Ambient Digital --->
        <script type="text/javascript" src="http://media.adnetwork.vn/js/jwplayer.js"></script>
        <script type="text/javascript">
        var _abd = _abd || [];
        _abd.push(["1344417668","Video","1344418247","divVideo","770","440"]);
        </script>
        <script src="http://media.adnetwork.vn/js/adnetwork.js" type="text/javascript"></script>
        <noscript><a href="http://track.adnetwork.vn/247/adServerNs/zid_1344418247/wid_1344417668/" target="_blank"><img src="http://delivery.adnetwork.vn/247/noscript/zid_1344418247/wid_1344417668/" /></a></noscript>
        <!--- Video Ad by Ambient Digital --->
    <!-- END Code Ambient Digital -->
    <?php endif; ?>
