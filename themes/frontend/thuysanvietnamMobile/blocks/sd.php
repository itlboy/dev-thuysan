<?php
session_start();
$aResponse['error'] = false;
$aResponse['message'] = '';

// ONLY FOR THE DEMO, YOU CAN REMOVE THIS VAR
$aResponse['server'] = '';
// END ONLY FOR DEMO

/*****KET NOI CSDL ********/
$dbc = @mysql_connect('localhost','letkit','');
mysql_select_db('let_kit_article',$dbc);
/***********************/

if(isset($_POST['action']))
{
    if(htmlentities($_POST['action'], ENT_QUOTES, 'UTF-8') == 'rating')
    {
        /*
        * vars
        */
        $id = intval($_POST['idBox']);
        $rate = floatval($_POST['rate']);

        // YOUR MYSQL REQUEST HERE or other thing :)
        /*
        *
        */

        // if request successful
        $success = true;
        // else $success = false;

        /************KIEM TRA SESSION ************/
        if($_SESSION['rated']==$id){ //Nếu tồn tại session và bằng id thì báo lổi
            $aResponse['error'] = true;
            $aResponse['message'] = 'Da binh chon';

            // ONLY FOR THE DEMO, YOU CAN REMOVE THE CODE UNDER
            $aResponse['server'] = 'da binh chon';
            // END ONLY FOR DEMO


            echo json_encode($aResponse);
        }else{ //Nếu chưa thì cập nhật vào CSDL
            // json datas send to the js file
            if($success)
            {
                $aResponse['message'] = 'Your rate has been successfuly recorded. Thanks for your rate :)';

                // ONLY FOR THE DEMO, YOU CAN REMOVE THE CODE UNDER
                $aResponse['server'] = '<strong>Success answer :</strong> Success : Your rate has been recorded. Thanks for your rate :)<br />';
                $aResponse['server'] .= '<strong>Rate received :</strong> '.$rate.'<br />';
                $aResponse['server'] .= '<strong>ID to update :</strong> '.$id;
                // END ONLY FOR DEMO

                /************CAP NHAT VAO CSDL **********/
                $query = "UPDATE `let_kit_article` SET `rate_sum` = `rate_sum`+$rate, `rate_count`=`rate_count`+1 WHERE `id`='".$id."'";
                $result = mysql_query($query);
                $sql = "SELECT `rate_t`,`rate_c` FROM `bang_tin` WHERE `id`='$id'";
                $rate = mysql_fetch_assoc(mysql_query($sql));
                $aResponse['server'] .= "<br />Diem so".floor($rate['rate_t']/$rate['rate_c']);
                $result_data = floor($rate['rate_sum']/$rate['rate_count']);
                $_SESSION['rated'] = $id;
                $aResponse['server'] .= $_SESSION['rated'];
                /**************************************/


                echo json_encode($aResponse);
            }else{
                $aResponse['error'] = true;
                $aResponse['message'] = 'An error occured during the request. Please retry';

                // ONLY FOR THE DEMO, YOU CAN REMOVE THE CODE UNDER
                $aResponse['server'] = '<strong>ERROR :</strong> Your error if the request crash !';
                // END ONLY FOR DEMO


                echo json_encode($aResponse);
            }
        }
    }
    else
    {
        $aResponse['error'] = true;
        $aResponse['message'] = '"action" post data not equal to \'rating\'';

        // ONLY FOR THE DEMO, YOU CAN REMOVE THE CODE UNDER
        $aResponse['server'] = '<strong>ERROR :</strong> "action" post data not equal to \'rating\'';
        // END ONLY FOR DEMO


        echo json_encode($aResponse);
    }
}
else
{
    $aResponse['error'] = true;
    $aResponse['message'] = '$_POST[\'action\'] not found';

    // ONLY FOR THE DEMO, YOU CAN REMOVE THE CODE UNDER
    $aResponse['server'] = '<strong>ERROR :</strong> $_POST[\'action\'] not found';
    // END ONLY FOR DEMO


    echo json_encode($aResponse);
} 