<div class="navbar-right">
    <p style="height: 260px">
        <embed align="middle" width="300" height="250" quality="high" wmode="transparent" allowscriptaccess="always" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" src="http://xahoi.com.vn/data/adv/thang062012/bia_huongsen.swf">
    </p>
    <div class="box-tool">
        <div class="box-tool-wap">
            <div class="box-tool-wap-left">
                <h3 class="rates">Vietcombank</h3>
                <div class="rates-table">
                    <table border="0">
                        <tr>
                            <td class="rates-label">
                                USD
                            </td>
                            <td class="rates-value">
                                20,990.000
                            </td>
                        </tr>
                        <tr>
                            <td class="rates-label">
                                AUD
                            </td>
                            <td class="rates-value">
                                21,147.610
                            </td>
                        </tr>
                        <tr>
                            <td class="rates-label">
                                CAD
                            </td>
                            <td class="rates-value">
                                20,633.870
                            </td>
                        </tr>
                        <tr>
                            <td class="rates-label">
                                CHF
                            </td>
                            <td class="rates-value">
                                22,221.090
                            </td>
                        </tr>
                        <tr>
                            <td class="rates-label">
                                DKK
                            </td>
                            <td class="rates-value">
                                3,598.520
                            </td>
                        </tr>
                        <tr>
                            <td class="rates-label">
                                EUR
                            </td>
                            <td class="rates-value">
                                26,679.990
                            </td>
                        </tr>
                        <tr>
                            <td class="rates-label">
                                GBP
                            </td>
                            <td class="rates-value">
                                32,789.990
                            </td>
                        </tr>
                        <tr>
                            <td class="rates-label">
                                HKD
                            </td>
                            <td class="rates-value">
                                2,727.410
                            </td>
                        </tr>
                        <tr>
                            <td class="rates-label">
                                INR
                            </td>
                            <td class="rates-value">
                                384.430
                            </td>
                        </tr>
                        <tr>
                            <td class="rates-label">
                                JPY
                            </td>
                            <td class="rates-value">
                                266.830
                            </td>
                        </tr>
                        <tr>
                            <td class="rates-label">
                                KRW
                            </td>
                            <td class="rates-value">
                                20.050
                            </td>
                        </tr>
                        <tr>
                            <td class="rates-label">
                                KWD
                            </td>
                            <td class="rates-value">
                                75,829.830
                            </td>
                        </tr>
                        <tr>
                            <td class="rates-label">
                                MYR
                            </td>
                            <td class="rates-value">
                                6,667.730
                            </td>
                        </tr>
                        <tr>
                            <td class="rates-label">
                                NOK
                            </td>
                            <td class="rates-value">
                                3,566.070
                            </td>
                        </tr>
                        <tr>
                            <td class="rates-label">
                                RUB
                            </td>
                            <td class="rates-value">
                                719.430
                            </td>
                        </tr>
                        <tr>
                            <td class="rates-label">
                                SEK
                            </td>
                            <td class="rates-value">
                                3,019.430
                            </td>
                        </tr>
                        <tr>
                            <td class="rates-label">
                                SGD
                            </td>
                            <td class="rates-value">
                                16,583.800
                            </td>
                        </tr>
                        <tr>
                            <td class="rates-label">
                                THB
                            </td>
                            <td class="rates-value">
                                679.950
                            </td>
                        </tr>
                    </table>
                </div>
                <h3 class="gold">&nbsp;</h3>
                <div class="gold-table">
                    <table border="1">
                        <tr>
                            <th>
                                Loại
                            </th>
                            <th>
                                Mua
                            </th>
                            <th>
                                Bán
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <strong>SJC</strong>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                    <small>Cập nhật: 08h50 - 16/06/2012</small>
                </div>
            </div>
            <div class="box-tool-wap-right">
                <h3 class="weather">
                    <select size="1" id="weather">
                        <option value="location_0">Hồ Chí Minh</option>
                        <option value="location_1">Hải Phòng</option>
                        <option value="location_2">Nha Trang</option>
                        <option value="location_3">Pleiku</option>
                        <option value="location_4">Sơn La</option>
                        <option value="location_5">Đà nẵng</option>
                        <option selected="selected" value="location_6">Hà Nội</option>
                        <option value="location_7">Việt Trì</option>
                        <option value="location_8">Vinh</option>
                    </select>
                </h3>
                <div class="weather-table" style="display:none" id="location_0">
                    <div class="weather-table-info">
                        <img width="55" height="45" src="data/image_weather/300.gif" alt="Mây thay đổi, trời nắng"/>
                        <span>26<sup>o</sup>C</span>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="weather-table-others">
                        <ul>
                            <li>Mây thay đổi, trời nắng</li>
                            <li>Độ ẩm 90%</li>
                            <li>Gió tây nam - tốc độ: 10 m/s</li>
                        </ul>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="weather-table" style="display:none" id="location_1">
                    <div class="weather-table-info">
                        <img width="55" height="45" src="data/image_weather/260.gif" alt="Nhiều mây, không mưa"/>
                        <span>27<sup>o</sup>C</span>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="weather-table-others">
                        <ul>
                            <li>Nhiều mây, không mưa</li>
                            <li>Độ ẩm 80%</li>
                            <li>Gió bắc - tốc độ: 3 m/s</li>
                        </ul>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="weather-table" style="display:none" id="location_2">
                    <div class="weather-table-info">
                        <img width="55" height="45" src="data/image_weather/260.gif" alt="Nhiều mây, không mưa"/>
                        <span>29<sup>o</sup>C</span>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="weather-table-others">
                        <ul>
                            <li>Nhiều mây, không mưa</li>
                            <li>Độ ẩm 79%</li>
                            <li>Lặng gió</li>
                        </ul>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="weather-table" style="display:none" id="location_3">
                    <div class="weather-table-info">
                        <img width="55" height="45" src="data/image_weather/110.gif" alt="Có mưa"/>
                        <span>22<sup>o</sup>C</span>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="weather-table-others">
                        <ul>
                            <li>Có mưa</li>
                            <li>Độ ẩm 97%</li>
                            <li>Gió tây tây nam - tốc độ: 5 m/s</li>
                        </ul>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="weather-table" style="display:none" id="location_4">
                    <div class="weather-table-info">
                        <img width="55" height="45" src="data/image_weather/260.gif" alt="Nhiều mây, không mưa"/>
                        <span>24<sup>o</sup>C</span>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="weather-table-others">
                        <ul>
                            <li>Nhiều mây, không mưa</li>
                            <li>Độ ẩm 95%</li>
                            <li>Lặng gió</li>
                        </ul>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="weather-table" style="display:none" id="location_5">
                    <div class="weather-table-info">
                        <img width="55" height="45" src="data/image_weather/260.gif" alt="Nhiều mây, không mưa"/>
                        <span>28<sup>o</sup>C</span>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="weather-table-others">
                        <ul>
                            <li>Nhiều mây, không mưa</li>
                            <li>Độ ẩm 85%</li>
                            <li>Gió tây - tốc độ: 1 m/s</li>
                        </ul>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="weather-table" style="display:block" id="location_6">
                    <div class="weather-table-info">
                        <img width="55" height="45" src="data/image_weather/260.gif" alt="Nhiều mây, không mưa"/>
                        <span>28<sup>o</sup>C</span>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="weather-table-others">
                        <ul>
                            <li>Nhiều mây, không mưa</li>
                            <li>Độ ẩm 89%</li>
                            <li>Gió tây - tốc độ: 2 m/s</li>
                        </ul>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="weather-table" style="display:none" id="location_7">
                    <div class="weather-table-info">
                        <img width="55" height="45" src="data/image_weather/300.gif" alt="Mây thay đổi, trời nắng"/>
                        <span>27<sup>o</sup>C</span>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="weather-table-others">
                        <ul>
                            <li>Mây thay đổi, trời nắng</li>
                            <li>Độ ẩm 90%</li>
                            <li>Lặng gió</li>
                        </ul>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="weather-table" style="display:none" id="location_8">
                    <div class="weather-table-info">
                        <img width="55" height="45" src="data/image_weather/260.gif" alt="Nhiều mây, không mưa"/>
                        <span>27<sup>o</sup>C</span>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="weather-table-others">
                        <ul>
                            <li>Nhiều mây, không mưa</li>
                            <li>Độ ẩm 92%</li>
                            <li>Gió nam - tốc độ: 2 m/s</li>
                        </ul>
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </div>
            <div class="clear">
            </div>
            <div style="border-top: 1px solid #d6b7c7;">
                <div style="padding: 5px; border-top: 1px solid #f3e6ef;">
                    <marquee scrollamount="2" scrolldelay="30" onmouseout="this.start();" onmouseover="this.stop();">
                        Nhận giá vàng mới nhất: Soạn tin: <strong style="color: #a21362">VANG</strong> gửi <strong style="color: #a21362">7171</strong> : Giá: 1000đ/tin&nbsp;&nbsp;|&nbsp;&nbsp;Nhận thông tin tỷ giá ngoại tệ mới nhất: Soạn tin: <strong style="color: #a21362">TG</strong> gửi <strong style="color: #a21362">7171</strong> : Giá: 1000đ/tin&nbsp;&nbsp;|&nbsp;&nbsp;Nhận thông tin thời tiết mới nhất: Soạn tin: <strong style="color: #a21362">TT</strong> Têntỉnh gửi <strong style="color: #a21362">7171</strong> : Giá: 1000đ/tin </marquee>
                </div>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
    <div class="box-news">
        <div class="box-news-head">
            <h2><span><a>Ảnh đẹp</a></span></h2>
            <small>Down</small>
        </div>
        <div class="box-news-image">
            <a href="xa-hoi/phong-su/co-to-song-dong-trong-anh-panorama-102231.html" title="Cô Tô sống động trong ảnh panorama"><img src="http://image1.xahoi.com.vn/news/2012/6/14/co-to.jpg" width="347" height="220" alt="Cô Tô sống động trong ảnh panorama"/></a>
            <h3><a href="xa-hoi/phong-su/co-to-song-dong-trong-anh-panorama-102231.html" title="Cô Tô sống động trong ảnh panorama">Cô Tô sống động trong ảnh panorama</a></h3>
            <div class="scroll-vd">
                <ul class="others">
                    <li><a title="Đến Na Uy, nhớ ghé thăm con đường &quot;đứng tim&quot;" href="du-lich-kham-pha/danh-thang/den-na-uy-nho-ghe-tham-con-duong-dung-tim-102068.html">Đến Na Uy, nhớ ghé thăm con đường "đứng tim"</a></li>
                    <li><a title="6 điểm đến kỳ thú dưới nước trong mùa hè" href="du-lich-kham-pha/danh-thang/6-diem-den-ky-thu-duoi-nuoc-trong-mua-he-101899.html">6 điểm đến kỳ thú dưới nước trong mùa hè</a></li>
                    <li><a title="Sững sờ ngắm cây năng lượng mặt trời khổng lồ" href="du-lich-kham-pha/danh-thang/sung-so-ngam-cay-nang-luong-mat-troi-khong-lo-101817.html">Sững sờ ngắm cây năng lượng mặt trời khổng lồ</a></li>
                    <li><a title="Mù Căng Chải đẹp ngỡ ngàng mùa nước đổ" href="xa-hoi/phong-su/mu-cang-chai-dep-ngo-ngang-mua-nuoc-do-101688.html">Mù Căng Chải đẹp ngỡ ngàng mùa nước đổ</a></li>
                    <li><a title="10 ngọn núi 'quyến rũ' nhất Lào Cai" href="du-lich-kham-pha/danh-thang/10-ngon-nui-quyen-ru-nhat-lao-cai-101565.html">10 ngọn núi 'quyến rũ' nhất Lào Cai</a></li>
                    <li><a title="Cung điện Potala trong băng giá" href="du-lich-kham-pha/danh-thang/cung-dien-potala-trong-bang-gia-101461.html">Cung điện Potala trong băng giá</a></li>
                    <li><a title="Vẻ đẹp thanh bình của thiền viện Trúc Lâm Bạch Mã" href="du-lich-kham-pha/danh-thang/ve-dep-thanh-binh-cua-thien-vien-truc-lam-bach-ma-101345.html">Vẻ đẹp thanh bình của thiền viện Trúc Lâm Bạch Mã</a></li>
                    <li><a title="Kỳ ảo mây nhiều màu sắc tại Trung Quốc" href="quoc-te/the-gioi-trong-ngay/ky-ao-may-nhieu-mau-sac-tai-trung-quoc-101125.html">Kỳ ảo mây nhiều màu sắc tại Trung Quốc</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="box-navbar-video video-flash">
        <div class="box-news-head">
            <h2><span><a href="http://xahoi.com.vn/xa-hoi/video/">Clip đặc sắc trong ngày</a></span></h2>
            <small>Down</small>
        </div>
        <h3><a title="Mưa dông lớn Hà Nội biến thành sông trên nhiều tuyến phố" id="vtitle">Mưa dông lớn Hà Nội biến thành sông trên nhiều tuyến phố</a></h3>
        <p class="flash_284_215" id="vfile">
            <embed width="284" height="215" flashvars="width=284&height=215&file=http://image1.xahoi.com.vn/video/2012/6/ngapngap.flv&image=http://image1.xahoi.com.vn/video/2012/6/vlcsnap-2007-01-02-13h34m02s201.png" wmode="transparent" allowfullscreen="true" allowscriptaccess="always" quality="high" name="MediaPlayer" id="MediaPlayer" style="" src="images/mediaplayer.swf" type="application/x-shockwave-flash">
        </p>
        <div class="scroll-vd">
            <ul class="others">
                <li class="first"><a class="hvideo" href="http://image1.xahoi.com.vn/video/2012/6/1A.flv" rel="Hơn 30km &quot;cực hình&quot; trên quốc lộ 1A">Hơn 30km "cực hình" trên quốc lộ 1A</a></li>
                <li><a class="hvideo" href="http://image1.xahoi.com.vn/video/2012/6/nguyenngocvu.mp4" rel="Cháy trên đường Nguyễn Ngọc Vũ">Cháy trên đường Nguyễn Ngọc Vũ</a></li>
                <li><a class="hvideo" href="http://image1.xahoi.com.vn/video/2012/6/canh.FLV" rel="Cảnh sát giao thông vụt dùi cui vào mặt tài xế">Cảnh sát giao thông vụt dùi cui vào mặt...</a></li>
                <li><a class="hvideo" href="http://image1.xahoi.com.vn/video/2012/6/1(1)1.flv" rel="Mỗi mùa bóng có cả triệu người Việt tham gia cá độ">Mỗi mùa bóng có cả triệu người Việt...</a></li>
                <li><a class="hvideo" href="http://image1.xahoi.com.vn/video/2012/6/quay1.flv" rel="Gặp thí sinh quay clip tiêu cực trong thi cử ở Bắc Giang">Gặp thí sinh quay clip tiêu cực trong thi...</a></li>
                <li><a class="hvideo" href="http://image1.xahoi.com.vn/video/2012/6/trachanh_.flv" rel="Trào lưu &quot;ngày làm giám đốc, tối bán trà đá&quot; nở rộ ở HN">Trào lưu "ngày làm giám đốc, tối bán...</a></li>
                <li><a class="hvideo" href="http://image1.xahoi.com.vn/video/2012/6/trungcap.mp4" rel="Nữ sinh trung cấp lập đường dây gái gọi giá 2 triệu đồng">Nữ sinh trung cấp lập đường dây gái...</a></li>
            </ul>
        </div>
    </div>
    <script type="text/javascript" src="http://admicro1.vcmedia.vn/ads_codes/ads_box_2467.ads"></script>
    <div class="box-news">
        <div class="box-news-head">
            <h2><span><a href="http://xahoi.com.vn/giao-duc-du-hoc/tuyen-sinh/">Tuyển sinh</a></span></h2>
            <small>Down</small>
        </div>
        <div class="box-news-style2">
            <p>
                <a href="giao-duc-du-hoc/tuyen-sinh/tuyen-sinh-dh-cd-2012-nhieu-nganh-thie-u-thi-sinh-101682.html" title="Tuyển sinh ĐH, CĐ 2012: Nhiều ngành thiếu thí sinh"><img src="http://image1.xahoi.com.vn/news/2012/6/11/tuyensinh1_v.jpg" width="143" height="143" alt="Tuyển sinh ĐH, CĐ 2012: Nhiều ngành thiếu thí sinh" class="bo"/></a>
                <a href="giao-duc-du-hoc/tuyen-sinh/tuyen-sinh-dh-cd-2012-nhieu-nganh-thie-u-thi-sinh-101682.html" class="title" title="Tuyển sinh ĐH, CĐ 2012: Nhiều ngành thiếu thí sinh">Tuyển sinh ĐH, CĐ 2012: Nhiều ngành thiếu thí sinh</a>
            </p>
            <div>
                <ul class="others">
                    <li class="first"><a title="Mùa luyện thi ĐH, CĐ: "Nóng" cùng sĩ tử" href="giao-duc-du-hoc/tuyen-sinh/mua-luyen-thi-dh-cd-nong-cung-si-tu-101611.html">Mùa luyện thi ĐH, CĐ: "Nóng" cùng sĩ tử</a></li>
                    <li class="first"><a title="&quot;Đỏ mắt&quot; tìm thí sinh tới &quot;lò&quot; luyện thi" href="giao-duc-du-hoc/tuyen-sinh/do-mat-tim-thi-sinh-toi-lo-luyen-thi-101317.html">"Đỏ mắt" tìm thí sinh tới "lò" luyện thi</a></li>
                    <li class="first"><a title="Thi vào sư phạm, bao giờ hết ảm đạm?" href="giao-duc-du-hoc/tuyen-sinh/thi-vao-su-pham-bao-gio-het-am-dam-101114.html">Thi vào sư phạm, bao giờ hết ảm đạm?</a></li>
                    <li class="first"><a title="Tuyển sinh đầu cấp ở quận 4, TP. HCM" href="giao-duc-du-hoc/tuyen-sinh/tuyen-sinh-dau-cap-o-quan-4-tp-hcm-101060.html">Tuyển sinh đầu cấp ở quận 4, TP. HCM</a></li>
                    <li class="first"><a title="18/6 sẽ công bố điểm thi tốt nghiệp THPT 2012" href="giao-duc-du-hoc/tuyen-sinh/18/6-se-cong-bo-diem-thi-tot-nghiep-thpt-2012-100787.html">18/6 sẽ công bố điểm thi tốt nghiệp THPT 2012</a></li>
                </ul>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="box-news">
        <div class="box-news-head">
            <h2><span><a href="o-to-xe-may/">Ô tô - Xe máy</a></span></h2>
            <small>Down</small>
        </div>
        <div class="box-news-style2">
            <p>
                <a href="o-to-xe-may/bo-suu-tap-xe-dang-ghen-ti-cua-mot-ceo-bi-truy-na-102369.html" title="Bộ sưu tập xe đáng ghen tị của một CEO bị truy nã"><img src="http://image1.xahoi.com.vn/news/2012/6/15/sieu-xe_v.jpg" width="143" height="143" alt="Bộ sưu tập xe đáng ghen tị của một CEO bị truy nã" class="bo"/></a>
                <a href="o-to-xe-may/bo-suu-tap-xe-dang-ghen-ti-cua-mot-ceo-bi-truy-na-102369.html" class="title" title="Bộ sưu tập xe đáng ghen tị của một CEO bị truy nã">Bộ sưu tập xe đáng ghen tị của một CEO bị truy nã</a>
            </p>
            <div>

                <ul class="others">
                    <li class="first"><a title="Thêm ảnh "bé hạt tiêu" candy 50cc" href="o-to-xe-may/them-anh-be-hat-tieu-candy-50cc-102347.html">Thêm ảnh "bé hạt tiêu" Candy 50cc</a></li>
                    <li><a title="Giật mình với giá bán của Aston Martin V12 Zagato" href="o-to-xe-may/giat-minh-voi-gia-ban-cua-aston-martin-v12-zagato-102280.html">Giật mình với giá bán của Aston Martin V12 Zagato</a></li>
                    <li><a title="Hyundai i30 mới ra mắt, giá gần 400 triệu đồng" href="o-to-xe-may/hyundai-i30-moi-ra-mat-gia-gan-400-trieu-dong-102227.html">Hyundai i30 mới ra mắt, giá gần 400 triệu đồng</a></li>
                    <li><a title="Lexus ra xe mới siêu tiết kiệm nhiên liệu" href="o-to-xe-may/lexus-ra-xe-moi-sieu-tiet-kiem-nhien-lieu-102210.html">Lexus ra xe mới siêu tiết kiệm nhiên liệu</a></li>
                </ul>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="main-adv">
        <a href="http://xahoi.com.vn/website-lien-ket/" title="danh ba website"><img src="images/adv/danhba.png" width="300"/></a>
    </div>
    <div class="box-news">
        <div class="box-news-head">
            <h2><span><a href="cntt-vien-thong/">CNTT - Viễn thông</a></span></h2>
            <small>Down</small>
        </div>
        <div class="box-news-style2">
            <p>
                <a href="cntt-vien-thong/vi-tinh/sap-co-may-tinh-bang-microsoft-102368.html" title="Sắp có máy tính bảng Microsoft"><img src="http://image1.xahoi.com.vn/news/2012/6/15/may-tinh-bang-Microsoft_v.jpg" width="143" height="143" alt="Sắp có máy tính bảng Microsoft" class="bo"/></a>
                <a href="cntt-vien-thong/vi-tinh/sap-co-may-tinh-bang-microsoft-102368.html" class="title" title="Sắp có máy tính bảng Microsoft">Sắp có máy tính bảng Microsoft</a>
            </p>
            <div>
                <ul class="others">
                    <li class="first"><a title="Clip hài Bill Gates và Steve Jobs hát rap gây sốt" href="cntt-vien-thong/tin-cntt/clip-hai-bill-gates-va-steve-jobs-hat-rap-gay-sot-102342.html">Clip hài Bill Gates và Steve Jobs hát rap gây sốt</a></li>
                    <li><a title="Đã mắt với MacBook Pro 2012 của Apple" href="cntt-vien-thong/vi-tinh/da-mat-voi-macbook-pro-2012-cua-apple-102275.html">Đã mắt với MacBook Pro 2012 của Apple</a></li>
                    <li><a title="iPhone 5 rò rỉ thiết kế với logo Apple nhuốm đen" href="cntt-vien-thong/dien-thoai-phu-kien/iphone-5-ro-ri-thiet-ke-voi-logo-apple-nhuom-den-102225.html">iPhone 5 rò rỉ thiết kế với logo Apple nhuốm đen</a></li>
                    <li><a title="Có cần phải chờ iPhone 5 để mua?" href="cntt-vien-thong/dien-thoai-phu-kien/co-ca-n-pha-i-cho-iphone-5-de-mua-102211.html">Có cần phải chờ iPhone 5 để mua?</a></li>
                </ul>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="box-news">
        <div class="box-news-head">
            <h2><span><a href="http://xahoi.com.vn/thoi-trang/dia-chi-mua-sam/">Địa chỉ mua sắm</a></span></h2>
            <small>Down</small>
        </div>
        <div class="box-news-style2">
            <p>
                <a title="Playboy giảm giá 30% hè 2012 tại Hà Nội" href="dia-chi-mua-sam/playboy-giam-gia-30-he-2012-tai-ha-noi-420.html"><img width="143" height="143" class="bo" alt="Playboy giảm giá 30% hè 2012 tại Hà Nội" src="http://image1.xahoi.com.vn/shopping/play-boy-2.jpg"></a>
                <a title="Playboy giảm giá 30% hè 2012 tại Hà Nội" class="title" href="dia-chi-mua-sam/playboy-giam-gia-30-he-2012-tai-ha-noi-420.html">Playboy giảm giá 30% hè 2012 tại Hà Nội</a>
            </p>
            <div>
                <ul class="others">
                    <li class="first"><a href="dia-chi-mua-sam/hi-fashion-mo-gian-hang-dong-gia-59-000-dong-419.html" title="Hi Fashion mở gian hàng đồng giá 59.000 đồng">Hi Fashion mở gian hàng đồng giá 59.000 đồng</a></li>
                    <li><a href="dia-chi-mua-sam/dam-cong-so-tao-nha-trong-nang-he-418.html" title="Đầm công sở tao nhã trong nắng hè">Đầm công sở tao nhã trong nắng hè</a></li>
                    <li><a href="dia-chi-mua-sam/dien-vay-dep-dam-xinh-cung-bst-cua-thu-han-417.html" title="Diện váy đẹp, đầm xinh cùng BST của Thu Hân">Diện váy đẹp, đầm xinh cùng BST của Thu Hân</a></li>
                    <li><a href="dia-chi-mua-sam/princess-house-net-ca-tinh-diu-dang-416.html" title="Princess House - Nét cá tính dịu dàng">Princess House - Nét cá tính dịu dàng</a></li>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="box-news">
        <div class="box-news-head">
            <h2><span><a href="suc-khoe/">Sức khỏe</a></span></h2>
            <small>Down</small>
        </div>
        <div class="box-news-style2">
            <p>
                <a href="suc-khoe/dieu-tri-ngung-tho-khi-ngu-cai-thien-doi-song-tinh-duc-102318.html" title="Điều trị ngừng thở khi ngủ cải thiện đời sống tình dục "><img src="http://image1.xahoi.com.vn/news/2012/6/15/ngung-tho-khi-ngu_v.jpg" width="143" height="143" alt="Điều trị ngừng thở khi ngủ cải thiện đời sống tình dục " class="bo"/></a>
                <a href="suc-khoe/dieu-tri-ngung-tho-khi-ngu-cai-thien-doi-song-tinh-duc-102318.html" class="title" title="Điều trị ngừng thở khi ngủ cải thiện đời sống tình dục ">Điều trị ngừng thở khi ngủ cải thiện đời sống tình dục </a>
            </p>
            <div>
                <ul class="others">
                    <li class="first"><a title="&quot;Yêu&quot; kiểu... khác người: Chị em gặp nạn" href="suc-khoe/yeu-kieu-khac-nguoi-chi-em-gap-nan-102269.html">"Yêu" kiểu... khác người: Chị em gặp nạn</a></li>
                    <li><a title=""Chìa khóa" sinh lý trong tảo đỏ " href="suc-khoe/chia-khoa-sinh-ly-trong-tao-do-102181.html">"Chìa khóa" sinh lý trong tảo đỏ </a></li>
                    <li><a title="Thói quen uống nước khi ăn hại cho sức khỏe" href="suc-khoe/thoi-quen-uong-nuoc-khi-an-hai-cho-suc-khoe-102137.html">Thói quen uống nước khi ăn hại cho sức khỏe</a></li>
                    <li><a title="Vitamin PP và những lưu ý khi sử dụng" href="suc-khoe/vitamin-pp-va-nhung-luu-y-khi-su-dung-102011.html">Vitamin PP và những lưu ý khi sử dụng</a></li>
                </ul>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="box-news">
        <div class="box-news-head">
            <h2><span><a href="giai-tri/thu-gian/">Thư giãn</a></span></h2>
            <small>Down</small>
        </div>
        <div class="box-news-style2">
            <p>
                <a href="giai-tri/thu-gian/ky-luc-nguoi-co-nhip-ngan-dai-nhat-102278.html" title="Kỷ lục người có nhịp ngân dài nhất"><img src="http://image1.xahoi.com.vn/news/2012/6/15/tim-22_v.jpg" width="143" height="143" alt="Kỷ lục người có nhịp ngân dài nhất" class="bo"/></a>
                <a href="giai-tri/thu-gian/ky-luc-nguoi-co-nhip-ngan-dai-nhat-102278.html" class="title" title="Kỷ lục người có nhịp ngân dài nhất">Kỷ lục người có nhịp ngân dài nhất</a>
            </p>
            <div>
                <ul class="others">
                    <li class="first"><a title="Clip: Hoảng hồn với những cú vỡ bất chợt" href="giai-tri/thu-gian/clip-hoang-hon-voi-nhung-cu-vo-bat-chot-102167.html">Clip: Hoảng hồn với những cú vỡ bất chợt</a></li>
                    <li><a title="Clip: Những cú ngã độc chiêu tuần 2 tháng 6" href="giai-tri/thu-gian/clip-nhung-cu-nga-doc-chieu-tuan-2-thang-6-101935.html">Clip: Những cú ngã độc chiêu tuần 2 tháng 6</a></li>
                    <li><a title="Sặc cười với clip hài hước tuần 2 tháng 6" href="giai-tri/thu-gian/sac-cuoi-voi-clip-hai-huoc-tuan-2-thang-6-101649.html">Sặc cười với clip hài hước tuần 2 tháng 6</a></li>
                    <li><a title="Khi các nàng vồ ếch vì quá hăng" href="giai-tri/thu-gian/khi-cac-nang-vo-ech-vi-qua-hang-101242.html">Khi các nàng vồ ếch vì quá hăng</a></li>
                </ul>
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
</div>