<!-- BEGIN QUOC-TE -->
<div class="box-news">
    <div class="box-news-head">
        <h2><span><a href="quoc-te/">Quốc tế</a></span></h2>
        <p>
            <a href="quoc-te/the-gioi-trong-ngay/" title="Thế giới trong ngày">Thế giới trong ngày</a> | <a href="quoc-te/the-gioi-do-day/" title="Chuyện lạ thế giới">Chuyện lạ thế giới</a> | <a href="quoc-te/vu-khi/" title="Vũ khí">Vũ khí</a> | <a href="quoc-te/ho-so-diep-vien/" title="Hồ sơ điệp viên">Hồ sơ điệp viên</a>
        </p>
        <a href="quoc-te.rss" class="rss"><img src="images/icon_rss.png"></a>
    </div>
    <div class="box-news-content">
        <div class="box-style-1">
            <ul class="style">
                <li>
                    <a href="quoc-te/the-gioi-trong-ngay/vu-ep-pha-thai-thang-thu-7-loi-xin-loi-muon-mang-102392.html" title="Vụ ép phá thai tháng thứ 7: Lời xin lỗi muộn màng">
                        <img class="bo img_lazy" src="http://image1.xahoi.com.vn/cnn_132x97/2012/6/16/Me-con2.jpg" alt="Vụ ép phá thai tháng thứ 7: Lời xin lỗi muộn màng" width="132" height="97"/>
                    </a>
                    <a href="quoc-te/the-gioi-trong-ngay/vu-ep-pha-thai-thang-thu-7-loi-xin-loi-muon-mang-102392.html" class="title" title="Vụ ép phá thai tháng thứ 7: Lời xin lỗi muộn màng">Vụ ép phá thai tháng thứ 7: Lời xin lỗi muộn màng</a>
                    <p>
                        Tờ ChinaDaily ngày 15/6 cho hay, đêm 14/6, đại diện chính quyền thành phố An Khang,...
                    </p>
                </li>
                <li>
                    <a href="quoc-te/tu-lieu/su-that-ve-ufo-gay-xon-xao-nuoc-my-102352.html" title="Sự thật về UFO gây xôn xao nước Mỹ ">
                        <img class="bo img_lazy" src="http://image1.xahoi.com.vn/cnn_132x97/2012/6/15/UFO2.jpg" alt="Sự thật về UFO gây xôn xao nước Mỹ " width="132" height="97"/>
                    </a>
                    <a href="quoc-te/tu-lieu/su-that-ve-ufo-gay-xon-xao-nuoc-my-102352.html" class="title" title="Sự thật về UFO gây xôn xao nước Mỹ ">Sự thật về UFO gây xôn xao nước Mỹ </a>
                    <p>
                        Hàng trăm người ở Washington DC đã bị sốc khi nhìn thấy những gì trông giống...
                    </p>
                </li>
            </ul>
            <div class="news-other">
                <h3>Các tin khác</h3>
                <ul class="others">
                    <li class="first"><a href="quoc-te/the-gioi-trong-ngay/thanh-nien-tre-nhat-nuoc-my-trung-xo-so-doc-dac-102085.html" title="Thanh niên trẻ nhất nước Mỹ trúng xổ số độc đắc">Thanh niên trẻ nhất nước Mỹ trúng xổ số độc đắc</a></li>
                    <li><a href="quoc-te/the-gioi-trong-ngay/phat-tu-bac-si-cay-ky-uc-gia-vao-dau-benh-nhan-102049.html" title="Phạt tù bác sĩ cấy ký ức giả vào đầu bệnh nhân">Phạt tù bác sĩ cấy ký ức giả vào đầu bệnh nhân</a></li>
                    <li><a href="quoc-te/video/video-loc-xoay-kinh-hoang-tan-cong-venice-102032.html" title="Video lốc xoáy kinh hoàng tấn công Venice ">Video lốc xoáy kinh hoàng tấn công Venice </a></li>
                    <li><a href="quoc-te/the-gioi-trong-ngay/vu-khoa-than-nhai-dau-nguoi-soc-voi-khuon-mat-bien-dang-101977.html" title="Vụ khỏa thân nhai đầu người: Sốc với khuôn mặt biến dạng ">Vụ khỏa thân nhai đầu người: Sốc với khuôn mặt biến dạng </a></li>
                    <li><a href="quoc-te/the-gioi-do-day/doi-pho-csgt-bang-bang-ve-sinh-101944.html" title="Đối phó CSGT bằng... băng vệ sinh">Đối phó CSGT bằng... băng vệ sinh</a></li>
                </ul>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
</div>
<!-- END QUOC-TE-->
<div class="box-news">
    <div class="box-news-head">
        <h2><span><a href="du-lich-kham-pha/">Du lịch - khám phá</a></span></h2>
        <p>
            <a href="du-lich-kham-pha/danh-thang/" title="Danh thắng">Danh thắng</a> | <a href="du-lich-kham-pha/tour-du-lich/" title="Tour du lịch">Tour du lịch</a> | <a href="du-lich-kham-pha/cam-nang-du-lich/" title="Cẩm nang du lịch">Cẩm nang du lịch</a> | <a href="du-lich-kham-pha/video/" title="Video">Video</a>
        </p>
        <a href="du-lich.rss" class="rss"><img src="images/icon_rss.png"></a>
    </div>
    <div class="box-news-content home-du-lich">
        <div class="box-style-5">
            <div class="slider">
                <ul>
                    <li>
                        <a href="du-lich-kham-pha/danh-thang/hucachina-thien-duong-nho-o-peru-102367.html" title="Hucachina - thiên đường nhỏ ở Peru"><img class="bo" src="http://image1.xahoi.com.vn/news/2012/6/15/Hucachina.jpg" alt="Hucachina - thiên đường nhỏ ở Peru" width="309" height="227"/></a>
                        <p>
                            <a href="du-lich-kham-pha/danh-thang/hucachina-thien-duong-nho-o-peru-102367.html" class="title" title="Hucachina - thiên đường nhỏ ở Peru">Hucachina - thiên đường nhỏ ở Peru</a>
                        </p>
                    </li>
                </ul>
            </div>
            <div class="news-other">
                <ul class="others">
                    <li class="first">
                        <a href="du-lich-kham-pha/danh-thang/ngam-canh-quan-ngoan-muc-cua-tho-nhi-ky-tu-khinh-khi-cau-102138.html" title="Ngắm cảnh quan ngoạn mục của Thổ Nhĩ Kỳ từ khinh khí cầu"><img class="bo img_lazy" src="http://image1.xahoi.com.vn/v_63x63/2012/6/14/du-lich-tho-nhi-ky_v.jpg" alt="Ngắm cảnh quan ngoạn mục của Thổ Nhĩ Kỳ từ khinh khí cầu" width="63" height="63"/></a>
                        <a href="du-lich-kham-pha/danh-thang/ngam-canh-quan-ngoan-muc-cua-tho-nhi-ky-tu-khinh-khi-cau-102138.html" class="title2" title="Ngắm cảnh quan ngoạn mục của Thổ Nhĩ Kỳ từ khinh khí cầu">Ngắm cảnh quan ngoạn mục của Thổ Nhĩ Kỳ từ khinh khí cầu</a>
                    </li>
                    <li>
                        <a href="du-lich-kham-pha/danh-thang/den-na-uy-nho-ghe-tham-con-duong-dung-tim-102068.html" title="Đến Na Uy, nhớ ghé thăm con đường &quot;đứng tim&quot;"><img class="bo img_lazy" src="http://image1.xahoi.com.vn/v_63x63/2012/6/13/duong-dai-tay-duong_v.jpg" alt="Đến Na Uy, nhớ ghé thăm con đường &quot;đứng tim&quot;" width="63" height="63"/></a>
                        <a href="du-lich-kham-pha/danh-thang/den-na-uy-nho-ghe-tham-con-duong-dung-tim-102068.html" class="title2" title="Đến Na Uy, nhớ ghé thăm con đường &quot;đứng tim&quot;">Đến Na Uy, nhớ ghé thăm con đường "đứng tim"</a>
                    </li>
                    <li>
                        <a href="du-lich-kham-pha/danh-thang/sung-so-truoc-ve-hoang-da-guatemala-102034.html" title="Sững sờ trước vẻ hoang dã Guatemala"><img class="bo img_lazy" src="http://image1.xahoi.com.vn/v_63x63/2012/6/13/sung-so-truoc-ve-hoang-da-guatemala-v.jpg" alt="Sững sờ trước vẻ hoang dã Guatemala" width="63" height="63"/></a>
                        <a href="du-lich-kham-pha/danh-thang/sung-so-truoc-ve-hoang-da-guatemala-102034.html" class="title2" title="Sững sờ trước vẻ hoang dã Guatemala">Sững sờ trước vẻ hoang dã Guatemala</a>
                    </li>
                </ul>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
</div>
<div class="box-news">
    <div class="box-news-head">
        <h2><span><a href="ban-tre-doi-song/">Bạn trẻ - đời sống</a></span></h2>
        <p>
            <a href="ban-tre-doi-song/tam-su/" title="Tâm sự">Tâm sự</a> | <a href="ban-tre-doi-song/tinh-yeu-gioi-tinh/" title="Tình yêu - giới tính">Tình yêu - giới tính</a> | <a href="ban-tre-doi-song/ngoai-tinh/" title="Ngoại tình">Ngoại tình</a> | <a href="ban-tre-doi-song/guong-tre/" title="Gương trẻ">Gương trẻ</a>
        </p>
        <a href="ban-tre-cuoc-song.rss" class="rss"><img src="images/icon_rss.png"></a>
    </div>
    <div class="box-news-content home-cuoc-song">
        <div class="box-style-2">
            <ul class="style">
                <li>
                    <a href="ban-tre-doi-song/tam-su/se-la-nguoi-mang-lai-hanh-phuc-cho-em-102365.html" title="Sẽ là người mang lại hạnh phúc cho em...">
                        <img class="bo img_lazy" src="http://image1.xahoi.com.vn/cnn_132x97/2012/6/15/tinh-yeu3.jpg" alt="Sẽ là người mang lại hạnh phúc cho em..." width="132" height="97"/>
                    </a>
                    <a href="ban-tre-doi-song/tam-su/se-la-nguoi-mang-lai-hanh-phuc-cho-em-102365.html" class="title" title="Sẽ là người mang lại hạnh phúc cho em...">Sẽ là người mang lại hạnh phúc cho em...</a>
                    <p>
                        Tôi nghe lòng rạo rực tiếng chim ca, đưa em đi dạo phố, tóc em bay trong gió, mùi quỳnh hương thoang thoảng...
                    </p>
                </li>
            </ul>
            <div class="news-other">
                <div class="news-more">
                    <a href="ban-tre-doi-song/tinh-yeu-gioi-tinh/10-tip-yeu-ban-nen-thu-mot-lan-trong-doi-p2-102332.html" title="10 tip "yêu" bạn nên thử một lần trong đời (p2)">
                       <img class="bo img_lazy" src="http://image1.xahoi.com.vn/v_63x63/2012/6/14/10-tip-sex_v.jpg" alt="10 tip "yêu" bạn nên thử một lần trong đời (p2)" width="90" height="66"/>
                    </a>
                    <a href="ban-tre-doi-song/tinh-yeu-gioi-tinh/10-tip-yeu-ban-nen-thu-mot-lan-trong-doi-p2-102332.html" class="title2" title="10 tip "yêu" bạn nên thử một lần trong đời (p2)">10 tip &quot;yêu&quot; bạn nên thử một lần trong đời (P2)</a>
                    <div class="clear">
                    </div>
                </div>
                <ul class="others">
                    <li class="first"><a href="ban-tre-doi-song/guong-tre/chang-trai-pha-che-tai-ba-nhat-viet-nam-2012-102204.html" title="Chàng trai pha chế tài ba nhất Việt Nam 2012">Chàng trai pha chế tài ba nhất Việt Nam 2012</a></li>
                </ul>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
</div>
<!-- BEGIN AN-NINH -->
<div class="box-news">
    <div class="box-news-head">
        <h2><span><a href="an-ninh-hinh-su/">An ninh hình sự</a></span></h2>
        <p>
            <a href="an-ninh-hinh-su/tin-113/" title="Tin 113">Tin 113</a> | <a href="an-ninh-hinh-su/ky-an/" title="Kỳ án">Kỳ án</a> | <a href="an-ninh-hinh-su/trong-an/" title="Trọng án">Trọng án</a> | <a href="an-ninh-hinh-su/ho-so-mat/" title="Hồ sơ mật">Hồ sơ mật</a> | <a href="an-ninh-hinh-su/toa-an-luong-tam/" title="Tòa án lương tâm">Tòa án lương tâm</a>
        </p>
        <a href="an-ninh.rss" class="rss"><img src="images/icon_rss.png"></a>
    </div>
    <div class="box-news-content home-giai-tri">
        <div class="box-style-7">
            <ul class="style">
                <li>
                    <a href="an-ninh-hinh-su/tin-113/bat-coc-nu-tiep-vien-vi-nghi-trom-dien-thoai-102366.html" title="Bắt cóc nữ tiếp viên vì nghi trộm điện thoại"><img src="http://image1.xahoi.com.vn/news/2012/6/15/trom-dien-thoai.jpg" class="bo img_lazy" width="309" height="227"/></a>
                    <a href="an-ninh-hinh-su/tin-113/bat-coc-nu-tiep-vien-vi-nghi-trom-dien-thoai-102366.html" title="Bắt cóc nữ tiếp viên vì nghi trộm điện thoại" class="title image">Bắt cóc nữ tiếp viên vì nghi trộm điện thoại</a>
                    <p>
                        Do nghi ngờ nữ tiếp viên quán gội đầu lấy điện thoại di động, một lái xe taxi được 3 người bạn hỗ trợ bắt cóc nữ tiếp viên này đưa đi tra hỏi. Cả bốn kẻ bắt giữ người trái pháp luật trên đã bị Viện Kiểm sát nhân dân thành phố Bến Tre khởi tố.
                    </p>
                </li>
            </ul>
            <div class="news-other">
                <ul>
                    <li class="first">
                        <a href="an-ninh-hinh-su/tin-113/nghi-an-bac-sy-xam-hai-be-3-tuoi-da-co-ket-luan-dieu-tra-102307.html" title="Nghi án bác sỹ xâm hại bé 3 tuổi: Đã có kết luận điều tra"><img class="bo img_lazy" src="http://image1.xahoi.com.vn/v_63x63/2012/6/15/hiep-dam22_v.jpg" alt="Nghi án bác sỹ xâm hại bé 3 tuổi: Đã có kết luận điều tra" width="63" height="63"/></a>
                        <a href="an-ninh-hinh-su/tin-113/nghi-an-bac-sy-xam-hai-be-3-tuoi-da-co-ket-luan-dieu-tra-102307.html" class="title2" title="Nghi án bác sỹ xâm hại bé 3 tuổi: Đã có kết luận điều tra">Nghi án bác sỹ xâm hại bé 3 tuổi: Đã có kết luận điều tra</a>
                    </li>
                    <li>
                        <a href="an-ninh-hinh-su/trong-an/dua-me-con-nguoi-rung-di-giam-dinh-y-khoa-102297.html" title="Đưa mẹ con "người rừng" đi giám định y khoa"><img class="bo img_lazy" src="http://image1.xahoi.com.vn/v_63x63/2012/6/15/nguoi-rung21_v.jpg" alt="Đưa mẹ con "người rừng" đi giám định y khoa" width="63" height="63"/></a>
                        <a href="an-ninh-hinh-su/trong-an/dua-me-con-nguoi-rung-di-giam-dinh-y-khoa-102297.html" class="title2" title="Đưa mẹ con "người rừng" đi giám định y khoa">Đưa mẹ con "người rừng" đi giám định y khoa</a>
                    </li>
                    <li>
                        <a href="an-ninh-hinh-su/tin-113/euro-go-cua-va-lang-que-suc-soi-ca-do-102273.html" title="Euro gõ cửa và làng quê sục sôi... cá độ"><img class="bo img_lazy" src="http://image1.xahoi.com.vn/v_63x63/2012/6/15/ca-do2_v.jpg" alt="Euro gõ cửa và làng quê sục sôi... cá độ" width="63" height="63"/></a>
                        <a href="an-ninh-hinh-su/tin-113/euro-go-cua-va-lang-que-suc-soi-ca-do-102273.html" class="title2" title="Euro gõ cửa và làng quê sục sôi... cá độ">Euro gõ cửa và làng quê sục sôi... cá độ</a>
                    </li>
                    <li>
                        <a href="an-ninh-hinh-su/tin-113/gia-con-nuoi-lanh-dao-cong-an-tinh-de-lua-dao-102230.html" title="Giả con nuôi lãnh đạo công an tỉnh để lừa đảo"><img class="bo img_lazy" src="http://image1.xahoi.com.vn/v_63x63/2012/6/14/lua-dao_v.jpg" alt="Giả con nuôi lãnh đạo công an tỉnh để lừa đảo" width="63" height="63"/></a>
                        <a href="an-ninh-hinh-su/tin-113/gia-con-nuoi-lanh-dao-cong-an-tinh-de-lua-dao-102230.html" class="title2" title="Giả con nuôi lãnh đạo công an tỉnh để lừa đảo">Giả con nuôi lãnh đạo công an tỉnh để lừa đảo</a>
                    </li>
                </ul>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
</div>
<!-- END  AN-NINH -->
<script type="text/javascript">
    window.onload = function(){
        $( '.u-others-items' ).scrollable({ vertical: true, keyboard: false });	
        $( '.box-news-moto-ds' ).scrollable();
    }
</script>