(function($){
    $.fn.tcScrollPaging = function(options) {

        // Default parameters
        var defaults = {
            heightOffset: 0,
            pageUrl: null,
            pageOffset: 0,
            scrollTarget: window,
            scrollUrl: '.comment-url',
            scrollItem: '.comment-item',
            scrollItems: '.comment-items',
            scrollLoading: '.comment-load', 
            scrollOffset: '.comment-offset',
            scrollFinish: '.comment-finish',            
            scrollFinishMsg: ''            
        };
        options = $.extend(defaults, options);

        var target = options.scrollTarget;
        if (target == null){
            target = obj;
        }
        options.scrollTarget = target;

        return this.each(function() {
            init($(this), options);
        });

        function init(obj, options){
            $(options.scrollTarget).scroll(function(){
                if (!($(obj).hasClass('dataLoading'))){
                    loadContent(obj, options);
                }
            });

            loadContent(obj, options);
        }

        function loadContent(obj, options){            
            var target = options.scrollTarget;
            var checkLoad = $(target).scrollTop() + options.heightOffset >= $(document).height() - $(target).height();
            if (checkLoad && !($(obj).hasClass('dataLoading'))){
                if ($(options.scrollOffset).length > 0){
                    options.pageOffset = $(options.scrollOffset).last().attr('rel');
                }
                $.ajax({
                    type: 'POST',
                    //url: options.pageUrl,
                    url: $(options.scrollUrl).last().attr('rel'),
                    data: {
                        offset: options.pageOffset
                        //YII_CSRF_TOKEN: YII_CSRF_TOKEN
                    },
                    beforeSend: function(){
                        $(obj).addClass('dataLoading');
                        $(options.scrollLoading).fadeIn('fast');
                    },
                    success: function(data){
                        $(options.scrollLoading).hide();

                        if (data.length == 0) {
                            $(obj).addClass('dataLoading');
                            $(options.scrollFinish).text(options.scrollFinishMsg).fadeIn(500);
                        } else {
                            data = $(data);
                            $(data).find(options.scrollItem).each(function(){
                                $(options.scrollItems).append($(this));
                                $(this).hide().fadeIn(500);
                            })
                            
                            $(options.scrollItems).append($(data).find(options.scrollUrl));
                            $(options.scrollUrl).first().remove();
                            
                            $(options.scrollItems).append($(data).find(options.scrollOffset));
                            $(options.scrollOffset).first().remove();
                            
                            $(obj).removeClass('dataLoading');
                        }
                    }
                });
            }
        }        
    };
})($);
