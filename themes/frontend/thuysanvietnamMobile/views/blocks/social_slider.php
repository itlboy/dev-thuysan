<?php
$cs = Yii::app()->getClientScript();
$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/ct_slider.css?ver=10.231');
$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.tools.min.js', CClientScript::POS_END);
?>
<div id="social-slider">
    <a title="Tin phía sau" class="back prev browse left">Tin phía sau</a>
    <div class="social-slider-items">
        <ul style="left: -928px; ">
            <li>
                <?php $this->widget('article.widgets.frontend.promotion', array('view' => 'promotion_3', 'limit' => 12, 'category' => !empty($category) ? $category : NULL)); ?>
            </li>
            <li>
                <?php $this->widget('article.widgets.frontend.promotion', array('view' => 'promotion_4', 'limit' => 12, 'category' => !empty($category) ? $category : NULL)); ?>
            </li>
        </ul>
    </div>
    <a title="Tin tiếp theo" class="next browse right">Tin tiếp theo</a>
</div>
<script type="text/javascript">
    window.onload = function(){
        $( '.social-slider-items' ).scrollable({ circular: true, speed: 600 });
    };

</script>