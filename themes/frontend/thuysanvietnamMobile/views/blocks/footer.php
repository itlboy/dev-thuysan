<div>
    <?php
    $this->widget('ads.widgets.frontend.ads', array(
        'category' => 70,
    )); 
    ?>
</div>
<div class="footer-warp">
    <div class="footer-nav">
        <div class="ft-logo">
        </div>
        <div class="ft-menu">
<!--            <a href="#" rel="nofollow" title="Trở lên đầu trang" class="gotop">Lên đầu trang</a>
            <a href="javascript:setHomepage()" rel="nofollow" title="Đặt làm trang chủ" class="sethome2">Đặt làm trang chủ</a>-->
            <p>
                <?php $this->widget('category.widgets.frontend.treeCategory', array('module' => 'article', 'promotion' => '1', 'view' => 'treeCategory_footer')); ?>
            </p>
            <!--
            <div class="ft-submenu">
                            <a href="tu-van/" title="Tư vấn - dịch vụ">Tư vấn - dịch vụ</a> | <a href="bat-dong-san/" title="Bất động sản">Bất động sản</a> | <a href="giao-duc-du-hoc/" title="Giáo dục du học">Giáo dục du học</a> | <a href="du-lich-kham-pha/" title="Du lịch - khám phá">Du lịch - khám phá</a> | <a href="o-to-xe-may/" title="Ô tô - Xe máy">Ô tô - Xe máy</a> | <a href="cntt-vien-thong/" title="CNTT - Viễn thông">CNTT - Viễn thông</a> | <a href="suc-khoe/" title="Sức khỏe">Sức khỏe</a> | <a href="tin-khuyen-mai/" title="Tin khuyến mãi">Tin khuyến mãi</a> | <a href="tuong-tac-ban-doc/" title="Tương tác bạn đọc">Tương tác bạn đọc</a> | <a href="giai-tri/thu-gian/" title="Thư giãn">Thư giãn</a>
                        </div>
            -->
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="ft-col">
        <div class="ft-col-1">
            <?php $this->renderPartial('//blocks/support_online'); ?>
        </div>
        <div class="ft-col-2" style="margin-top: 20px">
            <p>
                Giấy phép số 32/GP-TTĐT của Bộ thông tin và Truyền thông cấp ngày 16/04/2009
            </p>
            <p>
                <strong>Cơ quan chủ quản :</strong> Hội Nghề cá Việt Nam
            </p>
            <p>
                <strong>Cơ quan xuất bản :</strong> Tạp chí Thủy sản Việt Nam
            </p>
            <p>
                <b>Tổng biên tập </b>: Nhà báo <strong>Dương Xuân Hùng</strong>
            </p>
            <p>
                <span class="hotline"><strong>Đường dây nóng:</strong> (84.4) 37713699</span>
            </p>
            <p style="margin-top: 10px;">
                <span class="mail"><strong>Liên hệ tòa soạn </strong>: <a rel="nofollow" href="mailto:toasoan@thuysanvietnam.com.vn" title="Gửi thư tới toasoan@thuysanvietnam.com.vn">toasoan@thuysanvietnam.com.vn</a></span>
            </p>
            <p>
                <strong>Trụ sở:</strong> Phòng 303, tầng 3, nhà A7, số 10 Nguyễn Công Hoan, Ba Đình, Hà Nội
            </p>
            <p>
                <strong>Điện thoại:</strong> (84.4) 37713699; (84.4) 37713638; Fax: (84.4) 37711756
            </p>
            <p style="margin-right: 20px">
                <strong>&copy; 2011 Thuysanvietnam.com.vn </strong> giữ bản quyền nội dung trên website này. Ghi rõ nguồn "Tạp chí Thủy sản Việt Nam" khi bạn phát hành lại thông tin từ website này.
            </p>

        </div>
        <div class="clear">
        </div>
    </div>
</div>