<?php
//CVarDumper::dump($data, 10, true);
//CVarDumper::dump($user,10,true);
//CVarDumper::dump($arrCategory,10,true);
//CVarDumper::dump($stats,10,true);
//die();
$category = KitCategory::model()->findByPk($arrCategory);
//CVarDumper::dump($category, 10, true);
//die();
$categoryUrl = Yii::app()->createUrl($category->alias . "-c" . $category->id);
$model = new KitCategory();
$model->parent_id = $arrCategory;
$dataProvider = $model->search();
$childCategories = $dataProvider->getData();
//$data = KitArticle::getLastest($arrCategory, 10);
//$data = KitArticle::treatment($data);
//$relateArticles = array_slice($data, 0, 10);
//CVarDumper::dump($relateArticles,10,true);
//die();
$relateArticles = [];
?>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div class="main-body">
    <div class=" title1-wrapper">
        <div class="text-block">
            <a href="<?php echo $categoryUrl ?>">
                <h3><?php echo $category->name ?></h3>
            </a>
        </div>
    </div>
    <?php if ($childCategories): ?>
        <div class="col-xs-1 icon-wrapper">
        </div>
    <?php endif; ?>
</div>
<?php if ($childCategories): ?>
    <div class="mndt-content">
        <?php
        foreach ($childCategories as $childCategory):
            $childUrl = Yii::app()->createUrl($childCategory->alias . "-c" . $childCategory->id);
            ?>
            <div class="col-xs-5 mndt-wrapper1">
                <a href="<?php echo $childUrl ?>"><?php echo $childCategory->name ?></a>
            </div>
        <?php endforeach; ?>
        <br class="clear">
    </div>
<?php endif; ?>
<?php if (count($relateArticles) > 2): ?>
    <div class="content-new">
        <div class="tv-block">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                Indicators 
                Wrapper for slides 
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <?php
                        for ($i = 0; $i <= 1; $i++):
                            $relateArticle = $relateArticles[$i];
                            if ($relateArticle['image']) {
                                $image = Common::getImageUploaded('article/large/' . $relateArticle['image']);
                            } else {
                                $image = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
                            }
                            ?>
                            <div class="col-xs-6 block-img1">
                                <a href="<?php echo $relateArticle['title'] ?>">
                                    <img src="<?php echo $image; ?>">
                                </a>
                                <a href="<?php echo $relateArticle['url'] ?>">
                                    <h2><?php echo $relateArticle['title'] ?></h2>
                                </a>
                            </div>
                        <?php endfor; ?>
                        <div class="col-xs-6 block-img1">
                            <a href="">
                                <img src="http://vnpangasius.com.vn/resource/news/3/47.jpg">
                            </a>
                            <a href="">
                                <h2>Đã có cánh giải cứu cá tra</h2>
                            </a>
                        </div>

                        <div class="carousel-caption">
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-xs-6 block-img1">
                            <a href="">
                                <img src="http://vnpangasius.com.vn/resource/news/3/47.jpg">
                            </a>
                            <a href="">
                                <h2>Đã có cánh giải cứu cá tra</h2>
                            </a>
                        </div>

                        <div class="col-xs-6 block-img1">
                            <a href="">
                                <img src="http://vnpangasius.com.vn/resource/news/3/47.jpg">
                            </a>
                            <a href="">
                                <h2>Đã có cánh giải cứu cá tra</h2>
                            </a>
                        </div>

                        <div class="carousel-caption">
                        </div>
                    </div>
                </div>

                Controls 
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <div class="block-img2">
                    </div>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <div class="block-img3">
                    </div>
                </a>
            </div>
            <br class="clear">
        </div>
    </div>
<?php endif; ?>
<div class="nd-content">
    <h3><?php echo $data['title'] ?></h3>
    <span><?php echo $data['author'] ?></span>
    <h4><?php echo $data['intro'] ?></h4>
    <br class="clear">
    <?php echo $data['content']; ?>
    <div class="fb-like"></div>
    <br class="clear">
</div>
<div class="fom-conten">
    <div class="title-wrapper">
        <h2>Bình luận bài viết</h2>
    </div>
    <div class="forml-wrapper">
        <form>
            <label>Email</label><br>
            <input type="text">

        </form>
        <form>
            <label>Email</label><br>
            <input type="text">

        </form>
        <div class=" form-block">
            <form action="" class="item-form">
                <label>Nội dung liên hệ</label><br>
                <textarea name="message" rows="1"  >
                </textarea>

            </form>
        </div>
        <div class="col-sm-3 col-xs-6 item-iconf2">
            <button >Gửi bình luận</button>
        </div>
    </div>
    <br class="clear">
</div>
<div class="mn-content">
    <?php
    $this->widget('article.widgets.frontend.lastest', array(
        'view' => 'detailNew',
        'title' => 'Các tin mới nhất',
        'category' => NULL,
        'limit' => 3,
    ));
    ?>
</div>
<div class="mn-content">
    <?php
    $this->widget('article.widgets.frontend.lastest', array(
        'view' => 'detailNew',
        'title' => 'Các tin cũ cùng mục',
        'category' => !empty($arrCategory[0]) ? $arrCategory[0] : NULL,
        'limit' => 8,
        'olderId' => $data['id']
    ));
//                        
    ?>
    <br class="clear">
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/main.js" type="text/javascript"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->