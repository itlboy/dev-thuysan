<style>
    #search-content { float: left; width: 668px; border: #ccc 1px solid; min-height: 200px; margin: 0 0 10px }
    .search-item-head { padding: 5px; background: #e1e1e1 }
    .search-items { padding: 0 5px }
    .search-items ul.items li { clear: left; padding: 10px 0; float: left; width: 100%; border-bottom: #c2c0c0 1px dashed }
    .search-items ul.items li img.bo { float: left; margin: 0 10px 0 0; width: 132px; height: 95px }
    .search-items ul.items li p { padding: 10px 0 0 }
    .paging ul { float: right; height: 30px; padding: 15px 5px 15px 0 }
    .paging li { float: left; padding: 0 5px }
    .paging li a { display: block; float: left; border: #999 1px solid; padding: 4px 8px; color: #666; font-size: 11px; font-weight: bold }
    .paging li a.paging-active { background: #a31266; color: #fff }
    .paging li a.p-back { background: #901e78; color: #fff; border-color: #81186b }
    .paging li a.current { color: #993366; border-color: #81186b }
    #search-navbar { width: 300px; float: right; }
</style>
<div id="mainpage">
    <div id="box">
        <?php echo $this->renderPartial('//blocks/header', array('title' => 'Header')); ?>
        <div id="joc-nav">
            <!-- Main navigation -->
            <?php $this->widget('category.widgets.frontend.treeCategory', array('module' => 'article', 'promotion' => '1')); ?>
            <!-- End main navigation -->
            <div class="nav-sub">
            </div>
            <div class="clear">
            </div>
        </div>
        <!-- Start search by tag -->
        <div id="body-content">
            <div class="clear"></div>  
            <script type="application/javascript">
                function checkSubmit(){
                var q=$("#q").val(); 
                if (q == "Tìm kiếm ...") 
                {
                alert("Bạn chưa nhập từ khóa");return 
                false;
                }; 
                window.location.href='http://xahoi.com.vn/search/'+q;
                return false;
                }
            </script>
                <!-- phan noi dung search tag -->
                <?php
                $this->widget('article.widgets.frontend.tags', array(
                    'data' => $data,
                ));
                ?>
                <!-- //phan noi dung search tag -->
                
            <div id="search-navbar">
                <a><img src="data/temp/search.jpg" alt="" width="300px"></a>
            </div>

            <!-- END: Content -->
            <div class="clear"></div>
        </div>
        <!-- End search by tag -->
    </div>    
</div>