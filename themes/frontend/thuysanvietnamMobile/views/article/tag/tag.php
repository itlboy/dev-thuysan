<?php
$cs = Yii::app()->getClientScript();
$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/xahoi.css?ver=10.231');
$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/home.css?ver=10.231');
?>

<div id="body-content">
    <p style="height: 10px;"></p>
    <?php echo $this->renderPartial('//blocks/social_slider', array('title' => 'Social Slider')); ?>
    <p style="height: 10px;"></p>
    <div id="main-box-content">
        <div class="main-cat-contentleft">

            <div class="main-social">
                <?php $this->widget('article.widgets.frontend.search',array(
                    'options' => $data['options'],
                    'limit' => $find['limit_wg'],
                    'new' =>  !empty($find['new']) ? $find['new'] : NULL,
                    'old' => !empty($find['old']) ? $find['old'] : NULL,
                )) ?>
            </div>

        </div>
        <div class="main-cat-contentright">
            <?php
            $this->widget('article.widgets.frontend.promotion', array(
                'view' => 'promotion_5',
                'title' => 'Tin tiêu điểm',
                'limit' => '5',
            ));
            ?>
            <?php
            $this->widget('article.widgets.frontend.lastestViews', array(
                'view' => 'list_style1',
                'title' => 'Đọc nhiều nhất',
                'limit' => 5,
            ));
            ?>
        </div>
    </div>
    <div id="main-navbar">
        <?php
        $this->widget('article.widgets.frontend.lastest', array(
            'category' => 25,
            'view' => 'list_style7',
            'title' => 'Tư vấn tiêu dùng',
            'limit' => 9,
        ));
        ?>
        <p style="height: 10px"></p>
        <div class="ads_style1">
            <?php
            $this->widget('ads.widgets.frontend.ads', array(
                'category' => 66,
                'title' => 'Logo Thương hiệu thủy sản',
            ));
            ?>
        </div>

        <div class="ads_style1">
            <?php
            $this->widget('ads.widgets.frontend.ads', array(
                'category' => 77,
            ));
            ?>
        </div>

    </div>
    <div class="clear"></div>
</div>