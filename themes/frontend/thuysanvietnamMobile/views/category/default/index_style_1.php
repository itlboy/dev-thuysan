<?php
$cs = Yii::app()->getClientScript();
$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/xahoi.css?ver=10.231');
$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/home.css?ver=10.231');
?>
<div id="body-content">
    <?php echo $this->renderPartial('//blocks/category_hotnews', array('title' => 'Tin tức tiêu biểu' ,'category' => $category['id'])); ?>
    <div class="clear"></div>
    <?php echo $this->renderPartial('//blocks/social_slider', array('title' => 'Tin tức tiêu biểu','category' => $category['id'])); ?>
    <div style="width: 980px; padding: 0 0 10px" class="main-adv">
        <p class="flash_980_90" style="height: 70px">
            <embed height="70" align="middle" width="980" src="http://xahoi.com.vn/banner/980x902_xahoi.swf" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" allowscriptaccess="always" wmode="transparent" quality="high">
        </p>
    </div>
    <script type="text/javascript">
        window.onload = function(){   
            $( '.social-slider-items' ).scrollable({ circular: true, speed: 600 });
        };

    </script>
    <div id="main-box-content">
        <div class="main-social">
            <?php $this->widget('article.widgets.frontend.listByCat', array('category' => $category['id'], 'view' => 'listByCat','limit' => 15)); ?>
        </div>
        <div class="main-cat-contentright">
            <?php
            $this->widget('article.widgets.frontend.lastestViews', array(
                'category' => $category['id'],
                'view' => 'list_style1',
                'title' => 'Đọc nhiều nhất',
                'limit' => 5,
            ));
            ?>
<!--            <div class="main-adv">
                <p>Vị trí quảng cáo</p>
            </div>       -->
            <p style="height: 10px;"></p>
            <?php
            $this->widget('article.widgets.frontend.promotion', array(
                'category' => $category['id'],
                'view' => 'promotion_5',
                'title' => 'Tin tiêu điểm',
                'limit' => 5,
            ));
            ?>
            <div class="main-adv">
                <p style="margin-top: 0px;">
                    Vị trí quảng cáo
                </p>
            </div>
        </div>
    </div>
    <div id="main-navbar">
        <!-- Tin anh -->
            <?php
            $this->widget('article.widgets.frontend.lastest', array(
                'category' => $category['id'],
                'view' => 'list_style6',
                'title' => 'Tin chuyên mục',
                'limit' => 5,
            ));
            ?>
        <!-- END: Tin anh --> 
        <!-- Tin anh -->       


    </div>
    <div class="clear"></div>
</div>