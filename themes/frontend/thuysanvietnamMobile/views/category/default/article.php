<?php
Yii::import('application.modules.article.models.KitArticle');
$data = KitArticle::getLastest($details['id'], 11);
$data = KitArticle::treatment($data);
if ($data[0]['image']) {
    $image = Common::getImageUploaded('article/large/' . $data[0]['image']);
} else {
    $image = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
}
?>
<div class="main-body">
    <div class=" text-wrapper10">
        <a href="<?php echo $details['url'] ?>">
            <h3><?php echo $details['name'] ?></h3>
        </a>
    </div>
    <div class="col-xs-1 icon-wrapper">
    </div>
    <br class="clear">
</div>
<div class="mndt-content">
    <?php foreach ($category_child as $child): ?>
        <div class="col-xs-5 mndt-wrapper1">
            <a href="<?php echo $child['url'] ?>"><?php echo $child['name'] ?></a>
        </div>
    <?php endforeach; ?>
    <br class="clear">
</div>
<!--<div class="main-advertisement">
    <img src="">
</div>-->
<div class="main-bd">
    <a href="<?php echo $data[0]['url'] ?>"><img src="<?php echo $image ?>"></a>
    <div class="title1-block">
        <a href="<?php echo $data[0]['url'] ?>"><?php echo $data[0]['title'] ?></a>
    </div>
    <span><?php echo date('d/m/Y', strtotime($data[0]['create_time'])) ?></span>
    <p> 
        <?php echo $data[0]['intro'] ?>    
    </p>
</div>
<?php array_shift($data); ?>
<div class="content-new">
    <div class="tv-block">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <?php
                    for ($i = 0; $i <= 1; $i++):
                        if ($data[$i]['image']) {
                            $image = Common::getImageUploaded('article/large/' . $data[$i]['image']);
                        } else {
                            $image = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
                        }
                        ?>
                        <div class="col-xs-6 block-img1">
                            <a href="<?php echo $data[$i]['url'] ?>">
                                <img src="<?php echo $image ?>">
                            </a>
                            <a href="<?php echo $data[$i]['url'] ?>">
                                <h2><?php echo $data[$i]['title'] ?></h2>
                            </a>
                        </div>
                    <?php endfor; ?>
                    <?php unset($data[0]); ?>
                    <?php unset($data[1]); ?>
                    <div class="carousel-caption">
                    </div>
                </div>
                <?php
                $i = 0;
                foreach ($data as $item):
                    if ($i == 2) {
                        break;
                    }
                    ?>
                    <?php if ($i % 2 == 0): ?>
                        <div class="item">
                        <?php endif; ?>
                        <?php
                        if ($item['image']) {
                            $image = Common::getImageUploaded('article/large/' . $item['image']);
                        } else {
                            $image = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
                        }
                        ?>
                        <div class="col-xs-6 block-img1">
                            <a href="<?php echo $item['url'] ?>">
                                <img src="<?php echo $image ?>">
                            </a>
                            <a href="<?php echo $item['url'] ?>">
                                <h2><?php echo $item['title'] ?></h2>
                            </a>
                        </div>
                        <?php if ($i % 2 == 1): ?>
                            <div class="carousel-caption">
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php $i++ ?>
                <?php endforeach; ?>
                <?php if ($i % 2 == 0): ?>
                    <div class="carousel-caption">
                    </div>
                </div>
            <?php endif; ?>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <div class="block-img2">
            </div>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <div class="block-img3">
            </div>
        </a>
    </div>
    <br class="clear">
</div>
<!--</div>-->
<?php
$i = 2;
while ($i < count($data)):
    if ($data[$i]['image']) {
        $image = Common::getImageUploaded('article/large/' . $data[$i]['image']);
    } else {
        $image = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
    }
    ?>
    <div class="content-nd">
        <div class="col-xs-6 img1-block">
            <img src="<?php echo $image ?>">
        </div>
        <div class="col-xs-6 tex-block">
            <div class="title-item1">
                <a href="<?php echo $data[$i]['url'] ?>"><?php echo $data[$i]['title'] ?></a>
            </div>
            <span><?php echo date('d/m/Y', strtotime($data[$i]['create_time'])) ?></span>
            <p>
                <?php echo $data[$i]['intro'] ?>
            </p>
        </div>
        <br class="clear">
    </div>
    <?php
    $i++;
endwhile;
?>
<!--<div class="lk-content">
    <a class="load-more" href="">Xem tin cũ hơn</a>
</div>-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/main.js" type="text/javascript"></script>