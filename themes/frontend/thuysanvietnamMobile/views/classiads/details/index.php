<?php
$cs = Yii::app()->getClientScript();
$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/chitiet.css?ver=10.231');
//echo '<pre>';
//print_r($viewcount);
//die;
?>
<!-- Start of detail content page -->
<div id="body-ct">
<div id="ct-header">
    <?php if (isset($breadcrumb[0])): ?>
    <!-- ly do ko hien thi ??? -->
    <div id="ct-breadcrumbs">
        <?php foreach ($breadcrumb[0] as $key => $value): ?>
        <?php if ($key == 0): ?>
                <div class="main-path" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                    <a class="path-a" href="<?php echo $value['url']; ?>" itemprop="url" title="<?php echo $value['name']; ?>"><?php echo $value['name']; ?></a>
                    <?php else: ?>
                    <a class="last" href="<?php echo $value['url']; ?>" itemprop="url" title="<?php echo $value['name']; ?>"><strong itemprop="title"><?php echo $value['name']; ?></strong></a>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
    <?php echo $this->renderPartial('//blocks/social_slider', array('title' => 'Social Slider')); ?>
</div>
<!-- Content -->
<div class="bar-classiads">
    <a href="<?php echo Yii::app()->createUrl('/classiads/default') ?>">
        <span class="lb">Rao vặt thủy sản</span>
    </a>
    <?php if(!empty($category['name'])): ?>
        <a href="<?php echo !empty($category['url']) ? $category['url'] : 'javascript:;' ?>">
            <span class="lb"><?php echo !empty($category['name']) ? $category['name'] : ''; ?></span>
        </a>
    <?php endif; ?>
    <div class="bar-classiads-up">
        <?php
        echo $raovat = (Yii::app()->user->isGuest) ?
            '<a href="'.Yii::app()->createUrl('classiads/create').'">Đăng rao vặt</a>
            <a href="'.Yii::app()->CreateUrl('account/register') .'">Đăng ký</a>'
            : '<a href="'.Yii::app()->createUrl('classiads/create').'">Đăng rao vặt</a>' ;
        ?>
    </div>
</div>
<div id="ct-content" style="width: 741px;" >
    <div class="ct-entry">
            <?php
                $this->widget('classiads.widgets.frontend.classiads_promotion',array(
                    'limit' => 7,
                    'category' => !empty($category['id']) ? $category['id'] : NULL,
                ));
            ?>
        <h1 style="font-size: 20px;font-weight: bold; color: #d62800; margin-bottom: 10px;"><?php echo $data['title']; ?></h1>
            <div align="" class="check_login" style="width:97%;padding: 0px" >
                <table cellpadding="0" cellspacing="0" class="tbl-classiads-fp">
                    <tr>
                        <td valign="top">
                            <?php $day = Yii::t('global', date('l', strtotime ($data["created_time"]))); ?>
                            <span class="ct-time-public" style="color: #666;font-size: 11px;padding: 0px;margin-bottom: 5px">
                                <strong>Đăng lúc :</strong> <?php echo $day.','.date('d/m/Y H:i:s', strtotime ($data["created_time"])) . ' GMT+7'; ?>
                            </span>
                            <span class="ct-time-public" style="color: #666;font-size: 11px;padding: 0px;margin-bottom: 5px">
                                <strong>Người đăng : </strong> <?php echo !empty($user->username) ? $user->username : 'TSVN'; ?>
                            </span>
<!--
                            <span class="ct-time-public" style="color: #666;font-size: 11px;padding: 0px;margin-bottom: 5px">
                                <strong>Đã xem : </strong> <?php echo (!empty($viewcount->attributes['view_total']))? $viewcount->attributes['view_total'] : 0; ?>
                            </span>
-->
                        </td>
                        <td valign="top">
                            <span class="ct-time-public" style="color: #666;font-size: 11px;padding: 0px;margin-bottom: 5px">
                                    <strong>Email :</strong> <a style="color: #006e97;text-decoration: underline" title="Gửi thư tới <?php echo !empty($user->email) ? $user->username : 'toasoan@thuysanvietnam.com.vn' ?>" href="mailto:<?php echo !empty($user->email) ? $user->email : 'toasoan@thuysanvietnam.com.vn' ?>" rel="nofollow"><?php echo !empty($user->email) ? $user->email : 'toasoan@thuysanvietnam.com.vn' ?></a>
                            </span>
                            <span class="ct-time-public" style="color: #666;font-size: 11px;padding: 0px;margin-bottom: 5px">
                                    <strong>Điện thoại : </strong> <strong style="color: red"><?php echo !empty($data['phone'])? $data['phone']:'';  ?></strong></span>
                            <span class="ct-time-public">
                                    <div class="addthis_toolbox addthis_default_style " style="">
                                        <a class="addthis_button_preferred_1"></a>
                                        <a class="addthis_button_preferred_2"></a>
                                        <a class="addthis_button_preferred_3"></a>
                                        <a class="addthis_button_preferred_4"></a>
                                        <a class="addthis_button_preferred_5"></a>
                                        <a class="addthis_button_preferred_6"></a>
                                        <a class="addthis_button_preferred_7"></a>
                                        <a class="addthis_button_preferred_8"></a>
                                        <a class="addthis_button_preferred_9"></a>
                                        <a class="addthis_button_compact"></a>
                                        <a class="addthis_counter addthis_bubble_style"></a>
                                    </div>
                            </span>
                        </td>
                    </tr>
                </table>
            </div>
        <p style="height: 5px;"></p>
        <h2 style="color: #f20600;font-size: 15px;padding: 0px !important;">Nội dung :</h2>
        <p style="border-bottom: 1px solid #cccccc;margin-bottom:5px ;padding: 0px;"></p>
        <?php echo $data['content']; ?>
        <?php
            $this->widget('classiads.widgets.frontend.classiads_lastest',array(
                'limit' => 20,
                'view' => 'classiads_lastest_style_2',
                'title' => 'Các tin cùng chuyên mục ',
                'category' => !empty($category['id']) ? $category['id'] : NULL,
            ));
        ?>
    </div>
    <div class="clear"></div>
</div>
<!-- END: Content -->

<div id="ct-navbar" style="width: 230px;height: auto>
    /*<style>*/
        /*.ct-qc p{*/
            /*display:block;*/
            /*padding:3px 5px 3px 5px;*/
            /*float:left;*/
            /*width: 45%;*/
        /*}>*/
    /*</style*/
        <?php
        $this->widget('ads.widgets.frontend.ads', array(
            'category' => 91,
        )); //Main_Top_Ads
        ?>
<!--    <div class="ct-qc">
<!--        <div class="clear"></div>-->
<!--    </div>-->

    <!--                <div class="ct-qc">
        <img alt="" src="webskins/skins/news/images/ct/chitiet_xahoi_new_23.jpg">
    </div>-->



</div>

<div class="clear">
</div>
</div>

<!-- End of detail content page -->
<!-- Script AddThis -->
<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4dd945b77241e40c"></script>
<!-- Script Button END -->