<div class="comments-section ">
    <div class="ct-widget-header">
        <h4><span><a>Bình luận bài viết</a></span></h4>
    </div>
    <div class="section-comm">
        <?php $this->widget('application.modules.comment.widgets.WidgetComment', array(
            'module' => 'classiads',
            'itemId' => $data['id'],
            'limit' => 10,
            'requireUser' => false,
            'view' => 'WidgetComment_thuysan',
        )); ?>
    </div>
</div>