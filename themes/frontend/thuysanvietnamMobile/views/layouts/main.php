<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo isset($this->pageTitle) ? CHtml::encode($this->pageTitle) : Yii::app()->name; ?></title>
        <meta name="description" content="Kênh thông tin nhanh nhất được cập nhật liên tục về lĩnh vực thủy sản, nuôi trồng thủy sản, khai thác thủy sản, khoa học, công nghệ thủy sản ở Việt Nam, Thế giới. Cung cấp thông tin, dịch vụ về kỹ thuật nông nghiệp và thủy sản cho nông dân và các nhà đầu tư ở Việt Nam. Aquaculture & Fishing. Nuôi trồng, khai thác thủy hải sản. Kỹ thuật nuôi tôm, cá & các loại thủy hải sản khác..."/>
        <meta http-equiv="REFRESH" content="1800"/>

        <!-- Bootstrap -->
        <link rel="shortcut icon" href="favicon.ico"/>
        <link href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <link href="<?php echo Yii::app()->theme->baseUrl ?>/assets/css/index.css" rel="stylesheet" type="text/css"/>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->
    </head>
    <body>
        <?php include "menu.php"; ?>
        <div class="container-fluid than">
            <div class="row">
                <div class="container-fluid main-logo">
                    <div class="menu-wrapper">

                    </div>
                    <div class="col-xs-7 logo-wrapper">
                        <a href="">
                            <img src="<?php echo Yii::app()->theme->baseUrl ?>/assets/images/Untitled-2.png">
                        </a>
                    </div>
                    <a href="">
                        <div class=" logo2-wrapper">

                        </div>
                    </a>
                </div>
                <?php echo $content ?>
                <?php include 'footer.php'; ?>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/main.js" type="text/javascript"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
    </body>
</html>