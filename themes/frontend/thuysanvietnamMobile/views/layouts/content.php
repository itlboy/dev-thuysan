<div class="row">
    <div class="container-fluid main-logo">

        <div class="menu-wrapper">

        </div>
        <div class="col-xs-7 logo-wrapper">
            <a href="">
                <img src="<?php echo Yii::app()->theme->baseUrl ?>/assets/images/Untitled-2.png">
            </a>
        </div>
        <a href="">
            <div class=" logo2-wrapper">

            </div>
        </a>
    </div>
    <div class="main-body">
        <div class="text-wrapper">
            <a href="">
                <h3>Trang chủ</h3>
            </a>
        </div>
    </div>
    <div class="main-heard">
        <div class="opacity-block">
            <h2 class="title clearfix">
                <a href="" title="" class="inner" data-newstype="0" data-linktype="newsdetail" data-id="20151010093342419">Hà Nội: Giáo viên đánh, tát, lay giật đầu bé 16 tháng tuổi</a>
            </h2>
            <a href="" title="">
                <figure><img title="" alt="" src="http://tefco.edu.vn/uploads/news/2013_07/che-bien-thuy-san.jpg" width="100%" height="auto">

                </figure>
            </a>
        </div>

    </div>
    <div class="main-text">
        <p>Kỹ thuật nuôi cá tầm trong lồng bè trên hồ chứa: Cá tầm sống trong môi trường nước lạnh, sạch và oxy hòa tan cao. Nhiệt độ nước phù hợp cho sự sinh trưởng và phát triển của cá tầm từ 18 - 27 độ C...</p>
    </div>
    <div class="body-wrapper">
        <div class="logo-block">
            <h4>Tin Mới</h4>
        </div>
        <div class="br-block">
        </div>
        <div class="menu-des">
            <ul class="ulhomefocus1">
                <li>

                    <a href="" title="">
                        Hồ Ngọc Hà lên tiếng bênh vực khi Mỹ Tâm bị chê "hết thời"
                    </a>

                </li>
                <li>

                    <a href="" title="">
                        Không nên xem khi ăn: Cận cảnh quá trình tiêu hóa của con người
                    </a>

                </li>
                <li>
                    <a href="" title="">
                        Hồ Ngọc Hà lên tiếng bênh vực khi Mỹ Tâm bị chê "hết thời"
                    </a>

                </li>
                <li>

                    <a href="" title="">
                        Không nên xem khi ăn: Cận cảnh quá trình tiêu hóa của con người
                    </a>

                </li>

                <li>

                    <a href="" title="">
                        Cảnh ùn tắc khủng khiếp tại Bắc Kinh sau kỳ nghỉ lễ quốc khánh
                    </a>
                </li>
            </ul>
        </div>
        <div class="tdc-wrapper">
            <a href="">Xem thêm</a>
        </div>
        <br class="clear">
    </div>
    <div class="content-new">
        <div class="main-tv">
            <div class="title-block">

                <h4><a href="">Tin tức - Sự kiện</a></h4>

            </div>
        </div>
        <div class="tv-block">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="col-xs-6 block-img1">
                            <a href="">
                                <img src="http://vnpangasius.com.vn/resource/news/3/47.jpg">
                            </a>
                            <a href="">
                                <h2>Đã có cánh giải cứu cá tra</h2>
                            </a>
                        </div>

                        <div class="col-xs-6 block-img1">
                            <a href="">
                                <img src="http://vnpangasius.com.vn/resource/news/3/47.jpg">
                            </a>
                            <a href="">
                                <h2>Đã có cánh giải cứu cá tra</h2>
                            </a>
                        </div>

                        <div class="carousel-caption">
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-xs-6 block-img1">
                            <a href="">
                                <img src="http://vnpangasius.com.vn/resource/news/3/47.jpg">
                            </a>
                            <a href="">
                                <h2>Đã có cánh giải cứu cá tra</h2>
                            </a>
                        </div>

                        <div class="col-xs-6 block-img1">
                            <a href="">
                                <img src="http://vnpangasius.com.vn/resource/news/3/47.jpg">
                            </a>
                            <a href="">
                                <h2>Đã có cánh giải cứu cá tra</h2>
                            </a>
                        </div>

                        <div class="carousel-caption">
                        </div>
                    </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <div class="block-img2">
                    </div>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <div class="block-img3">
                    </div>
                </a>
            </div>


            <br class="clear">
        </div>
    </div>
    <div class="main-adv">
        <img src="http://image.slidesharecdn.com/bquytlmnhqungco-140828055439-phpapp01/95/10-b-quyt-lm-nh-qung-co-facebook-1-638.jpg?cb=1409205831">
    </div>
    <div class="main-detail">
        <div class="title-block">
            <h4><a href="">Tin tức - Sự kiện</a></h4>
            <a href="">
                <div class="item-logo3"></div>
            </a>
        </div>
        <div class="bd-block">
            <a href="">
                <h5>Sử đổi nghị định số 36</h5>
            </a>
            <div class="article-wrapper">
                <div class="col-xs-6 image-item">
                    <img src="http://www.fistenet.gov.vn/f-thuong-mai-thuy-san/a-xuat-nhap-khau/xuat-khau-thuy-san-giam-manh-nhat-trong-vong-5-nam-qua/image">
                </div>
                <div class="col-xs-6 text-item">
                    <p> Our plans include unlimited texting, calling, and data, starting as low as $18.99 per month with no contracts. We even have a free,</p>
                </div>
            </div>
            <div class="menu-des">
                <ul class="ulhomefocus1">

                    <li>

                        <a href="" title="">
                            Hồ Ngọc Hà lên tiếng bênh vực khi Mỹ Tâm bị chê "hết thời"
                        </a>

                    </li>

                    <li>

                        <a href="" title="">
                            Không nên xem khi ăn: Cận cảnh quá trình tiêu hóa của con người
                        </a>

                    </li>
                    <li>

                        <a href="" title="">
                            Hồ Ngọc Hà lên tiếng bênh vực khi Mỹ Tâm bị chê "hết thời"
                        </a>

                    </li>

                    <li>

                        <a href="" title="">
                            Không nên xem khi ăn: Cận cảnh quá trình tiêu hóa của con người
                        </a>

                    </li>

                    <li>

                        <a href="" title="">
                            Cảnh ùn tắc khủng khiếp tại Bắc Kinh sau kỳ nghỉ lễ quốc khánh
                        </a>

                    </li>
                    <li href="" class="">
                        <a href="">
                            Xem thêm tin tức sự kiện
                        </a>
                    </li>
                </ul>
            </div>
            <div class="tdc-wrapper">
                <a href="">Xem thêm</a>
            </div>
        </div>
    </div>
    <div class="main-detail">
        <div class="title-block">
            <h4><a href="">Tin tức - Sự kiện</a></h4>
            <a href="">
                <div class="item-logo3"></div>
            </a>
        </div>
        <div class="bd-block">
            <a href="">
                <h5>Sử đổi nghị định số 36</h5>
            </a>
            <div class="article-wrapper">
                <div class="col-xs-6 image-item">
                    <img src="http://www.fistenet.gov.vn/f-thuong-mai-thuy-san/a-xuat-nhap-khau/xuat-khau-thuy-san-giam-manh-nhat-trong-vong-5-nam-qua/image">
                </div>
                <div class="col-xs-6 text-item">
                    <p> Our plans include unlimited texting, calling, and data, starting as low as $18.99 per month with no contracts. We even have a free,</p>
                </div>
            </div>
            <div class="menu-des">
                <ul class="ulhomefocus1">

                    <li>

                        <a href="" title="">
                            Hồ Ngọc Hà lên tiếng bênh vực khi Mỹ Tâm bị chê "hết thời"
                        </a>

                    </li>

                    <li>

                        <a href="" title="">
                            Không nên xem khi ăn: Cận cảnh quá trình tiêu hóa của con người
                        </a>

                    </li>

                    <li>

                        <a href="" title="">
                            Cảnh ùn tắc khủng khiếp tại Bắc Kinh sau kỳ nghỉ lễ quốc khánh
                        </a>

                    </li>
                    <li>
                        <a href="" href="">
                            Xem thêm tin tức sự kiện
                        </a>
                    </li>
                </ul>

            </div>
            <div class="tdc-wrapper">
                <a href="">Xem thêm</a>
            </div>
        </div>
    </div>
    <div class="main-detail">
        <div class="title-block">
            <h4><a href="">Tin tức - Sự kiện</a></h4>
            <a href="">
                <div class="item-logo3"></div>
            </a>
        </div>
        <div class="bd-block">
            <a href="">
                <h5>Sử đổi nghị định số 36</h5>
            </a>
            <div class="article-wrapper">
                <div class="col-xs-6 image-item">
                    <img src="http://www.fistenet.gov.vn/f-thuong-mai-thuy-san/a-xuat-nhap-khau/xuat-khau-thuy-san-giam-manh-nhat-trong-vong-5-nam-qua/image">
                </div>
                <div class="col-xs-6 text-item">
                    <p> Our plans include unlimited texting, calling, and data, starting as low as $18.99 per month with no contracts. We even have a free,</p>
                </div>
            </div>
            <div class="menu-des">
                <ul class="ulhomefocus1">

                    <li>

                        <a href="" title="">
                            Hồ Ngọc Hà lên tiếng bênh vực khi Mỹ Tâm bị chê "hết thời"
                        </a>

                    </li>

                    <li>

                        <a href="" title="">
                            Không nên xem khi ăn: Cận cảnh quá trình tiêu hóa của con người
                        </a>

                    </li>

                    <li>

                        <a href="" title="">
                            Cảnh ùn tắc khủng khiếp tại Bắc Kinh sau kỳ nghỉ lễ quốc khánh
                        </a>

                    </li>
                    <li >
                        <a href="" href="">
                            Xem thêm tin tức sự kiện
                        </a>
                    </li>
                </ul>

            </div>
            <div class="tdc-wrapper">
                <a href="">Xem thêm</a>
            </div>
        </div>
    </div>
    <div class="main-detail">
        <div class="title-block">
            <h4><a href="">Tin tức - Sự kiện</a></h4>
            <a href="">
                <div class="item-logo3"></div>
            </a>
        </div>
        <div class="bd-block">
            <a href="">
                <h5>Sử đổi nghị định số 36</h5>
            </a>
            <div class="article-wrapper">
                <div class="col-xs-6 image-item">
                    <img src="http://www.fistenet.gov.vn/f-thuong-mai-thuy-san/a-xuat-nhap-khau/xuat-khau-thuy-san-giam-manh-nhat-trong-vong-5-nam-qua/image">
                </div>
                <div class="col-xs-6 text-item">
                    <p> Our plans include unlimited texting, calling, and data, starting as low as $18.99 per month with no contracts. We even have a</p>
                </div>
            </div>
            <div class="menu-des">
                <ul class="ulhomefocus1">

                    <li>

                        <a href="" title="">
                            Hồ Ngọc Hà lên tiếng bênh vực khi Mỹ Tâm bị chê "hết thời"
                        </a>

                    </li>

                    <li>

                        <a href="" title="">
                            Không nên xem khi ăn: Cận cảnh quá trình tiêu hóa của con người
                        </a>

                    </li>

                    <li>

                        <a href="" title="">
                            Cảnh ùn tắc khủng khiếp tại Bắc Kinh sau kỳ nghỉ lễ quốc khánh
                        </a>
                    </li>
                    <li class="after-item">
                        <a href="" href="">
                            Xem thêm tin tức sự kiện
                        </a>
                    </li>
                </ul>

            </div>
            <div class="tdc-wrapper1">
                <a href="">Xem thêm</a>
            </div>
        </div>
    </div>
    <div class="main-detail">
        <div class="title-block">
            <h4><a href="">Tin tức - Sự kiện </a></h4>
            <a href="">
                <div class="item-logo3"></div>
            </a>
        </div>
        <div class="bd-block">
            <a href="">
                <h5>Sử đổi nghị định số 36</h5>
            </a>
            <div class="article-wrapper">
                <div class="col-xs-6 image-item">
                    <img src="http://www.fistenet.gov.vn/f-thuong-mai-thuy-san/a-xuat-nhap-khau/xuat-khau-thuy-san-giam-manh-nhat-trong-vong-5-nam-qua/image">
                </div>
                <div class="col-xs-6 text-item">
                    <p> Our plans include unlimited texting, calling, and data, starting as low as $18.99 </p>
                </div>
            </div>
            <div class="menu-des">
                <ul class="ulhomefocus1">

                    <li>

                        <a href="" title="">
                            Hồ Ngọc Hà lên tiếng bênh vực khi Mỹ Tâm bị chê "hết thời"
                        </a>

                    </li>

                    <li>

                        <a href="" title="">
                            Không nên xem khi ăn: Cận cảnh quá trình tiêu hóa của con người
                        </a>

                    </li>

                    <li>

                        <a href="" title="">
                            Cảnh ùn tắc khủng khiếp tại Bắc Kinh sau kỳ nghỉ lễ quốc khánh
                        </a>

                    </li>
                    <li>
                        <a href="" href="">
                            Xem thêm tin tức sự kiện
                        </a>
                    </li>
                </ul>

            </div>
            <div class="tdc-wrapper">
                <a href="">Xem thêm</a>
            </div>
        </div>
    </div>
    <div class="main2-detail">
        <div class="title-block">
            <h4><a href="">Phóng sự</a></h4>
            <a href="">
                <div class="item-logo3"></div>
            </a>
        </div>
        <div class="heard-block">
            <div class="opacity-block">
                <h2 class="title1 clearfix">
                    <a href="" title="" class="inner" data-newstype="0" data-linktype="newsdetail" data-id="20151010093342419">Hà Nội: Giáo viên đánh, tát, lay giật đầu bé 16 tháng tuổi</a>
                </h2>
                <a href="" title="">
                    <figure><img title="" alt="" src="http://tefco.edu.vn/uploads/news/2013_07/che-bien-thuy-san.jpg" width="410" height="300">

                    </figure>
                </a>
            </div>

        </div>
        <div class="chitiet-block">
            <div class="col-xs-3 images-item">
                <img src="http://ipsard.gov.vn/images/2012/05/115_11_thuy-san_jpg.jpg">
            </div>
            <div class="col-xs-9 text2-item">
                <a href="">
                    <h5>Cá tra tiêu thụ nội địa được giá</h5>
                </a>
            </div>
            <br class="clear">
        </div>
        <div class="chitiet-block">
            <div class="col-xs-3 images-item">
                <img src="http://ipsard.gov.vn/images/2012/05/115_11_thuy-san_jpg.jpg">
            </div>
            <div class="col-xs-9 text2-item">
                <a href="">
                    <h5>Cá tra tiêu thụ nội địa được giá</h5>
                </a>
            </div>
            <br class="clear">
        </div>
        <div class="chitiet-block">
            <div class="col-xs-3 images-item">
                <img src="http://ipsard.gov.vn/images/2012/05/115_11_thuy-san_jpg.jpg">
            </div>
            <div class="col-xs-9 text2-item">
                <a href="">
                    <h5>Cá tra tiêu thụ nội địa được giá</h5>
                </a>
            </div>
            <br class="clear">
        </div>
        <div class="chitiet-block">
            <div class="col-xs-3 images-item">
                <img src="http://ipsard.gov.vn/images/2012/05/115_11_thuy-san_jpg.jpg">
            </div>
            <div class="col-xs-9 text2-item">
                <a href="">
                    <h5>Cá tra tiêu thụ nội địa được giá</h5>
                </a>
            </div>
            <br class="clear">
        </div>
        <div class="tdc-wrapper1">
            <a href="">Xem thêm</a>
        </div>
    </div>
    <div class="main3-detail">
        <div class="title-block">
            <h4><a href="">Tin tức - Sự kiện</a></h4>
            <a href="">
                <div class="item-logo3"></div>
            </a>
        </div>
        <div class="bd-block">
            <a href="">
                <h5>Sử đổi nghị định số 36</h5>
            </a>
            <div class="article-wrapper">
                <div class="col-xs-6 image-item">
                    <img src="http://www.fistenet.gov.vn/f-thuong-mai-thuy-san/a-xuat-nhap-khau/xuat-khau-thuy-san-giam-manh-nhat-trong-vong-5-nam-qua/image">
                </div>
                <div class="col-xs-6 text-item">
                    <p> Our plans include unlimited texting, calling, and data, starting as low as </p>
                </div>
            </div>
            <div class="menu-des">
                <ul class="ulhomefocus1">

                    <li >
                        <a  href="">

                            Hồ Ngọc Hà lên tiếng bênh vực khi Mỹ Tâm bị chê "hết thời"
                        </a>

                    </li>

                    <li>

                        <a href="" title="">
                            Không nên xem khi ăn: Cận cảnh quá trình tiêu hóa của con người
                        </a>

                    </li>

                    <li>

                        <a href="" title="">
                            Cảnh ùn tắc khủng khiếp tại Bắc Kinh sau kỳ nghỉ lễ quốc khánh
                        </a>

                    </li>
                    <li>
                        <a href="">
                            Xem thêm tin tức sự kiện
                        </a>
                    </li>
                </ul>

            </div>
            <div class="tdc-wrapper">
                <a href="">Xem thêm</a>
            </div>
        </div>
    </div>
    <div class="th-content">
        <div id="carousel-example" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->


            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <div class="img-slide">
                        <h2>Ấn phẩm đã xuất bản</h2>
                        <div class="gh-wrapper">
                        </div>
                        <div class="col-xs-6 img-wrapper">
                            <img src="">
                        </div>
                        <div class="title-footer">
                            <h3>Tra và basa số 31</h3>
                            <p>Số ra tháng 20 năm 2015</p>
                        </div>
                    </div>
                    <div class="carousel-caption">
                    </div>
                </div>
                <div class="item">
                    <div class="img-slide">
                        <h2>Ấn phẩm đã xuất bản</h2>
                        <div class="gh-wrapper">
                        </div>
                        <div class="col-xs-6 img-wrapper">
                            <img src="">
                        </div>
                        <div class="title-footer">
                            <h3>Tra và basa số 31</h3>
                            <p>Số ra tháng 20 năm 2015</p>
                        </div>
                    </div>
                    <div class="carousel-caption">
                    </div>
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example" role="button" data-slide="prev">
                <div class="icon-left">
                    <div class="item-icon1">

                    </div>
                </div>
            </a>
            <a class="right carousel-control" href="#carousel-example" role="button" data-slide="next">
                <div class="icon-right">
                    <div class="item-icon2">

                    </div>
                </div>
            </a>
        </div>
        <!--sau slide -->




        <br class="clear">
    </div>
    <div class="main3-detail">
        <div class="title-block">
            <h4><a href="">Tin tức - Sự kiện</a></h4>
            <a href="">
                <div class="item-logo3"></div>
            </a>
        </div>
        <div class="bd-block">
            <a href="">
                <h5>Sử đổi nghị định số 36</h5>
            </a>
            <div class="article-wrapper">
                <div class="col-xs-6 image-item">
                    <img src="http://www.fistenet.gov.vn/f-thuong-mai-thuy-san/a-xuat-nhap-khau/xuat-khau-thuy-san-giam-manh-nhat-trong-vong-5-nam-qua/image">
                </div>
                <div class="col-xs-6 text-item">
                    <p> Our plans include unlimited texting, calling, and data, starting as low as </p>
                </div>
            </div>
            <div class="menu-des">
                <ul class="ulhomefocus1">

                    <li>

                        <a href="" title="">
                            Hồ Ngọc Hà lên tiếng bênh vực khi Mỹ Tâm bị chê "hết thời"
                        </a>

                    </li>

                    <li>

                        <a href="" title="">
                            Không nên xem khi ăn: Cận cảnh quá trình tiêu hóa của con người
                        </a>

                    </li>

                    <li>

                        <a href="" title="">
                            Cảnh ùn tắc khủng khiếp tại Bắc Kinh sau kỳ nghỉ lễ quốc khánh
                        </a>

                    </li>
                    <li>
                        <a href="">
                            Xem thêm tin tức sự kiện
                        </a>
                    </li>
                </ul>

            </div>
            <div class="tdc-wrapper">
                <a href="">Xem thêm</a>
            </div>
        </div>
    </div>
    <div class="main-tv5">
        <div class="title-block">

            <h4><a href="">Tin tức - Sự kiện</a></h4>

        </div>
        <div class="tv-block">
            <div id="carousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <div class="col-xs-6 block-img1">
                            <a href="">
                                <img src="http://vnpangasius.com.vn/resource/news/3/47.jpg">
                            </a>
                            <a href="">
                                <h2>Đã có cánh giải cứu cá tra</h2>
                            </a>
                        </div>

                        <div class="col-xs-6 block-img1">
                            <a href="">
                                <img src="http://vnpangasius.com.vn/resource/news/3/47.jpg">
                            </a>
                            <a href="">
                                <h2>Đã có cánh giải cứu cá tra</h2>
                            </a>
                        </div>

                        <div class="carousel-caption">
                        </div>
                    </div>
                    <div class="item">
                        <div class="col-xs-6 block-img1">
                            <a href="">
                                <img src="http://vnpangasius.com.vn/resource/news/3/47.jpg">
                            </a>
                            <a href="">
                                <h2>Đã có cánh giải cứu cá tra</h2>
                            </a>
                        </div>

                        <div class="col-xs-6 block-img1">
                            <a href="">
                                <img src="http://vnpangasius.com.vn/resource/news/3/47.jpg">
                            </a>
                            <a href="">
                                <h2>Đã có cánh giải cứu cá tra</h2>
                            </a>
                        </div>

                        <div class="carousel-caption">
                        </div>
                    </div>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
                    <div class="block-img2">
                    </div>
                </a>
                <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
                    <div class="block-img3">
                    </div>
                </a>
            </div>


        </div>

    </div>
    <div class="main4-detail">
        <div class="title-block">
            <h4>Tin tức - Sự kiện</h4>
        </div>
        <div class="vd-block">
            <iframe src="https://www.youtube.com/embed/WtvhrC2TLsE" frameborder="0" allowfullscreen></iframe>
            <div class="video-title">
                <a href="">
                    <h4>Thủy sản đắc lộc - nhân lực rẻ</h4>
                </a>
            </div>
            <div class="menu-des3">
                <ul class="ulhomefocus1">

                    <li>

                        <a href="" title="">
                            Hồ Ngọc Hà lên tiếng bênh vực khi Mỹ Tâm bị chê "hết thời"
                        </a>

                    </li>

                    <li>

                        <a href="" title="">
                            Không nên xem khi ăn: Cận cảnh quá trình tiêu hóa của con người
                        </a>

                    </li>

                    <li>

                        <a href="" title="">
                            Cảnh ùn tắc khủng khiếp tại Bắc Kinh sau kỳ nghỉ lễ quốc khánh
                        </a>

                    </li>

                </ul>

            </div>
            <div class="col-xs-12 tdc-wrapper3">
                <a href="">Xem thêm</a>
            </div>
        </div>
        <br class="clear">
    </div>
    <div class="footer">
        <div class="gt-block">
            <div class="col-xs-6 item-gt">
                <ul>
                    <li>
                        <a href="">Thủy sản việt nam</a>
                    </li>
                    <li>
                        <a href="">Thủy sản việt nam</a>
                    </li>
                    <li>
                        <a href="">Thủy sản việt nam</a>
                    </li>
                    <li>
                        <a href="">Thủy sản việt nam</a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-6 item-gt">
                <ul>
                    <li>
                        <a href="">Thủy sản việt nam</a>
                    </li>
                    <li>
                        <a href="">Thủy sản việt nam</a>
                    </li>
                    <li>
                        <a href="">Thủy sản việt nam</a>
                    </li>
                    <li>
                        <a href="">Thủy sản việt nam</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="lk-block">
            <div class=" item-lk">
                <div class="col-xs-2 lk-thumbnail icon">
                    <a href="">
                    </a>
                </div>
                <div class="col-xs-2 lk-thumbnail2 icon">
                    <a href="">
                    </a>
                </div>
                <div class="col-xs-2 lk-thumbnail3 icon">
                    <a href="">
                    </a>
                </div>
                <div class="col-xs-2 lk-thumbnail4 icon">
                    <a href="">
                    </a>
                </div>
                <div class="col-xs-2 lk-thumbnail5 icon">
                    <a href="">
                    </a>
                </div>
                <div class="col-xs-2 lk-thumbnail6 icon">
                    <a href="">
                    </a>
                </div>
            </div>
        </div>
        <div class="footer-block">
            <p>&nbsp; &nbsp; &nbsp;<b>&copy; 2001 Thuysanvietnam.com.vn</b> giữ bản quyền nội dung trên webside này <br>
                Ghi rõ nguồn "Tạp chí Thủy Sản Việt Nam" khi bạn phát hành lại thông tin từ trang này
            </p>
        </div>
    </div>
</div>