<?php foreach ($data['cat'] as $cat): ?>
    <?php
//    if (!in_array($cat['id'], array(10, 100))):
        $articles = $data['article'][$cat['id']];
        if ($articles[0]['image']) {
            $image = Common::getImageUploaded('article/large/' . $articles[0]['image']);
        } else {
            $image = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
        }
//        CVarDumper::dump($data['article'][$cat['id']], 10, true);
//        die();
        ?>
        <div class="main-detail">
            <div class="title-block">
                <h4><a href="<?php echo $cat['url']; ?>"><?php echo $cat['name']; ?></a></h4>
                <a href="">
                    <div class="item-logo3"></div>
                </a>
            </div>
            <div class="bd-block">
                <a href="<?php echo $articles[0]['url'] ?>">
                    <h5><?php echo $articles[0]['title'] ?></h5>
                </a>
                <div class="article-wrapper">
                    <div class="col-xs-6 image-item">
                        <img src="<?php echo $image ?>">
                    </div>
                    <div class="col-xs-6 text-item">
                        <p>
                            <?php if (!empty($articles[0]['has_brand']) AND $articles[0]['has_brand'] == 1): ?>
                                (Thủy sản Việt Nam) -
                            <?php endif; ?>
                            <?php $intro = preg_replace('/\(Thủy(.*?)\)\ -/i', '', $articles[0]['intro']); ?>
                            <?php echo letText::limit_chars($intro, 250); ?>
                        </p>
                    </div>
                </div>
                <div class="menu-des">
                    <ul class="ulhomefocus1">
                        <?php
                        array_shift($articles);
                        foreach ($articles as $article):
                            ?>
                            <li>
                                <a href="<?php echo $article['url'] ?>" title="<?php echo $article['title'] ?>">
                                    <?php echo $article['title'] ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="tdc-wrapper">
                    <a href="<?php echo $cat['url']; ?>">Xem thêm</a>
                </div>
            </div>
        </div>
        <?php
//    endif;
endforeach;
?>