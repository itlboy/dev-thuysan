<div class="cr-classiads" style="padding-top: 10px">
    <div class="cr-classiads-l" style="border: 0;background:none">
        <p class="tt-register">Đăng kí thành viên</p>
        <?php
          $form = $this->beginWidget('CActiveForm',array('id' => 'registerForm'));
        ?>
        <table cellpadding="0" cellspacing="0" class="tbl-register">
            <tr>
                <td colspan="2">
                    <div class="bug-reg">
                    <?php echo CHtml::errorSummary($model); ?>
                       <center><strong style="color: red;">
                    <?php
                        if(Yii::app()->user->hasFlash('error')){
                            echo Yii::app()->user->getFlash('error');
                        }
                    ?>
                        </strong></center>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <p class="tt-tp">Thông tin tài khoản </p>
                </td>
            </tr>
            <tr>
                <td width="200px" align="right">Tài khoản : </td>
                <td><?php echo $form->textField($model,'username',array('class' => 'input-rg')); ?><span class="sp-comf">Tài khoản độ dài tối đa 15 kí tự</span></td>
            </tr>
            <tr>
                <td align="right">Mật khẩu : </td>
                <td><?php echo $form->passwordField($model,'password',array('class' => 'input-rg')); ?><span class="sp-comf">Mật khẩu độ dài tối đa 32 kí tự</span></td>
            </tr>
            <tr>
                <td align="right">Nhập lại mật khẩu : </td>
                <td><?php echo $form->passwordField($model,'password_repeat',array('class' => 'input-rg')); ?></td>
            </tr>
            <tr>
                <td colspan="2">
                    <p class="tt-tp">Thông tin cá nhân </p>
                </td>
            </tr>
            <tr>
                <td align="right">Họ và tên : </td>
                <td><?php echo $form->textField($model,'fullname',array('class' => 'input-rg')); ?></td>
            </tr>
            <tr>
                <td align="right">Email : </td>
                <td><?php echo $form->textField($model,'email',array('class' => 'input-rg')); ?></td>
            </tr>
            <tr>
                <td></td>
                <td valign="top">
                    <input type="text" class="input-rg" style="width: 60px;float: left;margin-right: 5px;" name="KitAccount[captcha]" id="KitAccount_Captcha" />
                    <?php $this->widget('CCaptcha',array(
                    'buttonLabel' => '',
                    'clickableImage' => true,
                    'imageOptions' => array(
                        'style' => 'border:1px solid #ccc;width:53px;cursor:pointer;',
                        'title' => 'Click lấy mã khác',
                        'id' => 'imgCaptcha',
                    ),
                    'captchaAction' => '//account/register/captcha',
                )); ?>
                </td>
            </tr>
            <tr>
                <td align="right"></td>
                <td><?php echo CHtml::submitButton('Đăng ký',array('class' => 'btn-create-classiads')) ?></td>
            </tr>
        </table>
        <?php $this->endWidget(); ?>
    </div>
    <div class="cr-classiads-r" style="width: 290px">
        <div class="regu-ct">
            <p class="regu-tt">Điều khoản</p>
            <div class="ct-regu">
                <p>1. Về tài khoản sử dụng (account): Khi đăng ký tài khoản, bạn nên cung cấp đầy đủ thông tin về tên tuổi, địa chỉ, điện thoại, số CMND… Đây không phải là những thông tin bắt buộc, nhưng khi có những rủi ro, mất mát sau này, chúng tôi chỉ tiếp nhận những trường hợp điền đúng và đầy đủ những thông tin trên. Những trường hợp điền thiếu thông tin hoặc thông tin sai sự thật sẽ không được giải quyết. Những thông tin này sẽ được dùng làm căn cứ để hỗ trợ giải quyết.</p>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
