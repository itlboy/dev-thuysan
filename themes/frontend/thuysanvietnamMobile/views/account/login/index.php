<?php
$client = Yii::app()->clientScript;
$client->registerCssFile(Yii::app()->theme->baseUrl.'/css/loginForm.css');
?>
<div class="loginForm">
    <?php $form = $this->beginWidget('CActiveForm',array()) ;?>
    <table cellpadding="0" cellspacing="0" class="tblLoginForm">
        <tr>
            <td colspan="2" align="center">
                <h1>Đăng nhập</h1>
            </td>
        </tr>
        <tr>
            <td width="110px"><?php echo $form->labelEx($model, 'username') ?></td>
            <td><?php echo $form->textField($model, 'username', array('class' => 'txtformLogin'));  ?></td>
        </tr>
        <tr>
            <td><?php echo $form->labelEx($model, 'password') ?></td>
            <td><?php echo $form->passwordField($model, 'password', array('class' => 'txtformLogin'));  ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo CHtml::submitButton('Đăng nhập', array('class' => 'btnLoginForm')) ?></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <?php if(Yii::app()->user->hasFlash('error')) : ?>
                    <?php echo Yii::app()->user->getFlash('error'); ?>
                <?php endif; ?>
                    <a href="<?php echo Yii::app()->CreateUrl('account/register'); ?>" style="color: red" >+ Đăng ký thành viên</a>
            </td>
        </tr>
    </table>
    <?php $this->endWidget(); ?>
</div>