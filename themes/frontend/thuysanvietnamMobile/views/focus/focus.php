<?php
if ($data[0]['image']) {
    $image = Common::getImageUploaded('article/large/' . $data[0]['image']);
} else {
    $image = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
}
?>
<div class="main-heard">
    <div class="opacity-block">
        <h2 class="title clearfix">
            <a href="<?php echo $data[0]['url']; ?>" title="" class="inner" data-newstype="0" data-linktype="newsdetail" data-id="20151010093342419"><?= $data[0]['title'] ?></a>
        </h2>
        <a href="<?php echo $data[0]['url']; ?>" title="<?= $data[0]['title'] ?>">
            <figure><img title="" alt="" src="<?php echo $image; ?>" width="100%" height="auto">
            </figure>
        </a>
    </div>
</div>
<div class="main-text">
    <?php if (!empty($data[0]['has_brand']) AND $data[0]['has_brand'] == 1): ?>
        (Thủy sản Việt Nam) -
    <?php endif; ?>
    <?php $intro = preg_replace('/\(Thủy(.*?)\)\ -/i', '', $data[0]['intro']); ?>
    <?php echo letText::limit_chars($intro, 250); ?>
</div>