<?php
$category = KitCategory::model()->findByPk($this->category);
$categoryUrl = Yii::app()->createUrl($category->alias . "-c" . $category->id);
?>
<div class="title2-wrapper">
    <a href="<?php echo $categoryUrl ?>"><?php echo $this->title ?></a>
</div>
<div class="menu-block">
    <ul class="ulhomefocus">
        <?php foreach ($data as $key => $value): ?>
            <li>
                <a title="<?php echo $value['title']; ?>" href="<?php echo $value['url']; ?>">
                    <?php echo $value['title']; ?> <span>(<?php echo date('d/m/Y', strtotime($value['created_time'])) ?>)</span>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
<br class="clear">