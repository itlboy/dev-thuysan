<div class="box_s2 relative clearfix maggin_bottom_10" style="padding: 0;border: 10px solid #F6F6F6;">
    <div class="fl maggin_right_10"><a href="<?php echo Yii::app()->createUrl('//wall-'.$this->user_id); ?>"><img src="<?php echo KitAccount::getAvatar($this->user_id, 'large'); ?>" width="150" height="150" alt="<?php echo KitAccount::getField($this->user_id, 'username'); ?>" /></a></div>
    <div class="maggin_top_10" style="color: #888;">
        <?php $userInfo = KitAccount::getDetails($this->user_id); ?>
        <div>Số bài: <?php echo number_format($userInfo['post_total'], 0, ',', '.'); ?></div>
        <div>Like: <?php echo number_format($userInfo['like_total'], 0, ',', '.'); ?></div>
        <div>Share: <?php echo number_format($userInfo['share_total'], 0, ',', '.'); ?></div>
        <div>Xem: <?php echo number_format($userInfo['view_total'], 0, ',', '.'); ?></div>
        <div>Bình luận: <?php echo number_format($userInfo['comment_total'], 0, ',', '.'); ?></div>
        <!--<div>Thứ hạng: <?php echo number_format($userInfo['rank'], 0, ',', '.'); ?></div>-->
        <div style="color: #0000FF">Chú ý: Có thể sẽ mất 1 khoảng thời gian cho việc update thống kê</div>
    </div>
    <h2 class="thumb_title opacity center maggin_top_5"><a href="<?php echo Yii::app()->createUrl('//wall-'.$this->user_id); ?>"><?php echo KitAccount::getField($this->user_id, 'username'); ?></a></h2>
</div>
