<div class="container-narrow">
    <div class="jumbotron">        
        <section id="cnt">
            <?php /*
            <ul class="ftg-filters">
                <li><a class="selected" href="#">All</a></li>
                <li><a href="#set-1">Filter 1</a></li>
                <li><a href="#set-2">Filter 2</a></li>
                <li><a href="#set-3">Filter 3</a></li>
            </ul> */ ?>

            <div class="ftg-items">
                <?php foreach ($rows as $i => $row): ?>
                <article class="tile ftg-set-3">
                    <a rel="gallery[Photo]" href="<?php echo Common::getImageUploaded('pin/origin/' . $row['image']); ?>">
                        <img class="item" src="<?php echo Common::getImageUploaded('pin/origin/' . $row['image']); ?>" />
                        <div class="caption">
                            <p><?php echo $row['title']; ?></p>
                        </div>
                    </a>
                </article>
                <?php endforeach; ?>
            </div>

        </section>
    </div>
</div>
