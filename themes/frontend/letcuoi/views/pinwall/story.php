<div class="container-narrow">
    <div class="jumbotron">
        <section id="cnt">
            <?php /*
            <ul class="ftg-filters">
                <li><a class="selected" href="#">All</a></li>
                <li><a href="#set-1">Filter 1</a></li>
                <li><a href="#set-2">Filter 2</a></li>
                <li><a href="#set-3">Filter 3</a></li>
            </ul> */ ?>

            <div class="ftg-items">

                <?php foreach ($rows as $i => $row): ?>

                <article class="tile ftg-set-3">
                    <a href="<?php echo Common::getImageUploaded('pin/large/' . $row['image']); ?>">
                        <img class="item" src="<?php echo Common::getImageUploaded('pin/large/' . $row['image']); ?>" />
                        <div class="caption">
                            <p><?php echo $row['title']; ?></p>
                        </div>
                    </a>
                </article>
                <?php /*
                <div class="pin-item row stickem-container">
                    <div class="content grid_14 alpha center">
                        <div class="relative">
                            <?php if ($row['type'] == 'video'): ?>
                            <?php
                            Yii::import('application.vendors.libs.grabVideo');
                            $youtubeId = grabVideo::getYoutubeId($row['video']);
                            ?>
                            <a href="<?php echo $row['url']; ?>" target="_blank">
                                <img src="http://img.youtube.com/vi/<?php echo $youtubeId; ?>/0.jpg" alt="<?php echo $row['title']; ?>" />
                                <div class="icon_play2"></div>
                            </a>
                            <?php elseif ($row['type'] == 'image'): ?>
                            <a href="<?php echo $row['url']; ?>" target="_blank">
                                <img src="<?php echo Common::getImageUploaded('pin/large/' . $row['image']); ?>" alt="<?php echo $row['title']; ?>" />
                            </a>
                            <?php else: ?>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="aside stickem grid_6 omega fr">
                        <div class="title"><?php echo CHtml::link($row['title'], $row['url'], array('target' => '_blank')); ?></div>
                        <p>
                            <span class="icon_play"><s title="">&nbsp;</s><em><?php echo number_format($row['view_count'], 0, ',', '.'); ?></em></span>
                            <span class="icon_comment"><s title="">&nbsp;</s><em><?php echo number_format($row['comment_count'], 0, ',', '.'); ?></em></span>
                        </p>
                        <div><?php echo $row['intro']; ?></div>
                        <div class="social_button clearfix maggin_top_15">
                            <a class="facebook"><span></span></a>
                            <a class="twitter"><span></span></a>
                            <a class="googleplus"><span></span></a>
                        </div>
                        <div class="maggin_top_10 likefacebook" style="display: none;">
                            <div class="fb-like" data-href="<?php echo $row['url']; ?>" data-send="true" data-layout="button_count" data-width="620" data-show-faces="false"></div>
                        </div>
                    </div>
                </div> */ ?>
                <?php endforeach; ?>
            </div>

        </section>
    </div>
</div>
