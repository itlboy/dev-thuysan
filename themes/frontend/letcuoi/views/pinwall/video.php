<?php if (Yii::app()->user->id == $this->creator) : ?>
<a style="float:right; margin-bottom:15px; font-weight:bold; font-size:17px" href="<?php echo Yii::app()->createUrl('pin/post/video'); ?>">Đăng video</a>
<?php endif; ?>

<div id="pin-list" class="grid_24 alpha omega list_video_s1">
    <ul class="pin-items list maggin_left_20">
        <?php foreach ($rows as $key => $row): ?>
            <li class="pin-item item clearfix<?php if ($key % 6 == 5): ?> last<?php endif; ?>">
                <div class="thumb mouseover">
                    <a title="<?php echo $row['title']; ?>" href="<?php echo $row['url']; ?>">
                        <?php
                        Yii::import('application.vendors.libs.grabVideo');
                        $youtubeId = grabVideo::getYoutubeId($row['video']);
                        ?>
                        <img alt="<?php echo $row['title']; ?>" src="http://img.youtube.com/vi/<?php echo $youtubeId; ?>/1.jpg" width="130" height="95" />
                    </a>
                </div>
                <div class="item_txtbox">
                    <h6>
                        <a class="item_title" title="<?php echo $row['title']; ?>" href="<?php echo $row['url']; ?>"><?php echo $row['title']; ?></a>
                    </h6>
                    <p>
                        <span class="icon_play"><s title="">&nbsp;</s><em><?php echo number_format($row['view_count'], 0, ',', '.'); ?></em></span>
                        <span class="icon_comment"><s title="">&nbsp;</s><em><?php echo number_format($row['comment_count'], 0, ',', '.'); ?></em></span>
                    </p>
                </div>
            </li>
        <?php endforeach; ?>
        <a href="javascript:;" style="display:none" class="pin-url" rel="<?php echo $url; ?>">URL</a>
        <a href="javascript:;" style="display:none" class="pin-offset" rel="<?php echo $offset; ?>">OFFSET</a>
    </ul>
    
    <div class="pin-load"></div>
    <div class="pin-finish"></div>    
</div>
<div class="clearfix"></div>


