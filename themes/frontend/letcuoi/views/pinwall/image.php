<?php if (Yii::app()->user->id == $this->creator) : ?>
<!--<a style="float:right; margin-bottom:15px; font-weight:bold; font-size:17px" href="<?php echo Yii::app()->createUrl('pin/post/image'); ?>">Đăng ảnh</a>
<div class="clearfix"></div>-->
<?php endif; ?>

<div style="float:right; margin-bottom:15px; font-weight:bold; font-size:12px">
    <a href="<?php echo Yii::app()->createUrl('wall-' . $this->creator .'/image'); ?>?style=1" class="btn <?php if ($this->viewImage == 'image_1') echo 'btn-primary'; ?>"><i class="icon-th"></i> Pin style</a>
    <a href="<?php echo Yii::app()->createUrl('wall-' . $this->creator .'/image'); ?>?style=2" class="btn <?php if ($this->viewImage == 'image_2') echo 'btn-primary'; ?>"><i class="icon-th-large"></i> Grid style</a>

</div>
<div class="clearfix"></div>
<?php
$this->render($this->viewImage, array(
    'rows' => $rows,
    'url' => $url,
    'offset' => $offset,
));
?>
