<?php
Yii::app()->clientScript->registerMetaTag('100001754856259', NULL, NULL, array('property' => 'fb:admins'));
$this->pageTitle = 'Cuộc thi: "Tìm kiếm troller tài năng"';
?>
<div class="grid_24">
    <div class="box_s2 clearfix bg_white padding_20 contest" style="line-height: 20px;">
        <div class="center" style="font-size: 35px; margin: 30px 0; color: #009900;">Cuộc thi "Tìm kiếm troller tài năng"</div>
        <div><i>Cuộc thi nhằm tìm ra những troller xuất sắc cho website <a href="http://cuoi.let.vn">Let Cười</a>. Hãy tham gia ngay và "giựt" TOP!</i></div>
        <div><strong>Thời gian:</strong> Từ 0h00 3/6/2013 – Đến 0h00 3/7/2013</div>
        <div><strong>Đối tượng tham gia:</strong> Tất cả các thành viên ở mọi lứa tuổi đang sống và làm việc tại Việt Nam có khả năng sáng tạo, có khiếu hài hước.</div>
        <div><strong>Hình thức thi:</strong> Các thành viên sẽ tự chế ảnh/video vui cười, hài hước có nội dung lành mạnh đăng lên website <a href="http://cuoi.let.vn">Let Cười</a></div>
        <div><strong>Cơ cấu giải thưởng:</strong></div>
        <div style="color: #009900; font: bold 15px Arial; margin-left: 30px;">
            <p>Troller Top 1: giải thưởng 5.000.000đ</p>
            <p>Troller Top 2: giải thưởng 3.000.000đ</p>
            <p>Troller Top 3: giải thưởng 1.000.000đ</p>
            <p>Troller Top 4: giải thưởng 500.000đ</p>
            <p>Troller Top 5: giải thưởng 500.000đ</p>
        </div>
        <div><strong>Nội dung chi tiết cuộc thi:</strong> 
            <p>Các thành viên đăng ảnh/ video tự chế có nội dung hài hước, vui cười, lành mạnh lên website cuoi.let.vn( có thể đăng nhiều ảnh/video, không giới hạn số lượng). Mỗi thành viên có 1 trang cá nhân riêng trên website cuoi.let.vn, ở bên phải màn hình ghi rõ: số bài đăng, số lượt xem (view), số lượt thích( like), số lượt bình luận (comment), số lượt chia sẻ (share). Những số liệu này, được tự động lấy về từ dữ liệu trên Facebook, vì vậy bảo đảm được tính công bằng cho tất cả các thành viên.</p>
            <p>Đến 0h00’ ngày 3/7/2013, BTC sẽ chốt số điểm mỗi thành viên đạt được tính trên tất cả các ảnh/video mà thành viên đó đã đăng, số điểm này được gọi là <strong>Let Point</strong>, được tính như sau:</p>
            <p class="center"><strong>Let Point = (View x 1) + (Like x 2) + (Comment x 3) + (Share x 4)</strong></p>
            <p><i>Trong đó:</i></p>
            <p><i>- View: là tổng số lượt xem trên các hình ảnh, video bạn đăng lên, phát sinh trong thời gian cuộc thi</i></p>
            <p><i>- Like: là tổng số lượt like (facebook) trên các hình ảnh, video bạn đăng lên, phát sinh trong thời gian cuộc thi</i></p>
            <p><i>- Comment: là tổng số lượt bình luận (facebook) trên các hình ảnh, video bạn đăng lên, phát sinh trong thời gian cuộc thi</i></p>
            <p><i>- Share: là tổng số lượt share trên các hình ảnh, video bạn đăng lên, phát sinh trong thời gian cuộc thi</i></p>
            <p>Vì mọi số liệu được lấy từ facebook nên kết quả là tuyệt đối <strong>chính xác</strong> và <strong>công bằng</strong>.</p>
            <p>Top 5 Thành viên có số Let Point cao nhất sẽ được BTC trao giải vào ngày 4/7/2013. Những Ảnh/Video vi phạm những điều sau, sẽ bị hủy kết quả:</p>
            <p>- Có nội dung không lành mạnh, đồi trụy, không phù hợp thuần phong mỹ tục của Việt Nam.</p>
            <p>- Có dán logo, đường link, PR… cho các website, fanpage khác.</p>
            <p>- <i>Nghiêm cấm các hình thức gian lận. Những người gian lận sẽ bị loại khỏi vòng thi mà không cần báo trước. (Hệ thống sẽ phát hiện nếu như có hành vi gian lận)</i></p>
        </div>
        <div><strong>Trao thưởng:</strong>
            <p>Mỗi thành viên khi tham gia chương trình, đều phải đăng ký số điện thoại với BTC. Sau khi chốt kết quả, vào ngày 4/7/2013 BTC sẽ thông báo trực tiếp với Top 5 Troller thông qua số điện thoại đã đăng ký.</p>
            <p>BTC không chịu trách nhiệm nếu trao thưởng cho chủ nhân số điện thoại đó mà không phải là người tham gia cuộc thi. BTC chỉ chấp nhận 1 số điện thoại duy nhất mà các bạn đã đăng ký khi tham gia chương trình. BTC cũng không trao thưởng nếu số điện thoại các bạn đã đăng ký không tồn tại, hoặc cố gắng mà không thể liên lạc được.</p>
        </div>
        <div><strong>Ban tổ chức:</strong> 
            <p>Đơn vị tổ chức chương trình: Công ty TNHH truyền thông iLet Media</p>
            <p>Website: http://let.vn</p>
            <p>Hotline: 0988.631988 – 0908.631988 (không trực điện thoại ngoài giờ hành chính, ngày nghỉ, lễ)</p>
        </div>
    </div>
    
    <div class="box_s2 clearfix maggin_top_10">
        <div class="fb-comments" data-href="http://cuoi.let.vn/cuoc-thi-tim-kiem-troller-tai-nang.let" data-width="925" data-num-posts="20"></div>
    </div>

</div>
<style>
    body {background: #fff;}
    .contest strong {color: #00F;}
</style>