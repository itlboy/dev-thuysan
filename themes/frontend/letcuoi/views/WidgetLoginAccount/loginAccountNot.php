    <form class="form-inline maggin_bottom_5" method="post" action="<?php echo Yii::app()->createUrl('account/default/loginCuoi'); ?>" id="cuoiLoginForm" onSubmit="js:ajaxLoginCuoi(jQuery(this).attr('id'), jQuery(this).attr('action')); return false;">
        <input type="text" class="input-small" placeholder="Username" name="KitAccount[username]" />
        <input type="password" class="input-small" placeholder="Password" name="KitAccount[password]" />
        <button type="submit" class="btn">Login</button>
    </form>
    <div class="fr">
        <div><a class="maggin_top_5" href="http://id.let.vn/account/default/forgot.let" target="_blank">Quên mật khẩu?</a></div>
        <div><a class="maggin_top_10" href="http://id.let.vn/" target="_blank">Đăng ký</a><br /></div>
    </div>
    <div class="fl">
        <?php $this->widget('ext.hoauth.widgets.HOAuth', array(
            'route' => '//account/frontend/oauth'
        )); ?>
    </div>
<!--<div class="widget-login-wrapper">
    <form method="post" action="<?php echo Yii::app()->createUrl('account/default/loginCuoi'); ?>" id="cuoiLoginForm" onSubmit="js:ajaxLoginCuoi(jQuery(this).attr('id'), jQuery(this).attr('action')); return false;">
        <input type="text" value="" maxlength="32" name="KitAccount[username]" placeholder="Tên truy cập" id="Kit_Acc" class="input-small">
        <input type="password" value="" maxlength="32" name="KitAccount[password]" placeholder="Mật khẩu" id="Kit_Pass" class="input-small">
        <br>
        <input type="checkbox" value="1" id="KitAccount_checkbox" name="KitAccount[checkbox]" style="margin-left:0">
        <label for="KitAccount_checkbox" class="checkbox">Lưu mật khẩu</label>        
        <a style="margin-left: 20px;" href="http://id.let.vn/account/default/forgot.let">Quên mật khẩu?</a>
        <br>
        <button name="login" type="submit" class="btn">Đăng nhập</button>
        <div style="float:right">
            <?php $this->widget('ext.hoauth.widgets.HOAuth', array(
                'route' => '//account/frontend/oauth'
            )); ?>
        </div>
    </form>
</div>-->
<script type="text/javascript">
    $(document).ready(function(){
        $('.login_form .btn').live('click',function(){
            var user = $('#Kit_Acc').val();
            var pass = $('#Kit_Pass').val();
            if(user === '' || user === 'Username' || pass === '' || pass === 'Password'){
                alert('Bạn chưa nhập tài khoản hoặc mật khẩu');
                return false;
            }
        });
    });

    function ajaxLoginCuoi(form, url) {
        jQuery.ajax({
            'url':url,
            'type':'POST',
            'data':jQuery('#'+form).serialize(),
            'dataType':'JSON',
            'beforeSend':function() {

            },
            'success':function(data) {            
                if (data.status === 'success') {
                     jQuery('.login_form').html(data.message);
                } else {
                    alert(data.message);
                }
            }
        });
    }
</script>

