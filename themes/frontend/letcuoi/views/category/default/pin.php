<?php
if ($details['parent_id'] == '0')
    $parent = $details['id'];
else
    $parent = $details['parent_id'];
?>
<style>
    body {background: #fff;}
    /*.tabs_s2 li {float: right;}*/
</style>
<div class="grid_24">
    <!--<h1 class="title padding_bottom_5 maggin_bottom_15" style="border-bottom: 1px solid #D0D0D0; text-transform: uppercase;">Ảnh troll</h1>-->
    <?php $this->widget('category.widgets.frontend.catInParent', array(
        'module' => 'pin',
        'parent_id' => 0,
        'active_id' => $parent,
        'view' => 'catInParent_letcuoi_tab',
    )); ?>
</div>
<div class="grid_24 side_left_bg">

    <div class="grid_4 alpha" id="scroller">
        <!-- Menu left side -->
        <?php $this->widget('category.widgets.frontend.catInParent', array(
            'module' => 'pin',
            'parent_id' => $parent,
            'view' => 'catInParent_letcuoi_leftside',
        )); ?>
    </div>

    <div class="grid_20 omega fr">
        <?php
        $this->widget('application.modules.pin.widgets.frontend.pinlist', array(
            'limit' => 18,
            'category' => $category,
            'request' => TRUE
        ));
        ?>
    </div>
</div>
