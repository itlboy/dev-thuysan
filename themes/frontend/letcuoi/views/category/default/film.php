<?php $options['category'] = $category; ?>

<style>
	body {background: #fff;}
    .list_video_s1 .list .item {margin: 0 30px 20px 0;}
</style>
<div class="grid_24 side_left_bg" style="position:relative;">
    <div class="grid_4 alpha">
        <!-- Menu -->
        <?php $this->widget('category.widgets.frontend.treeCategory', array(
            'module' => 'film',
            'view' => 'treeCategory_letphimNew',
        )); ?>

    </div>
    <div class="grid_20 omega fr">
        <h1 class="title padding_bottom_5 maggin_bottom_15" style="border-bottom: 1px solid #D0D0D0; text-transform: uppercase;">Phim <?php echo $details['name']; ?></h1>
        <div class="box_s2 maggin_bottom_15">
            <?php $urlSearch = Yii::app()->createUrl('film/search/list'); ?>
            <form id="let_search" onsubmit="submitSearch('let_search', '<?php echo $urlSearch; ?>'); return false;" name="let_search">
                <input type="hidden" id="let_search_category" value="<?php echo $options['category']; ?>"  name="category">
                <?php /*
                <div>
                    <input type="radio" name="filter_length" checked="checked" /> Tất cả
                    <input type="radio" name="filter_length" /> Phim bộ
                    <input type="radio" name="filter_length" /> Phim lẻ
                </div>
                <div>
                    <input type="radio" name="filter_hot" checked="checked" /> Tất cả
                    <input type="radio" name="filter_hot" /> Phim HOT
                    <input type="radio" name="filter_hot" /> Phim chiếu rạp
                </div>
                <input type="hidden" id="let_search_category" value="43"  name="category">
                <input type="hidden" id="let_search_promotion" value="0" name="promotion">
                <input type="text" value="" onchange="submitSearch('let_search', '<?php echo $urlSearch; ?>');" name="keyword" placeholder="Nhập từ khóa..." class="input-normal">
                */ ?>
                
                <div>
                    <?php
                    $filmLengthSelect = 'all';
                    $filmLengthList = array(
                        'all' => 'Tất cả',
                        'length_1' => 'Phim bộ',
                        'length_0' => 'Phim lẻ',
                    );
                    foreach ($filmLengthList as $key => $value) {
                        if (isset($options[$key]) AND $options[$key] == '1')
                            $filmLengthSelect = $key;
                    }
                    echo CHtml::radioButtonList('filter_length', $filmLengthSelect, $filmLengthList, array(
                        'separator' => '',
                        'template' => '<span>{input} {label}</span>'));
                    ?>
                </div>

                <div>
                    <?php
                    $filmTypeSelect = 'all';
                    $filmTypeList = array(
                        'all' => 'Tất cả',
                        'hot' => 'Phim HOT',
                        'theater' => 'Phim chiếu rạp',
                    );
                    foreach ($filmTypeList as $key => $value) {
                        if (isset($options[$key]) AND $options[$key] == '1')
                            $filmTypeSelect = $key;
                    }
                    echo CHtml::radioButtonList('filter_type', $filmTypeSelect, $filmTypeList, array(
                        'separator' => '',
                        'template' => '<span>{input} {label}</span>'));
                    ?>                    
                </div>           

                <a href="javascript:;" onclick="js:submitSearch('let_search', '<?php echo $urlSearch; ?>');">[Tìm kiếm]</a>
                <a href="javascript:;" onclick="js:resetSearch('let_search', '<?php echo $urlSearch; ?>');">[Làm lại]</a>
            </form>
        </div>

        <div id="result_viewlist">
            <?php $this->widget('film.widgets.frontend.searchNew', array('options' => $options)); ?>
        </div>

        <?php /*
        <!-- Video list -->
        <div class="list_video_s1">
            <ul class="list">
                <?php for ($i = 0; $i < 20; $i++): ?>
                    <li class="item clearfix<?php if ($i % 5 == 4): ?> last<?php endif; ?>">
                        <div class="thumb mouseover">
                            <a title="" href="details_film.php">
                                <img alt="" src="http://data.let.vn/uploads/film/medium/2013/04/17/bau-troi-tinh-yeu_628814.jpg" width="130" height="160" />
                            </a>
                        </div>
                        <div class="item_txtbox">
                            <h6>
                                <a class="item_title" title="" href="details_film.php">Tân câu chuyện cảnh sát</a>
                            </h6>
                            <p><span><a target="_blank" href="" class="gray">The police</a></span></p>
                            <p>
                                <span class="icon_play"><s title="">&nbsp;</s><em>48,613</em></span>
                                <span class="icon_comment"><s title="">&nbsp;</s><em>19</em></span>
                            </p>
                        </div>
                    </li>
                <?php endfor; ?>
            </ul>
        </div>
        <!-- END Video list -->
        */ ?>

    </div>
</div>


