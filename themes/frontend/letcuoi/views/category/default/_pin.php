<?php $this->renderPartial('//blocks/_topmenu'); ?>
<div id="wrapper" class="BoardLayout">
    <div id="ColumnContainer" class="clearfix">
        <div class="fl" style="width: 650px;">      
            
            <?php $this->widget('application.modules.pin.widgets.frontend.pinlist', array(
				'category' => $category,
                'request' => TRUE
			)); ?>

        </div>
        
        <div class="fr" style="width: 300px;">
            <!-- Nut dang bai -->
            <a class="special-btn green" href="<?php echo $this->createUrl('//pin/wall'); ?>" label="Sidebar">Đăng hình ảnh, clip...</a>
            
            <?php $this->renderPartial('//blocks/_fanpages'); ?>
            
            <?php $this->widget('application.modules.pin.widgets.frontend.pinfilter', array(
				'title' => 'Bài cùng danh mục',
				'order' => 'RAND()',
				'limit' => 10,
				'cache_time' => 60 * 60 * 10, // 10 phút
				'category' => $category,
			)); ?>
        </div>
	</div>
	<!-- #ColumnContainer -->
	<!-- Paginator -->
	<a class="MoreGrid Button WhiteButton Button18" href="?marker=&page=11" style="display:none">See More Pins</a>
	<!-- Infinite scroll loading indicator -->
	<div id="LoadingPins">
		<img src="http://passets-ec.pinterest.com/images/BouncingLoader.gif" alt="Pin Loader Image"/><span>Fetching pins&hellip;</span>
	</div>
</div>
<!-- #wrapper -->

