<div class="grid_16 alpha padding_bottom">
    <div class="box_s1 clearfix border_red bg_white">
        <div class="clearfix">
            <div class="grid_2 alpha title"><?php echo $this->title; ?></div>
        </div>
        <div class="mainContent">
            <!-- Video list -->
            <div class="grid_16 alpha omega list_video_s1">                
                <div id="pin-list">

                    <!-- Video list -->
                    <div class="list_video_s1">
                        <div class="list maggin_left_20">  
                            <div class="pin-items">
                                <?php foreach ($rows as $i => $row): ?>                        
                                    <div class="pin-item item clearfix<?php if ($i % 4 == 3): ?> last<?php endif; ?>">
                                        <div class="thumb mouseover">                                            
                                            <?php if ($row['type'] == 'video'): ?>
                                            <?php
                                            Yii::import('application.vendors.libs.grabVideo');
                                            echo grabVideo::youtubePlayer($row['video'], 120, 90);
                                            ?>
                                            <?php elseif ($row['type'] == 'image'): ?>
                                            <a href="<?php echo $row['url']; ?>" class="PinImage ImgLink" target="_blank">
                                            <img src="<?php echo Common::getImageUploaded('pin/medium/' . $row['image']); ?>" alt="<?php echo $row['title']; ?>" class="PinImageImg" />
                                            </a>
                                            <?php else: ?>
                                            <?php endif; ?>
                                        </div>
                                        <div class="item_txtbox">
                                            <h6>
                                                <a class="item_title" title="" href="<?php echo $row['url']; ?>"><?php echo $row['title']; ?></a>
                                            </h6>
                                            <p><span><a target="_blank" href="<?php echo $row['url']; ?>" class="gray"><?php if (isset($row['title_english']) AND $row['title_english'] !== '' AND $row['title_english'] !== $row['title']) echo '(' . $row['title_english'] . ')'; ?></a></span></p>
                                            <p>
                                                <span class="icon_play"><s title="">&nbsp;</s><em><?php echo $row['view_count'];?></em></span>
                                                <span class="icon_comment"><s title="">&nbsp;</s><em><?php echo $row['comment_count'];?></em></span>
                                            </p>
                                        </div>
                                    </div>
                                <?php endforeach; ?>

                            </div>

                        </div>
                    </div>
                    <!-- END Video list -->
                </div>
                   
            </div>
            <!-- END Video list -->
        </div>
    </div>
</div>
