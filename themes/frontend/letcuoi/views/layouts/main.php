<?php $host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ''; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title><?php echo isset($this->pageTitle) ? CHtml::encode($this->pageTitle) : Yii::app()->name; ?></title>
        <link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
        <meta name="ROBOTS" content="index, follow" />
        <!--<meta http-equiv="refresh" content="900" />-->
        <meta name="google-site-verification" content="P5DkQ2snxl62jA_Bhe49Kgv4Gxq9fsloWRK-yt6DM1E" />

		<?php
            $version = 5;
            $cs = Yii::app()->getClientScript();
            $cs->registerCssFile('http://assets.let.vn/plugins/bootstrap/css/bootstrap.css?version='.$version);
            $cs->registerCssFile('http://phim.let.vn/themes/frontend/let/css/reset.css?version='.$version);
            $cs->registerCssFile('http://phim.let.vn/themes/frontend/let/css/grid.css?version='.$version);
            $cs->registerCssFile('http://phim.let.vn/themes/frontend/let/css/let.css?version='.$version);
            $cs->registerCssFile('http://phim.let.vn/themes/frontend/let/css/letphim.css?version='.$version);
            $cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/letcuoi.css?version='.$version);
            $cs->registerCoreScript('jquery');
            $cs->registerCoreScript('cookie');
            $cs->registerScriptFile('http://assets.let.vn/plugins/bootstrap/js/bootstrap.min.js', CClientScript::POS_HEAD);
            $cs->registerScriptFile('http://phim.let.vn/themes/frontend/let/js/let.js', CClientScript::POS_HEAD);
            $cs->registerScriptFile('http://phim.let.vn/themes/frontend/let/js/film.js', CClientScript::POS_HEAD);
            $cs->registerMetaTag('-zWqXqeIf4eqyqG67qc7YbZZJs6FibkbZ_XKxmk7hx4','google-site-verification');
        ?>
        
        <script>
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/vi_VN/all.js#xfbml=1";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
        
        <!-- Place this tag in your head or just before your close body tag. -->
        <script type="text/javascript" src="https://apis.google.com/js/plusone.js">
          {lang: 'vi'}
        </script>
        <!-- Place this tag where you want the +1 button to render. -->
        
        <?php if (letFunction::is_mobile()): ?>
        <style>
            .no_mobile {display: none;}
        </style>
        <?php endif; ?>
    </head>
    <body>
        <?php if (isset($_GET['debug'])): ?>
        <div class="debug"></div>
        <?php endif; ?>
        <div class="letMenu">
            <div class="container_24" style="height: 38px;">
                <div class="grid_24 clearfix" style="height: 38px;">
                    <div class="home">
                        <a href="http://let.vn">
                            <s class="icon_home">&nbsp;</s>
                        </a>
                    </div>
                    <ul class="nav">
                        <li class="first"><a href="http://phim.let.vn">Phim</a></li>
                        <li><a href="http://cuoi.let.vn">Cười +</a></li>
                        <!--<li><a href="http://me2.vn">Chợ tình</a></li>-->
                        <li><a href="http://forum.let.vn">Diễn đàn</a></li>
                    </ul>
                    <div class="fr likefacebook" style="line-height: 50px; display: none;">
                        <div class="fb-like" data-href="http://www.facebook.com/trollervietnam" data-send="true" data-layout="button_count" data-width="620" data-show-faces="false"></div>
                    </div>
                    <div class="fr likefacebook" style="line-height: 43px; color: #fff; margin-right: 10px; display: none;">
                        Like để ủng hộ Let Cười nha !
                    </div>
                </div>
            </div>
        </div>
        <div class="container_24 clearfix">
            
            <!-- Header -->
            <div class="grid_24 letHeader">
                <div class="grid_6 alpha logo letcuoi">
                    <a title="Về trang chủ let.vn" href="<?php echo Yii::app()->getBaseUrl(true); ?>">let.vn</a>
                </div>

                <!-- Search box -->
                <div class="grid_10 search_box">
                    <?php $this->renderPartial('//blocks/searchbox'); ?>
<!--                    <div class="search_box_extra">
                        <ul class="search_box_keyword">
                            <li><a href="javascript:void(0);">Đàm Vĩnh Hưng</a></li>
                            <li><a href="javascript:void(0);">Phương Thanh</a></li>
                            <li><a href="javascript:void(0);">HKT</a></li>
                        </ul>
                    </div>-->
                </div>
                <!-- END Search box -->

                <!-- Cần code lại cho phù hợp với mục đích -->
                <div class="grid_8 omega clearfix">
                    <div class="loginForm">
                        
                    </div>
                    <div class="maggin_top_10 clearfix login_form" style="border: 1px solid #E3E3E3; background: #fff; padding: 10px;">
                        <?php $this->widget('application.modules.account.widgets.WidgetLoginAccount'); ?>
                    </div>
                </div>
                <!-- END Cần code lại cho phù hợp với mục đích -->
            </div>
            <!-- END Header -->
            
            <?php $this->renderPartial('//blocks/topmenu'); ?>
            
            <div class="grid_24 maggin_bottom_10 no_mobile">
                <?php
                // Top menu
                $banners = array(
                    array(
                        'from' => strtotime('2013-06-03 00:00:00'),
                        'to' => strtotime('2013-07-03 23:59:59'),
                        'source' => 'http://phim.let.vn/themes/frontend/let/ads/troller-tai-nang.jpg',
                        'url' => 'http://cuoi.let.vn/cuoc-thi-tim-kiem-troller-tai-nang.let',
                        'source_default' => 'http://assets.let.vn/ads/quang_cao_tai_day_950x100.gif',
                        'url_default' => 'https://docs.google.com/spreadsheet/ccc?key=0AukxH5-6U2AIdGZfSzlwcnVvcUtMMzhncGthOVRiaEE#gid=0',
                        'type' => 'image',
                        'class' => '',
                        'width' => '950',
                        'height' => '70',
                    ),
                );
                ?>
                <?php $this->renderPartial('//blocks/ads', array(
                    'banners' => $banners,
                )); ?>
            </div>

            <?php echo $content; ?>
        </div>
        <style>
/*            body {background: #fff;}*/
            .stickit {top: 38px;}
        </style>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.waitforimages.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.stickem.js"></script>
        <script type="text/javascript">
            $(document).waitForImages(function() {
                $('.pin_list').stickem();      
            });
        </script>
        
        <div class="letFooter">
<!--            <div class="search_box">
                <div class="container_24 clearfix">
                    <div class="grid_10">
                        <?php // $this->renderPartial('//blocks/searchbox'); ?>
                    </div>
                </div>
            </div>-->
            
            <div class="container_24 clearfix info">
                <div class="grid_24">
                    <div style="font-weight: bold;">Bản quyền thuộc về iLet Media</div>
                    <div>Website được xây dựng trên hệ thống <span class="labelinfo">Letkit v1.0</span></div>
                    <div>Website đang trong quá trình xây dựng và thử nghiệm. Mọi chi tiết xin liên hệ:</div>
                    <div><span class="labelinfo">[M]</span> 0988.631988 - 0908.631988 | <span class="labelinfo">[E]</span> nguago@let.vn | <span class="labelinfo">[Y!M]</span> letnguago</div>
                    <img border="0" src="http://whos.amung.us/swidget/cuoiletvn" width="0" height="0" />
                </div>
           </div>
        </div>
        <div id="fb-root"></div>
        
        <?php if (!letFunction::is_mobile()): ?>
        <!-- Quảng cáo 2 bên -->
        <script type="text/javascript">
        $(document).ready(function() {
            if(screen.width >= 1280) {
                var documentWidth = $(document).width(); // Chieu rong cua document
                var contentWidth = 950; // Chieu rong cua noi dung chinh
                var marginContent = 10; // Khoang cach tu quang cao den noi dung chinh
                var adDivWidth = ((documentWidth - contentWidth) / 2) - marginContent; // Chieu rong cua div chua quang cao
                //$('.ad_float').width(adDivWidth).show();
            }
        });
        </script>
        <style>
            .ad_float {position: fixed; width: 120px; height: 600px; top: 45px; display: none;}
            .ad_left {left: 0; text-align: right;}
            .ad_right {right: 0;}
        </style>
        <div class="ad_float ad_left">
            <?php
            // Top menu
            $banners = array(
                array(
                    'from' => strtotime('2013-05-21 00:00:00'),
                    'to' => strtotime('2013-07-21 23:59:59'),
                    'source' => Yii::app()->theme->baseUrl . '/ads/the-best-lee-soon-shin.jpg',
                    'url' => 'http://phim.let.vn/xem-phim-the-best-lee-soon-shin-online-film-3531.let',
                    'source_default' => 'http://assets.let.vn/ads/quang_cao_tai_day_120x600.gif',
                    'url_default' => 'https://docs.google.com/spreadsheet/ccc?key=0AukxH5-6U2AIdGZfSzlwcnVvcUtMMzhncGthOVRiaEE#gid=0',
                    'type' => 'image',
                    'class' => '',
                    'width' => '120',
                    'height' => '600',
                ),
            );
            ?>
            <?php $this->renderPartial('//blocks/ads', array(
                'banners' => $banners,
            )); ?>
        </div>
        <div class="ad_float ad_right">
            <?php
            // Top menu
            $banners = array(
                array(
                    'from' => strtotime('2013-04-26 00:00:00'),
                    'to' => strtotime('2013-04-27 23:59:59'),
                    'source' => Yii::app()->theme->baseUrl . '/ads/onec.swf',
                    'url' => '',
                    'source_default' => 'http://assets.let.vn/ads/quang_cao_tai_day_120x600.gif',
                    'url_default' => 'https://docs.google.com/spreadsheet/ccc?key=0AukxH5-6U2AIdGZfSzlwcnVvcUtMMzhncGthOVRiaEE#gid=0',
                    'type' => 'flash',
                    'class' => '',
                    'width' => '120',
                    'height' => '600',
                ),
            );
            ?>
            <?php $this->renderPartial('//blocks/ads', array(
                'banners' => $banners,
            )); ?>
        </div>
        <!-- END Quảng cáo 2 bên -->
        
        <!--- Popup Ads by Ambient Digital --->
        <script type="text/javascript">
        var _abd = _abd || [];
        _abd.push(["1344417668","Popup","1344418189"]);
        </script>
        <script src="http://media.adnetwork.vn/js/adnetwork.js" type="text/javascript"></script>
        <noscript><a href="http://track.adnetwork.vn/247/adServerNs/zid_1344418189/wid_1344417668/" target="_blank"><img src="http://delivery.adnetwork.vn/247/noscript/zid_1344418189/wid_1344417668/" /></a></noscript>
        <!--- Popup Ads by Ambient Digital --->
        <?php endif; ?>

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-41321499-1', 'cuoi.let.vn');
            ga('send', 'pageview');
        </script>
    </body>
</html>