                    <div class="search_box_main">
                        <!--[搜索:下拉与输入]-->
                        <div class="search_box_content">
                            <!--                            <div id="dropMenu0" class="f_drop_menu search_box_drop">
                                                            <div class="f_drop_menu_main">
                                                                <a href="javascript:void(0):" class="f_drop_menu_main_title"><span id="hSearchOptCur">全部</span><em class="f_drop_menu_main_arrow">v</em></a>
                                                            </div>
                                                            <div class="f_drop_menu_sub">
                                                                <div class="f_drop_menu_sub_ct">
                                                                    <ul class="f_drop_menu_sub_list">
                                                                        <li><a href="javascript:;">全部</a></li>
                                                                        <li><a href="javascript:;">视频</a></li>
                                                                        <li><a href="javascript:;">专辑</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>-->
                            <div class="search_box_input">
                                <input type="text" maxlength="60" id="search_keyword" class="gray inp_search" value="" placeholder="Hãy nhập từ khóa để tìm ảnh, video..." />
                            </div>
                        </div>
                        <!--[搜索:按钮]-->
                        <div class="search_box_button">
                            <div class="button_search" id="button_search" value="Tìm kiếm" onclick="checkSearch();"></div>
                        </div>
                    </div>
                    
                    <script type="text/javascript" language="javascript">
					$(document).ready(function(){
//						$("#search_keyword").focus();
						$(".search_box_main input").keydown(function(e){
							if (e.which == 13) checkSearch();
						});
					});

                    function checkSearch()
                    {
                        var q=$("#search_keyword").val();
                        q=trim(q,' ');
                        q=q.replace(/\s/g,'_');
                        q=q.replace('/','-');
                        if (q == "Nhập từ khóa...") alert("Bạn chưa nhập từ khóa"); 
                        window.location.href = 'http://search.let.vn/tim-kiem.let?module=pin&keyword=' + q;
                        return false;
                    }

                    function trim(str, chars) {
                        return ltrim(rtrim(str, chars), chars);
                    }
                     
                    function ltrim(str, chars) {
                        chars = chars || "\\s";
                        return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
                    }
                    
                    function rtrim(str, chars) {
                        chars = chars || "\\s";
                        return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
                    }
                    </script>

