            <div style="" class="clearfix ads_topbanner">
        	<?php foreach ($banners as $banner): ?>
            	<?php if($banner['from'] < time() AND time() < $banner['to']): ?>
                	<?php if ($banner['type'] == 'flash'): ?>
                    <div class="<?php echo $banner['class']; ?>">
                    	<embed src="<?php echo $banner['source']; ?>" quality="high" type="application/x-shockwave-flash" wmode="transparent" width="<?php echo $banner['width']; ?>" height="<?php echo $banner['height']; ?>" pluginspage="http://www.macromedia.com/go/getflashplayer" allowScriptAccess="always"></embed>
                	</div>
	                <?php elseif ($banner['type'] == 'image') : ?>
                	<div class="<?php echo $banner['class']; ?>">
                    	<a href="<?php echo $banner['url']; ?>" target="_blank" rel="nofollow"><img src="<?php echo $banner['source']; ?>" width="<?php echo $banner['width']; ?>" height="<?php echo $banner['height']; ?>" /></a>
                    </div>
	                <?php elseif ($banner['type'] == 'iframe') : ?>
                    <div class="<?php echo $banner['class']; ?>">
                    <iframe frameborder="0" marginwidth="0" marginheight="0" scrolling="no" src="<?php echo $banner['source']; ?>" width="<?php echo $banner['width']; ?>" height="<?php echo $banner['height']; ?>" style="background: #FFF;"></iframe>
                    </div>
	                <?php endif; ?>
                <?php else: ?>
                	<div class="<?php echo $banner['class']; ?>">
                    	<a href="<?php echo $banner['url_default']; ?>" target="_blank" rel="nofollow"><img src="<?php echo $banner['source_default']; ?>" width="<?php echo $banner['width']; ?>" height="<?php echo $banner['height']; ?>" /></a>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
            </div>