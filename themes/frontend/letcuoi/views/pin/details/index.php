<?php
if ($category[0]['parent_id'] == '0')
    $parent = $category[0]['id'];
else
    $parent = $category[0]['parent_id'];

//$data['url'] = 'http://cuoi.let.vn' . $data['url'];
?>
<div class="grid_24">
    <div class="grid_16 alpha pin_details">
        <div class="clearfix">
        <?php $this->widget('category.widgets.frontend.catInParent', array(
            'module' => 'pin',
            'parent_id' => 0,
            'active_id' => $parent,
            'view' => 'catInParent_letcuoi_tab',
        )); ?>
        </div>
        
        <h1 class="title padding_bottom_5 maggin_bottom_5 maggin_top_10" style="border-bottom: 1px solid #D0D0D0; text-transform: uppercase;">
            <?php echo $data['title']; ?>
        </h1>        

        <div class="grid_16 alpha omega stickem-container">
        	<div class="maggin_bottom_5 clearfix">
                <div class="grid_8 alpha">
                    <i class="icon-eye-open"></i> <?php echo number_format($data['view_count'], 0, ',', '.'); ?>
                    <i class="icon-thumbs-up" style="margin-left: 5px;"></i> <?php echo number_format($data['like_count'], 0, ',', '.'); ?>
                    <i class="icon-comment" style="margin-left: 5px;"></i> <?php echo number_format($data['comment_count'], 0, ',', '.'); ?>
                    <i class="icon-share" style="margin-left: 5px;"></i> <?php echo number_format($data['share_count'], 0, ',', '.'); ?>
                </div>
	            <div class="grid_8 omega gray" style="text-align:right;">Đăng bởi <a href="<?php echo $this->createUrl('//wall-'.$user['id']); ?>" title="Ghé thăm nhà của <?php echo $user['username']; ?>"><?php echo $user['username']; ?></a> vào <?php echo letDate::fuzzy_span(strtotime($data['created_time'])); ?></div>
            </div>
            <div class="grid_16 alpha omega stickem maggin_bottom_5">
                <div class="box_s2 bg_2 clearfix" style="">
                    <div class="fl">
                        <div class="g-plusone" data-size="medium" data-annotation="none" data-href="http://phim.let.vn"></div>
                    </div>
                    <div class="fl" style="margin-left: 5px;"><fb:like href="<?php echo $data['url']; ?>" send="false" layout="button_count" width="90px" show_faces="false" font="" label="Post" ref="fb.l"></fb:like></div>
                    <div class="fr">
                        <div class="social_button right clearfix">
                            <a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo $data['url']; ?>" target="_blank" class="facebook"><span></span></a>
                            <a href="" target="_blank" class="twitter"><span></span></a>
                            <a href="https://plus.google.com/share?url=<?php echo $data['url']; ?>" target="_blank" class="googleplus"><span></span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid_16 alpha omega maggin_bottom_15">
                <div class="pin_content box_s2 clearfix bg_white padding_20 center">
                    <?php if ($data['type'] == 'video'): ?>
                    <div id="divVideo" style="width: 605px;height: 315px">
                        <?php
                        Yii::import('application.vendors.libs.grabVideo');
                        echo grabVideo::youtubePlayer($data['video'], 605, 315);
                        ?>
                    </div>
                    <!--- Video Ad by Ambient Digital --->
                    <script type="text/javascript" src="http://media.adnetwork.vn/js/jwplayer.js"></script>
                    <script type="text/javascript">
                    var _abd = _abd || [];
                    _abd.push(["1344417668","Video","1344418247","divVideo","605","315"]);
                    </script>
                    <script src="http://media.adnetwork.vn/js/adnetwork.js" type="text/javascript"></script>
                    <noscript><a href="http://track.adnetwork.vn/247/adServerNs/zid_1344418247/wid_1344417668/" target="_blank"><img src="http://delivery.adnetwork.vn/247/noscript/zid_1344418247/wid_1344417668/" /></a></noscript>
                    <!--- Video Ad by Ambient Digital --->

                    <?php elseif ($data['type'] == 'image'): ?>
                        <img src="<?php echo Common::getImageUploaded('pin/origin/' . $data['image']); ?>" alt="<?php echo $data['title']; ?>" />
                    <?php else: ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="grid_16 alpha omega maggin_bottom_15" style="min-height: 200px;">
                <div class="box_s2 clearfix bg_white">
                    <div class="fb-comments" data-href="<?php echo $data['url']; ?>" data-width="605" data-num-posts="10"></div>
                </div>
            </div>
            
            <?php
            $arrCategory = array();
            foreach ($category as $row) {
                $arrCategory[] = $row['id'];
            }
            $this->widget('pin.widgets.frontend.pin_other',array(
                'title' => 'Cùng thể loại',
                'limit' => 8,
                'category' => $arrCategory,
                'creator_deny' => $data['creator'],
                'order_by' => 'RAND()',
                'view' => 'pin_other_1',
            ));
            ?>
        </div>

    </div>
    <div class="grid_8 omega">
        <?php $this->widget('application.modules.pin.widgets.frontend.userinfo', array(
            'user_id' => $data['creator'],
        )); ?>
        
        <?php
        $arrCategory = array();
        foreach ($category as $row) {
            $arrCategory[] = $row['id'];
        }
        $this->widget('pin.widgets.frontend.pin_other',array(
            'title' => 'Cùng tác giả',
            'limit' => 12,
            'category' => $arrCategory,
            'creator' => $data['creator'],
            'order_by' => 'RAND()',
            'view' => 'pin_other_2',
        ));
        ?>

        <?php $this->widget('application.modules.pin.widgets.frontend.pinusertop', array(
            'blacklist' => array(1, 48015, 26100, 48881, 48880, 48879, 48886, 49008, 48991, 47151, 47741, 48257, 47296),
            'limit' => 10
        )); ?>
        
        <div class="maggin_bottom_5">
            <script type="text/javascript" src="http://e.eclick.vn/delivery/zone/937.js"></script>
        </div>
        
        <div class="maggin_bottom_5" style="border: 5px #ccc solid;">
            <div id="InPage_300_250"></div>
        </div>
        <script type="text/javascript">
        var _abd = _abd || [];
        /* load placement for account: nguago, site: http://phim.let.vn, size: 300x250 - web, zone: in_page */
        _abd.push(["1344417668","InPage","1364360395","InPage_300_250"]);					
        </script>
        <script src="http://media.adnetwork.vn/js/adnetwork.js" type="text/javascript"></script>
        <noscript><a href="http://track.adnetwork.vn/247/adServerNs/zid_1364360395/wid_1344417668/" target="_blank"><img src="http://delivery.adnetwork.vn/247/noscript/zid_1364360395/wid_1344417668/" /></a></noscript>
        
        <div class="maggin_bottom_5" style="border: 5px #ccc solid;">
            <div id="InPage_300_600"> </div>
        </div>
        <script type="text/javascript">
        var _abd = _abd || [];
        /* load placement for account: nguago, site: http://phim.let.vn, size: 300x600 - web, zone: in_page */
        _abd.push(["1344417668","InPage","1369794459","InPage_300_600"]);					
        </script>
        <script src="http://media.adnetwork.vn/js/adnetwork.js" type="text/javascript"></script>
        <noscript><a href="http://track.adnetwork.vn/247/adServerNs/zid_1369794459/wid_1344417668/" target="_blank"><img src="http://delivery.adnetwork.vn/247/noscript/zid_1369794459/wid_1344417668/" /></a></noscript>
    </div>
</div>
    <style>
        body {background: #fff;}
        .stickit {top: 38px;}
    </style>

    <?php
    $cs = Yii::app()->getClientScript();
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/jquery.waitforimages.js', CClientScript::POS_HEAD);
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/jquery.stickem.js', CClientScript::POS_HEAD);

    ?>
    <script type="text/javascript">
        $(document).waitForImages(function() {
            $('.pin_details').stickem();
    //		setTimeout("$('.pin_details').stickem()", 3000);
        });
//        $('iframe').load(function(){
//            alert("loaded.");
//        }); 
    </script>