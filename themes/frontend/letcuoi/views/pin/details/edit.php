<?php $this->renderPartial('//blocks/_topmenu'); ?>
<div class="FixedContainer" style="margin-top: 150px;">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'profileEdit',
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
            'class' => 'Form StaticForm UserForm'
        ),
    ));
    ?>
    <?php echo CHtml::hiddenField('apply', 0); ?>
    <?php echo $form->hiddenField($model, 'image', array('value' => '')); ?>
    
        <div class="deactivate_wrapper">
            <!-- Deactivate -->

            <div id="DeactivateAccountConfirm">
                <button type="button" class="Button WhiteButton Button18 ConfirmOuterButton"><strong>Deactivate Account</strong></button>
            </div>
        </div>
        <h3 class="clearfix">Edit Account Settings</h3>
        <ul class="ControlGroups clearfix intermediate">

            <!-- Email -->
            <li>
                <?php echo $form->labelEx($model, 'title'); ?>
                <div class="Right">
                    <?php echo $form->textField($model, 'title'); ?>
                </div>
            </li>
        </ul>

        <!-- Button -->
        <div class="Submit">
            <button class="Button RedButton Button24 userform_submit editpage_submit" type="submit">Save Profile</button>
        </div>
        <div style='display:none'>
            <input type='hidden' name='csrfmiddlewaretoken' value='OCIzq09M7qthqJxY5jUY7Sgn6EWaLPer' />
        </div>
    <?php $this->endWidget(); ?>
</div>
<!-- .FixedContainer -->

<script type="text/javascript">

    FancyForm.setup();

    $(document).ready(function() {

        $(".spinner").hide();

        var formButton = $(".Submit .Button");

        // This is a simple helper class that is used
        // to show enabled/disabled state on a field.
        // It will also enable or disable the submit
        // button on the form.
        var FieldState = function(el) {
            var el = $(el);
            this.enable = function() {
                el.removeClass("disabled");
                formButton.removeClass("disabled");
            }
            this.disable = function() {
                el.addClass("disabled");
                formButton.addClass("disabled");
            }
        }

        // Character count
        CharacterCount.truncateData("#id_about", 200);
        CharacterCount.setup('#id_about', '#aboutCount', '.Submit .Button', 200, 'characters remaining');
        $('#id_about').keyup(function() {
            if ($(this).val().length == 0)
                $('.Submit .Button').removeClass('disabled');
        });


        // Change Avatar
        $(".change_avatar").click(function(e){ e.preventDefault(); $(this).remove(); $("#id_img").show(); });

        $("#id_img").change(function(){
            $("#profileEdit").ajaxSubmit({ dataType:  'json', iframe: true, url: '/pin/preview/',
                success:   function(data) {
                    if (data.status == 'success') { $("#profileEdit .current_avatar").attr("src", data.image_url); }
                    else { alert(data.message); }
                }
            });
            return false;
        });

        // Pretty much the same code for all networks.
        function setup_network_connector(network, connect, success, failure) {
            // Connect a Google account to a user account
            success = success || function() {};
            failure = failure || function() {};

            if (network == "facebook") {
                $('#unlink_fb_password').focus(function(){
                    $('#unlink_fb_confirm').removeAttr('disabled').removeClass('disabled');
                }).keyup(function(e){
                    $('#DisconnectFBhelp_text').fadeOut(200);
                    if (e.keyCode == 13) {
                        $('#unlink_fb_confirm').trigger('click');
                    }
                })

                $('#unlink_fb_confirm').click(function(e){
                    var button = $('#unlink_fb_confirm');
                    button.addClass('disabled').text('Checking...');
                    e.preventDefault();
                    var pw = $('#unlink_fb_password').val();
                    $.post("/password/verify/", { 'current_password' : pw }, function(data){
                        if (data.password_verified) {
                            (function(error) {
                                $.post("/connect/facebook/", function(data){
                                    if (data == "success") {
                                        $(".refresh_user_image").remove();
                                        $("#find_"+network+"_friends").remove();
                                        Modal.close('DisconnectFBModal');
                                        $("#"+network+"_connect").attr('checked', false).trigger('change');
                                        success();
                                    } else {
                                    }
                                });
                            })(function() {
                                $("#"+network+"_connect").attr("checked", "checked");
                                alert("Oops! Something went wrong disconnecting your account. Please try again.");
                                failure();
                            });
                        } else {
                            setTimeout(function(){
                                button.removeClass('disabled').text('Disconnect Facebook');
                                $('#unlink_fb_password').val('').focus();
                            }, 400);
                            $('#DisconnectFBhelp_text').text('Incorrect password, try again').fadeIn();
                            console.log('Bad password');
                        }
                    });
                });

                $('#facebook_connect').on("mousedown", function(e) {
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    $("label[for=facebook_connect]").trigger('click', e);
                })
            }

            $("label[for="+network+"_connect]").click(function(e) {
                var self =  $("#"+network+"_connect");
                var checked = self.is(":checked");
                var field = new FieldState(self.parent());
                if (!checked) {
                    connect();
                    return false;
                } else {
                    e.preventDefault();
                    if (self.attr('id') == 'facebook_connect') {
                        Modal.show('DisconnectFBModal');
                    } else {
                        (function(error) {
                            field.disable();
                            $.post("/connect/"+network+"/", function(data){
                                field.enable();
                                self.attr("checked", false).trigger('change');
                                if (data == "success") {
                                    $(".refresh_user_image").remove();
                                    $("#find_"+network+"_friends").remove();
                                    success();
                                } else {
                                    error()
                                }
                            }).error(error);
                        })(function() {
                            field.enable();
                            self.attr("checked", "checked").trigger('change');
                            alert("Oops! Something went wrong disconnecting your account. Please try again.");
                            failure();
                        });
                    }
                }
            });
        }

        $('#ChangeOfHeartFB').click(function() {
            var field = new FieldState($('#slider_fb').parent());
            $('#UnlinkForm').hide();
            field.enable;
            $('#slider_fb').attr('checked','checked');
            $('#unlink_fb_confirm').attr('disabled', 'disabled').addClass('disabled');
            $('#slider_fb').show();
            return false;
        });

        // Facebook
        setup_network_connector("facebook", function() {
            var scope = "email,user_likes,user_birthday";
            Facebook.startFacebookConnect(scope);
        }, function() {
            $("#facebook_timeline").parent().remove();
        });

        // Enabled Facebook Timeline publishing
        $("#facebook_timeline").change(function(){
            var field = new FieldState($(this).parent());
            if ($(this).is(":checked")) {
                var scope = "publish_actions";
                Facebook.startFacebookConnect(scope, true, true, "user_edit"); // reloads window
            } else {
                (function(error) {
                    field.disable();
                    $(".Submit a").addClass("disabled");
                    $.post("/connect/facebook/?disable_timeline=1&ref_page=user_edit", function(data){
                        field.enable();
                        if (data != "success")
                            error();
                    }).error(error);
                })(function() { // Error handler
                    field.enable();
                    $("#facebook_timeline").attr("checked", "checked");
                    alert("Oops! Something went wrong disabling Facebook Timeline. Please try again.");
                });
            }
        });

        // Twitter
        setup_network_connector("twitter", Twitter.startTwitterConnect);

        // Google
        setup_network_connector("google", Google.startGoogleConnect);

        // Yahoo
        setup_network_connector("yahoo", Yahoo.startYahooConnect);

        // refresh user image on /username/edit/ from facebook or twitter
        $(".refresh_user_image").click(function() {
            $('.spinner').show();
            $('.current_avatar').hide();
            $.get(window.location.pathname, {service: $(this).attr("data")}, function(data){
                if (data.status =="success") {
                    data.data = data.data.replace('.jpg', '_o.jpg');
                    data.data = data.data.replace('http://', 'https://');
                    $('.spinner').hide();
                    $(".current_avatar").attr("src", data.data);
                    $(".current_avatar").show();
                    $(".nav .UserImage").attr("src", data.data);
                } else { alert(data.message); }
            });
        });



        $("#id_username").keyup(function() {
            var $message = $(".username_available"),
            check_username = $(this).val();
            if (check_username.length < 3 | check_username.length > 15) {
                $message.html("Username should be 3 to 15 characters").css("color", "red");
                return false;
            }
            $.ajax({
                type: "GET",
                url: '/check_username/',
                dataType: "json",
                data: "check_username="+escape(check_username)+"&csrfmiddlewaretoken="+$("input[name=csrfmiddlewaretoken]").val(),
                success: function(data) {
                    if (data.status == "success") {
                        $message.html("Available").css("color", "green");
                    } else {
                        $message.html(data.message || "Already taken").css("color", "red");
                    }
                }
            });
            return false;
        });


        // Deactivate User
        $("#DeactivateAccountConfirm")
        .confirmForm()
        .on("confirmed", function() {
            var $self = $(this);
            $self.confirmForm("startProgress");

            $.ajax({
                type: 'delete',
                url: '/delete_user/',
                dataType: 'json'
            })
            .done( function(data, textStatus) {
                if (data.status == "success") {
                    window.location="/";
                } else {
                    alert(" Oops! Something went wrong! ");
                }
            })
            .fail( function(XMLHttpRequest, textStatus, errorThrown) {
                alert("Oops! The request failed for some reason.");
            })
            .always( function() {
                $self.confirmForm('endProgress');
            });
        });

    });
</script>
<div id="SearchAutocompleteHolder"></div>
<button id="ScrollToTop" class="Button WhiteButton Offscreen Indicator" type="button"> Scroll to Top </button>
<script type="text/javascript">
    $(document).ready(function() {

        if (top != self) {
            $('body').html('<h1>Unauthorized</h1>')
        }
    });
</script>
<script type="text/javascript">

    function filtered_gaq_push_pageview() {
        cur_url = document.URL;

        if (!cur_url.match(/email=/)) {
            _gaq.push(['_trackPageview']);
        }
    }

    function trackGAEvent(category, action, label, value) {
        _gaq = _gaq || []


        // Event
        _gaq.push(['_trackEvent', category, action, label, value]);

        // Virtual Page
        virtual_page = '_event_';
        virtual_page += "/" + category;

        if(!action) action = '_';
        virtual_page+="/" + action;
        if(label) virtual_page+= "/" + label;

        _gaq.push(['_trackPageview', virtual_page]);


    }

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-12967896-1']);
    _gaq.push(['_setCustomVar', 1, 'is_logged_in', 'logged in', 2]);
    _gaq.push(['_setCustomVar', 2, 'page_name', 'edit_user', 1]);





    filtered_gaq_push_pageview();


    (function() {
        var ga = document.createElement('script'); ga.type='text/javascript'; ga.async=true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);
    })();

</script>
<div id="ErrorDialog" class="ModalContainer alertBox">
    <div class="modal">
        <div class="closeBtn closeButton">&nbsp;</div>
        <div class="message"></div>
        <div class="footer">
            <button class="Button RedButton Button18 closeButton" type="button">Close</button>
        </div>
    </div>
    <div class="overlay"></div>
</div>
<div id="ConfirmationDialog" class="ModalContainer alertBox">
    <div class="modal">
        <div class="closeBtn closeButton">&nbsp;</div>
        <h2>Confirmation</h2>
        <div class="message"></div>
        <div class="footer">
            <button class="Button RedButton Button18 okButton confOkButton" type="button">Ok</button>
            <button class="Button RedButton Button18 okButton confOkButton2 hidden" type="button"></button>
            <button class="Button WhiteButton Button18 closeButton confCloseButton" type="button">Cancel</button>
        </div>
    </div>
    <div class="overlay"></div>
</div>
