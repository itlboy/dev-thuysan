<?php $this->renderPartial('//pin/wall/_header', array('user_id' => $user_id)); ?>
<div class="grid_24 ">
    <div class="tabs_s2_content" style="border: 1px solid #d6d6d6; border-top: 0;">
        <div class="form-horizontal">
            <div class="control-group">
                <div class="controls">
                    <?php
                    if ($model->scenario == 'image' AND $model->isNewRecord) {
                        Yii::import("ext.xupload.models.XUploadForm");
                        $uploadModel = new XUploadForm;
                        $this->widget('ext.xupload.XUploadWidget', array(
                            'url' => Yii::app()->createUrl('cms/upload/upload', array('module' => 'pin')),
                            'model' => $uploadModel,
                            'attribute' => 'file',
                            'multiple' => false,
                            'options' => array(
                                'onComplete' => 'js:function (event, files, index, xhr, handler, callBack) {
                                    if (handler.response.error !== 0) alert(handler.response.error);
                                    else {
                                        $("#KitPin_image").val(handler.response.name);
                                        $("#ajax_result_image").html();
                                        $("#ajax_result_image").html(\'<li><a rel="collection" href="' . Common::getImageUploaded() . '\'+handler.response.name+\'"><img src="' . Common::getImageUploaded() . '\'+handler.response.name+\'?update='.time().'" height="84" width="148"/><span class="name">Ảnh gốc</span></a></li>\');
                                    }
                                }',
                            )
                        ));
                    }
                    ?>
                </div>
            </div>
        </div>

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'pin-form',
            'enableAjaxValidation' => false,
            'htmlOptions' => array('enctype' => 'multipart/form-data', 'class' => 'main form-horizontal')
        ));
        echo $form->hiddenField($model, 'image', array('value' => ''));
        ?>       
        
            <?php if ($model->getError('image') != NULL) : ?>
            <div class="control-group">
                <div class="controls">
                    <?php echo $form->error($model, 'image'); ?>
                </div>
            </div>
            <?php endif; ?>
        
            <div class="control-group">
                <label class="control-label" for="KitPin_title">Tiêu đề</label>
                <div class="controls">
                    <?php $titleParams = array('class' => 'input-xxlarge');
                        if (!$model->isNewRecord)
                            $titleParams['readonly'] = 'readonly';
                    ?>
                    <?php echo $form->textField($model, 'title', $titleParams); ?>
                    <?php echo $form->error($model, 'title'); ?>
                </div>
            </div>

            <?php if ($model->scenario == 'video') : ?>
            <div class="control-group">
                <label class="control-label" for="KitPin_video">Đường link video</label>
                <div class="controls">
                    <?php $videoParams = array('class' => 'input-xxlarge');
                        if (!$model->isNewRecord)
                            $videoParams['readonly'] = 'readonly';
                    ?>
                    <?php echo $form->textField($model, 'video', $videoParams); ?>
                    <?php echo $form->error($model, 'video'); ?>
                </div>
            </div>
            <?php endif; ?>

            <div class="control-group">
                <label class="control-label" for="KitPin_content">Nội dung</label>
                <div class="controls">
                    <?php echo $form->textArea($model, 'content', array('class' => 'input-xxlarge', 'rows' => 3)); ?>
                    <?php echo $form->error($model, 'content'); ?>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="KitPin_categories">Chuyên mục</label>
                <div class="controls">
                    <?php
                    $categoryId = NULL;
                    if ($model->scenario == 'image') {
                        $categoryId = 87;
                    } else if ($model->scenario == 'video') {
                        $categoryId = 88;
                    }
                    echo $form->radioButtonList($model, 'categories', CHtml::listData(KitCategory::model()->getPinCategoryOptions('pin', $categoryId), 'id', 'name'), array(
                        'separator' => '',
                        'template' => '<label class="radio">{input} {label}</label>'));
                    ?>
                    <?php echo $form->error($model, 'categories'); ?>
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn">Cập nhật</button>
                </div>
            </div>
        <?php $this->endWidget(); ?>

    </div>
</div>