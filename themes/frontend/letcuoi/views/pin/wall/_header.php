
<style>
    body {background: #fff;}
    .stickit {top: 38px;}
</style>
<div class="grid_24 ">
    <div class="userinfo_box">
        <div class="avatar"><img src="<?php echo KitAccount::getAvatar($user_id,'large'); ?>" /></div>
        <div class="fullname"><a href="<?php echo Yii::app()->createUrl('wall-'.$user_id); ?>"><?php echo KitAccount::getField($user_id, 'username'); ?></a></div>
        <div class="user_stats"><?php echo KitAccount::getField($user_id, 'share_total'); ?> share | <?php echo KitAccount::getField($user_id, 'comment_total'); ?> comment | <?php echo KitAccount::getField($user_id, 'like_total'); ?> like | <?php echo KitAccount::getField($user_id, 'view_total'); ?> view | <?php echo KitAccount::getField($user_id, 'post_total'); ?> bài đăng</div>
        <ul class="tabs_s2 user_tab">
            <?php if (Yii::app()->user->id == $user_id): ?>
                <li<?php if ($this->action->id == 'index'): ?> class="active"<?php endif; ?> style="margin-left: 21px;"><a href="<?php echo Yii::app()->createUrl('wall-'.$user_id); ?>">Tất cả</a></li>
                <li<?php if ($this->action->id == 'image'): ?> class="active"<?php endif; ?>><a href="<?php echo Yii::app()->createUrl('wall-'.$user_id.'/image'); ?>">Ảnh</a></li>
            <?php else: ?>
                <li<?php if ($this->action->id == 'image' OR $this->action->id == 'index'): ?> class="active"<?php endif; ?> style="margin-left: 21px;"><a href="<?php echo Yii::app()->createUrl('wall-'.$user_id.'/image'); ?>">Ảnh</a></li>
            <?php endif; ?>
            
            <li<?php if ($this->action->id == 'video'): ?> class="active"<?php endif; ?>><a href="<?php echo Yii::app()->createUrl('wall-'.$user_id.'/video'); ?>">Video</a></li>
            <li<?php if ($this->action->id == 'chat'): ?> class="active"<?php endif; ?>><a href="<?php echo Yii::app()->createUrl('wall-'.$user_id.'/chat'); ?>">Chat</a></li>
<!--            <li<?php if ($this->action->id == 'story'): ?> class="active"<?php endif; ?>><a href="<?php echo Yii::app()->createUrl('wall-'.$user_id.'/story'); ?>">Truyện cười</a></li>-->
        </ul>
    </div>
</div>