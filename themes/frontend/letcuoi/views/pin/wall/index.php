<?php $this->renderPartial('//pin/wall/_header', array('user_id' => $user_id)); ?>
<div class="grid_24 ">
    <div class="tabs_s2_content" style="border: 1px solid #d6d6d6; border-top: 0;">
        <?php if (Yii::app()->user->id == $user_id) {
            $this->widget('application.modules.pin.widgets.frontend.pinwallall', array(
                'creator' => $user_id
            ));
        } else {
            $this->widget('application.modules.pin.widgets.frontend.pinwall', array(
                'creator' => $user_id,
                'type' => 'image',
                'view' => 'image',
            ));
        } ?>
    </div>
</div>
