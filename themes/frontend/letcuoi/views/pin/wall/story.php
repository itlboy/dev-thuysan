<?php $this->renderPartial('//pin/wall/_header', array('user_id' => $user_id)); ?>
<div class="grid_24 ">
    <div class="tabs_s2_content" style="border: 1px solid #d6d6d6; border-top: 0;">
        <?php
        $this->widget('application.modules.pin.widgets.frontend.pinwall', array(
            'creator' => $user_id,
            'type' => 'text',
            'view' => 'story'
        ));
        ?>
    </div>
</div>
