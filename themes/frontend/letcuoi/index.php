<?php include './header.php'; ?>
<?php include './_block/topmenu_film.php'; ?>
<div class="grid_24 padding_bottom">
    <div class="box_s1 clearfix border_red bg_white">
        <div class="clearfix">
            <div class="grid_2 alpha title">Phim hot</div>
            <div class="grid_16 omega box_link_extra fr" style="text-align: right;">
                <a href="javascript:void(0):">Hành động</a><span>|</span>
                <a href="javascript:void(0):">Phiêu lưu</a><span>|</span>
                <a href="javascript:void(0):">Hoạt hình</a><span>|</span>
                <a href="javascript:void(0):">Hài hước</a><span>|</span>
                <a href="javascript:void(0):">Gia đình</a><span>|</span>
                <a href="javascript:void(0):">Phá án</a><span>|</span>
                <a href="javascript:void(0):">Tâm lý</a><span>|</span>
                <a href="javascript:void(0):">Kinh dị</a><span>|</span>
                <a href="javascript:void(0):">...</a>
            </div>
        </div>
        <div class="mainContent">
            <!-- Video list -->
            <div class="grid_16 alpha omega list_video_s1">
                <ul class="list maggin_left_20">
                    <?php for ($i = 0; $i < 8; $i++): ?>
                        <li class="item clearfix<?php if ($i % 4 == 3): ?> last<?php endif; ?>">
                            <div class="thumb mouseover">
                                <a title="" href="details_film.php">
                                    <img alt="" src="http://data.let.vn/uploads/film/medium/2013/04/17/bau-troi-tinh-yeu_628814.jpg" width="130" height="160" />
                                </a>
                            </div>
                            <div class="item_txtbox">
                                <h6>
                                    <a class="item_title" title="" href="details_film.php">Tân câu chuyện cảnh sát</a>
                                </h6>
                                <p><span><a target="_blank" href="" class="gray">The police</a></span></p>
                                <p>
                                    <span class="icon_play"><s title="">&nbsp;</s><em>48,613</em></span>
                                    <span class="icon_comment"><s title="">&nbsp;</s><em>19</em></span>
                                </p>
                            </div>
                        </li>
                    <?php endfor; ?>
                </ul>
            </div>
            <!-- END Video list -->
            <div class="grid_8 alpha omega list_video_s2">
                <ul class="list maggin_right_10">
                    <?php for ($i = 0; $i < 7; $i++): ?>
                        <li class="item clearfix item_bg_<?php if ($i % 2 == 0): ?>1<?php else: ?>2<?php endif; ?>">
                            <div class="thumb mouseover">
                                <a title="" href="details_film.php">
                                    <img alt="" src="http://data.let.vn/uploads/film/medium/2013/04/17/bau-troi-tinh-yeu_628814.jpg" width="40" height="60" />
                                </a>
                            </div>
                            <div class="item_txtbox">
                                <h6>
                                    <a class="item_title" title="" href="details_film.php">Tân câu chuyện cảnh sát</a>
                                </h6>
                                <p><span><a target="_blank" href="" class="gray">The police</a></span></p>
                                <p>
                                    <span class="icon_play"><s title="">&nbsp;</s><em>48,613</em></span>
                                    <span class="icon_comment"><s title="">&nbsp;</s><em>19</em></span>
                                </p>
                            </div>
                        </li>
                    <?php endfor; ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="grid_24 padding_bottom">
    <div class="box_s1 clearfix">
        <div class="mainContent">
            <!-- Video list -->
            <div class="grid_16 alpha omega bg_white">
                <div class="clearfix">
                    <div class="grid_2 alpha title">Phim mới</div>
                    <div class="grid_10 omega box_link_extra fr" style="text-align: right;">
                        <a href="javascript:void(0):">Hành động</a><span>|</span>
                        <a href="javascript:void(0):">Phiêu lưu</a><span>|</span>
                        <a href="javascript:void(0):">...</a>
                    </div>
                </div>
                <div class="list_video_s1">
                    <ul class="list maggin_left_20">
                        <?php for ($i = 0; $i < 4; $i++): ?>
                            <li class="item clearfix<?php if ($i % 4 == 0): ?> first<?php endif; ?>">
                                <div class="thumb mouseover">
                                    <a title="" href="details_film.php">
                                        <img alt="" src="http://data.let.vn/uploads/film/medium/2013/04/17/bau-troi-tinh-yeu_628814.jpg" width="127" height="160" />
                                    </a>
                                </div>
                                <div class="item_txtbox">
                                    <h6>
                                        <a class="item_title" title="" href="details_film.php">Tân câu chuyện cảnh sát</a>
                                    </h6>
                                    <p><span><a target="_blank" href="" class="gray">The police</a></span></p>
                                    <p>
                                        <span class="icon_play"><s title="">&nbsp;</s><em>48,613</em></span>
                                        <span class="icon_comment"><s title="">&nbsp;</s><em>19</em></span>
                                    </p>
                                </div>
                            </li>
                        <?php endfor; ?>
                    </ul>
                </div>
            </div>
            <!-- END Video list -->
            <div class="grid_8 alpha omega border_red padding2Sites_5">
                <div class="clearfix">
                    <div class="grid_1 alpha title">Phim mới</div>
                </div>
                <div class="list_video_s2">
                    <ul class="list">
                        <?php for ($i = 0; $i < 3; $i++): ?>
                            <li class="item clearfix item_bg_<?php if ($i % 2 == 0): ?>1<?php else: ?>2<?php endif; ?>">
                                <div class="thumb mouseover">
                                    <a title="" href="details_film.php">
                                        <img alt="" src="http://data.let.vn/uploads/film/medium/2013/04/17/bau-troi-tinh-yeu_628814.jpg" width="40" height="60" />
                                    </a>
                                </div>
                                <div class="item_txtbox">
                                    <h6>
                                        <a class="item_title" title="" href="details_film.php">Tân câu chuyện cảnh sát</a>
                                    </h6>
                                    <p><span><a target="_blank" href="" class="gray">The police</a></span></p>
                                    <p>
                                        <span class="icon_play"><s title="">&nbsp;</s><em>48,613</em></span>
                                        <span class="icon_comment"><s title="">&nbsp;</s><em>19</em></span>
                                    </p>
                                </div>
                            </li>
                        <?php endfor; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="grid_24 padding_bottom">
    <div class="box_s1 clearfix">
        <div class="mainContent">
            <!-- Video list -->
            <div class="grid_16 alpha omega bg_white">
                <div class="clearfix">
                    <div class="grid_2 alpha title">Phim đề cử</div>
                    <div class="grid_10 omega box_link_extra fr" style="text-align: right;">
                        <a href="javascript:void(0):">Hành động</a><span>|</span>
                        <a href="javascript:void(0):">Phiêu lưu</a><span>|</span>
                        <a href="javascript:void(0):">...</a>
                    </div>
                </div>
                <div class="list_video_s1">
                    <ul class="list maggin_left_20">
                        <?php for ($i = 0; $i < 4; $i++): ?>
                            <li class="item clearfix<?php if ($i % 4 == 0): ?> first<?php endif; ?>">
                                <div class="thumb mouseover">
                                    <a title="" href="details_film.php">
                                        <img alt="" src="http://data.let.vn/uploads/film/medium/2013/04/17/bau-troi-tinh-yeu_628814.jpg" width="127" height="160" />
                                    </a>
                                </div>
                                <div class="item_txtbox">
                                    <h6>
                                        <a class="item_title" title="" href="details_film.php">Tân câu chuyện cảnh sát</a>
                                    </h6>
                                    <p><span><a target="_blank" href="" class="gray">The police</a></span></p>
                                    <p>
                                        <span class="icon_play"><s title="">&nbsp;</s><em>48,613</em></span>
                                        <span class="icon_comment"><s title="">&nbsp;</s><em>19</em></span>
                                    </p>
                                </div>
                            </li>
                        <?php endfor; ?>
                    </ul>
                </div>
            </div>
            <!-- END Video list -->
            <div class="grid_8 alpha omega border_red padding2Sites_5">
                <div class="clearfix">
                    <div class="grid_1 alpha title">Danh sách text</div>
                </div>
                <div>
                    <ul class="list_text_s1">
                        <?php for ($i = 0; $i < 1; $i++): ?>
                            <li class="item"><a href="javascript:void(0);">Bố Ơi! Mình Đi Đâu Thế?? ( Are You Going?)</a></li>
                            <li class="item">Bố Ơi! Mình Đi Đâu Thế?? ( Dad, Where?)</li>
                        <?php endfor; ?>
                    </ul>
                    <ul class="list_text_s2">
                        <?php for ($i = 0; $i < 1; $i++): ?>
                            <li class="item"><a href="javascript:void(0);">Bố Ơi! Mình Đi Đâu Thế?? ( Are You Going?)</a></li>
                            <li class="item">Bố Ơi! Mình Đi Đâu Thế?? ( Dad, Where?)</li>
                        <?php endfor; ?>
                    </ul>
                    <ul class="list_text_s3">
                        <?php for ($i = 0; $i < 1; $i++): ?>
                            <li class="item"><a href="javascript:void(0);">Bố Ơi! Mình Đi Đâu Thế?? ( Are You Going?)</a></li>
                            <li class="item">Bố Ơi! Mình Đi Đâu Thế?? ( Dad, Where?)</li>
                        <?php endfor; ?>
                    </ul>
                    <ul class="list_text_s4">
                        <?php for ($i = 0; $i < 1; $i++): ?>
                            <li class="item"><a href="javascript:void(0);">Bố Ơi! Mình Đi Đâu Thế?? ( Are You Going?)</a></li>
                            <li class="item">Bố Ơi! Mình Đi Đâu Thế?? ( Dad, Where?)</li>
                        <?php endfor; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!--<div class="grid_24 padding_bottom">
    <div class="box_s1 clearfix">
        <div class="grid_1 alpha title border_red">Phim hot</div>
        <div class="grid_8 omega box_link_extra fr" style="text-align: right;">
            <a href="javascript:void(0):">Hành động</a><span>|</span>
            <a href="javascript:void(0):">Phiêu lưu</a><span>|</span>
            <a href="javascript:void(0):">Hoạt hình</a><span>|</span>
            <a href="javascript:void(0):">Hài hước</a><span>|</span>
            <a href="javascript:void(0):">Gia đình</a><span>|</span>
            <a href="javascript:void(0):">Phá án</a><span>|</span>
            <a href="javascript:void(0):">Tâm lý</a><span>|</span>
            <a href="javascript:void(0):">Kinh dị</a><span>|</span>
            <a href="javascript:void(0):">...</a>
        </div>
        <div class="mainContent"></div>
    </div>
</div>

<div class="grid_24 letMain">
    <div class="grid_6 alpha background">aaa</div>
    <div class="grid_10 background">bbb</div>
    <div class="grid_8 omega background">ccc</div>
</div>-->
<?php include './footer.php'; ?>