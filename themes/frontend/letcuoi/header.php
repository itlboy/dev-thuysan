<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Giao diện mới của let.vn</title>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
        <meta name="title" content="" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="ROBOTS" content="index, follow" />
        <meta http-equiv="refresh" content="900" />
        <meta name="google-site-verification" content="P5DkQ2snxl62jA_Bhe49Kgv4Gxq9fsloWRK-yt6DM1E" />

        <link rel="stylesheet" href="css/reset.css" />
        <link rel="stylesheet" href="css/grid.css" />
        <link rel="stylesheet" href="css/let.css" />
        <link rel="stylesheet" href="css/letphim.css" />
        <link rel="stylesheet" href="css/letcuoi.css" />
        <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
        <script src="js/let.js"></script>
        
        <script>
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/vi_VN/all.js#xfbml=1";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
        
        <!-- Place this tag in your head or just before your close body tag. -->
        <script type="text/javascript" src="https://apis.google.com/js/plusone.js">
          {lang: 'vi'}
        </script>
        <!-- Place this tag where you want the +1 button to render. -->
    </head>
    <!--[if IE 6 ]><body class="ie6 ie"><![endif]-->
    <!--[if IE 7 ]><body class="ie7 ie"><![endif]-->
    <!--[if IE 8 ]><body class="ie8 ie"><![endif]-->
    <!--[if !IE]>-->
    <body>
        <?php if (isset($_GET['debug'])): ?>
        <div class="debug"></div>
        <?php endif; ?>
        <div class="letMenu">
            <div class="container_24" style="height: 38px;">
                <div class="grid_24 clearfix" style="height: 38px;">
                    <div class="home">
                        <a href="javascript:void(0);">
                            <s class="icon_home">&nbsp;</s>
                        </a>
                    </div>
                    <ul class="nav">
                        <li class="first"><a href="javascript:void(0);">Diễn đàn</a></li>
                        <li><a href="javascript:void(0);">Phim</a></li>
                        <li><a href="javascript:void(0);">Truyện</a></li>
                        <li><a href="javascript:void(0);">Tin scandal</a></li>
                        <li><a href="javascript:void(0);">Vui cười</a></li>
                    </ul>
                    <div class="fr likefacebook" style="line-height: 50px; display: none;">
                        <div class="fb-like" data-href="http://phim.let.vn" data-send="true" data-layout="button_count" data-width="620" data-show-faces="false"></div>
                    </div>
                    <div class="fr likefacebook" style="line-height: 43px; color: #fff; margin-right: 10px; display: none;">
                        Like để ủng hộ Let Phim nha !
                    </div>
                </div>
            </div>
        </div>
        <div class="container_24 clearfix">
            
            <!-- Header -->
            <div class="grid_24 letHeader">
                <div class="grid_6 alpha logo">
                    <a title="iLet Media" href="index.php">let.vn</a>
                </div>

                <!-- Search box -->
                <div class="grid_10 search_box">
                    <?php include './_block/searchbox.php'; ?>
                    <div class="search_box_extra">
                        <ul class="search_box_keyword">
                            <li><a href="javascript:void(0);">Đàm Vĩnh Hưng</a></li>
                            <li><a href="javascript:void(0);">Phương Thanh</a></li>
                            <li><a href="javascript:void(0);">HKT</a></li>
                        </ul>
                    </div>
                </div>
                <!-- END Search box -->

                <!-- Cần code lại cho phù hợp với mục đích -->
                <div class="grid_4 omega">
					<div>
                    	
                    </div>
                </div>
                <!-- END Cần code lại cho phù hợp với mục đích -->
            </div>
            <!-- END Header -->
            