<div class="cr-classiads" style="padding-top: 10px">
    <div class="ct-acc-l">
        <?php $this->renderPartial('//blocks/treeAccount'); ?>
    </div>
    <div class="cr-classiads-l" style="float:right;width: 770px">
        <h1 class="cr-cla-t" >Đăng rao vặt</h1>
        <div class="cr-cla-content">
            <?php $form = $this->beginWidget('CActiveForm'); ?>
            <table class="tbl-cr-cla" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2">
                        <?php
                            if(Yii::app()->user->hasFlash('success')):
                                echo '<div class="div-confirm success">'.Yii::app()->user->getFlash('success').'</div>';
                            endif;
                            if(Yii::app()->user->hasFlash('error')):
                                echo '<div class="div-confirm bug">'.Yii::app()->user->getFlash('error').'</div>';
                            endif;
                        ?>
                        <?php if(Yii::app()->user->isGuest): ?>
                            <div class="div-confirm">
                                Đăng tin rao vặt . Bạn phải <a style="color: #0000ff;font-weight: bold;" href="<?php echo Yii::app()->createUrl('account/login') ?>">Đăng nhập</a> hoặc <a href="javascript:;" style="color: #0000ff;font-weight: bold;">đăng ký</a>
                            </div>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td width="100px">Tiêu đề tin : </td>
                    <td><?php echo $form->textField($model, 'title', array('style'=>'width:95%','class' => 'input-cr-cla','id' => 'title-classiads')); ?></td>
                </tr>
                <tr>
                    <td>Danh mục : </td>
                    <td>
                        <?php echo $form->dropDownList($model,'category_id',CHtml::listData(KitCategory::model()->findAll('module="classiads"'),'id','name'), array('prompt'=>'Chọn danh mục')); ?>
                    </td>
                </tr>
                <tr>
                    <td>Nhu cầu : </td>
                    <td>
                        <select class="input-cr-cla" name="KitClassiads[type]" id="type-classiads">
                            <option class="" value="0">=== Chọn nhu cầu ===</option>
                            <option value="buy" <?php echo ($model->type == 'buy') ? 'selected="selected"' : ''; ?>>Cần mua</option>
                            <option value="sell" <?php echo ($model->type == 'sell') ? 'selected="selected"' : ''; ?>>Cần bán</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Điện thoại liên hệ:</td>
                    <td>
                        <?php echo $form->textField($model,'phone',array('id' => 'phone-classiads')) ?>
                    </td>
                </tr>
                <tr>
                    <td>Nội dung đăng</td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2">
<!--                        --><?php //echo $form->error($model,'content'); ?>
                        <?php

                        $this->widget('ext.elrtef.elRTE', array(
                            'model' => $model,
                            'attribute' => 'content',
                            'options' => array(
                                'doctype' => 'js:\'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\'',
                                'cssClass' => 'el-rte',
                                'cssfiles' => array('css/elrte-inner.css'),
                                'absoluteURLs'=>true,
                                'allowSource' => false,
                                'lang' => 'vi',
                                'styleWithCss'=>'',
                                'height' => 200,
                                'fmAllow'=>true, //if you want to use Media-manager
//                                'fmOpen'=>'js:function(callback) {$("<div id=\"elfinder\" />").elfinder(%elfopts%);}',//here used placeholder for settings
                                'toolbar' => 'web2pyToolbar'
                            ),
                        ));
                        ?>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="width: 200px;">
                        <?php echo $form->textField($model,'textCaptcha',array('id' => 'txtCaptcha', 'class' => 'input-cr-cla', 'style' => 'width: 80px;float:left;')); ?>
                        <span style="float: left;"><?php
                            $this->widget('CCaptcha',array(
                                "buttonLabel"=>'',
                                "clickableImage"=>true,
                                "buttonOptions"=>array(
                                    "id"=>"btnrefresh",
                                ),
                                "imageOptions"=>array(
                                    "id"=>"imgrefresh",
                                    "style" => "width:80px;height:20px;border:1px solid #e3e3e3;vertical-align: top;float:left;margin-left:5px;"
                                )
                            ));
                        ?>
                        </span>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="padding-left: 140px">
                        <?php if(!Yii::app()->user->isGuest): ?>
                            <?php echo CHtml::submitButton('ĐĂNG TIN',array('class' => 'btn-create-classiads','name' => 'btn-classiads', 'id' => 'btn-classiads')); ?>
                            <?php if(isset($_GET['id'])): ?>
                            <a href="<?php echo Yii::app()->createUrl('classiads/create/mylist'); ?>" class="btn-create-classiads">Hủy bỏ</a>
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
            <?php $this->endWidget(); ?>
        </div>
    </div>
    <div class="clear"></div>
</div>
<script type="text/javascript">
    $('#btn-classiads').live('click',function(){
        var title = $('#title-classiads').val();
        var category = $('#category-classiads').val();
        var type = $('#type-classiads').val();
        var captcha = $('#txtCaptcha').val();
        var bug ='';
        var i = 0;
        if(title == '') {
            bug +='* Bạn chưa nhập tiêu đề tin \n';
            i++;
        }
        if(category == 0){
            bug += '* Bạn chưa chọn Danh mục \n';
            i++;
        }
        if(type == 0){
            bug += '* Bạn chưa chọn mục Nhu cầu \n';
            i++;
        }
        if(captcha == ''){
            bug +='* Bạn chưa nhập mã xác nhận \n';
            i++;
        }
        if(i == 0){

        } else {
            alert(bug);
            return false;
        }
    });
    function invalid(data)
    {
        var reg=/[0-9]/;
        return reg.test(data);
    }
    $("#phone-classiads").live("keyup", function (e){
        var data=$(this).val();
        var obj=$(this);
        if(!invalid(data))
        {
            obj.val(data.substr(0,data.length-1));
            alert('Phải nhập ký tự số');
        }
    });
</script>