<div id="body-content" style="margin-bottom: 10px;">
    <div class="ct-acc-l">
        <?php $this->renderPartial('//blocks/treeAccount'); ?>
    </div>
    <div class="ct-acc-r">
        <h1 style="text-align: center;color: #D1D1D1;text-shadow:1px 1px white, -1px -1px #333 ;padding-bottom: 15px;">Danh sách Tin rao vặt</h1>
        <table width="100%">
            <thead>
                <tr>
                    <th width="100px">Số thứ tự</th>
                    <th width="570px">Tiêu đề</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <tr>
            <?php
                if(count($model) > 0):
            ?>
            <?php foreach($model as $key => $item){
                $stt = $key + 1;?>
                    <td class="border_td" style="text-align: center"><?php echo $stt; ?></td>
                    <td class="border_td"><a href="<?php echo $item['url']; ?> " target="_blank"><?php echo $item['title']; ?></a></td>
                    <td class="border_td">
                        <a  href="<?php echo Yii::app()->CreateUrl('classiads/create/index',array('id' => $item['id'])); ?>" title="sửa tin">
                        <i class="icon-eye-open icon-acc"></i>
                        </a>
                        <a onclick="return confirm('Bạn có muốn xóa tin này ?');"  href="<?php echo Yii::app()->CreateUrl('classiads/default/delete',array('id' => $item['id'])); ?>" title="xóa tin ">
                            <i class="icon-eye icon-acc"></i>
                        </a>
                    </td>
                </tr>
                <?php } ?>
                  <?php endif;  ?>
            </tbody>
        </table>
    </div>
    <div class="clear"></div>
</div>