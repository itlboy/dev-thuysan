<?php
$cs = Yii::app()->getClientScript();
$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/chitiet.css?ver=10.231');
?>
<!-- Start of detail content page -->

<div id="body-ct">
<div id="ct-header">
    <?php if (isset($breadcrumb[0])): ?>
    <!-- ly do ko hien thi ??? -->
    <div id="ct-breadcrumbs">
        <?php foreach ($breadcrumb[0] as $key => $value): ?>
        <?php if ($key == 0): ?>
                                <div class="main-path" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                                    <a class="path-a" href="<?php echo $value['url']; ?>" itemprop="url" title="<?php echo $value['name']; ?>"><?php echo $value['name']; ?></a>
            <?php else: ?>
            <a class="last" href="<?php echo $value['url']; ?>" itemprop="url" title="<?php echo $value['name']; ?>"><strong itemprop="title"><?php echo $value['name']; ?></strong></a>

                                </div>
                            <?php endif; ?>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
    <?php echo $this->renderPartial('//blocks/social_slider', array('title' => 'Social Slider')); ?>
</div>
<!-- Content -->
<div class="bar-classiads">
    <a href="<?php echo Yii::app()->createUrl('/classiads/default') ?>"><span class="lb">Rao vặt thủy sản</span></a>
    <div class="bar-classiads-up">
        <?php
            echo $raovat = (Yii::app()->user->isGuest) ?
            '<a href="'.Yii::app()->createUrl('classiads/create').'">Đăng rao vặt</a>
            <a href="'.Yii::app()->CreateUrl('account/register') .'">Đăng ký</a>'
            : '<a href="'.Yii::app()->createUrl('classiads/create').'">Đăng rao vặt</a>' ;
        ?>

    </div>
</div>
<div id="ct-content" style="width: 741px;">
    <div class="ct-entry" style="padding: 8px 0px 8px 8px">
        <?php
        $this->widget('classiads.widgets.frontend.classiads_promotion',array(
            'limit' => 7,
        ));
        ?>
        <?php if(!empty($data)): ?>
            <?php
                $i = 0;
                foreach($data as $item): ?>
                <?php
                    $this->widget('classiads.widgets.frontend.classiads_lastest',array(
                        'limit' => 7 ,
                        'view' => 'classiads_lastest_style_1',
                        'title' => $item['name'],
                        'category' => $item['id'],
                        'url' => $item['url'],
                        'index' => $i,
                        'type' => (isset($_GET['type']))? $_GET['type']: '',
                    ));
                    $i++;
                ?>
            <?php endforeach; ?>
        <?php endif; ?>
        <div class="clear"></div>
    </div>

</div>
<!-- END: Content -->

<div id="ct-navbar" style="width: 230px;height: auto;">
    <?php
    $this->widget('ads.widgets.frontend.ads', array(
        'category' => 91,
    )); //Main_Top_Ads
    ?>
</div>

<div class="clear">
</div>
</div>
<!-- End of detail content page -->
