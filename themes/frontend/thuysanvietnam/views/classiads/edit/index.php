<div class="cr-classiads" style="padding-top: 10px">
    <div class="ct-acc-l">
        <?php $this->renderPartial('//blocks/treeAccount'); ?>
    </div>
    <div class="cr-classiads-l" style="float:right;width: 770px">
        <h1 class="cr-cla-t">Sửa tin</h1>
        <div class="cr-cla-content">
            <?php $form = $this->beginWidget('CActiveForm'); ?>
            <table class="tbl-cr-cla" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="2">
                        <?php
                        if(Yii::app()->user->hasFlash('success')):
                            echo '<div class="div-confirm success">'.Yii::app()->user->getFlash('success').'</div>';
                        endif;
                        if(Yii::app()->user->hasFlash('bug')):
                            echo '<div class="div-confirm bug">'.Yii::app()->user->getFlash('bug').'</div>';
                        endif;
                        ?>
                        <?php if(Yii::app()->user->isGuest): ?>
                        <div class="div-confirm">
                            Đăng tin rao vặt . Bạn phải <a style="color: #0000ff;font-weight: bold;" href="<?php echo Yii::app()->createUrl('account/login') ?>">Đăng nhập</a> hoặc <a href="javascript:;" style="color: #0000ff;font-weight: bold;">đăng ký</a>
                        </div>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td width="100px">Tiêu đề tin : </td>
                    <td><?php echo $form->textField($model, 'title', array('style'=>'width:95%','class' => 'input-cr-cla','id' => 'title-classiads')); ?></td>
                </tr>
                <tr>
                    <td>Danh mục : </td>
                    <td>
                        <?php echo $form->dropDownList($model,'category_id',CHtml::listData(KitCategory::model()->findAll(),'id','name'), array('prompt'=>'Chọn danh mục')); ?>
<!--                        <select class="input-cr-cla" name="KitClassiads[category_id]" id="category-classiads">-->
<!--                            <option value="0">=== Chọn danh mục ===</option>-->
<!--                            --><?php //if(!empty($category)): ?>
<!--                            --><?php //foreach($category as $item): ?>
<!--                                <option value="--><?php //echo $item['id']; ?><!--" --><?php //echo ($item['id'] == $model->category_id) ? 'selected="selected"' : ''; ?><!-->--><?php //echo $item['name']; ?><!--</option>-->
<!--                                --><?php //endforeach; ?>
<!--                            --><?php //endif; ?>
<!--                        </select>-->
                    </td>
                </tr>
                <tr>
                    <td>Nhu cầu : </td>
                    <td>
                        <select class="input-cr-cla" name="KitClassiads[type]" id="type-classiads">
                            <option class="" value="0">=== Chọn nhu cầu ===</option>
                            <option value="buy" <?php echo ($model->type == 'buy') ? 'selected="selected"' : ''; ?>>Cần mua</option>
                            <option value="sell" <?php echo ($model->type == 'sell') ? 'selected="selected"' : ''; ?>>Cần bán</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Nội dung đăng</td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <textarea rows="" cols="" name="KitClassiads[content]" class="input-cr-cla" style="width:98%;height: 100px;"></textarea>
                        <!--                        --><?php //echo $form->textareaField($model,'content',array('class'=>"input-cr-cla",'style' => 'width:98%')); ?>
                    </td>
                </tr>
                <tr>
                    <td>

                    </td>
                    <td valign="top" style="">
                        <?php echo $form->textField($model,'textCaptcha',array('id' => 'txtCaptcha', 'class' => 'input-cr-cla', 'style' => 'width: 80px;float:left;')); ?>
                        <span style="float: left;"><?php
                            $this->widget('CCaptcha',array(
                                "buttonLabel"=>'',
                                "clickableImage"=>true,
                                "buttonOptions"=>array(
                                    "id"=>"btnrefresh",
                                ),
                                "imageOptions"=>array(
                                    "id"=>"imgrefresh",
                                    "style" => "width:80px;height:20px;border:1px solid #e3e3e3;vertical-align: top;float:left;margin-left:5px;"
                                )
                            ));
                            ?>
                            </span>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <?php if(!Yii::app()->user->isGuest): ?>
                        <?php echo CHtml::submitButton('ĐĂNG TIN',array('class' => 'btn-create-classiads','name' => 'btn-classiads', 'id' => 'btn-classiads')); ?>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
            <?php $this->endWidget(); ?>
        </div>
    </div>
    <div class="clear"></div>
</div>
<script type="text/javascript">
    $('#btn-classiads').live('click',function(){
        var title = $('#title-classiads').val();
        var category = $('#category-classiads').val();
        var type = $('#type-classiads').val();
        var captcha = $('#txtCaptcha').val();
        var bug ='';
        var i = 0;
        if(title == '') {
            bug +='* Bạn chưa nhập tiêu đề tin \n';
            i++;
        }
        if(category == 0){
            bug += '* Bạn chưa chọn Danh mục \n';
            i++;
        }
        if(type == 0){
            bug += '* Bạn chưa chọn mục Nhu cầu \n';
            i++;
        }
        if(captcha == ''){
            bug +='* Bạn chưa nhập mã xác nhận \n';
            i++;
        }
        if(i == 0){

        } else {
            alert(bug);
            return false;
        }
    });
</script>