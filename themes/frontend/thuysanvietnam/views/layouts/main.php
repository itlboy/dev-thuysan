<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>-->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=8"/>
        <!--<base href="http://localhost/letkit/"/>    -->
        <meta http-equiv="REFRESH" content="1800"/>
        <title><?php echo isset($this->pageTitle) ? CHtml::encode($this->pageTitle) : Yii::app()->name; ?></title>
        <meta name="keywords" content=""/>
        <meta name="description" content="Kênh thông tin nhanh nhất được cập nhật liên tục về lĩnh vực thủy sản, nuôi trồng thủy sản, khai thác thủy sản, khoa học, công nghệ thủy sản ở Việt Nam, Thế giới. Cung cấp thông tin, dịch vụ về kỹ thuật nông nghiệp và thủy sản cho nông dân và các nhà đầu tư ở Việt Nam. Aquaculture & Fishing. Nuôi trồng, khai thác thủy hải sản. Kỹ thuật nuôi tôm, cá & các loại thủy hải sản khác..."/>
        <meta content="no" http-equiv="imagetoolbar"/>
        <?php
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/main.css?ver=10.231');
        $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/letkit.css');
        $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/starrating.css');
        $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/custom.css');
        $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/custom2.css');
        //Back top top css
        $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/reset.css');
        $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/backtop.css');
        //End back to top css
        //$cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/football.css');
        //Sử dụng bản jquery 1.4.4.min.js
        $cs->registerCoreScript('jquery');
//        $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery-1.4.4.js', CClientScript::POS_END);
        //$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.jcarousel.js', CClientScript::POS_END);
        $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.lazyload.js', CClientScript::POS_END);
        $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/slides.jquery.js', CClientScript::POS_END);
        $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/main.js', CClientScript::POS_END);
        $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/backtop.js', CClientScript::POS_END);
        $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/modernizr.js', CClientScript::POS_END);
//        $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/starrating.js',CClientScript::POS_HEAD);
        ?>
        <script type="text/javascript" language="javascript">
            var root_url = "<?php echo Yii::app()->getBaseUrl(true); ?>";

            function trim(str, chars) {
                return ltrim(rtrim(str, chars), chars);
            }
            function ltrim(str, chars) {
                chars = chars || "\\s";
                return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
            }
            function rtrim(str, chars) {
                chars = chars || "\\s";
                return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
            }
            function checkSearch()
            {
                var q = $("#q").val();
                if (q == "Tìm kiếm...") {
                    alert("Bạn chưa nhập từ khóa");
                    return false;
                }
                q = trim(q, ' ');
                q = q.replace(/\s/g, '_');
                q = q.replace('/', '-');
                window.location.href = root_url + '/tim-kiem/' + q + '/';
                return false;
            }
        </script>
        <link rel="shortcut icon" href="favicon.ico"/>

        <!-- Mã theo dõi google analytics thuysanvietnam.com.vn -->
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-20744282-1']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>
        <!-- End  -->
    </head>
    <body>
        <div style="width: 980px; margin: 0 auto; background: #FFF; padding: 0 10px;">
            <div id="box-tras">
                <div id="mainpage">
                    <div id="box">
                            <?php echo $this->renderPartial('//blocks/header', array('title' => 'Header')); ?>
                        <div id="joc-nav" style="overflow: hidden;">
                            <!-- Main navigation -->
                            <?php
                            $category_id = (Yii::app()->request->getQuery('id') !== NULL) ? Yii::app()->request->getQuery('id') : NULL;
                            ?>
                            <?php
                            $this->widget('category.widgets.frontend.treeCategory', array(
                                'module' => 'article',
                                'promotion' => '1',
                                'category' => $category_id,
                            ));
                            ?>
                            <!-- End main navigation -->
                            <div class="nav-sub" style="height: 27px;">
                                <div class="form-search" style="position: relative;z-index: 9;">
                                    <form action="" method="post" name="search" onsubmit="return checkSearch()">
                                        <div class="input">
                                            <p>
                                                <input type="text" style="padding: 3px 0px !important;height: auto !important;" value="<?php echo!empty($_GET['keyword']) ? $_GET['keyword'] : 'Tìm kiếm...' ?>" name="q" id="q" onfocus="if (this.value == 'Tìm kiếm...') {
                                                            this.value = '';}" onblur="if (this.value == '') {
                                                                        this.value = 'Tìm kiếm...';
                                                                    }"/>
                                            </p>
                                        </div>
                                        <input type="submit" name="submit" value=" " style="height: 20px;background-position: 0px -2px" class="submit"/>
                                    </form>
                                </div>
                            </div>
                            <div>
<?php
$this->widget('ads.widgets.frontend.ads', array(
    'category' => 71,
)); //Main_Top_Ads
?>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <!-- Main content -->
                <?php echo $content; ?>
                    </div>
                </div>
            </div>
            <!-- //End main content -->
            <!-- Footer -->
            <div id="footer">
<?php echo $this->renderPartial('//blocks/footer', array('title' => 'Footer')); ?>
            </div>
            <!-- //End Footer -->
            <!--[if IE 6]><script  type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.pngFix.pack.js"></script><![endif]-->
        </div>
        <img border="0" src="http://whos.amung.us/swidget/thuysanvn" width="0" height="0" />
        <!-- Back to top -->
        <a href="#0" class="cd-top">Top</a>
    </body>
</html>