<?php
$cs = Yii::app()->getClientScript();
$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/home.css?ver=10.231');
$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery-1.4.4.js');
$this->pageTitle = 'Tạp chí thủy sản Việt Nam - Kênh thông tin thủy sản ';
?>

<!--        <div class="main-adv-center" style="background: #fff; padding: 5px 0 0;text-align: center">
            <p>
                <embed align="middle" width="980" height="70" quality="high" wmode="transparent" allowscriptaccess="always" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" src="http://xahoi.com.vn/data/adv/thang032012/dt980x70.swf"></embed>
            </p>
        </div>-->
        <div id="body-content">
            <!-- Start Main infomation on top -->

            <?php $this->widget('article.widgets.frontend.promotion', array('view' => 'promotion_6', 'limit' => 3)); ?>

            <div class="box-video">
                <div class="main-adv">
                    <?php
                    $this->widget('ads.widgets.frontend.ads', array(
                        'category' => 65,
                    )); //Home_right_L
                    ?>
                </div>
            </div>
            <!-- End main infomation on top -->
            <div class="clear">
            </div>
            <script type="text/javascript">
                function changePage(id)
                {
                    window.location.href = root_url+$("#select_"+id+" option:selected").val();
                }
                function checkSubmit(){var q=$("#q").val(); if (q == "Tìm kiếm...") {alert("Bạn chưa nhập từ khóa");return false;}; return true;}
            </script>
            <div class="main-adv">
                <!--
                <p style="height: 60px">
                <embed width="980" height="50" src="http://xahoi.com.vn/data/adv/thang042012/BN_980X50.swf" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" allowscriptaccess="always" wmode="transparent" quality="high"></embed>
                </p>
                -->
                <?php
                $this->widget('ads.widgets.frontend.ads', array(
                    'category' => 69,
                )); //Main_Top_Ads
                ?>
            </div>
            <!-- Main new category -->

            <div id="content">
                <?php
                $this->widget('ads.widgets.frontend.ads', array(
                    'category' => 136,
                )); // Phía trước tin mới của từng category
                ?>

                <?php $this->widget('article.widgets.frontend.lastestByCat', array()); ?>
            </div>
            <!-- //End main new categories -->
            <!-- Right blockt -->
            <div id="navbar">
                <!-- Navbar Left -->
                <?php echo $this->renderPartial('//blocks/right_block_l', array('title' => 'L navbar')); ?>

                <!-- //End Navbar Left -->
                <!-- Navbar Right -->
                <?php echo $this->renderPartial('//blocks/right_block_r', array('title' => 'R navbar')); ?>
                <!-- //End Navbar Right -->
            </div>
            <!-- //End right block -->
            <div class="clear">
            </div>
        </div>