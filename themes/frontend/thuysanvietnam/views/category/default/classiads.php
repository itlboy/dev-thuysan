<?php
$cs = Yii::app()->getClientScript();
$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/chitiet.css?ver=10.231');
?>
<!-- Start of detail content page -->

<div id="body-ct">
    <div id="ct-header">
        <?php if (isset($breadcrumb[0])): ?>
        <!-- ly do ko hien thi ??? -->
        <div id="ct-breadcrumbs">
            <?php foreach ($breadcrumb[0] as $key => $value): ?>
            <?php if ($key == 0): ?>
                                <div class="main-path" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                                    <a class="path-a" href="<?php echo $value['url']; ?>" itemprop="url" title="<?php echo $value['name']; ?>"><?php echo $value['name']; ?></a>
                <?php else: ?>
                                    <a class="last" href="<?php echo $value['url']; ?>" itemprop="url" title="<?php echo $value['name']; ?>"><strong itemprop="title"><?php echo $value['name']; ?></strong></a>

                                </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <?php endif; ?>
        <?php echo $this->renderPartial('//blocks/social_slider', array('title' => 'Social Slider')); ?>
    </div>
    <!-- Content -->
    <div class="bar-classiads">
        <a href="<?php echo Yii::app()->createUrl('/classiads/default') ?>">
            <span class="lb">Rao vặt thủy sản</span>
        </a>
        <?php if(!empty($details['name'])): ?>
            <a href="<?php echo !empty($details['url']) ? $details['url'] : 'javascript:;'; ?>">
                <span class="lb"><?php echo !empty($details['name']) ? $details['name'] : ''; ?></span>
            </a>
        <?php endif; ?>
        <?php if(isset($_GET['type'])): ?>
        <a href="<?php echo !empty($details['url']) ? $details['url'].'?type='.$_GET['type'] : 'javascript:;'; ?>">
            <span class="lb"><?php echo (isset($_GET['type']) AND $_GET['type'] == 'sell') ? 'Cần bán' : 'Cần mua'; ?></span>
        </a>
        <?php endif; ?>

        <div class="bar-classiads-up">
            <?php
            echo $raovat = (Yii::app()->user->isGuest) ?
                '<a href="'.Yii::app()->createUrl('classiads/create').'">Đăng rao vặt</a>
            <a href="'.Yii::app()->CreateUrl('account/register') .'">Đăng ký</a>'
                : '<a href="'.Yii::app()->createUrl('classiads/create').'">Đăng rao vặt</a>' ;
            ?>
        </div>
    </div>
    <div id="ct-content" style="width: 741px;">
        <div class="ct-entry" style="padding: 10px;">
            <?php
            $this->widget('classiads.widgets.frontend.classiads_promotion',array(
                'limit' => 7,
                'category' => $category,
            ));
            ?>
            <?php $this->widget('classiads.widgets.frontend.classiads_lastest',array(
                'limit' => 30,
                'view' => 'classiads_lastest_style_2',
                'category' => $category,
                'title' => 'Rao vặt '.$details['name'],
                'type' => (isset($_GET['type'])) ? $_GET['type'] : NULL
            )); ?>
        </div>
    </div>
    <!-- END: Content -->
    <div id="ct-navbar" style="width: 230px;height: 200px">

    </div>

    <div class="clear">
    </div>
</div>
<!-- End of detail content page -->
