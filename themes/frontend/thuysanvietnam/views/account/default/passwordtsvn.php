<div id="body-content" style="margin-bottom: 10px;">
    <div class="ct-acc-l">
        <?php $this->renderPartial('//blocks/treeAccount'); ?>
    </div>
    <div class="ct-acc-r">
    <h1 style="text-align: center;color: #D1D1D1;text-shadow:1px 1px white, -1px -1px #333 ;padding-bottom: 15px;">Thay đổi mật khẩu</h1>
        <?php 
            $form = $this->beginWidget('CActiveForm',array('id' => 'ChangeForm'));
         ?>
         <center>
             <table >
                <tr>
                    <td class="st">Mật khẩu cũ</td>
                    <td class="st"><input type="password" name="KitAccount[password_old]" id="password_old"></td>
                </tr>
                <tr>
                    <td class="st">Mật khẩu mới</td>
                    <td class="st"><input type="password" name="KitAccount[password_new]" id="password_new"> </td>
                </tr>
                <tr>
                    <td class="st">Nhập lại</td>
                    <td class="st"><input type="password" name="KitAccount[password_repeat]" id="password_repeat"></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center;" ><?php echo CHtml::submitButton('Lưu',array('id' => 'changpass','name' => 'changpass','class' => 'btn-create-classiads')); ?></td>
                </tr>
                 <tr>

                     <td colspan="2">
                         <?php
                            if(Yii::app()->user->hasFlash('success')){
                                echo Yii::app()->user->getFlash('success');
                            }
                            if(Yii::app()->user->hasFlash('error')){
                                echo Yii::app()->user->getFlash('error');
                            }
                         ?>
                     <?php
                         echo CHtml::errorSummary($model);
                         ?>
                     </td>
                 </tr>
             </table>
         </center>
         <?php 
            $this->endWidget();
         ?>
    </div>
    <div class="clear"></div>
</div>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#changpass").live("click",function(){
               var password_old =  $("#password_old").val();
               var password_new = $("#password_new").val();
               var password_repeat = $("#password_repeat").val();
               if(password_old == '' || password_new == '' || password_repeat == ''){
                   alert('Phải điền đầy đủ thông tin');
                   return false;
               }
               if(password_new != password_repeat ){
                   alert('Mật khẩu không giống nhau');
                   return false;
               }
            });
        });
    </script>