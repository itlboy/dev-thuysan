<div id="body-content" style="margin-bottom: 10px;">
    <div class="ct-acc-l">
        <?php $this->renderPartial('//blocks/treeAccount'); ?>
    </div>
    <div class="ct-acc-r">
    <h1 style="text-align: center;color: #D1D1D1;text-shadow:1px 1px white, -1px -1px #333 ;padding-bottom: 15px;">Thông tin tài khoản</h1>
        <?php
            $form = $this->beginWidget('CActiveForm',array('id' => 'RegisForm'));
            $styl = array('style' => 'border:1px solid #e3e3e3;color:#333;padding:2px;width:300px;');
         ?>
         <center>
             <table  >
                <tr>
                    <td colspan="2">
                        <div class="bug-reg">
                            <?php echo CHtml::errorSummary($model); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="st" width="150px" >Tài khoản : </td>
                    <td class="st"><strong><?php echo $model->username; ?></strong></td>
                </tr>
                 <tr>
                     <td class="st"><?php echo $form->labelEx($model,'email'); ?></td>
                     <td class="st"><strong><?php echo $model->email; ?></strong></td>
                 </tr>
                <tr>
                    <td class="st">Họ và tên : </td>
                    <td class="st"><?php echo $form->textField($model,'fullname',$styl); ?></td>
                </tr>

                <tr>
                    <td></td>
                    <td><?php echo CHtml::submitButton('Lưu',array(
                        'id' => 'submitL',
                        'name' => 'submitluu',
                        'class' => 'btn-create-classiads',
                    )); ?></td>
                </tr>
             </table>
         </center>
         <?php
            $this->endWidget();
         ?>
    </div>
    <div class="clear"></div>
</div>