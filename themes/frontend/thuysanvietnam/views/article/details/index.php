<?php
$cs = Yii::app()->getClientScript();
$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/chitiet.css?ver=10.231');
$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery-1.4.4.js');
?>
        <!-- Start of detail content page -->
        <div id="body-ct">
            <div id="ct-header">
                <?php if (isset($breadcrumb[0])): ?>
                    <!-- ly do ko hien thi ??? -->
                    <div id="ct-breadcrumbs">
                        <?php foreach ($breadcrumb[0] as $key => $value): ?>
                            <?php if ($key == 0): ?>
                                <div class="main-path" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                                    <a class="path-a" href="<?php echo $value['url']; ?>" itemprop="url" title="<?php echo $value['name']; ?>"><?php echo $value['name']; ?></a>
                                <?php else: ?>
                                    <a class="last" href="<?php echo $value['url']; ?>" itemprop="url" title="<?php echo $value['name']; ?>"><strong itemprop="title"><?php echo $value['name']; ?></strong></a>
<!--                                    <div class="select">
                                        <div class="v-select">
                                            <label>Bình Thuận</label>
                                            <ul>
                                                <li><a title="Hà Nội" href="#">Hà Nội</a></li>
                                                <li><a title="Hồ Chí Minh" href="#">Hồ Chí Minh</a></li>
                                                <li><a title="Đà Nẵng" href="#">Đà Nẵng</a></li>
                                            </ul>
                                        </div>
                                    </div>-->
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            <?php echo $this->renderPartial('//blocks/social_slider', array('title' => 'Social Slider')); ?>
            </div>
            <!-- Content -->
            <div id="ct-content">
<!--                <div class="ct-brea">-->
<!--                </div>-->
                <div class="ct-entry">
<!--                <div class="breadcrums">-->
<!--                    <a href="--><?php //echo Yii::app()->getBaseUrl(); ?><!--" class="breadA">Trang chủ</a>-->
<!--                    <span class="spanBread"></span>-->
<!--                </div>-->
                        <span class="ct-time-public" style="color: #666;font-size: 11px">
							<div style="margin:0 0 10px 0;">
								<?php
								$this->widget('ads.widgets.frontend.ads', array(
									'category' => 102,
								));
								?>
							</div>

                            <?php
                            $day = Yii::t('global', date('l', strtotime ($data["created_time"])));
                            echo $day.', '.date('d/m/Y H:i:s', strtotime ($data["created_time"])) . ' GMT+7';
                            ?>
<!--                            <span> - Lượt xem : --><?php //echo !empty($view_count->view_total) ? $view_count->view_total : 0; ?><!--</span>-->
                            <!-- AddThis Button BEGIN -->
                            <div class="addthis_toolbox addthis_default_style " style="float: right;width: 140px;">
                                <a class="addthis_button_preferred_1"></a>
                                <a class="addthis_button_preferred_2"></a>
                                <a class="addthis_button_preferred_3"></a>
                                <a class="addthis_button_preferred_4"></a>
                                <a class="addthis_button_compact"></a>
                                <a class="addthis_counter addthis_bubble_style"></a>
                            </div>
                            <!-- AddThis Button END -->
                        </span>
                        <?php if(!Yii::app()->user->isGuest AND in_array(8, json_decode(Yii::app()->user->getState('group')))): ?>
                        <button class="thongke" title="Hiển thị thống kê bài viết">Thống kê</button>
                        <div class="clear"></div>
                        <div class="thongkebaiviet" style="display: none;">
                            <div class="thongkebaiviet_child" >
                                <?php if(isset($stats)): ?>
                                <h3>Số liệu thống kê bài viết</h3>
                                <ul>
                                    <li>Đăng bởi <strong><?php echo $user['username']; ?></strong> vào lúc <?php echo (!empty($data['created_time']) AND isset($data['created_time'])) ? date('d/m/Y H:i',strtotime($data['created_time'])) : ''; ?> (<?php echo letDate::fuzzy_span(strtotime($data['created_time'])); ?>)</li>
                                    <li>Truy cập trong ngày: <?php echo $stats->view_intoday; ?></li>
                                    <li>Truy cập trong tuần: <?php echo $stats->view_inweek; ?></li>
                                    <li>Truy cập trong tháng: <?php echo $stats->view_inmonth; ?></li>
                                    <li>Truy cập trong năm: <?php echo $stats->view_inyear; ?></li>
                                    <li>Tất cả truy cập: <?php echo $stats->view_total; ?></li>
                                </ul>
                                <?php endif; ?>
                            </div>
                        </div>
                            <?php endif; ?>
                    <h1 style="margin-bottom: 10px;"><?php echo $data['title']; ?></h1>

                        <strong>Đánh giá bài viết</strong>
                        <script type="text/javascript">
                            // JavaScript Document
                            $(document).ready(function() {
                                // get current rating
                                getRating();
                                // get rating function
                                function getRating(){
                                    $.ajax({
                                        type: "GET",
                                        url: "<?php echo Yii::app()->CreateUrl('article/ajax/rating'); ?>",
                                        data: {id:<?php echo $data['id']; ?>},
                                        cache: false,
                                        async: false,
                                        success: function(result) {
                                            eval('data='+result)
                                            // apply star rating to element
                                            $("#current-rating").css({ width: "" + data.aver + "%" });
                                            $('.tb').html(data.mess);
                                        },
                                        error: function(result) {
                                            $('.fal').html(data.mess1);
                                        }
                                    });
                                }
                                // link handler
                                $('#ratelinks li a').click(function(){
                                    $.ajax({
                                        type: "GET",
                                        url: "<?php echo Yii::app()->CreateUrl('article/ajax/rate'); ?>",
                                        data: "rating="+$(this).text()+"&id=<?php echo $data['id']; ?>",
                                        cache: false,
                                        async: false,
                                        success: function(result) {
                                            // remove #ratelinks element to prevent another rate
                                            $("#ratelinks").remove();
                                            // get rating after click
                                            getRating();
                                        },
                                        error: function(result) {
                                            $('.fal').html(data.mess1);
                                        }
                                    });

                                });
                            });
                        </script>
                        <ul class='star-rating'>
                            <li class="current-rating" id="current-rating"><!-- will show current rating --></li>
                          <span id="ratelinks">
                              <li><a href="javascript:void(0)" title="1 star out of 5" class="one-star">1</a></li>
                              <li><a href="javascript:void(0)" title="2 stars out of 5" class="two-stars">2</a></li>
                              <li><a href="javascript:void(0)" title="3 stars out of 5" class="three-stars">3</a></li>
                              <li><a href="javascript:void(0)" title="4 stars out of 5" class="four-stars">4</a></li>
                              <li><a href="javascript:void(0)" title="5 stars out of 5" class="five-stars">5</a></li>
                          </span>
                        </ul>
                        <span class="tb"></span>
                        <div class="fal"></div>
<!--                        <div>
                            <?php
                            if(Yii::app()->user->hasFlash('error')){
                                echo Yii::app()->user->getFlash('error');
                            }
                            ?>
                        </div>-->
                        <p style="border-bottom: 1px solid #cccccc;margin: 5px 0 ;padding: 0px;"></p>
<?php if(Yii::app()->user->isGuest AND in_array('10',$arrCategory)): ?>
                    <div align="" class="check_login" >
                        Bạn chưa đăng nhập . <a style="color: blue;text-decoration: underline" href="<?php echo Yii::app()->createUrl('account/login'); ?>">Click</a> để đăng nhập
                    </div>
<?php else: ?>

                    <h2 >
                        <?php if(!empty($data['has_brand']) AND $data['has_brand'] == 1 ): ?>
                            (Thủy sản Việt Nam) -
                        <?php endif; ?>
                        <?php echo preg_replace('/\(Thủy(.*?)\)\ -/i','',$data['intro']); ?>
                    </h2>

                    <?php echo $data['content']; ?>

                    <?php if(!empty($data['author']) OR !empty($data['source'])): ?>
                        <div class="ct-author">
                            <!-- Phần này sẽ chỉnh sửa sau -->
                            <?php
                            if(!empty($data['author']))
                            {
                                ?>
                                <span> <?php echo $data['author']; ?></span>
                                <?php
                            }
                            ?>
                            <br style="margin-bottom: 10px;" />
                            <?php
                            if(!empty($data['source']))
                            {
                                ?>
                                <span style="font-weight: normal;font-style: italic;"><span style="font-weight: normal;">Theo</span> <?php echo $data['source']; ?></span>
                                <?php
                            }
                            ?>
                        </div>

                    <?php endif; ?>
                    <div style="width: 648px;">
                        <?php
                        $this->widget('ads.widgets.frontend.ads', array(
                            'category' => 89,
                        )); //Main_Top_Ads
                        ?>

                    </div>
                    <div id="fb-root"></div>
                    <script>
                        (function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = "//connect.facebook.net/vi_VN/all.js#xfbml=1";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));
                    </script>
                    <html xmlns:fb="http://ogp.me/ns/fb#">
                    <div style="margin: 10px;"><fb:like send="true" width="660" show_faces="true"></fb:like></div>
                    <?php $this->renderPartial('_comment',array('data' => $data)); ?>

<!--                    --><?php
//                    $tags = $data['tags'];
//                    $tags = explode(",", $tags);
//                    ?>
<!--                    <div class="ct-tags"><strong>Tags</strong>-->
<!--                        --><?php //foreach ($tags as &$demo) { ?>
<!--                        <a href="article/tag/?tag=--><?php //echo $demo; ?><!--">--><?php //echo $demo; ?><!--</a>-->
<!--                        --><?php //} ?>
<!--                    </div>-->
<!--                    <div class="ct-related"><ul><li><a href="xa-hoi/diem-nong/dong-nai-ngo-doc-ruou-o-dong-nai-hai-nguoi-chet-100641.html" title="Đồng Nai: Ngộ độc rượu ở Đồng Nai, hai người chết">Đồng Nai: Ngộ độc rượu ở Đồng Nai, hai người chết</a></li><li><a href="xa-hoi/diem-nong/tra-chanh-va-nguy-co-nhiem-doc-99149.html" title="Trà chanh và nguy cơ nhiễm độc">Trà chanh và nguy cơ nhiễm độc</a></li><li><a href="xa-hoi/diem-nong/an-trau-chet-60-nguoi-phai-nhap-vien-98966.html" title="Ăn trâu chết, 60 người phải nhập viện">Ăn trâu chết, 60 người phải nhập viện</a></li></ul></div>                          -->



<!--                    <div class="main-detail-share">-->
<!--                        <a href="javascript:print();" title="Bản in" class="print">Bản in</a>-->
<!--                        <a href="mailto:" title="Gửi mail cho bạn bè" class="mail">E-mail</a>-->
<!--                        <a target="_blank" href="http://delicious.com/post?url=#url" title="Chia sẻ --><?php //echo $data['title']; ?><!-- Delicious" class="bit">Delicious</a>-->
<!--                        <a target="_blank" href="http://www.google.com/bookmarks/mark?op=edit&amp;bkmk=--><?php //echo $data['url']; ?><!--&amp;title=--><?php //echo $data['title']; ?><!--" title="Chia sẻ --><?php //echo $data['title']; ?><!-- lên Google" class="buzz">Google</a>-->
<!--                        <a target="_blank" href="http://twitter.com/home?status=--><?php //echo $data['url']; ?><!---Hàng chục giáo viên ngộ độc thực phẩm ở Mũi Né" title="Chia sẻ --><?php //echo $data['title']; ?><!-- lên Twitter" class="twitter">Twitter</a>-->
<!--                        <a target="_blank" href="http://www.facebook.com/share.php?u=--><?php //echo $data['url']; ?><!--&amp;t=--><?php //echo $data['title']; ?><!--" title="Chia sẻ --><?php //echo $data['title']; ?><!-- lên Facebook" class="facebook">Facebook</a>-->
<!--                    </div>-->


<!--                    <div class="ct-qcc">
                        <img src="webskins/skins/news/images/ct/chitiet_xahoi_new_18.jpg" alt="">
                    </div>-->




<!--                    <div id="ct-comment">
                        <h4>Gửi bình luận</h4>
                        <p>
                            <input type="text" onfocus="processFocus('Họ tên','txt_name')" onblur="processBlue('Họ tên','txt_name')" id="txt_name" value="Họ tên" name="InsertCommentSonPC$txtName">
                            <input type="text" onfocus="processFocus('Email','txt_email')" onblur="processBlue('Email','txt_email')" id="txt_email" value="Email" name="InsertCommentSonPC$txtEmail">
                        </p>
                        <p>
                            <textarea onfocus="processFocus('Nội dung','comment');" onblur="processBlue('Nội dung','comment');" id="comment" cols="20" rows="2" name="comment">Nội dung</textarea>
                        </p>
                        <p class="sb">
                            <input type="submit" class="send_button" id="InsertCommentSonPC_btnSubmit" onclick="return send(101192);" value="Gửi" name="InsertCommentSonPC$btnSubmit">
                            <input type="text" onfocus="processFocus('Mã xác nhận','myimg')" onblur="processBlue('Mã xác nhận','myimg')" class="ver" id="myimg" value="Mã xác nhận" name="">
                            <span id="load_captchar"><img src="#" border="0">
                            </span>
                        </p>
                    </div>-->
                    <div class="ct-other ct-other-left">
                        <?php
                        $this->widget('article.widgets.frontend.lastest', array(
                            'view' => 'list_style3',
                            'title' => 'Các tin mới nhất',
                            'category' => NULL,
                            'limit' => 3,
                        ));
                        ?>
                    </div>
                    <div class="ct-other ct-other-left">
                        <?php
                            $this->widget('article.widgets.frontend.lastest', array(
                                'view' => 'list_style3',
                                'title' => 'Các tin cũ cùng mục',
                                'category' => !empty($arrCategory[0]) ? $arrCategory[0] : NULL ,
                                'limit' => 8,
                                'olderId' => $data['id']
                            ));
//                        ?>
                    </div>
                    <?php endif; ?>

                </div>
                <div class="clear"></div>
            </div>
            <!-- END: Content -->
            <div id="ct-navbar">
                <style>
                    .ct-qc p{
                        display:block;
                        padding:3px 5px 3px 5px;
                        float:left;
                        width: 45%;
                    }
                </style>
                <div class="ct-qc">
                    <div class="ads_style2" style="margin-bottom: 5px;">
                        <?php
                        $this->widget('ads.widgets.frontend.ads', array(
                            'category' => 97,
                        ));
                        ?>
                    </div>

                    <div class="ads_style1">
                        <!--Quang cao Top right -->
                        <?php
                        $this->widget('ads.widgets.frontend.ads', array(
                            'category' => 75,
                            'title' => 'Logo Thương hiệu thủy sản',
                        )); //Right_L_top 
                        ?>
                        <!--//End quang cao Top right -->
                    </div>

                    <div class="clear"></div>
                </div>

                <?php
                $this->widget('article.widgets.frontend.lastest', array(
                    'category' => 81,
                    'limit' => 1,
                    'title' => 'Doanh Nghiệp',
                    'view' => 'list_style8'
                ));
                ?>
                
<!--                <div style="width: 300px;margin-bottom: 5px;">
                <?php
//                $this->widget('comment.widgets.frontend.lastestComment', array(
//                    'view' => 'lastestComment_thuysan',
//                ));
                ?>
                </div>-->

                <div class="quangcao_right" style="width: 300px;">
                    <style>
                        .quangcao_right a img {margin-bottom: 5px;}
                    </style>
                    <?php
                    $this->widget('ads.widgets.frontend.ads', array(
                        'category' => 78,
                    )); //Main_Top_Ads
                    ?>
                </div>
				
				<?php
				$this->widget('article.widgets.frontend.lastest', array(
					'view' => 'list_style5',
					'title' => 'Ấn phẩm đã xuất bản',
					'category' => 10,
					'limit' => 3,
				));
				?>
				
                <?php
                $this->widget('article.widgets.frontend.lastest', array(
                    'category' => 25,
                    'view' => 'list_style7',
                    'title' => 'Tư vấn tiêu dùng',
                    'limit' => 9,
                ));
                ?>
                <br />
                <?php
                $this->widget('article.widgets.frontend.lastestViews', array(
                    'view' => 'list_style_ct',
                    'category' => NULL,
                    'title' => 'Đọc nhiều nhất',
                    'limit' => 5,
                ));
                ?>    
<!--                <div class="ct-qc">
                    <img alt="" src="webskins/skins/news/images/ct/chitiet_xahoi_new_23.jpg">
                </div>-->

                <?php /*
                $this->widget('article.widgets.frontend.promotion', array(
                    'view' => 'list_style_ct',
                    'category' => NULL,
                    'title' => 'Tiêu điểm',
                    'limit' => 5,
                )); */
                ?>
            </div>
            <div class="clear">
            </div>
        </div>
<!-- End of detail content page -->
<!-- Script AddThis -->
<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4dd945b77241e40c"></script>
<!-- Script Button END -->
<script type="text/javascript">
    $(document).ready(function(){
        $('.thongke').live('click',function(){
            $('.thongkebaiviet').slideToggle();
        });
    });
</script>