<?php $host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ''; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=8"/>
        <!--<base href="http://localhost/letkit/"/>-->
        <meta http-equiv="REFRESH" content="1800"/>
        <title><?php echo isset($this->pageTitle) ? CHtml::encode($this->pageTitle) : Yii::app()->name; ?></title>
        <meta name="keywords" content=""/>
        <meta name="description" content="Chào mừng quý khách. World of beo love - For Everything Bang & Olufsen. 236 Xã Đàn, Đống Đa, Hà nội. Email: sales@worldofbeolove.com. Tel: 0903400136. 09h30 – 18h00 từ Thứ Hai đến Thứ Bảy. Chuyên các thiết bị Bang & Olufsen" />
        <meta content="no" http-equiv="imagetoolbar"/>
        <?php
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/stylesheet.css');
        $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/let.css');
        $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/scroll.css');
        $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/jquery.fancybox.css');
        $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery-1.4.2.min.js',CClientScript::POS_END);
        $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.imageScroller.js',CClientScript::POS_END);
        $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.fancybox.js',CClientScript::POS_END);
        $cs->registerCoreScript('jquery');
        $cs->registerCoreScript('cookie');
        $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/test.js',CClientScript::POS_END);
        ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".splash div a div").mouseover(function () {
                    var x = -$(this).height();
                    $(this).css({ "background-position": "0px " + x + "px" });
                });
                $(".splash div a div").mouseout(function () {
                    $(this).css({ "background-position": "0px 0px" });
                });
            });
        </script>
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <body>
        <?php echo $content; ?>
    </body>
</html>