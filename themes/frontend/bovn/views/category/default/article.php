<?php
    $options = isset($_GET) ? $_GET : array();
    $category = KitCategory::getListInParent('film');
    $category = KitCategory::treatment($category);

    $options['category'] = isset($options['category']) ? $options['category'] : 0;
?>
<?php $this->renderPartial('//blocks/head'); ?>
<div class="bovn-container">
    <div class="bovn-treecategory" style="float: left;">
        <?php $this->renderPartial('//blocks/left'); ?>
    </div>
    <div id="content" style="width: 660px; float: left;">
        <div id="columns" style="text-align: justify; width: 660px;margin-left: 10px;">
            <?php $this->widget('article.widgets.frontend.listByCat_bovn',array(
                'category' => 99,
                'view' => 'listByCat_bovn',
                'page' => TRUE,
                'limit' => 4,
        )); ?>
        </div>
    </div>
</div>
<div class="clear"></div>
<?php $this->renderPartial('//blocks/footer'); ?>