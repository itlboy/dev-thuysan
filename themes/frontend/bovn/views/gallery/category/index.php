<?php $this->renderPartial('//blocks/head'); ?>
<div class="bovn-container">
    <div class="bovn-treecategory" style="float: left;">
        <?php $this->renderPartial('//blocks/left'); ?>
    </div>
    <div id="content" style="width: 660px; float: left;">
        <div style="width: 660px; float: left;margin-left: 18px;margin-bottom:10px;" class="content" >
            <?php
            $this->widget('category.widgets.frontend.treeCategory',array(
                'module' => 'gallery',
                'view' => 'treeCategory_gallery'
            ));
            ?>
        </div>
    </div>
    <div style="clear: both;"></div>
</div>
<?php $this->renderPartial('//blocks/footer'); ?>