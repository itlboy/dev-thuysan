<?php $this->renderPartial('//blocks/head');
    $cs = Yii::app()->getClientScript();
    $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.fancybox.js');
    $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/jquery.fancybox.css');
?>

<div class="bovn-container">
    <div class="bovn-treecategory" style="float: left;">
        <?php $this->renderPartial('//blocks/left'); ?>
    </div>
    <div id="content" style="width: 660px; float: left;margin-left:18px;">
<!--        --><?php //$this->widget('gallery.widgets.frontend.gallery_bovn',array(
//            'category' => isset($_GET['id']) ? letArray::get($_GET,'id') : NULL,
//        )) ?>
        <?php if(!empty($data)): ?>
        <span style="color:#39B54A;font-size: 14px;font-weight: bold;padding-bottom: 5px; ">Danh mục</span>
        <ul style="margin-top: 5px;">
            <?php foreach($data as $value): ?>
            <li style="float: left;margin: 2px 3px">
                <a style="color: #ffffff;font-size: 12px;" href="<?php echo Yii::app()->createUrl('gallery/default/index',array('id' => $value['id'])); ?>">
                    <?php
                    $this->widget('product.widgets.frontend.image_gallery',array(
                        'category' => $value['id'],
                        'limit' => 1,
                    ));
                    ?>
                </a>
                <div style="text-align: center"><a style="color: #39B54A;font-size: 11px;font-weight: bold" href="<?php echo Yii::app()->createUrl('gallery/default/index',array('id' => $value['id'])); ?>"><?php echo $value['name']; ?></a></div>
            </li>
            <?php endforeach; ?>
        </ul>
        <?php endif;  ?>
    </div>
<div style="clear: both;"></div>
</div>
<?php $this->renderPartial('//blocks/footer'); ?>