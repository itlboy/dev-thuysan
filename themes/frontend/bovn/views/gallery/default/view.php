<?php $this->renderPartial('//blocks/head');
    $cs = Yii::app()->getClientScript();
    $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.fancybox.js');
    $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/jquery.fancybox.css');
?>

<div class="bovn-container">
    <div class="bovn-treecategory" style="float: left;">
        <?php $this->renderPartial('//blocks/left'); ?>
    </div>
    <div id="content" style="width: 660px; float: left;margin-left:18px;">
            <?php $this->widget('gallery.widgets.frontend.gallerybycate',array(
                'category' => $idcate,
                'page' => TRUE,
                'limit' => 12,
            )); ?>

    </div>
<div style="clear: both;"></div>
</div>
<?php $this->renderPartial('//blocks/footer'); ?>