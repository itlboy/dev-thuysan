<?php $this->renderPartial('//blocks/head'); ?>
<div class="bovn-container">
    <div class="bovn-treecategory" style="float: left;">
        <?php $this->renderPartial('//blocks/left'); ?>
    </div>
    <div id="content" style="width: 660px; float: left;">
        <div id="columns" style="text-align: justify; width: 660px;margin-left: 10px;">
            <div>
                <?php $this->widget('article.widgets.frontend.welcome_bovn',array(
                'id' => 2527
            )); ?>
            </div>
            <div class="loading_contact"></div>
            <div class="succses" style="color: #39B54A;font-weight: bold;"></div>
            <div class="error" style="color: red;font-weight: bold;"></div>
            <div class="contact_bovn">
                <form method="post" id="CONTACT">
                    <div class="left_contact_bovn">
                        <span> Họ và tên</span>
                    </div>
                    <div class="right_contact_bovn">
                        <input type="text" class="name" name="Contact[name]" />
                    </div>
                    <div class="clear"></div>
                    <div class="left_contact_bovn">
                        <span> Email</span>
                    </div>
                    <div class="right_contact_bovn">
                        <input type="text" class="mail" name="Contact[mail]" />
                    </div>
                    <div class="clear"></div>
                    <div class="left_contact_bovn">
                        <span>Tiêu đề</span>
                    </div>
                    <div class="right_contact_bovn">
                        <input type="text" class="title" name="Contact[title]" />
                    </div>
                    <div class="clear"></div>
                    <div class="left_contact_bovn" style="color: #39B54A;font-weight: bold;">Nội dung</div>
                    <textarea rows="" cols="" class="content" name="Contact[content]" ></textarea>
                    <div class="clear"></div>
                    <div class="btn_contact"><button type="button" value="Gửi đi" class="contact_submit">Gửi đi</button></div>
                </form>
            </div>
            <script type="text/javascript">
                $(document).ready(function(){
                    $('.contact_submit').live('click',function(){
                        var name = $('.name').val();
                        var mail = $('.mail').val();
                        var title = $('.title').val();
                        var content = $('.content').val();
                        if((name == '') || (mail == '') || (title == '') || (content == '')){
                            alert('Bạn chưa nhập đủ thông tin');
                            return false;
                        }
                        $.ajax({
                            type:'POST',
                            url:'<?php echo Yii::app()->CreateUrl('contact/ajax'); ?>',
                            data:$('#CONTACT').serialize(),
                            beforeSend:function(){
                                $('.loading_contact').html('<center style="margin-top: 10px;"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif" /></center>',5000);
                            },
                            success:function(data){
                                eval('data ='+data);
                                if(data.result == 'true'){
                                    $('.succses').html(data.mess);
                                    setTimeout(function(){$('.succses').html('')},3000);
                                    $('.name, .mail, .title, .content').val('');
                                }else{
                                    $('.error').html(data.mess);
                                    setTimeout(function(){$('.error').html('')},3000);
                                }
                                $('.loading_contact').html('');
                            }
                        });
                        return false;
                    });
                });
            </script>
        </div>
    </div>
</div>
<div class="clear"></div>
<?php $this->renderPartial('//blocks/footer'); ?>