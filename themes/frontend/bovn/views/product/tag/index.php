<?php $this->renderPartial('//blocks/head'); ?>
<div class="bovn-container">
    <div class="bovn-treecategory" style="float: left;">
        <?php $this->renderPartial('//blocks/left'); ?>
    </div>
    <div id="content" style="width: 660px; float: left;">
        <div style="width: 660px; float: left;margin-left: 18px;margin-bottom:10px;" class="content" >
            <?php $this->widget('product.widgets.frontend.search',array(
            'options' => $data['options'],
            'limit' => $find['limit_wg'],
            'new' =>  !empty($find['new']) ? $find['new'] : NULL,
            'old' => !empty($find['old']) ? $find['old'] : NULL,
        )) ?>
        </div>
    </div>
    <div style="clear: both;"></div>
</div>
<?php $this->renderPartial('//blocks/footer'); ?>