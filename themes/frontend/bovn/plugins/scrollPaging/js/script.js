function ajaxSubmitComment(form, url, captcha) {
    var commentWrapper = '#widget-comment-wrapper';
    var commentWrapperItems = '#widget-comment-wrapper .comment-items';
    
    jQuery.ajax({
        'url':url,
        'type':'POST',
        'dataType':'json',
        'data':jQuery('#'+form).serialize(),
        'beforeSend':function() {
            jQuery('.comment-msg').hide().html('Đang xử lý yêu cầu').addClass('comment-msgSuccess').fadeIn(500);            
        },
        'success':function(data) {         
            jQuery('.comment-msg').removeClass('comment-msgError comment-msgSuccess').html(data.message);
            
            if (data.status == 'success') {
                jQuery(commentWrapper).removeClass('dataLoading');
                jQuery('.comment-msg').addClass('comment-msgSuccess').fadeIn(500);
                clearForm('#'+form);                     
                        
                jQuery.ajax({
                    type: 'POST',
                    url: url,
                    beforeSend: function(){
                        jQuery(commentWrapper).addClass('dataLoading');                        
                    },
                    success: function(data){
                        data = jQuery(data);
                        jQuery(commentWrapper).removeClass('dataLoading');                           
                        jQuery(commentWrapperItems).empty();
                        jQuery(data).find('.comment-item').each(function(){
                            jQuery(commentWrapperItems).append(jQuery(this));
                        })
                        jQuery(commentWrapperItems).append('<span class="comment-offset" style="display:none">0</span> ');
                    }
                }); 
            }
            if (data.status == 'error') {
                jQuery('.comment-msg').addClass('comment-msgError').fadeIn(500);             
            }
            
            /*jQuery.ajax({
                url: captcha,
                dataType: "json",
                cache: false,
                success: function(data) {
                    jQuery("div.captcha img").attr("src", data["url"]);
                }
            });*/
        }
    });
}

function clearForm(form) {
    jQuery(form).find(':input').each(function() {
        switch(this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'text':
            case 'textarea':
                jQuery(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
        }
    });
}