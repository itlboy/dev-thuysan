<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=8"/>
        <!--<base href="http://localhost/letkit/"/>-->
        <meta http-equiv="REFRESH" content="1800"/>
        <title>Xã Hội - Cập Nhật Tin Tức 63 Tỉnh Thành, Giải Trí, Bóng Đá, Thời Trang</title>
        <meta name="keywords" content="thong tin,tin tức,giai tri,giải trí,bong da,bóng đá,thoi trang,thời trang,tin tuc 63 tinh thanh,xã hội"/>
        <meta name="description" content="Kênh thông tin nhanh nhất được cập nhật liên tục về xã hội, giải trí,bóng đá, thể thao, thời trang, khoa học, công nghệ ở Việt Nam, Thế giới"/>
        <meta content="no" http-equiv="imagetoolbar"/>
        <link rel="stylesheet" type="text/css" href="css/main.css?ver=10.231"/>
        <link rel="stylesheet" type="text/css" href="css/home.css?ver=10.231"/>
        <script type="text/javascript" src="js/jquery-1.4.4.js"></script>

        <script type="text/javascript">var root_url = "http://http://localhost/letkit/";</script>
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <body>
        <div id="box-tras">
            <div id="mainpage">
                <div id="box">
                        <?php
                        include("blocks/header.php");
                        ?>
                    <div id="joc-nav">
                        <!-- Main navigation -->
                        <?php
                        include ("blocks/navigation.php");
                        ?>
                        <!-- End main navigation -->
                        <div class="nav-sub">
                        </div>
                        <div class="clear">
                        </div>
                    </div>
                    <div class="main-adv-center" style="background: #fff; padding: 5px 0 0;text-align: center">
                        <p>
                            <embed align="middle" width="980" height="70" quality="high" wmode="transparent" allowscriptaccess="always" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" src="http://xahoi.com.vn/data/adv/thang032012/dt980x70.swf"></embed>
                        </p>
                    </div>
                    <div id="body-content">
                        <!-- Start Main infomation on top -->
                        <?php
                        include("blocks/main_infomation.php");
                        ?>
                        <!-- End main infomation on top -->
                        <div class="clear">
                        </div>
                        <!--Start newest -->
                        <div class="box-catnew">
                            <div class="box-catnew-item ">
                                <div class="box-catnew-head">
                                    <h3><a href="xa-hoi/tin-tuc-63-tinh-thanh/" title="Tin tức 63 Tỉnh">Tin tức 63 Tỉnh</a></h3>
                                    <div class="select">
                                        <div class="v-select">
                                            <label>&nbsp;Chọn Tỉnh</label>
                                            <ul>
                                                <li><a title="Hà Nội" href="/xa-hoi/tin-tuc-63-tinh-thanh/80-2/">Hà Nội</a></li>
                                                <li><a title="Hồ Chí Minh" href="/xa-hoi/tin-tuc-63-tinh-thanh/80-3/">Hồ Chí Minh</a></li>
                                                <li><a title="Đà Nẵng" href="/xa-hoi/tin-tuc-63-tinh-thanh/80-4/">Đà Nẵng</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <span>Tin tức 63 Tỉnh</span>
                                </div>
                                <div class="clear">
                                </div>
                                <a href="xa-hoi/diem-nong/nghe-an-phat-hien-xac-dan-ong-treo-co-tren-cay-trong-cong-vien-102394.html" title="Nghệ An: Phát hiện xác đàn ông treo cổ trên cây trong công viên"><img class="bo" width="132" height="97" src="http://image1.xahoi.com.vn/cnn_132x97/2012/6/16/nghe-an-phat-hien-xac-dan-ong-treo-co-tren-cay-trong-cong-vien.jpg" alt="Nghệ An: Phát hiện xác đàn ông treo cổ trên cây trong công viên"/></a>
                                <a href="xa-hoi/diem-nong/nghe-an-phat-hien-xac-dan-ong-treo-co-tren-cay-trong-cong-vien-102394.html" title="Nghệ An: Phát hiện xác đàn ông treo cổ trên cây trong công viên" class="title">Nghệ An: Phát hiện xác đàn ông treo cổ trên cây trong công viên</a>
                                <a href="xa-hoi/giao-thong/xe-ben-de-sap-nha-dan-4-nguoi-thoat-chet-102371.html" title="Xe ben đè sập nhà dân, 4 người thoát chết" class="more2">Xe ben đè sập nhà dân, 4 người thoát chết</a>
                                <a href="xa-hoi/diem-nong/di-cho-ma-tuy-le-sai-thanh-102334.html" title="Đi &quot;chợ&quot; ma tuý lẻ Sài Thành" class="more2">Đi "chợ" ma tuý lẻ Sài Thành</a>
                            </div>
                            <div class="box-catnew-item w223">
                                <div class="box-catnew-head">
                                    <h3><a href="giai-tri/" title="Giải trí">Giải trí</a></h3>
                                    <div class="select">
                                        <div class="v-select">
                                            <label>&nbsp;Chọn danh mục</label>
                                            <ul>
                                                <li><a title="Scandal" href="giai-tri/scandal/">Scandal</a></li>
                                                <li><a title="Chuyện của sao" href="giai-tri/chuyen-cua-sao/">Chuyện của sao</a></li>
                                                <li><a title="Hoa hậu" href="giai-tri/hoa-hau/">Hoa hậu</a></li>
                                                <li><a title="Phim" href="giai-tri/phim/">Phim</a></li>
                                                <li><a title="Âm nhạc" href="giai-tri/am-nhac/">Âm nhạc</a></li>
                                                <li><a title="Thư giãn" href="giai-tri/thu-gian/">Thư giãn</a></li>
                                                <li><a title="Lịch phim chiếu rạp" href="lich-chieu-phim.php" target="_blank">Lịch phim chiếu rạp</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <span>Giải trí</span>
                                </div>
                                <div class="clear">
                                </div>
                                <a href="giai-tri/am-nhac/big-bang-lung-doan-thi-truong-am-nhac-nua-dau-nam-2012-102387.html" title="Big Bang &quot;lũng đoạn&quot; thị trường âm nhạc nửa đầu năm 2012"><img class="bo" width="132" height="97" src="http://image1.xahoi.com.vn/cnn_132x97/2012/6/16/bigbang-1.jpg" alt="Big Bang &quot;lũng đoạn&quot; thị trường âm nhạc nửa đầu năm 2012"/></a>
                                <a href="giai-tri/am-nhac/big-bang-lung-doan-thi-truong-am-nhac-nua-dau-nam-2012-102387.html" title="Big Bang &quot;lũng đoạn&quot; thị trường âm nhạc nửa đầu năm 2012" class="title">Big Bang "lũng đoạn" thị trường âm nhạc nửa đầu năm 2012</a>
                                <a href="giai-tri/chuyen-cua-sao/ngo-kien-huy-bo-khong-tu-quynh-theo-kim-hien-102329.html" title="Ngô Kiến Huy &quot;bỏ&quot; Khổng Tú Quỳnh... theo Kim Hiền" class="more2">Ngô Kiến Huy "bỏ" Khổng Tú Quỳnh... theo Kim Hiền</a>
                                <a href="giai-tri/chuyen-cua-sao/bat-gap-dong-nhi-va-ong-cao-thang-dat-nhau-vao-cho-toi-102324.html" title="Bắt gặp Đông Nhi và Ông Cao Thắng dắt nhau vào chỗ tối" class="more2">Bắt gặp Đông Nhi và Ông Cao Thắng dắt nhau vào chỗ tối</a>
                            </div>
                            <div class="box-catnew-item w240">
                                <div class="box-catnew-head">
                                    <h3><a href="bong-da/" title="Bóng đá">Bóng đá</a></h3>
                                    <div class="select">
                                        <div class="v-select">
                                            <label>&nbsp;Chọn giải</label>
                                            <ul>
                                                <li><a title="Việt Nam" href="bong-da/viet-nam/">Việt Nam</a></li>
                                                <li><a title="Tây Ban Nha" href="bong-da/tay-ban-nha/">Tây Ban Nha</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <span>Bóng đá</span>
                                </div>
                                <div class="clear">
                                </div>
                                <a href="bong-da/euro-2012/video-ukraina-phap-no-luc-102381.html" title="Video Ukraina – Pháp: Nỗ lực"><img class="bo" width="132" height="97" src="http://image1.xahoi.com.vn/cnn_132x97/2012/6/16/A1.jpg" alt="Video Ukraina – Pháp: Nỗ lực"/></a>
                                <a href="bong-da/euro-2012/video-ukraina-phap-no-luc-102381.html" title="Video Ukraina – Pháp: Nỗ lực" class="title">Video Ukraina – Pháp: Nỗ lực</a>
                                <a href="bong-da/tin-nong/nong-tay-ban-nha-se-bao-ve-thanh-cong-ngoi-vo-dich-euro-102375.html" title="NÓNG: Tây Ban Nha sẽ bảo vệ thành công ngôi vô địch Euro" class="more2">NÓNG: Tây Ban Nha sẽ bảo vệ thành công ngôi vô địch Euro</a>
                                <a href="bong-da/euro-2012/cdv-suong-vi-tran-phap-ukraina-bi-hoan-102379.html" title="CĐV &quot;sướng&quot; vì trận Pháp - Ukraina bị hoãn" class="more2">CĐV "sướng" vì trận Pháp - Ukraina bị hoãn</a>
                            </div>
                            <div class="box-catnew-item w2402">
                                <div class="box-catnew-head">
                                    <h3><a href="thoi-trang/" title="Thời trang">Thời trang</a></h3>
                                    <div class="select">
                                        <div class="v-select">
                                            <label>&nbsp;Chọn danh mục</label>
                                            <ul>
                                                <li><a title="Xu hướng - Tin Tức" href="thoi-trang/xu-huong-thoi-trang/">Xu hướng - Tin Tức</a></li>
                                                <li><a title="Thời trang tóc" href="thoi-trang/thoi-trang-toc/">Thời trang tóc</a></li>
                                                <li><a title="Phụ kiện Thời trang" href="thoi-trang/phu-kien-thoi-trang/">Phụ kiện Thời trang</a></li>
                                                <li><a title="Thời trang Công sở" href="thoi-trang/thoi-trang-cong-so/">Thời trang Công sở</a></li>
                                                <li><a title="Thời trang Dạ hội" href="thoi-trang/thoi-trang-da-hoi/">Thời trang Dạ hội</a></li>
                                                <li><a title="Bikini" href="thoi-trang/thoi-trang-bikini/">Bikini</a></li>
                                                <li><a title="Thời trang Sao" href="thoi-trang/thoi-trang-sao/">Thời trang Sao</a></li>
                                                <li><a title="Thời trang Teen" href="thoi-trang/thoi-trang-teen/">Thời trang Teen</a></li>
                                                <li><a title="Thời trang Nội y" href="thoi-trang/thoi-trang-noi-y/">Thời trang Nội y</a></li>
                                                <li><a title="Địa chỉ mua sắm" href="thoi-trang/dia-chi-mua-sam/">Địa chỉ mua sắm</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <span>Thời trang</span>
                                </div>
                                <div class="clear">
                                </div>
                                <a href="thoi-trang/thoi-trang-sao/phung-ngoc-yen-dien-vay-khoet-khoe-vai-tran-goi-cam-102356.html" title="Phùng Ngọc Yến diện váy khoét khoe vai trần gợi cảm"><img class="bo" width="132" height="97" src="http://image1.xahoi.com.vn/cnn_132x97/2012/6/15/ngoc-yen.jpg" alt="Phùng Ngọc Yến diện váy khoét khoe vai trần gợi cảm"/></a>
                                <a href="thoi-trang/thoi-trang-sao/phung-ngoc-yen-dien-vay-khoet-khoe-vai-tran-goi-cam-102356.html" title="Phùng Ngọc Yến diện váy khoét khoe vai trần gợi cảm" class="title">Phùng Ngọc Yến diện váy khoét khoe vai trần gợi cảm</a>
                                <a href="thoi-trang/thoi-trang-teen/vay-tay-bong-giup-ban-gai-khac-phuc-nhuoc-diem-tay-gay-102359.html" title="Váy tay bồng giúp bạn gái khắc phục nhược điểm tay gầy" class="more2">Váy tay bồng giúp bạn gái khắc phục nhược điểm tay gầy</a>
                                <a href="thoi-trang/xu-huong-thoi-trang/bst-resort-calvin-klein-doc-dao-voi-y-tuong-tu-sa-mac-102350.html" title="BST Resort Calvin Klein độc đáo với ý tưởng từ sa mạc" class="more2">BST Resort Calvin Klein độc đáo với ý tưởng từ sa mạc</a>
                            </div>
                        </div>
                        <?php
                        include("blocks/social_slider.php");
                        ?>
                        <!-- //End newest -->
                        <script type="application/javascript">
                            function changePage(id)
                            {
                            window.location.href = root_url+$("#select_"+id+" option:selected").val();
                            }
                            function checkSubmit(){var q=$("#q").val(); if (q == "Tìm kiếm ...") {alert("Bạn chưa nhập từ khóa");return false;}; return true}
                        </script>
                        <div class="main-adv">
                            <p style="height: 60px">
                                <embed width="980" height="50" src="http://xahoi.com.vn/data/adv/thang042012/BN_980X50.swf" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" allowscriptaccess="always" wmode="transparent" quality="high"></embed>
                            </p>
                        </div>
                        <!-- Main new category -->
                        
                        <div id="content">
                            <?php
                            include("blocks/main_content.php");
                            ?>
                        </div>
                        <!-- //End main new categories -->
                        <!-- Right blockt -->
                        <div id="navbar">
                            <!-- Navbar Left -->
                            <?php
                            include("blocks/right_block_l.php");
                            ?>
                            <!-- //End Navbar Left -->
                            <!-- Navbar Right -->
                            <?php
                            include("blocks/right_block_r.php");
                            ?>
                            <!-- //End Navbar Right -->
                        </div>
                        <!-- //End right block -->
                        <div class="clear">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer -->
        <div id="footer">
            <?php
            include("blocks/footer.php");
            ?>
        </div>
        <!-- //End Footer -->
        <script type="text/javascript" src="js/main.js"></script>
        <!--[if IE 6]><script  type="text/javascript" src="js/jquery.pngFix.pack.js"></script><![endif]-->
        <script type="text/javascript" src="js/jquery.lazyload.js"></script>
        <script type="text/javascript" src="js/slides.jquery.js"></script>
        <script type="text/javascript" src="http://media.adnetwork.vn/js/adnetwork.js"></script>
    </body>
</html>