<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?php echo isset($this->pageTitle) ? CHtml::encode($this->pageTitle) : Yii::app()->name; ?></title>
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
<?php
    $cs = Yii::app()->getClientScript();
    $cs->registerCssFile('http://assets.let.vn/css/youtube_core.css');
    $cs->registerCssFile('http://assets.let.vn/css/youtube_guide.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/templates.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/style_1.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/let.css');

    $cs->registerCoreScript('jquery');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/jquery.lazyload.mini.js', CClientScript::POS_END);
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/let.js', CClientScript::POS_END);
    $cs->registerScriptFile(Yii::app()->request->baseUrl.'/js/functions.js', CClientScript::POS_END);
?>
	<!--[if lte IE 7]>
		<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ie.css" type="text/css" />
	<![endif]-->
    
    <script type="text/javascript">
    $(function() {
        $('img.lazy').lazyload({
            effect : 'fadeIn'
        });
        $('img.lazy2').lazyload({
            effect : 'fadeIn'
        });
    });
	var root_url = '<?php echo Yii::app()->getBaseUrl(true); ?>';
    </script>
    
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-41321499-1', 'let.vn');
        ga('send', 'pageview');
    </script>
</head>
<body>    
    <!-- Header -->
    <div class="header">
        <div class="fix_width" style="position:relative;">
            <div class="logo fl" onclick="window.location = '<?php echo Yii::app()->getBaseUrl(true); ?>'"></div>
            <div class="link fr">                
                <?php $this->widget('application.modules.account.widgets.WidgetLoginAccount'); ?>
            </div>
            <div class="nguago"></div>
        </div>
    </div>
    <!-- END Header -->
    
    <!-- Body -->
    <div class="container">
        <?php echo $content; ?>
    </div>
    <!-- END Body -->

    <!-- Footer -->
    <div class="footer" style="background: none;">
        <div class="fix_width clearfix">
            <div class="fr info" style="text-align: right; color: #666;">
                <div style="font-weight: bold;">Bản quyền thuộc về iLet Media</div>
                <div>Website được xây dựng trên hệ thống <span class="labelinfo">Letkit v1.0 Beta</span></div>
                <div>Website đang trong quá trình xây dựng và thử nghiệm. Mọi chi tiết xin liên hệ</div>
                <div><span class="labelinfo">[M]</span> 0988.631988 - 0908.631988 | <span class="labelinfo">[E]</span> nguago@let.vn | <span class="labelinfo">[Y!M]</span> letnguago</div> 
            </div>

        </div>
    </div>
    <!-- END Footer -->    
</body>
</html>