<div class="fix_width body">
    <div class="fl col_s1 acc">
        <?php $this->renderPartial('//blocks/menu_left'); ?>
    </div>

    <div class="fr  ct-r" style="width:765px;">

        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
//            'action' => Yii::app()->createUrl('account/default/login'),
            'id'=>'inlineForm',
            'type'=>'inline',
            'htmlOptions'=>array('class'=>'well',
            ),
        )); ?>
        <h1 class="title-register">Sửa Thông tin tài khoản</h1>
        <table id="yw0" class="detail-view table table-striped table-condensed">
            <tr class="odd">
                <th>Tài khoản :</th>
                <td><strong><?php echo $model->username; ?></strong></td>
            </tr>
            <tr class="odd">
                <th>Email :</th>
                <td><?php echo $model->email; ?></td>
            </tr>
            <tr class="even">
                <th>Tên đầy đủ :</th>
                <td><?php echo $form->textFieldRow($model, 'fullname'); ?></td>
            </tr>
            <tr class="even">
                <th>Điện thoại :</th>
                <td><?php echo $form->textFieldRow($model, 'phone'); ?></td>
            </tr>
            <tr class="odd">
                <th>Ngày sinh :</th>
                <td>
<!--                    --><?php //echo $form->textFieldRow($model, 'birthday'); ?>
                    <div class="ct-birthday">
                        <select name="KitAccount[date]" id="birthday-date" class="sl-birthday" style="width: 80px">
                        </select>
                        <select name="KitAccount[month]" onchange="return dateChange('birthday-date','birthday-month','birthday-year')" id="birthday-month" class="sl-birthday"  style="width: 80px">
                        </select>
                        <select name="KitAccount[year]" onchange="return dateChange('birthday-date','birthday-month','birthday-year')" id="birthday-year" class="sl-birthday"  style="width: 80px" >
                        </select>
                    </div>
                </td>
            </tr>
            <tr class="even">
                <th>Giới tính :</th>
                <td>
                    <select id="drop_sex" name="KitAccount[sex]" style="width:107px;">
                        <option selected="selected" value="0" >--Giới tính --</option>
                        <option value="1" <?php echo (!empty($model->sex) AND $model->sex == 1) ? 'selected="selected"' : ''; ?>>Nam</option>
                        <option value="2" <?php echo (!empty($model->sex) AND $model->sex == 2) ? 'selected="selected"' : ''; ?>>Nữ</option>
                    </select>
                </td>
            </tr>
            <tr class="odd">
                <th>Số CMND :</th>
                <td><?php echo $form->textFieldRow($model, 'idcard'); ?></td>
            </tr>
            <tr class="odd">
                <th>ZipCode :</th>
                <td><?php echo $form->textFieldRow($model, 'zipcode'); ?></td>
            </tr>
            <tr class="odd">
                <th>Tỉnh thành :</th>
                <td><?php echo $form->dropDownList($model,'location_id',CHtml::listData(KitLocation::model()->findAll('status = 1 AND parent_id = 1 ORDER BY name'),'id','name'), array('prompt'=>'Chọn tỉnh thành')); ?></td>
            </tr>
            <tr class="odd">
                <th>Địa chỉ :</th>
                <td><?php echo $form->textArea($model, 'address'); ?></td>
            </tr>
<!--            <tr class="odd">-->
<!--                <th>Nguyên quán :</th>-->
<!--                <td>--><?php //echo $form->textArea($model, 'birthprovince'); ?><!--</td>-->
<!--            </tr>-->
<!--            <tr class="odd">-->
<!--                <th>Thường trú :</th>-->
<!--                <td>--><?php //echo $form->textArea($model, 'resideprovince'); ?><!--</td>-->
<!--            </tr>-->
            <tr style="background-color: #F5f5f5">
                <td style="padding-right: 0px;" colspan="2"><p class="btn-edit" style="text-align: right"><?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'icon'=>'arrow-right', 'label'=>'Lưu thông tin')); ?></p></td>
            </tr>

<!--            <tr class="odd">-->
<!--                <th>Công ty bạn đang công tác :</th>-->
<!--                <td>--><?php //echo $form->textFieldRow($model, 'company'); ?><!--</td>-->
<!--            </tr>-->
<!--            <tr class="odd">-->
<!--                <th>Nghề nghiệp :</th>-->
<!--                <td>--><?php //echo $form->textFieldRow($model, 'occupation'); ?><!--</td>-->
<!--            </tr>-->
            <tr class="odd">
                <th>Yahoo :</th>
                <td><?php echo $form->textFieldRow($model, 'yahoo'); ?></td>
            </tr>
            <tr class="odd">
                <th>Trình độ :</th>
                <td>
<!--                    --><?php //echo $form->textFieldRow($model, 'education'); ?>
                    <select name="KitAccount[education]">
                        <option selected="selected" value="0">Trình độ học vấn</option>
                        <option value="1" <?php echo (isset($model->education) AND !empty($model->education) AND (int)$model->education == 1) ? 'selected="selected"' : ''; ?> >Lao động phổ thông</option>
                        <option value="2" <?php echo (isset($model->education) AND !empty($model->education) AND (int)$model->education == 2) ? 'selected="selected"' : ''; ?> >Chứng chỉ</option>
                        <option value="3" <?php echo (isset($model->education) AND !empty($model->education) AND (int)$model->education == 3) ? 'selected="selected"' : ''; ?> >Trung học</option>
                        <option value="4" <?php echo (isset($model->education) AND !empty($model->education) AND (int)$model->education == 4) ? 'selected="selected"' : ''; ?> >Trung cấp</option>
                        <option value="5" <?php echo (isset($model->education) AND !empty($model->education) AND (int)$model->education == 5) ? 'selected="selected"' : ''; ?> >Cao đẳng</option>
                        <option value="6" <?php echo (isset($model->education) AND !empty($model->education) AND (int)$model->education == 6) ? 'selected="selected"' : ''; ?> >Đại học</option>
                        <option value="7" <?php echo (isset($model->education) AND !empty($model->education) AND (int)$model->education == 7) ? 'selected="selected"' : ''; ?> >Cao học</option>
                    </select>
                </td>
            </tr>
            <tr class="odd">
                <th>Nhóm máu của bạn :</th>
                <td>
<!--                    --><?php //echo $form->textFieldRow($model, 'bloodtype'); ?>
                    <select name="KitAccount[bloodtype]">
                        <option selected="selected" value="0">Nhóm máu</option>
                        <option value="1" <?php echo (isset($model->bloodtype) AND !empty($model->bloodtype) AND (int)$model->bloodtype == 1) ? 'selected="selected"' : ''; ?>  >O</option>
                        <option value="2" <?php echo (isset($model->bloodtype) AND !empty($model->bloodtype) AND (int)$model->bloodtype == 2) ? 'selected="selected"' : ''; ?> >A</option>
                        <option value="3" <?php echo (isset($model->bloodtype) AND !empty($model->bloodtype) AND (int)$model->bloodtype == 3) ? 'selected="selected"' : ''; ?> >B</option>
                        <option value="4" <?php echo (isset($model->bloodtype) AND !empty($model->bloodtype) AND (int)$model->bloodtype == 4) ? 'selected="selected"' : ''; ?> >AB</option>
                    </select>
                </td>
            </tr>
<!--            <tr class="odd">-->
<!--                <th>Cung :</th>-->
<!--                <td>--><?php //echo $form->textFieldRow($model, 'constellation'); ?><!--</td>-->
<!--            </tr>-->
<!--            <tr class="odd">-->
<!--                <th>Tuổi tử vi :</th>-->
<!--                <td>--><?php //echo $form->textFieldRow($model, 'zodiac'); ?><!--</td>-->
<!--            </tr>-->
            <tr class="odd">
                <th>Chiều cao :</th>
                <td>
<!--                    --><?php //echo $form->textFieldRow($model, 'height'); ?>
                    <select name="KitAccount[height]">
                        <option value="0">--Chọn chiều cao--</option>
                        <?php for($i = 140; $i <= 210; $i++): ?>
                        <option value="<?php echo $i; ?>" <?php echo (isset($model->height) AND !empty($model->height) AND (int)$model->height == $i) ? 'selected="selected"' : ''; ?> ><?php echo $i; ?> cm</option>
                        <?php endfor; ?>
                    </select>
                </td>
            </tr>
            <tr class="odd">
                <th>Cân nặng :</th>
                <td>
<!--                    --><?php //echo $form->textFieldRow($model, 'weight'); ?>
                    <select name="KitAccount[weight]">
                        <option value="0">--Chọn cân nặng--</option>
                        <?php for($i = 35; $i <= 150; $i++): ?>
                        <option value="<?php echo $i; ?>" <?php echo (isset($model->weight) AND !empty($model->weight) AND (int)$model->weight == $i) ? 'selected="selected"' : ''; ?> ><?php echo $i; ?> kg</option>
                        <?php endfor; ?>
                    </select></td>
            </tr>
            <tr class="odd">
                <th>Website cá nhân :</th>
                <td><?php echo $form->textFieldRow($model, 'site'); ?></td>
            </tr>
            <tr class="odd">
                <th>Sở thích :</th>
                <td><?php echo $form->textArea($model, 'interest'); ?></td>
            </tr>
        </table>
        <?php if(Yii::app()->user->hasFlash('error')): ?>
            <div>
                <?php echo Yii::app()->user->getFlash('error'); ?>
            </div>
        <?php endif; ?>
        <?php CHtml::errorSummary($model); ?>
        <?php $this->endWidget(); ?>
    </div>

</div>
<?php
    if(!empty($model->birthday)){
        $dateTime = date('d/m/Y',strtotime($model->birthday));
        $dateTime = explode('/',$dateTime);
        $date = $dateTime[0];
        $month = $dateTime[1];
        $year = $dateTime[2];
    }
?>
<script type="text/javascript">
    var date = new Date();
    var year = date.getFullYear() - 10;

    listDateOption(1,31,'Ngày','birthday-date',<?php echo !empty($date) ? $date : 0; ?>);
    listDateOption(1,12,'Tháng','birthday-month',<?php echo !empty($month) ? $month : 0; ?>);
    listDateOption(1950,year,'Năm','birthday-year','<?php echo !empty($year) ? $year : 0; ?>');

    /**
     *
     * @param to  - từ
     * @param from - đến
     * @param title -
     * @param TagId
     */
    function listDateOption(to,from,title,TagId,itemActive){
        var str = '<option selected="selected" value="0">'+title+'</option>';
        for(var i = to; i <= from; i++){
            var selected = '';
            if(itemActive != undefined){
                if(i == itemActive){
                    selected = 'selected="selected"';
                }
            }
            if(i < 10){
                i = '0'+ i;
            }
            str += '<option value="'+i+'" '+selected+'>'+i+'</option>';
        }
        $('#'+TagId).append(str);
    }
</script>