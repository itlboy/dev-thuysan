<div class="fix_width body">
    <div class="fl col_s1 acc">
       <?php $this->renderPartial('//blocks/menu_left'); ?>
    </div>
    <div class="fr " style="width:765px;">
        <h1 class="title-register">Thay đổi thông tin cá nhân</h1>
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                'id'=>'horizontalForm',
                'type'=>'horizontal',
            )
        );echo "\n";
        ?>
        <div class="ct-input">
            <?php echo $form->textFieldRow($model,'username'); ?>
            <?php echo $form->textFieldRow($model,'fullname'); ?>
            <?php echo $form->textFieldRow($model,'email'); ?>
            <div class="control-group ">
                <label for="KitAccount_pass_old" class="control-label">Mật khẩu cũ</label>
                <div class="controls">
                    <input type="password" name="KitAccount[pass_old]" id="Kitaccount_pass_old" />
                </div>
            </div>
            <?php echo $form->passwordFieldRow($model, 'password'); ?>
            <?php echo $form->passwordFieldRow($model, 'password_repeat'); ?>
        </div>
        <div  style="text-align: center;">
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'icon'=>'ok white', 'label'=>'Thay đổi')); ?>
        </div>
        <?php $this->endWidget();echo "\n"; ?>
    </div>
</div>