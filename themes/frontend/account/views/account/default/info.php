<div class="fix_width body">
    <div class="fl col_s1 acc">
        <?php $this->renderPartial('//blocks/menu_left'); ?>
    </div>
    <div class="fr  ct-r" style="width:765px;">
        <h1 class="title-register">Thông tin tài khoản</h1>
        <?php $this->widget('bootstrap.widgets.BootDetailView', array(
            'data'=>array('id'=>$model->id, 'username'=>$model->username, 'fullname'=>$model->fullname, 'email'=>$model->email),
            'attributes'=>array(
                array('name'=>'username', 'label'=>'Full name:'),
                array('name'=>'fullname', 'label'=>'User name:'),
                array('name'=>'email', 'label'=>'Email:'),
            ),
        )); ?>
    </div>
</div>