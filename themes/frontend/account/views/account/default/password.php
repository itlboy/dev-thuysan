<div class="fix_width body">
    <div class="fl col_s1 acc">
        <?php $this->widget('bootstrap.widgets.TbMenu', array(
        'type'=>'list',
        'items'=>array(
            array('label'=>'Xin Chao,Admin'),
            array('label'=>'Thông tin tài khoản', 'icon'=>'home', 'url'=>Yii::app()->createUrl('account/default'), 'active'=>true),
            array('label'=>'Đổi mật khẩu', 'icon'=>'book', 'url'=> Yii::app()->createUrl('account/default/password')),
            array('label'=>'Thoát', 'icon'=>'off', 'url'=> Yii::app()->createUrl('account/ajax/logout')),

            array('label'=>'Chức năng'),
            array('label'=>'Phim theo dõi', 'icon'=>'pencil', 'url'=>'#'),
            array('label'=>'Thông báo từ Admin', 'icon'=>'user', 'url'=>'#'),
            array('label'=>'Tin nhắn', 'icon'=>'cog', 'url'=>'#'),
            array('label'=>'Các bình luận của bạn', 'icon'=>'flag', 'url'=>'#'),
        ),
    )); ?>
    </div>
    <div class="fr " style="width:765px;">
        <h1 class="title-register">Đổi mật khẩu</h1>
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                'id'=>'horizontalForm',
                'type'=>'horizontal',
            )
        );echo "\n";
        ?>
        <div class="ct-input">
            <div class="control-group ">
                <label for="KitAccount_pass_old" class="control-label">Mật khẩu cũ</label>
                <div class="controls">
                    <input type="password" name="KitAccount[pass_old]" id="ForumUcenterMembers_pass_old" />
                </div>
            </div>
            <?php echo $form->passwordFieldRow($model, 'password'); ?>
            <?php echo $form->passwordFieldRow($model, 'password_repeat'); ?>
        </div>
        <div class="ct-btn" style="padding-right: 140px">
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'icon'=>'ok white', 'label'=>'Lưu')); ?>
            <!--            --><?php //$this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset', 'icon'=>'remove', 'label'=>'Làm mới')); ?>
        </div>
        <?php $this->endWidget();echo "\n"; ?>
    </div>
</div>