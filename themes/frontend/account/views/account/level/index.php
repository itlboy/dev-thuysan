<div class="fix_width body">
    <div class="fl col_s1 acc">
        <?php $this->renderPartial('//blocks/menu_left'); ?>
    </div>
    <div class="fr  ct-r  well" style="width:740px;">
        <?php
        $attributes = KitAccountStats::model()->attributeNames();
        $attributesNew = array();
        foreach($attributes as $key => $attribute)
            if ($attribute != 'user_id'){
                $attributesNew[] = $attribute;
            };

        $configs = json_decode($level['config'],true);
        for($i = 0; $i < count($attributesNew); $i++){
            if(!isset($configs[$i]['field'])){
                $configs[$i] = array(
                    'field' => $attributesNew[$i],
                    'comparing' => '',
                    'conditions' => 0,
                );
            }
        }

        $nextConfigs = json_decode($nextlevel['config'],true);
        for($i = 0; $i < count($attributesNew); $i++){
            if(!isset($nextConfigs[$i]['field'])){
                $nextConfigs[$i] = array(
                    'field' => $attributesNew[$i],
                    'comparing' => '',
                    'conditions' => 0,
                );
            }
        }
        ?>
        <div class="padding clearfix">
            <h2 style="font-weight: bold;font-size: 20px; text-align: center">Thông tin cấp độ của bạn</h2>
            <?php if(!empty($level)): ?>
            <table cellpadding="0" cellspacing="0" class="items table table-bordered table-level" style="background-color: white">
                <thead>
                    <tr>
                        <th align="center">Điều kiện </th>
                        <th align="center">Thông tin của bạn</th>
                        <th align="center">Cấp độ tiếp theo</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($attributesNew as $key => $attribute): ?>
                <tr>
                    <td width="150">
                        <?php echo $attribute; ?>
                    </td>
                    <td>
                        <?php echo number_format(KitAccountStats::getField(Yii::app()->user->id,$attribute),0,',','.'); ?>
                    </td>
                    <td>
                        <?php foreach($nextConfigs as $config): ?>
                        <?php echo ($config['field'] == $attribute) ?  $config['comparing'].' '.number_format($config['conditions'],0,',','.') : ''; ?>
                        <?php endforeach; ?>
                    </td>

                </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php endif; ?>
        </div>
        <div class="padding">
            <h2 style="font-weight: bold;font-size: 20px; text-align: center">Danh sách cấp độ</h2>
            <table class="items table table-bordered table-level" style="background-color: white">
                <thead>

                    <th colspan="2" style="text-align: center">Cấp độ</th>
                    <?php foreach($attributesNew as $attribute): ?>
                    <th ><?php echo $attribute; ?></th>
                    <?php endforeach; ?>
                </thead>
                <tbody>
                    <?php foreach($data as $item): ?>
                    <?php
                        $configs = json_decode($item['config'],true);
                        $configsNew = array();
                        foreach($attributesNew as $key => $itemConfig){
                            foreach($configs as $configitem){
                                    if($itemConfig === $configitem['field']){
                                        $configsNew[$key] = array(
                                            'field' => $configitem['field'],
                                            'comparing' => $configitem['comparing'],
                                            'conditions' => $configitem['conditions'],
                                        );
                                    } else {
                                        if(isset($configsNew[$key]['field']) AND $configsNew[$key]['field'] == $itemConfig){

                                        } else {
                                            $configsNew[$key] = array(
                                                'field' => $itemConfig,
                                                'comparing' => '',
                                                'conditions' => 0,
                                            );
                                        }
                                    }
                            }
                        }
                    ?>
                    <tr class="odd">
                        <td width="25"><img src="<?php echo KitAccountLevel::getField($item['id'],'image_url') ?>" width="25" /></td>
                        <td style="width: 60px" ><?php echo $item['title']; ?></td>
                        <?php foreach($attributesNew as $attribute): ?>
                            <?php foreach($configsNew as $config): ?>
                                <?php echo ($config['field'] == $attribute) ? '<td align="center">' . $config['comparing'].' '.number_format($config['conditions'],0,',','.') . '</td>' : ''; ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </tr>
                    <?php endforeach ?>

                </tbody>
            </table>
        </div>
    </div>
</div>