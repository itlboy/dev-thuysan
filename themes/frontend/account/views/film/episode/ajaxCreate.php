<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'kit_film_episode_form',
    'type' => 'horizontal',
    'htmlOptions' => array(
        'onSubmit' => 'js:ajaxSaveForm("KitFilmEpisode" ,jQuery(this).attr("id"), jQuery(this).attr("action")); return false',
    ),
));
?>

<fieldset>
    <?php echo $form->textFieldRow($model, 'title'); ?>
    <?php echo $form->textFieldRow($model, 'sorder'); ?>
    <?php echo $form->textFieldRow($model, 'vip'); ?>
    <?php echo $form->textFieldRow($model, 'link'); ?>    
    <?php echo $form->dropDownListRow($model, 'status', BackendFunctions::getStatusOptions()); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'label' => 'Lưu lại',
        'type' => 'primary',
        'size' => 'small',
    )); ?>
</fieldset>

<?php $this->endWidget(); ?>

<script>
    function copyForm(obj) {
        nextFormId = parseInt(obj.attr('rel')) + 1;
        html = jQuery('#nameOrigin').html();
        html = html.replace(new RegExp('_0_', 'g'), '_'+nextFormId+'_');
        html = html.replace(new RegExp('\\[0\\]', 'g'), '['+nextFormId+']');
        obj.attr('rel', nextFormId);
        jQuery('#nameExtra').append(html);
    }
</script>
