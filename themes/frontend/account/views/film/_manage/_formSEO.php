<fieldset>
    <?php echo $form->textFieldRow($model, 'seo_title'); ?>
    <?php echo $form->textFieldRow($model, 'seo_url'); ?>
    <?php echo $form->textAreaRow($model, 'seo_desc', array('class'=>'span8', 'rows'=>5)); ?>       
</fieldset>
