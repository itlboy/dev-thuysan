<div class="fix_width body">
    <div class="fl col_s1 acc">
        <?php $this->renderPartial('//blocks/menu_left'); ?>
    </div>
    <div class="fr  ct-r" style="width:765px;">
        <div class="well">
            <p class="lk-confirm"><?php echo $model->title; ?></p>
            <div class="ct-mess">
                <table class="table table-striped">
                    <tr>
                        <td style="width:120px"><?php echo $model->getAttributeLabel('title'); ?></td>
                        <td><?php echo $model->title; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $model->getAttributeLabel('intro'); ?></td>
                        <td><?php echo $model->intro; ?></td>
                    </tr>
                    <tr>
                        <td style="width:120px"><?php echo $model->getAttributeLabel('file'); ?></td>
                        <td><?php echo $model->file; ?></td>
                    </tr>
                    <tr>
                        <td style="width:120px"><?php echo $model->getAttributeLabel('type'); ?></td>
                        <td><?php echo $model->type; ?></td>
                    </tr>
                    <tr>
                        <td style="width:120px"><?php echo $model->getAttributeLabel('price'); ?></td>
                        <td><?php echo $model->price; ?></td>
                    </tr>
                    <tr>
                        <td style="width:120px"><?php echo $model->getAttributeLabel('operating_system'); ?></td>
                        <td><?php echo $model->parseOperatingSystemOptions(); ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $model->getAttributeLabel('categories'); ?></td>
                        <td>
                            <?php foreach (KitCategory::model()->getCategoryOptions($this->module->id) as $data) : ?>
                                <?php echo ((in_array($data->id, $model->categories)) ? '<strong><u>' . $data->name . '</u></strong>' : $data->name) . ', '; ?>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $model->getAttributeLabel('content'); ?></td>
                        <td><?php echo $model->content; ?></td>
                    </tr>
                </table>                
            </div>
        </div>
    </div>
</div>
