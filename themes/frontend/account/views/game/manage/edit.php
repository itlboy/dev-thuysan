<div class="fix_width body">
    <div class="fl col_s1 acc">
        <?php $this->renderPartial('//blocks/menu_left'); ?>
    </div>
    <div class="fr  ct-r" style="width:765px;">
        <div class="well">
            <p class="lk-confirm"><?php echo ($model->isNewRecord) ? 'Tạo game' : 'Cập nhật game'; ?></p>
            <div class="ct-mess">
                
                <?php
                $moduleName = 'game';
                $modelName = 'KitGame';
                $formID = 'kit_'.$moduleName.'_form';
                $gridID = 'kit_'.$moduleName.'_grid';
                ?>

                <div class="form-actions">
                    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'button', 'type'=>'primary', 'label'=>Yii::t('global', 'Save'), 'htmlOptions' => array(
                        'onclick' => "js:jQuery('#apply').val(0); jQuery('" . $modelName . "_content').elrte('updateSource'); document.forms['" . $formID . "'].submit();"        
                    ))); ?>
                    <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'button', 'type'=>'primary', 'label'=>Yii::t('global', 'Apply'), 'htmlOptions' => array(
                        'onclick' => "js:jQuery('#apply').val(1); jQuery('" . $modelName . "_content').elrte('updateSource'); document.forms['" . $formID . "'].submit();"        
                    ))); ?>
                </div>

                <div>
                    <label><span>Chọn hoặc kéo thả ảnh để upload</span></label>
                    <div>
                        <?php                    
                        Yii::import("ext.xupload.models.XUploadForm");
                        $uploadModel = new XUploadForm;
                        $this->widget('ext.xupload.XUploadWidget', array(
                            'url' => Yii::app()->createUrl('cms/upload/upload', array('module' => $moduleName)),
                            'model' => $uploadModel,
                            'attribute' => 'file',
                            'multiple' => false,
                            'options' => array(
                                'onComplete' => 'js:function (event, files, index, xhr, handler, callBack) {
                                    if (handler.response.error !== 0) alert(handler.response.error);
                                    else {
                                        $("#'.$modelName.'_image").val(handler.response.name);
                                        $("#ajax_result_image").html();
                                        $("#ajax_result_image").html(\'<li><a rel="collection" href="' . Common::getImageUploaded() . '\'+handler.response.name+\'"><img src="' . Common::getImageUploaded() . '\'+handler.response.name+\'?update='.time().'" height="84" width="148"/><span class="name">Ảnh gốc</span></a></li>\');
                                    }
                                }',
                            )
                        ));
                        ?>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <fieldset>

                </fieldset>

                <?php
                $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                    'id'=> $formID,
                    'type'=>'horizontal',
                    'htmlOptions' => array('enctype'=>'multipart/form-data'),
                ));

                echo CHtml::hiddenField('apply', 0);
                echo $form->hiddenField($model, 'image', array('value' => ''));

                $count = 0;
                $tabs = array();
                $tabs[] = array(
                    'active' => $count++ === 0, // Chả hiểu đây là cái quái gì, mà ko có nó thì Tab ko thể active
                    'label' => Yii::t('global', 'General'),
                    'content' => $this->renderPartial('_formContent', array('form' => $form, 'model' => $model), true),
                ); 
                if (!$model->isNewRecord) {
                //    $tabs[] = array(
                //        'active' => $count++ === 0,
                //        'label' => Yii::t('global', 'Media'),
                //        'content' => $this->widget('application.modules.media.widgets.backend.Media', array(
                //            'module' => $this->module->id,
                //            'itemId' => $model->id,
                //        ), true),
                //    ); 
                //    $tabs[] = array(
                //        'active' => $count++ === 0,
                //        'label' => Yii::t('global', 'Comment'),
                //        'content' => $this->widget('application.modules.comment.widgets.backend.Comment', array(
                //            'module' => $this->module->id,
                //            'itemId' => $model->id,
                //        ), true),
                //    ); 
                }
                $tabs[] = array(
                    'active' => $count++ === 0,
                    'label' => Yii::t('global', 'SEO'),
                    'content' => $this->renderPartial('_formSEO', array('form' => $form, 'model' => $model), true),
                ); 

                $this->widget('bootstrap.widgets.TbTabs', array('tabs' => $tabs));
                $this->endWidget();
                ?> 
                
            </div>
        </div>
    </div>
</div>

<script>
function ajaxDialogForm(grid, title, url, height, width) {
    var ajaxDialog = jQuery('#ajaxPageDialog');
    var gridId = typeof(grid) != 'undefined' ? 'ajax='+grid : '';
    var urlSep = (url.indexOf('?') == -1) ? '?' : '&';
    height = typeof(height) != 'undefined' ? height : 'auto';
    width  = typeof(width) != 'undefined' ? width : 'auto';

    if (ajaxDialog.length == 0)
        ajaxDialog = jQuery('<div id="ajaxPageDialog" style="display:none"></div>').appendTo('body');

    ajaxDialog.empty();
    ajaxDialog.dialog({
        'autoOpen' :false,
        'modal': true,
        'height': height,
        'width': width,
        'title': title,
        'close': function(event, ui) {}
    });
    ajaxDialog.load(url+urlSep+gridId, function() {
        ajaxDialog.dialog('open');
    });
    return false;
}

function ajaxSaveForm(model, form, url) {
    jQuery.ajax({
        'url':url,
        'type':'POST',
        'dataType':'json',
        'data':jQuery('#'+form).serialize(),
        'beforeSend':function(XMLHttpRequest) {
            jQuery('.errorMessage').remove();
        },
        'success':function(data) {
            if (data.status == 'success') {                
                jQuery('#ajaxPageDialog').dialog('close');
                ajaxDialogReload(form.replace('form','grid'));                
            }
            if (data.status == 'error') {
                jrvalue = jQuery.parseJSON(data.jrvalue);
                jQuery.each(jrvalue, function(key, value) {
                    jQuery('#'+form).find('#'+model+'_'+key).val(value);
                });  
                
                jrvalid = jQuery.parseJSON(data.jrvalid);
                jQuery.each(jrvalid, function(key, value) {
                    jQuery('#'+form).find('#'+key).after('<div class="errorMessage">'+value+'</div>');
                });                
            }
            
        }
    });
}

function ajaxDialogReload(grid) {
    var ajaxDialog = jQuery('#ajaxPageDialog');

    if (jQuery('#'+grid).length != 0)
        jQuery.fn.yiiGridView.update(grid);

    if (ajaxDialog.length != 0)
        ajaxDialog.dialog('close');
}

/*
 * Hien dialog xac nhan, cho phep xoa cung luc nhieu row trong gridview
 * @grid id cua gridview
 * @url url den action thuc hien chuc nang cap nhat status
 */
function ajaxGridDelete(module, model, grid, url, field, value, trash) {
    trash = typeof(trash) != 'undefined' ? trash : 0;
    var ajaxDialog = jQuery('#dialogConfirm');
    if (ajaxDialog.length == 0)
        ajaxDialog = jQuery('<div id="dialogConfirm" style="display:none"></div>').appendTo('body');

    ajaxDialog.empty();
    ajaxDialog.html('Are you sure you want to delete?');
    ajaxDialog.dialog({
        'autoOpen' :false,
        'modal': true,
        'minHeight': 140,
        'minWidth': 230,
        'title': 'Confirm',
        'buttons':[{
            text:'Delete',
            click:function() {
                jQuery(this).dialog('close');                
                jQuery.ajax({
                    'url':url,
                    'type':'POST',
                    'cache':false,
                    'data':{
                        'module':module,
                        'model':model,
                        'field':field,
                        'value':value,
                        'trash':trash
                    },'beforeSend':function() {
                    },'success':function() {                        
                        jQuery.fn.yiiGridView.update(grid);                        
                    }
                });
            }
        },{
            text:'No',
            click:function() {
                jQuery(this).dialog('close');
            }
        }]
    });
    ajaxDialog.dialog('open');
    return false;
}

function ajaxChangeStatusValue (module, model, id, field, value, url, grid) {
    jQuery.ajax({
        'url': url,
        'type':'POST',
        'cache':false,
        'data':{
            'module' : module,
            'model' : model,
            'field' : field,
            'value' : value,
            'ids' : id
        },
        'success':function() {
            jQuery('#'+grid).yiiGridView.update(grid);
        }
    });
    return false;
}

</script>
