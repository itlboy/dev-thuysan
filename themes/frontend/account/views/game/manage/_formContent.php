<fieldset>
    <?php echo $form->textFieldRow($model, 'title'); ?>
    
    <?php echo $form->textAreaRow($model, 'intro', array('class'=>'span8', 'rows'=>5)); ?>
    
    <?php echo $form->textFieldRow($model, 'file'); ?>
    <?php echo $form->dropDownListRow($model, 'type', $model->getTypeOptions(), array('empty' => '- Chọn thể loại -')); ?>
    <?php echo $form->dropDownListRow($model, 'price', $model->getPriceOptions(), array('empty' => '- Chọn giá game -')); ?>
    <?php echo $form->dropDownListRow($model, 'operating_system', $model->getOperatingSystemOptions(), array('empty' => '- Chọn hệ điều hành -')); ?>
    
    <?php echo $form->checkBoxListRow($model, 'categories', CHtml::listData(KitCategory::model()->getCategoryOptions($this->module->id), 'id', 'name')); ?>
    
    <?php        //echo $form->textArea($model, 'content', array('rows' => 6, 'cols' => 50, 'class' => 'tinymce'));
        $this->widget('ext.elrtef.elRTE', array(
            'model' => $model,
            'attribute' => 'content',
            'options' => array(
                    'doctype' => 'js:\'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\'',
                    'cssClass' => 'el-rte',
                    'cssfiles' => array('css/elrte-inner.css'),
                    'absoluteURLs'=>true,
                    'allowSource' => true,
                    'lang' => 'vi',
                    'styleWithCss'=>'',
                    'height' => 400,
                    'fmAllow'=>true, //if you want to use Media-manager
                    'fmOpen'=>'js:function(callback) {$("<div id=\"elfinder\" />").elfinder(%elfopts%);}',//here used placeholder for settings
                    'toolbar' => 'maxi',
            ),
            'elfoptions' => array( //elfinder options
                'url' => 'auto',  //if set auto - script tries to connect with native connector
                'passkey' => 'mypass', //here passkey from first connector`s line
                'lang' => 'vi',
                'dialog' => array('width' => '900','modal' => true, 'title' => 'Media Manager'),
                'closeOnEditorCallback' => true,
                'editorCallback' => 'js:callback'
            ),
        ));
    ?>
    
</fieldset>