//$(document).ready(function () {
//    ScrollToTop.setup();
//});
//
//// Scroll
//var b = null,
//    c = $(window),
//    e = false;
//$(window).scroll(function () {
//    var f = c.scrollTop() >= 44;
//    if ($("#CategoriesBarPage #TopNagCallout").length) f = c.scrollTop() >= 80;
//    b || (b = $("#CategoriesBar, .Nag"));
//    if (!e && f) {
//        b.addClass("fixed");
//        e = true
//    } else if (e && !f) {
//        b.removeClass("fixed");
//        e = false
//    }
//});
//
//// Scroll To Top
//var ScrollToTop = ScrollToTop || {
//    setup: function () {
//        var b = $(window).height() / 2;
//        $(window).scroll(function () {
//            (window.innerWidth ? window.pageYOffset : document.documentElement.scrollTop) >= b ? $("#ScrollToTop").removeClass("Offscreen") : $("#ScrollToTop").addClass("Offscreen")
//        });
//        $("#ScrollToTop").click(function () {
//            $("html, body").animate({
//                scrollTop: "0px"
//            }, 400);
//            return false
//        })
//    }
//}

// Make a div follow page scroll, but inside another div
$(window).scroll(function () {
    var scroller = $('#scroller');
    if (scroller.length){
        var topScroller = scroller.position().top; // Khoảng cách từ scroller đến đầu của div cha
        var topParent = scroller.parent().position().top; // Khoảng cách từ Parent đến top
        var hScroller = scroller.height(); // Chiều cao của scroller
        var hParent = scroller.parent().height(); // Chiều cao của Parent
        var topWindow = $(window).scrollTop(); // Scroll windows
        if(topWindow > topParent && topWindow <= (topParent + hParent - hScroller - 20)) {
            scroller.addClass('scroller');
        } else {
            scroller.removeClass('scroller');
        }
    }
});

$(document).ready(function () {
	// Submenu
    $('li').mouseover(function() {
        $(this).children('.submenu').show();
    }).mouseout(function(){
        $(this).children('.submenu').hide();
    });
	
	// mouseover
    $('.mouseover').mouseover(function() {
        $(this).addClass('hover');
    }).mouseout(function(){
        $(this).removeClass('hover');
    });

    $('.close').click(function () {
        $(this).parent().hide();
    });
    
    setTimeout("$('.likefacebook').show()", 3000);
});

