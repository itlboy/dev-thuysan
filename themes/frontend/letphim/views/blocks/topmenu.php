<!-- Top menu -->
<div class="grid_24 topMenu maggin_bottom_5">
    <div class="topmenu_s1 clearfix">
        <div class="topmenu_s1_in">
            <div class="topmenu_s1_chn">
                <ul class="topmenu_s1_chn_ul">
                    <li><a href="<?php echo Yii::app()->getBaseUrl(true); ?>" title="">Trang chủ</a></li>
                    <li><a href="http://phim.let.vn" title="" class="active">Phim</a></li>
                    <!--<li><a href="http://cuoi.let.vn" title="">Cười +</a></li>-->
                    <!--<li><a href="http://cuoi.let.vn/cuoc-thi-tim-kiem-troller-tai-nang.let" title="Cuộc thi ''Tìm kiếm Troller tài năng''" target="_blank">Cuộc thi</a><span class="topmenu_s1_icon_hot">hot</span></li>-->
                    <!--<li><a href="http://me2.vn" title="">Chợ tình</a></li>-->
                    <li class="last"><a href="http://forum.let.vn" title="Vào diễn đàn" target="_blank">Diễn đàn</a></li>
                </ul>
            </div>
            <div class="topmenu_s1_pro">
                <ul class="topmenu_s1_pro_ul">
<!--                    <li><a href="http://id.let.vn" title="Đăng ký tham gia let.vn">Đăng ký</a></li>
                    <li><a href="javascript:void(0);" title="Đăng nhập">Đăng nhập</a></li>--> 
               </ul>
            </div>
        </div>
    </div>
    
    <div class="topmenu_s2 clearfix">
        <ul class="list">
            <li><a href="<?php echo Yii::app()->createUrl('film/search/index'); ?>" title="">Phim mới</a></li>
            <li><a href="<?php echo Yii::app()->createUrl('film/search/index', array('hot' => 1)); ?>" title="">Phim HOT</a></li>
            <li><a href="<?php echo Yii::app()->createUrl('film/search/index', array('length_1' => 1)); ?>" title="">Phim bộ</a></li>
            <li><a href="<?php echo Yii::app()->createUrl('film/search/index', array('length_0' => 1)); ?>" title="">Phim lẻ</a></li>
            <li><a href="<?php echo Yii::app()->createUrl('film/search/index', array('theater' => 1)); ?>" title="">Phim chiếu rạp</a></li>
            <li class="last"><a href="javascript:void(0);" title="">Thể loại phim</a>
                <div class="submenu">
                    <?php $this->widget('category.widgets.frontend.treeCategory', array(
                        'module' => 'film',
                        'view' => 'treeCategory_letphimNew',
                    )); ?>
                </div>
            </li>
        </ul>
    </div>
</div>
<!-- END Top menu -->

            

