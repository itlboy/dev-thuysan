<style>
	body {background: #fff;}
    .list_video_s1 .list .item {margin: 0 30px 20px 0;}
</style>
<div class="grid_24 side_left_bg" style="position:relative;">
    <div class="grid_4 alpha">
        <!-- Menu -->
        <?php $this->widget('category.widgets.frontend.treeCategory', array(
            'module' => 'film',
            'view' => 'treeCategory_letphimNew',
        )); ?>

    </div>
    <div class="grid_20 omega fr">
        <h1 class="title padding_bottom_5 maggin_bottom_15" style="border-bottom: 1px solid #D0D0D0; text-transform: uppercase;">Tìm kiếm</h1>
        <div class="box_s2 maggin_bottom_15">
            <?php $urlSearch = Yii::app()->createUrl('film/search/list'); ?>
            <form id="let_search" onsubmit="submitSearch('let_search', '<?php echo $urlSearch; ?>'); return false;" name="let_search">
                <?php // echo CHtml::dropDownList('category', NULL, CHtml::listData(KitCategory::model()->getCategoryOptions($this->module->id), 'id', 'name'), array('id' => 'let_search_category', 'empty' => 'Tất cả thể loại')); ?>

                <div>
                    <?php
                    $filmLengthSelect = 'all';
                    $filmLengthList = array(
                        'all' => 'Tất cả',
                        'length_1' => 'Phim bộ',
                        'length_0' => 'Phim lẻ',
                    );
                    foreach ($filmLengthList as $key => $value) {
                        if (isset($options[$key]) AND $options[$key] == '1')
                            $filmLengthSelect = $key;
                    }
                    echo CHtml::radioButtonList('filter_length', $filmLengthSelect, $filmLengthList, array(
                        'separator' => '',
                        'template' => '<span>{input} {label}</span>'));
                    ?>
                </div>

                <div>
                    <?php
                    $filmTypeSelect = 'all';
                    $filmTypeList = array(
                        'all' => 'Tất cả',
                        'hot' => 'Phim HOT',
                        'theater' => 'Phim chiếu rạp',
                    );
                    foreach ($filmTypeList as $key => $value) {
                        if (isset($options[$key]) AND $options[$key] == '1')
                            $filmTypeSelect = $key;
                    }
                    echo CHtml::radioButtonList('filter_type', $filmTypeSelect, $filmTypeList, array(
                        'separator' => '',
                        'template' => '<span>{input} {label}</span>'));
                    ?>                    
                </div>

                <a href="javascript:;" onclick="js:submitSearch('let_search', '<?php echo $urlSearch; ?>');">[Tìm kiếm]</a>
                <a href="javascript:;" onclick="js:resetSearch('let_search', '<?php echo $urlSearch; ?>');">[Làm lại]</a>
            </form>
        </div>

        <div id="result_viewlist">
            <?php $this->widget('film.widgets.frontend.searchNew', array('options' => $options)); ?>
        </div>
    </div>
</div>


