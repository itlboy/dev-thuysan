<!-- Phim Hot bộ -->
<?php
$this->widget('film.widgets.frontend.film_lastest',array(
	'title' => 'Phim bộ HOT',
    'limit' => 24, // Số Row
    'group_count' => 6, // 8 Row gộp vào làm 1 group
    'where' => 'hot = 1 AND length = 1',
    'view' => 'list_s2',
));
?>

<!-- Phim Hot lẻ -->
<?php
$this->widget('film.widgets.frontend.film_lastest',array(
	'title' => 'Phim lẻ HOT',
    'limit' => 24, // Số Row
    'group_count' => 6, // 8 Row gộp vào làm 1 group
    'where' => 'hot = 1 AND length = 0',
    'view' => 'list_s2',
));
?>

<!-- Phim mới-->
<?php
$this->widget('film.widgets.frontend.film_lastest',array(
	'title' => 'Phim mới',
    'limit' => 18, // Số Row
    'group_count' => 6, // 8 Row gộp vào làm 1 group
//    'where' => 'hot = 1',
    'view' => 'list_s1',
));
?>

<?php // Common::urlReferer(); ?>