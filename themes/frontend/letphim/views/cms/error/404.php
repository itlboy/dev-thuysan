<link rel="canonical" href="<?php echo Yii::app()->getBaseUrl(true); ?>" />
<div class="fix_width body">
    <?php
    Yii::app()->user->setFlash('error', $error['message']);
    $this->widget('bootstrap.widgets.TbAlert');
    ?>
</div>
