function ajaxChangeStatusValue (module, model, id, field, value, url, grid) {
    jQuery.ajax({
        'url': url,
        'type':'POST',
        'cache':false,
        'data':{
            'module' : module,
            'model' : model,
            'field' : field,
            'value' : value,
            'ids' : id
        },
        'success':function() {
            jQuery.fn.yiiGridView.update(grid);
        }
    });
    return false;
}