<div class="fl col_s1" style="width: 200px;">
    <?php $this->widget('bootstrap.widgets.TbMenu', array(
    'type'=>'list',
    'items'=>array(
        array('label'=>'Xin Chao,Admin'),
        array('label'=>'Thông tin tài khoản', 'icon'=>'home', 'url'=>'#', 'active'=>true),
        array('label'=>'Đổi mật khẩu', 'icon'=>'book', 'url'=>'#'),
        array('label'=>'Thoát', 'icon'=>'book', 'url'=>'#'),

        array('label'=>'Chức năng'),
        array('label'=>'Phim theo dõi', 'icon'=>'pencil', 'url'=>'#'),
        array('label'=>'Thông báo từ Admin', 'icon'=>'user', 'url'=>'#'),
        array('label'=>'Tin nhắn', 'icon'=>'cog', 'url'=>'#'),
        array('label'=>'Các bình luận của bạn', 'icon'=>'flag', 'url'=>'#'),
    ),
)); ?>
</div>