<div class="fix_width body">
    <div class="fl col_s1 acc">
        <?php $this->renderPartial('//blocks/menu_left'); ?>
    </div>
    <div class="fr  ct-r" style="width:765px;">
        <div class="well">
            <p class="lk-confirm">Lịch sử giao dịch</p>
            <div class="ct-mess">
                <table id="yw0" class="detail-view table table-striped table-condensed">

                    <tr>
                        <th style="text-align: center">Thời gian giao dịch</th>
                        <th style="text-align: center">Nội dung giao dịch</th>
                        <th style="text-align: center">Số tiền giao dịch</th>
                    </tr>
                <?php
                    if(!empty($data)){
                        foreach($data as $value){?>
                        <tr>
                            <td style="text-align: center"><?php echo $value['time']; ?></td>
                            <td style="text-align: center"><?php echo $value['content']; ?></td>
                            <td style="text-align: center"><?php echo $value['money']; ?></td>
                        </tr>
                            <?php
                        }
                    }else{?>
                        <tr>
                            <td style="text-align: center;color: red" COLSPAN="3">Bạn chưa thực hiện giao dịch</td>
                        </tr>
                   <?php } ?>
                </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>