<?php
if(isset($_POST['http_referer']))
    $http_referer = $_POST['http_referer'];
elseif(isset($_SERVER['HTTP_REFERER']))
    $http_referer = $_SERVER['HTTP_REFERER'];
else
    $http_referer = '';
?>
<div class="fix_width body">
    <div class="fl col_s1 acc">
        <?php $this->renderPartial('//blocks/menu_left'); ?>
    </div>
    <div class="fr  ct-r" style="width:765px;">
        <div class="well">
            <p class="lk-confirm">Hình thức thanh toán</p>
            <?php if (Yii::app()->user->hasFlash('error')): ?>
            <div class="alert alert-block alert-error fade in">
                <?php echo Yii::app()->user->getFlash('error'); ?>
            </div>
            <?php endif; ?>
            <?php if (Yii::app()->user->hasFlash('success')): ?>
            <div class="alert alert-block alert-success fade in">
                <p><?php echo Yii::app()->user->getFlash('success'); ?></p>
                <?php if(!empty($http_referer)):?>
                    <p>Trở về <a href="<?php echo $http_referer; ?>"><?php echo $http_referer; ?></a></p>
                <?php endif; ?>
            </div>
            <?php endif; ?>
            <form action="<?php echo Yii::app()->createUrl('dev.php/currency/pay/index'); ?>" method="POST" id="formPayment" >
                <input type="hidden" name="http_referer" value="<?php echo $http_referer; ?>" />
                <input id="money" name="money" maxlength="15" type="hidden">
                <table id="yw0" class="detail-view table table-striped table-condensed">
                    <tr class="odd">
                        <th>Tài khoản :</th>
                        <td><strong><?php echo Yii::app()->user->name; ?></strong></td>
                    </tr>
                    <tr class="even">
                        <th>Nạp để :</th>
                        <td>
                            <div class="controls">
                                <?php $randomID = 'r'.rand(10000,99999); ?>
                                <label class="radio">
                                    <input class="radioType" type="radio" name="type" checked="checked" value="money" id="<?php echo $randomID; ?>">
                                    <label for="<?php echo $randomID; ?>">Nạp <?php echo Yii::t('global','letgold'); ?></label>
                                </label>
                                <?php $randomID = 'r'.rand(10000,99999); ?>
                                <label class="radio">
                                    <input class="radioType" <?php echo (isset($_REQUEST['type']) AND $_REQUEST['type'] == 'film_unlimit') ? 'checked ="checked"' : ''; ?> type="radio" name="type" value="film_unlimit" id="<?php echo $randomID; ?>">
                                    <label for="<?php echo $randomID; ?>">Gia hạn xem phim</label>
                                </label>
                                <?php $randomID = 'r'.rand(10000,99999); ?>
<!--                                <label class="radio">
                                    <input class="radioType" <?php echo (isset($_REQUEST['type']) AND $_REQUEST['type'] == 'cs_vip') ? 'checked ="checked"' : ''; ?> type="radio" name="type" value="cs_vip" id="<?php echo $randomID; ?>">
                                    <label for="<?php echo $randomID; ?>">Gia hạn VIP cho Counter-Strike (http://cs.let.vn)</label>
                                </label>-->
                            </div>
                        </td>
                    </tr>
                    <tr class="even">
                        <th>Chọn kiểu thành toán:</th>
                        <td>
                            <div class="btn-group" data-toggle="buttons-radio">
<!--                                <div style="position: relative;">-->
<!--                                    <div class="tooltip fade top in tooltip_baokim" style="top: -35px; left: 120px; display: block; "><div class="tooltip-arrow" style="margin-left: -25px !important;"></div><div class="tooltip-inner">Khuyến mại: Tặng thêm 50%</div></div>-->
                                    <button type="radio" onclick="$('#payment_type').val($(this).val()); return false;"  value="card" class="btn-service pay-card btn btn-primary <?php echo ((isset($_REQUEST['payment_type']) AND $_REQUEST['payment_type'] == 'card')) ? 'active' : ''; ?>">Thẻ cào</button>
<!--                                    <button type="radio" onclick="$('#payment_type').val($(this).val()); return false;"  value="sms" class="btn-service pay-sms btn btn-primary <?php echo (isset($_REQUEST['payment_type']) AND $_REQUEST['payment_type'] == 'sms') ? 'active' : ''; ?>">SMS</button>-->
                                    <button type="radio" onclick="$('#payment_type').val($(this).val()); return false;"  value="baokim" data-title='VNĐ' class="btn-service pay-baokim btn btn-primary <?php echo (!isset($_REQUEST['payment_type']) OR (isset($_REQUEST['payment_type']) AND $_REQUEST['payment_type'] == 'baokim')) ? 'active' : ''; ?>">Bảo Kim</button>
                                    <button type="radio" onclick="$('#payment_type').val($(this).val()); return false;"  value="nganluong" data-title='VNĐ' class="btn-service pay-nganluong btn btn-primary <?php echo (isset($_REQUEST['payment_type']) AND $_REQUEST['payment_type'] == 'nganluong') ? 'active' : ''; ?>">Ngân Lượng</button>
                                    <button type="radio" onclick="$('#payment_type').val($(this).val()); return false;" data-title='Xu'  value="money" class="btn-service pay-money btn btn-primary <?php echo (isset($_REQUEST['payment_type']) AND $_REQUEST['payment_type'] == 'money') ? 'active' : ''; ?>"><?php echo Yii::t('global','letgold'); ?></button>
                                    <input type="hidden" name="payment_type" value="<?php echo (isset($_REQUEST['payment_type']) AND !empty($_REQUEST['payment_type'])) ? $_REQUEST['payment_type'] : 'card'; ?>" id="payment_type">
<!--                                </div>-->
                            </div>
                        </td>
                    </tr>
                </table>
                <div id="result_payment_type">
                    <table id="yw1" class="detail-view table table-striped table-condensed">
                        <tr class="odd type_card type pay-card">
                            <th>Loại thẻ:</th>
                            <td>
                                <div class="btn-group" data-toggle="buttons-radio">
                                    <button type="radio" onclick="$('#payment_card').val($(this).val()); return false;"  value="VTT" class="btn active">Viettel</button>
                                    <button type="radio" onclick="$('#payment_card').val($(this).val()); return false;"  value="VMS" class="btn ">Mobifone</button>
                                    <button type="radio" onclick="$('#payment_card').val($(this).val()); return false;"  value="VNP" class="btn ">Vinaphone</button>
                                    <button type="radio" onclick="$('#payment_card').val($(this).val()); return false;"  value="GATE" class="btn ">Gate FPT</button>
                                    <button type="radio" onclick="$('#payment_card').val($(this).val()); return false;"  value="VTC" class="btn ">Vcoin VTC</button>
                                    <input type="hidden" name="card[type]" value="VTT" id="payment_card">
                                </div>
                            </td>
                        </tr>
                        <tr class="odd type_card type pay-card">
                            <th>Mã thẻ:</th>
                            <td>
                                <input name="card[code]" maxlength="15" type="text">
                            </td>
                        </tr>
                        <tr class="odd type_card type pay-card">
                            <th>Serial:</th>
                            <td>
                                <input name="card[serial]" maxlength="15" type="text">
                            </td>
                        </tr>
                        <tr class="odd type_money type">
                            <th>Bạn còn:</th>
                            <td>
                                <?php  ?>
                                <strong><?php echo (isset($user->money_balance) AND !empty($user->money_balance)) ? '<span id="limit" data-gold="'.KitCmsConfig::convertMoneyToGold($user->money_balance,'default').'">'.number_format(KitCmsConfig::convertMoneyToGold($user->money_balance,'default'),0,',','.').'</span>' : 0; ?> <?php echo Yii::t('global','letgold'); ?></strong>
                            </td>
                        </tr>
                        <tr class="odd type type_money">
                            <th>Bạn muốn nạp:</th>
                            <td colspan="2" align="center">
                                <input id="type_money" name="type_money" maxlength="15" type="text">&nbsp;<strong><?php echo Yii::t('global','letgold'); ?></strong>&nbsp;<span style="font-size: 11px;font-style: italic;">(Tối thiểu 10 <?php echo Yii::t('global','letgold'); ?>)</span>
                                <p class="date"></p>
                            </td>
                        </tr>
                        <tr class="odd type type_baokim" style="<?php echo (!isset($_REQUEST['payment_type']) OR (isset($_REQUEST['payment_type']) AND $_REQUEST['payment_type'] == 'baokim')) ? '' : 'display:none;'; ?>"  >
                            <th>Bạn muốn nạp:</th>
                            <td>
                                <div class="controls">
                                    <label class="radio">
                                        <?php

                                        $rate = KitCmsConfig::getValue('currency_rate','baokim');
                                        $money = array(10000,20000,50000,100000,200000);
                                        ?>
                                        <?php foreach($money as $key => $value): ?>
                                            <?php $randomID = 'r'.rand(10000,99999); ?>
                                            <?php
                                                $gold = ceil(($value * $rate) / 100000);
                                                $gold_promotion = ceil(($value * 100) / 100000);
                                            ?>
                                            <input class="radioGold goldbaokim" type="radio" name="baokim_money" <?php echo ($key == 0) ? 'checked="checked"' : ''; ?> value="<?php echo $value; ?>" id="<?php echo $randomID; ?>">
                                            <label for="<?php echo $randomID; ?>"><?php echo number_format($value,0,',','.'); ?> VNĐ = <span style="color: black"><span style="color:blue;"><?php echo $gold_promotion .'</span> + <span style="color:green;">'.($gold-$gold_promotion); ?><span style="font-size: 11px;"></span></span> <?php echo Yii::t('global','letgold'); ?></span></label>
                                        <?php endforeach; ?>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr class="odd type type_nganluong" style="<?php echo (isset($_REQUEST['payment_type']) AND $_REQUEST['payment_type'] == 'nganluong') ? '' : 'display:none;'; ?>" >
                            <th>Bạn muốn nạp:</th>
                            <td>
                                <div class="controls">
                                    <label class="radio">
                                        <?php

                                        $rate = KitCmsConfig::getValue('currency_rate','nganluong');
                                        $money = array(10000,20000,50000,100000,200000);
                                        ?>
                                        <?php foreach($money as $key => $value): ?>
                                            <?php $randomID = 'r'.rand(10000,99999); ?>
                                            <?php
                                                $gold = ceil(($value * $rate) / 100000);
                                            ?>
                                            <input class="radioGold goldnganluong" type="radio" name="ngaluong_money" <?php echo ($key == 0) ? 'checked="checked"' : ''; ?> value="<?php echo $value; ?>" id="<?php echo $randomID; ?>">
                                            <label for="<?php echo $randomID; ?>"><?php echo number_format($value,0,',','.'); ?> VNĐ = <span style="color: black"><span style="color:blue;"><?php echo $gold; ?></span>&nbsp;<?php echo Yii::t('global','letgold'); ?></span></label>
                                        <?php endforeach; ?>
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr class="odd type type_baokim" style="<?php echo (!isset($_REQUEST['payment_type']) OR (isset($_REQUEST['payment_type']) AND $_REQUEST['payment_type'] == 'baokim')) ? '' : 'display:none;'; ?>">
                            <th></th>
                            <td>

                                <a data-placement="bottom" rel="tooltip" href="http://merchant_cert.lab.baokim.vn/certs/00000/374_CONG_TY_TNHH_TRUYEN_THONG_ILET_MEDIA.jpg" target="_blank" rel="nofollow">Xem chứng nhận thanh toán trực tuyến an toàn</a>

                            </td>
                        </tr>
                        <tr class="odd type type_submit"  style="<?php echo (isset($_REQUEST['payment_type']) AND $_REQUEST['payment_type']=='sms') ? 'display:none;' : ''; ?>">
                            <td align="right" colspan="2">
                                <button type="submit" onclick="return checkForm();" class="btn btn-primary fr btn-submit">Nạp</button>
                                <a href="https://www.baokim.vn/uploads/payment_guide/hdphimletvn.html" target="_blank" rel="nofollow" class="btn btn-primary fr btn-success type_baokim type" style="margin-right: 5px; <?php echo (!isset($_REQUEST['payment_type']) OR (isset($_REQUEST['payment_type']) AND $_REQUEST['payment_type'] == 'baokim')) ? '' : 'display:none;'; ?>">Hướng dẫn thanh toán Bảo Kim</a>
                            </td>
                        </tr>
                        <tr class="odd type_sms type">
                            <td colspan="2">
                                <p style="font-size: 26px; text-align: center"><span style="color: black">Soạn tin: </span><strong style="color: #006AA8;">LET <span style="color: green;<?php echo (!empty($_REQUEST['type']) AND $_REQUEST['type'] == 'film_unlimit') ? '' : 'display: none;'; ?>" class="sms_film_unlimit sms" >PHIM</span> <span style="color: green;<?php echo ((!empty($_REQUEST['type']) AND $_REQUEST['type'] == 'money') OR !isset($_REQUEST['type'])) ? '' : 'display: none;'; ?>" class="sms_money sms">NAP</span> <span style="color: #9203C1;"><?php echo (!Yii::app()->user->isGuest) ? Yii::app()->user->id : 'ID'  ?></span> <span style="color: black;font-weight: normal;">gửi</span> <span style="color: red">8771</span></strong></a></p>
                                <p style="text-align: center;margin: 10px 0px;" ><span class="sms_film_unlimit sms" style="<?php echo (!empty($_REQUEST['type']) AND $_REQUEST['type'] == 'film_unlimit') ? '' : 'display: none;'; ?>">Bạn được xem phim </span><span class="sms_money sms" style="<?php echo ((!empty($_REQUEST['type']) AND $_REQUEST['type'] == 'money') OR !isset($_REQUEST['type'])) ? '' : 'display: none;'; ?>" >Bạn nạp </span><?php echo KitCmsConfig::convertMoneyToGold(15000,'sms'); ?><span class="sms_money sms" style="<?php echo ((!empty($_REQUEST['type']) AND $_REQUEST['type'] == 'money') OR !isset($_REQUEST['type'])) ? '' : 'display: none;'; ?>"> <?php echo Yii::t('global','letgold'); ?> vào tài khoản</span><span class="sms_film_unlimit sms" style="<?php echo (!empty($_REQUEST['type']) AND $_REQUEST['type'] == 'film_unlimit') ? '' : 'display: none;'; ?>"> ngày</span></p>
                            </td>
                        </tr>

                    </table>
                </div>
            </form>
        </div>
    </div>
</div>
<style type="text/css">
    .type_card{
        <?php echo ((isset($_REQUEST['payment_type']) AND $_REQUEST['payment_type'] == 'card')) ? 'display:marker;' : 'display:none;'; ?>
    }
    .type_money{
        <?php echo (isset($_REQUEST['payment_type']) AND $_REQUEST['payment_type'] == 'money') ? 'display:marker' : 'display:none'; ?>
    }
    .type_sms{
        <?php echo (isset($_REQUEST['payment_type']) AND $_REQUEST['payment_type'] == 'sms') ? 'display:marker' : 'display:none'; ?>
    }
    .box-service{
        margin-top: 5px;
    }
    select, textarea, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input{ margin-bottom: 0px;}
</style>

<script type="text/javascript">
    $(document).ready(function(){
        $('.type_<?php echo (isset($_REQUEST['payment_type']) AND $_REQUEST['payment_type']) ? $_REQUEST['payment_type'] : ''; ?>').show();
        $('.title_money').text($('.pay-<?php echo (isset($_REQUEST['payment_type']) AND $_REQUEST['payment_type']) ? $_REQUEST['payment_type'] : ''; ?>').data('title'));
        <?php if((isset($_REQUEST['type']) AND $_REQUEST['type'] == 'money') OR !isset($_REQUEST['type'])): ?>
            $('.pay-money').hide();
        <?php endif; ?>

        $('.btn-service').live('click',function(){
            var type = $(this).val();
            var title = $(this).data('title');
            $('.type').hide();
            $('.type_'+type).show();
            if(type != 'sms'){
                $('.type_submit').show();
            }
            $('.title_money').text(title);
            $('#money').val(10000);
            $('.goldnganluong:eq(0),.goldbaokim:eq(0)').attr('checked','checked');
        });

        $('.radioType').live('change',function(){
            var obj = $(this);
            if(obj.val() == 'money'){
                $('.pay-money').hide();
                if($('.pay-money').hasClass('active')){
                    $('.type_money').hide();
                }
                $('.tooltip_baokim').css({'left':'120px'});
            } else {
                $('.pay-money').show();
                $('.btn-service').each(function(){
                    if($(this).hasClass('active'))
                    {
                        $('.type_'+$(this).val()).show();
                    }
                });
//                $('.tooltip_baokim').css({'left':'180px'});
            }
            $('.sms').hide();
            $('.sms_'+obj.val()).show();
        });

    });
    $('.radioGold').change(function(){
        $('#money').val($(this).val());
    });
    function checkForm(){

        var type = $('.radioType:checked').val();
        if(type == 'money' && $('.pay-money').hasClass('active')){
            alert('Bạn chưa chọn kiểu thanh toán');
            return false;
        }

    }

    function getDate(time){
        var oneDate = 60*60*24;
        var oneHour = 60*60;
        var oneMinute = 60;
        var oneSecond = 60;
        var countdate = 0,countHour=0,countMinute=0,countSecond=0;
        var balance_hour = 0,balance_minute=0,balance_second=0;
        countdate = parseInt(time/oneDate);
        balance_hour = time - (countdate*oneDate);
        countHour = parseInt(balance_hour/oneHour);
        balance_minute = balance_hour - (countHour*oneHour);
        countMinute = parseInt(balance_minute/oneMinute);
        return countdate+' ngày '+countHour+' giờ '+countMinute+' phút';
    }

    function invalid(data)
    {
        var reg=/[a-zA-Z\.]/;
        return reg.test(data);
    }



    $("#type_money").live("keyup","blur", function (e){
        var data=$(this).val();
        var obj=$(this);
        var str1 = data.replace(/[a-z\W]/gi, '');
        obj.val(str1);

        var gold = $(this).val();
        var rate = <?php echo KitCmsConfig::getValue('currency_rate','default'); ?>;

        var limit = parseInt($('#limit').data('gold'));
        var color = 'color:blue';
        var message = '';
        if((gold > limit)){
            color = "color:red";
            message = 'Số <?php echo Yii::t('global','letgold'); ?> nhập vào phải nhỏ hơn số <?php echo Yii::t('global','letgold'); ?> đang có, và tối thiểu 10 <?php echo Yii::t('global','letgold'); ?>';
        } else if(gold != '' || gold > 0) {
            message = gold+' ngày';
        } else {
            message = '';
        }
        $('.date').html('<span style="'+color+'">'+message+'</span>');

    });
</script>