<div class="fix_width body">
    <div class="fl col_s1 acc">
        <?php $this->renderPartial('//blocks/menu_left'); ?>
    </div>
    <div class="fr  ct-r" style="width:765px;">
        <div class="well">
            <p class="lk-confirm">
                <?php
                if (!isset($_GET['status'])) {
                    echo 'Tất cả phim';
                } else {
                    echo ($_GET['status'] == 1) ? 'Phim đã duyệt' : 'Phim chưa duyệt';
                }
                ?>
            </p>
            <div class="ct-mess">
                
                <?php
                $moduleName = 'film';
                $modelName = 'KitFilm';
                $gridID = 'kit_'.$moduleName.'_grid';

                $this->widget('GridView', array(
                    'id' => $gridID,
                    'dataProvider' => $model->search(),
                    'filter' => $model,
                    'columns' => array(
                        array(
                            'class' => 'CCheckBoxColumn',
                            'id' => 'chkIds',
                            'selectableRows' => 2,
                            'htmlOptions' => array('width' => '20px', 'align' => 'center')
                        ),
                        array(
                            'name' => 'id',
                            'value' => '$data->id'
                        ),

                        array(
                            'name' => 'title',
                            'value' => '$data->title'
                        ),

                        array(
                            'name' => 'title_english',
                            'value' => '$data->title_english'
                        ),

                        array(
                            'name' => 'image',
                            'type' => 'raw',
                            'value' => '($data->image != "") ? CHtml::image(Common::getImageUploaded(Yii::app()->controller->module->name . "/small/" . $data->image), "", array("height" => "50")) : ""',
                            'htmlOptions' => array('width' => '50px', 'align' => 'center')
                        ),

                        array(
                            'filter' => CHtml::activeDropDownList($model, 'vip', BackendFunctions::getStatusOptions(), array('prompt' => Yii::t('global', 'Select all'))),
                            'type' => 'raw',
                            'name' => 'vip',
                            'value' => 'BackendFunctions::getBooleanIcon("'.$moduleName.'", "'.$modelName.'", $data->id, "vip", $data->vip, "'.$gridID.'")',
                            'htmlOptions' => array('align' => 'center')
                        ),
                        array(
                            'filter' => CHtml::activeDropDownList($model, 'hot', BackendFunctions::getStatusOptions(), array('prompt' => Yii::t('global', 'Select all'))),
                            'type' => 'raw',
                            'name' => 'hot',
                            'value' => 'BackendFunctions::getBooleanIcon("'.$moduleName.'", "'.$modelName.'", $data->id, "hot", $data->hot, "'.$gridID.'")',
                            'htmlOptions' => array('align' => 'center')
                        ),
                        array(
                            'filter' => CHtml::activeDropDownList($model, 'complete', BackendFunctions::getStatusOptions(), array('prompt' => Yii::t('global', 'Select all'))),
                            'type' => 'raw',
                            'name' => 'complete',
                            'value' => 'BackendFunctions::getBooleanIcon("'.$moduleName.'", "'.$modelName.'", $data->id, "complete", $data->complete, "'.$gridID.'")',
                            'htmlOptions' => array('align' => 'center')
                        ),
                        array(
                            'filter' => CHtml::activeDropDownList($model, 'status', BackendFunctions::getStatusOptions(), array('prompt' => Yii::t('global', 'Select all'))),
                            'type' => 'raw',
                            'name' => 'status',
                            'value' => 'BackendFunctions::getBooleanIcon("'.$moduleName.'", "'.$modelName.'", $data->id, "status", $data->status, "'.$gridID.'")',
                            'htmlOptions' => array('align' => 'center')
                        ),
                        array(
                            'filter' => CHtml::activeDropDownList($model, 'trash', BackendFunctions::getStatusOptions(), array('prompt' => Yii::t('global', 'Select all'))),
                            'type' => 'raw',
                            'name' => 'trash',
                            'value' => 'BackendFunctions::getBooleanIcon("'.$moduleName.'", "'.$modelName.'", $data->id, "trash", $data->trash, "'.$gridID.'")',
                            'htmlOptions' => array('align' => 'center')
                        ),
                        array( // display a column with "view", "update" and "delete" buttons
                            'class' => 'CButtonColumn',
                            'template' => '{update}',
                            'buttons' => array(
                                'gridInfo' => array(
                                    'label' => Yii::t('global', 'View'),
                                    'url' => 'Yii::app()->createUrl("//'.$moduleName.'/'.$this->id.'/info", array("id"=>$data->id))',
                                    'imageUrl'=>Yii::app()->theme->baseUrl.'/images/icons/system/info_16.png'
                                ),
                                'update' => array(
                                    'label' => Yii::t('global', 'Edit'),
                                    'url' => 'Yii::app()->createUrl("//'.$moduleName.'/'.$this->id.'/update", array("id"=>$data->id))',
                                    'imageUrl' => Yii::app()->theme->baseUrl.'/images/icons/system/edit_16.png',
                                    'options' => array('class' => 'btnGridUpdate')
                                ),
                                'delete' => array(
                                    'label' => Yii::t('global', 'Delete'),
                                    'url' => 'Yii::app()->createUrl("//'.$moduleName.'/'.$this->id.'/delete", array("id"=>$data->id))',
                                    'imageUrl' => Yii::app()->theme->baseUrl.'/images/icons/system/delete_16.png',
                                ),
                            ),
                        )
                    ),
                ));
                ?>
                
            </div>
        </div>
    </div>
</div>
