<div class="fix_width body">
    <div class="fl col_s1 acc">
        <?php $this->renderPartial('//blocks/menu_left'); ?>
    </div>
    <div class="fr  ct-r" style="width:765px;">
        <div class="well">
            <p class="lk-confirm"><?php echo $model->title; ?></p>
            <div class="ct-mess">
                <table class="table table-striped">
                    <tr>
                        <td style="width:120px"><?php echo $model->getAttributeLabel('title'); ?></td>
                        <td><?php echo $model->title; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $model->getAttributeLabel('title_english'); ?></td>
                        <td><?php echo $model->title_english; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $model->getAttributeLabel('imdb_grab'); ?></td>
                        <td><?php echo ($model->imdb_grab) ? 'Có' : 'Không'; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $model->getAttributeLabel('imdb_id'); ?></td>
                        <td><?php echo $model->imdb_id; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $model->getAttributeLabel('actor'); ?></td>
                        <td><?php echo $model->actor; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $model->getAttributeLabel('intro'); ?></td>
                        <td><?php echo $model->intro; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $model->getAttributeLabel('theater'); ?></td>
                        <td><?php echo ($model->theater) ? 'Có' : 'Không'; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $model->getAttributeLabel('length'); ?></td>
                        <td><?php echo ($model->length) ? 'Có' : 'Không'; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $model->getAttributeLabel('vip'); ?></td>
                        <td><?php echo ($model->vip) ? 'Có' : 'Không'; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $model->getAttributeLabel('episode_total'); ?></td>
                        <td><?php echo $model->episode_total; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $model->getAttributeLabel('episode_current'); ?></td>
                        <td><?php echo $model->episode_current; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo $model->getAttributeLabel('categories'); ?></td>
                        <td>
                            <?php foreach (KitCategory::model()->getCategoryOptions($this->module->id) as $data) : ?>
                                <?php echo ((in_array($data->id, $model->categories)) ? '<strong><u>' . $data->name . '</u></strong>' : $data->name) . ', '; ?>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php echo $model->getAttributeLabel('content'); ?></td>
                        <td><?php echo $model->content; ?></td>
                    </tr>
                </table>                
            </div>
        </div>
    </div>
</div>
