<div class="fix_width body">
    <div class="fl col_s1 ctimg">
        <img src="<?php echo Yii::app()->theme->baseUrl ?>/images/banner.png" />
    </div>
    <div class="fr" style="width:300px; margin-top:10px;">
        <div class="box-forgot">
            <?php
            $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                    'id'=>'horizontalForm',
                    'type'=>'inline',
                    'htmlOptions' => array('class' => 'well'),
                )
            );echo "\n";
            ?>

            <?php echo $form->passwordFieldRow($model,'password'); ?>
            <?php echo $form->passwordFieldRow($model,'password_repeat'); ?>
            <div class="ct-btn btn-submit" style="margin: 0px;text-align: right;margin-top: 10px;" >
                <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'icon'=>'ok white', 'label'=>'Nhập mật khẩu mới')); ?>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>