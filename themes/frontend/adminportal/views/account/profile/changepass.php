<div class="fix_width body">
    <div class="fl col_s1 acc">
        <?php $this->renderPartial('//blocks/menu_left'); ?>
    </div>
    <div class="fr  ct-r" style="width:765px;">
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
//            'action' => Yii::app()->createUrl('account/default/login'),
            'id'=>'inlineForm',
            'type'=>'inline',
            'htmlOptions'=>array('class'=>'well',
            ),
        )); ?>
        <h1 class="title-register">Đổi mật khẩu</h1>
        <table id="yw0" class="detail-view table table-striped table-condensed">
            <tr class="odd">
                <th>Mật khẩu cũ :</th>
                <td><input type="password" name="KitAccount[password_old]" id="password_old" /></td>
            </tr>
            <tr class="even">
                <th>Mật khẩu mới :</th>
                <td><input type="password" name="KitAccount[password_new]" id="password_new" /></td>
            </tr>
            <tr class="odd">
                <th>Nhập lại mật khẩu :</th>
                <td><input type="password" name="KitAccount[password_repeat]" id="password_repeat"  /></td>
            </tr>
            <tr style="background-color: #F5f5f5">
                <th></th>
                <td><p class="btn-change-pass"><?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'icon'=>'arrow-right', 'label'=>'Lưu')); ?></p></td>
            </tr>
            <tr style="background-color: #F5f5f5">
                <th></th>
                <td>
                    <?php if(Yii::app()->user->hasFlash('mess')): ?>
                    <div>
                        <?php echo Yii::app()->user->getFlash('mess'); ?>
                    </div>
                    <?php endif; ?>
                </td>
            </tr>
        </table>

        <?php $this->endWidget(); ?>
    </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.btn-change-pass .btn').live('click',function(){
            var password_new = $('#password_new').val();
            var password_repeat = $('#password_repeat').val();
            var password_old = $('#password_old').val();
            if(password_new =='' || password_repeat =='' || password_old ==''){
                alert('Bạn chưa nhập đầy đủ thông tin');
                return false;
            }
            if(password_new != password_repeat){
                alert('Mật khẩu không khớp. Mời bạn nhập lại !');
                return false;
            }
        });
    });
</script>