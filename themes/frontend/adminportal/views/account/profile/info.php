
<div class="fix_width body">
    <div class="fl col_s1 acc">
        <?php $this->renderPartial('//blocks/menu_left'); ?>
    </div>
    <div class="fr  ct-r" style="width:765px;">
        <div class="well form-inline">
            <h1 class="title-register">Thông tin tài khoản</h1>
            <table id="yw0" class="detail-view table table-striped table-condensed">
                <tr class="even">
                    <th>Tài khoản :</th>
                    <td><strong><?php echo $model->username; ?></strong></td>
                </tr>
                <tr class="odd">
                    <th>Email :</th>
                    <td><?php echo $model->email; ?></td>
                </tr>
                <tr class="odd">
                    <th>Tên đầy đủ :</th>
                    <td><?php echo $model->fullname; ?></td>
                </tr>
                <tr class="even">
                    <th>Điện thoại :</th>
                    <td><?php echo $model->phone; ?></td>
                </tr>
                <tr class="odd">
                    <th>Ngày sinh :</th>
                    <td><?php echo date('d/m/Y',strtotime($model->birthday)); ?></td>
                </tr>
                <tr class="even">
                    <th>Giới tính :</th>
                    <td><?php
                        if($model->sex == 1){
                            echo 'Nam';
                        } elseif($model->sex == 2){
                            echo 'Nữ';
                        } else {
                            echo 'Chưa rõ';
                        }
                        ?></td>
                </tr>
<!--                <tr class="odd">-->
<!--                    <th>Công ty bạn đang công tác :</th>-->
<!--                    <td>--><?php //echo $model->company; ?><!--</td>-->
<!--                </tr>-->
<!--                <tr class="odd">-->
<!--                    <th>Nghề nghiệp :</th>-->
<!--                    <td>--><?php //echo $model->occupation; ?><!--</td>-->
<!--                </tr>-->
                <tr class="odd">
                    <th>Yahoo :</th>
                    <td><?php echo $model->yahoo; ?></td>
                </tr>
                <tr class="odd">
                    <th>Trình độ: </th>
                    <td>
                        <?php
                            switch($model->education){
                                case 1:
                                    echo 'Lao động phổ thông';
                                    break;
                                case 2:
                                    echo 'Chứng chỉ';
                                    break;
                                case 3:
                                    echo 'Trung học';
                                    break;
                                case 4:
                                    echo 'Trung cấp';
                                    break;
                                case 5:
                                    echo 'Cao đẳng';
                                    break;
                                case 6:
                                    echo 'Đại học';
                                    break;
                                case 7:
                                    echo 'Cao học';
                                    break;
                                default:
                                    echo '';
                                    break;
                            }
                        ?>
                    </td>
                </tr>
                <tr class="odd">
                    <th>Nhóm máu của bạn :</th>
                    <td>
                        <?php
                        switch($model->bloodtype){
                            case 1:
                                echo 'O';
                                break;
                            case 2:
                                echo 'A';
                                break;
                            case 3:
                                echo 'B';
                                break;
                            case 4:
                                echo 'AB';
                                break;
                            default:
                                echo '';
                                break;
                        }
                        ?>
                    </td>
                </tr>
<!--                <tr class="odd">-->
<!--                    <th>Cung :</th>-->
<!--                    <td>--><?php //echo $model->constellation; ?><!--</td>-->
<!--                </tr>-->
<!--                <tr class="odd">-->
<!--                    <th>Tuổi tử vi:</th>-->
<!--                    <td>--><?php //echo $model->zodiac; ?><!--</td>-->
<!--                </tr>-->
                <tr class="odd">
                    <th>Chiều cao :</th>
                    <td><?php echo !empty($model->height) ? $model->height.' cm' : 'Chưa có thông tin'; ?></td>
                </tr>
                <tr class="odd">
                    <th>Cân nặng :</th>
                    <td><?php echo !empty($model->weight) ? $model->weight.' kg' : 'Chưa có thông tin';; ?></td>
                </tr>
                <tr class="odd">
                    <th>Website cá nhân :</th>
                    <td><?php echo $model->site; ?></td>
                </tr>
                <tr class="odd">
                    <th>Sở thích :</th>
                    <td><?php echo $model->interest; ?></td>
                </tr>
                <tr class="odd">
                    <th>Địa chỉ :</th>
                    <td><?php echo $model->address; ?></td>
                </tr>
                <tr style="background-color: #F5f5f5">

                    <td colspan="2" align="right" style="padding-right: 0px"><p class="btn-edit" style="text-align: right"><?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'icon'=>'arrow-right', 'label'=>'Sửa thông tin')); ?></p></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.btn-edit button.btn').live('click',function(){
            window.location.href = '<?php echo Yii::app()->createUrl('account/profile/edit'); ?>';
        });
    });
</script>
