<?php
$moduleName = 'account';
$modelName = 'KitAccount';
$formID = 'kit_'.$moduleName.'_form';
$gridID = 'kit_'.$moduleName.'_grid';
?>

<div class="fix_width body">
    <div class="fl col_s1 acc">
        <?php $this->renderPartial('//blocks/menu_left'); ?>
    </div>
    <div class="fr  ct-r" style="width:765px;">
        <div class="well">
            <p class="lk-confirm">Thay đổi avatar</p>
            <div class="ct-mess">
                <div style="margin-left: 200px;">
                    <?php if(isset($model->avatar) AND !empty($model->avatar)): ?>
                        <img style="margin:5px; max-height:120px;" src="<?php echo KitAccount::getAvatar(Yii::app()->user->id,'large'); ?>" />
                    <?php endif; ?>

                    ?>
                    <?php
                    Yii::import("ext.xupload.models.XUploadForm");
                    $uploadModel = new XUploadForm;
                    $this->widget('ext.xupload.XUploadWidget', array(
                        'url' => Yii::app()->createUrl('cms/upload/upload', array('module' => $moduleName)),
                        'model' => $uploadModel,
                        'attribute' => 'file',
                        'multiple' => false,
                        'options' => array(
                            'onComplete' => 'js:function (event, files, index, xhr, handler, callBack) {
                                    if (handler.response.error !== 0) alert(handler.response.error);
                                    else {
                                        $("#image").val(handler.response.name);
                                        $("#ajax_result_image").html();
                                        $("#ajax_result_image").html(\'<a rel="collection" href="' . Common::getImageUploaded() . '\'+handler.response.name+\'"><img src="' . Common::getImageUploaded() . '\'+handler.response.name+\'?update='.time().'" width="100"/></a>\');
                                    }
                                    }',
                        )
                    ));
                    ?>
                    <span id="ajax_result_image">
                    </span>
                    <?php $form = $this->beginWidget('CActiveForm'); ?>
                    <?php echo $form->hiddenField($model, 'avatar', array('value' => '','id' => 'image')); ?>

                    <button type="submit" class="btn" data-toggle="button" style="margin-top: 10px;">Lưu</button>
                    <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>
</div>