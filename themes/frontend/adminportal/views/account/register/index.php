<div class="fix_width body">
    <div class="form-register l">
<!--        --><?php //$this->widget('account.widgets.frontend.film_register_account'); ?>

        <h1 class="title-register">Đăng ký</h1>
        <?php
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                'id'=>'horizontalForm',
                'type'=>'horizontal',
            )
        );echo "\n";
        ?>
        <div class="ct-input">
            <p class="row-tt">Thông tin tài khoản :</p>
            <?php echo $form->textFieldRow($model, 'username'); ?>
            <?php echo $form->passwordFieldRow($model, 'password'); ?>
            <?php echo $form->passwordFieldRow($model, 'password_repeat'); ?>
            <p class="row-tt">Thông tin cá nhân :</p>
            <?php echo $form->textFieldRow($model, 'let_fullname'); ?>
            <?php echo $form->textFieldRow($model, 'email'); ?>
            <div class="control-group ">
                <label for="KitAccount_address" class="control-label">Address</label>
                <div class="controls">
                    <input type="text" name="KitAccount[addess]" id="KitAccount_address" />
                </div>
            </div>
            <div class="control-group ">
                <label  class="control-label">BirthDay</label>
                <div class="controls">
                    <select class="sl-day sl-bir">
                        <option>Ngày</option>
                    </select>
                    <select class="sl-month sl-bir">
                        <option>Tháng</option>
                    </select>
                    <select class="sl-year sl-bir">
                        <option>Năm</option>
                    </select>
                </div>
            </div>
            <div class="control-group ">
                <label for="KitAccount_Phone" class="control-label">Phone</label>
                <div class="controls">
                    <input type="text" name="KitAccount[phone]" id="KitAccount_Phone" />
                </div>
            </div>
            <div class="control-group ">
                <label for="KitAccount_Phone" class="control-label"></label>
                <div class="controls">
                    <input type="text" class="sl-bir" name="KitAccount[captcha]" id="KitAccount_Captcha" />
                    <?php $this->widget('CCaptcha',array(
                    'buttonLabel' => '',
                    'clickableImage' => true,
                    'imageOptions' => array(
                        'style' => 'border:1px solud #ccc;width:65px;cursor:point;',
                        'title' => 'Click lấy mã khác',
                    ),
                    'captchaAction' => '//account/register/captcha',
                )); ?>
                </div>
            </div>
        </div>
        <div class="ct-btn">
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'icon'=>'ok white', 'label'=>'Đăng ký')); ?>
            <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset', 'icon'=>'remove', 'label'=>'Làm mới')); ?>
        </div>
        <?php $this->endWidget();echo "\n"; ?>

    </div>
    <div class="div-regu r">
        <h2 class="tt-regu">Điều khoản</h2>
        <div class="ct-regu">
            <p>1. Về tài khoản sử dụng (account): Khi đăng ký tài khoản, bạn nên cung cấp đầy đủ thông tin về tên tuổi, địa chỉ, điện thoại, số CMND… Đây không phải là những thông tin bắt buộc, nhưng khi có những rủi ro, mất mát sau này, chúng tôi chỉ tiếp nhận những trường hợp điền đúng và đầy đủ những thông tin trên. Những trường hợp điền thiếu thông tin hoặc thông tin sai sự thật sẽ không được giải quyết. Những thông tin này sẽ được dùng làm căn cứ để hỗ trợ giải quyết.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</div>