<?php $username = Yii::app()->user->name; ?>
<?php
$groups = json_decode(Yii::app()->user->getState('group'));
$items = array(
    array('label'=>$username),
    array('label'=>'Thông tin tài khoản', 'icon'=>'home', 'url'=>Yii::app()->createUrl('account/profile/info')),
    array('label'=>'Thông tin cấp độ', 'icon'=>'briefcase', 'url'=>Yii::app()->createUrl('account/level/index')),
    array('label'=>'Đổi mật khẩu', 'icon'=>'book', 'url'=> Yii::app()->createUrl('account/profile/changepass')),
    array('label'=>'Đổi avatar', 'icon'=>'book', 'url'=> Yii::app()->createUrl('account/profile/avatar')),
    array('label'=>'Thoát', 'icon'=>'off', 'url'=> Yii::app()->createUrl('account/ajax/logout')),

    array('label'=>'XU'),
//    array('label'=>'Nạp xu', 'icon'=>'pencil', 'url'=>Yii::app()->createUrl('currency/pay')),
    array('label'=>'Nạp xu & Gia hạn', 'icon'=>'pencil', 'url'=>Yii::app()->createUrl('currency/pay')),
    array('label'=>'Lịch sử giao dịch', 'icon'=>'list', 'url'=>Yii::app()->createUrl('currency/history')),

    array('label'=>'Dịch vụ'),
    array('label'=>'Xem Phim', 'icon'=>'home', 'url' => 'http://phim.let.vn'),
    array('label'=>'Chợ tình', 'icon'=>'home', 'url' => 'http://phim.let.vn/cho-tinh.let?utm_source=id.let.vn&utm_medium=leftmenu&utm_campaign=chotinh'),
    array('label'=>'Hẹn hò', 'icon'=>'home', 'url' => 'http://phim.let.vn/cho-tinh.let?utm_source=id.let.vn&utm_medium=leftmenu&utm_campaign=cuochen'),    
);

if ($groups !== NULL AND (in_array(8, $groups) OR in_array(65, $groups))) {
    $items = CMap::mergeArray($items, array(
        array('label'=>'Phim của tôi'),
        array('label'=>'Tất cả phim', 'icon'=>'home', 'url' => Yii::app()->createUrl('film/manage/list')),
        array('label'=>'Phim đã duyệt', 'icon'=>'home', 'url' => Yii::app()->createUrl('film/manage/index/status/1')),
        array('label'=>'Phim chưa duyệt', 'icon'=>'home', 'url' => Yii::app()->createUrl('film/manage/index/status/0')),
        array('label'=>'Tạo phim', 'icon'=>'home', 'url' => Yii::app()->createUrl('film/manage/create'))
    ));
}
?>
<?php $this->widget('bootstrap.widgets.TbMenu', array(
    'type'=>'list',
    'items'=>$items,
));
?>
