<section id="container">
    <?php $this->renderPartial('//_blocks/leftcol'); ?>
    <div id="content">
        <div class="wc">
            <div class="breadcrumbs">
                <a href="<?php echo Yii::app()->baseUrl; ?>">Trang chủ</a><span>&#187;</span>
                <a href="<?php echo Yii::app()->createUrl('//article/frontend/list'); ?>">Tin tức</a><span>&#187;</span>
                <a href="<?php echo $details['url']; ?>"><?php echo $details['name']; ?></a>
            </div>
            <div class="w-text">
                <?php $this->widget('article.widgets.frontend.listByCat', array('category' => $category, 'view' => 'listByCat_gamemongtien','limit' => $limit_wg)); ?>
                <div class="view-all" style="padding: 0px; margin: 5px 0px;">
                    <?php if(!empty($new)): ?>
                        <a title="Mơi hơn" style="background: none;float: left;padding: 0px" href="<?php echo Yii::app()->createUrl($new); ?>"><< Mới hơn</a>
                    <?php endif; ?>
                    <?php if(!empty($old)): ?>
                        <a title="Cũ hơn" style="background: none;float: right;" href="<?php echo Yii::app()->createUrl($old); ?>">Cũ hơn >></a>
                    <?php endif ?>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</section>