<?php $host = letArray::get($_SERVER, 'HTTP_HOST', ''); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title><?php echo isset($this->pageTitle) ? CHtml::encode($this->pageTitle) : Yii::app()->name; ?></title>
        <link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
        <meta name="ROBOTS" content="index, follow" />

        <!-- Bootstrap -->
        <?php
        $version = 5;
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/bootstrap.css?version=' . $version);
        $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/style.css?version=' . $version);
        $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/liteaccordion.css?version=' . $version);
        $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/jqx.base.css?version=' . $version);
        $cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/jquery-ui.css?version=' . $version);
        $cs->registerCoreScript('jquery');
        $cs->registerCoreScript('cookie');
//    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/js/jquery-2.0.3.min.js', CClientScript::POS_HEAD);
        $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/bootstrap.js', CClientScript::POS_HEAD);
        $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.easing.1.3.js', CClientScript::POS_HEAD);
        $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/liteaccordion.jquery.js', CClientScript::POS_HEAD);
        $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery-ui.js', CClientScript::POS_HEAD);
        $cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/login.js', CClientScript::POS_HEAD);
        $cs->registerMetaTag('-zWqXqeIf4eqyqG67qc7YbZZJs6FibkbZ_XKxmk7hx4', 'google-site-verification');
        ?>
        <script>
            $(function() {
                $("#datepicker").datepicker();
            });
        </script>
    </head>
    <body>

        <div id="wap">
            <header id="header">
                <div class="top-header">
                    <div class="container1">
                        <div class="top-header-l">
                            <div class="navbar">
                                <div class="navbar-inner">
                                    <ul class="nav">
                                        <li><a class="logo-top" style="" href="<?php echo Yii::app()->getBaseUrl(true); ?>"><span>Logo</span></a></li>
<!--                                        <li class="btn-group">
                                            <a class="btn dropdown-toggle dsg border" data-toggle="dropdown" href="#">
                                                Danh sách game
                                                <span class="caret caret2"></span>
                                            </a>>
                                            <ul class="dropdown-menu">
                                                <li><a href="">Game 1</a></li>
                                                <li><a href="">Game 2</a></li>
                                                <li><a href="">Game 3</a></li>
                                            </ul>
                                        </li>-->
                                        <li><a href="<?php echo Yii::app()->createUrl('//article/frontend/list'); ?>">Tin tức</a></li>
                                        <li><a href="">Diễn đàn</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="top-header-r">
                            <div class="navbar">
                                <div class="navbar-inner">
                                    <ul class="nav">
                                        <li><a class="home" style="padding: 0px" href=""><span>Home</span></a></li>
                                        <li class="cy"><a class="border" href="">Nạp thẻ</a></li>
                                        <li  class="cy"><a href="">Gif code</a></li>
                                        <li><a href="">Đăng nhập</a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="menu">
                    <div class="container1">
                        <div class="navbar">
                            <div class="navbar-inner">
                                <ul class="nav">
                                    <li class="active border"><a href="#">Trang chủ</a></li>
                                    <li class="border"><a href="<?php echo Yii::app()->createUrl('//article/frontend/list'); ?>">Tin tức</a></li>
                                    <li class="border"><a href="#">Hướng dẫn</a></li>
                                    <li class="border"><a href="#">Tính năng</a></li>
                                    <li class="border"><a href="#">Thư viện</a></li>
                                    <li><a href="http://forum.mongtien.net"> Diễn đàn</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="<?php echo Yii::app()->getBaseUrl(true); ?>"><div class="mong-tien"></div></a>
            </header>
            <?php echo $content; ?>
            <footer id="footer"></footer>
        </div>

        <script>
            (function($, d) {

                $('#one').liteAccordion({
                    onTriggerSlide: function() {
                        this.find('figcaption').fadeOut();
                    },
                    onSlideAnimComplete: function() {
                        this.find('figcaption').fadeIn();
                    },
                    autoPlay: true,
                    pauseOnHover: true,
                    theme: 'stitch',
                    containerWidth: 715,
                    containerHeight: 190,
                    headerWidth: 76,
                    rounded: true,
                    enumerateSlides: true
                }).find('figcaption:first').show();
                $('#two').liteAccordion();
                $('#three').liteAccordion({theme: 'dark', rounded: true, enumerateSlides: true, firstSlide: 2, linkable: true, easing: 'easeInOutQuart'});
                $('#four').liteAccordion({theme: 'light', firstSlide: 3, easing: 'easeOutBounce', activateOn: 'mouseover'});

            })(jQuery, document);


        </script>
    </body>
</html>