<section id="container">
    <?php $this->renderPartial('//_blocks/leftcol'); ?>
    <div id="content">
        <div class="wc">
            <div class="breadcrumbs">
                <a href="<?php echo Yii::app()->baseUrl; ?>">Trang chủ</a><span>&#187;</span>
                <a href="<?php echo Yii::app()->createUrl('//article/list'); ?>">Tin tức</a><span>&#187;</span>
                <a href="<?php echo $data['url']; ?>"><?php echo $data['title']; ?></a>
            </div>
            <div class="w-text">
                <?php echo $data['content']; ?>
            </div>
        </div>
        
        <div class="wc">
            <div class="breadcrumbs">
                Các tin khác cùng mục
            </div>
            <div class="w-text">
                <?php $this->widget('article.widgets.frontend.lastest', array(
                    'category' => (!empty($arrCategory)) ? $arrCategory[0] : NULL,
                    'view' => 'lastest_gamemongtien_bycat',
                    'limit' => 10,
                    'olderId' => $data['id']
                )); ?>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</section>