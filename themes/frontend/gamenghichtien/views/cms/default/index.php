            <section id="container">
                <?php $this->renderPartial('//_blocks/leftcol'); ?>
                <div id="content">
                    <div class="news">
                        <div class="title-lr"></div>
                        <div class="slide-show">
                            <a href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img5.png"></a>
                        </div>
                        <div class="box-new">
                            <?php $this->widget('article.widgets.frontend.lastestByCat', array(
                                'title' => 'Tin mới',
                                'category' => 125,
                                'view' => 'lastestByCat_gamemongtien',
                                'limit' => 5,
                            )); ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="box-img">
                        <ul>
                            <li><a href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img1.png"></a></li>
                            <li><a href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img2.png"></a></li>
                            <li><a href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img3.png"></a></li>
                            <li><a href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img4.png"></a></li>

                        </ul>
                    </div>
                    <?php $this->widget('article.widgets.frontend.lastestByCat', array(
                        'title' => 'Cẩm nang',
                        'category' => 126,
                        'view' => 'lastestByCat_gamemongtien_camnang',
                        'limit' => 5,
                    )); ?>

                    <div class="content2">
                        <div class="tinh-nang">
                            <?php $this->widget('article.widgets.frontend.lastestByCat', array(
                                'title' => 'Tất cả tính năng',
                                'category' => 127,
                                'view' => 'lastestByCat_gamemongtien',
                                'limit' => 8,
                            )); ?>
                        </div>
                        <div class="phongthan">
                            <p class="p-mc-pt">Máy chủ</p>
                            <p class="p-s-pt">
                                <select>
                                    <option>S1 - Luffy</option>
                                    <option>S1 - Luffy</option>
                                    <option>S1 - Luffy</option>
                                </select>
                            </p>
                            <p class="p-t-pt">
                                <span>Level</span>
                                <span>|</span>
                                <span>Danh vọng</span>
                                <span>|</span>
                                <span>Đấu trường</span></p>
                            <table>
                                <thead>
                                    <tr>
                                        <th>Vị trí</th>
                                        <th>Tên nhân vật</th>
                                        <th>Cấp độ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Văn Mai A</td>
                                        <td>129</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Văn Mai A</td>
                                        <td>129</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Văn Mai A</td>
                                        <td>129</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Văn Mai A</td>
                                        <td>129</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="img-vi">
                        <div class="img-vi1">
                            <ul class="nav nav-tabs tab-img" id="myTab">
                                <li class="active"><a data-toggle="tab" href="#hinhnen">Hình nền</a></li>
                                <li><a data-toggle="tab" href="#anhgame">Ảnh game</a></li>
                                <li><a data-toggle="tab" href="#video">Video</a></li>
                                <li><a data-toggle="tab" href="#Cosplay">Cosplay</a></li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div id="hinhnen" class="tab-pane fade in active">
                                    <div class="p-img-list">
                                        <a class="a-img-list" href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img2.png"></a>
                                        <a class="a-img-list" href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img2.png"></a>
                                        <a class="a-img-list" href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img2.png"></a>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div id="anhgame" class="tab-pane fade">
                                    <div class="p-img-list">
                                        <a class="a-img-list" href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img2.png"></a>
                                        <a class="a-img-list" href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img2.png"></a>
                                        <a class="a-img-list" href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img2.png"></a>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div id="video" class="tab-pane fade">
                                    <div class="p-img-list">
                                        <a class="a-img-list" href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img2.png"></a>
                                        <a class="a-img-list" href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img2.png"></a>
                                        <a class="a-img-list" href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img2.png"></a>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div id="Cosplay" class="tab-pane fade">
                                    <div class="p-img-list">
                                        <a class="a-img-list" href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img2.png"></a>
                                        <a class="a-img-list" href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img2.png"></a>
                                        <a class="a-img-list" href=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img2.png"></a>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="img-vi2">
                            <a href="">
                                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/img4.png">
                            </a>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="clear"></div>
            </section>
