<!doctype html public "✰">
<!--[if lt IE 7]> <html lang="en-us" class="no-js ie6"> <![endif]-->
<!--[if IE 7]>    <html lang="en-us" class="no-js ie7"> <![endif]-->
<!--[if IE 8]>    <html lang="en-us" class="no-js ie8"> <![endif]-->
<!--[if IE 9]>    <html lang="en-us" class="no-js ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-us" class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">

<title>Adminica | The Professional Admin Theme</title>

<!-- iPhone, iPad and Android specific settings -->	

<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1;">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
<link rel="apple-touch-icon" href="images/iOS_icon.png">
<link rel="apple-touch-startup-image" href="images/iOS_startup.png">

<!-- Styles -->

<link rel="stylesheet" type="text/css" href="styles/reset.css">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,700">

<link rel="stylesheet" type="text/css" href="scripts/fancybox/jquery.fancybox-1.3.4.css">
<link rel="stylesheet" type="text/css" href="scripts/tinyeditor/style.css">
<link rel="stylesheet" type="text/css" href="scripts/slidernav/slidernav.css">
<link rel="stylesheet" type="text/css" href="scripts/syntax_highlighter/styles/shCore.css">
<link rel="stylesheet" type="text/css" href="scripts/syntax_highlighter/styles/shThemeDefault.css">
<link rel="stylesheet" type="text/css" href="scripts/uitotop/css/ui.totop.css">
<link rel="stylesheet" type="text/css" href="scripts/fullcalendar/fullcalendar.css">
<link rel="stylesheet" type="text/css" href="scripts/isotope/isotope.css">
<link rel="stylesheet" type="text/css" href="scripts/elfinder/css/elfinder.css">

<link rel="stylesheet" type="text/css" href="scripts/tiptip/tipTip.css">
<link rel="stylesheet" type="text/css" href="scripts/uniform/css/uniform.aristo.css">
<link rel="stylesheet" type="text/css" href="scripts/multiselect/css/ui.multiselect.css">
<link rel="stylesheet" type="text/css" href="scripts/selectbox/jquery.selectBox.css">
<link rel="stylesheet" type="text/css" href="scripts/colorpicker/css/colorpicker.css">
<link rel="stylesheet" type="text/css" href="scripts/uistars/jquery.ui.stars.min.css">

<link rel="stylesheet" type="text/css" href="scripts/themeroller/Aristo.css">

<link rel="stylesheet" type="text/css" href="styles/text.css">
<link rel="stylesheet" type="text/css" href="styles/grid.css">
<link rel="stylesheet" type="text/css" href="styles/main.css">
<link rel="stylesheet" type="text/css" href="styles/theme/theme_base.css">

<!-- Style Switcher  

The following stylesheet links are used by the styleswitcher to allow for dynamically changing how Adminica looks and acts.
Styleswitcher documentation: http://style-switcher.webfactoryltd.com/documentation/

switcher1.php : layout - fluid by default.								(eg. styles/theme/switcher1.php?default=layout_fixed.css)
switcher2.php : header and sidebar positioning - sidebar by default.	(eg. styles/theme/switcher1.php?default=header_top.css)
switcher3.php : colour skin - black/grey by default.					(eg. styles/theme/switcher1.php?default=theme_red.css)
switcher4.php : background image - dark boxes by default.				(eg. styles/theme/switcher1.php?default=bg_honeycomb.css)
switcher5.php : controls the theme - dark by default.					(eg. styles/theme/switcher1.php?default=theme_light.css)

-->

<link rel="stylesheet" type="text/css" href="styles/theme/switcher.css" media="screen">
<link rel="stylesheet" type="text/css" href="styles/theme/layout_fixed.css" media="screen" > 
<!--		<link rel="stylesheet" type="text/css" href="styles/theme/switcher2.php?default=switcher.css" media="screen" >-->
<link rel="stylesheet" type="text/css" href="styles/theme/theme_blue.css" media="screen" >
<link rel="stylesheet" type="text/css" href="styles/theme/bg_wood.css" media="screen" >
<!--		<link rel="stylesheet" type="text/css" href="styles/theme/switcher5.php?default=switcher.css" media="screen" >-->

<link rel="stylesheet" type="text/css" href="styles/colours.css">
<link rel="stylesheet" type="text/css" href="styles/ie.css">

<!-- Scripts -->

<!-- Load JQuery -->		
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<!-- Load JQuery UI -->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/jquery-ui.min.js" type="text/javascript"></script>

<!-- Global -->
<script src="scripts/touchPunch/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
<script src="scripts/uitotop/js/jquery.ui.totop.js" type="text/javascript"></script>

<!-- Forms -->
<script src="scripts/uniform/jquery.uniform.min.js"></script>
<script src="scripts/autogrow/jquery.autogrowtextarea.js"></script>
<script src="scripts/multiselect/js/ui.multiselect.js"></script>
<script src="scripts/selectbox/jquery.selectBox.min.js"></script>
<script src="scripts/timepicker/jquery.timepicker.js"></script>
<script src="scripts/colorpicker/js/colorpicker.js"></script>
<script src="scripts/uistars/jquery.ui.stars.min.js"></script>
<script src="scripts/tiptip/jquery.tipTip.minified.js"></script>
<script src="scripts/validation/jquery.validate.min.js" type="text/javascript"></script>		

<!-- Configuration Script -->
<script type="text/javascript" src="scripts/adminica/adminica_ui.js"></script>
<script type="text/javascript" src="scripts/adminica/adminica_forms.js"></script>
<script type="text/javascript" src="scripts/adminica/adminica_mobile.js"></script>

<!-- Live Load (remove after development) -->
<script>//document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>

</head>
<body>
<div id="wrapper">
    <div id="topbar" class="clearfix">

        <a href="dashboard_sorter.php" class="logo"><span>Adminica</span></a>

        <div class="user_box dark_box clearfix">
            <img src="images/profile.jpg" width="55" alt="Profile Pic" />
            <h2>Administrator</h2>
            <h3><a class="text_shadow" href="#">John Smith</a></h3>
            <ul>
                <li><a href="#">profile</a><span class="divider">|</span></li>
                <li><a href="#">settings</a><span class="divider">|</span></li>
                <li><a href="login.php">logout</a></li>
            </ul>
        </div><!-- #user_box -->	
    </div><!-- #topbar -->		
    <div id="sidebar">
        <div class="cog">+</div>

        <a href="index.php" class="logo"><span>Adminica</span></a>

        <div class="user_box dark_box clearfix">
            <img src="images/profile.jpg" width="55" alt="Profile Pic" />
            <h2>Administrator</h2>
            <h3><a href="#">John Smith</a></h3>
            <ul>
                <li><a href="#">settings</a><span class="divider">|</span></li>
                <li><a href="login.php">logout</a></li>
            </ul>
        </div><!-- #user_box -->

        <ul class="side_accordion"> <!-- add class 'open_multiple' to change to from accordion to toggles -->
            <li><a href="#"><img src="images/icons/small/grey/home.png"/>Home</a>
                <ul class="drawer">
                    <li><a href="#">Activity</a></li>
                    <li><a href="#">Events</a></li>
                    <li><a href="#">Tasks</a></li>
                </ul>
            </li>
            <li><a href="#"><img src="images/icons/small/grey/mail.png"/>Mailbox<span class="alert badge alert_red">5</span></a>
                <ul class="drawer">
                    <li><a href="#">Inbox</a></li>
                    <li><a href="#">Sent Items</a></li>
                    <li><a href="#">Drafts<span class="alert badge alert_grey">2</span></a></li>
                    <li><a href="#">Trash<span class="alert badge alert_grey">3</span></a></li>
                </ul>
            </li>
            <li><a href="#"><img src="images/icons/small/grey/documents.png"/>Documents<span class="alert badge alert_black">2</span></a>
                <ul class="drawer">
                    <li><a href="#">Create New</a></li>
                    <li><a href="#">View All</a></li>
                    <li><a href="#">Upload/Download<span class="alert badge alert_grey">2</span></a></li>
                </ul>
            </li>
            <li><a href="#"><img src="images/icons/small/grey/users.png"/>Members</a>
                <ul class="drawer">
                    <li><a href="#">Add New</a></li>
                    <li><a href="#">Edit/Delete</a></li>
                    <li><a href="#">Search Profiles</a></li>
                </ul>
            </li>
            <li><a href="http://www.tricycle.ie"><img src="images/icons/small/grey/graph.png"/>Statistics</a></li>
            <li><a href="#"><img src="images/icons/small/grey/cog_2.png"/>Settings</a>
                <ul class="drawer">
                    <li><a href="#">Account</a></li>
                    <li><a href="#">System</a></li>
                </ul>
            </li>
        </ul>
        <form>
            <div id="search_side" class="dark_box"><input class="" type="text" value="Search Adminica..." onclick="value=''"></div>
        </form>
        <ul id="side_links" class="side_links" style="margin-bottom:0;">
            <li><a href="http://goo.gl/UjRRe">Online Documentation</a>
            <li><a href="support.tricyclelabs.com">Expert Forum</a></li>
            <li><a href="#">Product Wiki</a></li>
            <li><a href="#">Latest Company News</a></li>
        </ul>
    </div><!-- #sidebar -->
    <div id="main_container" class="main_container container_16 clearfix">
        <div id="nav_top" class="clearfix round_top">
            <ul class="clearfix">
                <li><a href="index.php"><img src="images/icons/small/grey/laptop.png"/><span class="display_none">Home</span></a></li>

                <li><a href="#"><img src="images/icons/small/grey/frames.png"/><span>Layout</span></a>
                    <ul>
                        <li><a href="layout.php"><span>16 Grid - 960.gs</span></a></li>
                        <li><a href="text.php"><span>Text & Typography</span></a></li>
                        <li><a class="hide_mobile" href="#"><span>Layout Width</span></a>
                            <ul class="drawer">						
                                <li><a href="styles/theme/switcher1.php?style=layout_fixed.css"><span>Fixed</span></a></li>
                                <li><a href="styles/theme/switcher1.php?style=switcher.css"><span>Fluid</span></a></li>
                            </ul>
                        </li>
                        <li><a class="hide_mobile" href="#"><span>Layout Position</span></a>
                            <ul class="drawer">
                                <li><a href="styles/theme/switcher2.php?style=switcher.css"><span>Side</span></a></li>
                                <li><a href="styles/theme/switcher2.php?style=header_top.css"><span>Top</span></a></li>
                                <li><a href="styles/theme/switcher2.php?style=header_slideout.css"><span>Slide</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="#"><img src="images/icons/small/grey/coverflow.png"/><span>Boxes</span></a>
                    <ul>
                        <li><a href="tabs.php"><span>Tab Boxes</span></a></li>
                        <li><a href="accordions.php"><span>Accordions Boxes</span></a></li>
                        <li><a href="wizard.php"><span>Step by Step Wizard</span></a></li>
                        <li><a href="code.php"><span>Code View</span></a></li>
                    </ul>
                </li>	
                <li><a href="#"><img src="images/icons/small/grey/create_write.png"/><span>Forms</span><span class="alert badge alert_red">new</span></a>
                    <ul>
                        <li><a href="forms.php"><span>Input Fields</span></a></li>
                        <li><a href="buttons.php"><span>Buttons</span></a></li>				
                        <li><a href="#"><span>More components</span></a>
                            <ul class="drawer">
                                <li><a href="validation.php"><span>Validation</span></a></li>
                                <li><a href="editor.php"><span>WYSIWYG Editor</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </li>		
                <li><a href="gallery.php"><img src="images/icons/small/grey/images.png"/><span>Gallery</span></a></li>			
                <li><a href="#"><img src="images/icons/small/grey/blocks_images.png"/><span>Tables</span></a>
                    <ul>
                        <li><a href="tables.php"><span>DataTables</span></a></li>
                        <li><a href="tables_static.php"><span>Regular Tables</span><span class="alert badge alert_black">new</span></a></li>
                    </ul>			
                </li>
                <li><a href="#"><img src="images/icons/small/grey/file_cabinet.png"/><span>Org</span></a>
                    <ul>
                        <li><a href="files.php"><img src="images/icons/small/grey/folder.png"/><span>Files</span></a></li>
                        <li><a href="contacts.php"><img src="images/icons/small/grey/users.png"/><span>Contacts</span></a></li>
                    </ul>
                </li>
                <li><a href="calendar.php"><img src="images/icons/small/grey/month_calendar.png"/><span>Cal</span><span class="alert badge alert_blue">new</span></a></li>
                <li><a href="charts.php"><img src="images/icons/small/grey/graph.png"/><span>Charts</span><span class="alert badge alert_green">new</span></a></li>
                <li><a href="#"><img src="images/icons/small/grey/locked_2.png"/><span class="display_none">Login</span></a>
                    <ul class="dropdown_right">
                        <li><a href="login_regular.php" class="dialog_button" data-dialog="logout"><span>Regular Login</span></a></li>
                        <li><a href="login.php" class="dialog_button" data-dialog="logout"><span>Slide Login</span></a></li>
                    </ul>
                </li>
            </ul>
            <div class="display_none">						
                <div id="logout" class="dialog_content narrow" title="Logout">
                    <div class="block">
                        <div class="section">
                            <h1>Thank you</h1>
                            <div class="dashed_line"></div>	
                            <p>We will now log you out of Adminica in a 10 seconds...</p><p></p>
                        </div>
                        <div class="button_bar clearfix">
                            <button class="dark blue no_margin_bottom link_button" data-link="login.php">
                                <div class="ui-icon ui-icon-check"></div>
                                <span>Ok</span>
                            </button>
                            <button class="light send_right close_dialog">
                                <div class="ui-icon ui-icon-closethick"></div>
                                <span>Cancel</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div> 		


            <script type="text/javascript">
                var currentPage = 4 - 1; // This is only used in php version to tell the nav what the current page is
                $('#nav_top > ul > li').eq(currentPage).addClass("current");
                $('#nav_top > ul > li').addClass("icon_only").children("a").children("span:visible").parent().parent().removeClass("icon_only");
            </script>



            <div id="mobile_nav">
                <div class="main"></div>
                <div class="side"></div>
            </div>

        </div><!-- #nav_top -->
        <div class="flat_area grid_16">
            <h2>Form elements and controls</h2>
            <p>Check out the Application like <a href="#">navigation</a>. Resize to see the liquid layout in action. Expand/Collapse and sort boxes. Try out the WYSIWYGs.</p>
        </div>
        <div class="box grid_16">
            <h2 class="box_head">Form Elements</h2>
            <a href="#" class="grabber">&nbsp;</a>
            <a href="#" class="toggle">&nbsp;</a>
            <div class="toggle_container">
                <div class="block">
                    <h2 class="section">Text Fields</h2>

                    <fieldset class="label_side">
                        <label>Text Field<span>Label placed beside the Input</span></label>
                        <div>
                            <input type="text">
                        </div>
                    </fieldset>

                    <fieldset class="label_side">
                        <label>Textarea<span>Regular Textarea</span></label>
                        <div class="clearfix">
                            <textarea></textarea>
                        </div>
                    </fieldset>

                    <fieldset>
                        <label>Text Field<span>Label placed above the Input. Also has a required icon at the top right.</span></label>
                        <div>
                            <input title="Here's a tool tip for this input field." class="tooltip right" type="text">
                            <div class="required_tag tooltip hover left" title="This field is required"></div>
                        </div>
                    </fieldset>

                    <fieldset>
                        <label>Autogrow Textarea<span>This textarea grows as you type</span></label>
                        <div class="clearfix">
                            <textarea title="This field keeps expanding, just like on Facebook" class="tooltip autogrow" placeholder="You keep typing, I keep growing…"></textarea>
                            <div class="required_tag tooltip hover left" title="This field is required"></div>
                        </div>
                    </fieldset>
                    <div class="columns clearfix">
                        <div class="col_60">
                            <fieldset>
                                <label>60% Width Column</label>
                                <div>
                                    <input type="text" placeholder="Input is 66% column width">
                                </div>
                            </fieldset>
                        </div>
                        <div class="col_40">
                            <fieldset>
                                <label>40% Width Column</label>
                                <div>
                                    <select class="select_box full_width">
                                        <option>Option 1</option>
                                        <option>Option 2</option>
                                        <option>Option 3</option>
                                    </select>				
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    <fieldset class="label_side">
                        <label>Autocomplete<span>Programming</span></label>
                        <div>
                            <input type="text" class="autocomplete">
                        </div>
                    </fieldset>

                    <div class="button_bar clearfix">
                        <button class="green dark">
                            <img src="images/icons/small/white/bended_arrow_right.png">
                            <span>Yes</span>
                        </button>
                        <button class="red dark">
                            <img src="images/icons/small/white/bended_arrow_right.png">
                            <span>No</span>
                        </button>
                        <button class="grey dark send_right">
                            <img src="images/icons/small/grey/bended_arrow_right.png">
                            <span>Maybe</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="box grid_16">
            <div class="toggle_container">
                <div class="block">
                    <h2 class="section">Buttons</h2>
                    <fieldset class="label_side">
                        <label>Buttons</label>
                        <div>
                            <button class="skin_colour">
                                <img src="images/icons/small/white/file_cabinet.png">
                                <span>Button</span>
                            </button>

                            <button class="black icon_only">
                                <img src="images/icons/small/white/file_cabinet.png">
                            </button>

                            <button class="blue icon_only">
                                <img src="images/icons/small/white/film_strip.png">
                            </button>

                            <button class="navy icon_only">
                                <img src="images/icons/small/white/firefox.png">
                            </button>

                            <button class="red icon_only">
                                <img src="images/icons/small/white/flag.png">
                            </button>

                            <button class="green icon_only">
                                <img src="images/icons/small/white/folder.png">
                            </button>

                            <button class="orange icon_only">
                                <img src="images/icons/small/white/frames.png">
                            </button>

                            <div class="clearfix"></div>
                            <p><a href="buttons.php">Click here</a> to see the full 500+ icon set including all the jQuery UI action icons.</p>

                        </div>
                    </fieldset>					



                    <div class="columns clearfix">
                        <div class="col_33">
                            <fieldset>
                                <label>Radio Buttons</label>
                                <div>
                                    <div class="jqui_radios">
                                        <input type="radio" name="answer5" id="yes5"/><label for="yes5">Yes</label>
                                        <input type="radio" name="answer5" id="no5" checked="checked"/><label for="no5">No</label>																					</div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col_33">
                            <fieldset>
                                <label>Checkbox Buttons</label>
                                <div>
                                    <div class="jqui_checkbox">
                                        <input type="checkbox" name="answer6" id="yes6"/><label for="yes6">One</label>
                                        <input type="checkbox" name="answer6" id="no6"/><label for="no6">Two</label>
                                        <input type="checkbox" disabled="disabled" name="answer6" id="cant6"/><label for="cant6">Three</label>	
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col_33">
                            <fieldset>
                                <label for="file_upload">Uniform File Upload</label>

                                <div class="clearfix">
                                    <input type="file" id="fileupload" class="uniform">
                                </div>
                            </fieldset>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <div class="box grid_16">
            <div class="toggle_container">
                <div class="block">
                    <h2 class="section">Dialogs <small>All form fields can also be easily displayed in a dialog box.</small></h2>
                    <fieldset class="label_side">
                        <label>Dialog</label>
                        <div class="clearfix">
                            <button class="dialog_button" data-dialog="dialog_form">
                                <img src="images/icons/small/white/speech_bubble_2.png">
                                <span>Open</span>
                            </button>			
                        </div>
                    </fieldset>	
                </div>
            </div>
        </div>
        <div class="box grid_16 round_all">
            <div class="toggle_container">
                <div class="block">
                    <h2 class="section">Select Boxes</h2>
                    <div class="columns clearfix">
                        <div class="col_50">
                            <fieldset class="label_side">
                                <label><span class="alert badge alert_red">5</span>Browser Select Box</label>
                                <div>
                                    <select class="">
                                        <option>Albania</option>
                                        <option>Algeria</option>
                                        <option>Andorra</option>
                                        <option>Argentina</option>
                                        <option>Armenia</option>
                                        <option>Aruba</option>
                                        <option>Australia</option>
                                        <option>Austria</option>
                                    </select>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col_50">
                            <fieldset class="label_side">

                                <label>Uniform Select Box</label>
                                <div>
                                    <select class="select_box">
                                        <option>Albania</option>
                                        <option>Algeria</option>
                                        <option>Andorra</option>
                                        <option>Argentina</option>
                                        <option>Armenia</option>
                                        <option>Aruba</option>
                                        <option>Australia</option>
                                        <option>Austria</option>
                                    </select>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    <div class="columns clearfix">
                        <div class="col_50">
                            <fieldset>

                                <label>Multi Select Box</label>
                                <div>
                                    <select class="multiselect_box" multiple="multiple">
                                        <option>Albania</option>
                                        <option disabled="disabled">Algeria (disabled)</option>
                                        <option>Andorra</option>
                                        <option selected="selected">Argentina</option>
                                        <option>Armenia</option>
                                        <option>Aruba</option>
                                        <option>Australia</option>
                                        <option>Austria</option>
                                    </select>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col_50">
                            <fieldset>

                                <label>Multi Select Box with Option Groups</label>
                                <div>
                                    <select class="multiselect_box" multiple="multiple">
                                        <optgroup label="Section 1"> 
                                            <option>Item 1</option> 
                                            <option>Item 2</option> 
                                            <option selected="selected">Item 3</option> 
                                            <option>Item 4</option> 
                                        </optgroup> 
                                        <optgroup label="Section 2"> 
                                            <option>Item 5</option> 
                                            <option>Item 6</option> 
                                            <option>Item 7</option> 
                                            <option>Item 8</option> 
                                        </optgroup> 
                                        <optgroup label="Section 3"> 
                                            <option>Item 9</option> 
                                            <option>Item 10</option> 
                                            <option>Item 11</option> 
                                            <option>Item 12</option> 
                                        </optgroup> 
                                        <optgroup label="Section 4"> 
                                            <option>Item 13</option> 
                                            <option>Item 14</option> 
                                            <option>Item 15</option> 
                                            <option>Item 16</option> 
                                        </optgroup> 
                                    </select>
                                </div>

                            </fieldset>									
                        </div>
                    </div>

                    <fieldset>
                        <label>Filter & Search Draggable Multiselect</label>
                        <div>
                            <select class="multisorter indent" multiple="multiple" style="height:230px;">
                                <option>Albania</option>
                                <option>Algeria</option>
                                <option>Andorra</option>
                                <option selected="selected">Argentina</option>
                                <option>Armenia</option>
                                <option>Aruba</option>
                                <option selected="selected">Australia</option>
                                <option selected="selected">Austria</option>
                            </select>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="box grid_8">
            <div class="toggle_container">
                <div class="block">
                    <h2 class="section">Radios</h2>

                    <fieldset class="label_side">
                        <label>Regular Radios</label>
                        <div>
                            <label for="yes1"><input type="radio" name="answer1" id="yes1"/>Yes</label>
                            <label for="no1"><input type="radio" name="answer1" id="no1"/>No</label>
                            <label for="cant1"><input type="radio" disabled="disabled" name="answer1" id="cant1"/>Cant</label>
                        </div>
                        <label>Regular Checkboxes</label>
                        <div>
                            <label for="yes2"><input type="checkbox" name="answer2" id="yes2"/>Option 1</label>
                            <label for="no2"><input type="checkbox" name="answer2" id="no2"/>Option 2</label>
                            <label for="cant2"><input type="checkbox" disabled="disabled" name="answer2" id="cant2"/>Option 4</label>
                        </div>
                    </fieldset>

                    <fieldset class="label_side">
                        <label>Radios Inline</label>
                        <div class="inline clearfix">
                            <label for="yes1b"><input type="radio" name="answer1b" id="yes"/>Yes</label>
                            <label for="no1b"><input type="radio" name="answer1b" id="no"/>No</label>
                        </div>
                        <label>Checks Inline</label>
                        <div class="inline clearfix">
                            <label for="yes2b"><input type="checkbox" name="answer2b" id="yes2b"/>One</label>
                            <label for="no2b"><input type="checkbox" name="answer2b" id="no2b"/>Two</label>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="box grid_8">
            <div class="toggle_container">
                <div class="block">
                    <h2 class="section">Uniform Radios</h2>
                    <fieldset class="label_side">
                        <label>Uniform Radios</label>
                        <div class="uniform">
                            <label for="yes3"><input type="radio" name="answer3" id="yes3"/>Yes</label>
                            <label for="no3"><input type="radio" name="answer3" id="no3"/>No</label>
                            <label for="cant3"><input type="radio" disabled="disabled" name="answer3" id="cant3"/>Cant</label>
                        </div>
                        <label>Uniform Checkboxes</label>
                        <div class="uniform">
                            <label for="yes4"><input type="checkbox" name="answer4" id="yes4"/>Option 1</label>
                            <label for="no4"><input type="checkbox" name="answer4" id="no4"/>Option 2</label>
                            <label for="cant4"><input type="checkbox" disabled="disabled" name="answer4" id="cant4"/>Option 3</label>
                        </div>
                    </fieldset>

                    <fieldset class="label_side">
                        <label>Radios Inline</label>
                        <div class="uniform inline clearfix">
                            <label for="yes3b"><input type="radio" name="answer3b" id="yes3b"/>Yes</label>
                            <label for="no3b"><input type="radio" name="answer3b" id="no3b"/>No</label>
                        </div>
                        <label>Checks Inline</label>
                        <div class="uniform inline clearfix">
                            <label for="yes4b"><input type="checkbox" name="answer4b" id="yes4b"/>One</label>
                            <label for="no4b"><input type="checkbox" name="answer4b" id="no4b"/>Two</label>
                        </div>
                    </fieldset>	
                </div>
            </div>
        </div>

        <div id="date_picker_anchor" class="box grid_16">
            <div class="toggle_container">
                <div class="block">
                    <h2 class="section">Time and Date</h2>
                    <div class="columns clearfix">
                        <div class="col_50">
                            <fieldset class="label_side label_small">
                                <label>Date Picker</label>
                                <div class="clearfix">
                                    <div class="datepicker"></div>
                                </div>
                            </fieldset>			
                        </div>
                        <div class="col_50 on_right">
                            <fieldset class="label_side">
                                <label>Date Picker</label>
                                <div class="clearfix">
                                    <input type="text" class="datepicker" style="width:100px;">
                                </div>
                            </fieldset>	
                            <fieldset class="label_side">
                                <label>Time Picker</label>
                                <div class="clearfix time_picker_holder">
                                    <div class="time_picker"></div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box grid_16">
            <div class="toggle_container">
                <div class="block">
                    <h2 class="section">Miscellaneous</h2>

                    <fieldset class="label_side">
                        <label>Slider</label>
                        <div>
                            <div style="width:auto; margin-right: 45px;" class="slider has_labels">
                                <ol class="slider_labels">
                                    <li>0</li>
                                    <li>10</li>
                                    <li>20</li>
                                    <li>30</li>
                                    <li>40</li>
                                    <li>50</li>
                                    <li>60</li>
                                    <li>70</li>
                                    <li>80</li>
                                    <li>90</li>
                                    <li class="last-child">100</li>		
                                </ol>
                            </div>
                            <strong id="slider_value" style="float: right; font-size: 16px; margin-top: -15px;">0</strong>
                        </div>
                    </fieldset>

                    <fieldset class="label_side">
                        <label>Vertical Sliders</label>
                        <div class="clearfix">
                            <div class="slider_vertical">
                                <span>88</span>
                                <span>77</span>
                                <span>55</span>
                                <span>33</span>
                                <span>40</span>
                                <span>45</span>
                                <span>70</span>
                                <span>60</span>
                                <span>55</span>
                                <span>53</span>
                                <span>58</span>
                                <span>70</span>
                                <span>86</span>
                                <span>75</span>
                                <span>45</span>
                                <span>53</span>
                                <span>60</span>
                                <span>55</span>
                                <span>53</span>
                                <span class="last-child">70</span>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="label_side">
                        <label>Slider Range</label>
                        <div>
                            <div class="slider_range"></div>
                        </div>
                    </fieldset>

                    <fieldset class="label_side">
                        <label>Progress Bar</label>
                        <div>
                            <div class="progressbar"></div>
                        </div>
                    </fieldset>

                    <fieldset class="label_side">
                        <label>Colour Picker</label>
                        <div>
                            <div id="colorpicker_inline"></div>
                        </div>
                    </fieldset>

                    <fieldset class="label_side">
                        <label>Star Ratings</label>
                        <div class="clearfix">
                            <div id="star_rating">
                                <select name="selrate">
                                    <option value="1">Very poor</option>
                                    <option value="2">Not that bad</option>
                                    <option value="3">Average</option>
                                    <option value="4" selected="selected">Good</option>
                                    <option value="5">Perfect</option>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="box grid_8">
            <div class="toggle_container">
                <div class="block">
                    <div class="section" style="padding-bottom:5px;">
                        <div class="alert dismissible alert_blue">
                            <img height="24" width="24" src="images/icons/small/white/alert_2.png">
                            This is a <strong>blue</strong> Alert!
                        </div>					
                        <div class="alert dismissible alert_green">
                            <img height="24" width="24" src="images/icons/small/white/alert.png">
                            This is a <strong>green</strong> Alert!
                        </div>
                        <div class="alert dismissible alert_red">
                            <img height="24" width="24" src="images/icons/small/white/alarm_bell.png">
                            This is a <strong>red</strong> Alert!
                        </div>
                        <div class="alert alert_orange">
                            <img height="24" width="24" src="images/icons/small/white/alarm_clock.png">
                            This is an <strong>orange</strong> Alert and cannot be dismissed.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="alerts_anchor" class="box grid_8">
            <div class="toggle_container">
                <div class="block">
                    <div class="section" style="padding-bottom:5px;">
                        <div class="alert dismissible alert_navy">
                            <img height="24" width="24" src="images/icons/small/white/cog_3.png">
                            This is a <strong>navy</strong> Alert!
                        </div>
                        <div class="alert dismissible alert_black">
                            <img height="24" width="24" src="images/icons/small/white/locked_2.png">
                            This is a <strong>black</strong> Alert!
                        </div>
                        <div class="alert dismissible alert_grey">
                            <img height="24" width="24" src="images/icons/small/white/speech_bubble_2.png">
                            This is a <strong>grey</strong> Alert!
                        </div>
                        <div class="alert alert_light">
                            <img height="24" width="24" src="images/icons/small/grey/speech_bubble_2.png">
                            This alert cannot be dismissed.
                        </div>
                    </div>
                </div>
            </div>
        </div>						
    </div>
</div>

<div class="display_none">						
    <div id="dialog_form" class="dialog_content" title="Dialog with Form fields">
        <div class="block">
            <div class="section">
                <h2>Dialog Fields</h2>
                <div class="alert dismissible alert_black">
                    <img width="24" height="24" src="images/icons/small/white/alert_2.png">
                    <strong>All the form fields</strong> can be just as easily used in a dialog.
                </div>
            </div>													
            <fieldset class="label_side">
                <label>Text Field<span>Label placed beside the Input</span></label>
                <div>
                    <input type="text">
                </div>
            </fieldset>

            <fieldset class="label_side">
                <label>Textarea<span>Regular Textarea</span></label>
                <div class="clearfix">
                    <textarea class="autogrow"></textarea>
                </div>
            </fieldset>
            <fieldset class="label_side">																											
                <label>Bounce Slider<span>Slide to Unlock</span></label>
                <div style="padding-top:25px;">
                    <div class="slider_unlock" title="Slide to Close"></div>
                    <button class="close_dialog display_none"></button>
                </div>
            </fieldset>
            <div class="button_bar clearfix">
                <button class="dark green close_dialog">
                    <div class="ui-icon ui-icon-check"></div>
                    <span>Submit</span>
                </button>
                <button class="dark red close_dialog">
                    <div class="ui-icon ui-icon-closethick"></div>
                    <span>Cancel</span>
                </button>
            </div>
        </div>
    </div>
</div> 		
<div id="loading_overlay">
    <div class="loading_message round_bottom">
        <img src="images/loading.gif" alt="loading" />
    </div>
</div>


<div id="template_options" class="clearfix">
    <h3><img src="images/adminica.png" alt="Adminica"></h3>
    <div class="layout_size round_all">
        <label>Layout:</label>
        <a href="styles/theme/switcher1.php?style=switcher.css">Fluid</a><span>|</span>
        <a href="styles/theme/switcher1.php?style=layout_fixed.css">Fixed</a><span>
    </div>
    <div class="layout_position round_all">
        <label class="display_none">Header: </label>
        <a href="styles/theme/switcher2.php?style=switcher.css">Side</a><span>|</span>
        <a href="styles/theme/switcher2.php?style=header_top.css">Top</a><span>|</span>
        <a href="styles/theme/switcher2.php?style=header_slideout.css">Slide</a>
    </div>
    <div class="layout_position round_all">
        <label>Theme: </label>
        <a href="styles/theme/switcher.php?style=multiple&switcher5.php=switcher.css&switcher4.php=switcher.css">Dark</a><span>|</span>
        <a href="styles/theme/switcher.php?style=multiple&switcher5.php=theme_light.css&switcher4.php=switcher.css">Light</a>
    </div>
    <div class="theme_colour round_all">
        <label class="display_none">Colour:</label>
        <a class="black" href="styles/theme/switcher3.php?style=switcher.css"><span>Black</span></a>
        <a class="blue" href="styles/theme/switcher3.php?style=theme_blue.css"><span>Blue</span></a>
        <a class="navy" href="styles/theme/switcher3.php?style=theme_navy.css"><span>Navy</span></a>
        <a class="red" href="styles/theme/switcher3.php?style=theme_red.css"><span>Red</span></a>
        <a class="green" href="styles/theme/switcher3.php?style=theme_green.css"><span>Green</span></a>
        <a class="magenta" href="styles/theme/switcher3.php?style=theme_magenta.css"><span>Magenta</span></a>
        <a class="orange" href="styles/theme/switcher3.php?style=theme_brown.css"><span>Brown</span></a>
    </div>
    <div class="theme_background round_all" id="bg_dark">
        <label>BGs:</label>
        <a href="styles/theme/switcher4.php?style=switcher.css">Boxes</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_punched.css">Punched</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_honeycomb.css">Honeycomb</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_wood.css">Wood</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_dark_wood.css">Timber</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_noise.css">Noise</a>
    </div>
    <div class="theme_background round_all" id="bg_light">
        <label>BGs:</label>
        <a href="styles/theme/switcher4.php?style=switcher.css">Silver</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_white_wood.css">Wood</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_squares.css">Squares</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_noise_zero.css">Noise</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_stripes.css">Stripes</a>
    </div>
</div>

</body>
</html>