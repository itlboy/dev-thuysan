// Global

jQuery(document).ready(function() {
    // Tu dong mo chuc nang Update khi double click vao gridview
    jQuery('.grid-view table tbody tr td').live('dblclick', function() {
        var btnUpdate = jQuery(this).parent().find('.btnGridUpdate');
        if (btnUpdate.attr('onclick') !== undefined) {
            jQuery(this).parent().find('.btnGridUpdate').click();
        } else {
            jQuery(window.location).attr('href', btnUpdate.attr('href'));
        }
    });
});

// Chuyen den vi tri xac dinh
function moveto(position, speed) {
    $(document).ready(function() {
        $('html, body').animate({
            scrollTop: $(position).offset().top
        }, speed);
    });
}

// Submit form
function submitform(id) {
    $(document).ready(function() {
        $('#' + id + '').submit();
    });
}

// Mở Dialog voi ajax
function loadAjaxDialog(url, title, width) {
    width = typeof(width) != 'undefined' ? width : 'auto';

    var ajaxDialog = jQuery('#ajaxPageDialog');

    if (ajaxDialog.length == 0)
        ajaxDialog = jQuery('<div id="ajaxPageDialog" style="display:none"></div>').appendTo('body');

    ajaxDialog.dialog({
        autoOpen: false,
        title: title,
        modal: true,
        height: 'auto',
        width: width
    }).load(url, function() {
        ajaxDialog.dialog("open");
    });
    return false;
}

// Lưu form được mở trong Dialog
function submitAjax(idForm, url) {
    var queryString = $('#' + idForm + '').serialize();
    if (queryString != '') {
        jQuery.ajax({
            url: url,
            type: 'POST',
            data: queryString,
            dataType: 'json',
            beforeSend: function() {
                jQuery('.errorMessage').remove();
            },
            success: function(data) {
                if (data.status == 'success') {
                    jQuery.fn.yiiGridView.update(idForm.replace('form', 'grid'));
                    jQuery("#dialog").dialog("close");
                }
                if (data.status == 'error') {
                    jsdata = jQuery.parseJSON(data.jsdata);
                    jQuery.each(jsdata, function(key, value) {
                        jQuery('#' + key).after('<div class="errorMessage">' + value + '</div>');
                    });
                }
            }
        });
    }
}


// Check all
function checkallrow(object, value) {
    var checkall = $('#checkall').is(':checked');
    $('input[' + object + '=' + value + ']').each(function()
    {
        if (checkall == true)
            this.checked = true;
        else if (checkall == false)
            this.checked = false;
    });
    checkShowAction(object, value);
}

// Kiem tra xem co ton tai checkbox (checked) hay khong, neu co thi hien thi, khong co thi an
function checkShowAction(object, value) {
    if (checkExistCheckbox(object, value) > 0)
        $('.actionToCheckbox').show();
    else
        $('.actionToCheckbox').hide();
}

// Kiem tra co ton tai checkbox (checked) hay khong, neu khong co thi return false, neu co thi return count
function checkExistCheckbox(object, value) {
//    var checkall = $('#checkall').is(':checked');
    var count = 0;
    $('input[' + object + '=' + value + ']').each(function()
    {
        if (this.checked == true)
            count++;
    });
    if (count == 0)
        count = false;
    return count;
}

// Load ajax
// id la id cua element ma ket qua tra ve
function loadAjax(url, param, id) {
    $(document).ready(function() {
        $.ajax({
            url: url,
            type: 'POST',
            data: param,
            success: function(data) {
                $('#' + id).html(data);
            }
        });
    });
}

// Load ajax return Parent
// Dung cho truong hop ket qua tra ve can dua vao parent cua element
function loadAjaxReturnParent(url, param, id) {
    $(document).ready(function() {
        $.ajax({
            url: url,
            type: 'POST',
            data: param,
            success: function(data) {
                $('#' + id).parent().html(data);
            }
        });
    });
}


// Backend

// Chuyển từ dạng text sang dạng input để sửa trực tiếp cho 1 key của module ngôn ngữ
function editLangKey(id) {
//    var test = $('#row_'+id+':parent').text();
    var result;
    var trParent = $('#row_' + id).parent().parent();

    // Neu da chuyen che do sua roi thi return false
    if (trParent.is('.editingField'))
        return false;

    trParent.addClass('editingField');
    $('.saveButton').show();

    // Tim va chuyen che do sua
    trParent.find($('.editField')).each(function(index) {
        result = '<input name="list[' + id + '][' + $(this).attr('lang') + ']" id="' + $(this).attr('id') + '" value="' + $(this).text() + '" style="width: 100%; height: 25px; border: 0;" />';
        $(this).addClass('background_1').html(result);
    });
}

// Chuyển từ dạng text sang dạng input để sửa trực tiếp
function stringToInput(parent, inputValue, inputName) {
    var result = '<input name="' + inputName + '" id="' + inputName + '" value="' + inputValue + '" style="width: 100%; height: 25px; border: 0;" />';
    $('#' + parent).addClass('background_1').html(result);
    $('.saveButton').show();
}

/**
 *
 */
//function changeValueField(model, idField, valueField, changeField, changeValue, idResultElement) {
//    var url = '';
//    loadAjax (url, '', idResultElement);
//}

/**
 * module 
 * id (INT or STRING - 1,2,3,4,5...)
 * Xoa 1 hoac nhieu ban ghi, xac dinh TABLE thông qua URL
 */
//function deleteAjax (url, id) {
//	$(document).ready(function() {
//		$.ajax({
//            type: 'POST',
//			url: url,
//            data: 'id=' + id,
//			success: function(data) {
//				$('#'+id).html(data);
//			}
//		});
//	});
//}


function ajaxDialogForm(grid, title, url, height, width) {
    var ajaxDialog = jQuery('#ajaxPageDialog');
    var gridId = typeof(grid) != 'undefined' ? 'ajax=' + grid : '';
    var urlSep = (url.indexOf('?') == -1) ? '?' : '&';
    height = typeof(height) != 'undefined' ? height : 'auto';
    width = typeof(width) != 'undefined' ? width : 'auto';

    if (ajaxDialog.length == 0)
        ajaxDialog = jQuery('<div id="ajaxPageDialog" style="display:none"></div>').appendTo('body');

    ajaxDialog.empty();
    ajaxDialog.dialog({
        'autoOpen': false,
        'modal': true,
        'height': height,
        'width': width,
        'title': title,
        'close': function(event, ui) {
        }
    });
    ajaxDialog.load(url + urlSep + gridId, function() {
        ajaxDialog.dialog('open');
    });
    return false;
}

function ajaxSaveForm(model, form, url) {
    jQuery.ajax({
        'url': url,
        'type': 'POST',
        'dataType': 'json',
        'data': jQuery('#' + form).serialize(),
        'beforeSend': function(XMLHttpRequest) {
            jQuery('.errorMessage').remove();
        },
        'success': function(data) {
            if (data.status == 'success') {
                jQuery('#ajaxPageDialog').dialog('close');
                ajaxDialogReload(form.replace('form', 'grid'));
            }
            if (data.status == 'error') {
                jrvalue = jQuery.parseJSON(data.jrvalue);
                jQuery.each(jrvalue, function(key, value) {
                    jQuery('#' + form).find('#' + model + '_' + key).val(value);
                });

                jrvalid = jQuery.parseJSON(data.jrvalid);
                jQuery.each(jrvalid, function(key, value) {
                    jQuery('#' + form).find('#' + key).after('<div class="errorMessage">' + value + '</div>');
                });
            }

        }
    });
}

function ajaxDialogReload(grid) {
    var ajaxDialog = jQuery('#ajaxPageDialog');

    if (jQuery('#' + grid).length != 0)
        jQuery.fn.yiiGridView.update(grid);

    if (ajaxDialog.length != 0)
        ajaxDialog.dialog('close');
}

function ajaxGridSort(grid, id, url, moveTo) {
    jQuery.ajax({
        'url': url,
        'type': 'POST',
        'dataType': 'JSON',
        'cache': false,
        'data': {
            'id': id,
            'sort': moveTo,
            'YII_CSRF_TOKEN': YII_CSRF_TOKEN
        }, 'beforeSend': function() {
        }, 'success': function(data) {
            jQuery('#' + grid).yiiGridView.update(grid);
        }
    });
}

/*
 * Hien dialog xac nhan, cho phep xoa cung luc nhieu row trong gridview
 * @grid id cua gridview
 * @url url den action thuc hien chuc nang cap nhat status
 */
function ajaxGridDelete(module, model, grid, url, field, value, trash) {
    trash = typeof(trash) != 'undefined' ? trash : 0;
    var ajaxDialog = jQuery('#dialogConfirm');
    if (ajaxDialog.length == 0)
        ajaxDialog = jQuery('<div id="dialogConfirm" style="display:none"></div>').appendTo('body');

    ajaxDialog.empty();
    ajaxDialog.html('Are you sure you want to delete?');
    ajaxDialog.dialog({
        'autoOpen': false,
        'modal': true,
        'minHeight': 140,
        'minWidth': 230,
        'title': 'Confirm',
        'buttons': [{
                text: 'Delete',
                click: function() {
                    jQuery(this).dialog('close');
                    jQuery.ajax({
                        'url': url,
                        'type': 'POST',
                        'cache': false,
                        'data': {
                            'module': module,
                            'model': model,
                            'field': field,
                            'value': value,
                            'trash': trash,
                            'YII_CSRF_TOKEN': YII_CSRF_TOKEN
                        }, 'beforeSend': function() {
                        }, 'success': function() {
                            jQuery.fn.yiiGridView.update(grid);
                        }
                    });
                }
            }, {
                text: 'No',
                click: function() {
                    jQuery(this).dialog('close');
                }
            }]
    });
    ajaxDialog.dialog('open');
    return false;
}

/*
 * Hien dialog xac nhan, cho phep cap nhat trang thai nhieu row trong gridview
 * @grid id cua gridview
 * @url url den action thuc hien chuc nang cap nhat status
 * @status trang thai can cap nhat (1/0)
 * @column cot trong table se cap nhat
 */
function ajaxChangeBooleanValue(module, model, field, value, url, grid) {
    jQuery.ajax({
        'url': url,
        'type': 'POST',
        'cache': false,
        'data': {
            'module': module,
            'model': model,
            'field': field,
            'value': value,
            'ids': jQuery.fn.yiiGridView.getChecked(grid, 'chkIds'),
            'YII_CSRF_TOKEN': YII_CSRF_TOKEN
        },
        'success': function() {
            jQuery('#' + grid).yiiGridView.update(grid);
        }
    });
    return false;
}

function ajaxChangeStatusValue(module, model, id, field, value, url, grid) {
    jQuery.ajax({
        'url': url,
        'type': 'POST',
        'cache': false,
        'data': {
            'module': module,
            'model': model,
            'field': field,
            'value': value,
            'ids': id,
            'YII_CSRF_TOKEN': YII_CSRF_TOKEN
        },
        'success': function() {
            jQuery('#' + grid).yiiGridView.update(grid);
        }
    });
    return false;
}

/*
 * Hien dialog xac nhan, cho phep xoa cung luc nhieu row trong gridview
 * @grid id cua gridview
 * @url url den action thuc hien chuc nang cap nhat status
 */
function ajaxDeleteMultiRecord(module, model, url, grid, trash, field, value) {
    trash = typeof(trash) != 'undefined' ? trash : 0;
    var ajaxDialog = jQuery('#dialogManage');
    if (ajaxDialog.length == 0)
        ajaxDialog = jQuery('<div id="dialogManage" style="display:none"></div>').appendTo('body');

    ajaxDialog.empty();
    ajaxDialog.html('Are you sure you want to perform this action?');
    ajaxDialog.dialog({
        'autoOpen': false,
        'modal': true,
        'minHeight': 140,
        'minWidth': 230,
        'title': 'Confirm',
        'buttons': [{
                text: 'Delete',
                click: function() {
                    jQuery(this).dialog('close');
                    jQuery.ajax({
                        'url': url,
                        'type': 'POST',
                        'cache': 'false',
                        'data': {
                            'module': module,
                            'model': model,
                            'trash': trash,
                            'field': field,
                            'value': value,
                            'ids': jQuery.fn.yiiGridView.getChecked(grid, 'chkIds'),
                            'YII_CSRF_TOKEN': YII_CSRF_TOKEN
                        },
                        'success': function() {
                            jQuery('#' + grid).yiiGridView.update(grid);
                        }
                    });
                }
            }, {
                text: 'Cancel',
                click: function() {
                    jQuery(this).dialog('close');
                }
            }]
    });
    ajaxDialog.dialog('open');
    return false;
}



