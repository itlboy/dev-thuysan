<!doctype html public "✰">
<!--[if lt IE 7]> <html lang="en-us" class="no-js ie6"> <![endif]-->
<!--[if IE 7]>    <html lang="en-us" class="no-js ie7"> <![endif]-->
<!--[if IE 8]>    <html lang="en-us" class="no-js ie8"> <![endif]-->
<!--[if IE 9]>    <html lang="en-us" class="no-js ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-us" class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">

<title>Adminica | The Professional Admin Theme</title>

<!-- iPhone, iPad and Android specific settings -->	

<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1;">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
<link rel="apple-touch-icon" href="images/iOS_icon.png">
<link rel="apple-touch-startup-image" href="images/iOS_startup.png">

<!-- Styles -->

<link rel="stylesheet" type="text/css" href="styles/reset.css">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,700">

<link rel="stylesheet" type="text/css" href="scripts/fancybox/jquery.fancybox-1.3.4.css">
<link rel="stylesheet" type="text/css" href="scripts/tinyeditor/style.css">
<link rel="stylesheet" type="text/css" href="scripts/slidernav/slidernav.css">
<link rel="stylesheet" type="text/css" href="scripts/syntax_highlighter/styles/shCore.css">
<link rel="stylesheet" type="text/css" href="scripts/syntax_highlighter/styles/shThemeDefault.css">
<link rel="stylesheet" type="text/css" href="scripts/uitotop/css/ui.totop.css">
<link rel="stylesheet" type="text/css" href="scripts/fullcalendar/fullcalendar.css">
<link rel="stylesheet" type="text/css" href="scripts/isotope/isotope.css">
<link rel="stylesheet" type="text/css" href="scripts/elfinder/css/elfinder.css">

<link rel="stylesheet" type="text/css" href="scripts/tiptip/tipTip.css">
<link rel="stylesheet" type="text/css" href="scripts/uniform/css/uniform.aristo.css">
<link rel="stylesheet" type="text/css" href="scripts/multiselect/css/ui.multiselect.css">
<link rel="stylesheet" type="text/css" href="scripts/selectbox/jquery.selectBox.css">
<link rel="stylesheet" type="text/css" href="scripts/colorpicker/css/colorpicker.css">
<link rel="stylesheet" type="text/css" href="scripts/uistars/jquery.ui.stars.min.css">

<link rel="stylesheet" type="text/css" href="scripts/themeroller/Aristo.css">

<link rel="stylesheet" type="text/css" href="styles/text.css">
<link rel="stylesheet" type="text/css" href="styles/grid.css">
<link rel="stylesheet" type="text/css" href="styles/main.css">
<link rel="stylesheet" type="text/css" href="styles/theme/theme_base.css">

<!-- Style Switcher  

The following stylesheet links are used by the styleswitcher to allow for dynamically changing how Adminica looks and acts.
Styleswitcher documentation: http://style-switcher.webfactoryltd.com/documentation/

switcher1.php : layout - fluid by default.								(eg. styles/theme/switcher1.php?default=layout_fixed.css)
switcher2.php : header and sidebar positioning - sidebar by default.	(eg. styles/theme/switcher1.php?default=header_top.css)
switcher3.php : colour skin - black/grey by default.					(eg. styles/theme/switcher1.php?default=theme_red.css)
switcher4.php : background image - dark boxes by default.				(eg. styles/theme/switcher1.php?default=bg_honeycomb.css)
switcher5.php : controls the theme - dark by default.					(eg. styles/theme/switcher1.php?default=theme_light.css)

-->

<link rel="stylesheet" type="text/css" href="styles/theme/switcher.css" media="screen">
<link rel="stylesheet" type="text/css" href="styles/theme/layout_fixed.css" media="screen" > 
<!--		<link rel="stylesheet" type="text/css" href="styles/theme/switcher2.php?default=switcher.css" media="screen" >-->
<link rel="stylesheet" type="text/css" href="styles/theme/theme_blue.css" media="screen" >
<link rel="stylesheet" type="text/css" href="styles/theme/bg_wood.css" media="screen" >
<!--		<link rel="stylesheet" type="text/css" href="styles/theme/switcher5.php?default=switcher.css" media="screen" >-->

<link rel="stylesheet" type="text/css" href="styles/colours.css">
<link rel="stylesheet" type="text/css" href="styles/ie.css">

<!-- Scripts -->

<!-- Load JQuery -->		
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<!-- Load JQuery UI -->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/jquery-ui.min.js" type="text/javascript"></script>

<!-- Global -->
<script src="scripts/touchPunch/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
<script src="scripts/uitotop/js/jquery.ui.totop.js" type="text/javascript"></script>

<!-- Forms -->
<script src="scripts/uniform/jquery.uniform.min.js"></script>
<script src="scripts/autogrow/jquery.autogrowtextarea.js"></script>
<script src="scripts/multiselect/js/ui.multiselect.js"></script>
<script src="scripts/selectbox/jquery.selectBox.min.js"></script>
<script src="scripts/timepicker/jquery.timepicker.js"></script>
<script src="scripts/colorpicker/js/colorpicker.js"></script>
<script src="scripts/uistars/jquery.ui.stars.min.js"></script>
<script src="scripts/tiptip/jquery.tipTip.minified.js"></script>
<script src="scripts/validation/jquery.validate.min.js" type="text/javascript"></script>		

<!-- Configuration Script -->
<script type="text/javascript" src="scripts/adminica/adminica_ui.js"></script>
<script type="text/javascript" src="scripts/adminica/adminica_forms.js"></script>
<script type="text/javascript" src="scripts/adminica/adminica_mobile.js"></script>

<!-- Live Load (remove after development) -->
<script>//document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>

</head>
<body>		<div id="wrapper">	
    <div id="topbar" class="clearfix">

        <a href="dashboard_sorter.php" class="logo"><span>Adminica</span></a>

        <div class="user_box dark_box clearfix">
            <img src="images/profile.jpg" width="55" alt="Profile Pic" />
            <h2>Administrator</h2>
            <h3><a class="text_shadow" href="#">John Smith</a></h3>
            <ul>
                <li><a href="#">profile</a><span class="divider">|</span></li>
                <li><a href="#">settings</a><span class="divider">|</span></li>
                <li><a href="login.php">logout</a></li>
            </ul>
        </div><!-- #user_box -->	
    </div><!-- #topbar -->		
    <div id="sidebar">
        <div class="cog">+</div>

        <a href="index.php" class="logo"><span>Adminica</span></a>

        <div class="user_box dark_box clearfix">
            <img src="images/profile.jpg" width="55" alt="Profile Pic" />
            <h2>Administrator</h2>
            <h3><a href="#">John Smith</a></h3>
            <ul>
                <li><a href="#">settings</a><span class="divider">|</span></li>
                <li><a href="login.php">logout</a></li>
            </ul>
        </div><!-- #user_box -->

        <ul class="side_accordion"> <!-- add class 'open_multiple' to change to from accordion to toggles -->
            <li><a href="#"><img src="images/icons/small/grey/home.png"/>Home</a>
                <ul class="drawer">
                    <li><a href="#">Activity</a></li>
                    <li><a href="#">Events</a></li>
                    <li><a href="#">Tasks</a></li>
                </ul>
            </li>
            <li><a href="#"><img src="images/icons/small/grey/mail.png"/>Mailbox<span class="alert badge alert_red">5</span></a>
                <ul class="drawer">
                    <li><a href="#">Inbox</a></li>
                    <li><a href="#">Sent Items</a></li>
                    <li><a href="#">Drafts<span class="alert badge alert_grey">2</span></a></li>
                    <li><a href="#">Trash<span class="alert badge alert_grey">3</span></a></li>
                </ul>
            </li>
            <li><a href="#"><img src="images/icons/small/grey/documents.png"/>Documents<span class="alert badge alert_black">2</span></a>
                <ul class="drawer">
                    <li><a href="#">Create New</a></li>
                    <li><a href="#">View All</a></li>
                    <li><a href="#">Upload/Download<span class="alert badge alert_grey">2</span></a></li>
                </ul>
            </li>
            <li><a href="#"><img src="images/icons/small/grey/users.png"/>Members</a>
                <ul class="drawer">
                    <li><a href="#">Add New</a></li>
                    <li><a href="#">Edit/Delete</a></li>
                    <li><a href="#">Search Profiles</a></li>
                </ul>
            </li>
            <li><a href="http://www.tricycle.ie"><img src="images/icons/small/grey/graph.png"/>Statistics</a></li>
            <li><a href="#"><img src="images/icons/small/grey/cog_2.png"/>Settings</a>
                <ul class="drawer">
                    <li><a href="#">Account</a></li>
                    <li><a href="#">System</a></li>
                </ul>
            </li>
        </ul>
        <form>
            <div id="search_side" class="dark_box"><input class="" type="text" value="Search Adminica..." onclick="value=''"></div>
        </form>
        <ul id="side_links" class="side_links" style="margin-bottom:0;">
            <li><a href="http://goo.gl/UjRRe">Online Documentation</a>
            <li><a href="support.tricyclelabs.com">Expert Forum</a></li>
            <li><a href="#">Product Wiki</a></li>
            <li><a href="#">Latest Company News</a></li>
        </ul>
    </div><!-- #sidebar -->
    <div id="main_container" class="main_container container_16 clearfix">
        <div id="nav_top" class="clearfix round_top">
            <ul class="clearfix">
                <li><a href="index.php"><img src="images/icons/small/grey/laptop.png"/><span class="display_none">Home</span></a></li>

                <li><a href="#"><img src="images/icons/small/grey/frames.png"/><span>Layout</span></a>
                    <ul>
                        <li><a href="layout.php"><span>16 Grid - 960.gs</span></a></li>
                        <li><a href="text.php"><span>Text & Typography</span></a></li>
                        <li><a class="hide_mobile" href="#"><span>Layout Width</span></a>
                            <ul class="drawer">						
                                <li><a href="styles/theme/switcher1.php?style=layout_fixed.css"><span>Fixed</span></a></li>
                                <li><a href="styles/theme/switcher1.php?style=switcher.css"><span>Fluid</span></a></li>
                            </ul>
                        </li>
                        <li><a class="hide_mobile" href="#"><span>Layout Position</span></a>
                            <ul class="drawer">
                                <li><a href="styles/theme/switcher2.php?style=switcher.css"><span>Side</span></a></li>
                                <li><a href="styles/theme/switcher2.php?style=header_top.css"><span>Top</span></a></li>
                                <li><a href="styles/theme/switcher2.php?style=header_slideout.css"><span>Slide</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="#"><img src="images/icons/small/grey/coverflow.png"/><span>Boxes</span></a>
                    <ul>
                        <li><a href="tabs.php"><span>Tab Boxes</span></a></li>
                        <li><a href="accordions.php"><span>Accordions Boxes</span></a></li>
                        <li><a href="wizard.php"><span>Step by Step Wizard</span></a></li>
                        <li><a href="code.php"><span>Code View</span></a></li>
                    </ul>
                </li>	
                <li><a href="#"><img src="images/icons/small/grey/create_write.png"/><span>Forms</span><span class="alert badge alert_red">new</span></a>
                    <ul>
                        <li><a href="forms.php"><span>Input Fields</span></a></li>
                        <li><a href="buttons.php"><span>Buttons</span></a></li>				
                        <li><a href="#"><span>More components</span></a>
                            <ul class="drawer">
                                <li><a href="validation.php"><span>Validation</span></a></li>
                                <li><a href="editor.php"><span>WYSIWYG Editor</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </li>		
                <li><a href="gallery.php"><img src="images/icons/small/grey/images.png"/><span>Gallery</span></a></li>			
                <li><a href="#"><img src="images/icons/small/grey/blocks_images.png"/><span>Tables</span></a>
                    <ul>
                        <li><a href="tables.php"><span>DataTables</span></a></li>
                        <li><a href="tables_static.php"><span>Regular Tables</span><span class="alert badge alert_black">new</span></a></li>
                    </ul>			
                </li>
                <li><a href="#"><img src="images/icons/small/grey/file_cabinet.png"/><span>Org</span></a>
                    <ul>
                        <li><a href="files.php"><img src="images/icons/small/grey/folder.png"/><span>Files</span></a></li>
                        <li><a href="contacts.php"><img src="images/icons/small/grey/users.png"/><span>Contacts</span></a></li>
                    </ul>
                </li>
                <li><a href="calendar.php"><img src="images/icons/small/grey/month_calendar.png"/><span>Cal</span><span class="alert badge alert_blue">new</span></a></li>
                <li><a href="charts.php"><img src="images/icons/small/grey/graph.png"/><span>Charts</span><span class="alert badge alert_green">new</span></a></li>
                <li><a href="#"><img src="images/icons/small/grey/locked_2.png"/><span class="display_none">Login</span></a>
                    <ul class="dropdown_right">
                        <li><a href="login_regular.php" class="dialog_button" data-dialog="logout"><span>Regular Login</span></a></li>
                        <li><a href="login.php" class="dialog_button" data-dialog="logout"><span>Slide Login</span></a></li>
                    </ul>
                </li>
            </ul>
            <div class="display_none">						
                <div id="logout" class="dialog_content narrow" title="Logout">
                    <div class="block">
                        <div class="section">
                            <h1>Thank you</h1>
                            <div class="dashed_line"></div>	
                            <p>We will now log you out of Adminica in a 10 seconds...</p><p></p>
                        </div>
                        <div class="button_bar clearfix">
                            <button class="dark blue no_margin_bottom link_button" data-link="login.php">
                                <div class="ui-icon ui-icon-check"></div>
                                <span>Ok</span>
                            </button>
                            <button class="light send_right close_dialog">
                                <div class="ui-icon ui-icon-closethick"></div>
                                <span>Cancel</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div> 		


            <script type="text/javascript">
                var currentPage = 2 - 1; // This is only used in php version to tell the nav what the current page is
                $('#nav_top > ul > li').eq(currentPage).addClass("current");
                $('#nav_top > ul > li').addClass("icon_only").children("a").children("span:visible").parent().parent().removeClass("icon_only");
            </script>



            <div id="mobile_nav">
                <div class="main"></div>
                <div class="side"></div>
            </div>

        </div><!-- #nav_top -->
        <div class="flat_area grid_16">
            <h2>Beautifully formatted text content</h2>
            <p><strong>Adminica</strong> will make your text and <strong>typography content look great</strong>. All the usual elements from headers to ordered list items have been thoroughly styled. You can put your text right on the main page or in a box. You can create easy to read columns using the <a target="_blank" href="http://www.960.gs">960.gs Grid System</a></p>
        </div>
        <div class="flat_area grid_8">
            <h2>Column 1</h2>
            <p>At vero eos et accusamus et iusto odio <a href="#" title="this is a tooltip">dignissimos</a> ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt <strong>mollitia</strong> animi, id est laborum et dolorum fuga. </p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et <strong>dolore</strong> magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
        </div>
        <div class="flat_area grid_8">
            <h2>Column 2</h2>
            <p>At vero eos et accusamus et iusto odio <a href="#" title="this is a tooltip">dignissimos</a> ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias.</p>
            <ul>
                <li>List Item One</li>
                <li>List Item Two</li>
                <li>List Item Three</li>
            </ul>
            <p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime <strong>placeat facere possimus</strong>, omnis voluptas assumenda est, omnis dolor repellendus. </p>
        </div>
        <div class="box grid_8">
            <h2 class="box_head">
                Text Formatting
            </h2>
            <a href="#" class="grabber">&nbsp;</a>
            <a href="#" class="toggle">&nbsp;</a>
            <div class="toggle_container">	
                <div class="block">
                    <div class="section">
                        <h1>Primary Heading</h1>
                        <p>Lorem Ipsum is simply dummy text of the printing and <a href="#" title="This is a tooltip">typesetting</a> industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        <h2>Secondary Heading</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s <strong>standard dummy</strong> text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
                        <h3>Tertiary Heading</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        <h4>Quaternary Heading</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        <h5>Quinary Heading	</h5>
                        <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="box grid_8">
            <h2 class="box_head">List Formatting</h2>
            <a href="#" class="grabber">&nbsp;</a>
            <a href="#" class="toggle">&nbsp;</a>
            <div class="toggle_container">
                <div class="block">
                    <div class="section">
                        <h2>Ordered Lists</h2>
                        <ol>
                            <li>List Item One</li>
                            <li>List Item Two</li>
                            <li>List Item Three</li>
                        </ol>
                        <h2>Un-Ordered Lists</h2>
                        <ul>
                            <li>List Item One</li>
                            <li>List Item Two</li>
                            <li>List Item Three</li>
                        </ul>
                        <h2>Un-Ordered Lists with text</h2>
                        <ul>
                            <li>It has survived not only five centuries, but also the leap into <strong>electronic</strong> typesetting, remaining essentially unchanged.</li>
                            <li>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</li>
                        </ul>
                        <h2>Ordered List with text</h2>
                        <ol>
                            <li><strong>Lorem Ipsum</strong> has been the industry’s standard dummy text ever since the 1500s, took a galley of type and scrambled it to make a type specimen book.</li>
                            <li>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid_16 clearfix">
            <div class="alert dismissible alert_blue">
                <img width="24" height="24" src="images/icons/small/white/alert_2.png">
                <strong>Brand new feature:</strong> Adminica now has a flexible column system for use within content boxes.
            </div>
        </div>
        <div class="box grid_16">
            <h2 class="box_head">
                Multi-column Text
            </h2>
            <a href="#" class="grabber">&nbsp;</a>
            <a href="#" class="toggle">&nbsp;</a>
            <div class="toggle_container">	
                <div class="block">
                    <div class="columns clearfix">
                        <div class="col_50">
                            <div class="section">
                                <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                        </div>
                        <div class="col_50">
                            <div class="section">
                                <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                        </div>
                    </div>
                    <div class="columns clearfix">
                        <div class="col_33">
                            <div class="section">
                                <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                        </div>
                        <div class="col_33">
                            <div class="section">
                                <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                        </div>
                        <div class="col_33">
                            <div class="section">
                                <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                        </div>
                    </div>
                    <div class="columns clearfix">
                        <div class="col_25">
                            <div class="section">
                                <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                        </div>
                        <div class="col_25">
                            <div class="section">
                                <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                        </div>
                        <div class="col_25">
                            <div class="section">
                                <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                        </div>
                        <div class="col_25">
                            <div class="section">
                                <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                        </div>
                    </div>
                    <div class="columns clearfix">
                        <div class="col_40">
                            <div class="section">
                                <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                        </div>
                        <div class="col_60">
                            <div class="section">
                                <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>					
        </div>
    </div>
    <div class="main_container container_16 clearfix">
        <div class="flat_area grid_16">
            <h2>Fixed Height Content</h2>
            <p>Boxes can be given<strong> fixed heights</strong> and the content can be viewed by scrolling up and down. This can be handy if you want to keep the main interface above the fold or you want to avoid really long pages. This can have a header or be just a plain box. </p>
            <p><strong>Note: </strong>A two finger <strong>dragging gesture</strong> is used to navigate the content on a <strong>touchscreen</strong> such as iPhone or Android. </p>
        </div>
        <div class="box grid_8">
            <h2 class="box_head">Fixed Height Content</h2>
            <a href="#" class="grabber">&nbsp;</a>
            <a href="#" class="toggle">&nbsp;</a>
            <div class="toggle_container">
                <div class="block" style="height:200px; overflow:auto;">
                    <div class="section">
                        <h1>Primary Heading</h1>
                        <p>Lorem Ipsum is simply dummy text of the <a href="#" title="This is a tooltip">printing</a> industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 
                        <h2>Secondary Heading</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 
                        <h3>Tertiary Heading</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>				
                        <h4>Quaternary Heading</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>				
                        <h5>Quinary Heading</h5>
                        <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>				
                        <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 				
                    </div>
                </div>
            </div>
        </div>	
        <div class="box grid_8">
            <div class="block" style="height:236px; overflow:auto;">
                <div class="section">
                    <h1>Primary Heading</h1>
                    <p>Lorem Ipsum is simply dummy text of the <a href="#" title="This is a tooltip">printing</a> industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 

                    <h2>Secondary Heading</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 

                    <h3>Tertiary Heading</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 

                    <h4>Quaternary Heading</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 

                    <h5>Quinary Heading</h5>
                    <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 


                    <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 		
                </div>
            </div>
        </div>
    </div>
</div>
<div id="loading_overlay">
    <div class="loading_message round_bottom">
        <img src="images/loading.gif" alt="loading" />
    </div>
</div>


<div id="template_options" class="clearfix">
    <h3><img src="images/adminica.png" alt="Adminica"></h3>
    <div class="layout_size round_all">
        <label>Layout:</label>
        <a href="styles/theme/switcher1.php?style=switcher.css">Fluid</a><span>|</span>
        <a href="styles/theme/switcher1.php?style=layout_fixed.css">Fixed</a><span>
    </div>
    <div class="layout_position round_all">
        <label class="display_none">Header: </label>
        <a href="styles/theme/switcher2.php?style=switcher.css">Side</a><span>|</span>
        <a href="styles/theme/switcher2.php?style=header_top.css">Top</a><span>|</span>
        <a href="styles/theme/switcher2.php?style=header_slideout.css">Slide</a>
    </div>
    <div class="layout_position round_all">
        <label>Theme: </label>
        <a href="styles/theme/switcher.php?style=multiple&switcher5.php=switcher.css&switcher4.php=switcher.css">Dark</a><span>|</span>
        <a href="styles/theme/switcher.php?style=multiple&switcher5.php=theme_light.css&switcher4.php=switcher.css">Light</a>
    </div>
    <div class="theme_colour round_all">
        <label class="display_none">Colour:</label>
        <a class="black" href="styles/theme/switcher3.php?style=switcher.css"><span>Black</span></a>
        <a class="blue" href="styles/theme/switcher3.php?style=theme_blue.css"><span>Blue</span></a>
        <a class="navy" href="styles/theme/switcher3.php?style=theme_navy.css"><span>Navy</span></a>
        <a class="red" href="styles/theme/switcher3.php?style=theme_red.css"><span>Red</span></a>
        <a class="green" href="styles/theme/switcher3.php?style=theme_green.css"><span>Green</span></a>
        <a class="magenta" href="styles/theme/switcher3.php?style=theme_magenta.css"><span>Magenta</span></a>
        <a class="orange" href="styles/theme/switcher3.php?style=theme_brown.css"><span>Brown</span></a>
    </div>
    <div class="theme_background round_all" id="bg_dark">
        <label>BGs:</label>
        <a href="styles/theme/switcher4.php?style=switcher.css">Boxes</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_punched.css">Punched</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_honeycomb.css">Honeycomb</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_wood.css">Wood</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_dark_wood.css">Timber</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_noise.css">Noise</a>
    </div>
    <div class="theme_background round_all" id="bg_light">
        <label>BGs:</label>
        <a href="styles/theme/switcher4.php?style=switcher.css">Silver</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_white_wood.css">Wood</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_squares.css">Squares</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_noise_zero.css">Noise</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_stripes.css">Stripes</a>
    </div>
</div>

</body>
</html>