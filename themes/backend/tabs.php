<!doctype html public "✰">
<!--[if lt IE 7]> <html lang="en-us" class="no-js ie6"> <![endif]-->
<!--[if IE 7]>    <html lang="en-us" class="no-js ie7"> <![endif]-->
<!--[if IE 8]>    <html lang="en-us" class="no-js ie8"> <![endif]-->
<!--[if IE 9]>    <html lang="en-us" class="no-js ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-us" class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">

<title>Adminica | The Professional Admin Theme</title>

<!-- iPhone, iPad and Android specific settings -->	

<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1;">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
<link rel="apple-touch-icon" href="images/iOS_icon.png">
<link rel="apple-touch-startup-image" href="images/iOS_startup.png">

<!-- Styles -->

<link rel="stylesheet" type="text/css" href="styles/reset.css">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,700">

<link rel="stylesheet" type="text/css" href="scripts/fancybox/jquery.fancybox-1.3.4.css">
<link rel="stylesheet" type="text/css" href="scripts/tinyeditor/style.css">
<link rel="stylesheet" type="text/css" href="scripts/slidernav/slidernav.css">
<link rel="stylesheet" type="text/css" href="scripts/syntax_highlighter/styles/shCore.css">
<link rel="stylesheet" type="text/css" href="scripts/syntax_highlighter/styles/shThemeDefault.css">
<link rel="stylesheet" type="text/css" href="scripts/uitotop/css/ui.totop.css">
<link rel="stylesheet" type="text/css" href="scripts/fullcalendar/fullcalendar.css">
<link rel="stylesheet" type="text/css" href="scripts/isotope/isotope.css">
<link rel="stylesheet" type="text/css" href="scripts/elfinder/css/elfinder.css">

<link rel="stylesheet" type="text/css" href="scripts/tiptip/tipTip.css">
<link rel="stylesheet" type="text/css" href="scripts/uniform/css/uniform.aristo.css">
<link rel="stylesheet" type="text/css" href="scripts/multiselect/css/ui.multiselect.css">
<link rel="stylesheet" type="text/css" href="scripts/selectbox/jquery.selectBox.css">
<link rel="stylesheet" type="text/css" href="scripts/colorpicker/css/colorpicker.css">
<link rel="stylesheet" type="text/css" href="scripts/uistars/jquery.ui.stars.min.css">

<link rel="stylesheet" type="text/css" href="scripts/themeroller/Aristo.css">

<link rel="stylesheet" type="text/css" href="styles/text.css">
<link rel="stylesheet" type="text/css" href="styles/grid.css">
<link rel="stylesheet" type="text/css" href="styles/main.css">
<link rel="stylesheet" type="text/css" href="styles/theme/theme_base.css">

<!-- Style Switcher  

The following stylesheet links are used by the styleswitcher to allow for dynamically changing how Adminica looks and acts.
Styleswitcher documentation: http://style-switcher.webfactoryltd.com/documentation/

switcher1.php : layout - fluid by default.								(eg. styles/theme/switcher1.php?default=layout_fixed.css)
switcher2.php : header and sidebar positioning - sidebar by default.	(eg. styles/theme/switcher1.php?default=header_top.css)
switcher3.php : colour skin - black/grey by default.					(eg. styles/theme/switcher1.php?default=theme_red.css)
switcher4.php : background image - dark boxes by default.				(eg. styles/theme/switcher1.php?default=bg_honeycomb.css)
switcher5.php : controls the theme - dark by default.					(eg. styles/theme/switcher1.php?default=theme_light.css)

-->

<link rel="stylesheet" type="text/css" href="styles/theme/switcher.css" media="screen">
<link rel="stylesheet" type="text/css" href="styles/theme/layout_fixed.css" media="screen" > 
<!--		<link rel="stylesheet" type="text/css" href="styles/theme/switcher2.php?default=switcher.css" media="screen" >-->
<link rel="stylesheet" type="text/css" href="styles/theme/theme_blue.css" media="screen" >
<link rel="stylesheet" type="text/css" href="styles/theme/bg_wood.css" media="screen" >
<!--		<link rel="stylesheet" type="text/css" href="styles/theme/switcher5.php?default=switcher.css" media="screen" >-->

<link rel="stylesheet" type="text/css" href="styles/colours.css">
<link rel="stylesheet" type="text/css" href="styles/ie.css">

<!-- Scripts -->

<!-- Load JQuery -->		
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<!-- Load JQuery UI -->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/jquery-ui.min.js" type="text/javascript"></script>

<!-- Global -->
<script src="scripts/touchPunch/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
<script src="scripts/uitotop/js/jquery.ui.totop.js" type="text/javascript"></script>

<!-- Forms -->
<script src="scripts/uniform/jquery.uniform.min.js"></script>
<script src="scripts/autogrow/jquery.autogrowtextarea.js"></script>
<script src="scripts/multiselect/js/ui.multiselect.js"></script>
<script src="scripts/selectbox/jquery.selectBox.min.js"></script>
<script src="scripts/timepicker/jquery.timepicker.js"></script>
<script src="scripts/colorpicker/js/colorpicker.js"></script>
<script src="scripts/uistars/jquery.ui.stars.min.js"></script>
<script src="scripts/tiptip/jquery.tipTip.minified.js"></script>
<script src="scripts/validation/jquery.validate.min.js" type="text/javascript"></script>		

<!-- Configuration Script -->
<script type="text/javascript" src="scripts/adminica/adminica_ui.js"></script>
<script type="text/javascript" src="scripts/adminica/adminica_forms.js"></script>
<script type="text/javascript" src="scripts/adminica/adminica_mobile.js"></script>

<!-- Live Load (remove after development) -->
<script>//document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>

</head>
<body>
<div id="wrapper">
    <div id="topbar" class="clearfix">

        <a href="dashboard_sorter.php" class="logo"><span>Adminica</span></a>

        <div class="user_box dark_box clearfix">
            <img src="images/profile.jpg" width="55" alt="Profile Pic" />
            <h2>Administrator</h2>
            <h3><a class="text_shadow" href="#">John Smith</a></h3>
            <ul>
                <li><a href="#">profile</a><span class="divider">|</span></li>
                <li><a href="#">settings</a><span class="divider">|</span></li>
                <li><a href="login.php">logout</a></li>
            </ul>
        </div><!-- #user_box -->	
    </div><!-- #topbar -->		
    <div id="sidebar">
        <div class="cog">+</div>

        <a href="index.php" class="logo"><span>Adminica</span></a>

        <div class="user_box dark_box clearfix">
            <img src="images/profile.jpg" width="55" alt="Profile Pic" />
            <h2>Administrator</h2>
            <h3><a href="#">John Smith</a></h3>
            <ul>
                <li><a href="#">settings</a><span class="divider">|</span></li>
                <li><a href="login.php">logout</a></li>
            </ul>
        </div><!-- #user_box -->

        <ul class="side_accordion"> <!-- add class 'open_multiple' to change to from accordion to toggles -->
            <li><a href="#"><img src="images/icons/small/grey/home.png"/>Home</a>
                <ul class="drawer">
                    <li><a href="#">Activity</a></li>
                    <li><a href="#">Events</a></li>
                    <li><a href="#">Tasks</a></li>
                </ul>
            </li>
            <li><a href="#"><img src="images/icons/small/grey/mail.png"/>Mailbox<span class="alert badge alert_red">5</span></a>
                <ul class="drawer">
                    <li><a href="#">Inbox</a></li>
                    <li><a href="#">Sent Items</a></li>
                    <li><a href="#">Drafts<span class="alert badge alert_grey">2</span></a></li>
                    <li><a href="#">Trash<span class="alert badge alert_grey">3</span></a></li>
                </ul>
            </li>
            <li><a href="#"><img src="images/icons/small/grey/documents.png"/>Documents<span class="alert badge alert_black">2</span></a>
                <ul class="drawer">
                    <li><a href="#">Create New</a></li>
                    <li><a href="#">View All</a></li>
                    <li><a href="#">Upload/Download<span class="alert badge alert_grey">2</span></a></li>
                </ul>
            </li>
            <li><a href="#"><img src="images/icons/small/grey/users.png"/>Members</a>
                <ul class="drawer">
                    <li><a href="#">Add New</a></li>
                    <li><a href="#">Edit/Delete</a></li>
                    <li><a href="#">Search Profiles</a></li>
                </ul>
            </li>
            <li><a href="http://www.tricycle.ie"><img src="images/icons/small/grey/graph.png"/>Statistics</a></li>
            <li><a href="#"><img src="images/icons/small/grey/cog_2.png"/>Settings</a>
                <ul class="drawer">
                    <li><a href="#">Account</a></li>
                    <li><a href="#">System</a></li>
                </ul>
            </li>
        </ul>
        <form>
            <div id="search_side" class="dark_box"><input class="" type="text" value="Search Adminica..." onclick="value=''"></div>
        </form>
        <ul id="side_links" class="side_links" style="margin-bottom:0;">
            <li><a href="http://goo.gl/UjRRe">Online Documentation</a>
            <li><a href="support.tricyclelabs.com">Expert Forum</a></li>
            <li><a href="#">Product Wiki</a></li>
            <li><a href="#">Latest Company News</a></li>
        </ul>
    </div><!-- #sidebar -->
    <div id="main_container" class="main_container container_16 clearfix">
        <div id="nav_top" class="clearfix round_top">
            <ul class="clearfix">
                <li><a href="index.php"><img src="images/icons/small/grey/laptop.png"/><span class="display_none">Home</span></a></li>

                <li><a href="#"><img src="images/icons/small/grey/frames.png"/><span>Layout</span></a>
                    <ul>
                        <li><a href="layout.php"><span>16 Grid - 960.gs</span></a></li>
                        <li><a href="text.php"><span>Text & Typography</span></a></li>
                        <li><a class="hide_mobile" href="#"><span>Layout Width</span></a>
                            <ul class="drawer">						
                                <li><a href="styles/theme/switcher1.php?style=layout_fixed.css"><span>Fixed</span></a></li>
                                <li><a href="styles/theme/switcher1.php?style=switcher.css"><span>Fluid</span></a></li>
                            </ul>
                        </li>
                        <li><a class="hide_mobile" href="#"><span>Layout Position</span></a>
                            <ul class="drawer">
                                <li><a href="styles/theme/switcher2.php?style=switcher.css"><span>Side</span></a></li>
                                <li><a href="styles/theme/switcher2.php?style=header_top.css"><span>Top</span></a></li>
                                <li><a href="styles/theme/switcher2.php?style=header_slideout.css"><span>Slide</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="#"><img src="images/icons/small/grey/coverflow.png"/><span>Boxes</span></a>
                    <ul>
                        <li><a href="tabs.php"><span>Tab Boxes</span></a></li>
                        <li><a href="accordions.php"><span>Accordions Boxes</span></a></li>
                        <li><a href="wizard.php"><span>Step by Step Wizard</span></a></li>
                        <li><a href="code.php"><span>Code View</span></a></li>
                    </ul>
                </li>	
                <li><a href="#"><img src="images/icons/small/grey/create_write.png"/><span>Forms</span><span class="alert badge alert_red">new</span></a>
                    <ul>
                        <li><a href="forms.php"><span>Input Fields</span></a></li>
                        <li><a href="buttons.php"><span>Buttons</span></a></li>				
                        <li><a href="#"><span>More components</span></a>
                            <ul class="drawer">
                                <li><a href="validation.php"><span>Validation</span></a></li>
                                <li><a href="editor.php"><span>WYSIWYG Editor</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </li>		
                <li><a href="gallery.php"><img src="images/icons/small/grey/images.png"/><span>Gallery</span></a></li>			
                <li><a href="#"><img src="images/icons/small/grey/blocks_images.png"/><span>Tables</span></a>
                    <ul>
                        <li><a href="tables.php"><span>DataTables</span></a></li>
                        <li><a href="tables_static.php"><span>Regular Tables</span><span class="alert badge alert_black">new</span></a></li>
                    </ul>			
                </li>
                <li><a href="#"><img src="images/icons/small/grey/file_cabinet.png"/><span>Org</span></a>
                    <ul>
                        <li><a href="files.php"><img src="images/icons/small/grey/folder.png"/><span>Files</span></a></li>
                        <li><a href="contacts.php"><img src="images/icons/small/grey/users.png"/><span>Contacts</span></a></li>
                    </ul>
                </li>
                <li><a href="calendar.php"><img src="images/icons/small/grey/month_calendar.png"/><span>Cal</span><span class="alert badge alert_blue">new</span></a></li>
                <li><a href="charts.php"><img src="images/icons/small/grey/graph.png"/><span>Charts</span><span class="alert badge alert_green">new</span></a></li>
                <li><a href="#"><img src="images/icons/small/grey/locked_2.png"/><span class="display_none">Login</span></a>
                    <ul class="dropdown_right">
                        <li><a href="login_regular.php" class="dialog_button" data-dialog="logout"><span>Regular Login</span></a></li>
                        <li><a href="login.php" class="dialog_button" data-dialog="logout"><span>Slide Login</span></a></li>
                    </ul>
                </li>
            </ul>
            <div class="display_none">						
                <div id="logout" class="dialog_content narrow" title="Logout">
                    <div class="block">
                        <div class="section">
                            <h1>Thank you</h1>
                            <div class="dashed_line"></div>	
                            <p>We will now log you out of Adminica in a 10 seconds...</p><p></p>
                        </div>
                        <div class="button_bar clearfix">
                            <button class="dark blue no_margin_bottom link_button" data-link="login.php">
                                <div class="ui-icon ui-icon-check"></div>
                                <span>Ok</span>
                            </button>
                            <button class="light send_right close_dialog">
                                <div class="ui-icon ui-icon-closethick"></div>
                                <span>Cancel</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div> 		


            <script type="text/javascript">
                var currentPage = 3 - 1; // This is only used in php version to tell the nav what the current page is
                $('#nav_top > ul > li').eq(currentPage).addClass("current");
                $('#nav_top > ul > li').addClass("icon_only").children("a").children("span:visible").parent().parent().removeClass("icon_only");
            </script>



            <div id="mobile_nav">
                <div class="main"></div>
                <div class="side"></div>
            </div>

        </div><!-- #nav_top -->
        <div class="flat_area grid_16">
            <h2>Tab Controls</h2>
            <p>Like accordions, tabs are a great way to<strong> present alot of content/data without overwhelming the user</strong>. <strong>Adminica</strong> has two types: a regular <strong>horizontal tab</strong> layout and a <strong>vertical tab</strong> layout. </p>
            <p><strong>Note: </strong>Like nearly all Adminica layout objects, the tab boxes width can be controlled using the <a target="_blank" href="http://www.960.gs">960.gs Grid System</a>. In this example, both tab boxes have a <strong>class="grid_8".</strong></p>
        </div>

        <div class="box grid_16 tabs">
            <ul class="tab_header clearfix">
                <li><a href="#tabs-1">Quicklinks</a></li>
                <li><a href="#tabs-2">Content</a></li>
            </ul>
            <a href="#" class="grabber"></a>
            <a href="#" class="toggle"></a>
            <div class="toggle_container">
                <div id="tabs-1" class="block">
                    <ul class="flat_large">
                        <li>Aenean tempor ullamcorper</li>
                        <li>Rutrum commodo, vehicula tempus</li>
                        <li><a href="#">Curabitur nec arcu</a></li>
                        <li>Donec sollicitudin mi sit amet mauris</li>
                    </ul>
                </div>
                <div id="tabs-2" class="block">
                    <div class="section">
                        <h1>Primary Heading</h1>
                        <p>Lorem Ipsum is simply dummy text of the <a href="#" title="This is a tooltip">printing industry</a>. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 
                        <h2>Secondary Heading</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 					</div>
                </div>
            </div>
        </div>
        <div class="box grid_16 side_tabs tabs">
            <div class="side_holder">
                <ul class="tab_sider clearfix">
                    <li><a href="#tabs-a">Daily Stats</a></li>
                    <li><a href="#tabs-b">Content</a></li>
                    <li><a href="#tabs-c">Tab 3</a></li>
                    <li><a href="#tabs-d">Tab 4</a></li>			
                    <li><a href="#tabs-e">Tab 5</a></li>	
                </ul>
            </div>
            <div id="tabs-a" class="block">
                <ul class="flat_large">
                    <li><span>341</span>Items</li>
                    <li><span>892</span>Posts</li>
                    <li><span>12,062</span>Comments</li>
                    <li><span>90,568</span>Members</li>
                    <li><span>103,451</span>Unique Visitors</li>
                    <li><span>983,876</span>Hits</li>
                    <li><span>7,543,948</span>Monthly Hits</li>
                </ul>
            </div>
            <div id="tabs-b" class="block">
                <div class="section">
                    <h1>Primary Heading</h1>
                    <p>Lorem Ipsum is simply dummy text of the <a href="#" title="This is a tooltip">printing industry</a>. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 

                    <h2>Secondary Heading</h2>
                    <p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p> 
                </div>
            </div>
            <div id="tabs-c" class="block">
                <div class="section">
                    <p>Content goes here.</p> 
                </div>
            </div>
            <div id="tabs-d" class="block">
                <div class="section">
                    <p>Content goes here.</p>
                </div>
            </div>
            <div id="tabs-e" class="block">
                <div class="section">
                    <p>Content goes here.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="loading_overlay">
    <div class="loading_message round_bottom">
        <img src="images/loading.gif" alt="loading" />
    </div>
</div>


<div id="template_options" class="clearfix">
    <h3><img src="images/adminica.png" alt="Adminica"></h3>
    <div class="layout_size round_all">
        <label>Layout:</label>
        <a href="styles/theme/switcher1.php?style=switcher.css">Fluid</a><span>|</span>
        <a href="styles/theme/switcher1.php?style=layout_fixed.css">Fixed</a><span>
    </div>
    <div class="layout_position round_all">
        <label class="display_none">Header: </label>
        <a href="styles/theme/switcher2.php?style=switcher.css">Side</a><span>|</span>
        <a href="styles/theme/switcher2.php?style=header_top.css">Top</a><span>|</span>
        <a href="styles/theme/switcher2.php?style=header_slideout.css">Slide</a>
    </div>
    <div class="layout_position round_all">
        <label>Theme: </label>
        <a href="styles/theme/switcher.php?style=multiple&switcher5.php=switcher.css&switcher4.php=switcher.css">Dark</a><span>|</span>
        <a href="styles/theme/switcher.php?style=multiple&switcher5.php=theme_light.css&switcher4.php=switcher.css">Light</a>
    </div>
    <div class="theme_colour round_all">
        <label class="display_none">Colour:</label>
        <a class="black" href="styles/theme/switcher3.php?style=switcher.css"><span>Black</span></a>
        <a class="blue" href="styles/theme/switcher3.php?style=theme_blue.css"><span>Blue</span></a>
        <a class="navy" href="styles/theme/switcher3.php?style=theme_navy.css"><span>Navy</span></a>
        <a class="red" href="styles/theme/switcher3.php?style=theme_red.css"><span>Red</span></a>
        <a class="green" href="styles/theme/switcher3.php?style=theme_green.css"><span>Green</span></a>
        <a class="magenta" href="styles/theme/switcher3.php?style=theme_magenta.css"><span>Magenta</span></a>
        <a class="orange" href="styles/theme/switcher3.php?style=theme_brown.css"><span>Brown</span></a>
    </div>
    <div class="theme_background round_all" id="bg_dark">
        <label>BGs:</label>
        <a href="styles/theme/switcher4.php?style=switcher.css">Boxes</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_punched.css">Punched</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_honeycomb.css">Honeycomb</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_wood.css">Wood</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_dark_wood.css">Timber</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_noise.css">Noise</a>
    </div>
    <div class="theme_background round_all" id="bg_light">
        <label>BGs:</label>
        <a href="styles/theme/switcher4.php?style=switcher.css">Silver</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_white_wood.css">Wood</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_squares.css">Squares</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_noise_zero.css">Noise</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_stripes.css">Stripes</a>
    </div>
</div>

</body>
</html>