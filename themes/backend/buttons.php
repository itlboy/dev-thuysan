<!doctype html public "✰">
<!--[if lt IE 7]> <html lang="en-us" class="no-js ie6"> <![endif]-->
<!--[if IE 7]>    <html lang="en-us" class="no-js ie7"> <![endif]-->
<!--[if IE 8]>    <html lang="en-us" class="no-js ie8"> <![endif]-->
<!--[if IE 9]>    <html lang="en-us" class="no-js ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-us" class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">

<title>Adminica | The Professional Admin Theme</title>

<!-- iPhone, iPad and Android specific settings -->	

<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1;">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
<link rel="apple-touch-icon" href="images/iOS_icon.png">
<link rel="apple-touch-startup-image" href="images/iOS_startup.png">

<!-- Styles -->

<link rel="stylesheet" type="text/css" href="styles/reset.css">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,700">

<link rel="stylesheet" type="text/css" href="scripts/fancybox/jquery.fancybox-1.3.4.css">
<link rel="stylesheet" type="text/css" href="scripts/tinyeditor/style.css">
<link rel="stylesheet" type="text/css" href="scripts/slidernav/slidernav.css">
<link rel="stylesheet" type="text/css" href="scripts/syntax_highlighter/styles/shCore.css">
<link rel="stylesheet" type="text/css" href="scripts/syntax_highlighter/styles/shThemeDefault.css">
<link rel="stylesheet" type="text/css" href="scripts/uitotop/css/ui.totop.css">
<link rel="stylesheet" type="text/css" href="scripts/fullcalendar/fullcalendar.css">
<link rel="stylesheet" type="text/css" href="scripts/isotope/isotope.css">
<link rel="stylesheet" type="text/css" href="scripts/elfinder/css/elfinder.css">

<link rel="stylesheet" type="text/css" href="scripts/tiptip/tipTip.css">
<link rel="stylesheet" type="text/css" href="scripts/uniform/css/uniform.aristo.css">
<link rel="stylesheet" type="text/css" href="scripts/multiselect/css/ui.multiselect.css">
<link rel="stylesheet" type="text/css" href="scripts/selectbox/jquery.selectBox.css">
<link rel="stylesheet" type="text/css" href="scripts/colorpicker/css/colorpicker.css">
<link rel="stylesheet" type="text/css" href="scripts/uistars/jquery.ui.stars.min.css">

<link rel="stylesheet" type="text/css" href="scripts/themeroller/Aristo.css">

<link rel="stylesheet" type="text/css" href="styles/text.css">
<link rel="stylesheet" type="text/css" href="styles/grid.css">
<link rel="stylesheet" type="text/css" href="styles/main.css">
<link rel="stylesheet" type="text/css" href="styles/theme/theme_base.css">

<!-- Style Switcher  

The following stylesheet links are used by the styleswitcher to allow for dynamically changing how Adminica looks and acts.
Styleswitcher documentation: http://style-switcher.webfactoryltd.com/documentation/

switcher1.php : layout - fluid by default.								(eg. styles/theme/switcher1.php?default=layout_fixed.css)
switcher2.php : header and sidebar positioning - sidebar by default.	(eg. styles/theme/switcher1.php?default=header_top.css)
switcher3.php : colour skin - black/grey by default.					(eg. styles/theme/switcher1.php?default=theme_red.css)
switcher4.php : background image - dark boxes by default.				(eg. styles/theme/switcher1.php?default=bg_honeycomb.css)
switcher5.php : controls the theme - dark by default.					(eg. styles/theme/switcher1.php?default=theme_light.css)

-->

<link rel="stylesheet" type="text/css" href="styles/theme/switcher.css" media="screen">
<link rel="stylesheet" type="text/css" href="styles/theme/layout_fixed.css" media="screen" > 
<!--		<link rel="stylesheet" type="text/css" href="styles/theme/switcher2.php?default=switcher.css" media="screen" >-->
<link rel="stylesheet" type="text/css" href="styles/theme/theme_blue.css" media="screen" >
<link rel="stylesheet" type="text/css" href="styles/theme/bg_wood.css" media="screen" >
<!--		<link rel="stylesheet" type="text/css" href="styles/theme/switcher5.php?default=switcher.css" media="screen" >-->

<link rel="stylesheet" type="text/css" href="styles/colours.css">
<link rel="stylesheet" type="text/css" href="styles/ie.css">

<!-- Scripts -->

<!-- Load JQuery -->		
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<!-- Load JQuery UI -->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/jquery-ui.min.js" type="text/javascript"></script>

<!-- Global -->
<script src="scripts/touchPunch/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
<script src="scripts/uitotop/js/jquery.ui.totop.js" type="text/javascript"></script>

<!-- Forms -->
<script src="scripts/uniform/jquery.uniform.min.js"></script>
<script src="scripts/autogrow/jquery.autogrowtextarea.js"></script>
<script src="scripts/multiselect/js/ui.multiselect.js"></script>
<script src="scripts/selectbox/jquery.selectBox.min.js"></script>
<script src="scripts/timepicker/jquery.timepicker.js"></script>
<script src="scripts/colorpicker/js/colorpicker.js"></script>
<script src="scripts/uistars/jquery.ui.stars.min.js"></script>
<script src="scripts/tiptip/jquery.tipTip.minified.js"></script>
<script src="scripts/validation/jquery.validate.min.js" type="text/javascript"></script>		

<!-- Configuration Script -->
<script type="text/javascript" src="scripts/adminica/adminica_ui.js"></script>
<script type="text/javascript" src="scripts/adminica/adminica_forms.js"></script>
<script type="text/javascript" src="scripts/adminica/adminica_mobile.js"></script>

<!-- Live Load (remove after development) -->
<script>//document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>

</head>
<body>
<div id="wrapper">
    <div id="topbar" class="clearfix">

        <a href="dashboard_sorter.php" class="logo"><span>Adminica</span></a>

        <div class="user_box dark_box clearfix">
            <img src="images/profile.jpg" width="55" alt="Profile Pic" />
            <h2>Administrator</h2>
            <h3><a class="text_shadow" href="#">John Smith</a></h3>
            <ul>
                <li><a href="#">profile</a><span class="divider">|</span></li>
                <li><a href="#">settings</a><span class="divider">|</span></li>
                <li><a href="login.php">logout</a></li>
            </ul>
        </div><!-- #user_box -->	
    </div><!-- #topbar -->		
    <div id="sidebar">
        <div class="cog">+</div>

        <a href="index.php" class="logo"><span>Adminica</span></a>

        <div class="user_box dark_box clearfix">
            <img src="images/profile.jpg" width="55" alt="Profile Pic" />
            <h2>Administrator</h2>
            <h3><a href="#">John Smith</a></h3>
            <ul>
                <li><a href="#">settings</a><span class="divider">|</span></li>
                <li><a href="login.php">logout</a></li>
            </ul>
        </div><!-- #user_box -->

        <ul class="side_accordion"> <!-- add class 'open_multiple' to change to from accordion to toggles -->
            <li><a href="#"><img src="images/icons/small/grey/home.png"/>Home</a>
                <ul class="drawer">
                    <li><a href="#">Activity</a></li>
                    <li><a href="#">Events</a></li>
                    <li><a href="#">Tasks</a></li>
                </ul>
            </li>
            <li><a href="#"><img src="images/icons/small/grey/mail.png"/>Mailbox<span class="alert badge alert_red">5</span></a>
                <ul class="drawer">
                    <li><a href="#">Inbox</a></li>
                    <li><a href="#">Sent Items</a></li>
                    <li><a href="#">Drafts<span class="alert badge alert_grey">2</span></a></li>
                    <li><a href="#">Trash<span class="alert badge alert_grey">3</span></a></li>
                </ul>
            </li>
            <li><a href="#"><img src="images/icons/small/grey/documents.png"/>Documents<span class="alert badge alert_black">2</span></a>
                <ul class="drawer">
                    <li><a href="#">Create New</a></li>
                    <li><a href="#">View All</a></li>
                    <li><a href="#">Upload/Download<span class="alert badge alert_grey">2</span></a></li>
                </ul>
            </li>
            <li><a href="#"><img src="images/icons/small/grey/users.png"/>Members</a>
                <ul class="drawer">
                    <li><a href="#">Add New</a></li>
                    <li><a href="#">Edit/Delete</a></li>
                    <li><a href="#">Search Profiles</a></li>
                </ul>
            </li>
            <li><a href="http://www.tricycle.ie"><img src="images/icons/small/grey/graph.png"/>Statistics</a></li>
            <li><a href="#"><img src="images/icons/small/grey/cog_2.png"/>Settings</a>
                <ul class="drawer">
                    <li><a href="#">Account</a></li>
                    <li><a href="#">System</a></li>
                </ul>
            </li>
        </ul>
        <form>
            <div id="search_side" class="dark_box"><input class="" type="text" value="Search Adminica..." onclick="value=''"></div>
        </form>
        <ul id="side_links" class="side_links" style="margin-bottom:0;">
            <li><a href="http://goo.gl/UjRRe">Online Documentation</a>
            <li><a href="support.tricyclelabs.com">Expert Forum</a></li>
            <li><a href="#">Product Wiki</a></li>
            <li><a href="#">Latest Company News</a></li>
        </ul>
    </div><!-- #sidebar -->
    <div id="main_container" class="main_container container_16 clearfix">
        <div id="nav_top" class="clearfix round_top">
            <ul class="clearfix">
                <li><a href="index.php"><img src="images/icons/small/grey/laptop.png"/><span class="display_none">Home</span></a></li>

                <li><a href="#"><img src="images/icons/small/grey/frames.png"/><span>Layout</span></a>
                    <ul>
                        <li><a href="layout.php"><span>16 Grid - 960.gs</span></a></li>
                        <li><a href="text.php"><span>Text & Typography</span></a></li>
                        <li><a class="hide_mobile" href="#"><span>Layout Width</span></a>
                            <ul class="drawer">						
                                <li><a href="styles/theme/switcher1.php?style=layout_fixed.css"><span>Fixed</span></a></li>
                                <li><a href="styles/theme/switcher1.php?style=switcher.css"><span>Fluid</span></a></li>
                            </ul>
                        </li>
                        <li><a class="hide_mobile" href="#"><span>Layout Position</span></a>
                            <ul class="drawer">
                                <li><a href="styles/theme/switcher2.php?style=switcher.css"><span>Side</span></a></li>
                                <li><a href="styles/theme/switcher2.php?style=header_top.css"><span>Top</span></a></li>
                                <li><a href="styles/theme/switcher2.php?style=header_slideout.css"><span>Slide</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="#"><img src="images/icons/small/grey/coverflow.png"/><span>Boxes</span></a>
                    <ul>
                        <li><a href="tabs.php"><span>Tab Boxes</span></a></li>
                        <li><a href="accordions.php"><span>Accordions Boxes</span></a></li>
                        <li><a href="wizard.php"><span>Step by Step Wizard</span></a></li>
                        <li><a href="code.php"><span>Code View</span></a></li>
                    </ul>
                </li>	
                <li><a href="#"><img src="images/icons/small/grey/create_write.png"/><span>Forms</span><span class="alert badge alert_red">new</span></a>
                    <ul>
                        <li><a href="forms.php"><span>Input Fields</span></a></li>
                        <li><a href="buttons.php"><span>Buttons</span></a></li>				
                        <li><a href="#"><span>More components</span></a>
                            <ul class="drawer">
                                <li><a href="validation.php"><span>Validation</span></a></li>
                                <li><a href="editor.php"><span>WYSIWYG Editor</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </li>		
                <li><a href="gallery.php"><img src="images/icons/small/grey/images.png"/><span>Gallery</span></a></li>			
                <li><a href="#"><img src="images/icons/small/grey/blocks_images.png"/><span>Tables</span></a>
                    <ul>
                        <li><a href="tables.php"><span>DataTables</span></a></li>
                        <li><a href="tables_static.php"><span>Regular Tables</span><span class="alert badge alert_black">new</span></a></li>
                    </ul>			
                </li>
                <li><a href="#"><img src="images/icons/small/grey/file_cabinet.png"/><span>Org</span></a>
                    <ul>
                        <li><a href="files.php"><img src="images/icons/small/grey/folder.png"/><span>Files</span></a></li>
                        <li><a href="contacts.php"><img src="images/icons/small/grey/users.png"/><span>Contacts</span></a></li>
                    </ul>
                </li>
                <li><a href="calendar.php"><img src="images/icons/small/grey/month_calendar.png"/><span>Cal</span><span class="alert badge alert_blue">new</span></a></li>
                <li><a href="charts.php"><img src="images/icons/small/grey/graph.png"/><span>Charts</span><span class="alert badge alert_green">new</span></a></li>
                <li><a href="#"><img src="images/icons/small/grey/locked_2.png"/><span class="display_none">Login</span></a>
                    <ul class="dropdown_right">
                        <li><a href="login_regular.php" class="dialog_button" data-dialog="logout"><span>Regular Login</span></a></li>
                        <li><a href="login.php" class="dialog_button" data-dialog="logout"><span>Slide Login</span></a></li>
                    </ul>
                </li>
            </ul>
            <div class="display_none">						
                <div id="logout" class="dialog_content narrow" title="Logout">
                    <div class="block">
                        <div class="section">
                            <h1>Thank you</h1>
                            <div class="dashed_line"></div>	
                            <p>We will now log you out of Adminica in a 10 seconds...</p><p></p>
                        </div>
                        <div class="button_bar clearfix">
                            <button class="dark blue no_margin_bottom link_button" data-link="login.php">
                                <div class="ui-icon ui-icon-check"></div>
                                <span>Ok</span>
                            </button>
                            <button class="light send_right close_dialog">
                                <div class="ui-icon ui-icon-closethick"></div>
                                <span>Cancel</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div> 		


            <script type="text/javascript">
                var currentPage = 4 - 1; // This is only used in php version to tell the nav what the current page is
                $('#nav_top > ul > li').eq(currentPage).addClass("current");
                $('#nav_top > ul > li').addClass("icon_only").children("a").children("span:visible").parent().parent().removeClass("icon_only");
            </script>



            <div id="mobile_nav">
                <div class="main"></div>
                <div class="side"></div>
            </div>

        </div><!-- #nav_top -->
        <div class="flat_area grid_16">
            <h2>Button/Icon Collection</h2>
            <p>Adminica provides you with a <strong>huge selection</strong> of icons and buttons. All have a consistant style and should cater for most circumstances. If you need another icon please make a request and we will do our best to add it to the collection. Flick through the tabs to see them all.</p>

            <p><strong>Note: </strong>A dark version of each icon is also included for use on a lighter coloured buttons. You simply change the folder path from white to black... as simple as that!</p>
        </div>

        <div class="box grid_16  tabs" id="button_display">
            <ul id="touch_sort" class="tab_header clearfix">
                <li><a href="#tabs-0">Action Buttons</a></li>
                <li><a href="#tabs-1">Icons A-F</a></li>
                <li><a href="#tabs-2">Icons G-P</a></li>
                <li><a href="#tabs-3">Icons Q-Z</a></li>
            </ul>
            <a href="#" class="grabber">&nbsp;</a>
            <a href="#" class="toggle">&nbsp;</a>
            <div class="toggle_container">
                <div id="tabs-0" class="block">
                    <div class="section clearfix">

                        <h1>Action Buttons</h1>

                        <button>
                            <div class="ui-icon ui-icon-carat-1-n"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-carat-1-ne"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-carat-1-e"></div>
                        </button>

                        <button>
                            <div class="ui-icon ui-icon-carat-1-se"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-carat-1-s"></div>
                        </button>

                        <button>
                            <div class="ui-icon ui-icon-carat-1-sw"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-carat-1-w"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-carat-1-nw"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-carat-2-n-s"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-carat-2-e-w"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-triangle-1-n"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-triangle-1-ne"></div>
                        </button>

                        <button>
                            <div class="ui-icon ui-icon-triangle-1-e"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-triangle-1-se"></div>
                        </button>

                        <button>
                            <div class="ui-icon ui-icon-triangle-1-s"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-triangle-1-sw"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-triangle-1-w"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-triangle-1-nw"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-triangle-2-n-s"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-triangle-2-e-w"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrow-1-n"></div>
                        </button>

                        <button>
                            <div class="ui-icon ui-icon-arrow-1-ne"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrow-1-e"></div>
                        </button>

                        <button>
                            <div class="ui-icon ui-icon-arrow-1-se"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrow-1-s"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrow-1-sw"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrow-1-w"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrow-1-nw"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrow-2-n-s"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrow-2-ne-sw"></div>
                        </button>

                        <button>
                            <div class="ui-icon ui-icon-arrow-2-e-w"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrow-2-se-nw"></div>
                        </button>

                        <button>
                            <div class="ui-icon ui-icon-arrowstop-1-n"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowstop-1-e"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowstop-1-s"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowstop-1-w"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowthick-1-n"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowthick-1-ne"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowthick-1-e"></div>
                        </button>

                        <button>
                            <div class="ui-icon ui-icon-arrowthick-1-se"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowthick-1-s"></div>
                        </button>

                        <button>
                            <div class="ui-icon ui-icon-arrowthick-1-sw"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowthick-1-w"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowthick-1-nw"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowthick-2-n-s"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowthick-2-ne-sw"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowthick-2-e-w"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowthick-2-se-nw"></div>
                        </button>

                        <button>
                            <div class="ui-icon ui-icon-arrowthickstop-1-n"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowthickstop-1-e"></div>
                        </button>

                        <button>
                            <div class="ui-icon ui-icon-arrowthickstop-1-s"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowthickstop-1-w"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowreturnthick-1-w"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowreturnthick-1-n"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowreturnthick-1-e"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowreturnthick-1-s"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowreturn-1-w"></div>
                        </button>

                        <button>
                            <div class="ui-icon ui-icon-arrowreturn-1-n"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowreturn-1-e"></div>
                        </button>

                        <button>
                            <div class="ui-icon ui-icon-arrowreturn-1-s"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowrefresh-1-w"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowrefresh-1-n"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowrefresh-1-e"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrowrefresh-1-s"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrow-4"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-arrow-4-diag"></div>
                        </button>

                        <button>
                            <div class="ui-icon ui-icon-extlink"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-newwin"></div>
                        </button>

                        <button>
                            <div class="ui-icon ui-icon-refresh"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-shuffle"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-transfer-e-w"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-transferthick-e-w"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-folder-collapsed"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-folder-open"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-document"></div>
                        </button>

                        <button>
                            <div class="ui-icon ui-icon-document-b"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-note"></div>
                        </button>

                        <button>
                            <div class="ui-icon ui-icon-mail-closed"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-mail-open"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-suitcase"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-comment"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-person"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-print"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-trash"></div>
                        </button>

                        <button>
                            <div class="ui-icon ui-icon-locked"></div>
                        </button>
                        <button>
                            <div class="ui-icon ui-icon-unlocked"></div>
                        </button>

                        <button>
                            <div class="ui-icon ui-icon-bookmark"></div>
                        </button>							

                        <button class="light">
                            <div class="ui-icon ui-icon-tag"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-home"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-flag"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-calculator"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-cart"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-pencil"></div>
                        </button>

                        <button class="light">
                            <div class="ui-icon ui-icon-clock"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-disk"></div>
                        </button>

                        <button class="light">
                            <div class="ui-icon ui-icon-calendar"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-zoomin"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-zoomout"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-search"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-wrench"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-gear"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-heart"></div>
                        </button>

                        <button class="light">
                            <div class="ui-icon ui-icon-star"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-link"></div>
                        </button>

                        <button class="light">
                            <div class="ui-icon ui-icon-cancel"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-plus"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-plusthick"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-minus"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-minusthick"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-close"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-closethick"></div>
                        </button>

                        <button class="light">
                            <div class="ui-icon ui-icon-key"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-lightbulb"></div>
                        </button>

                        <button class="light">
                            <div class="ui-icon ui-icon-scissors"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-clipboard"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-copy"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-contact"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-image"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-video"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-script"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-alert"></div>
                        </button>

                        <button class="light">
                            <div class="ui-icon ui-icon-info"></div>
                        </button>

                        <button class="light">
                            <div class="ui-icon ui-icon-notice"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-help"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-check"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-bullet"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-radio-off"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-radio-on"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-pin-w"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-pin-s"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-play"></div>
                        </button>

                        <button class="light">
                            <div class="ui-icon ui-icon-pause"></div>
                        </button>

                        <button class="light">
                            <div class="ui-icon ui-icon-seek-next"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-seek-prev"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-seek-end"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-seek-first"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-stop"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-eject"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-volume-off"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-volume-on"></div>
                        </button>

                        <button class="light">
                            <div class="ui-icon ui-icon-power"></div>
                        </button>

                        <button class="light">
                            <div class="ui-icon ui-icon-signal-diag"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-signal"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-battery-0"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-battery-1"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-battery-2"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-battery-3"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-circle-plus"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-circle-minus"></div>
                        </button>

                        <button class="light">
                            <div class="ui-icon ui-icon-circle-close"></div>
                        </button>

                        <button class="light">
                            <div class="ui-icon ui-icon-circle-triangle-e"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-circle-triangle-s"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-circle-triangle-w"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-circle-triangle-n"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-circle-arrow-e"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-circle-arrow-s"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-circle-arrow-w"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-circle-arrow-n"></div>
                        </button>

                        <button class="light">
                            <div class="ui-icon ui-icon-circle-zoomin"></div>
                        </button>

                        <button class="light">
                            <div class="ui-icon ui-icon-circle-zoomout"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-circle-check"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-circlesmall-plus"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-circlesmall-minus"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-circlesmall-close"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-squaresmall-plus"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-squaresmall-minus"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-squaresmall-close"></div>
                        </button>

                        <button class="light">
                            <div class="ui-icon ui-icon-grip-dotted-vertical"></div>
                        </button>

                        <button class="light">
                            <div class="ui-icon ui-icon-grip-dotted-horizontal"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-grip-solid-vertical"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-grip-solid-horizontal"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-gripsmall-diagonal-se"></div>
                        </button>
                        <button class="light">
                            <div class="ui-icon ui-icon-grip-diagonal-se"></div>
                        </button>	
                    </div>

                </div>
                <div id="tabs-1" class="block">
                    <div class="section clearfix">

                        <button>
                            <img src="images/icons/small/white/abacus.png" width="24" height="24" title="Abacus">
                            <span>Button text</span>
                        </button>

                        <button>
                            <img src="images/icons/small/white/acces_denied_sign.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/address_book.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/admin_user_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/admin_user.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/airplane.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/alarm_bell_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/alarm_bell.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/alarm_clock.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/alert_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/alert.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/android.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/apartment_building.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/archive.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/bag.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/balloons.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/bandaid.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/battery_almost_empty.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/battery_almost_full.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/battery_empty.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/battery_full.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/battery.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/bended_arrow_down.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/bended_arrow_left.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/bended_arrow_right.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/bended_arrow_up.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/big_brush.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/blackberry.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/blocks_images.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/blu_ray.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/bluetooth_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/bluetooth.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/book_Large.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/book.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/books.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/box_Incoming.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/box_outgoing.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/Bulls_eye.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/calculator.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/camera_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/camera.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/car.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/cash_register.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/cassette.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/cat.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/cd.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/chair.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/chart_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/chart_3.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/chart_4.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/chart_5.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/chart_6.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/chart_7.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/chart_8.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/chart.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/chemical.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/chrome.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/clipboard.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/clock.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/cloud_download.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/cloud_upload.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/cloud.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/cog_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/cog_3.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/cog_4.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/cog.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/companies.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/computer_imac.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/coverflow.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/create_write.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/cup.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/cut_scissors.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/day_Calendar.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/delicious.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/digg_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/digg.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/document.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/documents.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/download_to_computer.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/download.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/dress.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/dribbble_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/dribbble.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/dropbox.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/drupal.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/dVD.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/electricity_input.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/electricity_plug.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/excel_document.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/excel_documents.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/exit.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/expose.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/expression_engine.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/eyedropper.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/facebook_like.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/facebook.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/fax.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/female_contour.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/female_symbol.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/file_cabinet.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/film_camera.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/film_strip_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/film_strip.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/finish_flag.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/firefox.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/flag_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/flag.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/folder.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/footprints.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/fountain_Pen.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/frames.png">
                        </button>				
                    </div>

                </div>
                <div id="tabs-2" class="block">
                    <div class="section clearfix">

                        <button>
                            <img src="images/icons/small/white/globe_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/globe.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/go_back_from_full_screen.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/go_back_from_screen.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/go_full_screen.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/google_buzz.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/google_maps.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/graph.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/hd_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/hd_3.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/hd.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/headphones.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/help.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/home_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/home.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/ice_cream_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/ice_cream.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/ichat.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/image_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/image.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/image2_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/images.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/inbox.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/info_about.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/iPad.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/iphone_3g.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/iphone_4.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/iPod_classic.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/iPod_nano.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/joomla.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/key_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/key.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/ladies_purse.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/lamp.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/laptop.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/lastfm_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/lemonade_stand.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/light_bulb.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/link_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/link.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/linux.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/list_w_image.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/list_w_images.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/list.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/loading_Bar.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/locked_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/locked.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/macos.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/magic_mouse.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/magnifying_glass.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/mail.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/male_contour.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/male_symbol.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/map.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/marker.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/medical_case.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/megaphone.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/microphone.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/mighty_mouse.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/mobile_phone.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/mobypicture.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/money_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/money.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/monitor.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/month_calendar.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/mouse_wires.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/myspace_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/note_book.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/pacman_ghost.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/pacman.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/paint_brush.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/pants.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/paperclip.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/paypal_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/paypal_3.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/paypal.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/pdf_document.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/pdf_documents.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/pencil.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/phone_3.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/phone_hook.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/phone.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/piggy_bank.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/plane_suitcase.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/plixi.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/post_card.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/power.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/powerpoint_document.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/powerpoint_documents.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/presentation.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/preview.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/price_tag.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/price_tags.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/printer.png">
                        </button>
                    </div>
                </div>
                <div id="tabs-3" class="block ">
                    <div class="section clearfix">

                        <button>
                            <img src="images/icons/small/white/radio.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/record.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/recycle_symbol.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/refresh_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/refresh_3.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/refresh_4.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/refresh.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/repeat.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/robot.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/rss.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/ruler_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/ruler.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/running_man.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/safari.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/scan_label_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/sD_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/sd_3.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/sd.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/settings_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/settings.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/shopping_bag.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/shopping_basket_1.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/shopping_basket_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/shopping_cart_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/shopping_cart_3.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/shopping_cart_4.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/shopping_cart.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/shuffle.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/sign_post.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/skype.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/sleeveless_shirt.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/small_brush.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/socks.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/sound.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/speech_bubble_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/speech_bubble.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/speech_bubbles_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/speech_bubbles.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/sport_shirt.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/stop_watch.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/strategy_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/strategy.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/stubleupon.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/suitcase.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/sweater.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/t_shirt.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/table.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/tag.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/tags_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/television.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/timer.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/trashcan_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/trashcan.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/travel_suitcase.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/tree.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/trolly.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/truck.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/tumbler.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/twitter_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/twitter.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/umbrella.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/under_construction.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/unlocked.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/upload.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/user_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/user_comment.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/user.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/users_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/users.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/v_card_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/v_card.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/vault.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/vimeo_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/vimeo.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/walking_man.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/wifi_signal_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/wifi_signal.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/windows.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/winner_podium.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/word_document.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/word_documents.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/wordpress_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/wordpress.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/youtube_2.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/youtube.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/zip_file.png">
                        </button>

                        <button>
                            <img src="images/icons/small/white/zip_files.png">
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="box grid_16">								
            <div class="block">			
                <div class="columns">
                    <div class="col_75">

                        <h2 class="section">Button Sizes</h2>

                        <fieldset class="label_top">
                            <label>Small Buttons</label>
                            <div class="clearfix">

                                <button class="light small">
                                    <span>Small Button</span>
                                </button>						
                                <button class="light  small">
                                    <div class="ui-icon ui-icon-grip-solid-horizontal"></div>
                                </button>								
                                <button class="light small">
                                    <div class="ui-icon ui-icon-grip-solid-horizontal"></div>
                                    <span>Small Button</span>
                                </button>
                                <button class="light  small">
                                    <img src="images/icons/small/grey/file_cabinet.png">
                                </button>										
                                <button class="light small">
                                    <img src="images/icons/small/grey/file_cabinet.png">
                                    <span>Small Button</span>
                                </button>		
                            </div>
                        </fieldset>		

                        <fieldset class="label_top">
                            <label>Regular Buttons</label>
                            <div class="clearfix">										

                                <button class="light">
                                    <span>Regular Button</span>
                                </button>
                                <button class="light ">
                                    <div class="ui-icon ui-icon-grip-solid-horizontal"></div>
                                </button>
                                <button class="light">
                                    <div class="ui-icon ui-icon-grip-solid-horizontal"></div>
                                    <span>Regular Button</span>
                                </button>
                                <button class="light ">
                                    <img src="images/icons/small/grey/file_cabinet.png">
                                </button>										
                                <button class="light">
                                    <img src="images/icons/small/grey/file_cabinet.png">
                                    <span>Regular Button</span>
                                </button>
                            </div>
                        </fieldset>

                        <fieldset class="label_top">
                            <label>Large Buttons</label>
                            <div class="clearfix">

                                <button class="light large">
                                    <span>Large Button</span>
                                </button>
                                <button class="light large ">
                                    <div class="ui-icon ui-icon-grip-diagonal-se"></div>
                                </button>
                                <button class="light large">
                                    <div class="ui-icon ui-icon-grip-diagonal-se"></div>
                                    <span>Large Button</span>
                                </button>
                                <button class="light  large">
                                    <img src="images/icons/small/grey/file_cabinet.png">
                                </button>										
                                <button class="light large">
                                    <img src="images/icons/small/grey/file_cabinet.png">
                                    <span>Large Button</span>
                                </button>
                            </div>

                        </fieldset>
                    </div>
                </div>
                <div class="col_25">
                    <div class="section clearfix" style="padding-bottom:10px;">
                        <p>Here are some other colours:</p>

                        <button class="light full_width">
                            <div class="ui-icon ui-icon-carat-1-n"></div>
                            <span>Light Button</span>
                        </button>

                        <button class="grey full_width">
                            <div class="ui-icon ui-icon-carat-1-n"></div>
                            <span>Grey Button</span>
                        </button>

                        <button class="black full_width">
                            <div class="ui-icon ui-icon-carat-1-n"></div>
                            <span>Black Button</span>
                        </button>

                        <button class="navy full_width">
                            <div class="ui-icon ui-icon-carat-1-n"></div>
                            <span>Navy Button</span>
                        </button>

                        <button class="blue full_width">
                            <div class="ui-icon ui-icon-carat-1-n"></div>
                            <span>Blue Button</span>
                        </button>

                        <button class="red full_width">
                            <div class="ui-icon ui-icon-carat-1-n"></div>
                            <span>Red Button</span>
                        </button>

                        <button class="green full_width">
                            <div class="ui-icon ui-icon-carat-1-n"></div>
                            <span>Green Button</span>
                        </button>

                        <button class="orange full_width">
                            <div class="ui-icon ui-icon-carat-1-n"></div>
                            <span>Orange Button</span>
                        </button>

                    </div>								
                </div>
            </div>
        </div>
    </div>
</div>
<div id="loading_overlay">
    <div class="loading_message round_bottom">
        <img src="images/loading.gif" alt="loading" />
    </div>
</div>


<div id="template_options" class="clearfix">
    <h3><img src="images/adminica.png" alt="Adminica"></h3>
    <div class="layout_size round_all">
        <label>Layout:</label>
        <a href="styles/theme/switcher1.php?style=switcher.css">Fluid</a><span>|</span>
        <a href="styles/theme/switcher1.php?style=layout_fixed.css">Fixed</a><span>
    </div>
    <div class="layout_position round_all">
        <label class="display_none">Header: </label>
        <a href="styles/theme/switcher2.php?style=switcher.css">Side</a><span>|</span>
        <a href="styles/theme/switcher2.php?style=header_top.css">Top</a><span>|</span>
        <a href="styles/theme/switcher2.php?style=header_slideout.css">Slide</a>
    </div>
    <div class="layout_position round_all">
        <label>Theme: </label>
        <a href="styles/theme/switcher.php?style=multiple&switcher5.php=switcher.css&switcher4.php=switcher.css">Dark</a><span>|</span>
        <a href="styles/theme/switcher.php?style=multiple&switcher5.php=theme_light.css&switcher4.php=switcher.css">Light</a>
    </div>
    <div class="theme_colour round_all">
        <label class="display_none">Colour:</label>
        <a class="black" href="styles/theme/switcher3.php?style=switcher.css"><span>Black</span></a>
        <a class="blue" href="styles/theme/switcher3.php?style=theme_blue.css"><span>Blue</span></a>
        <a class="navy" href="styles/theme/switcher3.php?style=theme_navy.css"><span>Navy</span></a>
        <a class="red" href="styles/theme/switcher3.php?style=theme_red.css"><span>Red</span></a>
        <a class="green" href="styles/theme/switcher3.php?style=theme_green.css"><span>Green</span></a>
        <a class="magenta" href="styles/theme/switcher3.php?style=theme_magenta.css"><span>Magenta</span></a>
        <a class="orange" href="styles/theme/switcher3.php?style=theme_brown.css"><span>Brown</span></a>
    </div>
    <div class="theme_background round_all" id="bg_dark">
        <label>BGs:</label>
        <a href="styles/theme/switcher4.php?style=switcher.css">Boxes</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_punched.css">Punched</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_honeycomb.css">Honeycomb</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_wood.css">Wood</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_dark_wood.css">Timber</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_noise.css">Noise</a>
    </div>
    <div class="theme_background round_all" id="bg_light">
        <label>BGs:</label>
        <a href="styles/theme/switcher4.php?style=switcher.css">Silver</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_white_wood.css">Wood</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_squares.css">Squares</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_noise_zero.css">Noise</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_stripes.css">Stripes</a>
    </div>
</div>

</body>
</html>