<html lang="en">
<head>
<meta charset="utf-8">
<title>jQuery File Upload Demo</title>
<meta name="description" content="File Upload widget with multiple file selection, drag&amp;drop support, progress bar and preview images for jQuery. Supports cross-domain, chunked and resumable file uploads. Works with any server-side platform (Google App Engine, PHP, Python, Ruby on Rails, Java, etc.) that supports standard HTML form file uploads.">
<meta name="viewport" content="width=device-width">
<!-- Bootstrap CSS Toolkit styles -->
<!--<link rel="stylesheet" href="http://blueimp.github.com/cdn/css/bootstrap.min.css">-->
<!-- Generic page styles -->
</head>
<body>
    <?php echo $content; ?>

<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
 The Templates plugin is included to render the upload/download listings 
<script src="http://blueimp.github.com/JavaScript-Templates/tmpl.min.js"></script>
 The Load Image plugin is included for the preview images and image resizing functionality 
<script src="http://blueimp.github.com/JavaScript-Load-Image/load-image.min.js"></script>
 The Canvas to Blob plugin is included for image resizing functionality 
<script src="http://blueimp.github.com/JavaScript-Canvas-to-Blob/canvas-to-blob.min.js"></script>
 Bootstrap JS and Bootstrap Image Gallery are not required, but included for the demo 
<script src="http://blueimp.github.com/cdn/js/bootstrap.min.js"></script>
<script src="http://blueimp.github.com/Bootstrap-Image-Gallery/js/bootstrap-image-gallery.min.js"></script>
 The Iframe Transport is required for browsers without support for XHR file uploads 
 The XDomainRequest Transport is included for cross-domain file deletion for IE8+ 
[if gte IE 8]><script src="<?php echo Yii::app()->baseUrl; ?>/js/cors/jquery.xdr-transport.js"></script><![endif]-->
</body> 
</html>
