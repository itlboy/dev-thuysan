<!doctype html public "✰">
<!--[if lt IE 7]> <html lang="en-us" class="no-js ie6"> <![endif]-->
<!--[if IE 7]>    <html lang="en-us" class="no-js ie7"> <![endif]-->
<!--[if IE 8]>    <html lang="en-us" class="no-js ie8"> <![endif]-->
<!--[if IE 9]>    <html lang="en-us" class="no-js ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-us" class="no-js"> <!--<![endif]-->
<head>
<script>
    YII_CSRF_TOKEN = '<?php echo Yii::app()->request->csrfToken; ?>';
</script>
<title>Administrator Letkit v1.0</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!-- iPhone, iPad and Android specific settings -->	

<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1;">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
<link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/<?php echo Yii::app()->theme->baseUrl; ?>/images/iOS_icon.png">
<link rel="apple-touch-startup-image" href="<?php echo Yii::app()->theme->baseUrl; ?>/<?php echo Yii::app()->theme->baseUrl; ?>/images/iOS_startup.png">

<!-- test -->
<!--<link rel="stylesheet" href="http://blueimp.github.com/cdn/css/bootstrap.min.css">-->

<!-- Styles -->

<?php    
    $cs = Yii::app()->getClientScript();
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/reset.css');
//    $cs->registerCssFile('http://fonts.googleapis.com/css?family=Open+Sans:400,700');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/fancybox/jquery.fancybox-1.3.4.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/tinyeditor/style.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/slidernav/slidernav.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/syntax_highlighter/styles/shCore.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/syntax_highlighter/styles/shThemeDefault.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/uitotop/css/ui.totop.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/fullcalendar/fullcalendar.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/isotope/isotope.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/elfinder/css/elfinder.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/tiptip/tipTip.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/uniform/css/uniform.aristo.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/multiselect/css/ui.multiselect.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/selectbox/jquery.selectBox.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/colorpicker/css/colorpicker.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/uistars/jquery.ui.stars.min.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/themeroller/Aristo.css');
    
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/text.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/grid.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/main.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/let.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/theme/theme_base.css');
    
    /* Style Switcher 
    The following stylesheet links are used by the styleswitcher to allow for dynamically changing how Adminica looks and acts.
    Styleswitcher documentation: http://style-switcher.webfactoryltd.com/documentation/

    switcher1.php : layout - fluid by default.								(eg. styles/theme/switcher1.php?default=layout_fixed.css)
    switcher2.php : header and sidebar positioning - sidebar by default.	(eg. styles/theme/switcher1.php?default=header_top.css)
    switcher3.php : colour skin - black/grey by default.					(eg. styles/theme/switcher1.php?default=theme_red.css)
    switcher4.php : background image - dark boxes by default.				(eg. styles/theme/switcher1.php?default=bg_honeycomb.css)
    switcher5.php : controls the theme - dark by default.					(eg. styles/theme/switcher1.php?default=theme_light.css)
    */
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/theme/switcher.css');
//    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/theme/layout_fixed.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/theme/theme_blue.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/theme/theme_blue.css');
    
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/colours.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/ie.css');
    
    $cs->registerCoreScript('jquery');
    $cs->registerCoreScript('jquery.ui');
    
    //https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js
//    $cs->registerScriptFile('https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/jquery-ui.min.js', CClientScript::POS_END);
    
    // Global
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/touchPunch/jquery.ui.touch-punch.min.js', CClientScript::POS_END);
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/uitotop/js/jquery.ui.totop.js', CClientScript::POS_END);
    
    // Forms
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/uniform/jquery.uniform.min.js', CClientScript::POS_END);
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/autogrow/jquery.autogrowtextarea.js', CClientScript::POS_END);
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/multiselect/js/ui.multiselect.js', CClientScript::POS_END);
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/selectbox/jquery.selectBox.min.js', CClientScript::POS_END);
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/timepicker/jquery.timepicker.js', CClientScript::POS_END);
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/colorpicker/js/colorpicker.js', CClientScript::POS_END);
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/uistars/jquery.ui.stars.min.js', CClientScript::POS_END);
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/tiptip/jquery.tipTip.minified.js', CClientScript::POS_END);
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/validation/jquery.validate.min.js', CClientScript::POS_END);
    
    // Configuration Script
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/adminica/adminica_ui.js', CClientScript::POS_END);
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/adminica/adminica_forms.js', CClientScript::POS_END);
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/adminica/adminica_mobile.js', CClientScript::POS_END);

    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/common/js/functions.js', CClientScript::POS_END);
?> 

<!-- Live Load (remove after development) -->
<script>//document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>

</head>
<body>
<div id="wrapper">
<!--    <div id="topbar" class="clearfix">

        <a href="" class="logo"><span>LetKit</span></a>

        <div class="user_box dark_box clearfix">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/profile.jpg" width="55" alt="Profile Pic" />
            <h2>Administrator</h2>
            <h3><a class="text_shadow" href="#">John Smith</a></h3>
            <ul>
                <li><a href="#">profile</a><span class="divider">|</span></li>
                <li><a href="#">settings</a><span class="divider">|</span></li>
                <li><a href="login.php">logout</a></li>
            </ul>
        </div>
    </div>	-->
    <div id="sidebar">
        <div class="cog">+</div>

        <a href="<?php echo Yii::app()->createUrl('cms') ;?>" class="logo"><span>LetKit</span></a>

        <div class="user_box dark_box clearfix">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/profile.jpg" width="55" alt="Profile Pic" />
            <h2>Administrator</h2>
            <h3><a href="#"><?php echo Yii::app()->user->name; ?></a></h3>
            <ul>
                <li><a href="#">settings</a><span class="divider">|</span></li>
                <li><?php echo CHtml::link('Thoát', array('//account/logout')); ?></li>
            </ul>
        </div><!-- #user_box -->

        <ul class="side_accordion"> <!-- add class 'open_multiple' to change to from accordion to toggles -->
<?php foreach (Yii::app()->modules as $moduleName => $moduleDetails): ?>
<?php
    $moduleLabel = Yii::t('global', $moduleName);
?>
            <li<?php if ($moduleName == $this->module->getName()): ?> class="active"<?php endif; ?> style="text-transform: capitalize;"><?php echo CHtml::link('<img src="'.Yii::app()->theme->baseUrl.'/images/icons/small/grey/home.png"/> '.$moduleLabel, array('//'.$moduleName)); ?></li>
<?php endforeach; ?>
        </ul>
<!--        <form>
            <div id="search_side" class="dark_box"><input class="" type="text" value="Search Adminica..." onclick="value=''"></div>
        </form>-->
    </div><!-- #sidebar -->
    <div id="main_container" class="main_container container_16 clearfix">
        <?php $this->renderPartial('//cms/default/_submenu'); ?>
        <?php $this->renderPartial('//cms/default/_breadcrumbs'); ?>
    
        <?php echo $content; ?>
    </div>
</div>	

<?php    
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/isotope/jquery.isotope.min.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/fancybox/jquery.fancybox-1.3.4.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/adminica/adminica_gallery.js');
?>

<div id="loading_overlay" style="display: none;">
    <div class="loading_message round_bottom">
        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/loading.gif" alt="loading" />
    </div>
</div>


<!--<div id="template_options" class="clearfix">
    <h3><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/adminica.png" alt="Adminica"></h3>
    <div class="layout_size round_all">
        <label>Layout:</label>
        <a href="styles/theme/switcher1.php?style=switcher.css">Fluid</a><span>|</span>
        <a href="styles/theme/switcher1.php?style=layout_fixed.css">Fixed</a><span>
    </div>
    <div class="layout_position round_all">
        <label class="display_none">Header: </label>
        <a href="styles/theme/switcher2.php?style=switcher.css">Side</a><span>|</span>
        <a href="styles/theme/switcher2.php?style=header_top.css">Top</a><span>|</span>
        <a href="styles/theme/switcher2.php?style=header_slideout.css">Slide</a>
    </div>
    <div class="layout_position round_all">
        <label>Theme: </label>
        <a href="styles/theme/switcher.php?style=multiple&switcher5.php=switcher.css&switcher4.php=switcher.css">Dark</a><span>|</span>
        <a href="styles/theme/switcher.php?style=multiple&switcher5.php=theme_light.css&switcher4.php=switcher.css">Light</a>
    </div>
    <div class="theme_colour round_all">
        <label class="display_none">Colour:</label>
        <a class="black" href="styles/theme/switcher3.php?style=switcher.css"><span>Black</span></a>
        <a class="blue" href="styles/theme/switcher3.php?style=theme_blue.css"><span>Blue</span></a>
        <a class="navy" href="styles/theme/switcher3.php?style=theme_navy.css"><span>Navy</span></a>
        <a class="red" href="styles/theme/switcher3.php?style=theme_red.css"><span>Red</span></a>
        <a class="green" href="styles/theme/switcher3.php?style=theme_green.css"><span>Green</span></a>
        <a class="magenta" href="styles/theme/switcher3.php?style=theme_magenta.css"><span>Magenta</span></a>
        <a class="orange" href="styles/theme/switcher3.php?style=theme_brown.css"><span>Brown</span></a>
    </div>
    <div class="theme_background round_all" id="bg_dark">
        <label>BGs:</label>
        <a href="styles/theme/switcher4.php?style=switcher.css">Boxes</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_punched.css">Punched</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_honeycomb.css">Honeycomb</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_wood.css">Wood</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_dark_wood.css">Timber</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_noise.css">Noise</a>
    </div>
    <div class="theme_background round_all" id="bg_light">
        <label>BGs:</label>
        <a href="styles/theme/switcher4.php?style=switcher.css">Silver</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_white_wood.css">Wood</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_squares.css">Squares</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_noise_zero.css">Noise</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_stripes.css">Stripes</a>
    </div>
</div>-->

</body>
</html>
