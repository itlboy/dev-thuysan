<!doctype html public "✰">
<!--[if lt IE 7]> <html lang="en-us" class="no-js ie6"> <![endif]-->
<!--[if IE 7]>    <html lang="en-us" class="no-js ie7"> <![endif]-->
<!--[if IE 8]>    <html lang="en-us" class="no-js ie8"> <![endif]-->
<!--[if IE 9]>    <html lang="en-us" class="no-js ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-us" class="no-js"> <!--<![endif]-->
<head>

<title>Login Administrator Letkit v1.0</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!-- iPhone, iPad and Android specific settings -->	

<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1;">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
<link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/images/iOS_icon.png">
<link rel="apple-touch-startup-image" href="<?php echo Yii::app()->theme->baseUrl; ?>/images/iOS_startup.png">

<!-- Styles -->

<?php    
    $cs = Yii::app()->getClientScript();
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/reset.css');
//    $cs->registerCssFile('http://fonts.googleapis.com/css?family=Open+Sans:400,700');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/fancybox/jquery.fancybox-1.3.4.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/tinyeditor/style.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/slidernav/slidernav.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/syntax_highlighter/styles/shCore.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/syntax_highlighter/styles/shThemeDefault.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/uitotop/css/ui.totop.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/fullcalendar/fullcalendar.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/isotope/isotope.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/elfinder/css/elfinder.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/tiptip/tipTip.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/uniform/css/uniform.aristo.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/multiselect/css/ui.multiselect.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/selectbox/jquery.selectBox.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/colorpicker/css/colorpicker.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/uistars/jquery.ui.stars.min.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/scripts/themeroller/Aristo.css');
    
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/text.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/grid.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/main.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/theme/theme_base.css');
    
    /* Style Switcher 
    The following stylesheet links are used by the styleswitcher to allow for dynamically changing how Adminica looks and acts.
    Styleswitcher documentation: http://style-switcher.webfactoryltd.com/documentation/

    switcher1.php : layout - fluid by default.								(eg. styles/theme/switcher1.php?default=layout_fixed.css)
    switcher2.php : header and sidebar positioning - sidebar by default.	(eg. styles/theme/switcher1.php?default=header_top.css)
    switcher3.php : colour skin - black/grey by default.					(eg. styles/theme/switcher1.php?default=theme_red.css)
    switcher4.php : background image - dark boxes by default.				(eg. styles/theme/switcher1.php?default=bg_honeycomb.css)
    switcher5.php : controls the theme - dark by default.					(eg. styles/theme/switcher1.php?default=theme_light.css)
    */
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/theme/switcher.css');
//    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/theme/layout_fixed.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/theme/theme_blue.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/theme/theme_blue.css');
    
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/colours.css');
    $cs->registerCssFile(Yii::app()->theme->baseUrl.'/styles/ie.css');
    
    $cs->registerCoreScript('jquery');
    //https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js
    $cs->registerScriptFile('https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/jquery-ui.min.js');
    
    // Global
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/touchPunch/jquery.ui.touch-punch.min.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/uitotop/js/jquery.ui.totop.js');
    
    // Forms
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/uniform/jquery.uniform.min.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/autogrow/jquery.autogrowtextarea.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/multiselect/js/ui.multiselect.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/selectbox/jquery.selectBox.min.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/timepicker/jquery.timepicker.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/colorpicker/js/colorpicker.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/uistars/jquery.ui.stars.min.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/tiptip/jquery.tipTip.minified.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/validation/jquery.validate.min.js');
    
    // Configuration Script
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/adminica/adminica_ui.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/adminica/adminica_forms.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/adminica/adminica_mobile.js');
    
//    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/common/js/functions.js');
?> 

<!-- Live Load (remove after development) -->
<script>//document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>

</head>
<body>

<?php echo $content; ?>
</body>
</html>
