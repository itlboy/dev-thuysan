<div id="wrapper">	
    <div class="isolate">
        <div id="login_box" class="center" style="display:none;">
            <div class="main_container clearfix">
                <div class="box">
                    <div class="block">
                        <div class="section">
                            <div class="alert dismissible alert_light">
                                <img width="24" height="24" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icons/small/grey/locked.png">
                                <strong>Welcome to LetKit.</strong> Please enter your details to login.
                            </div>
                        </div>
                        <?php $form=$this->beginWidget('CActiveForm', array(
                            'action'=>array('//account/backend/login'),
                            'id'=>'let-login-form',
                            'enableClientValidation'=>true,
                            'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                            ),
                        )); ?>
                        <?php echo $form->error($model,'message'); ?>
<!--                        <form action="index.php" class="validate_form">-->
                            <fieldset class="label_side">
                                <label for="username">Username<span>or email address</span></label>
                                <div>
<!--                                    <input type="text" id="username" name="username" class="required">-->
                                    <?php echo $form->textField($model,'username', array('class'=>'required')); ?>
                                    <?php echo $form->error($model,'username'); ?>
                                </div>
                            </fieldset>						
                            <fieldset class="label_side">
                                <label for="password">Password<span><a href="#">Do you remember?</a></span></label>
                                <div>
<!--                                    <input type="password" id="password" name="password" class="required">-->
                                    <?php echo $form->passwordField($model,'password', array('class'=>'required')); ?>
                                    <?php echo $form->error($model,'password'); ?>
                                </div>
                            </fieldset>
                            <fieldset class="no_label">																											
                                <div style="">
                                    <div class="slider_unlock" title="Trượt để đăng nhập"></div>
                                    <button type="submit" style="display:none"></button>
                                </div>
                            </fieldset>
                        <?php $this->endWidget(); ?>
                    </div>
                </div>
                <a href="index.php" id="login_logo"><span>LetKit</span></a>
                <button data-dialog="dialog_register" class="on_dark dark dialog_button" style="float:right; margin:-30px 0 0 0;">
                    <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icons/small/white/user.png">
                    <span>Not Registered ?</span>
                </button>
            </div>
            <div class="main_container clearfix" style="display:none;">
                <div class="box">
                    <div class="block">
                        <div class="section">
                            <div class="alert dismissible alert_light">
                                <img width="24" height="24" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icons/small/grey/locked.png">
                                <strong>Welcome to LetKit.</strong> Please enter your details to login.
                            </div>
                        </div>	
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="display_none">						
    <div id="dialog_register" class="dialog_content no_dialog_titlebar wide" title="Register for Adminica">
        <div class="block">
            <div class="section">
                <h2>Registration Form</h2>
            </div>
            <div class="columns clearfix">
                <div class="col_50">							
                    <fieldset class="label_top">
                        <label for="text_field_inline">Username<span>Between 5 and 20 characters</span></label>
                        <div>
                            <input type="text">
                        </div>
                    </fieldset>	
                </div>
                <div class="col_50">										
                    <fieldset class="label_top">
                        <label for="text_field_inline">Username again…<span>so we know you're human</span></label>
                        <div>
                            <input type="text">
                        </div>
                    </fieldset>
                </div>	
            </div>
            <div class="columns clearfix">
                <div class="col_50">							
                    <fieldset class="label_top">
                        <label for="text_field_inline">Password<span>Between 5 and 20 characters</span></label>
                        <div>
                            <input type="text">
                        </div>
                    </fieldset>	
                </div>
                <div class="col_50">										
                    <fieldset class="label_top">
                        <label for="text_field_inline">Repeat Password again…</label>
                        <div>
                            <input type="text">
                        </div>
                    </fieldset>
                </div>			
            </div>

            <fieldset class="label_side">
                <label>Permission</label>
                <div class="uniform inline clearfix">
                    <label for="agree_1"><input type="checkbox" name="agree_1" value="yes" id="agree_1"/>I agree with the terms and conditions</label>
                </div>
            </fieldset>	



            <div class="button_bar clearfix">
                <button class="dark blue no_margin_bottom link_button" data-link="index.php">
                    <div class="ui-icon ui-icon-check"></div>
                    <span>Register</span>
                </button>
                <button class="light send_right close_dialog">
                    <div class="ui-icon ui-icon-closethick"></div>
                    <span>Cancel</span>
                </button>
            </div>
        </div>
    </div>
</div> 
<script type="text/javascript">
    $(".validate_form").validate();
</script>

<!--<div id="loading_overlay">
    <div class="loading_message round_bottom">
        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/loading.gif" alt="loading" />
    </div>
</div>-->

