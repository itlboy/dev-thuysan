<div class="box grid_16" style="margin-top: 10px;">
    <div class="toggle_container">
        <div class="block">
            <h2 class="section"><?php echo Yii::t('global', 'Info'); ?></h2>
            
            <div class="columns clearfix">
                <div class="col_50">
                    <fieldset>
                        <label><?php echo $form->labelEx($model, 'regip'); ?></label>
                        <div>
                            <?php echo $model->regip; ?>
                        </div>
                    </fieldset>
                </div>
                <div class="col_50">
                    <fieldset>
                        <label><?php echo $form->labelEx($model, 'regdate'); ?></label>
                        <div>
                            <?php echo date(Yii::app()->params['timeFormat']['backend_default'], $model->regdate); ?>				
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="columns clearfix">
                <div class="col_50">
                    <fieldset>
                        <label><?php echo $form->labelEx($model, 'lastloginip'); ?></label>
                        <div>
                            <?php echo $model->lastloginip; ?>
                        </div>
                    </fieldset>
                </div>
                <div class="col_50">
                    <fieldset>
                        <label><?php echo $form->labelEx($model, 'lastlogintime'); ?></label>
                        <div>
                            <?php echo date(Yii::app()->params['timeFormat']['backend_default'], $model->lastlogintime); ?>				
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>
