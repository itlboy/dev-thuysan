<div class="box grid_16" style="margin-top: 10px;">
    <div class="toggle_container">
        <div class="block">
            <h2 class="section"><?php echo Yii::t('global', 'General'); ?></h2>

            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'username'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'username'); ?>
                    <?php echo $form->error($model, 'username'); ?>
                    <div class="required_tag tooltip hover left" title="<?php echo Yii::t('global', 'Required'); ?>"></div>
                </div>
            </fieldset>
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'password'); ?></label>
                <div>
                    <?php echo $form->passwordField($model, 'password'); ?>
                    <?php echo $form->error($model, 'password'); ?>
                    <div class="tooltip hover left" title="<?php echo Yii::t('global', 'Required'); ?>"></div>
                </div>
            </fieldset>
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'password_repeat'); ?></label>
                <div>
                    <?php echo $form->passwordField($model, 'password_repeat'); ?>
                    <?php echo $form->error($model, 'password_repeat'); ?>
                    <div class="tooltip hover left" title="<?php echo Yii::t('global', 'Required'); ?>"></div>
                </div>
            </fieldset>
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'email'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'email'); ?>
                    <?php echo $form->error($model, 'email'); ?>
                    <div class="tooltip hover left" title="<?php echo Yii::t('global', 'Required'); ?>"></div>
                </div>
            </fieldset>
            
            <fieldset class="label_side">
                <label><?php echo $model->getAttributeLabel('groups'); ?></label>
                <div>
                    <?php echo $form->checkBoxList($model, 'groups', CHtml::listData(KitUsergroup::model()->getGroupOptions(), 'usergroup_id', 'usergroup_name'), array(
                        'separator' => '',
                        'template' => '<div>{input} {label}</div>'));
                    ?>
                    <?php echo $form->error($model, 'groups'); ?>
                </div>
            </fieldset>

            
            <?php /*
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'groups'); ?></label>
                <div>
                    <?php
                    echo $form->checkBoxList($model, 'groups', CHtml::listData(KitUsergroup::model()->getGroupOptions(), 'usergroup_id', 'usergroup_name'), array(
                        'separator' => '',
                        'template' => '<div>{input} {label}</div>'));
                    ?>
                    <?php echo $form->error($model, 'groups'); ?>
                </div>
            </fieldset>
             * 
             */ ?>
        </div>
    </div>
</div>



