    <div class="let">
        <div class="logo fl"><span class="version">v0.1 Beta</span></div>
        <div class="logined fr">
            <div class="fl"> <?php echo CHtml::link('Ra trang chủ', Yii::app()->request->baseUrl, array('target'=>'_blank')); ?> | &nbsp;</div>
            <div class="fl"><strong>Xin chào:</strong> <?php echo Yii::app()->user->name; ?></div>
            <!--<div class="fl">&nbsp;| <a href="#">Nhiệm vụ</a></div>-->
        <div class="fl">&nbsp;| <?php echo CHtml::link('Thoát', array('//account/logout')); ?></div>
            <div class="letvn"></div>
        </div>
        <div class="letvn"></div>
    </div>
    <div class="letvn"></div>
	<div class="header">
<!--		<div class="search fr">
			<form name="search">
				<label>Tìm kiếm: </label>
				<input type="text" name="txtSearch"/>
				<input type="submit" name="btnSubmit" />
			</form>
                </div>-->
		<div class="topmenu">
			<ul>
<?php foreach (Yii::app()->modules as $moduleName => $moduleDetails): ?>
<?php
    $moduleLabel = Yii::t("backend", $moduleName);
?>
                <li<?php if ($moduleName == $this->module->getName()): ?> class="active"<?php endif; ?>><?php echo CHtml::link($moduleLabel, array('//'.$moduleName)); ?></li>
<?php endforeach; ?>
			</ul>
		</div>

		<div class="subNavi letvn" id="menu_module">
<?php $menuList = Yii::app()->controller->getModule()->menuList; ?>
<?php foreach ($menuList as $group): ?>
			<div class="subNavi-box navi-bg">
    <?php foreach ($group['items'] as $key => $item): ?>
    <?php
    if ($item['type'] == 'dialog') 
        $onclick = "loadAjaxDialog('".Yii::app()->createUrl($item['url'])."', '".$item['name']."', 400);";
    else
        $onclick = "location.href = '".Yii::app()->createUrl($item['url'])."'";
    ?>
				<!-- Item -->
				<div class="subNavi-box1 letvn-box" onclick="<?php echo $onclick; ?>">
					<div class="lf1">
						<div class="rt1"> <a href="javascript:void(0);"><img src="<?php echo Yii::app()->theme->baseUrl; ?><?php echo $item['icon']; ?>" alt="" /> <span><?php echo $item['name']; ?></span> </a></div>
					</div>
                    <div class="guideline"><?php echo $item['desc']; ?></div>
				</div>
				<!-- END Item -->
    <?php endforeach; ?>
                <div class="letvn"></div>
				<p><?php echo $group['groupName']; ?></p>
			</div>
<?php endforeach; ?>
        </div>
	</div>
    <div class="letvn"></div>
    <div class="clickstream border_bottom2">
        <div class="item home fl"><?php echo CHtml::link('Trang chủ', array('//cms')); ?></div>
        <div class="item fl">&raquo;</div>
        <div class="item fl"><?php echo CHtml::link($this->module->getName(), array('//'.$this->module->getName().'')); ?></div>
        <div class="letvn"></div>
    </div>
