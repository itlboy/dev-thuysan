<?php
function buildDialog($params) {
    echo "onclick=\"js:ajaxDialogForm('{$params['dialog']['grid']}', '{$params['dialog']['title']}', '" . Yii::app()->createUrl($params['url']) . "'); return false\"";
}
?>

<?php $menuList = isset(Yii::app()->controller->getModule()->menuList) ? Yii::app()->controller->getModule()->menuList : NULL; ?>
<?php if ($menuList !== NULL AND !empty($menuList)): ?>

        <div id="nav_top" class="clearfix round_top">
            <ul class="clearfix">
    <?php foreach ($menuList as $root): ?>
                <li>
        <?php if ($root['type'] == 'direct'): ?>
                    <a href="<?php echo $this->createUrl($root['url']); ?>"><img src="<?php echo Yii::app()->theme->baseUrl . $root['icon']; ?>"/><span><?php echo $root['name']; ?></span></a>
        <?php elseif ($root['type'] == 'dialog'):  ?>
                    <a href="javascript:void(0);" onclick="ajaxDialogForm('<?php echo $root['grid']; ?>', '<?php echo $root['name']; ?>', '<?php echo $this->createUrl($root['url']); ?>');"><img src="<?php echo Yii::app()->theme->baseUrl . $root['icon']; ?>"/><span><?php echo $root['name']; ?></span></a>
        <?php endif; ?>
        <?php if (isset($root['items']) AND !empty($root['items'])): ?>
                    <ul>
            <?php foreach ($root['items'] as $sub1): ?>
                        <li><a href="<?php echo $this->createUrl($sub1['url']); ?>"><img src="<?php echo Yii::app()->theme->baseUrl . $sub1['icon']; ?>"/><span><?php echo $sub1['name']; ?></span></a>
                <?php if (isset($sub1['items']) AND !empty($sub1['items'])): ?>
                            <ul>
                <?php foreach ($sub1['items'] as $sub2): ?>
                                <li><a href="<?php echo $this->createUrl($sub2['url']); ?>"><img src="<?php echo Yii::app()->theme->baseUrl . $sub2['icon']; ?>"/><span><?php echo $sub2['name']; ?></span></a></li>
                <?php endforeach; ?>
                            </ul>
                <?php endif; ?>
                        </li>
            <?php endforeach; ?>
                    </ul>
        <?php endif; ?>
                </li>
    <?php endforeach; ?>

            </ul>
            <div class="display_none">
                <div id="logout" class="dialog_content narrow" title="Logout">
                    <div class="block">
                        <div class="section">
                            <h1>Thank you</h1>
                            <div class="dashed_line"></div>
                            <p>We will now log you out of Adminica in a 10 seconds...</p><p></p>
                        </div>
                        <div class="button_bar clearfix">
                            <button class="dark blue no_margin_bottom link_button" data-link="login.php">
                                <div class="ui-icon ui-icon-check"></div>
                                <span>Ok</span>
                            </button>
                            <button class="light send_right close_dialog">
                                <div class="ui-icon ui-icon-closethick"></div>
                                <span>Cancel</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>


            <script type="text/javascript">
//                var currentPage = 3 - 1; // This is only used in php version to tell the nav what the current page is
//                $('#nav_top > ul > li').eq(currentPage).addClass("current");
//                $('#nav_top > ul > li').addClass("icon_only").children("a").children("span:visible").parent().parent().removeClass("icon_only");
            </script>



            <div id="mobile_nav">
                <div class="main"></div>
                <div class="side"></div>
            </div>

        </div><!-- #nav_top -->
<?php endif; ?>
