<?php
$form=$this->beginWidget('CActiveForm', array(
'id' => 'cms_chart',
'enableAjaxValidation' => false,
'htmlOptions' => array('enctype'=>'multipart/form-data'),
));
?>
<div class="grid_16">
    <button class="green small">
        <div class="ui-icon ui-icon-check"></div>
        <span><?php echo Yii::t('global', 'Xác nhận lọc'); ?></span>
    </button>
    <button class="green small">
        <div class="ui-icon ui-icon-check"></div>
        <span><?php echo Yii::t('global', 'Cập nhật dữ liệu'); ?></span>
    </button>
</div>
<div class="box grid_16" style="opacity: 1;">
    <h2 class="box_head">Bộ lọc dữ liệu</h2>
    <a class="grabber" href="#"></a>
    <a class="toggle" href="#"></a>
    <div class="">
        <div class="toggle_container">
            <div class="block">
                <div class="columns clearfix">
                    <div class="col_50">
                        <fieldset class="label_side">
                            <label>Thống kê từ</label>
                            <div>
                                <?php echo CHtml::textField('date_from', $from_time, array('class' => 'datepicker')); ?>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col_50">
                        <fieldset class="label_side">
                            <label>đến</label>
                            <div>
                                <?php echo CHtml::textField('date_to', $to_time, array('class' => 'datepicker')); ?>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="columns clearfix">
                    <div class="col_50">
                        <fieldset class="label_side">
                            <label>
                                <button class="blue small" >
                                    <span style="color: white;"><?php echo Yii::t('global', 'Chọn dữ liệu'); ?></span>
                                </button>
                            </label>
                            <div>
                                <?php
                                // Liệt kê danh sách Module lấy từ database bảng config và 3 trường liên quan tiền tệ
                                    $active = isset($_REQUEST['filter']) ? $_REQUEST['filter'] : $filter;
                                    $modules = array();
                                    foreach($moduleList as $module){
                                        $modules[$module] = $module;
                                    }
                                    $modules['currency_money'] = 'currency_money';
                                    $modules['currency_gold_in'] = 'currency_gold_in';
                                    $modules['currency_gold_out'] = 'currency_gold_out';
                                    echo CHtml::checkBoxList('filter', $active , $modules);
                                ?>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col_50" style="border: 0px;">
                        <fieldset class="label_side">
                            <style type="text/css">
                                .chart_item{list-style: none; margin: 10px;}
                                .chart_item li{padding: 5px 0px;}
                            </style>
                            <?php if(!empty($total_data)): ?>
                            <ul class="chart_item">
                                <?php foreach($total_data as $key => $item): ?>
                                <li><strong style="font-size: 18px;"><?php echo Yii::t('global',$key); ?>: <?php echo number_format($item,0,',','.'); ?></strong></li>
                                <?php endforeach; ?>
                            </ul>
                            <?php endif; ?>
                        </fieldset>
                    </div>
                </div>


            </div>
        </div>
    </div
</div>
<?php $this->endWidget(); ?>

<?php if (!empty($moduleSelected)): ?>
<div class="box grid_16" style="opacity: 1;margin: 0px; width: 100%; margin-top: 10px;">
    <h2 class="box_head">Thống kê Module</h2>
    <a class="grabber" href="#"></a>
    <a class="toggle" href="#"></a>
    <div class="">
        <div class="toggle_container">
<?php
    $this->widget('cms.widgets.backend.charts',array(
        'data' => $data['modules'],
    ));
?>
        </div>
    </div>
</div>
<?php endif; ?>

<?php if (isset($filter['currency_money']) AND !empty($filter['currency_money'])): // Em tu viet ?>
<div class="box grid_16" style="opacity: 1;margin: 0px; width: 100%; margin-top: 10px;">
    <h2 class="box_head">Thống kê Money</h2>
    <a class="grabber" href="#"></a>
    <a class="toggle" href="#"></a>
    <div class="">
        <div class="toggle_container">
<?php
    $this->widget('cms.widgets.backend.charts',array(
        'data' => $data['currency_money'],
    ));
?>
        </div>
    </div>
</div>
<?php endif; ?>

<?php if (isset($gold) AND !empty($gold)):?>
<div class="box grid_16" style="opacity: 1;margin: 0px; width: 100%; margin-top: 10px;">
    <h2 class="box_head">Thống kê Gold</h2>
    <a class="grabber" href="#"></a>
    <a class="toggle" href="#"></a>
    <div class="">
        <div class="toggle_container">
<?php
    $this->widget('cms.widgets.backend.charts',array(
        'data' => $data['gold'],
    ));
    ?>
        </div>
    </div>
</div>
<?php endif; ?>
