<div class="grid_12">
    <div class="indented round_all clearfix">
        <?php
        $this->widget('Breadcrumbs', array(
            'tagName' => 'ul',                
            'separator' => '',
            'homeIcon' => '<div class="ui-icon ui-icon-home"></div>',
            'homeLink' => 'javascript:;',                
            'htmlOptions' => array('class' => 'breadcrumb clearfix'),
            'links' => $this->breadcrumbs,
        ));                        
        ?>  
    </div>
</div>
