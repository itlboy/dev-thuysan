<?php if ($error !== ''): ?>
<div class="grid_16">
    <div class="alert dismissible alert_red">
        <img height="24" width="24" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icons/small/white/alarm_bell.png">
        <?php echo $error; ?>
    </div>
</div>
<?php endif;?>
