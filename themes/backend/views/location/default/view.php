<?php
$this->breadcrumbs=array(
	'Kit Categories'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List KitLocation', 'url'=>array('index')),
	array('label'=>'Create KitLocation', 'url'=>array('create')),
	array('label'=>'Update KitLocation', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete KitLocation', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage KitLocation', 'url'=>array('admin')),
);
?>

<h1>View KitLocation #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'alias',
		'parent_id',
		'description',
		'layout',
		'created_time',
		'updated_time',
		'sorder',
		'status',
	),
)); ?>
