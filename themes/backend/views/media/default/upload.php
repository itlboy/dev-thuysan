<?php

Yii::import("ext.xupload.models.XUploadForm");
$uploadModel = new XUploadForm;
$this->widget('ext.xupload.XUploadWidget', array(
    'url' => Yii::app()->createUrl('cms/upload/upload', array('filename' => Date('Ymd').'_'.rand(100000, 999999))),
    'model' => $uploadModel,
    'attribute' => 'file',
    'multiple' => false,
    'options' => array(
        'onComplete' => 'js:function (event, files, index, xhr, handler, callBack) {
            if (handler.response.error !== 0) alert(handler.response.error);
            else {

            }
        }',
    )
));
