<?php
$form=$this->beginWidget('CActiveForm', array(
    'id'=>'form_default',
    'enableAjaxValidation'=>false,
));
?>
<table>
    <tr>
        <td><?php echo $form->labelEx($model,'active'); ?></td>
        <td>
            <div><?php echo $form->checkBox($model,'active'); ?></div>
            <div><?php echo $form->error($model,'active'); ?></div>
        </td>
    </tr>
    <tr>
        <td><?php echo $form->labelEx($model,'keyword'); ?></td>
        <td>
            <div><?php echo $form->textField($model,'keyword'); ?></div>
            <div><?php echo $form->error($model,'keywrd'); ?></div>
        </td>
    </tr>
    <tr>
        <td><?php echo $form->labelEx($model,'module'); ?></td>
        <td>
            <div><?php echo $form->textField($model,'module'); ?></div>
            <div><?php echo $form->error($model,'module'); ?></div>
        </td>
    </tr>
    <tr>
        <td><?php echo $form->labelEx($model,'lang_group'); ?></td>
        <td>            
            <div><?php echo $form->dropDownList($model,'lang_group',Yii::app()->controller->module->languageGroups); ?></div>
            <div><?php echo $form->error($model,'lang_group'); ?></div>
        </td>
    </tr>
<?php foreach ($list['pack'] as $pack) : ?>
    <tr>
        <td><?php echo CHtml::label($pack->name, 'value['.$pack->alias.']'); ?></td>
        <td>
            <div><?php echo CHtml::textField('value['.$pack->alias.']', '', array('id'=>'value['.$pack->alias.']')); ?></div>
            <div><?php echo $form->error($model,'module'); ?></div>
        </td>
    </tr>
<?php endforeach; ?>
</table>

<?php 
$cs = Yii::app()->getClientScript();
$cs->registerCoreScript('jquery');
?>
<script type="text/javascript">
function submitAjax(idForm) {
    var queryString = $('#'+idForm+'').serialize();
	if (queryString != '') {
		$.ajax({
            url: "<?php echo Yii::app()->createUrl('//languages/default/InsertKey'); ?>",
            type: 'POST',
            data: queryString,
            success: function(){
                loadAjax('<?php echo Yii::app()->createUrl($this->module->getName() . '/' . Yii::app()->controller->id . '/LanguagesList'); ?>', '', 'ajax_result_list');
                $("#dialog").dialog("close");
            }
        });
	}
}
</script>
<div style="text-align: right;">
    <button type="button" class="button" onclick="$('#dialog').dialog('close');">Hủy</button>
    <button type="button" class="button" onclick="submitAjax('form_default');">Submit</button>
</div>
<?php $this->endWidget(); ?>
