<script>
function saveAllLangKey (idForm) {
    var queryString = $('#'+idForm+'').serialize();
	if (queryString != '') {
		$.ajax({
            url: "<?php echo Yii::app()->createUrl('//languages/default/UpdateKey'); ?>",
            type: 'POST',
            data: queryString,
            success: function(){
                loadAjax('<?php echo Yii::app()->createUrl($this->module->getName() . '/' . Yii::app()->controller->id . '/LanguagesList'); ?>', '', 'ajax_result_list');
            }
        });
	}
}
</script>
                            <div class="box1 margin_bottom3">
                            	<div class="bar1">Bộ lọc</div>
                                <div class="content">
                                    Từ khóa: <input name="name" type="text" value="value" class="text" />
                                    Group:
                                    <select name="name" size="1" class="select">
                                        <option value="">Tất cả</option>
                                        <option value="global">Global</option>
                                        <option value="backend">Backend</option>
                                        <option value="frontend">Frontend</option>
                                    </select>
                                    Module:
                                    <select name="name" size="1" class="select">
                                        <option value="">Tất cả</option>
                                        <option value="global">Global</option>
                                        <option value="languages">Languages</option>
                                        <option value="news">News</option>
                                    </select>
                                    <div>
                                        <button type="button" class="button icon" onclick=""><img class="icon" src="<?php echo Yii::app()->theme->baseUrl.'/common/img/icon/icon_filter_16.png'; ?>" />Xác nhận bộ lọc</button>
                                        <button type="button" class="button" onclick="">Làm lại bộ lọc</button>
                                    </div>
                                </div>
                            </div>
                            <div style="text-align: left; margin: 10px;">
                                <button type="button" class="button saveButton" onclick="saveAllLangKey('form_list_default');">Lưu tất cả</button>
                                <button type="button" class="button actionToCheckbox" onclick="">Xóa</button>
                                <button type="button" class="button actionToCheckbox" onclick="">Chuyển vào thùng rác</button>
                                <button type="button" class="button" onclick="">Export</button>
                                <button type="button" class="button" onclick="">Import</button>
                                <button type="button" class="button" onclick=""><img src="<?php echo Yii::app()->theme->baseUrl.'/common/img/icon/icon_refresh_16.png'; ?>" /></button>
                                <button type="button" class="button" onclick=""><img src="<?php echo Yii::app()->theme->baseUrl.'/common/img/icon/icon_refresh_16.png'; ?>" /> Import</button>
                            </div>
                            
                            <?php $form=$this->beginWidget('CActiveForm', array(
                            	'id'=>'form_list_default',
                            	'enableAjaxValidation'=>false,
                                'htmlOptions'=>array('enctype'=>'multipart/form-data'),
                            )); ?>
                            <table width="100%" border="0" cellspacing="1" cellpadding="0" class="table_list1">
                                <tr>
                                    <th scope="col" class="width_min"><input type="checkbox" name="checkall" id="checkall" onchange="checkallrow('class', 'checklist');" /></th>
                                    <th scope="col" class="width_min">ID</th>
                                    <th scope="col" class="width_min">Group</th>
                                    <th scope="col" class="width_min">Module</th>
                                    <th scope="col" class="width_min">Key</th>
                                    <?php foreach ($list['packLanguages'] as $key2 => $value2): ?>
                                    <th scope="col"><?php echo $value2->name; ?></th>
                                    <?php endforeach; ?>
                                    <th scope="col" class="width_min">Active</th>
                                    <th scope="col" class="width_min">Action</th>
                                </tr>
<?php foreach ($list['rows'] as $key => $value): ?>
    <?php $valueArray = json_decode($value->value, TRUE); ?>
                                <tr class="row">
                                    <td class="width_min"><input type="checkbox" name="row_<?php echo $value->id; ?>" id="row_<?php echo $value->id; ?>" class="checklist" value="<?php echo $value->id; ?>" onchange="checkShowAction('class', 'checklist');" /></td>
                                    <td class="width_min"><?php echo $value->id; ?></td>
                                    <td nowrap="nowrap"><?php echo $value->lang_group; ?></td>
                                    <td nowrap="nowrap"><?php echo $value->module; ?></td>
                                    <td nowrap="nowrap"><?php echo $value->keyword; ?></td>
    <?php foreach ($list['packLanguages'] as $key2 => $value2): ?>
        <?php $id = md5($this->module->getName() . $this->getId() . $this->action->id . $value->id . $value2->alias); ?>
                                    <td id="<?php echo $id; ?>" lang="<?php echo $value2->alias; ?>" class="editField" onclick="editLangKey('<?php echo $value->id; ?>');"><?php echo letArray::get($valueArray, $value2->alias); ?></td>
<!--                                    <td id="<?php echo $id; ?>" onclick="stringToInput(this.id, '<?php echo letArray::get($valueArray, $value2->alias); ?>', 'list[<?php echo $value->id; ?>][<?php echo $value2->alias; ?>]');"><?php echo letArray::get($valueArray, $value2->alias); ?></td>-->
    <?php endforeach; ?>
                                    <?php 
                                        $tdId = 'acive_' . $value->id;
                                        $valueChange = ($value->active == 0) ? 1 : 0;
                                    ?>
                                    <td class="width_min changeStatusAjax" id="<?php echo $tdId ?>" onclick="loadAjax('<?php echo Yii::app()->createUrl('//global/ChangeStatusValue/index', array('model'=>'Languages', 'valueChange'=>$valueChange)); ?>', '', '<?php echo $tdId ?>');"><?php echo letFunction::statusToImages($value->active); ?></td>
                                    <td class="width_min">
                                        <a href="javascript:void(0);" onclick="editLangKey('<?php echo $value->id; ?>');"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/common/img/icon/icon_edit_16.png" /></a>
                                    	<a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/common/img/icon/icon_delete_16.png" /></a>
                                    </td>
                                </tr>
<?php endforeach; ?>
                            </table>
                            <?php // echo CHtml::ajaxSubmitButton('Save', array('//languages/backend/default/UpdateKey')); ?>                            
                            <?php $this->endWidget(); ?>