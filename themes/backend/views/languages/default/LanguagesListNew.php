<script>
function saveAllLangKey (idForm) {
    var queryString = $('#'+idForm+'').serialize();
	if (queryString != '') {
		$.ajax({
            url: "<?php echo Yii::app()->createUrl('//languages/default/UpdateKey'); ?>",
            type: 'POST',
            data: queryString,
            success: function(){
                loadAjax('<?php echo Yii::app()->createUrl($this->module->getName() . '/' . Yii::app()->controller->id . '/LanguagesList'); ?>', '', 'ajax_result_list');
            }
        });
	}
}

function ajaxGridFilter() {
    $.fn.yiiGridView.update('languages-grid',{ data:{
        languageKeyword:$('#keyword').val(),
        languageGroup:$('#languageGroup').val(),
        languageModule:$('#languageModule').val()
    } })
}

function ajaxGridFilterReset() {
    $('#keyword').val('');
    $('#languageGroup').val('');
    $('#languageModule').val('');
    
    $.fn.yiiGridView.update('languages-grid',{ data:{
        languageKeyword:$('#keyword').val(),
        languageGroup:$('#languageGroup').val(),
        languageModule:$('#languageModule').val()
    } })
}

function ajaxGridLiveEdit(grid, obj) {            
    var tableTr = jQuery(obj).parent();
    var id = $.fn.yiiGridView.getKey(
        $(tableTr).closest('.grid-view').attr('id'),
        $(tableTr).prevAll().length
    );

    if (jQuery(tableTr).hasClass('liveEditing')) {
            jQuery(tableTr).removeClass('liveEditing');
            jQuery(tableTr).find('.liveEditField').each(function(index) {            
            jQuery(this).html(jQuery(this).find('input').val());
        });
        
        if (jQuery('.liveEditing').length == 0)
            $('.saveButton').hide();
        return false;
    }
    
    jQuery(tableTr).addClass('liveEditing');
    jQuery(tableTr).find('.liveEditField').each(function(index) {
        result = '<input name="list['+id+']['+$(this).attr('lang')+']" id="list['+id+']['+$(this).attr('lang')+']" value="'+$(this).text()+'" style="width:100%; padding:2px 0 3px; margin-right:3px; border:0px solid #4dab45; background-color:#539dce" />';
        jQuery(this).html(result);
    });
    
    if (jQuery('.liveEditing').length != 0)
        $('.saveButton').show();
    
    return false;
}
</script>
                            <div class="box1 margin_bottom3">
                            	<div class="bar1">Bộ lọc</div>
                                <div class="content">
                                    Từ khóa: <input name="keyword" id="keyword" type="text" value="" class="text" />
                                    Group:
                                    <?php
                                    echo CHtml::dropDownList('languageGroup',
                                        Yii::app()->user->getState('languageGroup'),
                                        array('global'=>'Global', 'backend'=>'Backend','frontend'=>'Frontend'),
                                        array(
                                            'prompt'=>'Tất cả',
                                            //'onchange'=>"$.fn.yiiGridView.update('languages-grid',{ data:{languageGroup: $(this).val()} })",
                                            'class'=>'select'
                                    )); ?>
                                    Module:
                                    <?php
                                    echo CHtml::dropDownList('languageModule',
                                        Yii::app()->user->getState('languageModule'),
                                        array('global'=>'Global', 'languages'=>'Languages','news'=>'News'),
                                        array(
                                            'prompt'=>'Tất cả',
                                            //'onchange'=>"$.fn.yiiGridView.update('languages-grid',{ data:{languageModule: $(this).val()} })",
                                            'class'=>'select'
                                    )); ?>
                                    <div>
                                        <button type="button" class="button icon" onclick="ajaxGridFilter()"><img class="icon" src="<?php echo Yii::app()->theme->baseUrl.'/common/img/icon/icon_filter_16.png'; ?>" />Xác nhận bộ lọc</button>
                                        <button type="button" class="button" onclick="ajaxGridFilterReset()">Làm lại bộ lọc</button>
                                    </div>
                                </div>
                            </div>
                            <div style="text-align: left; margin: 10px;">
                                <button type="button" class="button saveButton" onclick="saveAllLangKey('form_list_default');">Lưu tất cả</button>
                                <button type="button" class="button actionToCheckbox" onclick="">Xóa</button>
                                <button type="button" class="button actionToCheckbox" onclick="">Chuyển vào thùng rác</button>
                                <button type="button" class="button" onclick="">Export</button>
                                <button type="button" class="button" onclick="">Import</button>
                                <button type="button" class="button" onclick=""><img src="<?php echo Yii::app()->theme->baseUrl.'/common/img/icon/icon_refresh_16.png'; ?>" /></button>
                                <button type="button" class="button" onclick=""><img src="<?php echo Yii::app()->theme->baseUrl.'/common/img/icon/icon_refresh_16.png'; ?>" /> Import</button>
                            </div>
                            
<?php
echo '<form id="form_list_default">';
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'languages-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,   
    'template'=>'{items}{pager}',
    'ajaxUpdate'=>true,
    //'cssFile'=>Yii::app()->theme->baseUrl.'/common/gridview/styles.css',
    'pager'=>array(
        //'cssFile'=>Yii::app()->theme->baseUrl.'/common/gridview/styles.css',
        'header'=>'',
        'firstPageLabel'=>'&lt;&lt;',
        'prevPageLabel'=>'&lt;',
        'nextPageLabel'=>'&gt;', 
        'lastPageLabel'=>'&gt;&gt;',
        'maxButtonCount'=>10,
        'class'=>'CLinkPager'),
	'columns'=>$columns
));
echo '</form>';
