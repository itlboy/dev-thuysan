<?php
$moduleName = 'option';
$modelName = 'KitOption';
$formID = 'kit_'.$moduleName.'_form';
$gridID = 'kit_'.$moduleName.'_grid';

$form = $this->beginWidget('CActiveForm', array(
	'id' => $formID,
	'enableAjaxValidation'=>false,
    'htmlOptions' => array(
        'onSubmit' => 'js:ajaxSaveForm("'.$modelName.'" ,jQuery(this).attr("id"), jQuery(this).attr("action")); return false',
    )
)); ?>

    <div id="" class="dialog_content">
        <div class="block" style="opacity: 1;">
            <div class="section">
                <h2>Thông tin Option</h2>
<!--                <div class="alert dismissible alert_black">
                    <img width="24" height="24" src="images/icons/small/white/alert_2.png">
                    <strong>All the form fields</strong> can be just as easily used in a dialog.
                </div>-->
            </div>

            <fieldset class="label_side">
                <?php echo $form->labelEx($model, 'name'); ?>
                <div>
                    <?php echo $form->textField($model, 'name', array('class' => 'tooltip right text')); ?>
                </div>
            </fieldset>

            <fieldset class="label_side">
                <?php echo $form->labelEx($model, 'alias'); ?>
                <div>
                    <?php echo $form->textField($model,'alias',array('class' => 'tooltip right text', 'size' => 30,'maxlength' => 150)); ?>
                </div>
            </fieldset>

            <fieldset class="label_side">
                <?php echo $form->labelEx($model,'status'); ?>
                <div>
                    <div class="selector" id="uniform-undefined"><span style="-moz-user-select: none;"><?php echo Yii::t('BackEnd', "-- Select a status --"); ?></span>
                        <?php echo $form->dropdownList($model, 'status', BackendFunctions::getStatusOptions(), array(
                            'class' => 'select_box',
                            'style' => 'opacity:0',
                        )) ?>
                    </div>
                </div>
            </fieldset>

            <fieldset class="label_side">
                <?php echo $form->labelEx($model,'parent_id'); ?>
                <div>
                    <div class="selector" id="uniform-undefined"><span style="-moz-user-select: none;"><?php echo Yii::t('BackEnd', "-- Select a option --"); ?></span>
                        <?php $this->widget("option.widgets.MultiLevelDropDown", array(
                            'name' => 'KitOption[parent_id]',
                            'data' => KitOption::dumpAllOptionToArray($model->module),
                            'select' => $model->parent_id,
                            'noneSelect' => true,
                            'noneSelectValue' => 0,
                            'noneSelectLabel' => Yii::t('BackEnd', "-- Select a option --"),
                            'htmlOptions' => array(
                                'options' => array(
                                    $model->id => array('disabled' => true),
                                ),
                                'class' => 'select_box',
                                'style' => 'opacity:0',
                            ),
                        )); ?>
                    </div>
                </div>
            </fieldset>

            <fieldset class="label_side">
                <?php echo $form->labelEx($model,'module'); ?>
                <div>
                    <div class="selector" id="uniform-undefined"><span style="-moz-user-select: none;"><?php echo Yii::t('BackEnd', "-- Select a module --"); ?></span>
                        <?php echo $form->dropdownList($model, 'module', $model->getModuleList(), array(
                            'class' => 'select_box',
                            'style' => 'opacity:0',
                        )) ?>
                    </div>
                </div>
            </fieldset>

            <div class="button_bar clearfix">
                <button class="dark green close_dialog">
                    <div class="ui-icon ui-icon-check"></div>
                    <span>Submit</span>
                </button>
<!--                <button class="dark red close_dialog">
                    <div class="ui-icon ui-icon-closethick"></div>
                    <span>Cancel</span>
                </button>-->
            </div>
        </div>
    </div>

<?php $this->endWidget(); ?>

