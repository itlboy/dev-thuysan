<div class="box grid_16" style="margin-top: 10px;">
    <div class="toggle_container">
        <div class="block">
            <h2 class="section"><?php echo Yii::t('global', 'General'); ?></h2>
            
                            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'name'); ?></label>
                <div>
                    <?php echo $form->textField($model,'name'); ?>
                    <?php echo $form->error($model,'title'); ?>
                                <div class="required_tag tooltip hover left" title="<?php echo Yii::t('global', 'Required'); ?>"></div>
                            </div>
            </fieldset>
                                    <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'alias'); ?></label>
                <div>
                    <?php echo $form->textField($model,'alias'); ?>
                    <?php echo $form->error($model,'title'); ?>
                                <div class="required_tag tooltip hover left" title="<?php echo Yii::t('global', 'Required'); ?>"></div>
                            </div>
            </fieldset>
                                    <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'parent_id'); ?></label>
                <div>
                    <?php echo $form->textField($model,'parent_id'); ?>
                    <?php echo $form->error($model,'title'); ?>
                                <div class="required_tag tooltip hover left" title="<?php echo Yii::t('global', 'Required'); ?>"></div>
                            </div>
            </fieldset>
                                    <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'description'); ?></label>
                <div class="clearfix">
                    <?php echo $form->textArea($model,'description', array('class' => 'tooltip', 'title' => 'Nhập thông tin...')); ?>
                    <?php echo $form->error($model,'description'); ?>
                </div>
            </fieldset>
                                    <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'layout'); ?></label>
                <div>
                    <?php echo $form->textField($model,'layout'); ?>
                    <?php echo $form->error($model,'title'); ?>
                            </div>
            </fieldset>
                                    <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'module'); ?></label>
                <div>
                    <?php echo $form->textField($model,'module'); ?>
                    <?php echo $form->error($model,'title'); ?>
                            </div>
            </fieldset>
                                    <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'lft'); ?></label>
                <div>
                    <?php echo $form->textField($model,'lft'); ?>
                    <?php echo $form->error($model,'title'); ?>
                                <div class="required_tag tooltip hover left" title="<?php echo Yii::t('global', 'Required'); ?>"></div>
                            </div>
            </fieldset>
                                    <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'rgt'); ?></label>
                <div>
                    <?php echo $form->textField($model,'rgt'); ?>
                    <?php echo $form->error($model,'title'); ?>
                                <div class="required_tag tooltip hover left" title="<?php echo Yii::t('global', 'Required'); ?>"></div>
                            </div>
            </fieldset>
                                    <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'level'); ?></label>
                <div>
                    <?php echo $form->textField($model,'level'); ?>
                    <?php echo $form->error($model,'title'); ?>
                                <div class="required_tag tooltip hover left" title="<?php echo Yii::t('global', 'Required'); ?>"></div>
                            </div>
            </fieldset>
                                    <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'root'); ?></label>
                <div>
                    <?php echo $form->textField($model,'root'); ?>
                    <?php echo $form->error($model,'title'); ?>
                            </div>
            </fieldset>
                                                
        </div>
    </div>
</div>


<div class="box grid_16">
    <div class="toggle_container">
        <div class="block">
            <div class="columns clearfix">
                <div class="col_25">
                    <fieldset>
                        <label><?php echo $form->labelEx($model,'sorder'); ?></label>
                        <div>
                            <?php echo $form->textField($model,'sorder', array('class' => 'tooltip autogrow', 'title' => 'Số càng nhỏ thì thứ tự càng cao. Mặc định: 500', 'placeholder' => 'Nhập 1 số')); ?>
                            <?php echo $form->error($model,'sorder'); ?>
                        </div>
                    </fieldset>
                </div>
                <div class="col_25">
                    <fieldset>
                        <label><?php echo $form->labelEx($model,'promotion'); ?></label>
                        <div>
                            <div class="jqui_radios">
                                <?php echo $form->radioButtonList($model,'promotion', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                                <?php echo $form->error($model,'promotion'); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col_25">
                    <fieldset>
                        <label><?php echo $form->labelEx($model,'status'); ?></label>
                        <div>
                            <div class="jqui_radios">
                                <?php echo $form->radioButtonList($model,'status', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                                <?php echo $form->error($model,'status'); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col_25">
                    <fieldset>
                        <label><?php echo $form->labelEx($model,'trash'); ?></label>
                        <div>
                            <div class="jqui_radios">
                                <?php echo $form->radioButtonList($model,'trash', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                                <?php echo $form->error($model,'trash'); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>
    
