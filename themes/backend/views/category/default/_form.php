<?php
$moduleName = 'category';
$modelName = 'KitCategory';
$formID = 'kit_'.$moduleName.'_form';
$gridID = 'kit_'.$moduleName.'_grid';

$form = $this->beginWidget('CActiveForm', array(
	'id' => $formID,
	'enableAjaxValidation'=>false,
    'htmlOptions' => array(
        'onSubmit' => 'js:ajaxSaveForm("'.$modelName.'" ,jQuery(this).attr("id"), jQuery(this).attr("action")); return false',
    )
)); ?>
    <div id="" class="dialog_content">
        <div class="block" style="opacity: 1;">
            <div class="section">
                <h2>Thông tin danh mục</h2>
<!--                <div class="alert dismissible alert_black">
                    <img width="24" height="24" src="images/icons/small/white/alert_2.png">
                    <strong>All the form fields</strong> can be just as easily used in a dialog.
                </div>-->
            </div>

        <?php if ($model->isNewRecord) : ?>
            <a href="#" onclick="js:jQuery('#nameExtra').append(jQuery('#nameOrigin').html())">Add</a>
            <div id="nameOrigin">
            <fieldset class="label_side">
                <?php echo $form->labelEx($model, 'name'); ?>
                <div>
                    <?php echo CHtml::textField('KitCategory[name][]', $model->name, array('class' => 'tooltip right text')); ?>
                </div>
            </fieldset>
            </div>
            <div id="nameExtra"></div>
        <?php endif; ?>

        <?php if (!$model->isNewRecord) : ?>
            <fieldset class="label_side">
                <?php echo $form->labelEx($model, 'name'); ?>
                <div>
                    <?php echo $form->textField($model, 'name', array('class' => 'tooltip right text')); ?>
                </div>
            </fieldset>

            <fieldset class="label_side">
                <?php echo $form->labelEx($model, 'alias'); ?>
                <div>
                    <?php echo $form->textField($model,'alias',array('class' => 'tooltip right text', 'size' => 30,'maxlength' => 150)); ?>
                </div>
            </fieldset>
        <?php endif; ?>

            <fieldset class="label_side">
                <?php echo $form->labelEx($model,'status'); ?>
                <div>
                    <div class="selector" id="uniform-undefined"><span style="-moz-user-select: none;"><?php echo Yii::t('BackEnd', "-- Select a status --"); ?></span>
                        <?php echo $form->dropdownList($model, 'status', BackendFunctions::getStatusOptions(), array(
                            'class' => 'select_box',
                            'style' => 'opacity:0',
                        )) ?>
                    </div>
                </div>
            </fieldset>

            <fieldset class="label_side">
                <?php echo $form->labelEx($model,'parent_id'); ?>
                <div>
                    <div class="selector" id="uniform-undefined"><span style="-moz-user-select: none;"><?php echo Yii::t('BackEnd', "-- Select a category --"); ?></span>
                        <?php $this->widget("category.widgets.MultiLevelDropDown", array(
                                'name'=>'KitCategory[parent_id]',
                                'data'=>  KitCategory::dumpAllCategoryToArray($model->module),
                                'select'=>$model->parent_id,
                                'noneSelect'=>true,
                                'noneSelectValue'=>0,
                                'noneSelectLabel'=>Yii::t('BackEnd', "-- Select a category --"),
                                'htmlOptions'=>array(
                                    'options'=>array(
                                        $model->id=>array('disabled'=>true),
                                    ),
                                    'class' => 'select_box',
                                    'style' => 'opacity:0',
                                ),
                        ));?>
                    </div>
                </div>
            </fieldset>

            <fieldset class="label_side">
                <?php echo $form->labelEx($model,'module'); ?>
                <div>
                    <div class="selector" id="uniform-undefined"><span style="-moz-user-select: none;"><?php echo Yii::t('BackEnd', "-- Select a module --"); ?></span>
                        <?php echo $form->dropdownList($model, 'module', $model->getModuleList(), array(
                            'class' => 'select_box',
                            'style' => 'opacity:0',
                        )) ?>
                    </div>
                </div>
            </fieldset>

            <fieldset class="label_side">
                <?php echo $form->labelEx($model,'layout'); ?>
                <div>
                    <div class="selector" id="uniform-undefined"><span style="-moz-user-select: none;"><?php echo Yii::t('BackEnd', "-- Select a layout --"); ?></span>
                        <?php echo $form->dropdownList($model, 'layout', $model->getLayoutList(), array(
                            'class' => 'select_box',
                            'style' => 'opacity:0',
                        )) ?>
                    </div>
                </div>
            </fieldset>

            <div class="button_bar clearfix">
                <button class="dark green close_dialog">
                    <div class="ui-icon ui-icon-check"></div>
                    <span>Submit</span>
                </button>
<!--                <button class="dark red close_dialog">
                    <div class="ui-icon ui-icon-closethick"></div>
                    <span>Cancel</span>
                </button>-->
            </div>
        </div>
    </div>


<?php /*
<div class="form <?php if($shortForm) echo "short" ?>">

    <div class="row">
        <?php var_dump($model->getModuleList()); ?>
		<?php echo $form->labelEx($model,'module'); ?>
        <?php echo $form->dropdownList($model, 'module', $model->getModuleList()) ?>
		<?php echo $form->error($model,'module'); ?>
	</div>

    <?php if(!$shortForm):?>
	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'layout'); ?>
		<?php echo $form->textField($model,'layout',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'layout'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
		<?php echo $form->error($model,'updated_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sorder'); ?>
		<?php echo $form->textField($model,'sorder',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'sorder'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>
    <?php endif;?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>


</div><!-- form -->
*/ ?>

<?php $this->endWidget(); ?>