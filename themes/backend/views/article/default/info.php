<?php
$this->breadcrumbs = array(
    'Bài viết' => $this->createUrl('/article/default/index'),
    'Xem thông tin thống kê',
);
?>


<div class="box grid_16">
    <h2 class="box_head">Form Elements</h2>
    <a href="#" class="grabber">&nbsp;</a>
    <a href="#" class="toggle">&nbsp;</a>
    <div class="toggle_container">
        <div class="block">
            <h2 class="section">Text Fields</h2>

            <div class="columns clearfix">
                <div class="col_50">
                    <fieldset>
                        <?php echo CHtml::activeLabelEx($model, 'creator'); ?>
                        <div>
                            <?php if (!empty($model->creatorUser)) echo $model->creatorUser->username; ?>
                        </div>
                    </fieldset>
                </div>
                <div class="col_50">
                    <fieldset>
                        <?php echo CHtml::activeLabelEx($model, 'created_time'); ?>
                        <div>
                            <?php echo $model->created_time; ?>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="columns clearfix">
                <div class="col_50">
                    <fieldset>
                        <?php echo CHtml::activeLabelEx($model, 'editor'); ?>
                        <div>
                            <?php if (!empty($model->editorUser)) echo $model->editorUser->username; ?>
                        </div>
                    </fieldset>
                </div>
                <div class="col_50">
                    <fieldset>
                        <?php echo CHtml::activeLabelEx($model, 'updated_time'); ?>
                        <div>
                            <?php echo $model->updated_time; ?>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="columns clearfix">
                <div class="col_50">
                    <fieldset>
                        <?php echo CHtml::activeLabelEx($model, 'view_count'); ?>
                        <div>
                            <?php echo $model->view_count; ?>
                        </div>
                    </fieldset>
                </div>
                <div class="col_50">
                    <fieldset>
                        <label>Số lần viếng thăm từ công cụ tìm kiếm</label>
                        <div>

                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="button_bar clearfix">
                <button class="green dark">
                    <img src="images/icons/small/white/bended_arrow_right.png">
                    <span>Yes</span>
                </button>
                <button class="red dark">
                    <img src="images/icons/small/white/bended_arrow_right.png">
                    <span>No</span>
                </button>
                <button class="grey dark send_right">
                    <img src="images/icons/small/grey/bended_arrow_right.png">
                    <span>Maybe</span>
                </button>
            </div>
        </div>
    </div>
</div>

<div style="margin-top: 10px;"></div>
        <div class="box grid_16">
            <h2 class="box_head">Line Graph</h2>
            <a href="#" class="grabber"></a>
            <a href="#" class="toggle"></a>
            <div class="toggle_container">
                <div class="block">
                    <div class="section">
                        <div id="flot_line" class="flot"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="box grid_16">
            <h2 class="box_head">Point Graph with Pie chart</h2>
            <a href="#" class="grabber"></a>
            <a href="#" class="toggle"></a>
            <div class="toggle_container">
                <div class="block">
                    <div class="columns">
                        <div class="col_66">
                            <div class="section">
                                <div id="flot_points" class="flot"></div>
                            </div>
                        </div>
                        <div class="col_33">
                            <div class="section">
                                <div id="flot_pie_1" class="flot"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box grid_16">
            <h2 class="box_head">Bar Graph</h2>
            <a href="#" class="grabber"></a>
            <a href="#" class="toggle"></a>
            <div class="toggle_container">
                <div class="block">
                    <div class="section">
                        <div id="flot_bar" class="flot"></div>
                    </div>
                </div>
            </div>
        </div>

<?php
    $cs = Yii::app()->getClientScript();
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/flot/excanvas.min.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/flot/jquery.flot.min.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/flot/jquery.flot.resize.min.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/flot/jquery.flot.pie.min.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/flot/jquery.flot.pie.min.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/flot/jquery.flot.pie.resize_update.js');

    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/adminica/adminica_charts.js');
?>
