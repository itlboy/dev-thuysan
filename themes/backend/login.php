<!doctype html public "✰">
<!--[if lt IE 7]> <html lang="en-us" class="no-js ie6"> <![endif]-->
<!--[if IE 7]>    <html lang="en-us" class="no-js ie7"> <![endif]-->
<!--[if IE 8]>    <html lang="en-us" class="no-js ie8"> <![endif]-->
<!--[if IE 9]>    <html lang="en-us" class="no-js ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-us" class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">

<title>Adminica | The Professional Admin Theme</title>
<!-- iPhone, iPad and Android specific settings -->	

<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1;">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
<link rel="apple-touch-icon" href="images/iOS_icon.png">
<link rel="apple-touch-startup-image" href="images/iOS_startup.png">

<!-- Styles -->

<link rel="stylesheet" type="text/css" href="styles/reset.css">
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,700">

<link rel="stylesheet" type="text/css" href="scripts/fancybox/jquery.fancybox-1.3.4.css">
<link rel="stylesheet" type="text/css" href="scripts/tinyeditor/style.css">
<link rel="stylesheet" type="text/css" href="scripts/slidernav/slidernav.css">
<link rel="stylesheet" type="text/css" href="scripts/syntax_highlighter/styles/shCore.css">
<link rel="stylesheet" type="text/css" href="scripts/syntax_highlighter/styles/shThemeDefault.css">
<link rel="stylesheet" type="text/css" href="scripts/uitotop/css/ui.totop.css">
<link rel="stylesheet" type="text/css" href="scripts/fullcalendar/fullcalendar.css">
<link rel="stylesheet" type="text/css" href="scripts/isotope/isotope.css">
<link rel="stylesheet" type="text/css" href="scripts/elfinder/css/elfinder.css">

<link rel="stylesheet" type="text/css" href="scripts/tiptip/tipTip.css">
<link rel="stylesheet" type="text/css" href="scripts/uniform/css/uniform.aristo.css">
<link rel="stylesheet" type="text/css" href="scripts/multiselect/css/ui.multiselect.css">
<link rel="stylesheet" type="text/css" href="scripts/selectbox/jquery.selectBox.css">
<link rel="stylesheet" type="text/css" href="scripts/colorpicker/css/colorpicker.css">
<link rel="stylesheet" type="text/css" href="scripts/uistars/jquery.ui.stars.min.css">

<link rel="stylesheet" type="text/css" href="scripts/themeroller/Aristo.css">

<link rel="stylesheet" type="text/css" href="styles/text.css">
<link rel="stylesheet" type="text/css" href="styles/grid.css">
<link rel="stylesheet" type="text/css" href="styles/main.css">
<link rel="stylesheet" type="text/css" href="styles/theme/theme_base.css">

<!-- Style Switcher  

The following stylesheet links are used by the styleswitcher to allow for dynamically changing how Adminica looks and acts.
Styleswitcher documentation: http://style-switcher.webfactoryltd.com/documentation/

switcher1.php : layout - fluid by default.								(eg. styles/theme/switcher1.php?default=layout_fixed.css)
switcher2.php : header and sidebar positioning - sidebar by default.	(eg. styles/theme/switcher1.php?default=header_top.css)
switcher3.php : colour skin - black/grey by default.					(eg. styles/theme/switcher1.php?default=theme_red.css)
switcher4.php : background image - dark boxes by default.				(eg. styles/theme/switcher1.php?default=bg_honeycomb.css)
switcher5.php : controls the theme - dark by default.					(eg. styles/theme/switcher1.php?default=theme_light.css)

-->

<link rel="stylesheet" type="text/css" href="styles/theme/switcher.css" media="screen">
<!--		<link rel="stylesheet" type="text/css" href="styles/theme/switcher1.php?default=layout_fixed.css" media="screen" > -->
<link rel="stylesheet" type="text/css" href="styles/theme/layout_fixed.css" media="screen" > 
<!--		<link rel="stylesheet" type="text/css" href="styles/theme/switcher2.php?default=switcher.css" media="screen" >-->
<link rel="stylesheet" type="text/css" href="styles/theme/theme_blue.css" media="screen" >
<link rel="stylesheet" type="text/css" href="styles/theme/bg_wood.css" media="screen" >
<!--		<link rel="stylesheet" type="text/css" href="styles/theme/switcher5.php?default=switcher.css" media="screen" >-->

<link rel="stylesheet" type="text/css" href="styles/colours.css">
<link rel="stylesheet" type="text/css" href="styles/ie.css">

<!-- Scripts -->

<!-- Load JQuery -->		
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<!-- Load JQuery UI -->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/jquery-ui.min.js" type="text/javascript"></script>

<!-- Global -->
<script src="scripts/touchPunch/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
<script src="scripts/uitotop/js/jquery.ui.totop.js" type="text/javascript"></script>

<!-- Forms -->
<script src="scripts/uniform/jquery.uniform.min.js"></script>
<script src="scripts/autogrow/jquery.autogrowtextarea.js"></script>
<script src="scripts/multiselect/js/ui.multiselect.js"></script>
<script src="scripts/selectbox/jquery.selectBox.min.js"></script>
<script src="scripts/timepicker/jquery.timepicker.js"></script>
<script src="scripts/colorpicker/js/colorpicker.js"></script>
<script src="scripts/uistars/jquery.ui.stars.min.js"></script>
<script src="scripts/tiptip/jquery.tipTip.minified.js"></script>
<script src="scripts/validation/jquery.validate.min.js" type="text/javascript"></script>		

<!-- Configuration Script -->
<script type="text/javascript" src="scripts/adminica/adminica_ui.js"></script>
<script type="text/javascript" src="scripts/adminica/adminica_forms.js"></script>
<script type="text/javascript" src="scripts/adminica/adminica_mobile.js"></script>

<!-- Live Load (remove after development) -->
<script>//document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>

</head>
<body>		<div id="wrapper">	
    <div class="isolate">
        <div id="login_box" class="center" style="display:none;">
            <div class="main_container clearfix">
                <div class="box">
                    <div class="block">
                        <div class="section">
                            <div class="alert dismissible alert_light">
                                <img width="24" height="24" src="images/icons/small/grey/locked.png">
                                <strong>Welcome to Adminica.</strong> Please enter your details to login.
                            </div>
                        </div>	
                        <form action="index.php" class="validate_form">
                            <fieldset class="label_side">
                                <label for="username_field">Username<span>or email address</span></label>
                                <div>
                                    <input type="text" id="username_field" class="required">
                                </div>
                            </fieldset>						
                            <fieldset class="label_side">
                                <label for="password_field">Password<span><a href="#">Do you remember?</a></span></label>
                                <div>
                                    <input type="password" id="password_field" class="required">
                                </div>
                            </fieldset>
                            <fieldset class="no_label">																											
                                <div style="">
                                    <div class="slider_unlock" title="Slide to Login"></div>
                                    <button type="submit" style="display:none"></button>
                                </div>
                            </fieldset>
                        </form>	
                    </div>
                </div>
                <a href="index.php" id="login_logo"><span>Adminica</span></a>
                <button data-dialog="dialog_register" class="on_dark dark dialog_button" style="float:right; margin:-30px 0 0 0;">
                    <img src="images/icons/small/white/user.png">
                    <span>Not Registered ?</span>
                </button>
            </div>
            <div class="main_container clearfix" style="display:none;">
                <div class="box">
                    <div class="block">
                        <div class="section">
                            <div class="alert dismissible alert_light">
                                <img width="24" height="24" src="images/icons/small/grey/locked.png">
                                <strong>Welcome to Adminica.</strong> Please enter your details to login.
                            </div>
                        </div>	
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="display_none">						
    <div id="dialog_register" class="dialog_content no_dialog_titlebar wide" title="Register for Adminica">
        <div class="block">
            <div class="section">
                <h2>Registration Form</h2>
            </div>
            <div class="columns clearfix">
                <div class="col_50">							
                    <fieldset class="label_top">
                        <label for="text_field_inline">Username<span>Between 5 and 20 characters</span></label>
                        <div>
                            <input type="text">
                        </div>
                    </fieldset>	
                </div>
                <div class="col_50">										
                    <fieldset class="label_top">
                        <label for="text_field_inline">Username again…<span>so we know you're human</span></label>
                        <div>
                            <input type="text">
                        </div>
                    </fieldset>
                </div>	
            </div>
            <div class="columns clearfix">
                <div class="col_50">							
                    <fieldset class="label_top">
                        <label for="text_field_inline">Password<span>Between 5 and 20 characters</span></label>
                        <div>
                            <input type="text">
                        </div>
                    </fieldset>	
                </div>
                <div class="col_50">										
                    <fieldset class="label_top">
                        <label for="text_field_inline">Repeat Password again…</label>
                        <div>
                            <input type="text">
                        </div>
                    </fieldset>
                </div>			
            </div>

            <fieldset class="label_side">
                <label>Permission</label>
                <div class="uniform inline clearfix">
                    <label for="agree_1"><input type="checkbox" name="agree_1" value="yes" id="agree_1"/>I agree with the terms and conditions</label>
                </div>
            </fieldset>	



            <div class="button_bar clearfix">
                <button class="dark blue no_margin_bottom link_button" data-link="index.php">
                    <div class="ui-icon ui-icon-check"></div>
                    <span>Register</span>
                </button>
                <button class="light send_right close_dialog">
                    <div class="ui-icon ui-icon-closethick"></div>
                    <span>Cancel</span>
                </button>
            </div>
        </div>
    </div>
</div> 
<script type="text/javascript">
    $(".validate_form").validate();
</script>

<div id="loading_overlay">
    <div class="loading_message round_bottom">
        <img src="images/loading.gif" alt="loading" />
    </div>
</div>


<div id="template_options" class="clearfix">
    <h3><img src="images/adminica.png" alt="Adminica"></h3>
    <div class="layout_size round_all">
        <label>Layout:</label>
        <a href="styles/theme/switcher1.php?style=switcher.css">Fluid</a><span>|</span>
        <a href="styles/theme/switcher1.php?style=layout_fixed.css">Fixed</a><span>
    </div>
    <div class="layout_position round_all">
        <label class="display_none">Header: </label>
        <a href="styles/theme/switcher2.php?style=switcher.css">Side</a><span>|</span>
        <a href="styles/theme/switcher2.php?style=header_top.css">Top</a><span>|</span>
        <a href="styles/theme/switcher2.php?style=header_slideout.css">Slide</a>
    </div>
    <div class="layout_position round_all">
        <label>Theme: </label>
        <a href="styles/theme/switcher.php?style=multiple&switcher5.php=switcher.css&switcher4.php=switcher.css">Dark</a><span>|</span>
        <a href="styles/theme/switcher.php?style=multiple&switcher5.php=theme_light.css&switcher4.php=switcher.css">Light</a>
    </div>
    <div class="theme_colour round_all">
        <label class="display_none">Colour:</label>
        <a class="black" href="styles/theme/switcher3.php?style=switcher.css"><span>Black</span></a>
        <a class="blue" href="styles/theme/switcher3.php?style=theme_blue.css"><span>Blue</span></a>
        <a class="navy" href="styles/theme/switcher3.php?style=theme_navy.css"><span>Navy</span></a>
        <a class="red" href="styles/theme/switcher3.php?style=theme_red.css"><span>Red</span></a>
        <a class="green" href="styles/theme/switcher3.php?style=theme_green.css"><span>Green</span></a>
        <a class="magenta" href="styles/theme/switcher3.php?style=theme_magenta.css"><span>Magenta</span></a>
        <a class="orange" href="styles/theme/switcher3.php?style=theme_brown.css"><span>Brown</span></a>
    </div>
    <div class="theme_background round_all" id="bg_dark">
        <label>BGs:</label>
        <a href="styles/theme/switcher4.php?style=switcher.css">Boxes</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_punched.css">Punched</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_honeycomb.css">Honeycomb</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_wood.css">Wood</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_dark_wood.css">Timber</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_noise.css">Noise</a>
    </div>
    <div class="theme_background round_all" id="bg_light">
        <label>BGs:</label>
        <a href="styles/theme/switcher4.php?style=switcher.css">Silver</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_white_wood.css">Wood</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_squares.css">Squares</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_noise_zero.css">Noise</a><span>|</span>
        <a href="styles/theme/switcher4.php?style=bg_stripes.css">Stripes</a>
    </div>
</div>

</body>
</html>