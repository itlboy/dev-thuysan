<?php

//if ($_SERVER['HTTP_HOST'] == "baothuysan.dev.nhanh.cf") {
//    $_SERVER['HTTP_HOST'] = "thuysanvietnam.com.vn";
//}
//$host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
//if ($host == 'phimle.vn') die('website dang nang cap!');

date_default_timezone_set('Asia/Ho_Chi_Minh');

// change the following paths if necessary
define('APP_PATH', dirname(__FILE__));

$yii = dirname(__FILE__) . '/framework/yii.php';
$config = dirname(__FILE__) . '/protected/config/frontend.php';

error_reporting(E_ALL & ~(E_STRICT | E_NOTICE));

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG', true);
////show profiler
//defined('YII_DEBUG_SHOW_PROFILER') or define('YII_DEBUG_SHOW_PROFILER',true);
////enable profiling
//defined('YII_DEBUG_PROFILING') or define('YII_DEBUG_PROFILING',true);
////trace level
//defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
//execution time
//defined('YII_DEBUG_DISPLAY_TIME') or define('YII_DEBUG_DISPLAY_TIME',false);

require_once($yii);
Yii::createWebApplication($config)->run();
