<?php

/**
 * Tính tổng 4 loại (view, like, share, comment) để đẩy vào stats của bảng account. Đồng thời tính rank_point và rank
 * Có thể bỏ AccountPinCommand, AccountRankCommand
 */
class PinUpdateRankCommand extends CConsoleCommand {

    private $_command;
    private $_limit = 100;
    private $_exec = 0;
    private $_userInPin = array();

    public function init() {
        if ($this->_command == NULL) {
            $_db = Yii::app()->db;
            $this->_command = $_db->createCommand();
        }
    }

    public function run() {
        try {
            // Cập nhật thông tin count[post|view|like|share|comment], rank_point cho bảng let_kit_account
            $dataListStats = $this->_getAccountStats();     // Lấy danh sách các Account cần update
            echo "---------- Cap nhat Stats ----------\n";
            echo count($dataListStats) . '::';            // Hiển thị tổng số Account cần update
            $this->_updateAccountStats($dataListStats);   // Update bảng Account

            // Cập nhật thông tin rank cho bảng let_kit_account
            $dataListRank = $this->_getAccountRank();
            echo "\n---------- Cap nhat Rank ----------\n";
            echo count($dataListRank) . '::';
            $this->_updateAccountRank($dataListRank);
            
            $this->_updateAccountNotRank();

            Yii::log('------------------ END Update Stats, Rank Account ------------------', 'info');
        } catch (Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
    }

    // Lấy danh sách các thành viên đang hoạt động với Pin, và số lượng view, like, share, comment
    private function _getAccountStats() {
        $dataList = $this->_command->select('p.creator, (SELECT count(p.id)) AS post_total, (SELECT sum(p.view_count)) AS view_total, (SELECT sum(p.like_count)) AS like_total, (SELECT sum(p.share_count)) AS share_total, (SELECT sum(p.comment_count)) AS comment_total')
                ->from('let_kit_pin p')
                ->group('p.creator')
                ->queryAll();

        $this->_command->reset();
        return $dataList;
    }

    private function _updateAccountStats($dataList) {
        if (!is_array($dataList) OR empty($dataList))
            return;

        foreach ($dataList as $key => $data) {
            $dataUpdate = array();
            $dataUpdate['view_total'] = $data['view_total'];
            $dataUpdate['like_total'] = $data['like_total'];
            $dataUpdate['share_total'] = $data['share_total'];
            $dataUpdate['post_total'] = $data['post_total'];
            $dataUpdate['comment_total'] = $data['comment_total'];

            $rank_point = intval($data['view_total'] + (2 * $data['like_total']) + (3 * $data['comment_total']) + (4 * $data['share_total']));
            if ($rank_point > 0) {
                $dataUpdate['rank_point'] = $rank_point;
            }
            
            // Lấy thông tin các user hoạt động trong Pin
            $this->_userInPin[] = $data['creator'];

            //update bảng Account
            $this->_command->update('let_kit_account', $dataUpdate, 'id = :id', array(':id' => $data['creator']));
            $this->_command->reset();
            echo $key + 1 . '.';
        }
    }

    // Lấy danh sách các thành viên cần cập nhật Rank
    private function _getAccountRank() {
        $dataList = $this->_command->select('id, rank_point')
                ->from('let_kit_account')
                ->where('rank_point > 0')
                ->order('rank_point DESC')
                ->queryAll();

        $this->_command->reset();
        return $dataList;
    }

    private function _updateAccountRank($dataList) {
        if (!is_array($dataList) OR empty($dataList))
            return;

        foreach ($dataList as $key => $data) {
            $this->_command->update('let_kit_account', array(
                'rank' => $key + 1
            ), 'id = :id', array(':id' => $data['id']));
            echo $key + 1 . '.';
            $this->_command->reset();
        }
    }

    // Cập nhật lại rank, rank_point của những user ko hoạt động trong Pin
    private function _updateAccountNotRank() {
        if (!empty($this->_userInPin)) {
            $this->_command->update('let_kit_account', array(
                'rank_point' => NULL,
                'rank' => NULL
            ), 'id NOT IN (' . implode(',', $this->_userInPin) . ')');
        }
    }

}