<?php

/**
 * Cập nhật view_count của các user_id được chỉ định
 */
class PinUpdateViewFakeCommand extends CConsoleCommand {

    private $_command;

    public function init() {
        if ($this->_command == NULL) {
            $_db = Yii::app()->db;
            $this->_command = $_db->createCommand();
        }
    }

    // Danh sách các users truyền vào theo dạng: PinUpdateViewFake $user1, $user2 [, $userN]
    // Ví dụ muốn cập nhật Pin của các user 1, 2 chạy command: PinUpdateViewFake 1 2
    public function run($users) {
        try {
            if (empty($users))
                die("Vui long nhap user de Update\nVi du: PinUpdateViewFake 1 2");

            // Lấy danh sách tất cả các pin của những user này
            $dataList = $this->_getData($users);

            // Hiển thị số lượng pin cần update
            echo count($dataList) . '::';
            foreach ($dataList as $key => $data) {
                if ($this->_updateStats($data['id'], $data['view_total'])) {
                    echo $key + 1 . '.';
                }
            }

            Yii::log('------------------ END Update View Pin Fake ------------------', 'info');
        } catch (Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
    }

    private function _getData($users = array()) {
        if (empty($users))
            return;

        // Lấy danh sách các Pin cần update
        $dataList = $this->_command->select('p.id, s.view_total')
                ->from('let_kit_pin p')
                ->leftJoin('let_kit_stats s', 's.item_id = p.id')
                ->where('s.module = "pin" AND p.creator IN (' . implode(', ', $users) . ')')
                ->order('p.id ASC')
                ->queryAll();

        $this->_command->reset();
        return $dataList;
    }

    // Cập nhật bảng Pin
    private function _updateStats($id, $value) {
        $valueUpdate = $value + rand(0, 100);
        $updated = $this->_command->update('let_kit_stats', array(
            'view_total' => $valueUpdate,
            'view_inyear' => $valueUpdate
        ), 'module = "pin" AND item_id = :itemId', array(':itemId' => $id));
        $this->_command->reset();
        return ($updated) ? TRUE : FALSE;
    }
}
