<?php

class FilmVipCommand extends CConsoleCommand
{
    public function actionIndex() {
        try {
            $_db = Yii::app()->db;
            $command = $_db->createCommand();
			
			// Lay danh sach nhung tap phim VIP chua xu ly
            $items = $command->select('f.title, t.id, t.link, t.item_id')
                    ->from('let_kit_film_episode t')
                    ->join('let_kit_film f', 'f.id = t.item_id')
                    ->where('t.vip = 1')
                    ->queryAll();
            
			// Lay duong dan cac phien ban cua tap phim. Duong dan goc dang abc.mp4
            $path_source = "e:\\storage\\";
            $path_storage = "e:\\storage\\";
            
//user/nguago/web/abc.mp4
//user/nguago/mobile/abc.mp4
//user/nguago/tablet/abc.mp4
//
//LET NguaGo: 3 file nguồn nhá
//LET NguaGo: 3 file này
//LET NguaGo: bắn về storage như sau:
//LET NguaGo: /storage/film/web/2012/12/11/abc.mp4
//LET NguaGo: /storage/film/mobile/2012/12/11/abc.mp4
//LET NguaGo: /storage/film/tablet/2012/12/11/abc.mp4
//LET NguaGo: sau đó lưu vào database dạng: 2012/12/11/abc.mp4
            

//LET NguaGo: Common::mkPath <==
            
			// Danh sach phim co tap phim VIP
			$filmList = array();
			
			// Danh sach danh muc tuong ung voi tung phien ban
            $sourceDirList = array('/web/', '/mobile/', '/tablet/');
			
            $date = date('Y/m/d/');
            foreach ($items as $item) { // Xu ly tung tap phim             
		        $moved = FALSE; // Kiem tra xem da co file nao duoc move hay chua
                $sourceLink = $item['link']; // Duong link cua uploader
                $storageFilename = Common::convertStringToUrl($item['title']) . '.' . pathinfo($sourceLink, PATHINFO_EXTENSION); // Tao ten cua file
                $storageFileSave = $date . $storageFilename; // Duong dan tuong doi cua file, cai nay se duoc luu vao database
                foreach ($sourceDirList as $sourceDir) { // Xu ly voi tung phien ban (web, mobile, tablet)
                    $pathFileSource = $path_source . str_replace('/web/', $sourceDir, $sourceLink);
                    $pathFileStorage = $path_storage . 'film' . $sourceDir . $date . $storageFilename;

					// Kiem tra xem co ton tai file source hay khong, neu co thi move sang storage
                    if (file_exists($pathFileSource) AND is_file($pathFileSource)) {
                        Common::mkPath($sourceDir . $date, $path_storage . 'film');
                        
                        if (copy($pathFileSource, $pathFileStorage)) {
                            unlink($pathFileSource);
                            $moved = TRUE;
                        }                                  
                    }
                }
                
                if ($moved) {
                    $command->update('let_kit_film_episode', array(
                        'vip' => 2,
                        'link' => $storageFileSave,
                    ), 'id = :id', array(':id' => $item['id']));
					$filmList[] = $item['item_id'];
                }
            }
						
			// Update truong 'vip' cua bang let_kit_film WHERE id IN ('implode($filmList)') <== e xu ly cai nay nhe. Muc dich la tu dong biet nhung phim nao co truong VIP de doi cai truong do sang gia tri 1.
            $filmList = array_unique($filmList);
            $command->update('let_kit_film', array(
                'vip' => 1,
            ), 'id IN ('.implode(', ', $filmList).')');

				
            
//            Yii::log('------------------ END Convert Article ------------------', 'info');
			
        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }

    }
    
}