<?php
class FacebookCommand extends CConsoleCommand{

    public function actionIndex(){
        try{
            
        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
    }

    /**
     * Update lai nhung pin co su thay doi
     * @param type $from_day
     * @param type $to_day
     * @param type $limit 0: Khong gioi han
     */
    public function actionPinUpdate($from_day = 0, $to_day = 1, $limit = 0){
        try {
            if ($limit <= 0) $limit = 10000000;
            $from = date('Y-m-d', strtotime("-".$to_day." day"));
            $to = date('Y-m-d', strtotime("-".$from_day." day"));

            $rows = Yii::app()->db->createCommand()
                ->select('id, title, facebook_id, like_count, comment_count, share_count, updated_time')
                ->from('let_kit_pin')
                ->where("updated_time BETWEEN '".$from." 00:00:00' AND '".$to." 23:59:59'")
                ->limit($limit)
                ->order('updated_time DESC, id DESC')
                ->queryAll();
            
            echo 'Bat dau update pin '.$from.' -> '.$to.': ('.count($rows).')' . "\n";
            foreach ($rows as $key => $row) {
                $urlTitle = Common::convertStringToUrl($row['title']);
                $url = 'http://cuoi.let.vn/' . $urlTitle . '-pin-' . $row['id'] . '.let';
                
                // Get thong tin tu facebook
                $stats = $this->_getStats($url);
                $like_count = (int)  letArray::get($stats[0], 'total_count', 0);
                $share_count = (int)  letArray::get($stats[0], 'share_count', 0);
                $comment_count = (int)  letArray::get($stats[0], 'comment_count', 0);
                
				// Get ra facebook id
//				if (!isset($row['facebook_id']) OR empty($row['facebook_id'])) {
//                	$facebookId = $this->_getId($url);
//					if (preg_match("/http/i", $facebookId))
//						$facebookId = NULL;
//				} else
//					$facebookId = $row['facebook_id'];
				
				$facebookId = $row['facebook_id'];
				
                // Neu thong tin lay ve khong giong voi trong database thi update lai
                if ($like_count !== (int)$row['like_count'] OR $share_count !== (int)$row['share_count'] OR $comment_count !== (int)$row['comment_count'] OR $facebookId !== $row['facebook_id']) {
                    Yii::app()->db->createCommand()->update('let_kit_pin', array(
						'facebook_id' => $facebookId,
                        'like_count' => $like_count,
                        'share_count' => $share_count,
                        'comment_count' => $comment_count,
                        'updated_time' => Common::getNow(),
                    ), 'id='.$row['id']);

                    Yii::app()->cache->delete(md5('pin_details_' . $row['id'])); // Delete cache
                    echo $msg = 'Update Pin '.($key + 1).': ID:' . $row['id'] . '|facebookId:'.$facebookId.'|Like:'.$like_count.'|Share:'.$share_count.'|Comment:'.$comment_count.' - ' . Common::convertStringToUrl($row['title']) . "\n";
                    Yii::log($msg, 'info');
                } else echo 'Update Pin '.($key + 1).': ID:' . $row['id'] . '|facebookId:'.$facebookId.'|Like:'.$like_count.'|Share:'.$share_count.'|Comment:'.$comment_count. "\n";
            }
            
        } catch (Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
    }

    /**
     * Get thong tin thong ke URL tu facebook
     * @param type $link
     * @return type 
     */
    public function _getStats($link){
        $fql  = "SELECT url, normalized_url, share_count, like_count, comment_count, total_count, commentsbox_count, comments_fbid, click_count FROM link_stat WHERE url = '".$link."'";
        $apifql="http://api.facebook.com/method/fql.query?format=json&query=".urlencode($fql);
        $content = @file_get_contents($apifql);

        return json_decode($content, TRUE);
    }

    /**
     * Get ID cua URL tu facebook
     * @param type $link Duong dan can get ID
     * @return type Tra ve ket qua la ID cua URL
     */
    public function _getId($link){
        $apifql="https://graph.facebook.com/?ids=" . $link;
        $content=file_get_contents($apifql);

        $info = json_decode($content, TRUE);
		if (isset($info[$link]['id'])) {
			if (is_float($info[$link]['id']))
				$info[$link]['id'] = number_format($info[$link]['id'],0,'.','');
			return $info[$link]['id'];
		}
		else return NULL;
    }
}