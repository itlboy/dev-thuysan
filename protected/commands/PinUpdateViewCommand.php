<?php

/**
 * Cập nhật lại thông tin số lượt xem giữa 2 bảng pin - stats
 * Có thể bỏ PinViewStatsCommand
 */
class PinUpdateViewCommand extends CConsoleCommand {

    private $_command;
    private $_limit = 100;
    private $_exec = 0;

    public function init() {
        if ($this->_command == NULL) {
            $_db = Yii::app()->db;
            $this->_command = $_db->createCommand();
        }
    }

    public function run() {
        try {
            // Lấy danh sách các Pin cần update
            $dataList = $this->_getData();

            // Lọc ra danh sách các Pin cần update cho vào 2 mảng khác nhau
            $listKitPin = array();
            $listKitStats = array();

            foreach ($dataList as $data) {
                $viewPin = $data['view_count'];
                $viewStats = $data['view_total'];
                if ($viewStats > $viewPin) {
                    $listKitPin[] = array('id' => $data['id'], 'value' => $viewStats);
                } else if ($viewStats < $viewPin) {
                    $listKitStats[] = array('id' => $data['id'], 'value' => $viewPin);
                }
            }
            
            // Hiển thị tổng số Pin cần update
            echo (count($listKitPin) + count($listKitStats)) . '::';
            $this->_updateProcess($listKitPin, '_updatePin');
            $this->_updateProcess($listKitStats, '_updateStats');

            Yii::log('------------------ END Update View Pin vs Stats ------------------', 'info');
        } catch (Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
    }

    /**
     * Xử lý update thông tin vào database
     * @param type $listData    : array mang giá trị $id => $value
     * @param type $methodName  : gọi hàm cần update (_updatePin, _updateStats)
     */
    private function _updateProcess($listData, $methodName) {
        if (!is_array($listData) OR empty($listData))
            return;
        
        // Phân trang số rows trả về từ bảng pin
        $pages = array_chunk($listData, $this->_limit);

        // Duyệt qua từng trang
        foreach ($pages as $page) {
            $updateCount = 0;
            foreach ($page as $data) {
                call_user_func_array(array($this, $methodName), array($data['id'], $data['value']));
                $updateCount += 1;
            }

            // Hiển thị trạng thái thực thi. Đã chạy được bao nhiêu row
            $this->_exec = $this->_exec + $updateCount;
            echo $this->_exec . '.';
        }
    }

    private function _getData() {
        // Lấy danh sách các Pin cần update
        $dataList = $this->_command->select('p.id, p.view_count, s.view_total')
                ->from('let_kit_pin p')
                ->leftJoin('let_kit_stats s', 's.item_id = p.id')
                ->where('s.module = "pin"')
                ->queryAll();

        $this->_command->reset();
        return $dataList;
    }

    // Cập nhật bảng Pin
    private function _updatePin($id, $value) {
        $updated = $this->_command->update('let_kit_pin', array(
            'view_count' => $value
        ), 'id = :id', array(':id' => $id));
        $this->_command->reset();
        return ($updated) ? TRUE : FALSE;
    }

    // Cập nhật bảng Stats
    private function _updateStats($id, $value) {
        $updated = $this->_command->update('let_kit_stats', array(
            'view_total' => $value
        ), 'module = "pin" AND item_id = :itemId', array(':itemId' => $id));
        $this->_command->reset();
        return ($updated) ? TRUE : FALSE;
    }

}