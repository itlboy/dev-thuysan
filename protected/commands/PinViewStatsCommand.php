<?php

class PinViewStatsCommand extends CConsoleCommand
{
    public function run() {
        try {
            $_db = Yii::app()->db;
            $command = $_db->createCommand();

            $dataList = $command->select('p.id, p.view_count, s.view_total')
                    ->from('let_kit_pin p')
                    ->leftJoin('let_kit_stats s', 's.item_id = p.id')
                    ->where('s.module = "pin"')
                    ->queryAll();
            
            echo count($dataList) . '::';
            $command->reset();

            foreach ($dataList as $key => $data) {
                $viewPin = $data['view_count'];
                $viewStats = $data['view_total'];
                if ($viewStats > $viewPin) {
                    //update bảng Pin
                    $command->update('let_kit_pin', array(
                        'view_count' => $viewStats
                    ), 'id = :id', array(':id' => $data['id']));                    
                } else {
                    //update bảng Stats
                    $command->update('let_kit_stats', array(
                        'view_total' => $viewPin
                    ), 'module = "pin" AND item_id = :itemId', array(':itemId' => $data['id']));
                }
                echo $key . '::';
                $command->reset();
            }

            Yii::log('------------------ END Update View Pin vs Stats ------------------', 'info');

        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }

    }

}