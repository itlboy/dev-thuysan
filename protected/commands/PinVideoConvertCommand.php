<?php

/**
 * Chuyển tất cả các Pin có type=video sang category id=90
 * 
 * Tìm tất cả các Pin có type=video
 * Xóa tất các các Pin trong bảng category
 * Nạp lại category_id=90
 */
class PinVideoConvertCommand extends CConsoleCommand
{
    public function run() {
        try {
            $_db = Yii::app()->db;
            $command = $_db->createCommand();
            
            $dataList = $command->select('id')
                    ->from('let_kit_pin')
                    ->where('type = "video"')
                    ->queryAll();
            $command->reset();
            
            $pinIds = array();
            foreach ($dataList as $data) { 
                $pinIds[] = $data['id'];
            }
            
            $command->delete('let_kit_pin_category', 'pin_id IN (' . implode(', ', $pinIds) . ')');
            $command->reset();
            
            echo count($pinIds) . '::';
            foreach ($pinIds as $key => $pinId) { 
                $command->insert('let_kit_pin_category', array(
                    'pin_id' => $pinId,
                    'category_id' => 90
                ));
                echo $key + 1 . '::';
                $command->reset();
            }

            Yii::log('------------------ END Update Pin Category to 90 ------------------', 'info');

        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }

    }

}