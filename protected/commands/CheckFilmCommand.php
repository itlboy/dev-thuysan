<?php
Yii::import('application.vendors.libs.videoInfo');
Yii::import('application.vendors.libs.grabVideo');
class CheckFilmCommand extends CConsoleCommand{

    public function actionIndex(){
        try{
            $_db = Yii::app()->db;
            $command = $_db->CreateCommand();
            $listItem = $command->select('*')->from('let_kit_film_episode')->where('server = "youtube" OR checked_time <= NOW() - INTERVAL 7 DAY')->queryAll();
            $command->reset();
            foreach($listItem as $key => $value){
                if($key < 3){
                    $datalink = videoInfo::youtube($value['link']);
                    $date = date('Y-m-d H:i:s');
                    $isdie = 0;
                    $mess = 'good';
                    //Kiem tra neu link con chay thi is_die = 0  nguoc lai link chet is_die = 1
                    if(empty($datalink)){
                        $isdie = 1;
                        $mess = 'die';
                    }
                    //Update vao csdl
                    $update  = $command->update('let_kit_film_episode',array(
                        'is_die' => $isdie,
                        'checked_time' => $date),
                        'id = '.$value['id'].' AND server="youtube"');
                    $command->reset();
                    echo 'Check Link for Url = '.$value['link'].' Link : '.$mess."\n";
                    Yii::log('Check Link for Url = '.$value['link'], 'info');
                }
            }
            Yii::log('------------------ END Check Link Youtube ------------------', 'info');
        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
    }
}