<?php
Yii::import('application.modules.film.models.KitFilm');
Yii::import('application.vendors.libs.imdb');
Yii::import('application.components.common.Common');
class ImdbCommand extends CConsoleCommand{

    public function actionIndex(){
        try{
			if ($this->_checkFoldel('film') == FALSE) return FALSE;
			
            $db = Yii::app()->db;
            $comm = $db->createCommand();
            $comm->select('*')
                ->from('let_kit_film')
                ->where('imdb_grab = 1 AND imdb_id<>"" ')
                ->order('updated_time ASC');
//                ->limit(3);
            $data = $comm->queryAll();
            if(empty($data))
                return FALSE;

            $count = count($data);

            $comm->reset();
            $imdb = new imdb();

            foreach($data as $key => $value){

//                if($key == 51){
                    if(!empty($value['imdb_id'])){
                        $imdb_id = $value['imdb_id'];
                        $imdb_id = preg_replace('/tt/i','',$imdb_id);
                        $imdb_id = preg_replace('/\//i','',$imdb_id);
//                        $imdb_id = '1371111';
//                        echo $imdb_id;
                        $url = 'http://www.imdb.com/title/tt'.$imdb_id.'/';

                        $info = $imdb->getInfo($url);

//                        echo $info['duration']."\n";
                        if(!empty($info['image'])){
                            $info['image'] = $this->_createThumb('film',$info['image'],$value['title']);
                        }
                        @$comm->update('let_kit_film',array(
                            'image' => !empty($info['image']) ? $info['image'] : $value['image'],
//                            'title_english' => !empty($info['title']) ? $info['title'] : $value['title'],
                            'imdb_point' => !empty($info['rating']) ? $info['rating'] : NULL,
                            'imdb_grab' => 0,
                            'actor' => !empty($info['stars']) ? $info['stars'] : $value['actor'],
                            'director' => !empty($info['writer']) ? $info['writer'] : $value['director'],
                            'duration' => !empty($info['duration']) ? $info['duration'] : $value['duration'],
                            'year' => !empty($info['year']) ? $info['year'] : $value['year'],
                            'country' => !empty($info['country']) ? $info['country'] : $value['country'],
                            'updated_time' => date('Y-m-d H:i:s'),
                        ),'id = '.$value['id']);

                        echo $count . ' - Update "'.$value['id'].': '.$value['title_english'].'" IMDB:'.$imdb_id.' = success'."\n";
                        echo '----------------------------------------------------------------'."\n";
                        Yii::log('Update "'.$value['title_english'].'"', 'info');
                        $count--;
                    }
//                }
            }

        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
    }

    public function _checkFoldel ($module) {
		$date = date('Y/m/d/');

		// Thu muc luu file
		$fileDir = $module . '/origin/' . $date;
		
		// Duong dan file
		$filePath = Common::getImageUploaded($fileDir, 'PATH');

		if(is_dir($filePath))
			return TRUE;
		else
			return FALSE;
	}
    public function _createThumb ($module, $inputImage, $title) {
        try {

            $date = date('Y/m/d/');
            $url = $inputImage;
            $tmpFileInfo = pathinfo($inputImage);
            $inputImage = rand(100000, 999999) . '.' . $tmpFileInfo['extension'];

            // Download image
            $tmpFile = Yii::app()->params['uploadPath'] . '/' . Yii::app()->params['uploadDir'] . '/tmp/' . $inputImage;

            @file_put_contents($tmpFile, @file_get_contents($url));

            // Duong dan file luu o thu muc tam
            //            $tmpFile = Common::getImageUploaded($inputImage, 'PATH');

            // Lay thong tin file tam va tao ra ten file moi
            $fileName = Common::convertStringToUrl($title) . '_' . rand(100000, 999999) . '.' .$tmpFileInfo['extension'];

            // get image config
            $imageConfig = $this->image;
            // Resize
            foreach ($imageConfig as $key => $config) {
                if (in_array($key, array('origin', 'large', 'medium', 'small'))) {

                    // Thu muc luu file
                    $fileDir = $module . '/'.$config['folder'].'/' . $date;

                    // Duong dan file
                    $filePath = Common::getImageUploaded($fileDir . $fileName, 'PATH');

                    // Tao dir theo path neu dir chua ton tai
                    Common::mkPath($fileDir, Common::getImageUploaded('', 'PATH'));

                    if ($config['type'] == 'move') copy($tmpFile, $filePath);
                    elseif ($config['type'] == 'resize') {
                        $image = Yii::app()->image->load($tmpFile);
                        $image->resize($config['width'], $config['height'])->rotate($config['rotate'])->quality($config['quality']);
                        $image->save($filePath);
                    } elseif ($config['type'] == 'crop') {
                    $image = Yii::app()->image->load($tmpFile);

                    // Xac dinh master
                    $size = getimagesize($tmpFile);
                    if (isset($size[0]) AND $size[1]) {
                        $width = $size[0];
                        $height = $size[1];
                        if ($width/letArray::get($config, 'width') < $height/letArray::get($config, 'height'))
                            $master = Image::WIDTH;
                        else
                            $master = Image::HEIGHT;
                    }
                    // END Xac dinh master

                    $image->resize(letArray::get($config, 'width'), letArray::get($config, 'height'), $master)->crop(letArray::get($config, 'width'), letArray::get($config, 'height'))->rotate(letArray::get($config, 'rotate'))->quality(letArray::get($config, 'quality'));
                    $image->save($filePath);
                }
                }
            }
        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
        return $date . $fileName;
    }

    public $image = array(
        'origin' => array(
            'type' => 'move',
            'folder' => 'origin',
        ),
        'large' => array(
            'type' => 'crop',
            'width' => 250,
            'height' => 370,
            'cropWidth' => NULL,
            'cropHeight' => NULL,
            'cropTop' => NULL,
            'cropLeft' => NULL,
            'rotate' => 0, // Xoay anh
            'flip' => NULL, //[integer] Direction. Horizontal = 5, Vertical = 6
            'quality' => 70,
            'folder' => 'large',
        ),
        'medium' => array(
            'type' => 'crop',
            'width' => 130,
            'height' => 185,
            'cropWidth' => NULL,
            'cropHeight' => NULL,
            'cropTop' => NULL,
            'cropLeft' => NULL,
            'rotate' => 0, // Xoay anh
            'flip' => NULL, //[integer] Direction. Horizontal = 5, Vertical = 6
            'quality' => 80,
            'folder' => 'medium',
        ),
        'small' => array(
            'type' => 'crop',
            'width' => 43,
            'height' => 55,
            'cropWidth' => NULL,
            'cropHeight' => NULL,
            'cropTop' => NULL,
            'cropLeft' => NULL,
            'rotate' => 0, // Xoay anh
            'flip' => NULL, //[integer] Direction. Horizontal = 5, Vertical = 6
            'quality' => 100,
            'folder' => 'small',
        ),
    );


}