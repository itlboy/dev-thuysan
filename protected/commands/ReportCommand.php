<?php
Yii::import('application.modules.account.models.KitAccountLevel');
Yii::import('application.modules.account.models.KitAccount');
Yii::import('application.modules.cms.models.KitCmsReport');
Yii::import('application.modules.cms.models.KitCmsConfig');
class ReportCommand extends CConsoleCommand{

    
    public function actionIndex($offset = 30){
        try{

            //
            echo 'Update Chart'."\n";
            KitCmsConfig::updateConfig('chart_module',$offset);
            echo 'Ket Thuc Update Chart'."\n";

            echo 'Update Money va Gold'."\n";
            // update money va gold
            KitCmsConfig::updateCmsCurrency('',0);
            echo 'Ket Thuc Update  Money va Gold'."\n";

        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
    }

    public function actionCurrency(){
        try{
            KitCmsConfig::updateCmsCurrency('',0);
        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
    }

    public function actionUpdateCurrency(){
        try{
            $tables = array(
                'let_kit_currency_card',
                'let_kit_currency_cardfail',
                'let_kit_currency_history',
                'let_kit_currency_sms',
                'let_kit_currency_banking',
            );
            $command = Yii::app()->db->createCommand();
            foreach($tables as $table){
                $command->reset();
                $data = $command->select('*')
                    ->from($table)
                    ->queryAll();

                if(!empty($data)){
                    foreach($data as $item){
                        $command->reset();
                        if(isset($item['status']) AND $item['status'] == 0){
//                            $command->delete($table,'id ='.$item['id']);
//                            echo 'Xoa '.$table.'_Id:'.$item['id']."\n";
//                            Yii::log('Xoa '.$table.'_Id:'.$item['id'], 'info');
                        } else {
                            $update_currency = array();
                            if(isset($item['creator']) AND !empty($item['creator'])){
                                $update_currency['username'] = KitAccount::getField($item['creator'],'username');
                            } elseif(isset($item['user_id']) AND !empty($item['user_id'])){
                                $update_currency['username'] = KitAccount::getField($item['user_id'],'username');
                            }

                            // Gold
                            if(isset($item['money']) AND isset($item['payment'])){
                                $update_currency['gold'] = KitCmsConfig::convertMoneyToGold($item['money'],$item['payment']);
                            }
                            switch($table){
                                case 'let_kit_currency_card':
                                    $update_currency['gold'] = KitCmsConfig::convertMoneyToGold($item['money'],'card');
                                    break;
                                case 'let_kit_currency_sms':
                                    $update_currency['gold'] = KitCmsConfig::convertMoneyToGold($item['money'],'sms');
                                    break;
                            }

                            // money
                            if(isset($item['money']) AND $item['money'] < 0){
                                $update_currency['money'] = NULL;
                            }

                            if(!empty($update_currency['username'])){
                                $command->update($table,$update_currency,'id = '.$item['id']);
                                echo 'Update '.$table.'_username:'.$update_currency['username']."\n";
                                Yii::log('Update '.$table.'_username:'.$update_currency['username'], 'info');;
                            }
                        }
                    }
                }
            }

        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
    }

    public function actionConvertTime(){
        try{
            $modules = KitCmsConfig::getValue('module');
            $attributes = KitCmsReport::model()->attributeNames();

            if(empty($modules)) return FALSE;
            $modules = explode(',',$modules);
            $comm = Yii::app()->db->createCommand();

            foreach($modules as $key => $module){
                echo 'udpate module: '.$module."\n";
                $comm->select('*')
                    ->from('let_kit_'.$module)
                    ->where('created_time LIKE "%1970%"');
                $data  = $comm->queryAll();
                $comm->reset();
                if(!empty($data)){
                    foreach($data as $item){
                        $comm->update('let_kit_'.$module,array(
                            'created_time' => date('2011-m-d H:i:s')
                        ),'id='.$item['id']);
                        $comm->reset();
                        echo ' -----Update ItemId: '.$item['id'].'------------------------------ '."\n";
                    }
                };
                $comm->reset();
            }
        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
    }
}