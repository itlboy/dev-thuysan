<?php
Yii::import('application.modules.account.models.KitAccountLevel');
Yii::import('application.modules.account.models.KitAccount');
Yii::import('application.modules.cms.models.KitCmsReport');
Yii::import('application.modules.cms.models.KitCmsConfig');
class ConvertMoneyCommand extends CConsoleCommand{

    
    public function actionIndex($from_time = NULL, $to_time = NULL){
        try{
            $command = Yii::app()->db->createCommand();

            // bang banking
            $command->select('*')
                ->from('let_kit_currency_banking');
            $data = $command->queryAll();
            foreach($data as $value){
                $username = KitAccount::getField($value['creator'],'username');
                $gold = KitCmsConfig::convertMoneyToGold($value['money'],$value['payment']);
                $command->update('let_kit_currency_banking',array(
                    'gold' => $gold,
                    'username' => $username,
                ),'id ='.$value['id']);
                $command->reset();
                echo 'Update itemId.'.$value['id']."\n";
                Yii::log('Update itemId.'.$value['id'], 'info');
            }

            echo 'ket thuc banking'."\n\n\n";
            // bang sms
            $command->select('*')
                ->from('let_kit_currency_sms');
            $data = $command->queryAll();
            foreach($data as $value){
                $username = KitAccount::getField($value['creator'],'username');
                $gold = KitCmsConfig::convertMoneyToGold($value['money'],'sms');
                $command->update('let_kit_currency_sms',array(
                    'gold' => $gold,
                    'username' => $username,
                ),'id ='.$value['id']);
                $command->reset();
                echo 'Update itemId.'.$value['id']."\n";
                Yii::log('Update itemId.'.$value['id'], 'info');
            }


            echo 'ket thuc SMS'."\n\n\n";

            // bang Card
            $command->select('*')
                ->from('let_kit_currency_card');
            $data = $command->queryAll();
            foreach($data as $value){
                $username = KitAccount::getField($value['user_id'],'username');
                $gold = KitCmsConfig::convertMoneyToGold($value['money'],'card');
                $command->update('let_kit_currency_card',array(
                    'gold' => $gold,
                    'username' => $username,
                ),'id ='.$value['id']);
                $command->reset();
                echo 'Update itemId.'.$value['id']."\n";
                Yii::log('Update itemId.'.$value['id'], 'info');
            }


            echo 'ket thuc card'."\n\n\n";

            // bang History
            $command->select('*')
                ->from('let_kit_currency_history');
            $data = $command->queryAll();
            foreach($data as $value){
                $username = KitAccount::getField($value['user_id'],'username');
                $rate = !empty($value['payment']) ? $value['payment'] : 'default';
                $gold = KitCmsConfig::convertMoneyToGold($value['money'],$rate);
                $command->update('let_kit_currency_history',array(
                    'gold' => $gold,
                    'username' => $username,
                ),'id ='.$value['id']);
                $command->reset();
                echo 'Update itemId.'.$value['id']."\n";
                Yii::log('Update itemId.'.$value['id'], 'info');
            }

            echo 'ket thuc History'."\n\n\n";



        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
    }
}