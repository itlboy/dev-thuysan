<?php

class AccountPinCommand extends CConsoleCommand
{
    public function run() {
        try {
            $_db = Yii::app()->db;
            $command = $_db->createCommand();

            $dataList = $command->select('p.creator, (SELECT count(p.id)) AS post_total, (SELECT sum(p.view_count)) AS view_total, (SELECT sum(p.like_count)) AS like_total, (SELECT sum(p.share_count)) AS share_total, (SELECT count(*) FROM let_kit_comment c WHERE c.creator = p.creator) AS comment_total')
                    ->from('let_kit_pin p')
                    ->group('p.creator')
                    ->queryAll();

            echo count($dataList) . '::';
            $command->reset();

            foreach ($dataList as $key => $data) {
                $dataUpdate = array();
                $dataUpdate['view_total'] = $data['view_total'];
                $dataUpdate['like_total'] = $data['like_total'];
                $dataUpdate['share_total'] = $data['share_total'];
                $dataUpdate['post_total'] = $data['post_total'];
                $dataUpdate['comment_total'] = $data['comment_total'];

                $rank_point = intval($data['view_total'] + (2 * $data['like_total']) + (3 * $data['comment_total']) + (4 * $data['share_total']));
                if ($rank_point > 0) {
                    $dataUpdate['rank_point'] = $rank_point;
                }
                
                //update bảng Account
                $command->update('let_kit_account', $dataUpdate, 'id = :id', array(':id' => $data['creator']));
                echo $key . '::';
                $command->reset();
            }

            Yii::log('------------------ END Update View Film vs Stats ------------------', 'info');

        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }

    }

}