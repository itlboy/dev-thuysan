<?php
Yii::import('application.modules.account.models.KitAccountStats');
class UpdateUserStatsCommand extends CConsoleCommand{

    public function actionIndex(){
        try{
//            ini_set('memory_limit',-1);
            if(KitAccountStats::updateStatsAll()){
                echo 'true';
                Yii::log('------------------ Update Stats cho tat ca user thanh cong ------------------', 'info');
            } else{
                echo 'false';
                Yii::log('------------------ Khong Update Stats cho user nao ------------------', 'info');
            }
        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
    }

    public function actionMoney(){
        try{
            //            ini_set('memory_limit',-1);
            if(KitAccountStats::updateStatsAllMoney()){
                echo 'true';
                Yii::log('------------------ Update Stats cho tat ca user da tham gia giao dich thanh cong ------------------', 'info');
            } else{
                echo 'false';
                Yii::log('------------------ Khong Update Stats cho user nao ------------------', 'info');
            }
        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
    }

}