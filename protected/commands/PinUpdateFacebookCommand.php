<?php

class PinUpdateFacebookCommand extends CConsoleCommand {

    private $_command;
    private $_exec = 0;      // Tổng số row đã được update
    private $_limit = 100;   // Giới hạn số lần gửi 1 request tới facebook
    private $_domain = 'http://cuoi.let.vn/';
    private $_domainExt = '.let';

    public function init() {
        if ($this->_command == NULL) {
            $_db = Yii::app()->db;
            $this->_command = $_db->createCommand();
        }
    }

    public function run($args) {
        try {
            // Chon kieu update, dua vao thoi gian de phan loai pin
            if (empty($args))
                die('Chon 1, 2 hoac 3');
            $type = $args[0];
            if (!in_array($type, array(0, 1, 2, 3)))
                $type = 1;

            // Lấy danh sách các Pin cần update
            $dataList = $this->_getData($type);

            // Hiển thị tổng số rows cần update
            echo count($dataList) . '::';

            // Phân trang số rows trả về từ bảng pin
            $pages = array_chunk($dataList, $this->_limit);

            // Duyệt qua từng trang
            foreach ($pages as $pageKey => $page) {
                $urlList = array();
                foreach ($page as $data) {
                    $url = '"' . $this->_domain . Common::convertStringToUrl($data['title']) . '-pin-' . $data['id'] . $this->_domainExt . '"';
                    $urlList[] = $url;
                }

                $resultFB = $this->_getStatsByFql($urlList);        // Lấy thông tin thống kê từ Facebook
                $this->_updateData($pageKey, $resultFB, $dataList); // Cập nhật thông tin vào Database

                // Hiển thị trạng thái thực thi. Đã chạy được bao nhiêu row
                $this->_exec = $this->_exec + count($resultFB);
                echo $this->_exec . '.';
            }
            echo $this->_exec;

            Yii::log('------------------ END Update Like, Share, Comment Facebook ------------------', 'info');
        } catch (Exception $e) {
            echo $e->getMessage();
            Yii::log($e->getMessage(), 'info');
        }
    }

    // Lấy danh sách các Pin cần update
    private function _getData($type) {
        if ($type == 0) {
            $dataList = $this->_command->select('id, title')
                    ->from('let_kit_pin')
                    ->queryAll();
            $this->_command->reset();
            return $dataList;
        }
        
        if ($type == 1) {           // ngày hiện tại
            $condition = 'DATE(updated_time) = DATE(NOW())';
        } else if ($type == 2) {    // ngày hôm qua đến 4 ngày trước (tức là 3 ngày)
            $condition = 'DATE(updated_time) BETWEEN DATE_SUB(CURDATE(), INTERVAL 1 DAY) AND DATE_SUB(CURDATE(), INTERVAL 3 DAY)';
        } else if ($type == 3) {    // từ 5 ngày trước trở về quá khứ
            $condition = 'DATE(updated_time) < DATE_SUB(CURDATE(), INTERVAL 3 DAY)';
        }

        $dataList = $this->_command->select('id, title')
                ->from('let_kit_pin')
                ->where($condition)
                ->queryAll();

        $this->_command->reset();
        return $dataList;
    }

    // Cập nhật thông tin vào database
    private function _updateData($pageKey, $resultFB, $dataList) {
        // Kết quả trả về của FB dựa trên thứ tự các URL trong danh sách đưa vào
        // Nên ID để update dữ liệu tính theo công thức: (trang hiện tại * limit) + key kết quả trả về
        foreach ($resultFB as $keyFB => $resultFB) {
            $this->_command->update('let_kit_pin', array(
                'like_count' => $resultFB['total_count'],
                'share_count' => $resultFB['share_count'],
                'comment_count' => $resultFB['commentsbox_count'],
                'updated_time' => Common::getNow(),
            ), 'id = :id', array(':id' => $dataList[($pageKey * $this->_limit) + $keyFB]['id']));
            $this->_command->reset();
        }
    }

    // Lấy thông tin thống kê từ Facebook
    private function _getStatsByFql($urlList) {
        $query = 'SELECT url, like_count, share_count, commentsbox_count, click_count, total_count
                  FROM link_stat
                  WHERE url IN (' . implode(', ', $urlList) . ')';

        $url = 'http://api.facebook.com/method/fql.query?format=json&query=' . urlencode($query);
        $response = @file_get_contents($url);

        return json_decode($response, TRUE);
    }

}