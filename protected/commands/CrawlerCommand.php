<?php
Yii::import('application.vendors.libs.crawler');
class CrawlerCommand extends CConsoleCommand{

    public function actionIndex(){


    }

    // Crawler source de get ra link, luu link vao bang link
    public function actionCrawlerSource($crawler_id){
        try
        {
            // Get source de lay ra link va cat_id
            $sources = crawler::getSource($crawler_id);
            foreach($sources as $source){
                // kiem tra source->link co ton tai hay khong /
                if(!empty($source->link)){
                    $config = crawler::detectConfig($source->link,$crawler_id);
                    //Kiem tra config co ton tai khong va co phai la mang hay khong
                    if(!empty($config) AND is_array($config)){
                        //neu ton tai config va la mang thi se lay gia tri dau tien cua mang config
                        crawler::insertDataBase($config[0],$source->link,$crawler_id);
                        die;
                    }
                }
            }

        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }


        // Crawler source, insert vao bang Link voi cac truong: link, image, title, cat_id
    }

    // Crawler source de get ra link, luu link vao bang link
    public function actionCrawlerLink($crawler_id){
        try
        {
//            $a = "<div><a href='/da/ea/f.html?id=1'><a href='/da/ea/f.html'><img src='http://dantri.com.vn/adva/wgtaa/anh.jpg' /><img src='/adva/wgadaaa/anhda.jpg' /><a href='/da/ea/fad.html'><img src='/adva/wgadaaa/anhda.jpg' /><img src='/adva/wgadaaa/anhda.jpg' /><a href='/da/eaada/f.html'><img src='/adva/wgadaaa/anhda.jpg' /><a href='/da/ea/ffa.html'></div>";
//            $result = crawler::addBaseUrl($a,'http://vietnamnet.vn');
//            print_r($result);

            // Get link de lay ra link va cat_id
            $links = crawler::getLink($crawler_id);
            foreach($links as $link){
                if(!empty($link->link)){
                    $config = crawler::detectConfig($link->link,$crawler_id);
                    //Kiem tra config co ton tai khong va co phai la mang hay khong
                    if(!empty($config) AND is_array($config)){
                        //neu ton tai config va la mang thi se lay gia tri dau tien cua mang config
                        crawler::insertDataBase($config[0],$link->link,$crawler_id);
                    }
                }
            }

        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
    }
}