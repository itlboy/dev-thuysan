<?php

class FilmViewStatsCommand extends CConsoleCommand
{
    public function run() {
        try {
            $_db = Yii::app()->db;
            $command = $_db->createCommand();

            $dataList = $command->select('f.id, f.view_count, s.view_total')
                    ->from('let_kit_film f')
                    ->leftJoin('let_kit_stats s', 's.item_id = f.id')
                    ->where('s.module = "film"')
                    ->queryAll();
            
            echo count($dataList) . '::';
            $command->reset();

            foreach ($dataList as $key => $data) {
                $viewFilm = $data['view_count'];
                $viewStats = $data['view_total'];
                if ($viewStats > $viewFilm) {
                    //update bảng Film
                    $command->update('let_kit_film', array(
                        'view_count' => $viewStats
                    ), 'id = :id', array(':id' => $data['id']));                    
                } else {
                    //update bảng Stats
                    $command->update('let_kit_stats', array(
                        'view_total' => $viewFilm
                    ), 'module = "film" AND item_id = :itemId', array(':itemId' => $data['id']));
                }
                echo $key . '::';
                $command->reset();
            }

            Yii::log('------------------ END Update View Film vs Stats ------------------', 'info');

        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }

    }

}