<?php
Yii::import('application.vendors.libs.videoInfo');
Yii::import('application.vendors.libs.grabVideo');
class CheckFilmCommand extends CConsoleCommand{

    public function actionIndex(){
        $_db = Yii::app()->db;
        $command = $_db->CreateCommand();
        $listItem = $command->select('*')->from('let_kit_film_episode')->where('server = "youtube" OR checked_time <= NOW() - INTERVAL 7 DAY')->queryAll();
        $command->reset();
        foreach($listItem as $key => $value){
            if($key < 2){
                $datalink = videoInfo::youtube($value['link']);
                $date = date('Y-m-d H:i:s');
                $isdie = 0;
                if(empty($datalink)){
                    $isdie = 1;
                }
                $update = $command->update('let_kit_film_episode',array(
                    'checked_time' => $date,
                    'is_die' => $isdie),'id =' .$value['id']);
                echo $value['link']."\n";
                $command->reset();
            }
        }
    }
}