<?php

class FilmConvertCommand extends CConsoleCommand
{
    public function actionIndex() {
        try {
//            $t = 'Witness To A Prosecution (2000) [22/22]';
//
//            $title = $this->_getEpisode($t);
//            if($title['episode_current'] > 200) {
//                print_r($this->_getEpisode($title['title']));
//            }
//            die;
            $_db = Yii::app()->db;
            $command = $_db->createCommand();
            $command->truncateTable('let_kit_film');
            $command->truncateTable('let_kit_film_category');
            $command->truncateTable('let_kit_film_episode');
//            $command->truncateTable('let_kit_category');
            Yii::log('Empty target table!', 'info');

            // Get old data
//            $command->reset();
//            $catList = $command->select('*')->from('let_portal_news_cat')->where('ib_id = 17')->queryAll();
            $command->reset();
            $itemsList = $command->select('*')->from('let_portal_film_items')->where('ib_id = 7')->order('items_id DESC')->queryAll();

            /* Insert Data */
            // Cat
//            Yii::import('application.modules.category.models.KitCategory');
//            Yii::log('------------------ START Convert Category ('.  count($catList).' rows) ------------------', 'info');
//            foreach ($catList as $key => $value) {
//                $row = new KitCategory;
//                $row->id = $value['cat_id'];
//                $row->name = $value['cat_name'];
//                $row->alias = Common::convertStringToUrl($value['cat_name']);
//                $row->parent_id = $value['cat_parentid'];
//                $row->description = '';
//                $row->layout = 'category1';
//                $row->module = 'items';
//                $row->created_time = date('Y-m-d H:i:s');
//                $row->updated_time = date('Y-m-d H:i:s');
//                $row->sorder = $value['cat_order'];
//                $row->status = $value['cat_active'];
//                $row->save();
//                Yii::log('Converted category '.$value['cat_id'].':'.$value['cat_name'], 'info');
//            }
//            Yii::log('------------------ END Convert Category ------------------', 'info');

            
            // Items
            Yii::log('------------------ START Convert Items ('.  count($itemsList).' rows) ------------------', 'info');
            
            foreach ($itemsList as $key => $value) {
                $command->reset();
                if ($key < 300 OR TRUE) {
                    
                    $title = $this->_getEpisode($value['items_name1']);

//                if (in_array($value['cat_id'], array(1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20))) {
                    $value['items_active'] = $value['items_active'] == NULL ? 1 : $value['items_active'];
                    $insert = $command->insert('let_kit_film', array(
                        'id'=>$value['items_id'],
                        'title'=>$value['items_name1'],
//                        'title'=> !empty($title['title']) ? $title['title'] : '',
                        'title_english'=>$value['items_name2'],
                        'image'=>$value['items_image'],
                        'intro'=>$value['items_desc'],
                        'content'=>$value['items_content'],
                        'actor'=>$value['items_actor'],
                        'director'=>$value['items_director'],
                        'theater'=>$value['items_theater'],
                        'length'=>$value['items_length'],
                        'episode_total' => $title['episode_total'],
                        'episode_current' => $title['episode_current'],
                        'complete'=>$value['items_complete'],
                        'comment_count'=>$value['items_complete'],
                        'view_count'=>$value['items_count_view'],
//                        'tags'=>$value['items_keyword'],
                        'seo_url'=>$value['items_alias'],
                        'creator'=>$value['items_creator'],
                        'created_time'=>date('Y-m-d H:i:s', $value['items_created']),
                        'editor'=>$value['items_editor'],
                        'updated_time'=>date('Y-m-d H:i:s', $value['items_edited']),
                        'sorder'=>$value['items_order'],
                        'hot'=>$value['items_focus2'],
                        'promotion'=>$value['items_focus'],
                        'status'=>$value['items_active'],

                    ));
                    $msg = 'Converted items '.$value['items_id'].':'.$value['items_name1'];
                    echo $msg . "\n";
                    Yii::log($msg, 'info');
                    
                    // Episode
                    Yii::import('application.vendors.libs.grabVideo');
                    $command->reset();
                    $episodeList = $command->select('*')->from('let_portal_film_episode')->where('items_id = '.$value['items_id'])->queryAll();
                    $acceptServer = array(
                        'youtube.com',
                        'cyworld.vn',
                        'dailymotion.com',
                        '4shared.com',
                        'videobb.com',
                        'yume.vn',
                        'veoh.com',
                        'go.vn',
                        'zing.vn',
                        'videozer.com',
                        'tamtay.vn',
                        'clip.vn',
                        'viddler.com',
                        'blip.tv',
                        'seeon.tv',
                        'banbe.net',
                        'vidxden.com',
                        'metacafe.com',
                        'zippyshare.com',
                        'vimeo.com',
                    );
                    foreach ($episodeList as $key2 => $value2) {
                        if (in_array($value2['episode_server'], $acceptServer)) {
                            $insert = $command->insert('let_kit_film_episode', array(
                                'item_id'=>$value2['items_id'],
                                'title'=>$value2['episode_name'],
                                'embed'=>$value2['episode_embed'],
                                'link'=>$value2['episode_grab'],
                                'server' => grabVideo::getNameServer($value2['episode_grab']),
                                'creator'=>$value2['episode_creator'],
                                'created_time'=>date('Y-m-d H:i:s', $value2['episode_created']),
                                'editor'=>$value2['episode_creator'],
                                'updated_time'=>date('Y-m-d H:i:s', $value2['episode_created']),
                                'sorder'=>$value2['episode_order'],
                                'hot'=>0,
                                'promotion'=>0,
                                'status'=>$value2['episode_active'],
                                'trash'=>$value2['episode_recycle'],
                            ));
                            $msg = 'Converted episode '.$value['items_id'] . '-' . $value2['episode_id'] . '-' . $value2['episode_name'];
                            echo $msg . "\n";
                            Yii::log($msg, 'info');
                        }
                    }
                }
//                }

            }
            Yii::log('------------------ END Convert Items ------------------', 'info');
            
            // items Category
            Yii::log('------------------ START Convert Items Category ('.  count($itemsList).' rows) ------------------', 'info');
            $command->reset();
            foreach ($itemsList as $key => $value) {
                /*
                 * Phong su anh khong co
                 */
//                if (in_array($value['cat_id'], array(1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20))) {
                if ($key < 300  OR TRUE) {
                    $itemCatList = explode(',', $value['cat_id']);
                    if (is_array($itemCatList)) {
                        foreach ($itemCatList as $id) {
                            $id = (int) $id;
                            if ($id > 0) {
                                if ($id == 13) $newCat = 19;
                                elseif ($id == 1) $newCat = 43;
                                elseif ($id == 2) $newCat = 44;
                                elseif ($id == 3) $newCat = 45;
                                elseif ($id == 4) $newCat = 46;
                                elseif ($id == 16) $newCat = 47;
                                elseif ($id == 5) $newCat = 48;
                                elseif ($id == 20) $newCat = 48;
                                elseif ($id == 6) $newCat = 50;
                                elseif ($id == 7) $newCat = 51;
                                elseif ($id == 9) $newCat = 52;
                                elseif ($id == 10) $newCat = 53;
                                elseif ($id == 11) $newCat = 54;
                                elseif ($id == 12) $newCat = 55;
                                elseif ($id == 13) $newCat = 56;
                                elseif ($id == 14) $newCat = 57;
                                elseif ($id == 17) $newCat = 58;
                                elseif ($id == 18) $newCat = 59;
                                elseif ($id == 19) $newCat = 60;
                                else {}

                                $command->insert('let_kit_film_category', array(
                                    'film_id' => $value['items_id'],
                                    'category_id' => $newCat,
                                ));                
                                Yii::log('Converted items Category with film_id = '.$value['items_id'].'| category_id = '.$newCat, 'info');
                            }
                        }
                    }
                }
//                }
            }
            Yii::log('------------------ END Convert Items Category ------------------', 'info');
            
        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
    }

    public function actionImages() {
        try {
            $_db = Yii::app()->db;
            $command = $_db->createCommand();

            // Get old data
            $command->reset();
            $itemList = $command->select('id, title')
                    ->from('let_kit_film')
                    ->order('id DESC')
//                    ->limit(10)
                    ->queryAll();

            /* Insert Data */
            // Film Image
            Yii::log('------------------ START Convert Film Image ------------------', 'info');
            foreach ($itemList as $key => $value) {
                $url = 'http://phim.let.vn/uploads/film/'.$value['id'].'_big.jpg';
//                $img = '/my/folder/flower.gif';
//                file_put_contents($img, file_get_contents($url));
                $urlInsert = $this->_createThumb('film', $url, $value['title']);
                echo $key+1 . '---------' . $urlInsert;
                echo "\n";
//                die;
                
                $command->reset();
                $command->update('let_kit_film', array(
                    'image' => $urlInsert,
                ), 'id = ' . $value['id']);
                Yii::log('Update image for id = '.$value['id'], 'info');
            }
            Yii::log('------------------ END Convert Article Image ------------------', 'info');
            
        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
    }

    public function actionComment() {
        try {
            $_db = Yii::app()->db;
            $command = $_db->createCommand();
            $command->truncateTable('let_kit_comment');
            Yii::log('Empty target table!', 'info');
            $command->reset();

            $count = $command->select('count(*) as num')->from('let_portal_comment_comment')->queryAll();
            $onPage = 100;
            $totalRows = $count[0]['num'];
            $totalPage = intval($totalRows / $onPage) + 1 ;
            for ($i = 0 ; $i < $totalPage; $i++){
                $command->reset();
                $itemsList = $command->select('*')->from('let_portal_comment_comment')->limit($onPage)->offset(($i+1) * $onPage)->queryAll();
                Yii::log('------------------ START Convert Comment ('.  count($itemsList).' rows) ------------------', 'info');

                foreach ($itemsList as $key => $value) {
                    $command->reset();
                    Yii::import('application.modules.account.models.KitAccount');
                    $account = KitAccount::model()->findByPk($value['uid']);
                    $account = $account->attributes;
                    if ($key < 200 OR TRUE) {
                        $insert = $command->insert('let_kit_comment', array(
                            'id' => $value['comment_id'],
                            'author_name' => $account['username'],
                            'author_email' => $account['email'],
                            'module' => $value['comment_module'],
                            'content' => $value['comment_content'],
                            'item_id' => $value['comment_items_id'],
                            'reply_to' => 0,
                            'creator' => $value['uid'],
                            'created_time' => date('Y-m-d H:i:s',$value['comment_created']),
                            'status' => $value['comment_active'],
                        ));
                        $msg = 'Converted comment : ' . ($i+1) . ' - '.$value['comment_id'];
                        echo $msg . "\n";
                        Yii::log($msg, 'info');
                    }
                }
            }

        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }

    }
	
	public function actionUpdateInfo() {
		/**
		Em query ra tất cả các phim ở bảng hiện tại (không phải bảng cũ nhé)
		mỗi vòng lặp e xử lý cho a những việc sau:
		1. Xóa tập phim ở tiêu đề và tạo tập phim ở trường chứa tập phim (đã làm trong lúc convert rồi nhưng chưa xóa tập phim nên cứ làm lại)
		2. trong bảng let_kit_stats, thêm trường view_total ngay trước trường view_intoday, đẩy lượt xem từng phim vào trường này của bảng này. Trong đó module = film, item_id là ID của film
		Đừng xóa mấy dòng này, viết xuống dưới thôi để có gì anh còn cập nhật trên này, viết vào phần này là conflict ngay!
		*/
        try{
            $_db = Yii::app()->db;
            $command = $_db->createCommand();
            $command->truncateTable('let_kit_stats');
            Yii::log('Empty target table!', 'info');
            $command->reset();
            $itemList = $command->select('*')->from('let_kit_film')->queryAll();
            foreach($itemList as $key => $item)
            {
                $command->reset();
//                if($key < 400){
//                if($item['id'] == 394){
                    $title = $this->_getEpisode($item['title']);
                    if(($title['episode_current'] > 200 )){
                        $title = $this->_getEpisode($title['title']);
                    }
                    $field['episode_total'] = !empty($title['episode_total']) ? $title['episode_total'] : NULL;
                    $field['episode_current'] = !empty($title['episode_current']) ? $title['episode_current'] : NULL;

                    $field['title'] = $title['title'];
                    $update = $command->update('let_kit_film',$field,'id=:id',array(':id' => $item['id']));
                    $command->reset();
                    $insert = $command->insert('let_kit_stats',array(
                        'module' => 'film',
                        'item_id' => $item['id'],
                        'view_total' => $item['view_count'],
						'view_inyear' => $item['view_count'],
                    ));
                    $command->reset();
                    $msg = 'Converted film ['.$key.']: ' . $item['title'] . '  - View : '.$item['view_count'];
                    echo $msg . "\n";
                    Yii::log($msg, 'info');
//                }
            }

        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }

	}

	public function _createThumb ($module, $inputImage, $title) {
        try {
            $date = date('Y/m/d/');
            $url = $inputImage;
            $tmpFileInfo = pathinfo($inputImage);
            $inputImage = rand(100000, 999999) . '.' . $tmpFileInfo['extension'];

            // Download image
            $tmpFile = Yii::app()->params['uploadPath'] . Yii::app()->params['uploadDir'] . 'tmp/' . $inputImage;
            @file_put_contents($tmpFile, @file_get_contents($url));

            // Duong dan file luu o thu muc tam
//            $tmpFile = Common::getImageUploaded($inputImage, 'PATH');

            // Lay thong tin file tam va tao ra ten file moi
            $fileName = Common::convertStringToUrl($title) . '_' . rand(100000, 999999) . '.' .$tmpFileInfo['extension'];

            // get image config
            $imageConfig = Yii::app()->getModule($module)->image;

            // Resize
            foreach ($imageConfig as $key => $config) {
                if (in_array($key, array('origin', 'large', 'medium', 'small'))) {

                    // Thu muc luu file
                    $fileDir = $module . '/'.$config['folder'].'/' . $date;

                    // Duong dan file
                    $filePath = Common::getImageUploaded($fileDir . $fileName, 'PATH');

                    // Tao dir theo path neu dir chua ton tai
                    Common::mkPath($fileDir, Common::getImageUploaded('', 'PATH'));

                    if ($config['type'] == 'move') copy($tmpFile, $filePath);
                    elseif ($config['type'] == 'resize') {
                        $image = Yii::app()->image->load($tmpFile);
                        $image->resize($config['width'], $config['height'])->rotate($config['rotate'])->quality($config['quality']);
                        $image->save($filePath);
                    }
                }
            }
        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
        return $date . $fileName;
	}    

    public function _getEpisode($text) {
        $data = array(
            'title' => $text,
            'episode' => '',
            'episode_current' => NULL ,
            'episode_total' => NULL ,
        );
        $text = trim($text);
        $text = str_replace('   ', ' ', $text);
        $text = str_replace('  ', ' ', $text);
        $text = str_replace(array('-end', ' - end', ' -end', ' end', '-End', ' - End', ' -End', ' End', '-END', ' - END', ' -END', ' END', ' vietsub'), '', $text);
        $text = str_replace(array('( Tập','[ Tập','( Tap','[ Tap','[ tap','( tap'), '[Tập', $text);
        $text = str_replace(array(' )',' ]',')',' )','}',' }'), ']', $text);
        $text = str_replace(array('(','( ','[','[ ','{','{ '), '[', $text);
        if(preg_match('/\[(.*?)\]/i',$text,$array)){
            if(count($array)>0){
                $episode = $array[0];
                $name = str_replace($episode,'',$text);
            }
            $data['title'] = !empty($name) ? $name : $text ;
            $data['episode'] = !empty($episode) ? $episode : '' ;
            $episode = str_replace(array('15+','16+','17+','18+','19+','20+','21+'),'',$episode);

            if(strstr($episode,'Tập')){ //Kiểm tra xem có chữ Tập không
                $episode = strstr($episode,'Tập');//Lấy giá trị trừ chữ Tập đến hết
            }
            $l = strpos($episode,'/');// Vị trí " / "
            if(!empty($l)){
                if(preg_match('/\d+\/\d+/',$episode,$arr))
                {
                    $a = explode('/',$arr[0]);
                    $data['episode_current'] = !empty($a[0]) ? $a[0] : NULL ; // Số tập hiện tại
                    $data['episode_total'] = !empty($a[1]) ? $a[1] : NULL ;
                }
            } else {
                if(preg_match('/\d+/',$episode,$arr))
                {
                    $data['episode_current'] = !empty($arr[0]) ? $arr[0] : NULL ; // Số tập hiện tại
                    $data['episode_total'] = NULL;
                }
            }
        }
        return $data;
    }
}