<?php

class AccountRankCommand extends CConsoleCommand
{
    public function run() {
        try {
            $_db = Yii::app()->db;
            $command = $_db->createCommand();

            $dataList = $command->select('id, rank_point')
                    ->from('let_kit_account')
                    ->where('rank_point > 0')
                    ->order('rank_point DESC')
                    ->queryAll();

            echo count($dataList) . '::';
            $command->reset();

            foreach ($dataList as $key => $data) {       
                $command->update('let_kit_account', array(
                    'rank' => $key + 1
                ), 'id = :id', array(':id' => $data['id']));
                echo $key + 1 . '::';
                $command->reset();
            }

            Yii::log('------------------ END Update View Film vs Stats ------------------', 'info');

        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }

    }

}