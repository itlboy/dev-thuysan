<?php

class ArticleConvertCommand extends CConsoleCommand
{
    public function actionIndex() {
        try {
            $_db = Yii::app()->db;
            $command = $_db->createCommand();
            $command->truncateTable('let_kit_article');
            $command->truncateTable('let_kit_article_category');
//            $command->truncateTable('let_kit_category');
            Yii::log('Empty target table!', 'info');

            // Get old data
            $command->reset();
            $catList = $command->select('*')->from('let_portal_news_cat')->where('ib_id = 17')->queryAll();
            $command->reset();
            $articleList = $command->select('*')->from('let_portal_news_article')->queryAll();

            /* Insert Data */
            // Cat
//            Yii::import('application.modules.category.models.KitCategory');
//            Yii::log('------------------ START Convert Category ('.  count($catList).' rows) ------------------', 'info');
//            foreach ($catList as $key => $value) {
//                $row = new KitCategory;
//                $row->id = $value['cat_id'];
//                $row->name = $value['cat_name'];
//                $row->alias = Common::convertStringToUrl($value['cat_name']);
//                $row->parent_id = $value['cat_parentid'];
//                $row->description = '';
//                $row->layout = 'category1';
//                $row->module = 'article';
//                $row->created_time = date('Y-m-d H:i:s');
//                $row->updated_time = date('Y-m-d H:i:s');
//                $row->sorder = $value['cat_order'];
//                $row->status = $value['cat_active'];
//                $row->save();
//                Yii::log('Converted category '.$value['cat_id'].':'.$value['cat_name'], 'info');
//            }
//            Yii::log('------------------ END Convert Category ------------------', 'info');

            // Article
            Yii::log('------------------ START Convert Article ('.  count($articleList).' rows) ------------------', 'info');
            $command->reset();
            foreach ($articleList as $key => $value) {
                if (in_array($value['cat_id'], array(13, 12, 19, 20, 16, 14, 24, 18, 28, 31,68,69,76,35,34,70,71,72,90,89))) {
                    $value['cat_active'] = $value['cat_active'] == NULL ? 1 : $value['cat_active'];
                    $command->insert('let_kit_article', array(
                        'id'=>$value['article_id'],
                        'title'=>$value['article_name'],
                        'intro'=>$value['article_desc'],
                        'content'=>$value['article_content'],
                        'type'=>'text',
                        'image'=>NULL,
                        'tags'=>NULL,
                        'view_count'=>0,
                        'from_time'=>$value['article_effecttime'],
                        'to_time'=>$value['article_expiredtime'],
                        'creator'=>$value['article_creator'],
                        'created_time'=>date('Y-m-d H:i:s', $value['article_created']),
                        'editor'=>$value['article_editor'],
                        'updated_time'=>date('Y-m-d H:i:s', $value['article_edited']),
                        'layout_id'=>NULL,
                        'promotion'=>$value['article_focus'],
                        'sorder'=>$value['article_order'],
                        'status'=>$value['cat_active'],
                    ));                
                    Yii::log('Converted Article '.$value['article_id'].':'.$value['article_name'], 'info');                    
                }

            }
            Yii::log('------------------ END Convert Article ------------------', 'info');
            
            // Article Category
            Yii::log('------------------ START Convert Article Category ('.  count($articleList).' rows) ------------------', 'info');
            $command->reset();
            foreach ($articleList as $key => $value) {
                /*
                 * Phong su anh khong co
                 */
                if (in_array($value['cat_id'], array(13, 12, 19, 20, 16, 14, 24, 18, 28, 31,68,69,76,35,34,70,71,72,90,89))) {
                    if ($value['cat_id'] == 13) $newCat = 19;
                    elseif ($value['cat_id'] == 12) $newCat = 21;
                    elseif ($value['cat_id'] == 19) $newCat = 24;
                    elseif ($value['cat_id'] == 20) $newCat = 22;
                    elseif ($value['cat_id'] == 16) $newCat = 18;
                    elseif ($value['cat_id'] == 14) $newCat = 16;
                    elseif ($value['cat_id'] == 24) $newCat = 23;
                    elseif ($value['cat_id'] == 18) $newCat = 23;
                    elseif ($value['cat_id'] == 28) $newCat = 17;
                    elseif ($value['cat_id'] == 31) $newCat = 28;
                    elseif ($value['cat_id'] == 68) $newCat = 33;
                    elseif ($value['cat_id'] == 69) $newCat = 34;
                    elseif ($value['cat_id'] == 76) $newCat = 27;
                    elseif ($value['cat_id'] == 35) $newCat = 30;
                    elseif ($value['cat_id'] == 34) $newCat = 31;
                    elseif ($value['cat_id'] == 70) $newCat = 38;
                    elseif ($value['cat_id'] == 71) $newCat = 39;
                    elseif ($value['cat_id'] == 72) $newCat = 32;
                    elseif ($value['cat_id'] == 90) $newCat = 41;
                    elseif ($value['cat_id'] == 89) $newCat = 41;
                    else {}

                    $value['cat_active'] = $value['cat_active'] == NULL ? 1 : $value['cat_active'];
                    $command->insert('let_kit_article_category', array(
                        'article_id' => $value['article_id'],
                        'category_id' => $value['cat_id'],
                    ));                
                    Yii::log('Converted Article Category with article_id = '.$value['article_id'].'| category_id = '.$value['cat_id'], 'info');
                }
            }
            Yii::log('------------------ END Convert Article Category ------------------', 'info');
            
        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }

    }
    
    public function actionImages() {
        try {
            $_db = Yii::app()->db;
            $command = $_db->createCommand();

            // Get old data
            $command->reset();
            $articleList = $command->select('a.id, a.title, b.article_images')
                    ->from('let_kit_article as a')
                    ->join('let_portal_news_article as b', 'a.id = b.article_id')
                    ->queryAll();

            /* Insert Data */
            // Article Image
            Yii::log('------------------ START Convert Article Image ------------------', 'info');
            foreach ($articleList as $key => $value) {
//                $url = 'http://thuysanvietnam.com.vn/assets/uploads/'.$value['article_images'];
                $url = Yii::app()->createAbsoluteUrl('/assets/uploads/'.$value['article_images']);
//                $img = '/my/folder/flower.gif';
//                file_put_contents($img, file_get_contents($url));
                echo $urlInsert = $this->_createThumb('article', $url, $value['title']);
                echo "\n";
//                die;
                
                $command->reset();
                $command->update('let_kit_article', array(
                    'image' => $urlInsert,
                ), 'id = ' . $value['id']);
                Yii::log('Update image for id = '.$value['article_id'], 'info');
            }
            Yii::log('------------------ END Convert Article Image ------------------', 'info');
            
        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
    }

	public function _createThumb ($module, $inputImage, $title) {
        try {
            $date = date('Y/m/d/');
            $url = $inputImage;
            $tmpFileInfo = pathinfo($inputImage);
            $inputImage = rand(100000, 999999) . '.' . $tmpFileInfo['extension'];

            // Download image
            $tmpFile = Yii::app()->params['uploadPath'] . Yii::app()->params['uploadDir'] . 'tmp/' . $inputImage;
            @file_put_contents($tmpFile, @file_get_contents($url));

            // Duong dan file luu o thu muc tam
//            $tmpFile = Common::getImageUploaded($inputImage, 'PATH');

            // Lay thong tin file tam va tao ra ten file moi
            $fileName = Common::convertStringToUrl($title) . '_' . rand(100000, 999999) . '.' .$tmpFileInfo['extension'];

            // get image config
            $imageConfig = Yii::app()->getModule($module)->image;

            // Resize
            foreach ($imageConfig as $key => $config) {
                if (in_array($key, array('origin', 'large', 'medium', 'small'))) {

                    // Thu muc luu file
                    $fileDir = $module . '/'.$config['folder'].'/' . $date;

                    // Duong dan file
                    $filePath = Common::getImageUploaded($fileDir . $fileName, 'PATH');

                    // Tao dir theo path neu dir chua ton tai
                    Common::mkPath($fileDir, Common::getImageUploaded('', 'PATH'));

                    if ($config['type'] == 'move') copy($tmpFile, $filePath);
                    elseif ($config['type'] == 'resize') {
                        $image = Yii::app()->image->load($tmpFile);
                        $image->resize($config['width'], $config['height'])->rotate($config['rotate'])->quality($config['quality']);
                        $image->save($filePath);
                    }
                }
            }
        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
        return $date . $fileName;
	}
}