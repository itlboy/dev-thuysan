<?php echo "<?php\n"; ?>
/**
 * Module: <?php echo $this->moduleName."\n"; ?>
 * Auth: nguago@let.vn
 * Website: http://let.vn
 */
class DefaultController extends ControllerFrontend
{
	public function actionIndex()
	{
		$this->render('index');
	}
}