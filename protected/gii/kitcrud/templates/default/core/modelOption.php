<?php echo "<?php\n"; ?>

Yii::import('application.modules.<?php echo $this->ModuleName; ?>.models.db.Base<?php echo $this->modelClass; ?>Option');
class <?php echo $this->modelClass; ?>Option extends Base<?php echo $this->modelClass; ?>Option {

    var $className = __CLASS__;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function get<?php echo ucfirst($this->ModuleName); ?>2Option($<?php echo $this->ModuleName; ?>Id) {
        $result = array();
        $data = self::findAll('<?php echo $this->ModuleName; ?>_id=:<?php echo $this->ModuleName; ?>Id', array(':<?php echo $this->ModuleName; ?>Id' => $<?php echo $this->ModuleName; ?>Id));
        foreach ($data as $item2Type)
            $result[] = $item2Type->option_id;
        return $result;
    }

    /**
     * Create
     * @param array $options
     * @param int $itemId 
     */
    public static function create<?php echo ucfirst($this->ModuleName); ?>Option($options = array(), $itemId) {
        if (! is_array($options)) return FALSE;
        foreach ($options as $optionId) {
            $item2Opt = new <?php echo $this->modelClass; ?>Option;
            $item2Opt->option_id = $optionId;
            $item2Opt-><?php echo strtolower($this->ModuleName); ?>_id = $itemId;
            $item2Opt->save();
        }
    }

    /**
     * Delete
     * @param array $options
     * @param int $itemId 
     */
    public static function delete<?php echo ucfirst($this->ModuleName); ?>Option($options = array(), $itemId) {
        if (! is_array($options)) return FALSE;
        foreach ($options as $optionId) {
            self::model()->deleteAll('option_id=:optionId AND <?php echo $this->ModuleName; ?>_id=:<?php echo $this->ModuleName; ?>Id', array(
                ':optionId' => $optionId,
                ':<?php echo $this->ModuleName; ?>Id' => $itemId,
            ));
        }
    }
}