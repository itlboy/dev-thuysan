<?php echo '<?php'; ?>

class <?php echo ucfirst($this->controller); ?>Controller extends ControllerBackend
{
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id) {
		$model=<?php echo $this->ModelClass; ?>::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

    public function actionIndex() {
        $model = new <?php echo $this->ModelClass; ?>('search');
        $model->unsetAttributes();  // clear any default values

<?php if ($this->hasCategory == '1' AND $this->TypeObject == 'main'): ?>
        // Category
        if (Yii::app()->request->getQuery('category_id') !== NULL) {
            $model->category_id = Yii::app()->request->getQuery('category_id');
        }

<?php endif;?>
        if (isset($_GET['<?php echo $this->ModelClass; ?>'])) {
            $model->attributes = $_GET['<?php echo $this->ModelClass; ?>'];
        }

        // page size drop down changed
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$model = new <?php echo $this->ModelClass; ?>;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$this->save($model);

		$this->render('edit',array(
			'model'=>$model
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id) {
		$model=$this->loadModel($id);
<?php if ($this->hasCategory == '1' AND $this->TypeObject == 'main'): ?>
        $categoriesOld = $model->categories = <?php echo $this->ModelClass; ?>Category::model()->get($model->id);
        
<?php endif; ?>
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

<?php if ($this->hasCategory == '1' AND $this->TypeObject == 'main'): ?>
            $this->save($model, $categoriesOld);
<?php else: ?>
            $this->save($model);
<?php endif; ?>

		$this->render('edit',array(
			'model' => $model
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionInfo($id) {
        $model = <?php echo $this->ModelClass; ?>::getDetails($id);

		$this->render('info',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
                
            echo json_encode(array('status' => 'success'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

    
	protected function save($model, $categoriesOld = NULL) {
		if(isset($_POST['<?php echo $this->ModelClass; ?>'])) {
<?php if (in_array('image', array_keys($columns))): ?>
            // Upload image
			if ($_POST['<?php echo $this->ModelClass; ?>']['image'] !== NULL AND $_POST['<?php echo $this->ModelClass; ?>']['image'] !== '') {
                $_POST['<?php echo $this->ModelClass; ?>']['image'] = Common::createThumb($this->module->getName() , $_POST['<?php echo $this->ModelClass; ?>']['image'], letArray::get($_POST['<?php echo $this->ModelClass; ?>'], 'title', ''));
                
                // Remove Image
                Common::removeImage($this->module->id, $model->image);
            } else unset ($_POST['<?php echo $this->ModelClass; ?>']['image']);
<?php endif; ?>

<?php if (in_array('from_time', array_keys($columns))): ?>
			if ($_POST['<?php echo $this->ModelClass; ?>']['from_time'] == NULL OR $_POST['<?php echo $this->ModelClass; ?>']['from_time'] == '') {
                unset($_POST['<?php echo $this->ModelClass; ?>']['from_time']);
            }
<?php endif; ?>

<?php if (in_array('to_time', array_keys($columns))): ?>
            if ($_POST['<?php echo $this->ModelClass; ?>']['to_time'] == NULL OR $_POST['<?php echo $this->ModelClass; ?>']['to_time'] == '') {
                unset($_POST['<?php echo $this->ModelClass; ?>']['to_time']);
            }
<?php endif; ?>

            $model->attributes = $_POST['<?php echo $this->ModelClass; ?>'];

<?php if ($this->hasCategory == '1' AND $this->TypeObject == 'main'): ?>
            $categoriesNew = $model->categories;
            
<?php endif; ?>                
            if ($model->validate()) {
                if($model->save()) {
<?php if ($this->hasCategory == '1' AND $this->TypeObject == 'main'): ?>
                    $itemId = $model->id;
                    if (!$model->isNewRecord)
                        <?php echo $this->ModelClass; ?>Category::del($categoriesOld, $itemId);
                    <?php echo $this->ModelClass; ?>Category::create($categoriesNew, $itemId);
					
<?php endif; ?>
                    if (Yii::app()->request->getPost('apply') == 0)
                         $this->redirect(array('index'));
                    else $this->redirect(array('update', 'id' => $model->id));
                }
            }
		}
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model) {
		if(isset($_POST['ajax']) && $_POST['ajax']==='kit-<?php echo $this->moduleName; ?>-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    
    <?php if ($this->TypeObject == 'children') : ?>
    <?php $modelChildren = $this->convertToModelClass($this->children_tableName); ?>
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionAjaxCreate($item) {
        $model = new <?php echo $modelChildren; ?>;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['<?php echo $modelChildren; ?>']) AND !empty($_POST['<?php echo $modelChildren; ?>'])) {           
            foreach (array_keys($_POST['<?php echo $modelChildren; ?>']) as $i) {
                if (isset($_POST['<?php echo $modelChildren; ?>'][$i])) {
                    $modelSave = new <?php echo $modelChildren; ?>;
                    $modelSave->attributes = $_POST['<?php echo $modelChildren; ?>'][$i];
                    $modelSave->item_id = intval($item);
                    if ($modelSave->validate()) {                        
                        $modelSave->save();
                    }
                }
            }

            echo json_encode(array('status' => 'success'));
            Yii::app()->end();
        }

        $this->layout = false;
        $this->render('ajaxCreate', array(
            'model' => $model,
        ));
    }    
    
    
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionAjaxUpdate($id) {
		$model = $this->loadModel($id);
        
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
       
        if (isset($_POST['<?php echo $modelChildren; ?>'])) {           
            $model->attributes = $_POST['<?php echo $modelChildren; ?>'];
            if ($model->validate()) {                        
                $model->save();
            }
            
            echo json_encode(array('status' => 'success'));
            Yii::app()->end();
        }

        $this->layout = false;
        $this->render('ajaxUpdate', array(
            'model' => $model,
        ));
	}
    
    public function actionFilter($item) {
        $this->widget('application.modules.<?php echo $this->moduleName; ?>.widgets.backend.<?php echo ucfirst($this->children_controller); ?>', array('itemId' => intval($item), 'params' => $_GET));
    }    
    <?php endif; ?>
    
}
