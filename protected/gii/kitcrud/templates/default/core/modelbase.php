<?php echo '<?php'; ?>

/**
 * This is the model class for table "<?php echo $this->TableName; ?>".
 *
 * The followings are the available columns in table '<?php echo $this->TableName; ?>':
<?php foreach($columns as $column): ?>
 * @property <?php echo $column->type.' $'.$column->name."\n"; ?>
<?php endforeach; ?>
<?php if(!empty($relations)): ?>
 *
 * The followings are the available model relations:
<?php foreach($relations as $name=>$relation): ?>
 * @property <?php
	if (preg_match("~^array\(self::([^,]+), '([^']+)', '([^']+)'\)$~", $relation, $matches))
    {
        $relationType = $matches[1];
        $relationModel = $matches[2];

        switch($relationType){
            case 'HAS_ONE':
                echo $relationModel.' $'.$name."\n";
            break;
            case 'BELONGS_TO':
                echo $relationModel.' $'.$name."\n";
            break;
            case 'HAS_MANY':
                echo $relationModel.'[] $'.$name."\n";
            break;
            case 'MANY_MANY':
                echo $relationModel.'[] $'.$name."\n";
            break;
            default:
                echo 'mixed $'.$name."\n";
        }
	}
    ?>
<?php endforeach; ?>
<?php endif; ?>
 */

class Base<?php echo $this->ModelClass; ?> extends <?php echo $this->BaseClass; ?> {
    var $className = __CLASS__;
    
    public $category_id;
    public $categories;
    
	/**
	 * Returns the static model of the specified AR class.
	 * @return <?php echo $this->ModelClass; ?> the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{<?php echo $this->TableName; ?>}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
<?php foreach($rules as $rule): ?>
			<?php echo $rule.",\n"; ?>
<?php endforeach; ?>
            array('categories', 'safe'),
			// The following rule is used by search(). Please remove those attributes that should not be searched.
			array('<?php echo implode(', ', array_keys($columns)); ?>', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related class name for the relations automatically generated below.
		return array(
<?php if($this->hasCategory == TRUE AND $this->TypeObject == 'main'): ?>
            '<?php echo $this->ModelClass; ?>Category' => array(<?php echo $this->ModelClass; ?>::MANY_MANY, 'KitCategory',
                '{{kit_<?php echo $this->moduleName; ?>_category}}(<?php echo $this->moduleName; ?>_id, category_id)'),
<?php endif; ?>
            'creatorUser' => array(<?php echo $this->ModelClass; ?>::BELONGS_TO, 'KitAccount', 'creator', 'select' => 'username'),
            'editorUser' => array(<?php echo $this->ModelClass; ?>::BELONGS_TO, 'KitAccount', 'editor', 'select' => 'username'),
            'stats' => array(self::HAS_ONE, 'KitStats', 'item_id',
                'condition' => 'module = "<?php echo $this->moduleName; ?>"'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;
        $criteria->select = Common::getFieldInTable(<?php echo $this->ModelClass; ?>::model()->getAttributes(), 't.');
        
<?php foreach($columns as $name=>$column): ?>
<?php if($column->type==='string'): ?>
        $criteria->compare('t.<?php echo $name; ?>',$this-><?php echo $name; ?>,true);
<?php else: ?>
        $criteria->compare('t.<?php echo $name; ?>',$this-><?php echo $name; ?>);
<?php endif; ?>
<?php endforeach; ?>
<?php if($this->hasCategory == TRUE AND $this->TypeObject == 'main'): ?>

        if ($this->category_id !== NULL) {
            $categories = array();
            $categories[] = $this->category_id;
            $category = KitCategory::model()->findByPk($this->category_id);
            $descendants = $category->descendants()->findAll();
            foreach ($descendants as $cat) {
                $categories[] = $cat->id;
            }
            $criteria->addInCondition('ic.category_id', $categories);
        }
        
        $criteria->join = 'LEFT JOIN {{kit_<?php echo $this->moduleName; ?>_category}} ic ON ic.<?php echo $this->moduleName; ?>_id = t.id';
<?php endif; ?>
        
        $criteria->group = 't.<?php echo $this->tableSchema->primaryKey; ?>';
        
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
            'sort' => array(
                'defaultOrder' => '<?php echo $this->tableSchema->primaryKey; ?> DESC',
            ),
		));
	}
    
    protected function beforeValidate() {
<?php if (in_array('editor', array_keys($columns))): ?>
        $this->editor = Yii::app()->user->id;
<?php endif; ?>
<?php if (in_array('updated_time', array_keys($columns))): ?>
        $this->updated_time = Common::getNow();                    
<?php endif; ?>
        if ($this->isNewRecord) {
<?php if (in_array('creator', array_keys($columns))): ?>
            $this->creator = $this->editor;
<?php endif; ?>
<?php if (in_array('created_time', array_keys($columns))): ?>
            $this->created_time = $this->updated_time;
<?php endif; ?>
<?php if (in_array('view_count', array_keys($columns))): ?>
            $this->view_count = 0;
<?php endif; ?>
<?php if (in_array('comment_count', array_keys($columns))): ?>
            $this->comment_count = 0;
<?php endif; ?>
        }
        return parent::beforeValidate();
    }
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('<?php echo $this->ModelClass; ?>::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('<?php echo $this->ModelClass; ?>::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('<?php echo $this->ModelClass; ?>::getLastest_'));
        Yii::app()->cache->delete(md5('<?php echo $this->ModelClass; ?>::getPromotion_'));
    }
    
    public function parseCategory($data, $delimiter = '') {
        $arr = array();
        foreach ($data as $value) {
            $arr[] = $value->name;                                     
        }
        echo implode($arr, $delimiter);    
    }
                

    ///////////////////////////////// Frontend /////////////////////////////////
    /**
     * treatment
     * @param mixed $data
     * @return array 
     */
    public static function treatment($data)
    {
        $data = CJSON::decode ((CJSON::encode($data))); // Neu la object thi chuyen thanh array
        if (empty($data))
            return array();
        elseif (!isset ($data[0]))
            $data = <?php echo $this->ModelClass; ?>::treatmentRow($data);
        else {
            foreach ($data as $key => $value) {
                $data[$key] = <?php echo $this->ModelClass; ?>::treatmentRow($value);
            }
        }
        return $data;
    }
    
    public static function treatmentRow($row)
    {
        $urlTitle = Common::convertStringToUrl($row['title']);
        $row['url'] = Yii::app()->createUrl($urlTitle.'-<?php echo $this->moduleName; ?>-'.$row['<?php echo $this->tableSchema->primaryKey; ?>']);
        return $row;
    }
    
    public static function countRecord($from_time = NULL, $to_time = NULL) {
        if ($from_time == NULL) $from_time = '0000-00-00 00:00:00';
        if ($to_time == NULL) $to_time = date('Y-m-d H:i:s');
        
        $result = <?php echo $this->ModelClass; ?>::model()->count("created_time BETWEEN '".$from_time."' AND '".$to_time."'");
        return $result;
    }

    
    /**
     * Get Details
     * @param int $id
     * @return object 
     */
    public static function getDetails($id) {
        $id = intval($id);
        $cache_name = md5(__METHOD__ . '_' . $id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $result = <?php echo $this->ModelClass; ?>::model()->findByPk($id, array(
                'select' => Common::getFieldInTable(<?php echo $this->ModelClass; ?>::model()->getAttributes(), 't.'),
                'with' => array('creatorUser', 'editorUser')
            ));
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
    
    public static function getField($id, $field){
        $row = <?php echo $this->ModelClass; ?>::getDetails($id);
        $row = <?php echo $this->ModelClass; ?>::treatment($row);
        return letArray::get($row, $field, '');
    }

    public static function getLastest ($category = NULL, $limit = 20){
        $cache_name = md5(__METHOD__ . '_' . $category);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(<?php echo $this->ModelClass; ?>::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');


            if ($category !== NULL AND $category >= 0) {
                $catListObject = KitCategory::getListInParent('film', $category);
                $catList = array($category);
                foreach ($catListObject as $cat) {
                    $catList[] = $cat->id;
                }
                $criteria->join = "INNER JOIN {{kit_film_category}} t2 ON t.id=t2.film_id";
//                $criteria->group = "t.id";
                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', $catList).')');
            }

            $criteria->order = 't.id DESC';
            $criteria->limit = $limit;
            $result = <?php echo $this->ModelClass; ?>::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
    
    public static function getPromotion ($category = NULL, $limit = 20){
        $cache_name = md5(__METHOD__ . '_' . $category);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(<?php echo $this->ModelClass; ?>::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
            $criteria->condition = Common::addWhere($criteria->condition, 't.promotion = 1');

            if ($category !== NULL AND $category >= 0) {
                $criteria->join = "INNER JOIN {{kit_film_category}} t2 ON t.id=t2.film_id";
                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', KitCategory::getListInParent('film', $category)).')');
            }
            $criteria->order = 't.id DESC';
            $criteria->limit = $limit;
            $result = <?php echo $this->ModelClass; ?>::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

}
