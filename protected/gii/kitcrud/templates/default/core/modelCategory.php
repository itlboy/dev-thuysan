<?php echo "<?php\n"; ?>

Yii::import('application.modules.<?php echo $this->moduleName; ?>.models.db.Base<?php echo $this->ModelClass; ?>');
class <?php echo $this->ModelClass; ?> extends Base<?php echo $this->ModelClass; ?>{

    var $className = __CLASS__;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function get($itemId) {
        $result = array();
        $data = self::findAll('<?php echo $this->moduleName; ?>_id=:itemId', array(':itemId' => $itemId));
        foreach ($data as $item2Type)
            $result[] = $item2Type->category_id;
        return $result;
    }

    /**
     * Create
     * @param array $categories
     * @param int $itemId 
     */
    public static function create($categories = array(), $itemId) {
        if (! is_array($categories)) return FALSE;
        foreach ($categories as $categoryId) {
            $item2Cat = new <?php echo $this->modelClass; ?>;
            $item2Cat->category_id = $categoryId;
            $item2Cat-><?php echo strtolower($this->moduleName); ?>_id = $itemId;
            $item2Cat->save();
        }
    }

    /**
     * Delete
     * @param array $categories
     * @param int $itemId 
     */
    public static function del($categories = array(), $itemId) {
        if (! is_array($categories)) return FALSE;
        foreach ($categories as $categoryId) {
            self::model()->deleteAll('category_id=:categoryId AND <?php echo $this->moduleName; ?>_id=:<?php echo $this->moduleName; ?>Id', array(
                ':categoryId' => $categoryId,
                ':<?php echo $this->moduleName; ?>Id' => $itemId,
            ));
        }
    }
}