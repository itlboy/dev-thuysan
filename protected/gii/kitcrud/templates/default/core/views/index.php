<?php $columnList = array_keys($this->tableSchema->columns); ?>
<?php echo "<?php"; ?>

$moduleName = '<?php echo $this->moduleName; ?>';
$modelName = '<?php echo $this->modelClass; ?>';
$formID = 'kit_'.$moduleName.'_form';
$gridID = 'kit_'.$moduleName.'_grid';

$this->breadcrumbs = array(
    Yii::t('backend', $moduleName) => $this->createUrl('/'.$moduleName.'/default/index'),
    Yii::t('global', 'List') . ' ' . Yii::t('backend', $moduleName),
);
?>

<?php echo ""; ?>
<?php if($this->hasCategory == '1' AND $this->TypeObject == 'main'): ?>
<div class="grid_12">
<?php else: ?>
<div class="grid_16">
<?php endif; ?>
    <div class="flat_area">
        <h2><?php echo "<?php echo Yii::t('backend', 'List'); ?>"; ?> <?php echo "<?php echo Yii::t('backend', \$moduleName); ?>"; ?></h2>
    </div>
    <button class="blue small" onclick="ajaxChangeBooleanValue('<?php echo "<?php echo \$moduleName; ?>"; ?>', '<?php echo "<?php echo \$modelName; ?>"; ?>', 'status', 1, '<?php echo "<?php echo Yii::app()->createUrl('cms/ajax/ChangeBooleanValue'); ?>"; ?>', '<?php echo "<?php echo \$gridID; ?>"; ?>');">
        <div class="ui-icon ui-icon-check"></div>
        <span><?php echo "<?php echo Yii::t('global', 'Publish'); ?>"; ?></span>
    </button>
    <button class="light small" onclick="ajaxChangeBooleanValue('<?php echo "<?php echo \$moduleName; ?>"; ?>', '<?php echo "<?php echo \$modelName; ?>"; ?>', 'status', 0, '<?php echo "<?php echo Yii::app()->createUrl('cms/ajax/ChangeBooleanValue'); ?>"; ?>', '<?php echo "<?php echo \$gridID; ?>"; ?>');">
        <div class="ui-icon ui-icon-cancel"></div>
        <span><?php echo "<?php echo Yii::t('global', 'Unpublish'); ?>"; ?></span>
    </button>
    <button class="blue small" onclick="ajaxChangeBooleanValue('<?php echo "<?php echo \$moduleName; ?>"; ?>', '<?php echo "<?php echo \$modelName; ?>"; ?>', 'promotion', 1, '<?php echo "<?php echo Yii::app()->createUrl('cms/ajax/ChangeBooleanValue'); ?>"; ?>', '<?php echo "<?php echo \$gridID; ?>"; ?>');">
        <div class="ui-icon ui-icon ui-icon-arrowthick-1-n"></div>
        <span><?php echo "<?php echo Yii::t('global', 'Promotion'); ?>"; ?></span>
    </button>
    <button class="light small" onclick="ajaxChangeBooleanValue('<?php echo "<?php echo \$moduleName; ?>"; ?>', '<?php echo "<?php echo \$modelName; ?>"; ?>', 'promotion', 0, '<?php echo "<?php echo Yii::app()->createUrl('cms/ajax/ChangeBooleanValue'); ?>"; ?>', '<?php echo "<?php echo \$gridID; ?>"; ?>');">
        <div class="ui-icon ui-icon-arrowthick-1-s"></div>
        <span><?php echo "<?php echo Yii::t('global', 'Demotion'); ?>"; ?></span>
    </button>
    <button class="red small" onclick="ajaxChangeBooleanValue('<?php echo "<?php echo \$moduleName; ?>"; ?>', '<?php echo "<?php echo \$modelName; ?>"; ?>', 'trash', 1, '<?php echo "<?php echo Yii::app()->createUrl('cms/ajax/ChangeBooleanValue'); ?>"; ?>', '<?php echo "<?php echo \$gridID; ?>"; ?>');">
        <div class="ui-icon ui-icon-trash"></div>
        <span><?php echo "<?php echo Yii::t('global', 'Trash'); ?>"; ?></span>
    </button>
    <button class="red small" onclick="ajaxDeleteMultiRecord('<?php echo "<?php echo \$moduleName; ?>"; ?>', '<?php echo "<?php echo \$modelName; ?>"; ?>', '<?php echo "<?php echo Yii::app()->createUrl('cms/ajax/DeleteMultiRecord'); ?>"; ?>', '<?php echo "<?php echo \$gridID; ?>"; ?>')">
        <div class="ui-icon ui-icon-close"></div>
        <span><?php echo "<?php echo Yii::t('global', 'Delete'); ?>"; ?></span>
    </button>
    <?php echo "<?php"; ?>
    
    $this->widget('GridView', array(
        'id' => $gridID,
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => array(
            array(
                'class' => 'CCheckBoxColumn',
                'id' => 'chkIds',
                'selectableRows' => 2,
                'htmlOptions' => array('width' => '20px', 'align' => 'center')
            ),
            array(
                'name' => '<?php echo $this->tableSchema->primaryKey; ?>',
                'value' => '$data-><?php echo $this->tableSchema->primaryKey; ?>'
            ),
        <?php if (in_array('title', $columnList)) : ?>
            
            array(
                'name' => 'title',
                'value' => '$data->title'
            ),
        <?php endif; ?>
        <?php if (in_array('image', $columnList)) : ?>
            
            array(
                'name' => 'image',
                'type' => 'raw',
                'value' => '($data->image != "") ? CHtml::image(Common::getImageUploaded(Yii::app()->controller->module->name . "/small/" . $data->image), "", array("height" => "50")) : ""',
                'htmlOptions' => array('width' => '50px', 'align' => 'center')
            ),
        <?php endif; ?>
        <?php if (in_array('promotion', $columnList)) : ?>
            
            array(
                'filter' => CHtml::activeDropDownList($model, 'promotion', BackendFunctions::getStatusOptions(), array('prompt' => Yii::t('global', 'Select all'))),
                'type' => 'raw',
                'name' => 'promotion',
                'value' => 'BackendFunctions::getBooleanIcon("'.$moduleName.'", "'.$modelName.'", $data-><?php echo $this->tableSchema->primaryKey; ?>, "promotion", $data->promotion, "'.$gridID.'")',
                'htmlOptions' => array('align' => 'center')
            ),
        <?php endif; ?>
        <?php if (in_array('status', $columnList)) : ?>
            
            array(
                'filter' => CHtml::activeDropDownList($model, 'status', BackendFunctions::getStatusOptions(), array('prompt' => Yii::t('global', 'Select all'))),
                'type' => 'raw',
                'name' => 'status',
                'value' => 'BackendFunctions::getBooleanIcon("'.$moduleName.'", "'.$modelName.'", $data-><?php echo $this->tableSchema->primaryKey; ?>, "status", $data->status, "'.$gridID.'")',
                'htmlOptions' => array('align' => 'center')
            ),
        <?php endif; ?>
            array( // display a column with "view", "update" and "delete" buttons
                'class' => 'CButtonColumn',
                'header' => CHtml::dropDownList('pageSize',
                    Yii::app()->user->getState('pageSize',20),
                    array(2=>2,10=>10,20=>20,30=>30,40=>40,50=>50,100=>100,200=>200,500=>500,1000=>1000),
                    array('onchange'=>"$.fn.yiiGridView.update('".$gridID."',{ data:{pageSize: $(this).val() }})")
                ),
                'template' => '{gridInfo} {update} {delete}',
                'buttons' => array(
                    'gridInfo' => array(
                        'label' => Yii::t('global', 'View'),
                        'url' => 'Yii::app()->createUrl("//'.$moduleName.'/'.$this->id.'/info", array("<?php echo $this->tableSchema->primaryKey; ?>"=>$data-><?php echo $this->tableSchema->primaryKey; ?>))',
                        'imageUrl'=>Yii::app()->theme->baseUrl.'/images/icons/system/info_16.png'
                    ),
                    'update' => array(
                        'label' => Yii::t('global', 'Edit'),
                        'url' => 'Yii::app()->createUrl("//'.$moduleName.'/'.$this->id.'/update", array("<?php echo $this->tableSchema->primaryKey; ?>"=>$data-><?php echo $this->tableSchema->primaryKey; ?>))',
                        'imageUrl' => Yii::app()->theme->baseUrl.'/images/icons/system/edit_16.png',
                        'options' => array('class' => 'btnGridUpdate')
                    ),
                    'delete' => array(
                        'label' => Yii::t('global', 'Delete'),
                        'url' => 'Yii::app()->createUrl("//'.$moduleName.'/'.$this->id.'/delete", array("<?php echo $this->tableSchema->primaryKey; ?>"=>$data-><?php echo $this->tableSchema->primaryKey; ?>))',
                        'imageUrl' => Yii::app()->theme->baseUrl.'/images/icons/system/delete_16.png',
                    ),
                ),
            )
        ),
    ));
    ?>
</div>
<?php if($this->hasCategory == '1' AND $this->TypeObject == 'main'): ?>
<div class="grid_4 box">
    <?php echo "<?php"; ?>
    $this->widget('category.widgets.treeCategory', array(
        'module' => $moduleName,
    ));
    ?>
</div>
<?php endif; ?>
