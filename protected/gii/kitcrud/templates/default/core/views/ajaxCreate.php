<?php $modelName = $this->convertToModelClass($this->children_tableName); ?>
<?php echo '<?php'; ?>

$form = $this->beginWidget('CActiveForm', array(
	'id' => '<?php echo $this->removePrefix($this->children_tableName) . '_form'; ?>',
	'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'onSubmit' => 'js:ajaxSaveForm("<?php echo $modelName; ?>" ,jQuery(this).attr("id"), jQuery(this).attr("action")); return false',
    )
)); ?>
    <div id="" class="dialog_content">
        <div class="block" style="opacity: 1;">
            <div class="section">
                <h2>Thông tin</h2>
                <!--
                <div class="alert dismissible alert_black">
                    <img width="24" height="24" src="images/icons/small/white/alert_2.png">
                    <strong>All the form fields</strong> can be just as easily used in a dialog.
                </div>
                -->
            </div>

            <a href="javascript:;" id="copyForm" rel="0" onclick="copyForm(jQuery(this))">Add</a>
            <div id="nameOrigin">
                
        <?php if ($this->children_fieldUpdate != NULL) : ?>
        <?php foreach (explode(',', $this->children_fieldCreate) as $field) : ?>                
                
            <?php $columnList = array_keys(Yii::app()->db->schema->getTable($this->children_tableName)->columns); ?>
            <?php if (in_array($field, $columnList)) : ?>                
                <?php $dataType = Yii::app()->db->schema->getTable($this->children_tableName)->getColumn($field)->dbType; ?>
                <?php if ($dataType == 'tinyint(1)' OR $dataType == 'tinyint(1) unsigned') : ?>

                <fieldset class="label_side">
                    <label><?php echo '<?php'; ?> echo $model->getAttributeLabel('<?php echo $field; ?>'); ?></label>
                    <div>
                        <div class="selector" id="uniform-undefined"><span style="-moz-user-select: none;"><?php echo Yii::t('BackEnd', "-- Select a status --"); ?></span>
                            <?php echo '<?php'; ?> echo CHtml::dropdownList('<?php echo $modelName; ?>[0][<?php echo $field; ?>]', NULL, BackendFunctions::getStatusOptions(), array(
                                'class' => 'select_box',
                                'style' => 'opacity:0',
                            )) ?>
                        </div>
                    </div>
                </fieldset>
                <?php else : ?>
                
                <fieldset class="label_side">
                    <label><?php echo '<?php'; ?> echo $model->getAttributeLabel('<?php echo $field; ?>'); ?></label>
                    <div>
                        <?php echo '<?php'; ?> echo CHtml::textField('<?php echo $modelName; ?>[0][<?php echo $field; ?>]', $model-><?php echo $field; ?>, array('class' => 'tooltip right text')); ?>
                    </div>
                </fieldset>
                <?php endif; ?>
            <?php endif; // End: kiem tra field co ton tai trong table hay ko? ?>
        <?php endforeach; ?>     
        <?php endif; ?>
            </div>
            <div id="nameExtra"></div>

            <div class="button_bar clearfix">
                <button class="dark green close_dialog">
                    <div class="ui-icon ui-icon-check"></div>
                    <span>Submit</span>
                </button>

                <!--
                <button class="dark red close_dialog">
                    <div class="ui-icon ui-icon-closethick"></div>
                    <span>Cancel</span>
                </button>
                -->
            </div>
        </div>
    </div>

<?php echo '<?php'; ?> $this->endWidget(); ?>

<script>
    function copyForm(obj) {
        nextFormId = parseInt(obj.attr('rel')) + 1;
        html = jQuery('#nameOrigin').html();
        html = html.replace(new RegExp('_0_', 'g'), '_'+nextFormId+'_');
        html = html.replace(new RegExp('\\[0\\]', 'g'), '['+nextFormId+']');
        obj.attr('rel', nextFormId);
        jQuery('#nameExtra').append(html);
    }
</script>
