<?php $columnList = array_keys($this->tableSchema->columns); ?>
<?php //$columnList = array_keys(Yii::app()->db->schema->getTable($this->main_tableName)->columns); ?>
<?php echo "<?php"; ?> echo $form->errorSummary($model); ?>
<div id="message"></div>

<div class="box grid_16" style="margin-top: 10px;">
    <div class="toggle_container">
        <div class="block">
            <h2 class="section">SEO</h2>
            <?php if (in_array('seo_title', $columnList)) : ?>

            <fieldset class="label_side">
                <label><?php echo "<?php"; ?> echo $form->labelEx($model, 'seo_title'); ?><span>Ký tự nhỏ hơn 70</span></label>
                <div>
                    <?php echo "<?php"; ?> echo $form->textField($model, 'seo_title', array('size' => 30, 'maxlength' => 70, 'class' => 'tooltip autogrow', 'title' => 'Nhập tiêu đề', 'placeholder' => 'Tiêu đề thể hiện trên kết quả tìm kiếm...')); ?>
                    <?php echo "<?php"; ?> echo $form->error($model, 'seo_title'); ?>
                </div>
            </fieldset>
            <?php endif; ?>
            <?php if (in_array('seo_url', $columnList)) : ?>

            <fieldset class="label_side">
                <label><?php echo "<?php"; ?> echo $form->labelEx($model, 'seo_url'); ?><span>Ký tự nhỏ hơn 255</span></label>
                <div>
                    <?php echo "<?php"; ?> echo $form->textField($model, 'seo_url', array('size' => 30, 'maxlength' => 255, 'class' => 'tooltip autogrow', 'title' => 'Nhập Alias', 'placeholder' => 'Nhập Alias để tạo link...')); ?>
                    <?php echo "<?php"; ?> echo $form->error($model,'seo_url'); ?>
                </div>
            </fieldset>
            <?php endif; ?>
            <?php if (in_array('seo_desc', $columnList)) : ?>

            <fieldset class="label_side">
                <label><?php echo "<?php"; ?> echo $form->labelEx($model, 'seo_desc'); ?><span>Ký tự nhỏ hơn 160</span></label>
                <div class="clearfix">
                    <?php echo "<?php"; ?> echo $form->textArea($model, 'seo_desc', array('class' => 'tooltip', 'title' => 'Mô tả ngắn được hiển thị trên kết quả tìm kiếm')); ?>
                    <?php echo "<?php"; ?> echo $form->error($model, 'seo_desc'); ?>
                </div>
            </fieldset>
            <?php endif; ?>

        </div>
    </div>
</div>
