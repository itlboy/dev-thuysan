<?php // $columnList = array_keys($this->tableSchema->columns); ?>

<?php
if ($this->TypeObject == 'main') {
    $columns = Yii::app()->db->schema->getTable($this->main_tableName)->columns;
    $columnList = array_keys($this->tableSchema->columns);
} else {
    $columns = Yii::app()->db->schema->getTable($this->children_tableName)->columns;
    $columnList = array_keys(Yii::app()->db->schema->getTable($this->children_tableName)->columns);
}
?>
<div class="box grid_16" style="margin-top: 10px;">
    <div class="toggle_container">
        <div class="block">
            <h2 class="section"><?php echo "<?php"; ?> echo Yii::t('global', 'General'); ?></h2>
            
<?php foreach($columns as $name => $column): ?>
    <?php if (! in_array($name, array('id', 'image', 'content', 'view_count', 'comment_count', 'from_time', 'to_time', 'seo_title', 'seo_url', 'seo_desc', 'creator', 'created_time', 'editor', 'updated_time', 'sorder', 'promotion', 'status', 'trash'))): ?>
        <?php if (preg_match("/tinyint\(1\)/i", $column->dbType)): ?>
            
            <fieldset class="label_side">
                <label><?php echo "<?php"; ?> echo $form->labelEx($model, '<?php echo $name; ?>'); ?></label>
                <div>
                    <div class="jqui_radios">
                        <?php echo "<?php"; ?> echo $form->radioButtonList($model, '<?php echo $name; ?>', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                        <?php echo "<?php"; ?> echo $form->error($model, '<?php echo $name; ?>'); ?>
                    </div>
                </div>
            </fieldset>
        <?php elseif (preg_match("/datetime/i", $column->dbType)): ?>
            
            <fieldset class="label_side">
                <label><?php echo "<?php"; ?> echo $form->labelEx($model, '<?php echo $name; ?>'); ?></label>
                <div class="clearfix">
                    <?php echo "<?php"; ?> echo $form->textField($model, '<?php echo $name; ?>', array('class' => 'datepicker')); ?>
                    <?php echo "<?php"; ?> echo $form->error($model, '<?php echo $name; ?>'); ?>
                </div>
            </fieldset>
        <?php elseif (preg_match("/varchar\(500\)/i", $column->dbType)): ?>
            
            <fieldset class="label_side">
                <label><?php echo "<?php"; ?> echo $form->labelEx($model, '<?php echo $name; ?>'); ?></label>
                <div class="clearfix">
                    <?php echo "<?php"; ?> echo $form->textArea($model, '<?php echo $name; ?>', array('class' => 'tooltip', 'title' => 'Nhập thông tin...')); ?>
                    <?php echo "<?php"; ?> echo $form->error($model, '<?php echo $name; ?>'); ?>
                </div>
            </fieldset>
        <?php else: ?>

            <fieldset class="label_side">
                <label><?php echo "<?php"; ?> echo $form->labelEx($model, '<?php echo $name; ?>'); ?></label>
                <div>
                    <?php echo "<?php"; ?> echo $form->textField($model, '<?php echo $name; ?>'); ?>
                    <?php echo "<?php"; ?> echo $form->error($model, '<?php echo $name; ?>'); ?>
            <?php if (! $column->allowNull): ?>
        <div class="required_tag tooltip hover left" title="<?php echo "<?php"; ?> echo Yii::t('global', 'Required'); ?>"></div>
            <?php endif; ?>
    </div>
            </fieldset>
        <?php endif; ?>
    <?php endif; ?>
<?php endforeach; ?>

<?php if($this->hasCategory == '1' AND $this->TypeObject == 'main'): ?>
            <fieldset class="label_side">
                <label><?php echo "<?php"; ?> echo $form->labelEx($model, 'categories'); ?></label>
                <div>
                    <?php echo "<?php"; ?>

                    echo $form->checkBoxList($model, 'categories', CHtml::listData(KitCategory::model()->getCategoryOptions($this->module->id), 'id', 'name'), array(
                        'separator' => '',
                        'template' => '<div>{input} {label}</div>'));
                    ?>
                    <?php echo "<?php"; ?> echo $form->error($model, 'categories'); ?>
                </div>
            </fieldset>
<?php endif; ?>

<?php if($this->hasOption == '1' AND $this->TypeObject == 'main'): ?>
            <fieldset class="label_side">
                <label><?php echo "<?php"; ?> echo $form->labelEx($model, 'options'); ?></label>
                <div>
                    <?php echo "<?php"; ?>
                    
                    echo $form->checkBoxList($model, 'options', CHtml::listData(KitOption::model()->getOptionOptions($this->module->id), 'id', 'name'), array(
                        'separator' => '',
                        'template' => '<div>{input} {label}</div>'));
                    ?>
                    <?php echo "<?php"; ?> echo $form->error($model, 'options'); ?>
                </div>
            </fieldset>
<?php endif; ?>
        </div>
    </div>
</div>

<?php if (in_array('from_time', $columnList) AND in_array('to_time', $columnList)): ?>
<div class="box grid_16 round_all">
    <div class="toggle_container">
        <div class="block">
            <h2 class="section">Đặt giờ xuất bản</h2>
            <div class="columns clearfix">
                <div class="col_50">
                    <fieldset class="label_side">
                        <label><?php echo "<?php"; ?> echo $form->labelEx($model, 'from_time'); ?></label>
                        <div>
                            <?php echo "<?php"; ?> echo $form->textField($model, 'from_time', array('class' => 'datepicker')); ?>
                            <?php echo "<?php"; ?> echo $form->error($model, 'from_time'); ?>
                        </div>
                    </fieldset>
                </div>
                <div class="col_50">
                    <fieldset class="label_side">
                        <label><?php echo "<?php"; ?> echo $form->labelEx($model, 'to_time'); ?></label>
                        <div>
                            <?php echo "<?php"; ?> echo $form->textField($model, 'to_time', array('class' => 'datepicker')); ?>
                            <?php echo "<?php"; ?> echo $form->error($model, 'to_time'); ?>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<?php if (in_array('sorder', $columnList) AND in_array('promotion', $columnList) AND in_array('status', $columnList) AND in_array('trash', $columnList)): ?>
<div class="box grid_16">
    <div class="toggle_container">
        <div class="block">
            <div class="columns clearfix">
                <div class="col_25">
                    <fieldset>
                        <label><?php echo "<?php"; ?> echo $form->labelEx($model, 'sorder'); ?></label>
                        <div>
                            <?php echo "<?php"; ?> echo $form->textField($model, 'sorder', array('class' => 'tooltip autogrow', 'title' => 'Số càng nhỏ thì thứ tự càng cao. Mặc định: 500', 'placeholder' => 'Nhập 1 số')); ?>
                            <?php echo "<?php"; ?> echo $form->error($model, 'sorder'); ?>
                        </div>
                    </fieldset>
                </div>
                <div class="col_25">
                    <fieldset>
                        <label><?php echo "<?php"; ?> echo $form->labelEx($model, 'promotion'); ?></label>
                        <div>
                            <div class="jqui_radios">
                                <?php echo "<?php"; ?> echo $form->radioButtonList($model, 'promotion', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                                <?php echo "<?php"; ?> echo $form->error($model, 'promotion'); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col_25">
                    <fieldset>
                        <label><?php echo "<?php"; ?> echo $form->labelEx($model, 'status'); ?></label>
                        <div>
                            <div class="jqui_radios">
                                <?php echo "<?php"; ?> echo $form->radioButtonList($model, 'status', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                                <?php echo "<?php"; ?> echo $form->error($model, 'status'); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col_25">
                    <fieldset>
                        <label><?php echo "<?php"; ?> echo $form->labelEx($model, 'trash'); ?></label>
                        <div>
                            <div class="jqui_radios">
                                <?php echo "<?php"; ?> echo $form->radioButtonList($model, 'trash', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                                <?php echo "<?php"; ?> echo $form->error($model, 'trash'); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<?php if (in_array('content', $columnList)): ?>
<div class="box grid_16">
    <?php echo "<?php"; ?> echo $form->error($model,'content'); ?>
    <?php echo "<?php"; ?>
    $this->widget('ext.elrtef.elRTE', array(
        'model' => $model,
        'attribute' => 'content',
        'options' => array(
            'doctype'=>'js:\'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\'',
            'cssClass' => 'el-rte',
            'cssfiles' => array('css/elrte-inner.css'),
            'absoluteURLs'=>true,
            'allowSource' => true,
            'lang' => 'vi',
            'styleWithCss'=>'',
            'height' => 400,
            'fmAllow'=>true, //if you want to use Media-manager
            'fmOpen'=>'js:function(callback) {$("<div id=\"elfinder\" />").elfinder(%elfopts%);}',//here used placeholder for settings
            'toolbar' => 'maxi',
        ),
        'elfoptions' => array( //elfinder options
            'url'=>'auto',  //if set auto - script tries to connect with native connector
            'passkey'=>'mypass', //here passkey from first connector`s line
            'lang'=>'vi',
            'dialog'=>array('width'=>'900','modal'=>true,'title'=>'Media Manager'),
            'closeOnEditorCallback'=>true,
            'editorCallback'=>'js:callback'
        ),
    ));
    ?>  
</div>
<?php endif; ?>
