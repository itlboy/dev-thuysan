<div style="margin-top: 10px;"></div>
<div class="box grid_16">
    <h2 class="box_head">Line Graph</h2>
    <a href="#" class="grabber"></a>
    <a href="#" class="toggle"></a>
    <div class="toggle_container">
        <div class="block">
            <div class="section">
                <div id="flot_line" class="flot"></div>
            </div>
        </div>
    </div>
</div>

<div class="box grid_16">
    <h2 class="box_head">Point Graph with Pie chart</h2>
    <a href="#" class="grabber"></a>
    <a href="#" class="toggle"></a>
    <div class="toggle_container">
        <div class="block">
            <div class="columns">
                <div class="col_66">
                    <div class="section">
                        <div id="flot_points" class="flot"></div>
                    </div>
                </div>
                <div class="col_33">
                    <div class="section">
                        <div id="flot_pie_1" class="flot"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="box grid_16">
    <h2 class="box_head">Bar Graph</h2>
    <a href="#" class="grabber"></a>
    <a href="#" class="toggle"></a>
    <div class="toggle_container">
        <div class="block">
            <div class="section">
                <div id="flot_bar" class="flot"></div>
            </div>
        </div>
    </div>
</div>


<?php echo "<?php"; ?>

    $cs = Yii::app()->getClientScript();
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/flot/excanvas.min.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/flot/jquery.flot.min.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/flot/jquery.flot.resize.min.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/flot/jquery.flot.pie.min.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/flot/jquery.flot.pie.min.js');
    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/flot/jquery.flot.pie.resize_update.js');

    $cs->registerScriptFile(Yii::app()->theme->baseUrl.'/scripts/adminica/adminica_charts.js');
?>