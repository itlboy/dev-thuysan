<?php $columnList = array_keys($this->tableSchema->columns); ?>
<?php //$columnList = array_keys(Yii::app()->db->schema->getTable($this->main_tableName)->columns); ?>
<?php echo "<?php"; ?>

$moduleName = '<?php echo $this->moduleName; ?>';
$modelName = '<?php echo $this->modelClass; ?>';
$formID = 'kit_'.$moduleName.'_form';
$gridID = 'kit_'.$moduleName.'_grid';

$this->breadcrumbs = array(
    Yii::t('backend', $moduleName) => $this->createUrl('/'.$moduleName.'/default/index'),
    Yii::t('global', 'Edit') . ' ' . Yii::t('backend', $moduleName),
);
?>

<div class="flat_area grid_16">
    <h2><?php echo "<?php"; ?> echo (!$model->isNewRecord) ? ((isset($model->title) AND $model->title !== NULL) ? $model->title : Yii::t('global', 'Update') . ' ' . Yii::t('backend', $moduleName)) : Yii::t('global', 'Create') . ' ' . Yii::t('backend', $moduleName); ?></h2>
</div>

<div class="grid_16">
    <button class="green small" onclick="js:jQuery('#apply').val(0); document.forms['<?php echo "<?php"; ?> echo $formID; ?>'].submit();">
        <div class="ui-icon ui-icon-check"></div>
        <span><?php echo "<?php"; ?> echo Yii::t('global', 'Save'); ?></span>
    </button>
    <button class="green small" onclick="js:jQuery('#apply').val(1); document.forms['<?php echo "<?php"; ?> echo $formID; ?>'].submit();">
        <div class="ui-icon ui-icon-check"></div>
        <span><?php echo "<?php"; ?> echo Yii::t('global', 'Apply'); ?></span>
    </button>
    <?php echo "<?php"; ?> if (!$model->isNewRecord): ?>
    <button class="blue small" onclick="window.location = '<?php echo "<?php"; ?> echo Yii::app()->createUrl($moduleName . '/' . $this->id . '/info', array('<?php echo $this->tableSchema->primaryKey; ?>' => $model-><?php echo $this->tableSchema->primaryKey; ?>)); ?>'">
        <img src="<?php echo "<?php"; ?> echo Yii::app()->theme->baseUrl; ?>/images/icons/small/white/chart_8.png" />
        <span><?php echo "<?php"; ?> echo Yii::t('global', 'View Info and stats'); ?></span>
    </button>
    <button class="red small">
        <div class="ui-icon ui-icon-trash"></div>
        <span><?php echo "<?php"; ?> echo Yii::t('global', 'Trash'); ?></span>
    </button>
    <?php echo "<?php"; ?> endif; ?>
</div>

<?php if (in_array('image', $columnList)): ?>
<!-- Image -->
<div class="box grid_16">
    <div class="toggle_container">
        <div class="block">
            <h2 class="section"><?php echo "<?php"; ?> echo Yii::t('global', 'image'); ?></h2>
            <fieldset class="label_side">
                <label><span>Chọn hoặc kéo thả ảnh để upload</span></label>
                <div>
                    <?php echo "<?php"; ?>
                    
                    Yii::import("ext.xupload.models.XUploadForm");
                    $uploadModel = new XUploadForm;
                    $this->widget('ext.xupload.XUploadWidget', array(
                        'url' => Yii::app()->createUrl('cms/upload/upload', array('module' => $moduleName)),
                        'model' => $uploadModel,
                        'attribute' => 'file',
                        'multiple' => false,
                        'options' => array(
                            'onComplete' => 'js:function (event, files, index, xhr, handler, callBack) {
                                if (handler.response.error !== 0) alert(handler.response.error);
                                else {
                                    $("#'.$modelName.'_image").val(handler.response.name);
                                    $("#ajax_result_image").html();
                                    $("#ajax_result_image").html(\'<li><a rel="collection" href="' . Common::getImageUploaded() . '\'+handler.response.name+\'"><img src="' . Common::getImageUploaded() . '\'+handler.response.name+\'?update='.time().'" height="84" width="148"/><span class="name">Ảnh gốc</span></a></li>\');
                                }
                            }',
                        )
                    ));
                    ?>
                    <div class="clearfix"></div>
                </div>
            </fieldset>

            <?php echo "<?php"; ?> if (isset($model->image) AND $model->image !== NULL AND $model->image !== ''): ?>
            <!-- Image list -->
            <div class="columns clearfix">
                <div class="indent gallery fancybox">
                    <ul class="clearfix" id="ajax_result_image">
                        <?php echo "<?php"; ?>
                        
                        $imageKeys = array_keys(Yii::app()->controller->getModule()->image);
                        ?>
                        <?php echo "<?php"; ?> foreach ($imageKeys as $key): ?>
                        <li>
                            <?php echo "<?php"; ?> $imageLink = Common::getImageUploaded($this->module->getName() . '/' . $key . '/' .$model->image); ?>
                            <a rel="collection" href="<?php echo "<?php"; ?> echo $imageLink; ?>">
                                <img src="<?php echo "<?php"; ?> echo $imageLink; ?>" height="84" width="148"/>
                                <span class="name"><?php echo "<?php"; ?> echo $key; ?></span>
<!--                                        <span class="size">71234kb</span>-->
                            </a>
                        </li>
                        <?php echo "<?php"; ?> endforeach; ?>
                    </ul>
                </div>
            </div>
            <!-- END Image list -->
            <?php echo "<?php"; ?> endif; ?>

        </div>
    </div>
</div>
<!-- END Image -->
<?php endif; ?>

<?php if ($this->hasMedia == '1'): ?>
<?php echo '<?php'; ?> if (!$model->isNewRecord) : ?>
<!-- Media -->
<div class="box grid_16">
    <div class="toggle_container">
        <div class="block">
            <h2 class="section">Media</h2>
            <fieldset class="label_side">
                <label><span>Chọn hoặc kéo thả ảnh để upload</span></label>
                <div>
                    <?php echo '<?php'; ?>
                    
                    Yii::import("ext.xupload.models.XUploadForm");
                    $uploadModel = new XUploadForm;
                    $this->widget('ext.xupload.XUploadWidget', array(
                        'url' => Yii::app()->createUrl('cms/upload/upload', array('module' => $moduleName, 'attribute' => 'file_media')),
                        'model' => $uploadModel,
                        'attribute' => 'file_media',
                        'multiple' => true,
                        'options' => array(
                            'onComplete' => 'js:function (event, files, index, xhr, handler, callBack) {
                                if (handler.response.error !== 0) alert(handler.response.error);
                                else {

                                    jQuery.ajax({
                                        url: "' . Yii::app()->createUrl('media/default/upload') . '",
                                        type: "POST",
                                        data:{
                                            "module": "'.$this->module->id.'",
                                            "itemId": "'.$model->id.'",
                                            "title": "'.$model->title.'",
                                            "media": handler.response.name
                                        },                                        
                                        success: function(data){
                                            jQuery.fn.yiiGridView.update("kit_media_grid");
                                        }
                                    });

                                }
                            }',
                        )
                    ));
                    ?>
                    <div class="clearfix"></div>
                </div>
            </fieldset>

        </div>
    </div>
</div>
<!-- END Media -->
<?php echo '<?php'; ?> endif; ?>
<?php endif; ?>

<?php echo "<?php"; ?>

$form=$this->beginWidget('CActiveForm', array(
    'id' => $formID,
    'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype'=>'multipart/form-data'),
));
?>
<?php echo "<?php"; ?> echo $this->renderPartial('//cms/default/_errorSummary', array('error' => $form->errorSummary($model))); ?>

<?php echo "<?php"; ?> echo CHtml::hiddenField('apply', 0); ?>
<?php if (in_array('image', $columnList)): ?>
<?php echo "<?php"; ?> echo $form->hiddenField($model, 'image', array('value' => '')); ?>
<?php endif; ?>

<div class="box grid_16 tabs">
    <ul class="tab_header clearfix">
        <li><a href="#tab_general"><?php echo "<?php"; ?> echo Yii::t('global', 'General'); ?></a></li>
    <?php if ($this->hasMedia == '1' OR $this->hasComment == '1' OR $this->hasChildren == '1') : ?>
    <?php echo "<?php"; ?> if (!$model->isNewRecord) : ?>
        <?php if($this->hasMedia == '1'): ?>
        <li><a href="#tab_media"><?php echo "<?php"; ?> echo Yii::t('global', 'Media'); ?></a></li>
        <?php endif; ?>
        <?php if($this->hasComment == '1'): ?>
        <li><a href="#tab_comment"><?php echo "<?php"; ?> echo Yii::t('global', 'Comment'); ?></a></li>
        <?php endif; ?>
        <?php if($this->hasChildren == '1' AND $this->TypeObject == 'main'): ?>
        <li><a href="#tab_children"><?php echo "<?php"; ?> echo Yii::t('global', '<?php echo ucfirst($this->children_controller); ?>'); ?></a></li>
        <?php endif; ?>
    <?php echo "<?php"; ?> endif; ?>
    <?php endif; ?>
    <?php if (in_array('seo_title', $columnList) AND in_array('seo_url', $columnList) AND in_array('seo_desc', $columnList)): ?>
        <li><a href="#tab_seo"><?php echo "<?php"; ?> echo Yii::t('global', 'SEO'); ?></a></li>
    <?php endif; ?>
    </ul>
    <a href="#" class="grabber"></a>
    <a href="#" class="toggle"></a>
    <div class="toggle_container">
        <div id="tab_general" class="block">
            <?php echo "<?php"; ?> echo $this->renderPartial('_formContent', array('form' => $form, 'model' => $model), true); ?>
        </div>
    <?php if ($this->hasMedia == '1' OR $this->hasComment == '1' OR $this->hasChildren == '1') : ?>
    <?php echo "<?php"; ?> if (!$model->isNewRecord) : ?>
        <?php if($this->hasMedia == '1'): ?>
    
        <div id="tab_media" class="block">
            <?php echo "<?php"; ?> $this->widget('application.modules.media.widgets.backend.Media', array(
                'module' => $this->module->id,
                'itemId' => $model-><?php echo $this->tableSchema->primaryKey; ?>,
            )); ?>
        </div>
        <?php endif; ?>
        <?php if($this->hasComment == '1'): ?>
    
        <div id="tab_comment" class="block">
            <?php echo "<?php"; ?> $this->widget('application.modules.comment.widgets.backend.Comment', array(
                'module' => $this->module->id,
                'itemId' => $model-><?php echo $this->tableSchema->primaryKey; ?>,
            )); ?>
        </div>
        <?php endif; ?>
        <?php if($this->hasChildren == '1' AND $this->TypeObject == 'main'): ?>
    
        <div id="tab_children" class="block">
            <?php echo "<?php"; ?> $this->widget('application.modules.<?php echo $this->moduleName; ?>.widgets.backend.<?php echo ucfirst($this->children_controller); ?>', array(
                'itemId' => $model-><?php echo $this->tableSchema->primaryKey; ?>,
            )); ?>
        </div>
        <?php endif; ?>
    <?php echo "<?php"; ?> endif; ?>
    <?php endif; ?>
    <?php if (in_array('seo_title', $columnList) AND in_array('seo_url', $columnList) AND in_array('seo_desc', $columnList)): ?>
        <div id="tab_seo" class="block">
            <?php echo "<?php"; ?> echo $this->renderPartial('_formSEO', array('form' => $form, 'model' => $model), true); ?>
        </div>
    </div>
    <?php endif; ?>
</div>
<?php echo "<?php"; ?> $this->endWidget(); ?>

