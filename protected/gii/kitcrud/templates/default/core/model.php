<?php echo '<?php'; ?>

Yii::import('application.modules.<?php echo $this->moduleName; ?>.models.db.Base<?php echo $this->ModelClass; ?>');
class <?php echo $this->ModelClass; ?> extends Base<?php echo $this->ModelClass; ?>{
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

}