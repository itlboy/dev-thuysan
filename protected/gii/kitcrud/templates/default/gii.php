<?php
// Trong nay lay ra 1 mang gom cac config cua Gii danh cho module nay. De sau nay get file nay ra la co ngay config, ko can config lai nua.
// Co the luu tru dang array php hoặc json

$data = array(
    'moduleName' => $this->moduleName,    
    
    'tablePrefix' => $this->tablePrefix,
    'main_tableName' => $this->main_tableName,        
    'main_controller' => $this->main_controller,
    'main_baseClass' => $this->main_baseClass,
    
	'hasCategory' => $this->hasCategory,
	'hasOption' => $this->hasOption,
	'hasComment' => $this->hasComment,
	'hasMedia' => $this->hasMedia,
	'hasChildren' => $this->hasChildren,

    'children_tableName' => $this->children_tableName,
    'children_controller' => $this->children_controller,
    'children_baseClass' => $this->children_baseClass,
    'children_fieldCreate' => $this->children_fieldCreate,
    'children_fieldUpdate' => $this->children_fieldUpdate
);

//echo json_encode($data);

//echo $results = print_r($data, true);
//echo $results = serialize($data);
echo "<?php\n";
echo 'return ';
echo var_export($data);
echo ';';