<?php
$class=get_class($model);
//Yii::app()->clientScript->registerScript('gii.crud',"
//$('#{$class}_controller').change(function(){
//	$(this).data('changed',$(this).val()!='');
//});
//$('#{$class}_model').bind('keyup change', function(){
//	var controller=$('#{$class}_controller');
//	if(!controller.data('changed')) {
//		var id=new String($(this).val().match(/\\w*$/));
//		if(id.length>0)
//			id=id.substring(0,1).toLowerCase()+id.substring(1);
//		controller.val(id);
//	}
//});
//");
?>
<h1>KitCrud Generator</h1>

<p>Công cụ tạo CRUD cho hệ thống LetKit</p>

<?php $form=$this->beginWidget('CCodeForm', array('model'=>$model)); ?>

<!-- ////////////////////////////////////// Main ////////////////////////////////////// -->
	<div class="row">
		<?php echo $form->labelEx($model,'moduleName'); ?>
		<?php echo $form->textField($model,'moduleName',array('size'=>65)); ?>
		<div class="tooltip">
			Tên Module (ví dụ <code>film</code>)
		</div>
		<?php echo $form->error($model,'moduleName'); ?>
        <a href="javascript:;" id="loadGiiConfig">Get cấu hình</a>
	</div>
    <?php
    Yii::app()->clientScript->registerCoreScript('bbq');
    Yii::app()->clientScript->registerScript('loadGiiConfig', "
        jQuery(document).ready(function () {
            jQuery('#loadGiiConfig').click(function(){            
                window.location.replace(jQuery.param.querystring(jQuery(location).attr('href'), 'module='+jQuery('#KitcrudCode_moduleName').val()));
            });
        });
    ");
    ?>

    <div class="row sticky">
		<?php echo $form->labelEx($model,'tablePrefix'); ?>
		<?php echo $form->textField($model,'tablePrefix', array('size'=>65)); ?>
		<div class="tooltip">
		This refers to the prefix name that is shared by all database tables.
		Setting this property mainly affects how model classes are named based on
		the table names. For example, a table prefix <code>tbl_</code> with a table name <code>tbl_post</code>
		will generate a model class named <code>Post</code>.
		<br/>
		Leave this field empty if your database tables do not use common prefix.
		</div>
		<?php echo $form->error($model,'tablePrefix'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'main_tableName'); ?>
		<?php echo $form->textField($model,'main_tableName', array('size'=>65)); ?>
		<div class="tooltip">
		This refers to the table name that a new model class should be generated for
		(e.g. <code>tbl_user</code>). It can contain schema name, if needed (e.g. <code>public.tbl_post</code>).
		You may also enter <code>*</code> (or <code>schemaName.*</code> for a particular DB schema)
		to generate a model class for EVERY table.
		</div>
		<?php echo $form->error($model,'main_tableName'); ?>
	</div>

    <div class="row">
		<?php echo $form->labelEx($model,'main_controller'); ?>
		<?php echo $form->textField($model,'main_controller',array('size'=>65)); ?>
		<div class="tooltip">
			Controller ID is case-sensitive. CRUD controllers are often named after
			the model class name that they are dealing with. Below are some examples:
			<ul>
				<li><code>post</code> generates <code>PostController.php</code></li>
				<li><code>postTag</code> generates <code>PostTagController.php</code></li>
			</ul>
		</div>
		<?php echo $form->error($model,'main_controller'); ?>
	</div>

	<div class="row sticky">
		<?php echo $form->labelEx($model,'main_baseClass'); ?>
		<?php echo $form->textField($model,'main_baseClass',array('size'=>65)); ?>
		<div class="tooltip">
			This is the class that the new model class will extend from.
			Please make sure the class exists and can be autoloaded.
		</div>
		<?php echo $form->error($model,'main_baseClass'); ?>
	</div>
<!-- ////////////////////////////////////// END Main ////////////////////////////////////// -->

<!-- ////////////////////////////////////// Other ////////////////////////////////////// -->
	<div class="row">
		<?php echo $form->labelEx($model,'hasCategory'); ?>
		<?php echo $form->checkBox($model,'hasCategory'); ?>
		<div class="tooltip">
			Trường này xác nhận xem có Category hay không?
		</div>
		<?php echo $form->error($model,'hasCategory'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hasOption'); ?>
		<?php echo $form->checkBox($model,'hasOption'); ?>
		<div class="tooltip">
			Trường này xác nhận xem có Option hay không?
		</div>
		<?php echo $form->error($model,'hasOption'); ?>
	</div>

    <div class="row">
		<?php echo $form->labelEx($model,'hasComment'); ?>
		<?php echo $form->checkBox($model,'hasComment'); ?>
		<div class="tooltip">
			Trường này xác nhận xem có Comment hay không?
		</div>
		<?php echo $form->error($model,'hasComment'); ?>
	</div>

    <div class="row">
		<?php echo $form->labelEx($model,'hasMedia'); ?>
		<?php echo $form->checkBox($model,'hasMedia'); ?>
		<div class="tooltip">
			Trường này xác nhận xem có Media hay không?
		</div>
		<?php echo $form->error($model,'hasMedia'); ?>
	</div>

<!-- ////////////////////////////////////// END Other ////////////////////////////////////// -->

<!-- ////////////////////////////////////// Children ////////////////////////////////////// -->
    <div class="row">
		<?php echo $form->labelEx($model,'hasChildren'); ?>
		<?php echo $form->checkBox($model,'hasChildren'); ?>
		<div class="tooltip">
			Trường này xác nhận đối tượng con hay không?
		</div>
		<?php echo $form->error($model,'hasChildren'); ?>
	</div>
    
    <div class="row">
		<?php echo $form->labelEx($model,'children_tableName'); ?>
		<?php echo $form->textField($model,'children_tableName', array('size'=>65)); ?>
		<div class="tooltip">Tên bảng của đối tượng con</div>
		<?php echo $form->error($model,'children_tableName'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'children_controller'); ?>
		<?php echo $form->textField($model,'children_controller', array('size'=>65)); ?>
		<div class="tooltip">Controller ID của đối tượng con</div>
		<?php echo $form->error($model,'children_controller'); ?>
	</div>

	<div class="row sticky">
		<?php echo $form->labelEx($model,'children_baseClass'); ?>
		<?php echo $form->textField($model,'children_baseClass', array('size'=>65)); ?>
		<div class="tooltip">
			Base Class của đối tượng con
		</div>
		<?php echo $form->error($model,'children_baseClass'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'children_fieldCreate'); ?>
		<?php echo $form->textField($model,'children_fieldCreate', array('size'=>65)); ?>
		<div class="tooltip">
			Các trường trong form Create
		</div>
		<?php echo $form->error($model,'children_fieldCreate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'children_fieldUpdate'); ?>
		<?php echo $form->textField($model,'children_fieldUpdate', array('size'=>65)); ?>
		<div class="tooltip">
			Các trường trong form Update
		</div>
		<?php echo $form->error($model,'children_fieldUpdate'); ?>
	</div>

<!-- ////////////////////////////////////// END Children ////////////////////////////////////// -->

<?php $this->endWidget(); ?>
