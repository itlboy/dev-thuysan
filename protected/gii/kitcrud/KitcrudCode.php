<?php

class KitcrudCode extends CCodeModel
{
    public $moduleName = 'film';
    
    public $tablePrefix = 'let_';
    public $main_tableName = 'let_kit_film';
    public $main_controller = 'default';
    public $main_baseClass = 'ActiveRecord';
    
	public $hasCategory = true;
	public $hasOption = false;
	public $hasComment = true;
	public $hasMedia = true;
	public $hasChildren = true;

    public $children_tableName = 'let_kit_film_episode';
    public $children_controller = 'episode';
    public $children_baseClass = 'ActiveRecord';
    public $children_fieldCreate = 'title,sorder,link,status';
    public $children_fieldUpdate = 'title,sorder,link,status,intro,hot,promotion,status,trash';

	private $_table;
	private $_tableName;
	private $_modelClass;
	private $_baseClass;
	private $_controller;
	private $_type = 'main'; // main OR children

	public function rules()
	{
		return array_merge(parent::rules(), array(
			array('moduleName, main_tableName, main_controller, main_baseClass, children_tableName, children_baseClass, children_fieldCreate, children_fieldUpdate', 'filter', 'filter'=>'trim'),
//			array('model, controller, baseControllerClass', 'required'),
//			array('model', 'match', 'pattern'=>'/^\w+[\w+\\.]*$/', 'message'=>'{attribute} should only contain word characters and dots.'),
//			array('controller', 'match', 'pattern'=>'/^\w+[\w+\\/]*$/', 'message'=>'{attribute} should only contain word characters and slashes.'),
//			array('baseControllerClass', 'match', 'pattern'=>'/^[a-zA-Z_]\w*$/', 'message'=>'{attribute} should only contain word characters.'),
//			array('baseControllerClass', 'validateReservedWord', 'skipOnError'=>true),
//			array('model', 'validateModel'),
			array('moduleName, tablePrefix, main_tableName, main_controller, main_baseClass, hasCategory, hasOption, hasComment, hasMedia, hasChildren, children_tableName, children_controller, children_baseClass, children_fieldCreate, children_fieldUpdate', 'sticky'),
//			array('tablePrefix, main_baseClass, children_baseClass', 'sticky'),
		));
	}

	public function attributeLabels()
	{
		return array_merge(parent::attributeLabels(), array(
//			'model'=>'Model Class',
//			'controller'=>'Controller ID',
//			'baseControllerClass'=>'Base Controller Class',
//			'relationCategoryModel'=>'Category Model káº¿t ná»‘i',
		));
	}

	public function requiredTemplates()
	{
		return array(
//			'controller.php',
//			'model.php',
		);
	}

	public function init()
	{
		$module = Yii::app()->request->getQuery('module');
        if ($module != NULL) {
            $giiPath = Yii::getPathOfAlias('application.modules.'.$module) . DIRECTORY_SEPARATOR . 'gii.php';        
            if (file_exists($giiPath) AND is_file($giiPath)) {
                $giiData = require_once($giiPath);
                if (is_array($giiData)) {
                    foreach (get_class_vars(__CLASS__) as $key => $val) {
                        if (array_key_exists($key, $giiData)) {
                            $this->$key = $giiData[$key];
                        }
                    }
                }
            }
        }
            
        if(Yii::app()->db===null)
			throw new CHttpException(500,'An active "db" connection is required to run this generator.');
		parent::init();
	}

	public function successMessage()
	{
		$link=CHtml::link('try it now', Yii::app()->createUrl($this->main_controller), array('target'=>'_blank'));
		return "The controller has been generated successfully. You may $link.";
	}

	public function validateModel($attribute,$params)
	{
		if($this->hasErrors('model'))
			return;
		$class = @Yii::import($this->model,true);
		if(!is_string($class) || !$this->classExists($class))
			$this->addError('model', "Class '{$this->model}' does not exist or has syntax error.");
		else if(!is_subclass_of($class,'CActiveRecord'))
			$this->addError('model', "'{$this->model}' must extend from CActiveRecord.");
		else
		{
			$table=CActiveRecord::model($class)->tableSchema;
			if($table->primaryKey===null)
				$this->addError('model',"Table '{$table->name}' does not have a primary key.");
			else if(is_array($table->primaryKey))
				$this->addError('model',"Table '{$table->name}' has a composite primary key which is not supported by crud generator.");
			else
			{
				$this->_modelClass=$class;
				$this->_table=$table;
			}
		}
	}

	public function prepare()
	{
		$this->files=array();
        $this->setObject('main', $this->main_baseClass, $this->main_tableName);

        // Tao cac file ngoai thu muc 'core'
		$files=CFileHelper::findFiles($this->templatePath,array(
			'exclude'=>array('.svn', 'core'),
		));
		foreach($files as $file)
		{
            if(CFileHelper::getExtension($file)==='php')
                $content=$this->render($file);
            else if(basename($file)==='.yii') // an empty directory
            {
                $file=dirname($file);
                $content=null;
            }
            else
                $content=file_get_contents($file);
            $this->files[]=new CCodeFile(
                $this->getModulePath().substr($file,strlen($this->templatePath)),
                $content
            );
		}
        
        
        // Module
		$templateFile = $this->templatePath.DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR.'module.php';
$this->files[]=new CCodeFile(
			$this->getModulePath().'/'. ucfirst($this->moduleName) . 'Module' .'.php',
			$this->render($templateFile)
		);

        // Controller
		$this->genController('main', $this->main_baseClass, $this->main_tableName);
		
        // Model
        $this->genModel('main', $this->main_baseClass, $this->main_tableName);
        
        if ($this->hasCategory == '1') {
            $this->genModel('main', $this->main_baseClass, 'let_kit_' . $this->moduleName . '_category', 'BASE');
            
            $templateFile = $this->templatePath.DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR.'modelCategory.php';
            $this->files[] = new CCodeFile(
                $this->getModulePath().'/models/'. $this->ModelClass . '.php',
                $this->render($templateFile)
            );
        }
		
        if ($this->hasOption == '1') {
            $this->genModel('main', $this->main_baseClass, 'let_kit_' . $this->moduleName . '_option', 'BASE');
            
            $templateFile = $this->templatePath.DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR.'modelOption.php';
            $this->files[] = new CCodeFile(
                $this->getModulePath().'/models/'. $this->ModelClass . '.php',
                $this->render($templateFile)
            );
        }

        // View
		$this->genView('main', $this->main_baseClass, $this->main_tableName);
		
        ///////////////////////////////////////////// Children /////////////////////////////////////////////
        if ($this->hasChildren == '1') {
            // Controller
			$this->genController('children', $this->children_baseClass, $this->children_tableName);

            // Model
            $this->genModel('children', $this->children_baseClass, $this->children_tableName);
            
			// View
			$this->genView('children', $this->children_baseClass, $this->children_tableName);
            
            // Widget
            $this->genWidget('children', $this->children_baseClass, $this->children_tableName, $this->children_controller);            
        }
        ///////////////////////////////////////////// END Children /////////////////////////////////////////////
		
		sort($this->files);
	}

	public function genView($type, $baseClass, $tableName)
	{
        $this->setObject($type, $baseClass, $tableName);

        $params=array(
            'columns' => $this->_table->columns,
            'labels'=>$this->generateLabels($this->_table),
            'rules'=>$this->generateRules($this->_table),
//            'relations'=>isset($this->relations[$className]) ? $this->relations[$className] : array(),
        );
        
		$files=CFileHelper::findFiles($this->templatePath.DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR.'views',array(
			'exclude'=>array('.svn'),
		));
               
		foreach($files as $file)
		{
            if(CFileHelper::getExtension($file)==='php')
                $content=$this->render($file);
            else if(basename($file)==='.yii') // an empty directory
            {
                $file=dirname($file);
                $content=null;
            }
            else
                $content=file_get_contents($file);
            
            // xu ly neu la children co them ajaxCreate, ajaxUpdate
            if (strpos($file, 'ajax') == FALSE AND $this->TypeObject == 'main') {
                $this->files[]=new CCodeFile(
                    $this->getModulePath().'/views/backend/'.$this->getController().substr($file,strlen($this->templatePath.'/core/views')),
                    $content
                );
            }
            if (strpos($file, 'ajax') == TRUE AND $this->TypeObject == 'children') {
                $this->files[]=new CCodeFile(
                    $this->getModulePath().'/views/backend/'.$this->getController().substr($file,strlen($this->templatePath.'/core/views')),
                    $content
                );
            }
		}
	}

    /**
     *
     * @param string $baseClass
     * @param string $tableName
     * @param string $type ALL | BASE
     */
	public function genModel($type, $baseClass, $tableName, $package = 'ALL')
	{
        $this->setObject($type, $baseClass, $tableName);

        $params=array(
            'columns' => $this->_table->columns,
            'labels'=>$this->generateLabels($this->_table),
            'rules'=>$this->generateRules($this->_table),
//            'relations'=>isset($this->relations[$className]) ? $this->relations[$className] : array(),
        );
        
        // Tao file Model 
        if ($package == 'ALL') {
            $templateFile = $this->templatePath.DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR.'model.php';
            $this->files[] = new CCodeFile(
                $this->getModulePath().'/models/'. $this->ModelClass . '.php',
                $this->render($templateFile, $params)
            );
        }

        // Tao file ModelBase
        $templateFile = $this->templatePath.DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR.'modelbase.php';
		$this->files[] = new CCodeFile(
			$this->getModulePath().'/models/db/Base'. $this->ModelClass . '.php',
			$this->render($templateFile, $params)
		);
	}

	public function genController($type, $baseClass, $tableName)
	{
        $this->setObject($type, $baseClass, $tableName);

        $params=array(
            'columns' => $this->_table->columns,
            'labels'=>$this->generateLabels($this->_table),
            'rules'=>$this->generateRules($this->_table),
//            'relations'=>isset($this->relations[$className]) ? $this->relations[$className] : array(),
        );
		
		$templateFile = $this->templatePath.DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR.'controller.php';
		$this->files[] = new CCodeFile(
			$this->getModulePath().'/controllers/backend/'. ucfirst($this->_controller) . 'Controller' .'.php',
			$this->render($templateFile, $params)
		);

	}
    
    /**
     *
     * @param string $baseClass
     * @param string $tableName
     * @param string $type ALL | BASE
     */
	public function genWidget($type, $baseClass, $tableName, $childrenController)
	{
        $this->setObject($type, $baseClass, $tableName);

        $params=array(
            'columns' => $this->_table->columns,
        );

        // Tao file ModelBase
        $templateFile = $this->templatePath.DIRECTORY_SEPARATOR.'core'.DIRECTORY_SEPARATOR.'widget.php';
		$this->files[] = new CCodeFile(
			$this->getModulePath().'/widgets/backend/'. ucfirst($childrenController) . '.php',
			$this->render($templateFile, $params)
		);
	}

	public function setObject($type, $baseClass, $tableName)
	{
        $this->_type = $type;
        $this->_baseClass = $baseClass;
        $this->_tableName = $tableName;
        $this->_table = Yii::app()->db->getSchema()->getTable($this->_tableName);
        $this->_modelClass = $this->convertToModelClass($this->_tableName);
		if ($this->_type == 'main')
        	$this->_controller = $this->main_controller;
		else
			$this->_controller = $this->children_controller;
	}

	public function generateLabels($table)
	{
		$labels=array();
		foreach($table->columns as $column)
		{
			$label=ucwords(trim(strtolower(str_replace(array('-','_'),' ',preg_replace('/(?<![A-Z])[A-Z]/', ' \0', $column->name)))));
			$label=preg_replace('/\s+/',' ',$label);
			if(strcasecmp(substr($label,-3),' id')===0)
				$label=substr($label,0,-3);
			if($label==='Id')
				$label='ID';
			$labels[$column->name]=$label;
		}
		return $labels;
	}

    public function generateRules($table)
	{
		$rules=array();
		$required=array();
		$integers=array();
		$numerical=array();
		$length=array();
		$safe=array();
		foreach($table->columns as $column)
		{
			if($column->autoIncrement)
				continue;
			$r=!$column->allowNull && $column->defaultValue===null;
			if($r)
				$required[]=$column->name;
			if($column->type==='integer')
				$integers[]=$column->name;
			else if($column->type==='double')
				$numerical[]=$column->name;
			else if($column->type==='string' && $column->size>0)
				$length[$column->size][]=$column->name;
			else if(!$column->isPrimaryKey && !$r)
				$safe[]=$column->name;
		}
		if($required!==array())
			$rules[]="array('".implode(', ',$required)."', 'required')";
		if($integers!==array())
			$rules[]="array('".implode(', ',$integers)."', 'numerical', 'integerOnly'=>true)";
		if($numerical!==array())
			$rules[]="array('".implode(', ',$numerical)."', 'numerical')";
		if($length!==array())
		{
			foreach($length as $len=>$cols)
				$rules[]="array('".implode(', ',$cols)."', 'length', 'max'=>$len)";
		}
		if($safe!==array())
			$rules[]="array('".implode(', ',$safe)."', 'safe')";

		return $rules;
	}

    public function removePrefix($table)
	{
        return $modelClass = str_replace($this->tablePrefix, '', $table);
	}

	public function getTableName()
	{
		return $this->removePrefix($this->_tableName);
	}

	public function convertToModelClass($table)
	{
        $modelClass = $this->removePrefix($table);
        $modelClass = str_replace('_', ' ', $modelClass);
        $modelClass = ucwords($modelClass);
        return $modelClass = str_replace(' ', '', $modelClass);
	}

	public function getModelClass()
	{
		return $this->convertToModelClass($this->_tableName);
	}

	public function getController()
	{
		return $this->_controller;
	}

	public function getBaseClass()
	{
		return $this->_baseClass;
	}

	public function getTypeObject()
	{
		return $this->_type;
	}

	public function getModelFile()
	{
        $module=$this->getModule();
		$modelPath = YiiBase::getPathOfAlias($this->model);
		return $modelPath . '.php';
	}

	public function getModelCategoryFile()
	{
        $module=$this->getModule();
		$modelPath = YiiBase::getPathOfAlias($this->model.'Category');
		return $modelPath . '.php';
	}
    
	public function getModelOptionFile()
	{
        $module=$this->getModule();
		$modelPath = YiiBase::getPathOfAlias($this->model.'Option');
		return $modelPath . '.php';
	}

	public function getTableSchema()
	{
		return $this->_table;
	}

	public function getModulePath()
	{
		return Yii::app()->getModulePath().DIRECTORY_SEPARATOR.$this->moduleName;
	}

	public function getViewPath()
	{
		return $this->getModule()->getViewPath().'/backend/'.$this->getController();
	}

	public function generateInputLabel($modelClass,$column)
	{
		return "CHtml::activeLabelEx(\$model,'{$column->name}')";
	}

	public function generateInputField($modelClass,$column)
	{
		if($column->type==='boolean')
			return "CHtml::activeCheckBox(\$model,'{$column->name}')";
		else if(stripos($column->dbType,'text')!==false)
			return "CHtml::activeTextArea(\$model,'{$column->name}',array('rows'=>6, 'cols'=>50))";
		else
		{
			if(preg_match('/^(password|pass|passwd|passcode)$/i',$column->name))
				$inputField='activePasswordField';
			else
				$inputField='activeTextField';

			if($column->type!=='string' || $column->size===null)
				return "CHtml::{$inputField}(\$model,'{$column->name}')";
			else
			{
				if(($size=$maxLength=$column->size)>60)
					$size=60;
				return "CHtml::{$inputField}(\$model,'{$column->name}',array('size'=>$size,'maxlength'=>$maxLength))";
			}
		}
	}

	public function generateActiveLabel($modelClass,$column)
	{
		return "\$form->labelEx(\$model,'{$column->name}')";
	}

	public function generateActiveField($modelClass,$column)
	{
		if($column->type==='boolean')
			return "\$form->checkBox(\$model,'{$column->name}')";
		else if(stripos($column->dbType,'text')!==false)
			return "\$form->textArea(\$model,'{$column->name}',array('rows'=>6, 'cols'=>50))";
		else
		{
			if(preg_match('/^(password|pass|passwd|passcode)$/i',$column->name))
				$inputField='passwordField';
			else
				$inputField='textField';

			if($column->type!=='string' || $column->size===null)
				return "\$form->{$inputField}(\$model,'{$column->name}')";
			else
			{
				if(($size=$maxLength=$column->size)>60)
					$size=60;
				return "\$form->{$inputField}(\$model,'{$column->name}',array('size'=>$size,'maxlength'=>$maxLength))";
			}
		}
	}

	public function guessNameColumn($columns)
	{
		foreach($columns as $column)
		{
			if(!strcasecmp($column->name,'name'))
				return $column->name;
		}
		foreach($columns as $column)
		{
			if(!strcasecmp($column->name,'title'))
				return $column->name;
		}
		foreach($columns as $column)
		{
			if($column->isPrimaryKey)
				return $column->name;
		}
		return 'id';
	}
}