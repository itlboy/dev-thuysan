<?php echo "<?php\n"; ?>
/**
 * Module: <?php echo $this->moduleID."\n"; ?>
 * Auth:
 * Date: <?php echo date('Y-m-d h:i:s')."\n"; ?>
 */
class DefaultController extends ControllerFrontend
{
	public function actionIndex()
	{
		$this->render('index');
	}
}