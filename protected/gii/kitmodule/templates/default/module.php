<?php echo "<?php\n"; ?>

class <?php echo $this->moduleClass; ?> extends CWebModule
{
<?php // if($this->hasCategory == '1'): ?>
    public $category = TRUE; // Xác định Module có category hay không
<?php // endif; ?>
<?php // if($this->hasOption == '1'): ?>
    public $option = TRUE; // Xác định Module có option hay không
<?php // endif; ?>
    public $menuList = array(
<?php // if($this->hasCategory == '1'): ?>
        array(
            'name' => 'Tạo danh mục',
            'url' => '/category/default/create/module/<?php echo $this->moduleID; ?>',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'type' => 'dialog',
            'grid' => '',
        ),
        array(
            'name' => 'Quản lý danh mục',
            'url' => '/category/default/index/module/<?php echo $this->moduleID; ?>',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Quản lý danh mục bài viết',
            'type' => 'direct'
        ),
<?php // endif; ?>
<?php // if($this->hasOption == '1'): ?>
        array(
            'name' => 'Tạo option',
            'url' => '/option/default/create/module/<?php echo $this->moduleID; ?>',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'type' => 'dialog',
            'grid' => '',
        ),
        array(
            'name' => 'Quản lý option',
            'url' => '/option/default/index/module/<?php echo $this->moduleID; ?>',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Quản lý option bài viết',
            'type' => 'direct'
        ),
<?php // endif; ?>
        array(
            'name' => 'Tạo <?php echo $this->moduleID; ?>',
            'url'  => '/<?php echo $this->moduleID; ?>/default/create',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Tạo <?php echo $this->moduleID; ?>',
            'type' => 'direct'
        ),
        array(
            'name' => 'Quản lý <?php echo $this->moduleID; ?>',
            'url'  => '/<?php echo $this->moduleID; ?>/default/index',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Quản lý <?php echo $this->moduleID; ?>',
            'type' => 'direct'
        ),
    );
    public $image = array(
        'origin' => array(
            'type' => 'move',
            'folder' => 'origin',
        ),
        'large' => array(
            'type' => 'resize',
            'width' => 250,
            'height' => 370,
            'cropWidth' => NULL,
            'cropHeight' => NULL,
            'cropTop' => NULL,
            'cropLeft' => NULL,
            'rotate' => 0, // Xoay anh
            'flip' => NULL, //[integer] Direction. Horizontal = 5, Vertical = 6
            'quality' => 70,
            'folder' => 'large',
        ),
        'medium' => array(
            'type' => 'resize',
            'width' => 130,
            'height' => 185,
            'cropWidth' => NULL,
            'cropHeight' => NULL,
            'cropTop' => NULL,
            'cropLeft' => NULL,
            'rotate' => 0, // Xoay anh
            'flip' => NULL, //[integer] Direction. Horizontal = 5, Vertical = 6
            'quality' => 80,
            'folder' => 'medium',
        ),
        'small' => array(
            'type' => 'resize',
            'width' => 43,
            'height' => 55,
            'cropWidth' => NULL,
            'cropHeight' => NULL,
            'cropTop' => NULL,
            'cropLeft' => NULL,
            'rotate' => 0, // Xoay anh
            'flip' => NULL, //[integer] Direction. Horizontal = 5, Vertical = 6
            'quality' => 100,
            'folder' => 'small',
        ),
    );

    public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'<?php echo $this->moduleID; ?>.models.*',
			'<?php echo $this->moduleID; ?>.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
