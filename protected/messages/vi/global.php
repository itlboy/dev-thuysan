<?php
return array(
    // Module
    'article' => 'Bài viết',
    'category' => 'Danh mục',
    'product' => 'Sản phẩm',
    'gallery' => 'Showroom hình ảnh',
    'account' => 'Tài khoản',
    'currency' => 'Tiền tệ',
    'cms' => 'Tổng quan',
    'option' => 'Lựa chọn',
    'comment' => 'Bình luận',
    'media' => 'Truyền thông',
    'ads' => 'Quảng cáo',
    'film' => 'Phim',
    'classiads' => 'Rao vặt',
    'poll' => 'Thăm dò ý kiến',
    'location' => 'Khu vực',
    'date' => 'Hẹn hò',
    'contact' => 'Liên hệ',
    'gii' => 'Tạo chức năng',
    'crawler' => 'Thu thập thông tin',


    // module's label
    'Publish' => 'Kích hoạt',
    'Unpublish' => 'Không kích hoạt',
    'Promotion' => 'Tiêu điểm',
    'Demotion' => 'Không tiêu điểm',
    'Trash' => 'Thùng rác',
    'Delete' => 'Xóa',
    
    // Datetime
    'Monday' => 'Thứ 2',
    'Tuesday' => 'Thứ 3',
    'Wednesday' => 'Thứ 4',
    'Thursday' => 'Thứ 5',
    'Friday' => 'Thứ 6',
    'Saturday' => 'Thứ 7',
    'Sunday' => 'Chủ nhật',

    // currency
    'letgold' => 'Let Xu',
);
?>