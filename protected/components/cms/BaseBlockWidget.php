<?php
class BaseBlockWidget extends CWidget {
    var $views = array();
    var $activatedView = "default";
    
    public function run() {
        Yii::log("Run ".get_class($this)." block ", "trace");
        $this->render($this->activatedView);
    }
}
?>