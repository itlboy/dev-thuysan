<?php
Yii::import('application.modules.cms.models.KitBlock');
class BasePosition {
    var $name="";
    
    function __construct($name) {
        $this->name = $name;
    }
    
    public function getBlocks($itemId) {
        $criteria = new CDbCriteria();
        $criteria->condition = "position=:position AND category_id=:item_id";
        $criteria->order = "sorder";
        $criteria->params = array(":position" => $this->name, ":item_id" => $itemId);
        
        return KitCategoryBlock::model()->findAll($criteria);
    }
    
}
?>