<?php
class BaseLayoutWidget extends CWidget {
    var $itemId;    //
    
    var $positions = array();
    
    public function run() {
        $viewName   = $this->getName();
//        $this->render(lcfirst($viewName));
        $this->render($viewName);
    }
    
    public function getName() {
        $className  = get_class($this);
        $viewName   = substr($className, 0, strlen($className)-6); // 6 = strlen("Layout")
//        return lcfirst($viewName);
        return $viewName;
    }
    
    public function hasPosition($positionName) {
        $position = new BasePosition($positionName);
        $blocks = $position->getBlocks($this->itemId);
        return !empty ($blocks);
    }
    
    public function displayPosition($positionName) {
        Yii::log("Display possition $positionName", "trace");
        $position = new BasePosition($positionName);
        $blocks = $position->getBlocks($this->itemId);
        foreach($blocks as $block) {
            // hien thi block tuong ung
            CBaseController::widget($block->classPath,$block->decodeParams());
        }
        
    }
}
?>