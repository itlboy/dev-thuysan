<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class ControllerFrontend extends Controller {

    public $checkLogin = FALSE;

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function init() {
        parent::init();
        if (isset($_GET['language']) AND ! empty($_GET['language'])) {
            $language = new CHttpCookie("language", $_GET['language']);
            $language->expire = time() + (60 * 60 * 24 * 7);
            Yii::app()->request->cookies["language"] = $language;
        } elseif (!isset(Yii::app()->request->cookies["language"]) OR empty(Yii::app()->request->cookies["language"])) {
            $language = new CHttpCookie("language", 'vi');
            $language->expire = time() + (60 * 60 * 24 * 7);
            Yii::app()->request->cookies["language"] = $language;
        }
        Yii::app()->language = Yii::app()->request->cookies["language"]->value;
    }

    public function beforeRender($view) {

        if (($module = $this->getModule()) !== null) {
            $moduleViewPath = $module->getViewPath();
            if (isMobileDevice()) {
                
            } else {
                $module->setViewPath($moduleViewPath . DIRECTORY_SEPARATOR . "frontend");
            }
        }
        return parent::beforeRender($view);
    }

    public function render($view, $data = null, $return = false) {
        if ($this->beforeRender($view)) {
//        if(true){
            // Kiem tra xem da dang nhap chua, neu chua thi goi view cua trang dang nhap
            if ($this->checkLogin == TRUE AND Yii::app()->user->isGuest) {
                $this->layout = '//layouts/login';
                $view = '//account/id/login';
                $data['model'] = new LoginFormId;
            }

            $output = $this->renderPartial($view, $data, true);
            if (($layoutFile = $this->getLayoutFile($this->layout)) !== false)
                $output = $this->renderFile($layoutFile, array('content' => $output), true);

            $this->afterRender($view, $output);

            $output = $this->processOutput($output);

            if ($return)
                return $output;
            else
                echo $output;
        }
    }

}
