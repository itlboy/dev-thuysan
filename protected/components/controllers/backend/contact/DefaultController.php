<?php

class DefaultController extends ControllerBackend
{
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id) {
		$model=KitContact::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

    public function actionIndex() {
        $model = new KitContact('search');
        $model->unsetAttributes();  // clear any default values

        // Option
        if (Yii::app()->request->getQuery('option_id') !== NULL) {
            $model->option_id = Yii::app()->request->getQuery('option_id');
        }

        if (isset($_GET['KitContact'])) {
            $model->attributes = $_GET['KitContact'];
        }

        // page size drop down changed
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$model = new KitContact;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$this->save($model);

		$this->render('edit',array(
			'model'=>$model
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id) {
		$model=$this->loadModel($id);
                $optionsOld = $model->options = KitContactOption::model()->get6a9fdd972Option($model->id);
    
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

            $this->save($model, $optionsOld);

		$this->render('edit',array(
			'model' => $model
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionInfo($id) {
        $model = KitContact::getDetails($id);

		$this->render('info',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

    
	protected function save($model, $optionsOld = NULL) {
		if(isset($_POST['KitContact'])) {
            // Upload image
			if ($_POST['KitContact']['image'] !== NULL AND $_POST['KitContact']['image'] !== '') {
                $_POST['KitContact']['image'] = Common::createThumb($this->module->getName() , $_POST['KitContact']['image'], letArray::get($_POST['KitContact'], 'title', ''));
            }

			if ($_POST['KitContact']['from_time'] == NULL OR $_POST['KitContact']['from_time'] == '') {
                unset($_POST['KitContact']['from_time']);
            }

            if ($_POST['KitContact']['to_time'] == NULL OR $_POST['KitContact']['to_time'] == '') {
                unset($_POST['KitContact']['to_time']);
            }

            $model->attributes = $_POST['KitContact'];

                            $optionsNew = $model->options;
        
            if ($model->validate()) {
                if($model->save()) {
                    $itemId = $model->id;
                    if (!$model->isNewRecord)
                        KitContactOption::delete6a9fdd97Option($optionsOld, $itemId);
                    KitContactOption::create6a9fdd97Option($optionsNew, $itemId);

                    if (Yii::app()->request->getPost('apply') == 0)
                         $this->redirect(array('index'));
                    else $this->redirect(array('update', 'id' => $model->id));
                }
            }
		}
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model) {
		if(isset($_POST['ajax']) && $_POST['ajax']==='kit-contact-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
