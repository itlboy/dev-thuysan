<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
//class ControllerBackend extends SBaseController
class ControllerBackend extends Controller
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/main';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

    public function beforeAction($action)
    {
//        echo "module=".$this->getModule()->name."; controller=".$this->getId()."; action=".$action->getId();
//        die();
        if (parent::beforeAction($action)) { 
//            Yii::app()->theme = 'backend/' . Yii::app()->params['themeBackend'];
            
            if ((Yii::app()->controller->id !== 'backend/login' AND Yii::app()->controller->id !== 'backend/logout') AND (Yii::app()->user->isGuest OR !in_array(8, json_decode(Yii::app()->user->getState('group'))))) {
                $this->redirect(array('//account/logout'));
            }
            
            return true;
        } else return false;
    }
    
    public function beforeRender($view) {
        if ($this->module->getName() == 'srbac') return parent::beforeRender($view);
        
        if(($module=$this->getModule())!==null) {
            $moduleViewPath=$module->getViewPath();
            $module->setViewPath($moduleViewPath.DIRECTORY_SEPARATOR."backend");
        }
        
        return parent::beforeRender($view);
    }

}