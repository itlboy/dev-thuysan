<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class BackendFunctions {
    
    public static function getStatusOptions() {
        return array(
            1 => Yii::t('global', 'Publish'),
            0 => Yii::t('global', 'Unpublish')
        );
    }
    
    public static function getBooleanIcon($module, $model, $id, $field, $value, $grid) { //$action, $id, $status
        $data = array(
            0 => array('icon' => '/images/icons/system/unpublish_16.png', 'title' => Yii::t('global', 'Unpublish')),
            1 => array('icon' => '/images/icons/system/publish_16.png', 'title' => Yii::t('global', 'Publish')),
        );
        $key = (key_exists($value, $data)) ? $value : 0;
        $url = Yii::app()->createUrl('cms/ajax/ChangeBooleanValue');
        $imageIcon = '<img src="' . Yii::app()->theme->baseUrl . $data[$key]['icon'] . '" height="16" width="16" title="' . $data[$key]['title'] . '">';
        return CHtml::link($imageIcon, 'javascript:;', array(
            'onclick' => "js:ajaxChangeStatusValue('{$module}', '{$model}', '{$id}', '{$field}', '{$value}', '{$url}', '{$grid}'); return false"
        ));
    }
    
    public static function getStatusLink($action, $id, $status) {
        $data = array(
            0 => array('icon' => '/common/icon/icon_16_unpublish.png', 'title' => Yii::t('default', 'Unpublish')),
            1 => array('icon' => '/common/icon/icon_16_published.png', 'title' => Yii::t('default', 'Published')),
            2 => array('icon' => '/common/icon/icon_16_trash.png', 'title' => Yii::t('default', 'Trash')),
        );
        $key = (key_exists($status, $data)) ? $status : 0;
        $url = Yii::app()->createUrl('//' . Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/' . $action);
        $imageIcon = '<img class="status" id="statusContext' . $id . '" src="' . Yii::app()->theme->baseUrl . $data[$key]['icon'] . '" height="16" width="16" title="' . $data[$key]['title'] . '">';
        echo CHtml::link($imageIcon, 'javascript:;', array('rel' => $url));
    }
}