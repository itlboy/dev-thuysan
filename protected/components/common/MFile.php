<?php

class MFile {

    public static function remove($file) {
        if (file_exists($file) && is_file($file)) {
            @chmod($file, 0755);
            return (unlink($file)) ? TRUE : FALSE;
        } else return FALSE;
    }

    public static function removeDir($dir) {
        function delSelf($dir) {
            $track = array();
            if (is_dir($dir)) {
                $result = array_diff(scandir($dir), array('.', '..'));
                foreach ($result as $item) {
                    if (!@unlink($dir . '/' . $item))
                        delSelf($dir . '/' . $item);
                }
                if (!rmdir($dir))
                    $track[] = FALSE;
            }
            return $track;
        }

        return (in_array(FALSE, delSelf($dir))) ? FALSE : TRUE;
    }

    // Liệt kê danh sách tất cả các file có trong folder (không bao gồm folder)
    public static function listFileInDir($dir, $backlist = array()) {
        $dh = opendir($dir);
        $files = array();
        while (($file = readdir($dh)) !== false) {
            if ($file !== '.' && $file !== '..' && !in_array($file, $backlist)) {
                $files[] = $file;
            }
        }
        return $files;
    }

    // Xóa tất cả các file trong folder (Không bao gồm forder con)
    // $deleteDir = if true delete also $dir, if false leave it alone
    public static function removeFileInDir($dir, $deleteDir = FALSE) {
        if (!$dh = @opendir($dir))
            return;
        while (false !== ($obj = readdir($dh))) {
            if ($obj == '.' || $obj == '..')
                continue;
            if (!@unlink($dir . '/' . $obj))
                self::removeFileInDir($dir . '/' . $obj, true);
        }
        if ($deleteDir) {
            closedir($dh);
            @rmdir($dir);
        }
    }

    public static function autoName($prefix = '') {
        $prefix = ($prefix == '') ? '' : substr(self::convertToSafeName($prefix), 0, 63) . '-';
        return $prefix . md5($_SERVER['REMOTE_ADDR'] . microtime() . mt_rand());
    }

    /**
     * Tạo src tới nơi chứa ảnh được lưu
     * @param type $dir: moduleID
     * @param type $file
     * @return type
     */
    public static function autoPathView($file, $dir, $dirSub = 'origin') {
        return Yii::app()->baseUrl . '/' . Yii::app()->params['uploadDir'] . '/' . $dir . '/' . $dirSub . '/' . $file;
    }

    public static function convertToSafeName($str) {
        $str = trim(mb_strtolower($str, 'UTF-8'));
        $strFind = array(
            '- ',' ','đ',
            'á','à','ạ','ả','ã','ă','ắ','ằ','ặ','ẳ','ẵ','â','ấ','ầ','ậ','ẩ','ẫ',
            'ó','ò','ọ','ỏ','õ','ô','ố','ồ','ộ','ổ','ỗ','ơ','ớ','ờ','ợ','ở','ỡ',
            'é','è','ẹ','ẻ','ẽ','ê','ế','ề','ệ','ể','ễ',
            'ú','ù','ụ','ủ','ũ','ư','ứ','ừ','ự','ử','ữ',
            'í','ì','ị','ỉ','ĩ',
            'ý','ỳ','ỵ','ỷ','ỹ');
        $strReplace = array(
            '','-','d',
            'a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a','a',
            'o','o','o','o','o','o','o','o','o','o','o','o','o','o','o','o','o',
            'e','e','e','e','e','e','e','e','e','e','e',
            'u','u','u','u','u','u','u','u','u','u','u',
            'i','i','i','i','i',
            'y','y','y','y','y');
        return preg_replace('/[^a-z0-9\-\.]+/i', '', str_replace($strFind, $strReplace, $str));
        //return preg_replace('/[^a-z0-9]+/i', '', str_replace($strFind, $strReplace, $str));
    }

    /**
     * Tạo đường link lưu trong database
     * Luôn luôn dùng $date = date('Y/m') trước khi thao tác với tập tin được upload
     * @param type $file
     * @param type $date
     * @return type
     */
    public static function autoPathSave($file, $date) {
        return $date . '/' . $file;
    }

    public static function autoPathStore($file, $dir, $dirSub = 'origin', $date = NULL) {
        $path = Yii::app()->params['uploadPath'] . DIRECTORY_SEPARATOR . Yii::app()->params['uploadDir'] . DIRECTORY_SEPARATOR . $dir;
        //$path = Yii::getPathOfAlias('webroot.' . Yii::app()->params['uploadDir']) . DIRECTORY_SEPARATOR . $dir;

        if (!is_dir($path)) {
            mkdir($path);
            chmod($path, 0755);
        }

        $path .= DIRECTORY_SEPARATOR . $dirSub;
        if (!is_dir($path)) {
            mkdir($path);
            chmod($path, 0755);
        }

        if ($date != NULL) {
            $dirArray = explode('/', $date);
            foreach ($dirArray as $dirName) {
                $path .= DIRECTORY_SEPARATOR . $dirName;
                if (!is_dir($path)) {
                    mkdir($path);
                    chmod($path, 0755);
                }
            }
        }
        return $path . DIRECTORY_SEPARATOR . $file;
    }

    public static function listBacklist() {
        return array(
            '.html', '.htm', '.php', '.php2', '.php3', '.php4', '.php5', '.phtml', '.pwml', '.inc',
            '.asp', '.aspx', '.ascx', '.jsp', '.cfm', '.cfc', '.pl', '.bat', '.exe', '.com', '.dll',
            '.vbs', '.js', '.reg', '.cgi', '.htaccess', '.asis', '.sh', '.shtml', '.shtm', '.phtm'
        );
    }

    public static function listMimetype() {
        return array(
            'IMAGE' => array(
                'jpe' => 'image/jpeg',
                'jpeg' => 'image/jpeg',
                'jpg' => 'image/jpeg',
                'gif' => 'image/gif',
                'png' => 'image/png',

//                'ico' => 'image/x-icon',
//                'tif' => 'image/tiff',
//                'tiff' => 'image/tiff',
//                'svg' => 'image/svg+xml',
//                'svgz' => 'image/svg+xml',
            ),
            'FLASH' => array(
                'swf' => 'application/x-shockwave-flash',
            ),
            'VIDEO' => array(
                'mp4' => 'video/mp4',
                'avi' => 'video/x-msvideo',
                'flv' => 'video/x-flv',
                'wmv' => 'video/x-ms-wmv',
                'mpeg' => 'video/mpeg',
            ),
            'AUDIO' => array(
                'wma' => 'audio/x-ms-wma',
            ),
            'ARCHIVE' => array(
                'zip' => 'application/zip',
                'rar' => 'application/x-rar-compressed',
                '7z' => 'application/x-7z-compressed',
                'gz' => 'application/x-gzip',
            ),
            'DOCUMENT' => array(
                'pdf' => 'application/pdf',
                'xps' => 'application/vnd.ms-xpsdocument',
                'prc' => 'application/x-mobipocket-ebook',
                'txt' => 'text/plain',
                'rtf' => 'application/rtf',

                'doc' => 'application/msword',
                'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',

                'dotx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',

                'xls' => 'application/vnd.ms-excel',
                'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',

                'xltx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.template',

                'ppt' => 'application/vnd.ms-powerpoint',
                'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',

                'potx' => 'application/vnd.openxmlformats-officedocument.presentationml.template',

                'odt' => 'application/vnd.oasis.opendocument.text',
                'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
            ),
            'OTHER' => array(

            ),
        );
    }

    public static function parseYoutubeId($url) {
        $result = NULL;
        preg_match('#(\.be/|/embed/|/v/|/vi/|/1/|/5/|/watch\?v=|/?v=|/watch\?vi=|/?vi=)([A-Za-z0-9_-]{5,11})#', $url, $matches);
        if(isset($matches[2]) && $matches[2] != ''){
            $result = $matches[2];
        }

//        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match)) {
//            return $match[1];
//        }

        return $result;
    }

    public static function parseSize($bytes) {
        if (mb_strlen($bytes) == 0)
            return '';

        $size = $bytes / 1024;
        if ($size < 1024) {
            $size = number_format($size, 2);
            $size .= ' KB';
        } elseif ($size / 1024 < 1024) {
            $size = number_format($size / 1024, 2);
            $size .= ' MB';
        } else if ($size / 1024 / 1024 < 1024) {
            $size = number_format($size / 1024 / 1024, 2);
            $size .= ' GB';
        }
        return $size;
    }
}
