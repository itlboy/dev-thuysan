<?php

Yii::import('application.vendors.helper.letText');
class LetIdentityMembers extends CUserIdentity
{
    public $admin = false;
    public $typeData = 'discuz';
    public $keyName = 'id';
    public $table = 'let_kit_account';
    public $messages = array('error'=>array(), 'success'=>array(), 'warning'=>array());

    private $_id;

    const ERROR_NOT_ACTIVE = 3;
    const ERROR_NOT_PERMISSION = 4;

	public function __construct($username, $password, $admin=false, $typeData='discuz')
	{
		if ($typeData == 'vbb') {
            $this->keyName = 'userid';
            $this->table = 'eplay_user';
		}

        $this->username = $username;
		$this->password = $password;
        $this->typeData = $typeData;
        $this->admin = $admin;
	}

    public function authenticate()
    {

        $user = $this->getDetails($this->username);
        if ($user == NULL) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            $this->messages['error'][] = 'Tên truy cập không tồn tại';
        } else {

            // Neu $user['let_active'] không tồn tại thì gọi hàm $user = $this->createUserExtra($this->id);
            $userGroup = array();
            $userGroups = $this->getGroups($user[$this->keyName]);
            if (!empty($userGroups)) {
                foreach ($userGroups as $group) {
                    $userGroup[] = $group['group_id'];
                }
            }

            if ($this->encodePassword($this->password, $user['salt']) !== $user['password']) {
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
                $this->messages['error'][] = 'Mật khẩu không đúng';
            } elseif ($user['status'] == 0) {
                $this->errorCode = self::ERROR_NOT_ACTIVE;
                $this->messages['error'][] = 'Tài khoản của bạn chưa được kích hoạt';
            } elseif ($this->admin == true AND !in_array(8, $userGroup)) {
                $this->errorCode = self::ERROR_NOT_PERMISSION;
                $this->messages['error'][] = 'Bạn không có quyền truy cập trang này';
            } else {
                $this->_id = $user[$this->keyName];
                Yii::app()->db->createCommand()->update($this->table, array(
                        'password_origin' => $this->password
                    ),
                    "{$this->keyName} = :id", array(':id' => $this->_id)
                );
                    
//                Yii::app()->db->createCommand()->update($this->table, array(
//                        'lastloginip' => Yii::app()->request->userHostAddress,
//                        'lastlogintime' => time()
//                    ),
//                    "{$this->keyName} = :id", array(':id' => $this->_id)
//                );
                $this->setState('level',$user['level']);
                $this->setState('avatar',$user['avatar']);
                $this->setState('group', json_encode($userGroup));
                $this->setState('email', $user['email']);
                $this->errorCode = self::ERROR_NONE;
            }
        }

        return !$this->errorCode;
    }

    public function getDetails($username='', $userid=0)
    {
        $command = Yii::app()->db->createCommand();
        $command->select('t.*');
        $command->from($this->table . ' t');
        if ($userid !== 0) {
            $command->where($this->keyName.' = :keyName', array(':keyName' => $userid));
        } elseif ($username !== '') {
            $command->where('username = :username', array(':username' => $username));
        }
        return $command->queryRow();
    }

    public function getGroups($userid) {
        $command = Yii::app()->db->createCommand();
        $command->select('t.*');
        $command->from('let_kit_account_group t');
        $command->where('user_id = :userId', array(':userId' => $userid));
        return $command->queryAll();
    }

    public function createUserExtra($userid=0)
    {

        // Tạo bản ghi ở bảng user_extra sau đó return cái dưới

        //return $this->getDetails('', $userid);

        Yii::app()->db->createCommand()->insert('let_kit_account_group', array(
            'user_id'=>$userid,
            'group_id'=>Yii::app()->controller->getModule('account')->defaultLetGroup,
        ));
    }

	public static function encodePassword($password = '', $salt = '') {
        return md5(md5($password) . $salt);
    }

    public static function createSalt() {
        return letText::random(NULL,3);
    }

    public function getId() {
        return $this->_id;
    }
}
