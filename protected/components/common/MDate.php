<?php

/**
 * MDate class file.
 * 
 * Class này được dùng để chuyển đỗi định quan lại giữa dạng ngày giờ trong database
 * và ngày giờ hiển thị trên form theo i18n
 *
 * @package application.components.lib
 * @author Mai Ba Duy <maibaduy@gmail.com>
 * @copyright Copyright (c) 2012 TRUMCODE Group
 * @license GNU General Public License version 2
 * @link http://trumcode.com
 * @version 1.0
 * @since 1.0
 * 
 * Một số giá trị dateFormat của CLocale có thể dùng:
 * - full:   Thứ bảy, ngày 08 tháng chín năm 2012
 * - long:   Ngày 08 tháng 9 năm 2012
 * - medium: 08-09-2012
 * - short:  08/09/2012
 */

class MDate {

    /**
     * Lấy giá trị ngày giờ hiện tại để lưu và database
     *
     * @return string
     */
    public static function getNow() {
        return date('Y-m-d H:i:s', time());
    }
    
    public static function getDate() {
        return date('Y-m-d', time());
    }

    /**
     * Hiển thị định dạng ngày tháng. Dùng trong Frontend
     * Lấy cấu hình mặc định theo App
     *
     * @param string $value Giá trị ngày giờ lưu trong database
     * @param integer $format Định dạng sẽ hiển thị (1:Hiển thị cả date+time, 2:Chỉ hiện date, 3:Chỉ hiện time)
     * @param string $module Nếu không được chỉ định, sẽ lấy cấu hình trong module hiện tại
     *
     * @return string Giá trị ngày giờ theo i18n
     */
    public static function view($value, $format = 1, $module = NULL) {
        //$module = ($module != NULL) ? $module : Yii::app()->controller->module->id;
        $config = Yii::app()->controller->module->getConfig('app');
        $dateWidth = $config['formatDate'];
        $timeWidth = $config['formatTime'];        

        if ($dateWidth == FALSE)
            $dateWidth = Yii::app()->params['showFormatDate'];
        if ($timeWidth == FALSE)
            $timeWidth = Yii::app()->params['showFormatTime'];

        if ($format == 2) {
            $timeWidth = NULL;
        } else if ($format == 3) {
            $dateWidth = NULL;
        }       

        return Yii::app()->dateFormatter->formatDateTime($value, $dateWidth, $timeWidth);
    }

    /**
     * Chuyển giá trị ngày giờ được lưu trong database thành ngày giờ hiển thị trên form. Dùng trong Backend
     *
     * @param string $value Giá trị ngày giờ lưu trong database
     * @param integer $format Định dạng sẽ hiển thị (1:Hiển thị cả date+time, 2:Chỉ hiện date, 3:Chưa xử lý được time)
     * @param string $module Nếu không được chỉ định, sẽ lấy cấu hình trong module hiện tại
     *
     * @return string Giá trị ngày giờ được dùng để hiển thị trên form
     */
    public static function show($value, $showFormat = 1) {
        if ($showFormat == 1) {
            return Yii::app()->dateFormatter->formatDateTime($value, 'short', 'medium');
        } else if ($showFormat == 2) {
            return Yii::app()->dateFormatter->formatDateTime($value, 'short', NULL);
        } else {
            return Yii::app()->dateFormatter->formatDateTime($value, NULL, 'medium');
        }
    }

    /**
     * Chuyển định dạng ngày giờ trên form thành đúng định dạng có thể lưu trong database
     *
     * @param string $value Giá trị ngày giờ hiển thị trên form
     * @param integer $format Định dạng sẽ lưu trữ (1:Lưu trữ cả date+time, 2:Chỉ lưu date, 3:Chỉ lưu time)
     * @param string $module Nếu không được chỉ định, sẽ lấy cấu hình trong module hiện tại
     *
     * @return string Giá trị ngày giờ theo kiểu dữ liệu quy định trong database
     */
    public static function store($value, $format = 1) {
        if ($value == NULL)
            return NULL;
        
        if ($format == 1) {
            $dateTimeFormat = Yii::app()->locale->getDateTimeFormat();
            $dateTimeFormat = str_replace('{1}', Yii::app()->locale->getDateFormat('short'), $dateTimeFormat);
            $dateTimeFormat = str_replace('{0}', Yii::app()->locale->getTimeFormat('medium'), $dateTimeFormat);

            return date('Y-m-d H:i:s', CDateTimeParser::parse($value, $dateTimeFormat));
        } else if ($format == 2) {
            return date('Y-m-d', CDateTimeParser::parse($value, Yii::app()->locale->getDateFormat('short')));
        } else {
            return NULL;
        }
    }

}
