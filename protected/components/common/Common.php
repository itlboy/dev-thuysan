<?php

/**
 * Common library class
 */
class Common {

    /**
     *
     * Load language message by category
     * @param STRING $category
     * @return ARRAY
     */
    public static function loadMessages($category) {
        $languageFile = Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . "messages" . DIRECTORY_SEPARATOR . Yii::app()->getLanguage() . DIRECTORY_SEPARATOR . "{$category}.php";
        if (!file_exists($languageFile))
            $languageFile = Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . "messages" . DIRECTORY_SEPARATOR . Yii::app()->getLanguage() . DIRECTORY_SEPARATOR . "db.php";
        return require($languageFile);
    }

    /**
     * Add WHERE for sql query
     * @param type $where
     * @param type $string
     * @return string
     */
    public static function addWhere($where = '', $string = '') {
        if (!isset($where) OR $where == NULL OR $where == '')
            $where = $string;
        else
            $where .= ' AND ' . $string;
        return $where;
    }

    // Chuyển số thành chữ
    public static function convertIntToText($int = 0) {
        // now filter it;
        if ($int > 1000000000000)
            return round(($int / 1000000000000), 1) . ' nghìn tỷ';
        else if ($int > 1000000000)
            return round(($int / 1000000000), 1) . ' tỷ';
        else if ($int > 1000000)
            return round(($int / 1000000), 1) . ' triệu';
        else if ($int > 1000)
            return round(($int / 1000), 1) . ' nghìn';
        else
            return $int;
    }

    public static function convertStringToUrl($str = '') {
        $str = self::convertStrToAscii($str);
//        $str = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $str = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '-', $str);
        $str = strtolower($str);
        $str = preg_replace("/[\/_|+ -]+/", '-', $str);

//        $str = preg_replace('/\s\s+/', '-', $str);
        $str = trim($str, '-');
        return $str;
    }

    public static function convertStrToAscii($str = '') {
        $str = str_replace(array("à", "á", "ạ", "ả", "ã", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ"), "a", $str);
        $str = str_replace(array("À", "Á", "Ạ", "Ả", "Ã", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ"), "A", $str);
        $str = str_replace(array("è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ"), "e", $str);
        $str = str_replace(array("È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ"), "E", $str);
        $str = str_replace("đ", "d", $str);
        $str = str_replace("Đ", "D", $str);
        $str = str_replace(array("ỳ", "ý", "ỵ", "ỷ", "ỹ"), "y", $str);
        $str = str_replace(array("Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ"), "Y", $str);
        $str = str_replace(array("ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ"), "u", $str);
        $str = str_replace(array("Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ"), "U", $str);
        $str = str_replace(array("ì", "í", "ị", "ỉ", "ĩ"), "i", $str);
        $str = str_replace(array("Ì", "Í", "Ị", "Ỉ", "Ĩ"), "I", $str);
        $str = str_replace(array("ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ"), "o", $str);
        $str = str_replace(array("Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ"), "O", $str);
        return $str;
    }

    /**
     * get Field In Table
     * @param object $Attributes
     * @param string $prefix
     * @param string $suffix
     * @param integer $type
     * @return string
     */
    public static function getFieldInTable($Attributes, $prefix = '', $suffix = '') {
        $result = array_keys($Attributes);
        foreach ($result as $key => $value) {
            $result[$key] = $prefix . $value . $suffix;
        }
        return $result = implode(',', $result);
    }

    /* //////////////////////////////// Upload /////////////////////////////////*

      /**
     * Get duong dan cua anh da up len Server, co 2 dang PATH và URL
     * @param string $image
     * @param string $type PATH or URL
     * @return string
     */

    public static function getImageUploaded($image = '', $type = 'URL') {
        if ($type == 'PATH')
            $result = Yii::app()->params['uploadPath'] . DIRECTORY_SEPARATOR . Yii::app()->params['uploadDir'] . DIRECTORY_SEPARATOR . $image;
        elseif ($type == 'URL') {
            $host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
            $result = Yii::app()->params['uploadUrl'] . '/' . Yii::app()->params['uploadDir'] . '/' . $image;
        }
        return $result;
    }

    /**
     * Tao cac thu muc trong path neu cac thu muc do khong ton tai
     * @param type $dir
     * @param string $path
     * @return string
     */
    public static function mkPath($dir, $path = '') {
        $dirArray = explode('/', $dir);
        foreach ($dirArray as $dirName) {
            $path .= '/' . $dirName;
            if (!is_dir($path)) {
                mkdir($path, 0777);
            }
        }
        Yii::log('mkPath: ' . $path, 'info');
        return $path;
    }

    /**
     * Tao thumb cho anh dai dien theo cac kich co quy dinh truoc
     * @param type $module
     * @param type $inputImage
     * @param type $title
     * @return string Tra ve duong dan cua anh de luu vao database
     */
    public static function createThumb($module, $inputImage, $title, $oldFileToDelete = "") {
        $date = date('Y/m/d/');

        // Duong dan file luu o thu muc tam
        $tmpFile = Common::getImageUploaded($inputImage, 'PATH');
        $size = getimagesize($tmpFile);
        if (!isset($size[0]) OR !isset($size[1]) OR (int) $size[0] <= 0 OR (int) $size[1] <= 0) {
            unset($tmpFile);
            return FALSE;
        }

        // Lay thong tin file tam va tao ra ten file moi
        $tmpFileInfo = pathinfo($tmpFile);

        //        $fileName = Common::convertStringToUrl($title) . '_' . rand(100000, 999999) . '.' .strtolower($tmpFileInfo['extension']);
        $fileName = Common::convertStringToUrl($title) . '_' . rand(100000, 999999) . '.jpg';

        // get image config
        $imageConfig = Yii::app()->getModule($module)->image;

        // Resize
        foreach ($imageConfig as $key => $config) {
            if (in_array($key, array('origin', 'large', 'medium', 'small'))) {
                
                //Xóa thư mục ảnh cũ
                if(!empty($oldFileToDelete))
                    self::removeImage ($module, $oldFileToDelete);
                
                // Thu muc luu file
                $fileDir = $module . '/' . $config['folder'] . '/' . $date;

                // Duong dan file
                $filePath = Common::getImageUploaded($fileDir . $fileName, 'PATH');

                // Tao dir theo path neu dir chua ton tai
                Common::mkPath($fileDir, Common::getImageUploaded('', 'PATH'));

                if ($config['type'] == 'move')
                    copy($tmpFile, $filePath);
                elseif ($config['type'] == 'resize') {
                    $image = Yii::app()->image->load($tmpFile);
                    $image->resize(letArray::get($config, 'width'), letArray::get($config, 'height'), letArray::get($config, 'master', Image::AUTO))->rotate(letArray::get($config, 'rotate'))->quality(letArray::get($config, 'quality'));
                    $image->save($filePath);
                } elseif ($config['type'] == 'crop') {
                    $image = Yii::app()->image->load($tmpFile);

                    // Xac dinh master
                    $width = $size[0];
                    $height = $size[1];
                    if ($width / letArray::get($config, 'width') < $height / letArray::get($config, 'height'))
                        $master = Image::WIDTH;
                    else
                        $master = Image::HEIGHT;
                    // END Xac dinh master

                    $image->resize(letArray::get($config, 'width'), letArray::get($config, 'height'), $master)->crop(letArray::get($config, 'width'), letArray::get($config, 'height'))->rotate(letArray::get($config, 'rotate'))->quality(letArray::get($config, 'quality'));
                    $image->save($filePath);
                }

                // Gan Logo
                if (letArray::get($config, 'addlogo') !== NULL) {
                    Yii::import('application.vendors.libs.Addlogo');
                    $logoPath = Yii::app()->basePath . '/../' . letArray::get($config, 'addlogo');
                    $Addlogo = new Addlogo();
                    $Addlogo->createImageAndLogo($filePath, $logoPath);
                    $Addlogo->InsertLogo(3); // 1 = top left, 2 = top right, 3 = bottom right, 4 = bottom left, 5 = bottom center, 6 = top center
                    $Addlogo->save($filePath);
                }
            }
        }
        unlink($tmpFile);
        return $date . $fileName;
    }

    public static function getNow() {
        Yii::app()->setTimeZone('Asia/Ho_Chi_Minh');
//        date_default_timezone_set('Asia/Ho_Chi_Minh');
        return date('Y-m-d H:i:s', time());
    }

    public static function removeImage($module, $image) {
        if ($image == NULL OR $image == '')
            return FALSE;

        $imageConfig = Yii::app()->getModule($module)->image;
        foreach ($imageConfig as $key => $config) {
            if (in_array($key, array('origin', 'large', 'medium', 'small'))) {
                $fileSrc = Common::getImageUploaded($module . '/' . $config['folder'] . '/' . $image, 'PATH');
                if (file_exists($fileSrc) AND is_file($fileSrc)) {
                    unlink($fileSrc);
                }
            }
        }
    }

    /**
     * @static
     * @param string $app let|discuz|phpfox...
     * @param $id
     */
    public static function getFolderAvatar($app = 'let', $id, $length = 9) {
        switch ($app) {
            case 'let':
                $folder = '';
                break;
            case 'discuz':
                $idNew = '';
                if (strlen($id) < $length) {
                    for ($i = 0; $i < ($length - strlen($id)); $i++) {
                        $idNew .='0';
                    }
                    $idNew = $idNew . $id;
                } else {
                    $idNew = $id;
                }
                $idnew2 = '';
                for ($j = strlen($idNew) - 1; $j >= 0; $j--) {
                    if ($j % 2 == 0) {
//                        $idnew2 = '/'.$
                    }
                }
                return $idNew;
                break;
        }
    }

    /**
     * Tạo 1 mảng có số ngày từ $strDateFrom đến $strDateTo
     * @param str $strDateFrom
     * @param str $strDateTo
     * @return array
     */
    public static function createDateRangeArray($strDateFrom, $strDateTo) {
        $aryRange = array();
        $iDateFrom = strtotime($strDateFrom);
        $iDateTo = strtotime($strDateTo);

        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, date('Y-m-d', $iDateFrom)); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom+=86400; // add 24 hours
                array_push($aryRange, date('Y-m-d', $iDateFrom));
            }
        }
        return $aryRange;
    }

    public static function get_user_browser() {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $ub = '';
        if (preg_match('/MSIE/i', $u_agent))
            $ub = "ie";
        elseif (preg_match('/Chrome/i', $u_agent))
            $ub = "chrome";
        elseif (preg_match('/Firefox/i', $u_agent))
            $ub = "firefox";
        elseif (preg_match('/Opera/i', $u_agent))
            $ub = "opera";
        elseif (preg_match('/Flock/i', $u_agent))
            $ub = "flock";
        elseif (preg_match('/Safari/i', $u_agent))
            $ub = "safari";

        return $ub;
    }

    // Dùng để test lưu lượng truy cập
    public static function urlReferer() {
        $url = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : NULL;
        if ($url == NULL)
            return;

        $command = Yii::app()->db->createCommand();
        $referer = $command->from('{{kit_test}} t')
                ->where('link = :link', array(':link' => $url))
                ->queryRow();

        if ($referer == FALSE) {
            $command->insert('{{kit_test}}', array(
                'link' => $url,
                'count' => 1,
            ));
        } else {
            $command->update('{{kit_test}}', array(
                'count' => $referer['count'] + 1,
                    ), 'link = :link', array(':link' => $referer['link']));
        }
    }
    
    public static function randomString( $length = 6) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $size = strlen( $chars );
        for( $i = 0; $i < $length; $i++ ) {
            $str .= $chars[ rand( 0, $size - 1 ) ];
        }
        return $str;
    }
    
    public static function getClientIp()
    {
        if(!isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ip = $_SERVER['REMOTE_ADDR'];
        else 
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        return $ip;
    }
    
    /**
    * xoa file trong thu muc
    * 
    * @param mixed $tempDir
    * @param mixed $defaultTime
    */
    public static function deleteOldTempFiles($tempDir, $when = 0, $defaultTime = 24) {
        // Open temp directory
        if (is_dir($tempDir)) {
            if ($dh = opendir($tempDir)) {
                $timeLeft = $defaultTime * 60 * 60; // default 24h
                while (($file = readdir($dh)) !== false) {
                    preg_match('/^(?<timespam>\d+)_(.*)$/i', $file, $match);
                    if(isset($match['timespam'])) {
                        // remove temp file
                        @unlink($tempDir . $file);
                    }
                }
                closedir($dh);
            }
        }
    }
    
    public static function getImageUrl($modelName, $type, $image){
        $url = "";
        if($modelName AND $type AND $image){
            $uploadFolder = Yii::app()->params['uploadUrl'] . "/" . Yii::app()->params['uploadDir'];
            $url = $uploadFolder. "/" .$modelName."/". $type ."/". $image;
        }
        
        return $url;
    }
    
    public static function emptyFolder($dir) {
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if (($file != ".") && ($file != "..") && file_exists($dir . DIRECTORY_SEPARATOR . $file))
                        unlink($dir . DIRECTORY_SEPARATOR . $file);
                }
                closedir($dh);
            }
        }
    }

    public static function removeDir($dir) {
        if (is_dir($dir)) {
            self::emptyFolder($dir);
            rmdir($dir);
        }
    }

}