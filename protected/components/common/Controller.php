<?php
class Controller extends CController {
    /**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/main';
    
    public function render($view,$data=null,$return=false) {
        if ($this->module->getName() == 'srbac') {
            return parent::render ($view, $data, $return);
        }
        
        $view = ($view[0] == "/")?$view :'/'.substr(strrchr($this->id,'/'),1)."/$view"; 
        return parent::render($view,$data,$return);
    }
    
    public function renderPartial($view,$data=null,$return=false,$processOutput=false) {
        if ($this->module->getName() == 'srbac') {
            return parent::renderPartial ($view, $data, $return, $processOutput);
        }
        
		$view = ($view[0] == "/")?$view :'/'.substr(strrchr($this->id,'/'),1)."/$view";
        if(($viewFile=$this->getViewFile($view))!==false)
		{
			$output=$this->renderFile($viewFile,$data,true);
			if($processOutput)
				$output=$this->processOutput($output);
			if($return)
				return $output;
			else
				echo $output;
		}
		else
			throw new CException(Yii::t('yii','{controller} cannot find the requested view "{view}".',
				array('{controller}'=>get_class($this), '{view}'=>$view)));
	}

    
    // chua sua
	public function getViewFile($viewName)
	{
        
		if(($theme=Yii::app()->getTheme())!==null && ($viewFile=$theme->getViewFile($this,$viewName))!==false)
			return $viewFile;
		$moduleViewPath=$basePath=Yii::app()->getViewPath();
		if(($module=$this->getModule())!==null)
			$moduleViewPath=$module->getViewPath();
		return $this->resolveViewFile($viewName,$this->getViewPath(),$basePath,$moduleViewPath);
	}
}