<?php

class LetIdentity extends CUserIdentity
{
    public $admin = false;
    public $typeData = 'discuz';
    public $keyName = 'uid';
    public $table = 'let_forum_ucenter_members';
    public $messages = array('error'=>array(), 'success'=>array(), 'warning'=>array());

    private $_id;

    const ERROR_NOT_ACTIVE = 3;
    const ERROR_NOT_PERMISSION = 4;

	public function __construct($username, $password, $admin=false, $typeData='discuz')
	{
		if ($typeData == 'vbb') {
            $this->keyName = 'userid';
            $this->table = 'eplay_user';
		}

        $this->username = $username;
		$this->password = $password;
        $this->typeData = $typeData;
        $this->admin = $admin;
	}

    public function authenticate()
    {
        $user = $this->getDetails($this->username);
        if ($user == NULL) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
            $this->messages['error'][] = 'Tên truy cập không tồn tại';
        } else {
            // Neu $user['let_active'] không tồn tại thì gọi hàm $user = $this->createUserExtra($this->id);
            if ($user['let_active'] == NULL) {
                $user = $this->createUserExtra($user['uid']);
            }

            if ($this->encodePassword($this->password, $user['salt']) !== $user['password']) {
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
                $this->messages['error'][] = 'Mật khẩu không đúng';
            } else if ($user['let_active'] == 0) {
                $this->errorCode = self::ERROR_NOT_ACTIVE;
                $this->messages['error'][] = 'Tài khoản của bạn chưa được kích hoạt';
            } else if ($this->admin == true AND $user['let_group'] != '8') {
                $this->errorCode = self::ERROR_NOT_PERMISSION;
                $this->messages['error'][] = 'Bạn không có quyền truy cập trang này';
            } else {
                $this->_id = $user[$this->keyName];

//                Yii::app()->db->createCommand()->update($this->table, array(
//                        'lastloginip' => Yii::app()->request->userHostAddress,
//                        'lastlogintime' => time()
//                    ),
//                    "{$this->keyName} = :id", array(':id' => $this->_id)
//                );

                $this->setState('group', $user['let_group']);
                $this->setState('email', $user['email']);
                $this->errorCode = self::ERROR_NONE;
            }
        }
        return !$this->errorCode;
    }

    public function getDetails($username='', $userid=0)
    {
        $command = Yii::app()->db->createCommand();
        $command->select();
        $command->from($this->table . ' t');
        if ($userid !== 0) {
            $command->where($this->keyName.' = :keyName', array(':keyName' => $userid));
        } elseif ($username !== '') {
            $command->where('username = :username', array(':username' => $username));
        }
        $command->leftJoin('let_kit_user_extra ue', 'ue.id=t.'.$this->keyName);
        return $command->queryRow();
    }

    public function createUserExtra($userid=0)
    {
        // Tạo bản ghi ở bảng user_extra sau đó return cái dưới

        //return $this->getDetails('', $userid);

        Yii::app()->db->createCommand()->insert('let_kit_user_extra', array(
            'id'=>$userid,
            'let_group'=>Yii::app()->controller->getModule('account')->defaultLetGroup,
            'let_active'=>Yii::app()->controller->getModule('account')->defaultLetActive,
        ));
    }

	public static function encodePassword($password = '', $salt = '') {
        return md5(md5($password) . $salt);
    }

    public static function createSalt() {
        return 'abc';
    }

    public function getId() {
        return $this->_id;
    }
}