<?php
class HtmlBlock extends BaseBlockWidget {
    var $views = array(
        'default' => "Default",
    );
    
    public function toHtml() {
        return $this->data->description;
    }
}
?>