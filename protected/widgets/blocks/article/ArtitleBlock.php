<?php
class ArtitleBlock extends BaseBlockWidget {
    var $views = array(
        'default' => "Default",
    );
    
    public function getList() {
        $criteria = new CDbCriteria();
        $criteria->order = "id desc";
        $criteria->limit = 10;
        return KitArticle::model()->findAll($criteria);
    }
}
?>