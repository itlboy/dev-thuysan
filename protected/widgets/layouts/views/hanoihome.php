<?php
$category = Yii::app()->request->getParam('id', NULL);
?>
    <div class="let_body">
        <div class="let_container">
            <div class="let_body_inner">

                <?php if ($this->hasPosition('banner_top') AND FALSE): ?>
                <div class="let_banner_full">
                    <?php // $this->displayPosition('banner_top') ?>
                </div>
                <?php endif; ?>
                <div class="let_featured clearfix">
                    <?php $this->widget('article.widgets.frontend.lastest', array(
                        'category' => $category,
                    )); ?>
                    <div class="let_right">
                        <div class="let_banner">
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/samples/banner_right1.jpg" alt="banner_right1" />
                        </div>
                        <div class="let_banner">
                            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/samples/banner_right2.jpg" alt="banner_right2" />
                        </div>
                    </div>
                </div>

                <?php $this->widget('article.widgets.frontend.promotion', array(
                    'view' => 'list_style2',
                    'category' => $category,
                )); ?>

                <div class="let_maincontent clearfix">
                    <div class="let_left">
                        <div class="let_left_inner">
                            <div class="let_news_category">

                                <div class="let_news clearfix">
                                    <div class="let_left">
                                        <div class="let_left_inner">

                                            <?php $this->widget('article.widgets.frontend.lastestByCat', array(
                                                'category' => $category,
                                            )); ?>

                                        </div>
                                    </div>
                                    <div class="let_right">
                                        <?php $this->widget('article.widgets.frontend.mostPopular', array(
                                            'view' => 'list_style1',
                                            'category' => $category,
                                            'title' => 'Đọc nhiều nhất',
                                        )); ?>
                                    </div>										
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="let_right">
                        <div class="let_module">
                            <div class="let_module_inner">
                                <div class="let_module_title">Tin tức 63 tỉnh thành</div>
                                <div class="let_module_content">
                                    <div class="let_first clearfix">
                                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/samples/new-right.jpg" alt="new-right.jpg" />
                                        <a href="#">5 yếu tố giúp MU trở lại ngôi đầu Premier League</a>
                                        <div class="let_description">“Quỷ đỏ” đã chính thức giật lấy ngôi đầu từ tay Man City sau vòng 28 bằng chiến thắng nhẹ nhàng 2-0 trước West Brom. Để có thể vượt mặt đối thủ đầy khó chịu này, thầy trò Sir Alex đã phải cậy nhờ đến khá nhiều sự đổi thay suốt thời gian qua.</div>
                                    </div>
                                    <ul>
                                        <li><a href="#">Vòng 8 V.League: Nội binh lên tiếng </a></li>
                                        <li><a href="#">Những phát hiện “động trời” trong biên bản ghi nhớ VTV-VPF </a></li>
                                        <li><a href="#">Con số sau vòng 6 V-League 2012 </a></li>
                                        <li><a href="#">Đánh bại Bờ Biển Ngà, Zambia đăng quang CAN 2012</a></li> 
                                        <li><a href="#">Những vụ bạo động đáng quên trong lịch sử bóng đá thế giới </a></li>
                                        <li><a href="#">Vấn đề Super League: Chuyên nghiệp khó thế sao...</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="let_module">
                            <div class="let_module_inner">
                                <div class="let_module_title">Tin tức 63 tỉnh thành</div>
                                <div class="let_module_content">
                                    <div class="let_first clearfix">
                                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/samples/new-right.jpg" alt="new-right.jpg" />
                                        <a href="#">5 yếu tố giúp MU trở lại ngôi đầu Premier League</a>
                                        <div class="let_description">“Quỷ đỏ” đã chính thức giật lấy ngôi đầu từ tay Man City sau vòng 28 bằng chiến thắng nhẹ nhàng 2-0 trước West Brom. Để có thể vượt mặt đối thủ đầy khó chịu này, thầy trò Sir Alex đã phải cậy nhờ đến khá nhiều sự đổi thay suốt thời gian qua.</div>
                                    </div>
                                    <ul>
                                        <li><a href="#">Vòng 8 V.League: Nội binh lên tiếng </a></li>
                                        <li><a href="#">Những phát hiện “động trời” trong biên bản ghi nhớ VTV-VPF </a></li>
                                        <li><a href="#">Con số sau vòng 6 V-League 2012 </a></li>
                                        <li><a href="#">Đánh bại Bờ Biển Ngà, Zambia đăng quang CAN 2012</a></li> 
                                        <li><a href="#">Những vụ bạo động đáng quên trong lịch sử bóng đá thế giới </a></li>
                                        <li><a href="#">Vấn đề Super League: Chuyên nghiệp khó thế sao...</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="let_news_categories clearfix">
                    <div class="let_item first">
                        <div class="let_item_inner">
                            <div class="let_title">Sự kiện - bình luận</div>
                            <div class="let_content">
                                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/samples/cat1.jpg" alt="new-right.jpg" />
                                <a href="#">5 yếu tố giúp MU trở lại ngôi đầu Premier League</a>
                                <ul>
                                    <li><a href="#">Vòng 8 V.League: Nội binh lên tiếng </a></li>
                                    <li><a href="#">Những phát hiện “động trời” trong biên bản ghi nhớ VTV-VPF </a></li>
                                </ul>
                            </div>											
                        </div>
                    </div>
                    <div class="let_item">
                        <div class="let_item_inner">
                            <div class="let_title">Sự kiện - bình luận</div>
                            <div class="let_content">
                                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/samples/cat2.jpg" alt="new-right.jpg" />
                                <a href="#">Bắt quả tang vận chuyển 5 kg thuốc phiện</a>
                                <ul>
                                    <li><a href="#">Vòng 8 V.League: Nội binh lên tiếng </a></li>
                                    <li><a href="#">Những phát hiện “động trời” trong biên bản ghi nhớ VTV-VPF </a></li>
                                </ul>
                            </div>											
                        </div>
                    </div>
                    <div class="let_item">
                        <div class="let_item_inner">
                            <div class="let_title">Sự kiện - bình luận</div>
                            <div class="let_content">
                                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/samples/cat3.jpg" alt="new-right.jpg" />
                                <a href="#">Tiêu hủy gần 8 tấn ruột heo không rõ nguồn gốc</a>
                                <ul>
                                    <li><a href="#">Vòng 8 V.League: Nội binh lên tiếng </a></li>
                                    <li><a href="#">Những phát hiện “động trời” trong biên bản ghi nhớ VTV-VPF </a></li>
                                </ul>
                            </div>											
                        </div>
                    </div>
                    <div class="let_item">
                        <div class="let_item_inner">
                            <div class="let_title">Sự kiện - bình luận</div>
                            <div class="let_content">
                                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/samples/cat1.jpg" alt="new-right.jpg" />
                                <a href="#">Bắt đối tượng tàng trữ gần 2 gam hêrôin</a>
                                <ul>
                                    <li><a href="#">Làm rõ thông tin cán bộ nhờ thi hộ ở Đồng Nai </a></li>
                                    <li><a href="#">Những phát hiện “động trời” trong biên bản ghi nhớ VTV-VPF </a></li>
                                </ul>
                            </div>											
                        </div>
                    </div>
                    <div class="let_item last">
                        <div class="let_item_inner">
                            <div class="let_title">Sự kiện - bình luận</div>
                            <div class="let_content">
                                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/samples/cat2.jpg" alt="new-right.jpg" />
                                <a href="#">Làm rõ thông tin cán bộ nhờ thi hộ ở Đồng Nai</a>
                                <ul>
                                    <li><a href="#">Vòng 8 V.League: Nội binh lên tiếng </a></li>
                                    <li><a href="#">Những phát hiện “động trời” trong biên bản ghi nhớ VTV-VPF </a></li>
                                </ul>
                            </div>											
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
