<?php
class DefaultLayout extends BaseLayoutWidget {
    var $positions = array(
        'header',
        'content' => array ('left', 'middle', 'right'),
        'footer',
    );
}
?>