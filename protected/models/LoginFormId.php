<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginFormId extends CFormModel {

    public $email;
    public $password;
    public $rememberMe;
    public $checkAdmin = false;
    public $message;
    private $_identity;

    /**
     * Declares the validation rules.
     * The rules state that email and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            array('email, password', 'required'),
            array('rememberMe', 'boolean'),
            array('password', 'authenticate'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'rememberMe' => 'Remember me next time',
        );
    }

    /**
     * Authenticates the password.
     * This is the 'authenticate' validator as declared in rules().
     */
    public function authenticate($attribute, $params) {        
        if (!$this->hasErrors()) {
            $this->_identity = new LetIdentityId($this->email, $this->password, $this->checkAdmin);

            if (!$this->_identity->authenticate()) {                
                $this->message = $this->_identity->messages['error'][0];
                //$this->addError('password', $this->_identity->messages['error'][0]);
                //$this->addError('password','Incorrect email or password.');
            }
        }
    }

    /**
     * Logs in the user using the given email and password in the model.
     * @return boolean whether login is successful
     */
    public function login() {

        if ($this->_identity === null) {
            $this->_identity = new LetIdentityId($this->email, $this->password);
            $this->_identity->authenticate();
        }

        if ($this->_identity->errorCode === LetIdentityId::ERROR_NONE) {

            $duration = $this->rememberMe ? 3600 * 24 * 30 : 0; // 30 days
            Yii::app()->user->login($this->_identity, $duration);
            return true;
        }
        else
            return false;
    }

}
