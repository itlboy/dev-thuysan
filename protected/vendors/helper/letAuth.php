<?php

class letAuth {

	public function init() {
		Yii::import('account.models.KitAccount');
	}
    
    public static function getOne($id = NULL, $username = NULL) {
        $criteria=new CDbCriteria;
//        $criteria->select='title';
        $where = '';
        if ($id !== NULL AND $id > 0)
            $where = letFunction::addWhere ($where, 'id='.$id);
        if ($username !== NULL)
            $where = letFunction::addWhere ($where, 'username='.$username);
        $criteria->condition = $where;
        
        return $row = KitAccount::model()->find($criteria);
	}

}