<?php

class letFunction
{
    /**
     *
     * @param type $where
     * @param type $string
     * @return string Add WHERE for sql
     */
	public static function addWhere($where = '', $string = '')
	{
        if (!isset ($where) OR $where == NULL OR $where == '')
            $where = $string;
        else
            $where .= ' AND ' . $string;
        return $where;
	}

    /**
     *
     * @param type $status
     * @return type 
     */
	public static function statusToImages($status = NULL)
	{
        if ($status > 0)
            $result = '<img src="'.Yii::app()->theme->baseUrl.'/common/img/icon/icon_check_16.png" />';
        elseif ($status == 0)
            $result = '<img src="'.Yii::app()->theme->baseUrl.'/common/img/icon/icon_disable_16.png" />';
        else
            $result = '';
        return $result;
	}
    
    public static function statusLink($module, $model, $column, $id, $status) {
        $value = ($status) ? 0 : 1;
        $params = 'module='.$module.'&model='.$model.'&id='.$id.'&column='.$column.'&value='.$value;
        echo CHtml::link(letFunction::statusToImages($status), "", array(
            'id' => $column . '_' . $id,
            'onclick' => 'loadAjaxReturnParent("'.Yii::app()->createUrl('//global/ChangeStatusValue/index').'", "'.$params.'", "'.$column.'_'.$id.'")'
        ));
    }

	public static function is_mobile()
	{
//		return TRUE;
		if(isset($_SERVER['HTTP_X_WAP_PROFILE']))
			return true;
	    if(preg_match('/wap.|.wap/i',$_SERVER['HTTP_ACCEPT']))
			return true;
			
		if(isset($_SERVER['HTTP_USER_AGENT']))
		{
			$user_agents = array(
				'midp', 'j2me', 'avantg', 'docomo', 'novarra', 'palmos', 
				'palmsource', '240x320', 'opwv', 'chtml', 'pda', 'windows ce', 
				'mmp\/', 'blackberry', 'mib\/', 'symbian', 'wireless', 'nokia', 
				'hand', 'mobi', 'phone', 'cdm', 'up.b', 'audio', 'SIE-', 'SEC-', 
				'samsung', 'HTC', 'mot-', 'mitsu', 'sagem', 'sony', 'alcatel', 
				'lg', 'erics', 'vx', 'NEC', 'philips', 'mmm', 'xx', 'panasonic', 
				'sharp', 'wap', 'sch', 'rover', 'pocket', 'benq', 'java', 'pt', 
				'pg', 'vox', 'amoi', 'bird', 'compal', 'kg', 'voda', 'sany', 
				'kdd', 'dbt', 'sendo', 'sgh', 'jb', 'dddi', 'moto', 'Opera Mobi',
				'android', 'Android' 
			);
			$user_agents = implode('|', $user_agents);
			if (preg_match("/$user_agents/i", $_SERVER['HTTP_USER_AGENT']))
				return true;
		}
		
		return false;
	}

    public static function fullUrl()
    {
        $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
        $protocol = substr(strtolower($_SERVER["SERVER_PROTOCOL"]), 0, strpos(strtolower($_SERVER["SERVER_PROTOCOL"]), "/")) . $s;
        $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
        return $protocol . "://" . $_SERVER['SERVER_NAME'] . $port . $_SERVER['REQUEST_URI'];
    }

}