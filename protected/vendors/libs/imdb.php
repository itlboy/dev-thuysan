<?php

/**
 * $imdb = new imdb();
 * $info = $imdb->getMovieInfo('Bay Rong');
 */
Yii::import('application.vendors.libs.crawler');
class imdb
{
	public $info;

	function __construct($str = null)
	{
		if(!is_null($str))
			$this->autodetect($str);
	}

	function autodetect($str)
	{
		// Attempt to cleanup $str in case it's a filename ;-)
		$str = pathinfo($str, PATHINFO_FILENAME);
		$str = $this->normalize($str);

		// Is it a movie or tv show?
		if(preg_match('/s[0-9][0-9]?.?e[0-9][0-9]?/i', $str) == 1)
			$this->info = $this->getEpisodeInfo($str);
		else
			$this->info = $this->getMovieInfo($str);

		return $this->info;
	}

	function getEpisodeInfo($str)
	{
		$arr = array();
		$arr['kind'] = 'tv';
		return $arr;
	}

	function getMovieInfo($str)
	{
		$str  = str_ireplace('the ', '', $str);
		$url  = "http://www.google.com/search?hl=en&q=imdb+" . urlencode($str) . "&btnI=I%27m+Feeling+Lucky";

        return $this->getInfo($url);
//		$html = $this->geturl($url);
//		if(stripos($html, "302 Moved") !== false)
//			$html = $this->geturl($this->match('/HREF="(.*?)"/ms', $html, 1));
//
//		$arr = array();
//		$arr['kind'] = 'movie';
//		$arr['id'] = $this->match('/poster.*?(tt[0-9]+)/ms', $html, 1);
//		$arr['title'] = $this->match('/<title>(.*?)<\/title>/ms', $html, 1);
//		$arr['title'] = preg_replace('/\([0-9]+\)/', '', $arr['title']);
//		$arr['title'] = str_replace(' - IMDb', '', $arr['title']);
//		$arr['title'] = trim($arr['title']);
//		$arr['rating'] = $this->match('/([0-9]\.[0-9])\/10/ms', $html, 1);
//		$arr['country'] = trim(strip_tags($this->match('/Country:(.*?)<\/a>/ms', $html, 1)));
//
//        $arr['director'] = trim(strip_tags($this->match('/Director:(.*?)<\/a>/ms', $html, 1)));
//
//		$arr['writer'] = trim(strip_tags($this->match('/Writer:(.*?)<\/a>/ms', $html, 1)));
//        if ($arr['writer'] == '')
//		$arr['writer'] = trim(strip_tags($this->match('/Writers:(.*?)<\/div>/ms', $html, 1)));
//
//		$arr['stars'] = trim(strip_tags($this->match('/Stars:(.*?)<\/div>/ms', $html, 1)));
//		$arr['stars'] = str_replace(' and ', ', ', $arr['stars']);
//
//		//IMDB YEAR: (<a href="/year/2001/">2001</a>)
//		$strStart = '<a href="/year/';
//		$strEnd   = '/">';
//		$arr['year'] = $this->cleanString(strip_tags($this->extractStringFromString($html, $strStart, $strEnd)));
//		if ($arr['year'] === false) $arr['year'] = '';
//
//		//RUNTIME: <img width="18" alt="R" src="http://i.media-imdb.com/images/SFe18791ef3ed9f039376bc190ac0597cd/certificates/us/r.png" class="absmiddle" title="R" height="15"> 129 min
//		$strStart = 'class="absmiddle" title="R" height="15">';
//		$strEnd   = 'min';
//		$arr['runtime'] = $this->cleanString(strip_tags($this->extractStringFromString($html, $strStart, $strEnd)));
//		if ($arr['runtime'] === false) $arr['runtime'] = '';
//
//		//IMAGE: <meta property="og:image" content="http://ia.media-imdb.com/images/M/MV5BMTcxMDE4NTU4OF5BMl5BanBnXkFtZTcwMjg4NjQzMQ@@._V1._SX99_SY140_.jpg"/>
//		$strStart = '<meta property="og:image" content="';
//		$strEnd   = '._V1._';
//		$arr['image'] = $this->cleanString(strip_tags($this->extractStringFromString($html, $strStart, $strEnd)));
//        if ($arr['image'] === false) $arr['image'] = '';
//        else $arr['image'] = $arr['image'] . '._V1_SX700.jpg';
//
//		//release_date: <time itemprop="datePublished" datetime="2006-03-24">24 March 2006</time>
//		$strStart = '<time itemprop="datePublished" datetime="';
//		$strEnd   = '">';
//		$arr['release_date'] = $this->cleanString(strip_tags($this->extractStringFromString($html, $strStart, $strEnd)));
//        if ($arr['release_date'] === false) $arr['release_date'] = '';
//
////		$arr['release_date'] = $this->match('/([0-9][0-9]? (January|February|March|April|May|June|July|August|September|October|November|December) (19|20)[0-9][0-9])/ms', $html, 1);
//		$arr['plot'] = trim(strip_tags($this->match('/Plot:(.*?)<a/ms', $html, 1)));
//		$arr['genres'] = $this->match_all('/Sections\/Genres\/(.*?)[\/">]/ms', $html, 1);
//		$arr['genres'] = array_unique($arr['genres']);
//		$arr['poster'] = $this->match('/<a.*?name=.poster.*?src=.(.*?)(\'|")/ms', $html, 1);
//
//		$arr['cast'] = array();
//		foreach($this->match_all('/class="nm">(.*?\.\.\..*?)<\/tr>/ms', $html, 1) as $m)
//		{
//			list($actor, $character) = explode('...', strip_tags($m));
//			$arr['cast'][trim($actor)] = trim($character);
//		}

//		return $arr;
	}

    // ****************************************************************
    function getInfo($url){
        $arr = array();
        $html = $this->geturl($url);
        if(stripos($html, "302 Moved") !== false)
            $html = $this->geturl($this->match('/HREF="(.*?)"/ms', $html, 1));

        // ten phim
        $arr['title'] = $this->match('/<title>(.*?)<\/title>/i',$html,1);
        $arr['title'] = preg_replace('/- imdb/i','',$arr['title']);
        $arr['rating'] = $this->match('/([0-9]\.[0-9])\/10/ms', $html, 1);
        $arr['country'] = trim(strip_tags($this->match('/Country:(.*?)<\/a>/ms', $html, 1)));
        $arr['director'] = trim(strip_tags($this->match('/Director:(.*?)<\/a>/ms', $html, 1)));
        if(!empty($arr['director'])){
            $arr['director'] = preg_replace('/\((.*)\)/','',$arr['director']);
            $arr['director'] = preg_replace('/screenplay/','',$arr['director']);
        }
        $arr['writer'] = trim(strip_tags($this->match('/Writer:(.*?)<\/a>/ms', $html, 1)));
        if ($arr['writer'] == ''){
            $arr['writer'] = trim(strip_tags($this->match('/Writers:(.*?)<\/div>/ms', $html, 1)));

        }
        if(strpos($arr['writer'],'and') == TRUE){
            $arr['writer'] = preg_replace('/\n/','',$arr['writer']);
            $arr['writer'] = substr($arr['writer'],0,strpos($arr['writer'],'and'));
        }


        $arr['stars'] = trim(strip_tags($this->match('/Stars:(.*?)<\/div>/ms', $html, 1)));
        $arr['stars'] = str_replace(' and ', ', ', $arr['stars']);
        $arr['stars'] = preg_replace('/See full cast, crew/i','',$arr['stars']);
        $arr['stars'] = preg_replace('/\|/i','',$arr['stars']);

        //IMDB duration
        $arr['duration'] = $this->match('/<div class="infobar">(.*?)min&nbsp;&nbsp;-&nbsp;&nbsp;/ms',$html,1);
        if(!empty($arr['duration'])){
            $arr['duration'] = preg_replace('/&nbsp;/','',strip_tags($arr['duration']));
            $arr['duration'] = preg_replace('/-/','',$arr['duration']);
            $arr['duration'] = trim(preg_replace('/min/','',$arr['duration']));
            $arr['duration'] = trim(preg_replace('/tv movie/i','',$arr['duration']));
        }
//        print_r($arr);
//        die;

        //IMDB YEAR: (<a href="/year/2001/">2001</a>)
//        $strStart = '<a href="/year/';
//        $strEnd   = '/">';
//        $arr['year'] = $this->cleanString(strip_tags($this->extractStringFromString($html, $strStart, $strEnd)));
//        if ($arr['year'] === false) $arr['year'] = '';

        //IMDB YEAR: <h1 class="header" itemprop="name">The Shawshank Redemption<span class="nobr">(<a href="/year/1994/">1994</a>)</span>
        $arr['year'] = $this->match('/<h1 class="header" itemprop="name">(.*?)<\/h1>/ms',$html,1);
        if(!empty($arr['year'])){
            $arr['year'] = trim(strip_tags($arr['year']));
            $arr['year'] = trim($this->match('/\((.*?)\)/i',$arr['year'],1));
        }

        $arr['image'] = $this->match('/<meta property="og:image" content="(.*?)"\/>/ms',$html,1);
        $arr['image'] = (strpos($arr['image'],'logo.png') === FALSE) ? $arr['image'] : '';
        if(!empty($arr['image'])){
            $char = '._V1._';
            $arr['image'] = substr($arr['image'],0,strpos($arr['image'],$char));
            $arr['image'] = $arr['image'].'._V1_SX700.jpg';
        }
//        $arr['image'] = 'http://files.myopera.com/phanhung_good_to/albums/311087/IMG73ZNLQ7X56.jpg';

        //release_date: <time itemprop="datePublished" datetime="2006-03-24">24 March 2006</time>
        $strStart = '<time itemprop="datePublished" datetime="';
        $strEnd   = '">';
        $arr['release_date'] = $this->cleanString(strip_tags($this->extractStringFromString($html, $strStart, $strEnd)));
        if ($arr['release_date'] === false) $arr['release_date'] = '';


        $plot = $this->match('/Plot Keywords:(.*?)<\/div/ms',$html,1);
        foreach($this->match_all('/(.*?)<\/a>/ms',$plot,1) as $item){
            $crawler_plot = strip_tags(trim($item));
            $crawler_plot = preg_replace('/&nbsp;/','',$crawler_plot);
            $crawler_plot = preg_replace('/\|/','',$crawler_plot);
            $arr['plot'][] = trim($crawler_plot);
        }


        $genres = $this->match('/Genres:(.*?)<\/div/ms',$html,1);
        foreach($this->match_all('/(.*?)<\/a>/ms',$genres,1) as $item){
            $crawler_genres = strip_tags(trim($item));
            $crawler_genres = preg_replace('/&nbsp;/','',$crawler_genres);
            $crawler_genres = preg_replace('/\|/','',$crawler_genres);
            $arr['genres'][] = trim($crawler_genres);
        }

        $arr['poster'] = $this->match('/<a.*?name=.poster.*?src=.(.*?)(\'|")/ms', $html, 1);

        $arr['cast'] = array();
        foreach($this->match_all('/class="nm">(.*?\.\.\..*?)<\/tr>/ms', $html, 1) as $m)
        {
            list($actor, $character) = explode('...', strip_tags($m));
            $arr['cast'][trim($actor)] = trim($character);
        }

        return $arr;
    }

	// ****************************************************************

	function normalize($str)
	{
		$str = str_replace('_', ' ', $str);
		$str = str_replace('.', ' ', $str);
		$str = preg_replace('/ +/', ' ', $str);
		return $str;
	}

	function geturl($url, $username = null, $password = null)
	{
		$ch = curl_init();
		if(!is_null($username) && !is_null($password))
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic ' .  base64_encode("$username:$password")));
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		$html = curl_exec($ch);
		curl_close($ch);
		return $html;
	}

	function match_all($regex, $str, $i = 0)
	{
		if(preg_match_all($regex, $str, $matches) === false)
			return false;
		else
			return $matches[$i];

	}

	function match($regex, $str, $i = 0)
	{
		if(preg_match($regex, $str, $match) == 1)
			return $match[$i];
		else
			return false;
	}
    
    function extractStringFromString($string, $start, $end) {
        //
        $startPos = strpos($string,$start);
        $stringEndTagPos = strpos($string,$end,$startPos);
        $stringBetween = substr($string,$startPos+strlen($start),$stringEndTagPos-$startPos-strlen($start));
        //
        if (strlen($stringBetween) != 0) {
            //
            return $stringBetween;
            return true;
        }
        else {
            //
            return false;
        }
    }
    
    function cleanString($imputString) {
        $whatToCleanArray = array(chr(13),chr(10),chr(13).chr(10),chr(10).chr(13),"\n","  ","   ","    ","\n\n","\n\r");
        $cleanWithArray = array("","","","","","","","","","");
        $cleaned = str_replace($whatToCleanArray,$cleanWithArray,$imputString);
        $cleaned = trim($cleaned);
        return $cleaned;
    }
}