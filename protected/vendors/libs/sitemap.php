<?php
/** A class for generating simple google sitemaps
 *@author NguaGo <nguago@let.vn> 
 *@copyright 2012 
 *@version 0.1 
 *@access public 
 *@package sitemap 
 *@link http://let.vn 
 */ 
class sitemap {
    private static $header = "<\x3Fxml version=\"1.0\" encoding=\"UTF-8\"\x3F>\n\t<urlset xmlns=\"http://www.google.com/schemas/sitemap/0.84\">"; 
    private static $charset = "UTF-8"; 
    private static $footer = "\t</urlset>\n"; 
    private static $items = array(); 
    
	public static function render ($items = array()) {
        self::$items = $items;
        if (!empty(self::$items)) {
            return self::_build();
        }
        return '';
	}
    
	public static function header () {
        header( "Content-type: application/xml; charset=\"" . self::$charset . "\"", true ); 
        header( 'Pragma: no-cache' ); 
	}
    
    private static function _build() { 
        $map = self::$header . "\n"; 
        foreach(self::$items as $item) 
        {
            $item['loc'] = htmlentities($item['loc'], ENT_QUOTES);
            $map .= "\t\t<url>\n\t\t\t<loc>" . $item['loc'] . "</loc>\n";

            // lastmod
            if (!empty($item['lastmod']))
                $map .= "\t\t\t<lastmod>" . $item['lastmod'] . "</lastmod>\n"; 

            // changefreq
            if (!empty($item['changefreq']))
                $map .= "\t\t\t<changefreq>" . $item['changefreq'] . "</changefreq>\n";

            // priority
            if (!empty($item['priority']))
                $map .= "\t\t\t<priority>" . $item['priority'] . "</priority>\n";

            $map .= "\t\t</url>\n\n";
        } 

        $map .= self::$footer . "\n";

        return $map;
    }
} // End sitemap
