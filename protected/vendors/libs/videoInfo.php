<?php

/**
 * Class ho tro player, grab cac nguon video tren internet 
 */
Yii::import('application.vendors.libs.grabVideo');
class videoInfo extends grabVideo {

    public static function youtube($url) {
        $link = self::getMovieInfo($url);
        $youtubeId = self::getYoutubeId($link);

        if(empty($youtubeId)) return FALSE;

        $url =  'http://gdata.youtube.com/feeds/api/videos/'.$youtubeId.'?&alt=json';
        $data = self::getCurl($url);
        $data = json_decode($data,true);
        $result['title'] = !empty($data['entry']['title']['$t']) ? $data['entry']['title']['$t'] : NULL;
        $result['link'] = $link;
        $result['time'] = !empty($data['entry']['media$group']['yt$duration']['seconds']) ? $data['entry']['media$group']['yt$duration']['seconds'] : NULL ;
        $result['thumbnail'] = !empty($data['entry']['media$group']['media$thumbnail'][0]['url']) ? $data['entry']['media$group']['media$thumbnail'][0]['url'] : NULL;
        $result['id'] = $youtubeId;
        return $result;
    }

    /**
     * @param $url
     * @return array
     */
    public static function getMovieInfo($url){
        $html = self::getCurl($url);
        if(stripos($html, "302 Moved") !== false)
            $html = self::getCurl(self::match('/HREF="(.*?)"/ms', $html, 1));
        $arr = array();
        $arr = self::RegExpYouTube($html,$arr);
        return $arr;
    }
    public static function match_all($regex, $str, $i = 0)
    {
        if(preg_match_all($regex, $str, $matches) === false)
            return false;
        else
            return $matches[$i];

    }
    public static function RegExpYouTube($html,$arr = array()){
        $link = self::match('/meta property="og:url" content="(.*?)\">/ms', $html, 1);
        return $link;
    }
    public static function match($regex, $str, $i = 0)
    {
        if(preg_match($regex, $str, $match) == 1)
            return $match[$i];
        else
            return false;
    }

}
