<?php
Yii::import('application.vendors.libs.simple_html_dom');
class crawler {

    /**
     * Rule get list
     * @param string $html
     * @param string $tag
     * @param string $index
     * @return type 
     */
    public static function ruleGetList($type ,$content, $tag, $index = NULL,$attr = NULL) {
        if($type == 'url'){
            $html = self::file_get_html($content);
        } elseif($type == 'html') {
            $html = self::str_get_html($content);
        } else $html = letText::getCurl($content);

        if($index !== NULL AND $index !== ''){
            //Kiem tra co tim den attributes ko
            if($attr == NULL AND $attr ==''){
                $attr = 'outertext';
            }

            $result = ($html->find($tag, $index)) ? $html->find($tag, $index)->$attr : '';
            $result = trim($result);
        } else {
            $result = array();
            foreach($html->find($tag) as $item){
                $result[]  = $item->outertext;
            }
        }
        return $result;
    }

    public static function crawlerData($config,$link,$crawler_id){

        $content = $link;
        // Lay thong tin Crawler "baseUrl"

        Yii::import('application.modules.crawler.models.KitCrawler');

        if(!empty($crawler_id)){
            $crawler = KitCrawler::getDetails($crawler_id);
        }
        foreach($config as $rule){
            // Kiem tra ton tai ket qua theo key = $rule['object'] khong, neu co thi lay gia tri ket qua do, neu khong thi van lay html get ve.
            if (isset($rule['object']) AND $rule['object'] !== '' AND isset($result[$rule['object']]) AND $result[$rule['object']] !== '') {
                $content = $result[$rule['object']];
                $dataType = 'html';
            } else {
                $dataType = 'url';
            }
            // $result[$i][$rule['key']] la ket qua xu ly
            if($rule['type'] == 'getlist'){
                if(is_string($content)){
                    $result[$rule['key']] = crawler::ruleGetList($dataType,$content,$rule['tag'],$rule['index'],$rule['attr']);
                    // Neu is_replace ton tai va bang 1 thi thay the string $result[$rule['key']] bang gia tri o $rule['replace_text']
                    if(!empty($rule['is_replace']) AND $rule['is_replace'] == 1){
                        $rule['replace_text'] = !empty($rule['replace_text']) ? $rule['replace_text'] : '';
                        $result[$rule['key']] = trim(str_replace($result[$rule['key']],$rule['replace_text'],$content));
                    }


                } elseif(is_array($content)) {
                    // kiểm tra trường hợp $content là 1 mảng
                    foreach($content as $key => $value){
                        $data = crawler::ruleGetList($dataType,$value,$rule['tag'],$rule['index'],$rule['attr']);
                        $result[$rule['key']][] = $data;
                    }
                }

                // Neu action_url ton tai va action_url co gia tri bang 1 thi thay the cac Url cu ve dang day du
                if(isset($rule['action_url']) AND $rule['action_url'] == 1){
                    if(isset($crawler->baseUrl) AND !empty($crawler->baseUrl)){
                        $contents = $result[$rule['key']];
                        if(is_string($contents)){ // Xet truong hop content la string
                            $result[$rule['key']] = self::addBaseUrl($result[$rule['key']],$crawler->baseUrl);
                        } elseif (is_array($result[$rule['key']])){ // Xet truong hop content la array
                            $i = 0;
                            foreach($contents as $content){
                                unset($result[$rule['key']][$i]);
                                $result[$rule['key']][$i] = self::addBaseUrl($content,$crawler->baseUrl);
                                $i++;
                            }
                        }
                    }

                }

                // Neu action_url ton tai va action_url co gia tri bang 2 thi thay the cac Url cu voi Url la server minh
                if(isset($rule['action_url']) AND $rule['action_url'] == 2){
                    if(isset($crawler->baseUrl) AND !empty($crawler->baseUrl)){
                        $contents = $result[$rule['key']];
                        if(is_string($contents)){ // Xet truong hop content la string
                            $result[$rule['key']] = self::addBaseUrl($result[$rule['key']],$crawler->baseUrl);
                        } elseif (is_array($result[$rule['key']])){ // Xet truong hop content la array
                            $i = 0;
                            foreach($contents as $content){
                                unset($result[$rule['key']][$i]);
                                $result[$rule['key']][$i] = self::addbaseUrlServ($content,$crawler->baseUrl);
                                $i++;
                            }
                        }
                    }
                }


            } else {
                $result[$rule['key']] = crawler::ruleCutString($dataType,$content,$rule['tag'],$rule['index']);
            }
            //

        }
        foreach($result as $key => $item){
            if(is_string($item)){
                $result[$key] = $item;
            }
        }
        return $result;
    }

    /**
     * Ham Insert vao Database
     * @static
     * @param $config
     * @param $link
     * @return array
     */
    public static function insertDataBase($config, $link, $crawler_id = 0){

        $db = Yii::app()->db;
        $command = $db->CreateCommand();
        $data = array();

        // Chuyen doi mang ket qua cua qua trinh crawler sang dang mang moi luu theo table va field ($data[table][field] = value)
        $result = self::crawlerData($config, $link, $crawler_id);

        foreach($result as $key => $item){
            $field = isset($config[$key]['field']) ? $config[$key]['field'] : '';
            if ($field !== '') {
                $field = explode('.',$field);
                if (isset($field[1])) {
                    $table = $field[0];
                    $field = $field[1];
                    $data[$table][$field]['value'] = $item;
                    $data[$table][$field]['config'] = $config[$key];
                }

            }

        }
        /**
         * Dem so phan tu insert vao database cua tung mang
         * Neu gia tri cua tung field la string thi config de insert vao database 1 lan
         * Neu gia tri cua 1 field nao do la array thi config de insert vao database n(so phan tu cua field) lan
         */
        $tableConfig = array();
        foreach($data as $table => $fields){
            $tableConfig[$table] = 1;
            foreach($fields as $field => $value){

                if(is_array($value['value'])){
                    $tableConfig[$table] = count($value['value']);
                }
            }
        }
        foreach($tableConfig as $table => $numElement){ //Lay ten bang va so lan insert vao bang do
            for($i = 0; $i < $numElement; $i++){ // moi vong lap la 1 lan insert
                $insertDatabase = array();
                $validate = 1; // cho phep insert
                foreach($data[$table] as $fieldName => $fieldValue){ // lay danh sach truong va gia tri cua chung cho moi bang
                    //Kiem tra xem co crawler_id ko
                    if(!empty($crawler_id)  AND $crawler_id != 0){
                        $insertDatabase['item_id'] = $crawler_id;
                    }
                    if(is_string($fieldValue['value'])){ // Neu gia tri cua truong la str thi gia tri insert bang chinh no
                        $insertDatabase[$fieldName] = $fieldValue['value'];
                    } elseif(is_array($fieldValue['value']) AND isset($fieldValue['value'][$i])){ // Neu gia tri cua truong la array thi gia tri insert lay theo phan tu thu i
                        $insertDatabase[$fieldName] = $fieldValue['value'][$i];
                    } else {
                        $insertDatabase[$fieldName] = '';
                    }

                    // Kiem tra gia tri cua tung truong (doi voi nhung truong yeu cau ton tai)
                    $check_exists = 0;
                    if(isset($config[$data[$table][$fieldName]['config']['key']]['check_exists']) AND (int)$config[$data[$table][$fieldName]['config']['key']]['check_exists'] == 1)
                        $check_exists = 1;

                    if ($check_exists = 1 AND $insertDatabase[$fieldName] == '')
                        $validate = 0; // khong cho phep insert
                }

                // insert
                if ($validate == 1) { // kiem tra su cho phep insert
                    $insert = $command->insert($table,$insertDatabase);
                    $command->reset();
                    echo "Log : ".implode('_',$insertDatabase)."\n";
                }

            }
        }
        return $result;
    }


    /**
     * Ham get du lieu tu bang config crawler
     * @static
     * @param $crawler_id
     * @return mixed
     */
    public static function getConfig($crawler_id){
        Yii::import('application.modules.crawler.models.KitCrawlerConfig');
        $cache_name = md5('crawler_' .$crawler_id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE OR TRUE) {
            $criteria=new CDbCriteria;
            $criteria->select='*'; // only select the 'title' column
            $criteria->condition='item_id=:itemId';
            $criteria->params=array(':itemId'=> $crawler_id);
            $result = KitCrawlerConfig::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

    /**
     * @static
     * @param $crawler_id
     * @return mixed
     */
    public static function getSource($crawler_id, $ext = 0){
        Yii::import('application.modules.crawler.models.KitCrawlerSource');
        $cache_name = md5('crawler_source_' .$crawler_id.'_'.$ext);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE OR $ext == 1) {
            $criteria=new CDbCriteria;
            $criteria->select='*'; // only select the 'title' column
            $criteria->condition='item_id=:itemId';
            $criteria->params=array(':itemId'=> $crawler_id);

            $result = KitCrawlerSource::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

    public static function getLink($crawler_id){
        Yii::import('application.modules.crawler.models.KitCrawlerLink');
        $cache_name = md5('crawler_link_' .$crawler_id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria=new CDbCriteria;
            $criteria->select='*'; // only select the 'title' column
            $criteria->condition='item_id=:itemId';
            $criteria->params=array(':itemId'=> $crawler_id);

            $result = KitCrawlerLink::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

    /**
     * @static Ham tim kiem $link thuoc config nao
     * @param $link
     * @param $crawler_id
     * @return array
     */
    public static function detectConfig($link,$crawler_id){
        // get config theo du an
        $configs = crawler::getConfig($crawler_id);
        $result = array();
        foreach($configs as $config){// Liet ke tat cac cac config da co trong database
            // replace tat ca ki tu / trong truong regex thanh \/ cho dung cu phap de su dung preg_match
            $regex = str_replace('/','\/',$config->regex);
            if(preg_match('/'.$regex.'/i',$link,$data)){ // Kiem tra $link xem phu hop coi regex nao
                $result[] = json_decode($config->config,true);
            }
        }
        return $result;
    }

    /**
     * @static add them http vao link
     * @param $url
     * @return string
     */
    public static function addHttp($url, $baseUrl = 'http://') {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = $baseUrl . $url;
        }
        return $url;
    }

    /**
     * @static Ham lay Url cua A
     * @param $content
     * @param $baseUrl
     * @return bool|mixed
     */
    public static function getLinkFromHtml($content, $tags = array('a'=>'href', 'img' => 'src')){
        $urls = array();
        $html = self::str_get_html($content);
        if(empty($html))
            return array();
        foreach($tags as $tag => $attribute){
            foreach($html->find($tag) as $row){
                $urls[] = $row->$attribute;
            }
        }
        return array_unique($urls);
    }

    /**
     * @static
     * @param $content
     * @param $baseUrl
     * @return mixed
     */
    public static function addBaseUrl($content,$baseUrl){
        // add BaseUrl vao url
        $urls = array();
        $urls_tmp = self::getLinkFromHtml($content);
        foreach($urls_tmp as $key => $url){
            $key = md5(rand(0,1000000));
            $urls[$key] = $url;
            $content = str_replace($url,$key,$content);
        }
        foreach($urls as $key => $url){
            $content = str_replace($key,self::addHttp($url,$baseUrl),$content);
        }
        return $content;
    }

    /**
     * @static
     * @param $content
     * @param $baseUrl
     * @return mixed
     */
    public static function addbaseUrlServ($content,$baseUrl){
        try
        {
            $urls = array();
            $content = self::addBaseUrl($content,$baseUrl);
            $images = self::getLinkFromHtml($content,array('img' => 'src'));
            foreach($images as $images){
                $key = md5(rand(0,1000000));
                $urls[$key] = $images;
                $content = str_replace($images,$key,$content);
            }

            foreach($urls as $key => $url){
                $content = str_replace($key,self::_createThumb($url),$content);
            }

            return $content;
        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
    }

    /**
     * Rule cut string
     * @param string $html
     * @param string $tag
     * @param string $index 
     */
    public static function ruleCutString($html, $tag, $index = NULL) {
        $html = self::str_get_html($html);

    }

    /**
     * @static Thuc hien luu anh voi n image
     * @param $inputImages
     * @return array
     */
    public static function _saveImage($inputImages){
        try{
            $images = array();
            if(is_array($inputImages) AND count($inputImages) > 0){
                foreach($inputImages as $image){
                    $images[] = self::_createThumb($image);
                }
            }
            return $images;
        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
    }

    /**
     * Ham thuc hien luu anh ve tu duong dan khac ve server minh
     * @param $inputImage
     */
    public static function _createThumb ($linkImage) {
        try
        {
            $date = date('Y/m/d/');
            $tmpFileInfo = pathinfo($linkImage);
            $fileName = md5($linkImage.$date) . '.' . $tmpFileInfo['extension'];
            $fileDir = 'crawler/large/' . $date;
            Common::mkPath($fileDir, Common::getImageUploaded('', 'PATH'));
            $filePath = Common::getImageUploaded($fileDir . $fileName, 'PATH');
            copy($linkImage, $filePath);
            return "/uploads/".$fileDir.$fileName;
        } catch(Exception $e) {
            Yii::log($e->getMessage(), 'info');
        }
    }

// helper functions
// -----------------------------------------------------------------------------
// get html dom from file
// $maxlen is defined in the code as PHP_STREAM_COPY_ALL which is defined as -1.
    public static function file_get_html($url, $use_include_path = false, $context=null, $offset = -1, $maxLen=-1, $lowercase = true, $forceTagsClosed=true, $target_charset = "UTF-8", $stripRN=true, $defaultBRText="\r\n")
    {
        // We DO force the tags to be terminated.
        $dom = new simple_html_dom(null, $lowercase, $forceTagsClosed, $target_charset, $defaultBRText);
        // For sourceforge users: uncomment the next line and comment the retreive_url_contents line 2 lines down if it is not already done.
        $contents = file_get_contents($url, $use_include_path, $context, $offset);
        // Paperg - use our own mechanism for getting the contents as we want to control the timeout.
        //    $contents = retrieve_url_contents($url);
        if (empty($contents))
        {
            return false;
        }
        // The second parameter can force the selectors to all be lowercase.
        $dom->load($contents, $lowercase, $stripRN);
        return $dom;
    }

// get html dom from string
    public static function str_get_html($str, $lowercase=true, $forceTagsClosed=true, $target_charset = 'UTF-8', $stripRN=true, $defaultBRText="\r\n")
    {
        $dom = new simple_html_dom(null, $lowercase, $forceTagsClosed, $target_charset, $defaultBRText);
        if (empty($str))
        {
            $dom->clear();
            return false;
        }
        $dom->load($str, $lowercase, $stripRN);
        return $dom;
    }

// dump html dom tree
    public static function dump_html_tree($node, $show_attr=true, $deep=0)
    {
        $node->dump($node);
    }


} // End grapVideo
