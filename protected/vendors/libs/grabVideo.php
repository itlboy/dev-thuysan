<?php

/**
 * Class ho tro player, grab cac nguon video tren internet 
 */
class grabVideo {

    /**
     * Get embed for link
     * grabVideo::player('http://youtu.be/D7n_AsUw3nQ', 770, 440);
     * @param string $link
     * @param int $width
     * @param int $height
     * @return string
     */
	public static function player ($link = '', $width = 640, $height = 390) {
        $serverAlias = self::getNameServer($link);
        if ($serverAlias == 'youtube') return self::youtubePlayer ($link, $width, $height);
        
		return self::defaultPlayer($link, $serverAlias, $width, $height);
	}

    /**
     * Get default Player
     * @param string $link
     * @param string $serverAlias
     * @param int $width
     * @param int $height
     * @return string 
     */
	public static function defaultPlayer ($link = '', $serverAlias = '', $width = 640, $height = 390) {
		return $embed = '
            <object id="flashplayer" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="' . $width . '" height="' . $height . '">
                <param name="movie" value="http://player.let.vn/player.swf" />
                <param name="allowFullScreen" value="true" />
                <param name="allowScriptAccess" value="always" />
                <param name="FlashVars" value="plugins=http://player.let.vn/plugins/' . $serverAlias . '/proxy.swf&proxy.link=' . $link . '&skin=skins/newtube/newtube.xml" />
                <embed name="flashplayer" src="http://player.let.vn/player.swf" FlashVars="plugins=http://player.let.vn/plugins/' . $serverAlias . '/proxy.swf&proxy.link=' . $link . '&skin=http://player.let.vn/skins/newtube/newtube.xml" type="application/x-shockwave-flash" allowfullscreen="true" allowScriptAccess="always" width="' . $width . '" height="' . $height . '" />
            </object>
        ';
	}

    /**
     * get Youtube Player
     * @param string $link
     * @param int $width
     * @param int $height
     * @return string 
     */
	public static function youtubePlayer ($link = '', $width = 640, $height = 390) {
        $id = self::getYoutubeId($link);
        if ($id !== FALSE) $link = 'http://www.youtube.com/v/' . $id;
		return $embed = '
            <object height="' . $height . '" width="' . $width . '">
                <param value="' . $link . '&amp;hl=vi_VN&amp;feature=player_embedded&amp;version=3" name="movie" />
                <param value="true" name="allowFullScreen" />
                <param value="always" name="allowScriptAccess" />
                <param name="wmode" value="opaque" />
                <embed height="' . $height . '" width="' . $width . '" allowscriptaccess="always" allowfullscreen="true" type="application/x-shockwave-flash" src="' . $link . '&amp;hl=vi_VN&amp;feature=player_embedded&amp;version=3" wmode="opaque" />
            </object>
        ';
	}

    /**
     * Get Youtube ID
     * @param string $link
     * @return string 
     */
	public static function getYoutubeId ($link = '') {
        $pattern = '/(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i';
        preg_match($pattern, $link, $matches);
        return isset($matches[1]) ? $matches[1] : FALSE;
	}

    /**
     * get Name Server for Link
     * @param string $link
     * @return string 
     */
	public static function getNameServer ($link = '') {
        // Fix
        $maps = array(
            'picasaweb' => 'picasaweb.google.com',
            'docsgoogle' => 'docs.google.com',
            '4sharevn' => '4share.vn',
            'glumbo' => 'glumbouploads.com',
            'telly' => 'twitvid.com',
            'zingvn' => 'zing.vn',
            'youtube' => 'youtu.be',
            'govn' => 'go.vn',
        );
        foreach ($maps as $nameServer => $host) {
            if (preg_match('/' . $host . '/i', $link)) return $nameServer;
        }

        // Auto
		$link = parse_url($link);
        if (isset($link['host'])) {
            $server = explode ('.', $link['host']);
            if (isset($server[2]))
                $nameServer = $server[1];
            else
                $nameServer = $server[0];
        } else
            $nameServer = 'other';
        
        return $nameServer;
	}

    public static function getCurl($url, $username = null, $password = null)
    {
        $ch = curl_init();
        if(!is_null($username) && !is_null($password))
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic ' .  base64_encode("$username:$password")));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        $html = curl_exec($ch);
        curl_close($ch);
        return $html;
    }



} // End grapVideo
