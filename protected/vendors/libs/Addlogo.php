<?php

class Addlogo {
	var $img_logo = '';
	var $img_pic = '';
	
	var $dpx = 5;
	var $dpy = 5;

	function createImageAndLogo($pic_file, $logo_file) {
		$type = exif_imagetype($pic_file); // [] if you don't have exif you could use getImageSize() 
		$allowedTypes = array( 
			1,  // [] gif 
			2,  // [] jpg 
			3,  // [] png 
			6   // [] bmp 
		); 
		if (!in_array($type, $allowedTypes)) { 
			return false; 
		} 
		switch ($type) { 
			case 1 : 
				$this->img_pic = imageCreateFromGif($pic_file); 
			break; 
			case 2 : 
				$this->img_pic = imageCreateFromJpeg($pic_file); 
			break; 
			case 3 : 
				$this->img_pic = imageCreateFromPng($pic_file); 
			break; 
			case 6 : 
				$this->img_pic = imageCreateFromBmp($pic_file); 
			break; 
		}   
		$this->img_logo = imagecreatefrompng($logo_file);
	}

	// note: $align | 1 = top left, 2 = top right, 3 = bottom right, 4 = bottom left, 5 = bottom center, 6 = top center

	function setAlign($align) {
		if ($align ==1) {
			$this->dpy = 5;
			$this->dpx = 5;
		} elseif ($align ==2) {
			$this->dpy = 5;
			$this->dpx = imagesx($this->img_pic)-(imagesx($this->img_logo)+5);
		} elseif ($align ==3) {
			$this->dpy = imagesy($this->img_pic)-(imagesy($this->img_logo)+5);
			$this->dpx = imagesx($this->img_pic)-(imagesx($this->img_logo)+5);
		} elseif ($align ==4) {
			$this->dpy = imagesy($this->img_pic)-(imagesy($this->img_logo)+5);
			$this->dpx = 5;
		} elseif ($align ==5) {
			$this->dpy = imagesy($this->img_pic)-(imagesy($this->img_logo)+5);
			$dc = imagesx($this->img_pic)/2;
			$dc2 = imagesx($this->img_logo)/2;
			$this->dpx = $dc-$dc2;
		} elseif ($align ==6) {
			$this->dpy = 5;
			$dc = imagesx($this->img_pic)/2;
			$dc2 = imagesx($this->img_logo)/2;
			$this->dpx = $dc-$dc2;
		} else {
			$this->dpy = 5;
			$this->dpx = 5;
		}

	}

	function InsertLogo($align) {
		$this->setAlign($align);
		$sx = imagesx($this->img_logo);
		$sy = imagesy($this->img_logo);

		imagecopy($this->img_pic, $this->img_logo, $this->dpx, $this->dpy, 0, 0, $sx, $sy);
	}

	function save($image = '') {
		imagejpeg($this->img_pic, $image);
	}
}