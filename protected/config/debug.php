<?php
$list = array(
    dirname(__FILE__).'/local.php',
    dirname(__FILE__).'/modules.php',
);
$config = array();
foreach ($list as $key => $value) {
    if ($key == 0) $config = require($value);
    else $config = CMap::mergeArray($config, require($value));
}

return CMap::mergeArray(
    $config,
    array(
        // preloading 'log' component
        'preload' => array('log'),

        // autoloading model and component classes
        'components'=>array(
            'cache'=>array(
                'class'=>'system.caching.CFileCache',
            ),
        ),
    )
);