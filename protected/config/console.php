<?php

$list = array(
    dirname(__FILE__).'/main.php',
//    dirname(__FILE__).'/local.php',
);

$config = array();
foreach ($list as $key => $value) {
    if(file_exists($value))
        $config = CMap::mergeArray($config, require($value));
}

//$hostname = 'localhost';
//$username = 'root';
//$password = '';
//$database = 'letkit';

$hostname = 'localhost';
$username = 'letkit';
$password = 'f72685f6d9fd7f385df9122c';
$database = 'letkit';


return CMap::mergeArray(
$config,
array(
	// autoloading model and component classes
    'import' => array(
        'application.vendors.helper.*',
        'application.components.frontend.*',
        'application.modules.account.models.*',
        'application.components.cms.*',
    ),

	// application components
	'components'=>array(
        'db'=>array(
            'class'=>'CDbConnection',
            'connectionString'=>'mysql:host='.$hostname.';dbname='.$database.'',
            'username'=>$username,
            'password'=>$password,
            'charset' => 'utf8',
            'emulatePrepare'=>true,  // needed by some MySQL installations
            'tablePrefix'=>'let_',

            'enableProfiling'=>true,
            'enableParamLogging' => true,

            'schemaCachingDuration' => 1
        ),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'info',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
        'image'=>array(
            'class'=>'application.extensions.image.CImageComponent',
            // GD or ImageMagick
            'driver'=>'GD',
            // ImageMagick setup path
            'params'=>array('directory'=>'/opt/local/bin'),
        ),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
        'coreMessages'=>array(
            'basePath'=>null,
        ),
  	),
));