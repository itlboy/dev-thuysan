<?php

unset($config['modules']);
$projectConfig = array(
    'theme' => 'frontend/bovn',
    'bootstrap' => FALSE,
    'debug' => FALSE,
    'errorHandler' => FALSE,
);

return
    array(
        'name'=>'World of beo love - For Everything Bang & Olufsen.',
        'components' => array(
            'cache'=>array(
                'class'=>'system.caching.CFileCache',
            ),
            'urlManager' => array(
                'rules' => array(
                ),
                'urlSuffix' => '.bolov',
            ),
            'user' => array(
                // enable cookie-based authentication
                'allowAutoLogin' => true,
            ),
//        'log' => array(
//            'class' => 'CLogRouter',
//            'routes' => array(
//                array(
//                    'class' => 'CFileLogRoute',
//                    'levels' => 'error',
//                ),
//            // uncomment the following to show log messages on web pages
//            /*
//                array(
//                'class'=>'CWebLogRoute',
//                ),
//                */
//            ),
//        ),
        ),
        'modules' => array(
            'cms',
            'account' => array(
                'returnUrl' => array('//cms/default'),
            ),
            'category',
            'article',
            'product',
            'gallery',

        ),
        // application-level parameters that can be accessed
        // using Yii::app()->params['paramName']
        'params'=>array(
            'timeCache' => 5,
            'timeFormat' => array(
                'backend_default' => 'H:i:s d/m/Y',
                'frontend_default' => 'H:i:s d/m/Y',
            ),

            // Upload
            'uploadUrl' => 'http://worldofbeolove.com', // http://phimle.vn
            'uploadDir' => 'uploads', // uploads
            'uploadPath' => dirname(__FILE__) . '/../../../', // dirname(__FILE__) . '/../..'
        ),
    );