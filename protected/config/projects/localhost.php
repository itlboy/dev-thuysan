<?php

return
    array(
        'components' => array(
            'cache'=>array(
                'class'=>'system.caching.CFileCache',
            ),
            'user' => array(
                'class' => 'WebUser',
                // enable cookie-based authentication
                'allowAutoLogin' => true,
            ),
            'session' => array(
                'cookieMode' => 'allow',
            ),

            'log' => array(
                'class' => 'CLogRouter',
                'routes' => array(
                    array(
                        'class' => 'CFileLogRoute',
                        'levels' => 'error,info',
                    ),
                // uncomment the following to show log messages on web pages
                /*
                    array(
                    'class'=>'CWebLogRoute',
                    ),
                    */
                ),
            ),
        ),
        'params'=>array(
            'timeFormat' => array(
                'backend_default' => 'H:i:s d/m/Y',
                'frontend_default' => 'H:i:s d/m/Y',
            ),

            // Upload
//            'uploadUrl' => 'http://localhost/letkit', // http://phimle.vn
            'uploadUrl' => 'http://data.let.vn', // http://phimle.vn
            'uploadDir' => 'uploads', // uploads
            'uploadPath' => dirname(__FILE__) . '/../../../', // dirname(__FILE__) . '/../..'
        ),
    );