<?php
$projectConfig = array(
    'theme' => 'frontend/account',
    'bootstrap' => TRUE,
    'debug' => FALSE,
    'errorHandler' => FALSE,
);

// Thông tin kết nối data server Game
//$hostname = '27.118.18.140';
//$username = 'amx';
//$password = 'TvQd9Tfxhs5CZMAa';
//$database = 'amx';

return
    array(
        'id' => $sso_domain,
        'name'=>'Quản lý Account',
        'defaultController' => 'account/frontend/profile',
        'components' => array(
            'cache'=>array(
                'class'=>'system.caching.CFileCache',
            ),
//            'db2'=>array(
//                'class'=>'CDbConnection',
//                'connectionString'=>'mysql:host='.$hostname.';dbname='.$database.'',
//                'username'=>$username,
//                'password'=>$password,
//                'charset' => 'utf8',
//                'emulatePrepare'=>true,  // needed by some MySQL installations
//                'tablePrefix'=>'let_',
//
//                'enableProfiling'=>true,
//                'enableParamLogging' => true,
//
//                'schemaCachingDuration' => 1
//            ),
            'user' => array(
                'class' => 'WebUser',
                // enable cookie-based authentication
                'allowAutoLogin' => true,
            ),
            'session' => array(
                'timeout' => 604800, // 7 days
                'savePath' => '/tmp/let',
                'cookieMode' => 'allow',
                'cookieParams' => array(
                    'path' => '/',
                    'domain' => '.' . $sso_domain,
                    'httpOnly' => true,
                ),
            ),
//            'session' => array (
//                'sessionName' => 'SSOID',
//                'class' => 'CDbHttpSession',
//                'autoCreateSessionTable' => true,
//                'connectionID' => 'db',
//                'sessionTableName' => 'let_kit_account_sessions',                
//                'cookieMode' => 'allow',
//                'autoStart' => true,
//                'timeout' => 60*60*24,
//                'cookieParams' => array(
//                    'path' => '/',
//                    'domain' => '.' . $sso_domain,
//                    'httpOnly' => true,
//                ),
//            ),
            'urlManager' => array(
                'rules' => array(
                    // account
                    'home' => 'cms/frontend/default/HomeProfile',
                    // rewrite cho cms
                    '<module:\w+>/<controller:\w+>' => '<module>/frontend/<controller>/index',
                    '<module:\w+>/<controller:\w+>/<action:\w+>/*' => '<module>/frontend/<controller>/<action>',
                ),
                'urlSuffix' => '.let',
            ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error',
                ),
            // uncomment the following to show log messages on web pages
            /*
                array(
                'class'=>'CWebLogRoute',
                ),
                */
            ),
        ),
        ),
        // application-level parameters that can be accessed
        // using Yii::app()->params['paramName']
        'params'=>array(
            'timeFormat' => array(
                'backend_default' => 'H:i:s d/m/Y',
                'frontend_default' => 'H:i:s d/m/Y',
            ),

            // Upload
            'uploadUrl' => 'http://data.let.vn', // http://phimle.vn
            'uploadDir' => 'uploads', // uploads
            'uploadPath' => '/home/let.vn/data', // dirname(__FILE__) . '/../..'            
        ),
    );