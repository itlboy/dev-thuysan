<?php
$sso_domain = 'let.vn';

$projectConfig = array(
    'theme' => 'frontend/mobilegame',
    'bootstrap' => FALSE,
    'debug' => TRUE,
    'errorHandler' => TRUE,
);

return
    array(
        'id' => $sso_domain,
        'name'=>'Kinggame.info',
        'components' => array(
            'cache'=>array(
                'class'=>'system.caching.CFileCache',
//                'class'=>'system.caching.CXCache',
            ),
            'user' => array(
                'class' => 'WebUser',
                // enable cookie-based authentication
                'allowAutoLogin' => true,
                'loginUrl' => array('/'),
            ),
            'session' => array(
                'savePath' => '/tmp/let',
                'cookieMode' => 'allow',
				'timeout' => 604800, // 7 days
                'cookieParams' => array(
                    'path' => '/',
                    'domain' => '.' . $sso_domain,
                    'httpOnly' => true,
                ),
            ),
//            'session' => array (
//                'sessionName' => 'SSOID',
//                'class' => 'CDbHttpSession',
//                'autoCreateSessionTable' => true,
//                'connectionID' => 'db',
//                'sessionTableName' => 'let_kit_account_sessions',                
//                'cookieMode' => 'allow',
//                'autoStart' => true,
//                'timeout' => 60*60*24,
//                'cookieParams' => array(
//                    'path' => '/',
//                    'domain' => '.' . $sso_domain,
//                    'httpOnly' => true,
//                ),
//            ),
            'urlManager' => array(
                'rules' => array(
                    'tim-kiem-vui-cuoi' => 'pin/frontend/search',
                    'wall-<id:\d+>' => 'pin/frontend/wall/index',
                    'wall-<id:\d+>/<action:\w+>' => 'pin/frontend/wall/<action>',
                    'cuoc-thi-tim-kiem-troller-tai-nang' => 'cms/frontend/contest/troller',
                ),
                'urlSuffix' => '.html',
            ),
            'log' => array(
                'class' => 'CLogRouter',
                'routes' => array(
                    array(
                        'class' => 'CFileLogRoute',
                        'levels' => 'error',
                    ),
                // uncomment the following to show log messages on web pages
                /*
                    array(
                    'class'=>'CWebLogRoute',
                    ),
                    */
                ),
            ),
        ),
        // application-level parameters that can be accessed
        // using Yii::app()->params['paramName']
        'params'=>array(
            'timeFormat' => array(
                'backend_default' => 'H:i:s d/m/Y',
                'frontend_default' => 'H:i:s d/m/Y',
            ),

            // Upload
            'uploadUrl' => 'http://data.let.vn:8080', // http://phimle.vn
            'uploadDir' => 'uploads', // uploads
            'uploadPath' => '/home/let.vn/data', // dirname(__FILE__) . '/../..'
//            'uploadUrl' => 'http://210.245.90.239/data', // http://phimle.vn
//            'uploadDir' => 'uploads', // uploads
//            'uploadPath' => '/storage/data', // dirname(__FILE__) . '/../..'
//            'uploadPath' => dirname(__FILE__) . '/../../../', // dirname(__FILE__) . '/../..'
        ),
    );