<?php
$sso_domain = 'let.vn';

$projectConfig = array(
    'theme' => 'frontend/cs',
    'bootstrap' => TRUE,
    'debug' => TRUE,
    'errorHandler' => FALSE,
);

// Thông tin kết nối data server Game
$hostname = '210.245.90.238';
$username = 'amx';
$password = 'BxThm3BdTYJJaACj';
$database = 'amx';

return
    array(
        'id' => $sso_domain,
        'name'=>'LetCS - Nơi bản lĩnh thăng hoa',
        'components' => array(
            'cache'=>array(
                'class'=>'system.caching.CFileCache',
//                'class'=>'system.caching.CXCache',
            ),
            'db2'=>array(
                'class'=>'CDbConnection',
                'connectionString'=>'mysql:host='.$hostname.';dbname='.$database.'',
                'username'=>$username,
                'password'=>$password,
                'charset' => 'utf8',
                'emulatePrepare'=>true,  // needed by some MySQL installations
                'tablePrefix'=>'let_',

                'enableProfiling'=>true,
                'enableParamLogging' => true,

                'schemaCachingDuration' => 1
            ),
            'user' => array(
                'class' => 'WebUser',
                // enable cookie-based authentication
                'allowAutoLogin' => true,
            ),
            'session' => array(
                'savePath' => '/tmp/let',
                'cookieMode' => 'allow',
				'timeout' => 604800, // 7 days
                'cookieParams' => array(
                    'path' => '/',
                    'domain' => '.' . $sso_domain,
                    'httpOnly' => true,
                ),
            ),
//            'session' => array (
//                'sessionName' => 'SSOID',
//                'class' => 'CDbHttpSession',
//                'autoCreateSessionTable' => true,
//                'connectionID' => 'db',
//                'sessionTableName' => 'let_kit_account_sessions',                
//                'cookieMode' => 'allow',
//                'autoStart' => true,
//                'timeout' => 60*60*24,
//                'cookieParams' => array(
//                    'path' => '/',
//                    'domain' => '.' . $sso_domain,
//                    'httpOnly' => true,
//                ),
//            ),
            'urlManager' => array(
                'rules' => array(
                ),
                'urlSuffix' => '.let',
            ),
//            'log' => array(
//                'class' => 'CLogRouter',
//                'routes' => array(
//                    array(
//                        'class' => 'CFileLogRoute',
//                        'levels' => 'error',
//                    ),
//                // uncomment the following to show log messages on web pages
//                /*
//                    array(
//                    'class'=>'CWebLogRoute',
//                    ),
//                    */
//                ),
//            ),
        ),
        // application-level parameters that can be accessed
        // using Yii::app()->params['paramName']
        'params'=>array(
            'timeFormat' => array(
                'backend_default' => 'H:i:s d/m/Y',
                'frontend_default' => 'H:i:s d/m/Y',
            ),

            // Upload
            'uploadUrl' => 'http://data.let.vn', // http://phimle.vn
            'uploadDir' => 'uploads', // uploads
            'uploadPath' => '/home/let.vn/data', // dirname(__FILE__) . '/../..'
//            'uploadPath' => dirname(__FILE__) . '/../../../', // dirname(__FILE__) . '/../..'
        ),
    );