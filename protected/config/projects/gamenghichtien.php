<?php
$sso_domain = 'let.vn';

$projectConfig = array(
    'theme' => 'frontend/gamenghichtien',
    'bootstrap' => FALSE,
    'debug' => FALSE,
    'errorHandler' => FALSE,
);

return
    array(
        'id' => $sso_domain,
        'name'=>'Nghịch Tiên',
        'components' => array(
            'cache'=>array(
                'class'=>'system.caching.CFileCache',
//                'class'=>'system.caching.CXCache',
            ),
            'user' => array(
                'class' => 'WebUser',
                // enable cookie-based authentication
                'allowAutoLogin' => true,
            ),
            'session' => array(
                'savePath' => '/tmp/let',
                'cookieMode' => 'allow',
				'timeout' => 604800, // 7 days
                'cookieParams' => array(
                    'path' => '/',
                    'domain' => '.' . $sso_domain,
                    'httpOnly' => true,
                ),
            ),
//            'session' => array (
//                'sessionName' => 'SSOID',
//                'class' => 'CDbHttpSession',
//                'autoCreateSessionTable' => true,
//                'connectionID' => 'db',
//                'sessionTableName' => 'let_kit_account_sessions',                
//                'cookieMode' => 'allow',
//                'autoStart' => true,
//                'timeout' => 60*60*24,
//                'cookieParams' => array(
//                    'path' => '/',
//                    'domain' => '.' . $sso_domain,
//                    'httpOnly' => true,
//                ),
//            ),
            'urlManager' => array(
                'rules' => array(
                    'cho-tinh' => 'classiads/frontend/default/index',
                    'cho-tinh/<user_id:\d+>' => 'classiads/frontend/default/index',
                    'cho-tinh/dang-tin' => 'classiads/frontend/create',

                    'cuoc-hen' => 'date/frontend/default/index',
                    'cuoc-hen/sap-dien-ra' => 'date/frontend/default/index/type/start',
                    'cuoc-hen/da-dien-ra' => 'date/frontend/default/index/type/end',
                    'cuoc-hen/toi-da-tham-ra' => 'date/frontend/default/index/type/me',
                    'cuoc-hen/<user_id:\d+>' => 'date/frontend/default/index',
                    'cuoc-hen/tao-cuoc-hen' => 'date/frontend/default/create',
                    // Film
//                    '<title>-film-<id:\d+>-<episode:\d+>' => 'film/frontend/details',
//                    '<title>-film-<id:\d+>' => 'film/frontend/details',
//                    'tim-kiem-phim' => 'film/frontend/search',
//                    'tag' => 'film/frontend/search/index',
//                    'tag/<keyword>' => 'film/frontend/search/index/keyword/<keyword>',
//                    'phim-bo' => 'film/frontend/search/index/length_1/1',
//                    'phim-le' => 'film/frontend/search/index/length_0/1',
//                    'phim-hot' => 'film/frontend/search/index/hot/1',
//                    'phim-chieu-rap' => 'film/frontend/search/index/theater/1',
//                    'phim-de-cu' => 'film/frontend/search/index/promotion/1',
//                    '<id:\d+>/<title>' => 'film/frontend/search', // /4/phim-Hai-huoc.let
//
//                    'xem-phim/<title>/f<id:\d+>' => 'film/frontend/redirect', // xem-phim/Lan-Dau-Tien/f2420.let
//                    'xem-phim/<title>/<title2>/f<id:\d+>' => 'film/frontend/redirect', // xem-phim/Lan-Dau-Tien/Lan-Dau-Tien/f2420.let
//                    'xem-phim/<title>/<title2>/<title3>/f<id:\d+>' => 'film/frontend/redirect', // xem-phim/Lan-Dau-Tien/Lan-Dau-Tien/Lan-Dau-Tien/f2420.let
//                    //xem-phim/Tom--va-Jerry---Flirty-Birdy/Tom-and-Jerry---Flirty-Birdy/e679-0.let
//                    'xem-phim/<title>/e<id:\d+>-<ep:\d+>' => 'film/frontend/redirect',
//                    'xem-phim/<title>/<title2>/e<id:\d+>-<ep:\d+>' => 'film/frontend/redirect',
//                    'xem-phim/<title>/<title2>/<title3>/e<id:\d+>-<ep:\d+>' => 'film/frontend/redirect',
//
//                    //anime/Lan-Dau-Tien/f2420.let
//                    'anime/<title>/f<id:\d+>' => 'film/frontend/redirect',
//                    'anime/<title>/<title2>/f<id:\d+>' => 'film/frontend/redirect',
//                    'anime/<title>/<title2>/<title3>/f<id:\d+>' => 'film/frontend/redirect',
                ),
                'urlSuffix' => '.let',
            ),
//            'log' => array(
//                'class' => 'CLogRouter',
//                'routes' => array(
//                    array(
//                        'class' => 'CFileLogRoute',
//                        'levels' => 'error',
//                    ),
//                // uncomment the following to show log messages on web pages
//                /*
//                    array(
//                    'class'=>'CWebLogRoute',
//                    ),
//                    */
//                ),
//            ),
        ),
        // application-level parameters that can be accessed
        // using Yii::app()->params['paramName']
        'params'=>array(
            'timeFormat' => array(
                'backend_default' => 'H:i:s d/m/Y',
                'frontend_default' => 'H:i:s d/m/Y',
            ),

            // Upload
            'uploadUrl' => 'http://data.let.vn:8080', // http://phimle.vn
            'uploadDir' => 'uploads', // uploads
            'uploadPath' => '/home/let.vn/data', // dirname(__FILE__) . '/../..'
//            'uploadUrl' => 'http://210.245.90.239/data', // http://phimle.vn
//            'uploadDir' => 'uploads', // uploads
//            'uploadPath' => '/storage/data', // dirname(__FILE__) . '/../..'
//            'uploadPath' => dirname(__FILE__) . '/../../../', // dirname(__FILE__) . '/../..'
        ),
    );