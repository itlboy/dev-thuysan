<?php
$sso_domain = 'vailua.vn';

$projectConfig = array(
    'theme' => 'frontend/vailua',
    'bootstrap' => FALSE,
    'debug' => TRUE,
    'errorHandler' => FALSE,
);

return
    array(
        'id' => $sso_domain,
        'name'=>'Vãi lúa',
//		'import' => array(
//			'ext.YiiMongoDbSuite.*',
//		),
        'components' => array(
            'cache'=>array(
                'class'=>'system.caching.CFileCache',
            ),
//			'mongodb' => array(
//				'class'            => 'EMongoDB',
//				'connectionString' => 'mongodb://localhost',
//				'dbName'           => 'myDatabaseName',
//				'fsyncFlag'        => true,
//				'safeFlag'         => true,
//				'useCursor'        => false
//			),
            'user' => array(
                'class' => 'WebUser',
                // enable cookie-based authentication
                'allowAutoLogin' => true,
            ),
            'session' => array(
                'savePath' => '/tmp/let',
                'cookieMode' => 'allow',
				'timeout' => 604800, // 7 days
                'cookieParams' => array(
                    'path' => '/',
                    'domain' => '.' . $sso_domain,
                    'httpOnly' => true,
                ),
            ),
//            'session' => array (
//                'sessionName' => 'SSOID',
//                'class' => 'CDbHttpSession',
//                'autoCreateSessionTable' => true,
//                'connectionID' => 'db',
//                'sessionTableName' => 'let_kit_account_sessions',                
//                'cookieMode' => 'allow',
//                'autoStart' => true,
//                'timeout' => 60*60*24,
//                'cookieParams' => array(
//                    'path' => '/',
//                    'domain' => '.' . $sso_domain,
//                    'httpOnly' => true,
//                ),
//            ),
            'urlManager' => array(
                'rules' => array(
                    'wall-<id:\d+>' => 'pin/frontend/wall/index',                    
                    '<username>' => 'pin/frontend/wall/index',                    
                ),
                'urlSuffix' => '.vl',
            ),
//            'log' => array(
//                'class' => 'CLogRouter',
//                'routes' => array(
//                    array(
//                        'class' => 'CFileLogRoute',
//                        'levels' => 'error',
//                    ),
//                // uncomment the following to show log messages on web pages
//                /*
//                    array(
//                    'class'=>'CWebLogRoute',
//                    ),
//                    */
//                ),
//            ),
        ),
        // application-level parameters that can be accessed
        // using Yii::app()->params['paramName']
        'params'=>array(
            'timeFormat' => array(
                'backend_default' => 'H:i:s d/m/Y',
                'frontend_default' => 'H:i:s d/m/Y',
            ),

            // Upload
            'uploadUrl' => 'http://data.let.vn', // http://phimle.vn
            'uploadDir' => 'uploads', // uploads
            'uploadPath' => '/home/let.vn/data', // dirname(__FILE__) . '/../..'
        ),
    );