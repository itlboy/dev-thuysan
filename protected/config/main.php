<?php
//die('hehehe'); 
date_default_timezone_set('Asia/Bangkok');
return
    array(
        'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
        'name'=>'LetKit',
        'sourceLanguage'=>'code',
        'language'=>'en',

        // preloading 'log' component
        'preload' => array('log'),

        // autoloading model and component classes
        'import'=>array(
            'application.vendors.helper.*',
            'application.models.*',
            'application.components.common.*',
            'application.modules.category.models.*',
            'application.modules.option.models.*',
            'application.modules.account.models.*',
        ),
        'modules'=>array(
        ),
        'components'=>array(
            'cache'=>array(
                'class'=>'system.caching.CFileCache',
            ),
            
        ),

        // application-level parameters that can be accessed
        // using Yii::app()->params['paramName']
        'params'=>array(
            'themeBackend' => 'default',
            'themeFrontend' => 'default',
            'timeFormat' => array(
                'backend_default' => 'H:i:s d/m/Y',
                'frontend_default' => 'H:i:s d/m/Y',
            ),

            // Account
            'account_level_image_dir' => 'http://assets.let.vn/levels/style_1',

            // Upload
            'uploadUrl' => 'http://210.245.90.239/data', // http://phimle.vn
            'uploadDir' => 'uploads', // uploads
            'uploadPath' => '/storage/data', // dirname(__FILE__) . '/../..',
            
            'fileTypes' => array('jpg','jpeg','gif','png'),
        ),
    );
