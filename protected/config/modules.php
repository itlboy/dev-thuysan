<?php

/**
 * File nay dinh nghia danh sach cac module duoc su dung trong he thong
 */
return array(
    'modules' => array(
        'cms',
        'account' => array(
            'returnUrl' => array('//cms/default'),
        ),
        'currency',
        'category',
        'option',
        'comment',
        'media',
        'article',
        'ads',
        'film',
        'pin',
        'classiads',
        'video',
        'poll',
        'location',
        'date',
        'product',
        'gallery',
        'cs',
        'contact',
        'game',
        'pin',
        'webservice',
        'library',
//        'property',
    ),
);
?>