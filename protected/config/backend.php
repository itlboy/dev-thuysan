<?php

$host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
if ($host == 'demo.letkit.vn' OR $host == 'thuysanvietnam.com.vn' OR $host == 'www.thuysanvietnam.com.vn' || $host == "baothuysan.dev.nhanh.cf"
) {
    $project = 'thuysanvietnam';
} elseif ($host == 'hanoihome.vn') {
    $project = 'hanoihome';
} elseif ($host == 'phim.let.vn' OR $host == 'data.let.vn') {
    $project = 'letphim';
    $sso_domain = 'let.vn';
} elseif ($host == 'id.let.vn') {
    $project = 'id';
    $sso_domain = 'let.vn';
} elseif ($host == 'phimle.vn') {
    $project = 'letphimwap';
    $sso_domain = 'let.vn';
} elseif ($host == 'truyen.let.vn') {
    $project = 'lettruyen';
    $sso_domain = 'let.vn';
} elseif ($host == 'worldofbeolove.com' OR $host == 'www.worldofbeolove.com') {
    $project = 'bovn';
} else {
    $project = 'bovn';
}

$list = array(
    dirname(__FILE__) . '/main.php',
    dirname(__FILE__) . '/local.php',
    dirname(__FILE__) . '/modules.php',
    dirname(__FILE__) . '/mail.php',
);

$config = array();
foreach ($list as $key => $value) {
    if (file_exists($value))
        $config = CMap::mergeArray($config, require($value));
}

// Config rieng cho tung Project
$file = dirname(__FILE__) . '/projects/' . $project . '.php';
if (file_exists($file))
    $config = CMap::mergeArray($config, require($file));

// Quy dinh rieng cho localhost
if ($host == 'localhost') {
    unset($config['components']['session']);
    $includeConfig = require(dirname(__FILE__) . '/projects/localhost.php');
    $config = CMap::mergeArray($config, $includeConfig);
}

return CMap::mergeArray(
                $config, array(
            'defaultController' => 'cms/backend/default',
            'theme' => 'backend',
            // autoloading model and component classes
            'import' => array(
                'application.components.backend.*',
//        'application.modules.srbac.controllers.SBaseController',
                'application.modules.languages.models.*',
            ),
            'modules' => array(
                // uncomment the following to enable the Gii tool
                'gii' => array(
                    'class' => 'system.gii.GiiModule',
                    'password' => 'handsom3000',
//			'generatorPaths'=>array(
//				'ext.YiiMongoDbSuite.gii'
//			),
                    // If removed, Gii defaults to localhost only. Edit carefully to taste.
                    'ipFilters' => array('127.0.0.1', '::1'),
                ),
                'crawler',
                'mail',
//        'srbac' => array(
//            'userclass'=>'ForumUcenterMembers', //default: User
//            'userid'=>'uid', //default: userid
//            'username'=>'username', //default:username
//            'delimeter'=>'@', //default:-
//            'debug'=>true, //default :false
//            'pageSize'=>10, // default : 15
//            'superUser' =>'Authority', //default: Authorizer
//            'css'=>'srbac.css', //default: srbac.css
//            'layout'=> 'webroot.themes.backend.views.layouts.main', //default: application.views.layouts.main, //layouts/main
//            //must be an existing alias
//            'notAuthorizedView'=> 'srbac.views.authitem.unauthorized', // default:
//            //srbac.views.authitem.unauthorized, must be an existing alias
//            'alwaysAllowed'=>array( //default: array()
//                'SiteLogin','SiteLogout','SiteIndex','SiteAdmin',
//                'SiteError', 'SiteContact'
//            ),
//            'userActions'=>array('Show','View','List'), //default: array()
//            'listBoxNumberOfLines' => 15, //default : 10 'imagesPath' => 'srbac.images', // default: srbac.images 'imagesPack'=>'noia', //default: noia 'iconText'=>true, // default : false 'header'=>'srbac.views.authitem.header', //default : srbac.views.authitem.header,
//            //must be an existing alias 'footer'=>'srbac.views.authitem.footer', //default: srbac.views.authitem.footer,
//            //must be an existing alias 'showHeader'=>true, // default: false 'showFooter'=>true, // default: false
//            'alwaysAllowedPath'=>'srbac.components', // default: srbac.components
//            // must be an existing alias
//        ),
            ),
            // application components
            'components' => array(
                'user' => array(
                    // enable cookie-based authentication
                    'allowAutoLogin' => true,
                    'loginUrl' => "account/login.html",
                ),
//        'authManager'=>array(
//            // Path to SDbAuthManager in srbac module if you want to use case insensitive
//            //access checking (or CDbAuthManager for case sensitive access checking)
//            'class'=>'application.modules.srbac.components.SDbAuthManager',
//            // The database component used
//            'connectionID'=>'db',
//            // The itemTable name (default:authitem)
//            'itemTable'=>'let_kit_permission_items',
//            // The assignmentTable name (default:authassignment)
//            'assignmentTable'=>'let_kit_permission_assignments',
//            // The itemChildTable name (default:authitemchild)
//            'itemChildTable'=>'let_kit_permission_itemchildren',
//        ),
                'urlManager' => array(
                    'urlFormat' => 'path',
                    'rules' => array(
                        'gii' => 'gii',
                        'gii/<controller:\w+>' => 'gii/<controller>',
                        'gii/<controller:\w+>/<action:\w+>' => 'gii/<controller>/<action>',
//                'srbac/<controller:\w+>/<action:\w+>'=>'srbac/<controller>/<action>',
//                'srbac/<controller:\w+>'=>'srbac/<controller>',
//                'srbac'=>'srbac/default',
                        '<module:\w+>' => '<module>/backend/default',
                        '<module:\w+>/<controller:\w+>' => '<module>/backend/<controller>',
                        '<module:\w+>/<controller:\w+>/<action:\w+>/*' => '<module>/backend/<controller>/<action>',
                    ),
                    'showScriptName' => true, // An index
                    'caseSensitive' => false, // Phan biet hoa - thuong
                    'urlSuffix' => '.html',
                ),
                'image' => array(
                    'class' => 'application.extensions.image.CImageComponent',
                    // GD or ImageMagick
                    'driver' => 'GD',
                    // ImageMagick setup path
                    'params' => array('directory' => '/opt/local/bin'),
                ),
//		'log'=>array(
//			'class'=>'CLogRouter',
//			'routes'=>array(
//				array(
////					'class'=>'CFileLogRoute',
//                    'class'=>'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
//					'levels'=>'error, warning',
////                    'ipFilters'=>array('127.0.0.1','192.168.1.215'),
//				),
//				// uncomment the following to show log messages on web pages
//				/*
//				array(
//					'class'=>'CWebLogRoute',
//				),
//				*/
//			),
//		),
            ),
            'params' => array(
                'pageSize' => 50,
            ),
        ));
