<?php

$sso_domain = '';
$host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
if ($host == 'demo.letkit.vn' OR $host == 'thuysanvietnam.com.vn' OR $host == 'www.thuysanvietnam.com.vn' || $host == "baothuysan2.dev.nhanh.cf") {
    $project = 'thuysanvietnam';
    if ($host == "baothuysan2.dev.nhanh.cf") {
        define('DEV_MODE', true);
    }
} elseif ($host == 'hanoihome.vn') {
    $project = 'hanoihome';
} elseif ($host == 'api.let.vn') {
    $project = 'api';
    $sso_domain = 'let.vn';
} elseif ($host == 'phim.let.vn' OR $host == 'data.let.vn') {
    $project = 'letphim';
    $sso_domain = 'let.vn';
} elseif ($host == 'id.let.vn') {
    $project = 'idNew';
    $sso_domain = 'let.vn';
} elseif ($host == 'search.let.vn') {
    $project = 'letsearch';
    $sso_domain = 'let.vn';
} elseif ($host == 'cuoi.let.vn') {
    $project = 'letcuoi';
    $sso_domain = 'let.vn';
} elseif ($host == 'portal.let.vn') {
    $project = 'adminportal';
    $sso_domain = 'let.vn';
} elseif ($host == 'vailua.vn') {
    $project = 'vailua';
} elseif (preg_match("/.vailua.vn/i", $host)) {
    $project = 'vailua_user';
} elseif ($host == 'bovn.demo.let.vn' OR $host == 'worldofbeolove.com' OR $host == 'www.worldofbeolove.com') {
    $project = 'bovn';
    $sso_domain = 'let.vn';
} elseif ($host == 'kinggame.info') {
    $project = 'mobilegame';
    $sso_domain = 'let.vn';
} elseif ($host == 'nghichtien.com') {
    $project = 'gamenghichtien';
} else {
//    $project = 'adminportal';
//    $project = 'letcuoi';
//    $project = 'mobilegame';
//    $project = 'letphim';
//    $project = 'idNew';
    $project = 'gamenghichtien';
}
$list = array(
    dirname(__FILE__) . '/main.php',
    dirname(__FILE__) . '/local.php',
    dirname(__FILE__) . '/modules.php',
    dirname(__FILE__) . '/mail.php',
    dirname(__FILE__) . '/checkIp.php',
);

$config = array();
foreach ($list as $key => $value) {
    if (file_exists($value))
        $config = CMap::mergeArray($config, require($value));
}

// Config rieng cho tung Project
$file = dirname(__FILE__) . '/projects/' . $project . '.php';
if (file_exists($file))
    $config = CMap::mergeArray($config, require($file));

// Quy dinh loi
$includeConfig = array(
    'components' => array(
        'errorHandler' => array(
            'errorAction' => 'cms/frontend/error',
        ),
    ),
);
if (!isset($projectConfig['errorHandler']) OR $projectConfig['errorHandler'] == FALSE)
    $includeConfig = array();
$config = CMap::mergeArray($includeConfig, $config);
// END Quy dinh bootstrap
// Quy dinh bootstrap
$includeConfig = array(
    'components' => array(
        'bootstrap' => array(
            'class' => 'ext.bootstrap.components.Bootstrap', // assuming you extracted bootstrap under extensions
        ),
    ),
);
if (!isset($projectConfig['bootstrap']) OR $projectConfig['bootstrap'] == FALSE)
    $includeConfig = array();
$config = CMap::mergeArray($includeConfig, $config);
// END Quy dinh bootstrap
// Quy dinh yii-debug-toolbar
$includeConfig = array(
    'components' => array(
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
//                    'ipFilters'=>array('127.0.0.1','192.168.1.215','118.71.26.20'),
                ),
            ),
        ),
    ),
);
if (!isset($projectConfig['debug']) OR $projectConfig['debug'] == FALSE)
    $includeConfig = array();
$config = CMap::mergeArray($includeConfig, $config);
// END Quy dinh yii-debug-toolbar
// Quy dinh rieng cho localhost
if ($host == 'localhost') {
    unset($config['components']['session']);
    $includeConfig = require(dirname(__FILE__) . '/projects/localhost.php');
    $config = CMap::mergeArray($config, $includeConfig);
}

return CMap::mergeArray(
                array(
            'defaultController' => 'cms/frontend/default',
            'theme' => $projectConfig['theme'],
            // autoloading model and component classes
            'import' => array(
                'application.vendors.helper.*',
                'application.components.frontend.*',
                'application.components.cms.*',
                'application.modules.cms.models.*',
            ),
            'preload' => array('bootstrap'),
            // application components
            'components' => array(
                'urlManager' => array(
                    'urlFormat' => 'path',
                    'rules' => array(
                        '<title>-c<id:\d+>-<page:\d+>' => 'category/frontend/default', // Category
                        '<title>-c<id:\d+>' => 'category/frontend/default', // Category
                        '<title>-a<id:\d+>' => 'article/frontend/details', //
                        'tin-tuc-<page:\d+>' => 'article/frontend/list',
                        'tin-tuc' => 'article/frontend/list',
                        'tim-kiem/<keyword>/trang-<page:\d>/' => 'article/frontend/tag',
                        'tim-kiem/<keyword>/' => 'article/frontend/tag',
                        // Film
                        '<title>-film-<id:\d+>-<episode:\d+>' => 'film/frontend/details',
                        'tim-kiem-phim' => 'film/frontend/search',
                        'tag' => 'film/frontend/search/index',
                        'tag/<keyword>' => 'film/frontend/search/index/keyword/<keyword>',
                        'phim-bo' => 'film/frontend/search/index/length_1/1',
                        'phim-le' => 'film/frontend/search/index/length_0/1',
                        'phim-hot' => 'film/frontend/search/index/hot/1',
                        'phim-chieu-rap' => 'film/frontend/search/index/theater/1',
                        'phim-de-cu' => 'film/frontend/search/index/promotion/1',
                        '<id:\d+>/<title>' => 'film/frontend/search', // /4/phim-Hai-huoc.let
                        'xem-phim/<title>/f<id:\d+>' => 'film/frontend/redirect', // xem-phim/Lan-Dau-Tien/f2420.let
                        'xem-phim/<title>/<title2>/f<id:\d+>' => 'film/frontend/redirect', // xem-phim/Lan-Dau-Tien/Lan-Dau-Tien/f2420.let
                        'xem-phim/<title>/<title2>/<title3>/f<id:\d+>' => 'film/frontend/redirect', // xem-phim/Lan-Dau-Tien/Lan-Dau-Tien/Lan-Dau-Tien/f2420.let
                        'xem-phim/<title>/<title2>/<title3>/<title4>/f<id:\d+>' => 'film/frontend/redirect', // xem-phim/Lan-Dau-Tien/Lan-Dau-Tien/Lan-Dau-Tien/f2420.let
                        'xem-phim/<title>/<title2>/<title3>/<title4>/<title5>/f<id:\d+>' => 'film/frontend/redirect', // xem-phim/Lan-Dau-Tien/Lan-Dau-Tien/Lan-Dau-Tien/f2420.let
                        //xem-phim/Tom--va-Jerry---Flirty-Birdy/Tom-and-Jerry---Flirty-Birdy/e679-0.let
                        'xem-phim/<title>/e<id:\d+>-<ep:\d+>' => 'film/frontend/redirect',
                        'xem-phim/<title>/<title2>/e<id:\d+>-<ep:\d+>' => 'film/frontend/redirect',
                        'xem-phim/<title>/<title2>/<title3>/e<id:\d+>-<ep:\d+>' => 'film/frontend/redirect',
                        'xem-phim/<title>/<title2>/<title3>/<title4>/e<id:\d+>-<ep:\d+>' => 'film/frontend/redirect',
                        'xem-phim/<title>/<title2>/<title3>/<title4>/<title5>/e<id:\d+>-<ep:\d+>' => 'film/frontend/redirect',
                        //anime/Lan-Dau-Tien/f2420.let
                        'anime/<title>/f<id:\d+>' => 'film/frontend/redirect',
                        'anime/<title>/<title2>/f<id:\d+>' => 'film/frontend/redirect',
                        'anime/<title>/<title2>/<title3>/f<id:\d+>' => 'film/frontend/redirect',
                        '<title>-<module:\w+>-<id:\d+>' => '<module>/frontend/details',
//                '<title>-property<id:\d+>' => 'property/frontend/details',
                        //'property-search' => 'property/frontend/search',
                        // rewrite cho cms
                        //Kien add
                        '<module:\w+>/<controller:\w+>' => '<module>/frontend/<controller>/index',
                        '<module:\w+>/<controller:\w+>/<action:\w+>/*' => '<module>/frontend/<controller>/<action>',
                    ),
                    'showScriptName' => false,
                    'caseSensitive' => false,
                    'urlSuffix' => '.let',
                ),
                'image' => array(
                    'class' => 'application.extensions.image.CImageComponent',
                    // GD or ImageMagick
                    'driver' => 'GD',
                    // ImageMagick setup path
                    'params' => array('directory' => '/opt/local/bin'),
                ),
//        'user' => array(
//            // enable cookie-based authentication
//            'allowAutoLogin' => true,
//        ),
//        'log' => array(
//            'class' => 'CLogRouter',
//            'routes' => array(
//                array(
//                    'class' => 'CFileLogRoute',
//                    'levels' => 'error',
//                ),
//            // uncomment the following to show log messages on web pages
//            /*
//                array(
//                'class'=>'CWebLogRoute',
//                ),
//                */
//            ),
//        ),
            ),
            'modules' => array(
            // danh sach cac module chi load cho phan web
            ),
                ), $config);
