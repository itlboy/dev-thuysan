elRTE.prototype.options.panels.web2pyPanel = [
    'bold', 'italic', 'underline', 'forecolor', 'formatblock', 'justifyleft',
    'justifyright', 'justifycenter', 'justifyfull', 'outdent', 'indent',
    'insertorderedlist', 'insertunorderedlist', 'link', 'image'
];
elRTE.prototype.options.toolbars.web2pyToolbar = ['web2pyPanel', 'elfinder','fullscreen'];