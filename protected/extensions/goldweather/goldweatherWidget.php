<?php

class goldweather extends CWidget {

	/**
	 * Publishes the required assets
	 */
	public function init() {
		parent::init();
		$this->publishAssets();
	}

	public function run() {

	}

	/**
	 * Publises and registers the required CSS and Javascript
	 * @throws CHttpException if the assets folder was not found
	 */
	public function publishAssets() {
		$assets = dirname(__FILE__) . '/assets';
		$baseUrl = Yii::app()->assetManager->publish($assets);
		if (is_dir($assets)) {
			Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/ajax.js', CClientScript::POS_END);
			Yii::app()->clientScript->registerCssFile($baseUrl . '/xuploads.css');
		} else {
			throw new CHttpException(500, 'XUpload - Error: Couldn\'t find assets to publish.');
		}
	}
}