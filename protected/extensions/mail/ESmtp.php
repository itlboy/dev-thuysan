<?php
Yii::import('application.extensions.mail.PHPMailer');
// require MOD extension=php_openssl.dll

class ESmtp extends CApplicationComponent
{
    /* USE:
    $mailTo = array(
        'admin@trumcode.com'=>'Ba Duy'',
        'support@trumcode.com'=>'Ba Duy');
    $mailExtra = array(
        'cc-email-a@yahoo.com'=>'CC Name A',
        'cc-email-a@yahoo.com'=>'CC Name A');

    $mailSubject = "Test mail script from trumcode.com";
    $mailHtml = "<b>Mail này được gửi bằng phpmailer class. - <a href='http://trumcode.com'>trumcode.com</a></b>"; //HTML Body
    $mailText = "Kiểm tra hệ thống mail với phpmailer class. - trumcode.com"; //Text Body

    $mail = new ESmtp;
    $mail->setServer('smtp.gmail.com', 465, 'admin@trumcode.com', '');
    $mail->setSender('admin@trumcode.com', 'Mai Bá Duy');
    $mail->send($mailSubject, $mailHtml, $mailText, $mailTo, null);
    */
    private $mailHost;
    private $mailPort;
    private $mailUser;
    private $mailPass;

    public $mailFrom; // Reply to this email
    public $mailFromName;

    public function setServer($host, $port, $user, $pass)
    {
        $this->mailHost = $host;
        $this->mailPort = $port;
        $this->mailUser = $user;
        $this->mailPass = $pass;
    }

    public function setSender($from, $formName)
    {
        $this->mailFrom = $from;
        $this->mailFromName = $formName;
    }

    public function send($mailSubject, $mailHtml, $mailText, $mailTo, $mailExtra=null, $mailExtraType='bcc')
    {

        $mail = new PHPMailer;
        $mail->CharSet = 'utf-8';
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'ssl';
        $mail->Host = $this->mailHost;
        $mail->Port = $this->mailPort;
        $mail->Username = $this->mailUser;
        $mail->Password = $this->mailPass;
        $mail->From = $this->mailFrom;
        $mail->FromName = $this->mailFromName;

        foreach ($mailTo as $address => $name) { //$mail->AddAddress($mailTo,$mailToName);
            $mail->AddAddress($address , $name);
        }

        if (!empty($mailExtra) && is_array($mailExtra)) {
            if ($mailExtraType=='bcc') {
                foreach($mailExtra as $address => $name) $mail->AddBCC($address , $name);
            } else {
                foreach($mailExtra as $address => $name) $mail->AddCC($address , $name);
            }
        }

        $mail->AddReplyTo($this->mailFrom,$this->mailFromName);
        //$mail->AddStringAttachment('path/to/file', "YourPhoto.jpg");

        $mail->Subject = $mailSubject;
        $mail->Body = $mailHtml;
        $mail->AltBody = $mailText;
        $mail->SMTPDebug = 0;

        $mail->WordWrap = 50; // set word wrap
        $mail->IsHTML(true); // send as HTML


        if($mail->Send()) {
            return true;
        } else {
//            echo $mail->Password.' -- '.$mail->Username;
//            echo "Mailer Error: " . $mail->ErrorInfo;die();
            $msg = $mail->Username.'|'. $mail->Password. " has send mail with error: " . $mail->ErrorInfo;
            Yii::log($msg, 'error', 'Mail');
            return false;
        }

        // Clear all addresses and attachments for next loop
        //$mail->ClearAddresses();
        //$mail->ClearAttachments();
    }

    public function sendAuto($email, $mailSubject, $mailHtml, $mailText='', $service='default') {
        Yii::import('module.mail.models.MailServer');
        Yii::import('module.mail.models.MailSender');

        $mailTo = array();
        if (is_array($email) AND !empty($email)) {
            foreach ($email as $address => $name)
                $mailTo[$address] = $name;
        } else {
            $mailTo[$email] = $email;
        }

        $mail = new ESmtp;
        $modelServer = MailServer::model()->getServerAuto($service);
        $modelSender = MailSender::model()->getSenderDefault($service);

        $mail->setServer($modelServer['smtphost'], $modelServer['smtpport'], $modelServer['smtpuser'], $modelServer['smtppass']);
        $mail->setSender($modelSender['fromemail'], $modelSender['fromname']);

        return $mail->send($mailSubject, $mailHtml, $mailText, $mailTo);
    }
}

