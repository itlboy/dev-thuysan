<body class='contrast-grass-green sign-up contrast-background'>
    <div id='wrapper'>
        <div class='application'>
            <div class='application-content'>
                <a href="<?php echo $this->createUrl('//account/frontend/id/login'); ?>"><div class='icon-heart'></div>
                    <span>LetID</span>
                </a>
            </div>
        </div>
        <div class='controls'>
            <div class='caret'></div>
            <div class='form-wrapper'>
                <?php
                /**
                 * @var HOAuthAction $this
                 * @var HUserInfoForm $form
                 */
                echo $form->form;
                //echo $form->model->scenario;
                ?>
            </div>
        </div>
        <div class='login-action text-center'>
            <a href="forgot_password.php"><i class='icon-lock'></i>
                Quên mật khẩu?
            </a>
        </div>
    </div>
</body>
<style>
    #huserinfoform .row {margin-left:0px; margin-top:8px}
    #huserinfoform .row label {display:block; font-weight:bold}

    .row.field_confirm > label {display:none !important}
    #HUserInfoForm_confirm {margin-left:-5px}
    #HUserInfoForm_confirm label {display:inline-block !important; margin-right:10px !important}

    <?php if ($form->model->scenario != 'update') : ?>
    .row.field_confirm {display:none}
    <?php endif; ?>

    <?php if ($form->model->scenario == 'update') : ?>
    .row.field_password {display:none}
    <?php endif; ?>
</style>

<script>
    jQuery(function(){
        <?php if ($form->model->scenario == 'update') : ?>
        jQuery('input.btnAccSubmit').val('Xác nhận');
        <?php endif; ?>

        if (jQuery('.field_password .errorMessage').length > 0) {
            jQuery('.field_password').show();
        }
        jQuery('input.accConfirm').on('click', function(){                        
            if (jQuery(this).val() === '1') {
                jQuery('.field_password').show();
            } else {
                jQuery('.field_password').hide();
            }
        });
    });
</script>

