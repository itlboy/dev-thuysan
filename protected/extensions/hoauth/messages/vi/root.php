<?php
return array (
	'Email' => 'Email',
	'Nickname' => 'Tên truy cập',
	'Password' => 'Mật khẩu',
	'Sorry, but password is incorrect' => 'Mật khẩu không chính xác',
	'Submit' => 'Đăng ký',
	'This {attribute} is taken by another user. If this is your account, enter password in field below or change {attribute} and leave password blank.' => '{attribute} này đã được sử dụng bởi người khác. Nếu đây là tài khoản của bạn, vui lòng nhập mật khẩu xác nhận bên dưới. Hoặc đỗi Email khác và để trống trường Mật khẩu.',
	//'Please specify your nickname and email to end with registration.' => 'Vui lòng nhập Tên truy cập và Mật khẩu để đăng ký.',
	'Please specify your nickname and email to end with registration.' => 'Nhập đầy đủ thông tin để hoàn tất đăng ký.',
	'Please specify your nickname to end with registration.' => 'Vui lòng nhập Tên truy cập để đăng ký.',
	'Please specify your email to end with registration.' => 'Vui lòng nhập Email để đăng ký.',
	
	'Sorry, but your account' => 'Xin lỗi, nhưng tài khoản của bạn',
	'must be activated! Check your email for details!' => 'noch nicht Aktiviert! Überprüfen Sie Ihre E-Mail für Details!',
	'is banned!' => 'bị cấm!',
	'Return to main page' => 'Trở về trang chủ',
	'Return to login page' => 'Trở về trang đăng nhập',
);
