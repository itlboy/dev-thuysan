<?php

class TinyMCE extends CWidget {

    public $element = 'textarea.tinymce';
    public $language = 'vi';
    public $width = '100%';
    public $height = '370px';
    public $plugins = 'youtube,autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist,-autosave'; //contextmenu
    public $buttons1 = 'save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect';
    public $buttons2 = 'cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,youtube,cleanup,help,|,insertdate,inserttime,preview,|,forecolor,backcolor';
    public $buttons3 = 'tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen,code';
    public $buttons4 = 'insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak';

    public function run() {

        if ($this->language == NULL)
            $this->language = Yii::app()->language;

        $cs = Yii::app()->clientScript;
        $baseUrl = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . DIRECTORY_SEPARATOR);
        $fileUrl = $baseUrl . '/elfinder/php/connector.php';

        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');

        $cs->registerScriptFile($baseUrl . '/tinymce/jquery.tinymce.js');
        $cs->registerScriptFile($baseUrl . '/tinymce/tiny_mce.js');
        $cs->registerScriptFile($baseUrl . '/tinymce/function.js');

        $cs->registerCssFile($baseUrl . '/elfinder/css/elfinder.min.css');
        $cs->registerScriptFile($baseUrl . '/elfinder/js/elfinder.min.js');

        $cs->registerScript('TinyMCE' . $this->element, "
            runTinyMCE(
                '" . $baseUrl . "',
                '" . $fileUrl . "',
                '" . $this->element . "',
                '" . $this->language . "',
                '" . $this->height . "',
                '" . $this->width . "',
                '" . $this->plugins . "',
                '" . $this->buttons1 . "',
                '" . $this->buttons2 . "',
                '" . $this->buttons3 . "',
                '" . $this->buttons4 . "'
            );
        ");
    }
}
