
function runTinyMCE($baseUrl, $fileUrl, $element, $language, $height, $width, $plugins, $buttonsLine1, $buttonsLine2, $buttonsLine3, $buttonsLine4) {
    var sTinyMCERegistry = {
        baseURI: $baseUrl + '/tinymce/',
        registerResources:function() {
            tinymce.baseURL = new tinymce.util.URI(tinymce.documentBaseURL).toAbsolute(this.baseURI);
            tinymce.baseURI = new tinymce.util.URI(tinymce.baseURL);
        }
    };
    sTinyMCERegistry.registerResources();
    
    jQuery($element).tinymce({
        setup : function(ed) {
            ed.onKeyUp.add(function(ed, e) {
                jQuery('.contentTinyMCE').val(tinyMCE.activeEditor.getContent({format : 'raw'}));
                tinyMCE.triggerSave();
            });
        },

        // General options
        mode : 'textareas',
        theme : 'advanced',
        //skin : 'o2k7',
        language : $language,
        height : $height,
        width : $width,
        plugins : $plugins,

        // Extra options
        //theme_advanced_path : false,
        force_p_newlines : true,
        force_br_newlines : false,
        auto_cleanup_word : false,
        relative_urls : true,
        convert_urls : false,
        remove_script_host : true,
        verify_html : false,
        forced_root_block : false,

        // Theme options
        theme_advanced_buttons1 : $buttonsLine1,
        theme_advanced_buttons2 : $buttonsLine2,
        theme_advanced_buttons3 : $buttonsLine3,
        theme_advanced_buttons4 : $buttonsLine4,
        remove_linebreaks : false,
        extended_valid_elements : 'textarea[cols|rows|disabled|name|readonly|class]',
        theme_advanced_toolbar_location : 'top',
        theme_advanced_toolbar_align : 'left',
        theme_advanced_statusbar_location : 'bottom',
        theme_advanced_resizing : true,

        // Example content CSS (should be your site CSS)
        //content_css : '$baseUrl/css/content.css',

        // Replace values for the template plugin
        template_replace_values : {
            username : 'Some User',
            staffid : '991234'
        },

        // Custome
        accessibility_warnings : false,
        file_browser_callback : function (field_name, url, type, win) {
            aFieldName = field_name,
            aWin = win;
            opts = {
                places: 'Root',
                url : $fileUrl,
                lang : $language,
                selectMultiple : false,
                getFileCallback: function(url) { // editor callback
                    aWin.document.forms[0].elements[aFieldName].value = url;
                    if (aFieldName=='src' && aWin.ImageDialog.showPreviewImage)
                        aWin.ImageDialog.showPreviewImage(url);
                    jQuery('#finder').dialog('close');
                }
            };

            if(jQuery('#finder').length == 0) {
                jQuery('body').append(jQuery('<div/>').attr('id', 'finder'));
            }

            jQuery('#finder').elfinder(opts).dialog({
                title : 'File manager',
                height : jQuery(window).height()-20, //465
                width : jQuery(window).width()-20,//900
                modal : true,
                zIndex : 300029
            });
        }
    });
}
