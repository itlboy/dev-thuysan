tinyMCEPopup.requireLangPack();

function getVideoId(url){
    if(url.indexOf('?') != -1 ) {
        var query = decodeURI(url).split('?')[1];
        var params = query.split('&');
        for(var i=0,l = params.length;i<l;i++)
            if(params[i].indexOf('v=') === 0)
                return params[i].replace('v=','');
    } else if (url.indexOf('youtu.be') != -1) {
        return decodeURI(url).split('youtu.be/')[1];
    }
    return null;
}


function youtubeParser(url){
    /*
    support:
    http://www.youtube.com/watch?v=6o-nmK9WRGE&feature=player_embedded
    http://www.youtube.com/watch?v=0zM3nApSvMg&feature=feedrec_grec_index
    http://www.youtube.com/user/IngridMichaelsonVEVO#p/a/u/1/QdK8U-VIH_o
    http://www.youtube.com/v/0zM3nApSvMg?fs=1&hl=en_US&rel=0
    http://www.youtube.com/watch?v=0zM3nApSvMg#t=0m10s
    http://www.youtube.com/embed/0zM3nApSvMg?rel=0
    http://www.youtube.com/watch?v=0zM3nApSvMg
    http://youtu.be/0zM3nApSvMg
    */
    var b = '';
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    var match = url.match(regExp);
    if (match&&match[7].length==11){
        b = match[7];        
    }
    return b;
} 

var YoutubeDialog = {
    init: function () {
        var f = document.forms[0];

        // Get the selected contents as text and place it in the input
        f.youtubeURL.value = tinyMCEPopup.editor.selection.getContent({format: 'text'});
    },

    insert: function () {
        // Insert the contents from the input into the document
        var url = document.forms[0].youtubeURL.value;
        if (url === null) {tinyMCEPopup.close();return;}

        var code, regexRes;
//        regexRes = url.match("[\\?&]v=([^&#]*)");
//        code = (regexRes === null) ? url : regexRes[1];

        code = getVideoId(url);
        if (code === "") {tinyMCEPopup.close();return;}

        tinyMCEPopup.editor.execCommand('mceInsertContent', false, '<object type=\"application\/x-shockwave-flash\" data=\"http:\/\/www.youtube.com\/v\/'+code+'\" width=\"425\" height=\"350\"><param name=\"movie\" value=\"http:\/\/www.youtube.com\/v\/'+code+'\" \/><param name=\"wmode\" value=\"transparent\" \/><\/object>');
        tinyMCEPopup.close();
    }
};

tinyMCEPopup.onInit.add(YoutubeDialog.init, YoutubeDialog);
