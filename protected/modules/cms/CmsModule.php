<?php

class CmsModule extends CWebModule
{
    public $category = TRUE; // Xác định Module có category hay không
    public $menuList = array(
        array(
            'name' => 'Tạo Config',
            'url'  => '/cms/config/create',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Tạo Config',
            'type' => 'direct'
        ),
        array(
            'name' => 'Quản lý Config',
            'url'  => '/cms/config/index',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Quản lý Config',
            'type' => 'direct'
        ),
    );

    public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'cms.models.*',
			'cms.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
