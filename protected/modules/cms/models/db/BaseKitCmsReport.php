<?php

/**
 * This is the model class for table "{{kit_cms_report}}".
 *
 * The followings are the available columns in table '{{kit_cms_report}}':
 * @property string $id
 * @property string $date
 * @property integer $module_account
 * @property integer $module_comment
 * @property integer $module_contact
 * @property integer $module_article
 * @property integer $module_film
 * @property integer $module_product
 * @property integer $module_date
 * @property integer $module_classiads
 * @property integer $currency_money
 * @property integer $currency_gold_in
 * @property integer $currency_gold_out
 */
class BaseKitCmsReport extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitCmsReport the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_cms_report}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('module_account, module_comment, module_contact, module_article, module_film, module_product, module_date, module_classiads, currency_money, currency_gold_in, currency_gold_out', 'numerical', 'integerOnly'=>true),
			array('date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, date, module_account, module_comment, module_contact, module_article, module_film, module_product, module_date, module_classiads, currency_money, currency_gold_in, currency_gold_out', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('module_account',$this->module_account);
		$criteria->compare('module_comment',$this->module_comment);
		$criteria->compare('module_contact',$this->module_contact);
		$criteria->compare('module_article',$this->module_article);
		$criteria->compare('module_film',$this->module_film);
		$criteria->compare('module_product',$this->module_product);
		$criteria->compare('module_date',$this->module_date);
		$criteria->compare('module_classiads',$this->module_classiads);
		$criteria->compare('currency_money',$this->currency_money);
		$criteria->compare('currency_gold_in',$this->currency_gold_in);
		$criteria->compare('currency_gold_out',$this->currency_gold_out);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
		));
	}
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitCmsReport::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitCmsReport::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitCmsReport::getLastest_'));
        Yii::app()->cache->delete(md5('KitCmsReport::getPromotion_'));
    }

}
