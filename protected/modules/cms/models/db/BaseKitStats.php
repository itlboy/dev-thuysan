<?php

/**
 * This is the model class for table "{{kit_stats}}".
 *
 * The followings are the available columns in table '{{kit_stats}}':
 * @property string $id
 * @property string $module
 * @property integer $item_id
 * @property string $thistime
 * @property integer $view_total
 * @property integer $view_intoday
 * @property integer $view_inweek
 * @property integer $view_inmonth
 * @property integer $view_inyear
 * @property integer $vote_total
 * @property integer $vote_intoday
 * @property integer $vote_inweek
 * @property integer $vote_inmonth
 * @property integer $vote_inyear
 */
class BaseKitStats extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitStats the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_stats}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item_id, view_total, view_intoday, view_inweek, view_inmonth, view_inyear, vote_total, vote_intoday, vote_inweek, vote_inmonth, vote_inyear', 'numerical', 'integerOnly'=>true),
			array('module', 'length', 'max'=>20),
			array('thistime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, module, item_id, thistime, view_total, view_intoday, view_inweek, view_inmonth, view_inyear, vote_total, vote_intoday, vote_inweek, vote_inmonth, vote_inyear', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('module',$this->module,true);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('thistime',$this->thistime,true);
		$criteria->compare('view_total',$this->view_total);
		$criteria->compare('view_intoday',$this->view_intoday);
		$criteria->compare('view_inweek',$this->view_inweek);
		$criteria->compare('view_inmonth',$this->view_inmonth);
		$criteria->compare('view_inyear',$this->view_inyear);
		$criteria->compare('vote_total',$this->vote_total);
		$criteria->compare('vote_intoday',$this->vote_intoday);
		$criteria->compare('vote_inweek',$this->vote_inweek);
		$criteria->compare('vote_inmonth',$this->vote_inmonth);
		$criteria->compare('vote_inyear',$this->vote_inyear);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
		));
	}
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {        
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitStats::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitStats::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitStats::getLastest_'));
        Yii::app()->cache->delete(md5('KitStats::getPromotion_'));
    }

}
