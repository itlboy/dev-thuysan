<?php

Yii::import('application.modules.cms.models.db.BaseKitCmsReport');
class KitCmsReport extends BaseKitCmsReport
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Lay du lieu theo ngay tu $from -> $to
     * @param type $to
     * @param type $from
     * @return type 
     */
    public static function getReport($from = '', $to = ''){
        if(empty($to))
            $to = KitCmsConfig::getLastDate();
        if(empty($from))
            $from = date('Y-m-d',strtotime('-5 day',strtotime($to)));

        $command = Yii::app()->db->createCommand();
        $command = $command->select(Common::getFieldInTable(self::model()->getAttributes()))
            ->from('let_kit_cms_report')
            ->where("`date` BETWEEN '".$from." 00:00:00' AND '".$to." 23:59:59'")
            ->order('date ASC');
        $result = $command->queryAll();
        return $result;
    }

}