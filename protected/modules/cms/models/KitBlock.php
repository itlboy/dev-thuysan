<?php

Yii::import('application.modules.cms.models.db.BaseKitBlock');
class KitBlock extends BaseKitBlock
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function getClassPath() {
        $code = explode("_", $this->code);
        $widgetName = $code[0];
        $widgetClassName = ucfirst($widgetName)."Block";
        
        return "application.widgets.blocks.{$widgetName}.{$widgetClassName}";
    }
    
    public function getActivatedView() {
        $code = explode("_", $this->code);
        $viewName = $code[1];
        return $viewName;
    }
}