<?php
Yii::import('application.modules.cms.models.db.BaseKitCmsConfig');
class KitCmsConfig extends BaseKitCmsConfig{
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    /**
     * @static
     * @param $key
     * @return array|bool|null
     */
    public static function getValue($key, $key2 = NULL){
        if($key == NULL) return FALSE;

        // lay danh sach config
        $cache_name = md5('cms_config_'.$key);
//        echo 'cms_config_'.$key;
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $data = self::model()->findAll();
            if(empty($data)) return FALSE;
            $data = self::treatment($data);

            // chuyen ve dang mang array('name' => 'value')
            $configs = array();
            if(!empty($data)){
                foreach($data as $item){
                    $configs[$item['name']] = $item['value'];
                }
            }
            Yii::app()->cache->set($cache_name, $configs,60*60); // Set cache
        } else $configs = $cache;

        // Kiem tra xem trong mang co ton tai $key hay khong
        if(isset($configs[$key])){
            if($key2 !== NULL){
                $result = json_decode($configs[$key],true);
                if(isset($result[$key2]))
                    return $result[$key2];
                else
                    return '';
            }
            return $configs[$key];
        } else {
            $config = NULL;
            $model = KitCmsConfig::model()->find('name="'.$key.'"');
            if(empty($model)){
                $model = new KitCmsConfig();
                $model->name = $key;
                $model->value = '';
                $model->save();
            }
            Yii::app()->cache->delete(md5('cms_config_'.$key));
            return '';
        }

    }
    /**
     * @static
     * @param $money
     * @param $key
     * @return bool|float
     */
    public static function convertMoneyToGold($money, $key){
        if(empty($money) OR empty($key)) return FALSE;

        $rate = self::getValue('currency_rate',$key);
        $rate = !empty($rate) ? $rate : 100;
        $gold = ($money > 0) ? ceil(($money * $rate) / 100000) : ($money * $rate) / 100000;
        Yii::app()->cache->delete(md5('cms_config_'.$key));
        return $gold;
    }

    /**
     * @static
     * @param $gold
     * @param $key
     * @return bool|float
     */
    public static function convertGoldToMoney($gold, $key){
        if(empty($gold) OR empty($key)) return FALSE;

        $rate = self::getValue('currency_rate',$key);
        $rate = !empty($rate) ? $rate : 100;
        $money = ($gold > 0) ? ceil(($gold * 100000) / $rate ) : ($gold * 100000) / 100;
        Yii::app()->cache->delete(md5('cms_config_'.$key));
        return $money;
    }

    public static function updateConfig($module,$offset = 30){
        $from = self::getLastDate();
        self::_updateConfig($module,$from,$offset);
    }

    public static function getLastDate(){
        $date = Yii::app()->db->createCommand('SELECT MAX(date) AS date FROM let_kit_cms_report')->queryRow();
        if(empty($date)) return FALSE;
        return $date['date'];
    }

    public static function _updateConfig($module,$from = '2012-01-01', $offset = 30){

        $modules = KitCmsConfig::getValue($module);
        $attributes = KitCmsReport::model()->attributeNames();
        if(empty($modules)) return FALSE;

        $modules = json_decode($modules);
        $command = Yii::app()->db->createCommand();
        if(empty($from)){
            $arrFrom = array();
            foreach($modules as $module){
                $command->select('min(created_time) as date')
                    	->from('let_kit_'.$module);
                $from_date = $command->queryRow();
                if(isset($from_date['date']) AND !empty($from_date)){
                    $arrFrom[] = strtotime($from_date['date']);
                }
                $command->reset();
            }
            $from = min($arrFrom);
            $from = date('Y-m-d',$from);
        }

		// Tim ngay $to, neu vuot qua ngay hom qua thi chon ngay hom qua.
		if ($offset > 0) {
			$oneday = 60*60*24;
            $to = strtotime($from) + ($oneday * $offset);
            if($to > (time() - $oneday)) $to = time() - $oneday;
		} else $to = time() - $oneday;
		$to = date("Y-m-d",$to);
		
        $dateList = Common::createDateRangeArray($from, $to);
        //            $dateList = self::createDateRangeArray('2012-04-20', '2012-05-01');
        //            print_r($dateList);
        //            die;

        $command = Yii::app()->db->createCommand();

        foreach ($dateList as $date) {

            $command->reset();
            $cms_report = array('date' => $date);
            foreach($modules as $key => $module){
                if(!empty($module)){
                    if(in_array('module_'.$module,$attributes))
                    {
                        $count = $command->select('COUNT(*)')
                            ->from('let_kit_'.$module.'')
                            ->where("created_time BETWEEN '".$date." 00:00:00' AND '".$date." 23:59:59'")
                            ->queryScalar();
                        $cms_report['module_'.$module] = $count;
                    }
                }
                $command->reset();
            }

            if(!empty($cms_report)){
                $count = $command->select('COUNT(*)')
                                ->from('let_kit_cms_report')
                                ->where('date="'.$date.'"')
                                ->queryScalar();
                if($count > 0){
                    $command->update('let_kit_cms_report',$cms_report,'date ="'.$date.'"');
                    Yii::log('Update report: "'.$date.'"', 'info');
                    echo 'Update report: '.$date."\n";
                } else {
                    $command->insert('let_kit_cms_report',$cms_report);
                    Yii::log('Them report: "'.$date.'"', 'info');
                    echo 'Them moi report: '.$date."\n";
                }


            } else {
                Yii::log('Loi: "'.$date.'"', 'info');
                echo 'Loi: '.$date."\n";
            }
        }
    }

    /**
     * @static
     * @param string $from
     * @param int $offset
     * @return bool
     */
    public static function updateCmsCurrency($from = '2012-01-01', $offset = 30){

        $command = Yii::app()->db->createCommand();
        if(empty($from)){
            $command->select('min(time) as date')
                ->from('let_kit_currency_history');
            $from_date = $command->queryRow();
            if(isset($from_date['date']) AND !empty($from_date)){
                $from = strtotime($from_date['date']);
            }
            $command->reset();
            $from = date('Y-m-d',$from);
        }

        if($offset > 0){
            $to = strtotime($from) + (60*60*24*$offset);
            if($to > time()) return FALSE;
            $to = date("Y-m-d",$to);
        } else {
            $to = !empty($to) ? $to : date('Y-m-d');
        }

        $dateList = Common::createDateRangeArray($from, $to);

        foreach ($dateList as $date) {
            $command->reset();
            $cms_report = array('date' => $date);

                // Tong so tien
                $currency_money = $command->select('SUM(money) as currency_money')
                    ->from('let_kit_currency_history')
                    ->where("money > 0 AND time BETWEEN '".$date." 00:00:00' AND '".$date." 23:59:59'")
                    ->queryRow();
                $currency['currency_money'] = !empty($currency_money['currency_money']) ? $currency_money['currency_money'] : 0;
                $command->reset();

                //Tong so gold
                $currency_gold_in = $command->select('SUM(gold) as currency_gold_in')
                    ->from('let_kit_currency_history')
                    ->where("gold > 0 AND time BETWEEN '".$date." 00:00:00' AND '".$date." 23:59:59'")
                    ->queryRow();
                $currency['currency_gold_in'] = !empty($currency_gold_in['currency_gold_in']) ? $currency_gold_in['currency_gold_in'] : 0;
                $command->reset();

                // Tong so gold su dung
                $currency_gold_out = $command->select('SUM(gold) as currency_gold_out')
                    ->from('let_kit_currency_history')
                    ->where("gold < 0 AND time BETWEEN '".$date." 00:00:00' AND '".$date." 23:59:59'")
                    ->queryRow();
                $currency['currency_gold_out'] = !empty($currency_gold_out['currency_gold_out']) ? $currency_gold_out['currency_gold_out'] : 0;
                $command->reset();


                if(!empty($cms_report)){
                    $count = $command->select('count(id)')
                        ->from('let_kit_cms_report')
                        ->where('date="'.$date.'"')
                        ->queryScalar();
                    if($count > 0){
                        $command->update('let_kit_cms_report',$currency,' date ="'.$date.'"');
                    }else{
                        $command->insert('let_kit_cms_report',CMap::mergeArray($currency,$cms_report));
                    }
                    Yii::log('Them report: "'.$date.'"', 'info');

//                    echo 'Them moi report: '.$date."\n";
                } else {
                    Yii::log('Loi: "'.$date.'"', 'info');
//                    echo 'Loi: '.$date."\n";
                }


        }
    }
}