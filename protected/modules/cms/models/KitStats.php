<?php

Yii::import('application.modules.cms.models.db.BaseKitStats');
class KitStats extends BaseKitStats
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    protected function beforeValidate() {
        $this->thistime = Common::getNow();

        return parent::beforeValidate();
    }

    /* Use:
     * Yii::import('application.modules.cms.models.KitStats');
     * KitStats::updateValue($itemId);
     * 
     * $updateMain = TRUE
     * $updateTable = '{{kit_film}}'
     * $updateField = 'view_count'
     */
    public static function updateValue($itemId, $module = NULL, $type = 1, $object = 'view', $value = 1, $updateMain = FALSE, $updateTable = '', $updateField = '') {
        $moduleId = ($module != NULL) ? $module : Yii::app()->controller->module->id;

		// Tim xem co record nao chua
        $model = self::model()->find('module = :module AND item_id = :itemId', array(
            ':module' => $moduleId,
            ':itemId' => $itemId
        ));

        if ($model !== NULL) { // Neu co record roi thi set $type
            $sqlTime = strtotime($model->thistime);
            $nowTime = time();

            if(date('Y', $nowTime) !== date('Y', $sqlTime)){ // Khac Nam
                $type = 4;
            } elseif (date('m', $nowTime) !== date('m', $sqlTime)){ // Khac thang
                $type = 3;
            } elseif (date('W', $nowTime) !== date('W', $sqlTime)){ // Khac tuan
                $type = 2;
            } elseif (date('d', $nowTime) !== date('d', $sqlTime)){ // Khac ngay
                $type = 1;
            } else {
                $type = 0;// Trong ngay
            }
        } else { // Neu chua co record thi tao record moi
            $model = new KitStats;
            $model->module = $moduleId;
            $model->item_id = $itemId;
        }
        return self::model()->_updateValue($model, $type, $object, $value, $updateMain, $updateTable, $updateField);
    }


    private function _updateValue($model, $type = 1, $object = 'view', $value = 1, $updateMain = FALSE, $updateTable = '', $updateField = '') {
        $object_intoday = $object . '_intoday';
        $object_inweek = $object . '_inweek';
        $object_inmonth = $object . '_inmonth';
        $object_inyear = $object . '_inyear';
        $object_total = $object . '_total';

        if ($type == 1) { // Khac ngay
            $model->$object_intoday = 1;
            $model->{$object.'_inweek'} += $value;
            $model->{$object.'_inmonth'} += $value;
            $model->{$object.'_inyear'} += $value;
        } elseif ($type == 2) { // Khac tuan
            $model->$object_intoday = $model->$object_inweek = 1;
            $model->{$object.'_inmonth'} += $value;
            $model->{$object.'_inyear'} += $value;
        } elseif ($type == 3) { // Khac thang
            $model->$object_intoday = $model->$object_inweek = $model->$object_inmonth = 1;
            $model->{$object.'_inyear'} += $value;
        } elseif ($type == 4) { // Khac nam
            $model->$object_intoday = $model->$object_inweek = $model->$object_inmonth = $model->$object_inyear = 1;
        } else { // Trong ngay
            $model->{$object.'_intoday'} += $value;
            $model->{$object.'_inweek'} += $value;
            $model->{$object.'_inmonth'} += $value;
            $model->{$object.'_inyear'} += $value;
        }

        $model->$object_total += $value;

        // update view_count
        if ($updateMain == TRUE AND $updateTable != '' AND $updateField != '') {
            Yii::app()->db->createCommand()->update($updateTable, array(
                $updateField => $model->$object_total // trường hợp view thì đúng, mấy cái khác chưa xét tới
            ), 'id = :id', array(':id' => $model->item_id));
        }

        return ($model->save()) ? true : false;
    }

    /**
     * @static
     * @param $itemId
     * @param null $module
     * @param string $object
     * @param int $value
     * @return bool
     */
    public static function updateValueItem($itemId, $module = NULL, $object = 'view', $value = 1) {

        $moduleId = ($module != NULL) ? $module : Yii::app()->controller->module->id;

        // Tim xem co record nao chua
        $model = self::model()->find('module = :module AND item_id = :itemId', array(
            ':module' => $moduleId,
            ':itemId' => $itemId
        ));

        if ($model !== NULL) { // Neu co record roi thi set $type
            $sqlTime = strtotime($model->thistime);
            $nowTime = time();
        } else { // Neu chua co record thi tao record moi
            $model = new KitStats;
            $model->module = $moduleId;
            $model->item_id = $itemId;
        }
        return self::model()->_updateValueItem($model, $object, $value);
    }

    /**
     * @param $model
     * @param string $object
     * @param int $value
     * @return bool
     */
    private function _updateValueItem($model, $object = 'view', $value = 1) {
        $object_total = $object . '_total';
        $model->$object_total += $value;
        return ($model->save()) ? true : false;
    }

    /**
     * @static
     * @param string $module
     * @param int $type
     * @param null $category
     * @param int $limit
     * @return array|bool
     */
    public static function getLastest($module = '', $type = 3, $category = NULL, $limit = 10){
        $criteria = new CDbCriteria();
        $criteria->select = '*';
        if(!empty($module)){
            $criteria->condition = 'module=:module';
            $criteria->params = array(
                ':module' => $module
            );
        }
        switch($type){
            case 1:
                $criteria->order = 'view_intoday DESC';
                break;
            case 2:
                $criteria->order = 'view_inweek DESC';
                break;
            case 3:
                $criteria->order = 'view_inmonth DESC';
                break;
            case 4:
                $criteria->order = 'view_inyear DESC';
                break;
            default:
                $criteria->order = 'view_total DESC';
                break;
        }
        $criteria->limit = $limit;
        $result = KitStats::model()->findAll($criteria);
        if(empty($result)) return FALSE;
        $data = array();
        foreach($result as $item){
            $data[] = $item->attributes;
        }

        return $data;
    }

    /**
     * @static
     * @param $module
     * @param $item_id
     * @return mixed
     */
    public static function getDetails($module,$item_id){
        $item_id = intval($item_id);
        $cache_name = 'stats_details_' . $module . '_' . $item_id;
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $result = self::model()->find('module=:module AND item_id=:itemId',array(
                ':module' => $module,
                ':itemId' => $item_id,
            ));
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

    /**
     * @static
     * @param $module
     * @param $item_id
     * @param int $limit_count
     * @param int $num_record_update
     * @return bool
     */
    public static function setStats($module, $item_id, $limit_count = 5, $num_record_update = 20){
        $stats = self::getStats();

        if(isset($stats[$module][$item_id])){
            $stats[$module][$item_id] += 1;
        } else {
            $stats[$module][$item_id] = 1;
        }
        $cache_name = 'stats_count_tmp';
        Yii::app()->cache->set($cache_name,$stats);
//        print_r(self::getStats());
        if ($stats[$module][$item_id] >= $limit_count)
            self::updateStats(NULL, NULL, $num_record_update);
        return TRUE;
    }

    /**
     * @static
     * @param null $module
     * @param null $item_id
     * @return array
     */
    public static function getStats($module = NULL, $item_id = NULL){
        $cache_name = 'stats_count_tmp';
        $cache = Yii::app()->cache->get($cache_name);
//        $stats = ($cache !== FALSE) ? $cache : array();
        if($cache === FALSE){
            $stats = array();
        } else $stats = $cache;

        if(!empty($module) AND isset($stats[$module]))
            $stats = $stats[$module];

        if(!empty($item_id) AND isset($stats[$item_id]))
            $stats = $stats[$item_id];

        return $stats;
    }

    /**
     * @static
     * @param null $module
     * @param null $item_id
     * @param int $num_record_update
     * @return bool
     */
    public static function updateStats($module = NULL, $item_id = NULL, $num_record_update = 0){
        $cache_name = 'stats_count_tmp';
        $stats = self::getStats();

        if(!empty($stats)){
            $i = 0;

            if(empty($module) AND empty($item_id)){ // Neu khong ton tai module va item_id
                foreach($stats as $moduleName => $items){
                    if(!empty($items)){ // Neu danh sach item khong rong
                        foreach($items as $itemId => $count){
                            if($i < $num_record_update){
                                self::updateValue($itemId,$moduleName,1,'view',$count);
                                unset($stats[$moduleName][$itemId]);
                                $i++;
                            }
                        }
                    } else return FALSE;
                }
                Yii::app()->cache->set($cache_name,$stats);
            } elseif(!empty($module) AND empty($item_id)) { // Neu ton tai module va khong ton tai item_id
                foreach($stats[$module] as $itemId => $count){
                    if($i < $num_record_update){
                        self::updateValue($itemId,$module,1,'view',$count);
                        unset($stats[$module][$itemId]);
                        $i++;
                    }
                }
                Yii::app()->cache->set($cache_name,$stats);
            } elseif(!empty($module) AND !empty($item_id)) {
                self::updateValue($item_id,$module,1,'view',$stats[$module][$item_id]);
                unset($stats[$module][$item_id]);
                Yii::app()->cache->set($cache_name,$stats);
            } else return FALSE;
        } else return FALSE;
        return TRUE;
    }

    public static function treatment($data)
    {
        $data = CJSON::decode ((CJSON::encode($data))); // Neu la object thi chuyen thanh array
        if (!isset ($data[0]))
            $data = self::treatmentRow($data);
        else {
            foreach ($data as $key => $value) {
                $data[$key] = self::treatmentRow($value);
            }
        }
        return $data;
    }

    public static function treatmentRow($row)
    {
        return $row;
    }


    public static function getField($id, $field, $module = NULL){
        $row = KitStats::getDetails($module, $id);
        $row = KitStats::treatment($row);
        return letArray::get($row, $field, '');
    }
}
