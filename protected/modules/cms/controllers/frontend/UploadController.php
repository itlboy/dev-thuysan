<?php
Yii::import("ext.xupload.models.XUploadForm");
class UploadController extends ControllerFrontend
{
    public function actions() {
		return array(
            'upload' => array(
                'class' => 'ext.xupload.actions.XUploadAction',
                'path' => Yii::app()->params['uploadPath'] . '/' . Yii::app()->params['uploadDir'],
                'subfolderVar' => 'tmp',
                'filename' => Date('Ymd').'_'.rand(100000, 999999),
//                'filename' => Yii::app()->request->getQuery('filename'),
//                'subfolderVar' => Yii::app()->request->getQuery('module')
                ),
        );
	}
}