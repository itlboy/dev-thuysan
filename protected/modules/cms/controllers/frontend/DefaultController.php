<?php

class DefaultController extends ControllerFrontend {

    public function actionIndex() {
//        global $isMobile;
//        if (!$isMobile)
        Yii::app()->clientScript->registerLinkTag('canonical', NULL, Yii::app()->getBaseUrl(true));
        $this->render('index');
    }

    public function actionHome() {
        $this->render('home');
    }

    public function actionComment() {
        $this->render('comment');
    }

    public function actionHomeProfile() {
        if (!Yii::app()->user->isGuest) {
            $this->redirect(Yii::app()->getBaseUrl(true));
        } else {
            Yii::app()->user->logout();
            $this->render('index');
        }
    }

}
