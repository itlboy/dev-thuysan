<?php

class ErrorController extends ControllerFrontend
{
    //public $layout = '//layouts/error';
    
    public function actionIndex() {
        $error = Yii::app()->errorHandler->error;
        if ($error != NULL) {           
            if ($error['code'] == 404) {
                $this->render('404', array(
                    'error' => $error
                ));
            } // else {Xử lý các lỗi khác}
        }
    }
    
}