<?php

Yii::import('application.modules.pin.models.*');
Yii::import('application.modules.film.models.*');

class SearchController extends ControllerFrontend {

    public function actionIndex() {        
        $assign = $this->_getParam();
        $view = 'all'; // ko dùng chung 1 view để sau này có mở rộng cũng dễ hơn
        
        $module = Yii::app()->request->getQuery('module');
        if (in_array($module, array('film', 'pin')))
            $view = $module;
        
        $this->render($view, $assign);
    }

    public function _getParam() {
        $assign['options'] = isset($_GET) ? $_GET : array();
        if (isset($assign['options']['keyword'])) {
            $assign['options']['keyword'] = str_ireplace('_', ' ', $assign['options']['keyword']);
        } else {
            $assign['options']['keyword'] = '';
        }
        
        // Search Film
        if (isset($_GET['filter_length'])) {
            $assign['options'][$_GET['filter_length']] = 1;
        }
        if (isset($_GET['filter_type'])) {
            $assign['options'][$_GET['filter_type']] = 1;
        }

        return $assign;
    }

}