<?php

class AjaxController extends ControllerFrontend{

    public function actionChangeBooleanValue() {
        if (!Yii::app()->request->isAjaxRequest)
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');

        $moduleName = Yii::app()->request->getPost('module');
        $modelName = Yii::app()->request->getPost('model');
        $field = Yii::app()->request->getPost('field');
        $value = (int) Yii::app()->request->getPost('value');
        $ids = Yii::app()->request->getPost('ids');
        if (array_key_exists(strtolower($moduleName), Yii::app()->modules) AND $field !== NULL) {

            Yii::import('application.modules.'.$moduleName.'.models.'.$modelName);
            $model = CActiveRecord::model($modelName);
            if (is_array($ids) AND !empty($ids)) {                
                $criteria = new CDbCriteria;
                $criteria->addInCondition('id', $ids);

                $model->updateAll(array($field => $value), $criteria);
            } else {

                $model->updateByPk((int) $ids, array($field => ($value) ? 0 : 1));
            }

            echo 'success';
        }
    }
    
}