<?php

class DefaultController extends ControllerBackend {
    
	public function actionIndex() {
        // Khai bao
        $assign['to_time'] = letArray::get($_POST,'date_to', KitCmsConfig::getLastDate());
        $assign['from_time'] = letArray::get($_POST,'date_from', date('Y-m-d',strtotime('-30 day',strtotime($assign['to_time']))));
        // lay filter trong data
        $assign['filter'] = json_decode(KitCmsConfig::getValue('chart_filter'));
        $assign['filter'] = !empty($assign['filter']) ? $assign['filter'] : array();
        $assign['filter'] = letArray::get($_POST,'filter',$assign['filter']);
        $assign['moduleList'] = json_decode(KitCmsConfig::getValue('chart_module')); // Đổi thành json
        // Kiem tra va lay ra mang co cac module thuoc ca hai mang $filter va $moduleList
        $assign['moduleSelected'] = array();
        foreach ($assign['filter'] as $item) {
            if (in_array($item, $assign['moduleList'])) {
                $assign['moduleSelected'][] = $item;
            }
        }

        // Luu trang thai active
        if(isset($_POST) AND !empty($_POST)){
            $model = KitCmsConfig::model()->find('name ="chart_filter"');
            if(empty($model)){
                $model = new KitCmsConfig();
            }
            $model->value = json_encode(letArray::get($_POST,'filter'));
            if($model->save()){
                Yii::app()->cache->delete(md5('cms_config_chart_filter'));
            };
        }

        $getReport = KitCmsReport::getReport($assign['from_time'], $assign['to_time']);

        // Lay du lieu cua chart cac module
        if (!empty($assign['moduleSelected'])) {
            unset($data);
            $data['label'] = $assign['moduleSelected'];
            foreach ($getReport as $item) {
                foreach ($assign['moduleSelected'] as $module) {
                    $data['data'][$item['date']][$module] = $item['module_' . $module];
                }
            }
            $assign['data']['modules'] = $data;
        }
        if(in_array('currency_money',$assign['filter'])){
            $assign['filter']['currency_money'] = 'currency_money';
        }
        // Lay du lieu cua chart money (doanh thu)
        if (isset($assign['filter']['currency_money']) AND $assign['filter']['currency_money'] == 'currency_money') {
            unset($data);
            $data['label'] = array('currency_money');
            foreach ($getReport as $item) {
                $data['data'][$item['date']]['currency_money'] = $item['currency_money'];
            }
            $assign['data']['currency_money'] = $data;
        }

        if(in_array('currency_gold_in',$assign['filter'])){
            $assign['filter']['currency_gold_in'] = 'currency_gold_in';
            $assign['gold'][] = 'currency_gold_in';
        }
        if(in_array('currency_gold_out',$assign['filter'])){
            $assign['filter']['currency_gold_out'] = 'currency_gold_out';
            $assign['gold'][] = 'currency_gold_out';
        }
        if(!empty($assign['gold'])){
            unset($data);
            $data['label'] = $assign['gold'];
            foreach ($getReport as $item) {
                foreach($assign['gold'] as $gold){
                    $data['data'][$item['date']][$gold] = abs($item[$gold]);
                }
            }
            $assign['data']['gold'] = $data;
        }
        $this->render('index', $assign);
        
	}


}