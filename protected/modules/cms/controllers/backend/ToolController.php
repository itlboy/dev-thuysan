<?php

class ToolController extends ControllerBackend {

    /*
     * Mục đích của tool này là quét dữ liệu và đưa vào CSDL 1 cách tự động. Giảm thời gian nhập liệu.
     */
    public function actionImportFolder() {
        
        try
        {
            $host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';

            if ($host == 'localhost')
                $dir_root = 'D:\\DataGameMobile';
            else
                $dir_root = '/home/data.kinggame.info';

            $dir_selected = Yii::app()->request->getParam('dir', '');
            $dir_fullpath = $dir_root . DIRECTORY_SEPARATOR . $dir_selected;

            $dirs = scandir($dir_root);
            $dirs = array_diff($dirs, array('.', '..'));
            rsort($dirs);
            echo '<div>Danh sach thu muc game can import len database:</div>';
            foreach ($dirs as $key => $dir) {
                echo '<div>'.($key + 1).': <a href="'.  Yii::app()->createUrl('cms/tool/ImportFolder', array('dir' => $dir)).'">'.$dir.'</a></div>';
            }

            echo '
                <div>----------------------------------------------------</div>
                <div>Dir: '.$dir_selected.'</div>
                <div>Thu muc (full): '.$dir_fullpath.'</div>
            ';
            
            if (isset($dir_selected) AND !empty($dir_selected) AND is_dir($dir_fullpath)) {

                /* Quét 1 thư mục và xác định có bao nhiêu item (Mỗi item có 1 file chính, 1 file ảnh đại diện và 1 file txt lưu thông tin của item. 3 file này trùng tên) */
                // Quét thư mục
                $files = scandir($dir_fullpath);

                // Loai bo ext ra khỏi tên của từng file trong mảng
                $files = array_map(function($e){
                    return pathinfo($e, PATHINFO_FILENAME);
                }, $files);
                // Tìm kiếm những file trùng
                foreach ($files as $key => $file) {
                    if ($key > 0 AND $file == $files[$key - 1]) {
                        $items[] = $file;
                    }
                }
                $items = array_unique($items);
                $items = array_values($items);

                echo '<div>Tim thay: '.count($items).'</div>';
                
                /* Nhập liệu */
                foreach ($items as $key => $filename) {
                    $nameSource = $dir_fullpath . DIRECTORY_SEPARATOR . $filename;
                    
                    // Get file txt
                    $fileTxt = $nameSource . '.txt';
                    $fileTxt = require($fileTxt);
                    foreach ($fileTxt as $module => $info) {
                        // trường title = title trong file txt, nếu không có thì lấy tên của file
                        if ((isset($info['title']) AND !empty($info['title']))) {
                            $data['title'] = $info['title'];
                        } else {
                            $data['title'] = str_replace('_', ' ', $filename);
                        }

                        // Kiểm tra xem trong database có row nào trùng tên chưa?
                        $command = Yii::app()->db->createCommand();
                        $count = $command->select('count(*) as num')->from('let_kit_' . $module)->where('title = "' . $data['title'] . '"')->queryRow();
                        if ($count['num'] == 0) {
                            if ($module == 'game') { // Trường hợp đặc biệt của Game
                                if (file_exists($nameSource . '.apk')) {
                                    $data['file'] = $dir_selected . '/' . $filename . '.apk';
                                } elseif (file_exists($nameSource . '.ipa')) {
                                    $data['file'] = $dir_selected . '/' . $filename . '.apk';
                                } elseif (file_exists($nameSource . '.jar')) {
                                    $data['file'] = $dir_selected . '/' . $filename . '.apk';
                                }
                            }

                            // Xứ lý ảnh
                            $image_path = $nameSource . '.jpg';
                            $image = self::createThumb($module, $image_path, $data['title']);
                            $data['image'] = $image;

                            $data['intro'] = $info['intro'];
                            $data['video'] = $info['video'];
                            $data['type'] = $info['type'];
                            $data['price'] = $info['price'];
                            $data['operating_system'] = $info['operating_system'];
                            $data['content'] = $info['content'];

                            $data['module'] = $module;

                            // Lay danh sach category
                            $data['categories'] = explode(',', $info['categories']);

                            // trường nào có trong file txt thì mới đưa vào database
                            $insert[] = $data;
                            echo '<div>[success]['.($key+1).'] '.$data['title'].' da duoc tao</div>';
                        } else echo '<div>[fail] '.$data['title'].' da co trong database</div>';
                    }

                }
    //            echo "<pre>";
    //            var_dump($insert);
    //            echo "</pre>";
                if (isset($insert) AND !empty($insert)) {
                    foreach ($insert as $key => $item) {
                        $module = $item['module'];
                        unset($item['module']);

                        $categories = $item['categories'];
                        unset($item['categories']);

                        $command = Yii::app()->db->createCommand();
                        $command->insert('let_kit_' . $module, $item);
                        $item_id = Yii::app()->db->lastInsertId;

                        // Xu ly category
                        foreach ($categories as $categorie) {
                            $command = Yii::app()->db->createCommand();
                            $command->insert('let_kit_' . $module . '_category', array(
                                $module . '_id' => $item_id,
                                'category_id' => $categorie,
                            ));
                        }
                    }
                    echo '<div><strong>So luong ban ghi tao thanh cong: '.count($insert).' ban ghi da duoc tao </strong></div>';
                }
            }
        } catch(Exception $e) {
            var_dump($e->getMessage());
        }
    }
    
    /**
     * Tao thumb cho anh dai dien theo cac kich co quy dinh truoc
     * @param type $module
     * @param type $inputImage
     * @param type $title
     * @return string Tra ve duong dan cua anh de luu vao database
     */
    public static function createThumb ($module, $tmpFile, $title) {
        $date = date('Y/m/d/');

        $size = getimagesize($tmpFile);
        if (!isset($size[0]) OR !isset($size[1]) OR (int)$size[0] <= 0 OR (int)$size[1] <=0) {
                unset($tmpFile);
                return FALSE;
        }

        // Lay thong tin file tam va tao ra ten file moi
        $tmpFileInfo = pathinfo($tmpFile);

//        $fileName = Common::convertStringToUrl($title) . '_' . rand(100000, 999999) . '.' .strtolower($tmpFileInfo['extension']);
        $fileName = Common::convertStringToUrl($title) . '_' . rand(100000, 999999) . '.jpg';

        // get image config
        $imageConfig = Yii::app()->getModule($module)->image;

        // Resize
        foreach ($imageConfig as $key => $config) {
            if (in_array($key, array('origin', 'large', 'medium', 'small'))) {
                // Thu muc luu file
                $fileDir = $module . '/'.$config['folder'].'/' . $date;

                // Duong dan file
                $filePath = Common::getImageUploaded($fileDir . $fileName, 'PATH');

                // Tao dir theo path neu dir chua ton tai
                Common::mkPath($fileDir, Common::getImageUploaded('', 'PATH'));

                if ($config['type'] == 'move') copy($tmpFile, $filePath);
                elseif ($config['type'] == 'resize') {
                    $image = Yii::app()->image->load($tmpFile);
                    $image->resize(letArray::get($config, 'width'), letArray::get($config, 'height'), letArray::get($config, 'master', Image::AUTO))->rotate(letArray::get($config, 'rotate'))->quality(letArray::get($config, 'quality'));
                    $image->save($filePath);
                } elseif ($config['type'] == 'crop') {
                    $image = Yii::app()->image->load($tmpFile);

                    // Xac dinh master
                    $width = $size[0];
                    $height = $size[1];
                    if ($width/letArray::get($config, 'width') < $height/letArray::get($config, 'height'))
                        $master = Image::WIDTH;
                    else
                        $master = Image::HEIGHT;
                    // END Xac dinh master

                    $image->resize(letArray::get($config, 'width'), letArray::get($config, 'height'), $master)->crop(letArray::get($config, 'width'), letArray::get($config, 'height'))->rotate(letArray::get($config, 'rotate'))->quality(letArray::get($config, 'quality'));
                    $image->save($filePath);
                }

                // Gan Logo
                if (letArray::get($config, 'addlogo') !== NULL) {
                    Yii::import('application.vendors.libs.Addlogo');
                    $logoPath = Yii::app()->basePath . '/../' . letArray::get($config, 'addlogo');
                    $Addlogo = new Addlogo();
                    $Addlogo->createImageAndLogo($filePath, $logoPath);
                    $Addlogo->InsertLogo(3); // 1 = top left, 2 = top right, 3 = bottom right, 4 = bottom left, 5 = bottom center, 6 = top center
                    $Addlogo->save($filePath);
                }
            }
        }
//        unlink($tmpFile);
        return $date . $fileName;
    }

}