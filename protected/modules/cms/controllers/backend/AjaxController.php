<?php

class AjaxController extends ControllerBackend {

    public function actionChangeBooleanValue() {
        if (!Yii::app()->request->isAjaxRequest)
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');

        $moduleName = Yii::app()->request->getPost('module');
        $modelName = Yii::app()->request->getPost('model');
        $field = Yii::app()->request->getPost('field');
        $value = (int) Yii::app()->request->getPost('value');
        $ids = Yii::app()->request->getPost('ids');
        if (array_key_exists(strtolower($moduleName), Yii::app()->modules) AND $field !== NULL) {

            Yii::import('application.modules.'.$moduleName.'.models.'.$modelName);
            $model = CActiveRecord::model($modelName);
            if (is_array($ids) AND !empty($ids)) {                
                $criteria = new CDbCriteria;
                $criteria->addInCondition('id', $ids);

                $model->updateAll(array($field => $value), $criteria);
            } else {

                $model->updateByPk((int) $ids, array($field => ($value) ? 0 : 1));
            }

            echo 'success';
        }
    }
    
    public function actionDeleteRecord($id) {
        if (!Yii::app()->request->isAjaxRequest)
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');

        $moduleName = Yii::app()->request->getPost('module');
        $modelName = Yii::app()->request->getPost('model');
        $field = Yii::app()->request->getPost('field');
        $value = (int) Yii::app()->request->getPost('value');
        $trash = Yii::app()->request->getPost('trash');
        if (array_key_exists(strtolower($moduleName), Yii::app()->modules) AND $field !== NULL) {
            Yii::import('application.modules.'.$moduleName.'.models.'.$modelName);

            $model = CActiveRecord::model($modelName)->findByPk((int) $id);
            if ($model === null)
                throw new CHttpException(404, 'The requested page does not exist.');
            
            if ($trash) {
                $model->updateByPk((int) $id, array($field => $value));
            } else {
                $model->delete();
            }

            echo 'success';
        }
    }

    public function actionDeleteMultiRecord() {
        if (!Yii::app()->request->isAjaxRequest)
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');

        $moduleName = Yii::app()->request->getPost('module');
        $modelName = Yii::app()->request->getPost('model');
        $ids = Yii::app()->request->getPost('ids');

        if (array_key_exists(strtolower($moduleName), Yii::app()->modules) AND is_array($ids) AND !empty($ids)) {

            Yii::import('application.modules.'.$moduleName.'.models.'.$modelName);
            $model = CActiveRecord::model($modelName);
            
            $criteria = new CDbCriteria;
            $criteria->addInCondition($model->tableSchema->primaryKey, $ids);

            $model->deleteAll($criteria);
            echo 111;
        }
    }

}