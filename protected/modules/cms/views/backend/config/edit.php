<?php
$moduleName = 'cms';
$modelName = 'KitCmsConfig';
$formID = 'kit_'.$moduleName.'_form';
$gridID = 'kit_'.$moduleName.'_grid';

$this->breadcrumbs = array(
    Yii::t('backend', $moduleName) => $this->createUrl('/'.$moduleName.'/default/index'),
    Yii::t('global', 'Edit') . ' ' . Yii::t('backend', $moduleName),
);
?>

<div class="flat_area grid_16">
    <h2><?php echo (!$model->isNewRecord) ? ((isset($model->title) AND $model->title !== NULL) ? $model->title : Yii::t('global', 'Update') . ' ' . Yii::t('backend', $moduleName)) : Yii::t('global', 'Create') . ' ' . Yii::t('backend', $moduleName); ?></h2>
</div>

<div class="grid_16">
    <button class="green small" onclick="js:jQuery('#apply').val(0); document.forms['<?php echo $formID; ?>'].submit();">
        <div class="ui-icon ui-icon-check"></div>
        <span><?php echo Yii::t('global', 'Save'); ?></span>
    </button>
    <button class="green small" onclick="js:jQuery('#apply').val(1); document.forms['<?php echo $formID; ?>'].submit();">
        <div class="ui-icon ui-icon-check"></div>
        <span><?php echo Yii::t('global', 'Apply'); ?></span>
    </button>
    <?php if (!$model->isNewRecord): ?>
    <button class="blue small" onclick="window.location = '<?php echo Yii::app()->createUrl($moduleName . '/' . $this->id . '/info', array('id' => $model->id)); ?>'">
        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icons/small/white/chart_8.png" />
        <span><?php echo Yii::t('global', 'View Info and stats'); ?></span>
    </button>
    <button class="red small">
        <div class="ui-icon ui-icon-trash"></div>
        <span><?php echo Yii::t('global', 'Trash'); ?></span>
    </button>
    <?php endif; ?>
</div>



<?php
$form=$this->beginWidget('CActiveForm', array(
    'id' => $formID,
    'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype'=>'multipart/form-data'),
));
?>
<?php echo $this->renderPartial('//cms/default/_errorSummary', array('error' => $form->errorSummary($model))); ?>
<?php if(Yii::app()->user->hasFlash('error')): ?>
<div class="grid_16">
    <div class="alert dismissible alert_red">
        <img height="24" width="24" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icons/small/white/alarm_bell.png">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </div>
</div>
<?php endif; ?>
<?php echo CHtml::hiddenField('apply', 0); ?>

<div class="box grid_16 tabs">
    <ul class="tab_header clearfix">
        <li><a href="#tab_general"><?php echo Yii::t('global', 'General'); ?></a></li>
            </ul>
    <a href="#" class="grabber"></a>
    <a href="#" class="toggle"></a>
    <div class="toggle_container">
        <div id="tab_general" class="block">
            <?php echo $this->renderPartial('_formContent', array('form' => $form, 'model' => $model), true); ?>
        </div>
        </div>
<?php $this->endWidget(); ?>

