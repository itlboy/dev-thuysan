<?php
$moduleName = 'cms';
$modelName = 'KitCmsConfig';
$formID = 'kit_'.$moduleName.'_form';
$gridID = 'kit_'.$moduleName.'_grid';

$this->breadcrumbs = array(
    Yii::t('backend', $moduleName) => $this->createUrl('/'.$moduleName.'/default/index'),
    Yii::t('global', 'List') . ' ' . Yii::t('backend', $moduleName),
);
?>

<div class="grid_16">
    <div class="flat_area">
        <h2><?php echo Yii::t('backend', 'List'); ?> <?php echo Yii::t('backend', $moduleName); ?></h2>
    </div>
    <button class="red small" onclick="ajaxDeleteMultiRecord('<?php echo $moduleName; ?>', '<?php echo $modelName; ?>', '<?php echo Yii::app()->createUrl('cms/ajax/DeleteMultiRecord'); ?>', '<?php echo $gridID; ?>')">
        <div class="ui-icon ui-icon-close"></div>
        <span><?php echo Yii::t('global', 'Delete'); ?></span>
    </button>
    <?php    
    $this->widget('GridView', array(
        'id' => $gridID,
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => array(
            array(
                'class' => 'CCheckBoxColumn',
                'id' => 'chkIds',
                'selectableRows' => 2,
                'htmlOptions' => array('width' => '20px', 'align' => 'center')
            ),
            array(
                'name' => 'id',
                'value' => '$data->id'
            ),
            array(
                'name' => 'name',
                'value' => '$data->name'
            ),
                                            array( // display a column with "view", "update" and "delete" buttons
                'class' => 'CButtonColumn',
                'header' => CHtml::dropDownList('pageSize',
                    Yii::app()->user->getState('pageSize',20),
                    array(2=>2,10=>10,20=>20,30=>30,40=>40,50=>50,100=>100,200=>200,500=>500,1000=>1000),
                    array('onchange'=>"$.fn.yiiGridView.update('".$gridID."',{ data:{pageSize: $(this).val() }})")
                ),
                'template' => '{update} {delete}',
                'buttons' => array(
                    'update' => array(
                        'label' => Yii::t('global', 'Edit'),
                        'url' => 'Yii::app()->createUrl("//'.$moduleName.'/'.$this->id.'/update", array("id"=>$data->id))',
                        'imageUrl' => Yii::app()->theme->baseUrl.'/images/icons/system/edit_16.png',
                        'options' => array('class' => 'btnGridUpdate')
                    ),
                    'delete' => array(
                        'label' => Yii::t('global', 'Delete'),
                        'url' => 'Yii::app()->createUrl("//'.$moduleName.'/'.$this->id.'/delete", array("id"=>$data->id))',
                        'imageUrl' => Yii::app()->theme->baseUrl.'/images/icons/system/delete_16.png',
                    ),
                ),
            )
        ),
    ));
    ?>
</div>
