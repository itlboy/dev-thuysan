
<div class="box grid_16" style="margin-top: 10px;">
    <div class="toggle_container">
        <div class="block">
            <h2 class="section"><?php echo Yii::t('global', 'General'); ?></h2>
            
                
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'name'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'name'); ?>
                    <?php echo $form->error($model, 'name'); ?>
                </div>
            </fieldset>
                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'value'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'value'); ?>
                    <?php echo $form->error($model, 'value'); ?>
                </div>
            </fieldset>
            

        </div>
    </div>
</div>



