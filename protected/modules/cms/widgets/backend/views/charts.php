<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<?php if(!empty($this->data)): ?>
<?php
    $id = rand(1000,9000);
    $data = $this->data;
    $label = array();
    foreach($data['label'] as $value){
        $label[] = Yii::t('global',$value);
    }
    $total = array();
?>
<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Year', <?php echo '"'.implode('","',$label).'"'; ?>],
            <?php foreach($data['data'] as $key => $item): ?>
            ['<?php echo $key; ?>', <?php echo implode(',',$item); ?>],
            <?php endforeach; ?>
        ]);

        var options = {
            title: 'Thống kê <?php echo implode(',',$label); ?>'
        };

        var chart = new google.visualization.<?php echo $this->type; ?>(document.getElementById('chart_div_<?php echo $id; ?>'));
        chart.draw(data, options);
    }
</script>

    <div id="chart_div_<?php echo $id; ?>" style="width: 100%; height: 500px;">
    </div>

<?php endif; ?>