<?php

class charts extends Widget
{
    public $view = '';
    public $title = '';
    public $data = array();
    public $type = 'AreaChart'; // Cac kieu bieu do: LineChart, AreaChart, PieChart, ComboChart, BarChart, ColumnChart;
    public $option = 'mutil'; // mutil nhieu bieu do - one tren cung 1 bieu do

    public function init()
    {
        if ($this->view == '') $this->view = __CLASS__;
    }

    public function run()
    {
        $this->render($this->view);
    }
}