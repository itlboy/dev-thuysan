<?php

class gwmod extends Widget
{
    public $view = '';

    public function init()
    {
        if ($this->view == '') $this->view = __CLASS__;
    }
 
    public function run()
    {
        $data = NULL;
        $this->render($this->view, array(
            'data' => $data,
        ));
    }
}