<?php

Yii::import('application.modules.article.models.db.BaseKitArticleCategory');
class KitArticleCategory extends BaseKitArticleCategory {

    var $className = __CLASS__;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getArticle2Category($articleId) {
        $result = array();
        $data = self::findAll('article_id=:articleId', array(':articleId' => $articleId));
        foreach ($data as $item2Type)
            $result[] = $item2Type->category_id;
        return $result;
    }

    /**
     * Create
     * @param array $categories
     * @param int $itemId 
     */
    public static function createArticleCategory($categories = array(), $itemId) {
        if (! is_array($categories)) return FALSE;
        foreach ($categories as $categoryId) {
            $item2Cat = new KitArticleCategory;
            $item2Cat->category_id = $categoryId;
            $item2Cat->article_id = $itemId;
            $item2Cat->save();
        }
    }

    /**
     * Delete
     * @param array $categories
     * @param int $itemId 
     */
    public static function deleteArticleCategory($categories = array(), $itemId) {
        if (! is_array($categories)) return FALSE;
        foreach ($categories as $categoryId) {
            self::model()->deleteAll('category_id=:categoryId AND article_id=:articleId', array(
                ':categoryId' => $categoryId,
                ':articleId' => $itemId,
            ));
        }
    }
    public static function checkCategory($id)
    {
        $id = intval($id);
        $result = self::model()->findAll('article_id=:articleId', array(':articleId' => $id));
        return $result;
    }
}