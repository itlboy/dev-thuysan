<?php

Yii::import('application.modules.article.models.db.BaseKitArticle');
class KitArticle extends BaseKitArticle{
    var $className = __CLASS__;

    public $category_id;
    public $categories;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function attributeLabels() {
        return array_merge(parent::attributeLabels(), array(
            'title' => 'Tiêu đề',
            'status' => 'Kích hoạt ?',
            'trash' => 'Thùng rác',
            'image' => 'Ảnh đại diện',
            'intro' => 'Giới thiệu',
            'content' => 'Nội dung',
            'from_time' => 'Thời gian bắt đầu',
            'to_time' => 'Thời gian kết thúc',
            'promotion' => 'Tiêu điểm',
            'sorder' => 'Thứ tự',
            'tags' => 'Thẻ',
            'focus' => 'Tin hot ?',
            'author' => 'Tác giả',
            'source' => 'Nguồn',
            'categories' => 'Danh mục'
        ));
    }

    /**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
        return CMap::mergeArray(
            parent::rules(),
            array(
                            array('categories', 'safe'),
                                    )
        );
	}

    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'KitArticleCategory' => array(self::MANY_MANY, 'KitCategory',
                '{{kit_article_category}}(article_id, category_id)'),
            'creatorUser' => array(self::BELONGS_TO, 'KitAccount', 'creator', 'select' => 'username'),
            'editorUser' => array(self::BELONGS_TO, 'KitAccount', 'editor', 'select' => 'username'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;
        $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');

		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.title',$this->title,true);
		$criteria->compare('t.author',$this->author,true);
		$criteria->compare('t.source',$this->source,true);
		$criteria->compare('t.image',$this->image,true);
		$criteria->compare('t.intro',$this->intro,true);
		$criteria->compare('t.content',$this->content,true);
		$criteria->compare('t.tags',$this->tags,true);
		$criteria->compare('t.view_count',$this->view_count);
		$criteria->compare('t.comment_count',$this->comment_count);
		$criteria->compare('t.from_time',$this->from_time,true);
		$criteria->compare('t.to_time',$this->to_time,true);
		$criteria->compare('t.layout_id',$this->layout_id,true);
		$criteria->compare('t.seo_title',$this->seo_title,true);
		$criteria->compare('t.seo_url',$this->seo_url,true);
		$criteria->compare('t.seo_desc',$this->seo_desc,true);
		$criteria->compare('t.creator',$this->creator,true);
		$criteria->compare('t.created_time',$this->created_time,true);
		$criteria->compare('t.editor',$this->editor,true);
		$criteria->compare('t.updated_time',$this->updated_time,true);
		$criteria->compare('t.sorder',$this->sorder,true);
		$criteria->compare('t.promotion',$this->promotion);
		$criteria->compare('t.status',$this->status);
		$criteria->compare('t.trash',$this->trash);

        if ($this->category_id !== NULL) {
            $categories = array();
            $categories[] = $this->category_id;
            $category = KitCategory::model()->findByPk($this->category_id);
            $descendants = $category->descendants()->findAll();
            foreach ($descendants as $cat) {
                $categories[] = $cat->id;
            }
            $criteria->addInCondition('ic.category_id', $categories);
        }

        $criteria->join = 'LEFT JOIN {{kit_article_category}} ic ON ic.article_id = t.id';

        $criteria->group = 't.id';

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
            'sort' => array(
                'defaultOrder' => 'id DESC',
            ),
		));
	}

    protected function beforeValidate() {
        $this->editor = Yii::app()->user->id;
        $this->updated_time = Common::getNow();
        if ($this->isNewRecord) {
            $this->creator = $this->editor;
            $this->created_time = $this->updated_time;
            $this->view_count = 0;
            $this->comment_count = 0;
            $this->sorder = 500;
        }

        return parent::beforeValidate();
    }

    public function parseCategory($data, $delimiter = '') {
        $arr = array();
        foreach ($data as $value) {
            $arr[] = $value->name;
        }
        echo implode($arr, $delimiter);
    }



    ///////////////////////////////// Frontend /////////////////////////////////
    /**
     * treatment
     * @param mixed $data
     * @return array
     */
    public static function treatment($data)
    {
        $data = CJSON::decode ((CJSON::encode($data))); // Neu la object thi chuyen thanh array
        if (!isset ($data[0]))
            $data = self::treatmentRow($data);
        else {
            foreach ($data as $key => $value) {
                $data[$key] = self::treatmentRow($value);
            }
        }
        return $data;
    }

    public static function treatmentRow($row)
    {
        $urlTitle = Common::convertStringToUrl($row['title']);
        $row['url'] = Yii::app()->createUrl($urlTitle.'-article-'.$row['id']);
        $row['title'] = str_replace (array ('"', "'"), array ('&quot;', '&apos;'), $row['title'] );
        return $row;
    }


    /**
     * Get Details
     * @param int $id
     * @return object
     */
    public static function getDetails($id) {

        $id = intval($id);
        $cache_name = md5(__METHOD__ . '_' . $id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache

        if ($cache === FALSE) {
            $result = self::model()->findByPk($id, array(
                'select' => Common::getFieldInTable(self::model()->getAttributes(), 't.'),
                'with' => array('creatorUser', 'editorUser')
            ));
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

    public static function getList ($category = NULL, $conditions = array(), $limit = 20){
        $cache_name = md5(__METHOD__ . '_' . $category);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
            foreach ($conditions as $key => $value) {
                $criteria->condition = Common::addWhere($criteria->condition, 't.' . $key . ' = ' . $value);
            }


            if ($category !== NULL AND $category >= 0) {
                $catListObject = KitCategory::getListInParent('article', $category);
                $catList = array($category);
                foreach ($catListObject as $cat) {
                    $catList[] = $cat->id;
                }
                $criteria->join = "INNER JOIN {{kit_article_category}} t2 ON t.id=t2.article_id";
//                $criteria->group = "t.id";
                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', $catList).')');
            }

            $criteria->order = 't.id DESC';
            $criteria->limit = $limit;
            $result = self::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }


    public static function getLastest ($category = NULL, $limit = 30, $olderId = 0, $show_home = 0){
        $olderId = intval($olderId);
        $cache_name = md5(__METHOD__ . '_' . $limit . '_' . $category);
        if ($olderId != 0)
            $cache_name = md5(__METHOD__ . '_' . $limit . '_' . $category . '_' . $olderId);

        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {

            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
            if ($category !== NULL AND $category >= 0) {
                $catListObject = KitCategory::getListInParent('article', $category);
                $catList = array($category);
                foreach ($catListObject as $cat) {
                    $catList[] = $cat->id;
                }

                $criteria->join = "INNER JOIN {{kit_article_category}} t2 ON t.id=t2.article_id";
//                $criteria->group = "t.id";
                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', $catList).')');
            }

            // chỉ lấy ra những bài viết ID < $id của bài viết hiện tại
            if ($olderId != 0) {
                $criteria->addCondition('t.id < ' . $olderId);
            }

            if ($show_home == 1) {
                $criteria->addCondition('t.is_home = 1');
            }

            $criteria->order = 't.id DESC';
            $criteria->limit = $limit;
            $result = self::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

//    public static function getLastestBackup ($category = NULL, $limit = 30){
//        $cache_name = md5(__METHOD__ . '_' . $category);
//        $cache = Yii::app()->cache->get($cache_name); // Get cache
//        if ($cache === FALSE) {
//
//            $criteria = new CDbCriteria;
//            $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');
//            $criteria->condition = '';
//            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
//            if ($category !== NULL AND $category >= 0) {
//                $catListObject = KitCategory::getListInParent('article', $category);
//                $catList = array($category);
//                foreach ($catListObject as $cat) {
//                    $catList[] = $cat->id;
//                }
//
//                $criteria->join = "INNER JOIN {{kit_article_category}} t2 ON t.id=t2.article_id";
////                $criteria->group = "t.id";
//                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', $catList).')');
//            }
//
//            $criteria->order = 't.id DESC';
//            $criteria->limit = $limit;
//            $result = self::model()->findAll($criteria);
//            Yii::app()->cache->set($cache_name, $result); // Set cache
//        } else return $cache;
//        return $result;
//    }

    public static function getPromotion ($category = NULL, $limit = 20){
        $cache_name = md5(__METHOD__ . '_' . $category);
        $cache = Yii::app()->cache->get($cache_name); // Get cache

        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
            $criteria->condition = Common::addWhere($criteria->condition, 't.promotion = 1');
            if ($category !== NULL AND $category >= 0) {
                $criteria->join = "INNER JOIN {{kit_article_category}} t2 ON t.id=t2.article_id";
                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', KitCategory::arrCategory('article', $category)).')');
            }
            $criteria->order = 't.id DESC';
            $criteria->limit = $limit;
            $result = self::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

    public static function getMostPopular ($category = NULL, $limit = 20){
        $cache_name = md5(__METHOD__ . '_' . $category);
        $cache = Yii::app()->cache->get($cache_name); // Get cache

        if ($cache === FALSE) {

            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
            $criteria->condition = Common::addWhere($criteria->condition, 't.promotion = 1');

            if ($category !== NULL AND $category >= 0) {
                $criteria->join = "INNER JOIN {{kit_article_category}} t2 ON t.id=t2.article_id";

                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', KitCategory::arrCategory('article', $category)).')');
            }
            $criteria->order = 't.id DESC';
            $criteria->limit = $limit;
            $result = self::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
    public static function getLastestByCat ($category = array(), $limit = 20){
        $cache_name = md5(__METHOD__.'_'.json_encode($category).'_'.$limit);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
            if ($category !== NULL) {
                $catList = $category;
                $criteria->join = "INNER JOIN {{kit_article_category}} t2 ON t.id=t2.article_id";
                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', $catList).')');
            }

            $criteria->order = 't.id DESC';
            $lm = explode(',',$limit);
            if(count($lm) > 1)
            {
                $criteria->limit = $lm[0];
                $criteria->offset = $lm[1];
            } else {
                $criteria->limit = $lm[0];
            }
            $result = self::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result, Yii::app()->params['timeCache']); // Set cache
        } else return $cache;
        return $result;
    }
    public static function getLastestView ($category = NULL, $limit = 20){
        $cache_name = md5(__METHOD__ . '_' . json_encode($category));
        $cache = Yii::app()->cache->get($cache_name); // Get cache;

        if ($cache === FALSE) {
            $command = Yii::app()->db;
            $where = '';
            if(!empty($category)){
                if(is_array($category)){
                    $catList = implode(',',$category);
                } else {
                    $catList = $category;
                }
                $where = ' AND t2.category_id IN ('.$catList.') ';
            }

            $sql = 'SELECT t.*,t2.*,t3.*,t.id as id FROM let_kit_article AS t,let_kit_article_category AS t2,let_kit_stats AS t3
                    WHERE
                    t3.module="article" AND
                    t.id = t2.article_id AND
                    t.id = t3.item_id '.
                    $where.
                    'ORDER BY t3.view_inmonth DESC
                    LIMIT '.$limit;
            $result = $command->CreateCommand($sql);
            $result = $result->queryAll();
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
    public static function getFocus ($category = array(), $limit = 20){
        $cache_name = md5(__METHOD__ . '_' . $category.'_'.$limit);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
            $criteria->condition = Common::addWhere($criteria->condition, 't.focus = 1');
            if ($category !== NULL) {
                $catList = $category;
                $criteria->join = "INNER JOIN {{kit_article_category}} t2 ON t.id=t2.article_id";
                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', $catList).')');
            }

            $criteria->order = 't.id DESC';
            $lm = explode(',',$limit);
            if(count($lm) > 1)
            {
                $criteria->limit = $lm[0];
                $criteria->offset = $lm[1];
            } else {
                $criteria->limit = $lm[0];
            }
            $result = self::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
}