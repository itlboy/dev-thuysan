<?php

//Cần khai báo thêm $view để sử dụng các template khác theo param danh mục
class listByCat extends Widget {

    public $category = 0;
    public $limit = 8;
    public $view = '';
    public $href = '';

    public function init() {
        if ($this->view == '') $this->view = __CLASS__;//newly added by Mr.Điệp
        Yii::import('article.models.KitArticle');
        Yii::import('category.models.KitCategory');
    }

    public function run() {
        // Get category

        if(!empty($this->category)){
            $arrCat[] = $this->category;
        } else {
            $arrCat = array();
        }
        $data['cat'] = KitCategory::getListInParent('article', $this->category);
        if (empty($data['cat'])){
            $arrCat[] = $this->category;
        } else {
            $data['cat'] = KitCategory::treatment($data['cat']);
            foreach ($data['cat'] as $key => $cat) {
                // Get sub category
                $data['subcat'][$cat['id']] = KitCategory::getListInParent('article', $cat['id']);
                $data['subcat'][$cat['id']] = KitCategory::treatment($data['subcat'][$cat['id']]);
            }
            foreach ($data['cat'] as $key => $cat) {
                $arrCat[] = $cat['id'];
            }
        }

        $data = KitArticle::getLastestByCat($arrCat,$this->limit);
        if(!empty($data))
        {
            $data = KitArticle::treatment($data);
        }
        $this->render($this->view, array(
            'data' => $data,
        ));
    }

}