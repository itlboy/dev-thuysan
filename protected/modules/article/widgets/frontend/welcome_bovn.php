<?php

    class welcome_bovn extends Widget{

        public $id = '';
        public $view = '';

        public function init(){
            if($this->view == '')
                $this->view = __CLASS__;
            Yii::import('article.models.KitArticle');
        }

        public function run(){

            $data = KitArticle::getDetails($this->id);
            if(!empty($data)){
                $data = KitArticle::treatment($data);
            }else{
                return false;
            }
            $this->render($this->view,array(
                'data' => $data,
            ));
        }

    }

?>