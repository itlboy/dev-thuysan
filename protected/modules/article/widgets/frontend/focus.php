<?php

class focus extends Widget{
    /**
     * Bài viết đầu tiên ở phần Hot News
     *
     */
    public $view ='';
    public $limit = 1;
    public $category = NULL;
    public function init(){
        if($this->view ==''){
            $this->view = __CLASS__;
        }
        Yii::import('article.models.KitArticle');
    }
    public function run(){
        $data = KitArticle::getFocus($this->category,$this->limit);
        if(empty($data))
            $data = KitArticle::getPromotion($this->category,$this->limit);
            if(empty($data))
                $data = KitArticle::getLastest($this->category,$this->limit);
                if(empty($data))
                    return FALSE;
        $data = KitArticle::treatment($data);
        $this->render($this->view,array(
            'data' => $data,
        ));
    }
}