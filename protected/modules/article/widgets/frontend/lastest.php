<?php

class lastest extends Widget {

    public $view = '';    
    public $category = NULL;
    public $title = '';
    public $limit = 10;
    public $href = '';
    public $olderId = 0; // Chỉ lọc ra những bài viết cũ hơn bài hiện tại
    public $show_home = 0;

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
        Yii::import('article.models.KitArticle');
		Yii::import('category.models.KitCategory');
    }

    public function run() {
        // Get category
		if (!empty($this->category)) {
			$assign['cat'] = KitCategory::getDetails($this->category);
			$assign['cat'] = KitCategory::treatment($assign['cat']);
			$assign['subcat'] = KitCategory::getListInParent('article', $this->category);
			$assign['subcat'] = KitCategory::treatment($assign['subcat']);	
		}
		
        $data = KitArticle::getLastest($this->category, $this->limit, $this->olderId, $this->show_home);
        if (empty($data))
            return FALSE;
        $data = KitArticle::treatment($data);
        $assign['data'] = array_slice($data, 0, $this->limit);
        $this->render($this->view, $assign);
    }

}