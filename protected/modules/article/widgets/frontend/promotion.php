<?php

class promotion extends Widget {

    public $view = '';
    public $category = NULL;
    public $title = '';
    public $limit = 10;
    public $href = '';

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
        Yii::import('article.models.KitArticle');
    }

    public function run() {
        $data = KitArticle::getPromotion($this->category);

        if (empty($data))
            return FALSE;
        $data = KitArticle::treatment($data);
        $data = array_slice($data, 0, $this->limit);

        $this->render($this->view, array(
            'data' => $data,
        ));
    }

}