<?php

class tags extends Widget {

    public $view = '';
    public $data = array();

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
        Yii::import('article.models.KitArticle');
    }

    public function run() {
        $criteria = new CDbCriteria;
        $criteria->condition = '';
        if (isset($this->data['tag']) AND $this->data['tag'] !== '') {
            $criteria->condition = letFunction::addWhere($criteria->condition, "tags LIKE '%" . $this->data['tag'] . "%'");
        }

        $results = KitArticle::model()->findAll($criteria);
//        if (empty($results))
//            return FALSE;
        $this->render($this->view, array(
            'results' => $results,
        ));
    }

}