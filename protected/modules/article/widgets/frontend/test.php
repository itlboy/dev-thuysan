<?php

class test extends KitWidget {

    public $view = '';
    public $category = NULL;
    public $title = '';

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
    }

    public function run() {
        $this->render($this->view);
    }

}