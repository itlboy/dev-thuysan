<?php
if ($data[0]['image']) {
    $image = Common::getImageUploaded('article/large/'.$data[0]['image']);
} else {
    $image = Yii::app()->theme->baseUrl."/images/no_image.jpg";
}
?>
<script type="text/javascript">
    $(document).ready(function(){
        var objImg = new Image();
        objImg.src = '<?php echo $image . '?' . time(); ?>';
        objImg.onload = function()
        {
            $('.loading').remove();
           $('.hot-news-ct').fadeIn();
        }
    });
</script>
<div class="hot-news loading"></div>
<div class="hot-news hot-news-ct" style="display: none;">
    <div class="hot-news-item">
        <a href="<?php echo $data[0]['url']; ?>" title="<?php echo $data[0]['title']; ?>" ><img title="<?php echo $data[0]['title']; ?>" width="350px" src="<?php echo $image; ?>" alt="<?php echo $data[0]['title']; ?>"  onerror="this.src='<?php echo $image; ?>'"></a>
        <h2><a title="<?php echo $data[0]['title']; ?>" class="title" href="<?php echo $data[0]['url']; ?>"><?php echo $data[0]['title']; ?></a></h2>
    </div>
    <p class="description">
        <?php if(!empty($data[0]['has_brand']) AND $data[0]['has_brand'] == 1 ): ?>
        (Thủy sản Việt Nam) -
        <?php endif; ?>
        <?php $intro = preg_replace('/\(Thủy(.*?)\)\ -/i','',$data[0]['intro']); ?>
        <?php echo letText::limit_chars($intro, 250); ?>
    </p>
</div>