<div class="ct-widget">
    <div class="ct-widget-header">
        <h4><span><a><?php echo $this->title ?></a></span></h4>
    </div>       
    <div class="ct-widget-content">
        <?php foreach ($data as $key => $value): ?>
        <?php
        if ($key == 0) {
            $css = 'first';
        } elseif ($key == (count($data) - 1)) {
            $css = 'last';
        }else{
            $css = '';
        }
        ?>
        <?php
        if($value['image']){
            $a_image = Common::getImageUploaded('article/small/' . $value['image']);
        }else{
            $a_image = Yii::app()->theme->baseUrl."/images/no_image.jpg";
        }
        ?>
            <div class="ct-item <?php echo $css; ?>">
                <a class="thumb" href="<?php echo $value['url']; ?>" title="<?php echo $value['title']; ?>"><img src="<?php echo $a_image; ?>" alt="<?php echo $value['title']; ?>" title="<?php echo $value['title']; ?>"></a>
                <a href="<?php echo $value['url']; ?>" title="<?php echo $value['title']; ?>"><?php echo $value['title']; ?></a>
            </div>
        <?php endforeach; ?>
        <div class="clear"></div>
    </div>
</div> 