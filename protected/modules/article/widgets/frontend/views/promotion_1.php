<?php $num_rows_slide = 2; ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#news-hot-slider').slides({
            preload: true,
            play: 7000000,
            fadeSpeed: 800,
            effect: 'fade',
            generatePagination: false,
            generateNextPrev: false
        });	
    });		
</script>
<?php // endif; ?>
            <div class="box-news-hot">
                <!-- News slider -->
                <div class="news-hot" id="news-hot-slider">
                    <div class="slides_container">
                        <?php foreach ($data as $key => $value): ?>
                        <?php if ($key < $num_rows_slide): ?>
                        <div class="news-hot-item">
                            <div class="image-hot">
                                <?php
                                if ($value['image']) {
                                    $image = Common::getImageUploaded('article/large/'.$value['image']);
                                } else {
                                    $image = Yii::app()->theme->baseUrl."/images/no_image.jpg";
                                }
                                ?>
                                <a href="<?php echo $value['url']; ?>" title="<?php echo $value['title']; ?>"><img class="bo" src="<?php echo $image; ?>" alt="<?php echo $value['title']; ?>" title="<?php echo $value['title']; ?>" onerror="this.src='<?php echo $image; ?>'"/></a>
                                <h2><a href="<?php echo $value['url']; ?>" class="title" title="<?php echo $value['title']; ?>"><?php echo $value['title']; ?></a></h2>
                            </div>
                            <p class="description">
                                <?php echo letText::limit_chars($value['intro'], 150); ?>
                            </p>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <!-- //End News slider -->
                <div class="news-hot-other">
                    <ul class="new">
                        <?php foreach ($data as $key => $value): ?>
                        <?php if ($key >= ($num_rows_slide - 1)): ?>
                        <li>
                            <a href="<?php echo $value['url']; ?>" title="<?php echo $value['title']; ?>"><img src="<?php if($value['image']){?><?php echo Common::getImageUploaded('article/medium/'.$value['image']); ?><?php }else{ ?><?php echo Yii::app()->theme->baseUrl . "/images/no_image.jpg"; ?><?php } ?>" width="132" height="97" class="bo" alt="<?php echo $value['title']; ?>"></a>
                            <a href="<?php echo $value['url']; ?>" class="title" title="<?php echo $value['title']; ?>"><?php echo $value['title']; ?></a>
                        </li>
                        <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                    <?php // $this->widget('article.widgets.frontend.promotion', array('view' => 'promotion_2', 'limit' => 2)); ?>
                    <div class="clear">
                    </div>
                    <?php $this->widget('article.widgets.frontend.lastest', array('view' => 'lastest_1', 'limit' => 5)); ?>
                </div>
            </div>
