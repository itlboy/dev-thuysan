        <div class="box-news-content home-THOI-TRANG">	
            <div class="box-style-7">	
                <ul class="style">
                    <li>
                        <?php
                        if ($rows[0]['image']) {
                            $image_l = Common::getImageUploaded('article/large/' . $rows[0]['image']);
                        } else {
                            $image_l = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
                        }
                        ?>
                        <a title="<?php echo $rows[0]['title']; ?>" href="<?php echo $rows[0]['url']; ?>">
                            <img class="bo img_lazy" src="<?php echo $image_l; ?>" alt="<?php echo $rows[0]['title']; ?>" width="132" height="97" original="<?php echo $image_l; ?>" style="display: block; " />
                        </a>
                        <a title="<?php echo $rows[0]['title']; ?>" class="title" href="<?php echo $rows[0]['url']; ?>"><?php echo $rows[0]['title']; ?></a>
                        <p><?php echo letText::limit_chars($rows[0]['intro'], 160); ?></p>
                    </li>
                </ul>
<?php if (isset($rows[1])) : ?>
                <div class="news-other">
                    <ul>
    <?php foreach ($rows as $key => $article): ?>
        <?php if ($key >= 1 AND $key < 5): ?>
                        <?php
                        if ($article['image']) {
                            $image_l = Common::getImageUploaded('article/medium/' . $article['image']);
                        } else {
                            $image_l = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
                        }
                        ?>
                        <li <?php if ($key == 1): ?>class="first"<?php endif; ?>>
                            <a href="<?php echo $article['url']; ?>" title="<?php echo $article['title']; ?>"><img class="bo img_lazy" src="<?php echo $image_l; ?>" alt="<?php echo $article['title']; ?>" width="63" height="63" original="<?php echo $image_l; ?>" style="display: block; "></a>
                            <a href="<?php echo $article['url']; ?>" class="title2" title="<?php echo $article['title']; ?>"><?php echo $article['title']; ?></a>
                        </li>
        <?php endif; ?>
    <?php endforeach; ?>
                    </ul>
                </div>
<?php endif; ?>
                <div class="clear"></div>
            </div>           
        </div>
