<div class="main-box">
    <div class="main-box-head">
        <h2><span><a><?php echo $this->title ?></a></span></h2>
        <small><?php echo $this->title ?></small>  
    </div>
    <div class="new-focus-bg">
        <ul class="new-focus">
            <?php foreach ($data as $key => $value): ?>
                <?php
                if ($value['image']) {
                    $a_image = Common::getImageUploaded('article/small/' . $value['image']);
                } else {
                    $a_image = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
                }
                if ($key == 0) {
                    $css = "first";
                } elseif ($key == count($data) - 1) {
                    $css = "last";
                }else{
                    $css='';
                }
                ?>
                <li class="<?php echo $css; ?>">
                    <a href="<?php echo $value['url']; ?>" title="<?php echo $value['title']; ?>"><img class="bo" src="<?php echo $a_image; ?>" alt="<?php echo $value['title']; ?>"></a>
                    <a href="<?php echo $value['url']; ?>" title="<?php echo $value['title']; ?>"><?php echo $value['title']; ?></a>
                </li>
            <?php endforeach; ?>
        </ul>     
        <div class="clear"></div>
    </div>     

</div>