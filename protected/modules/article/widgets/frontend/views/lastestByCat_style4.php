<?php
if ($rows[0]['image']) {
    $image_l = Common::getImageUploaded('article/medium/' . $rows[0]['image']);
} else {
    $image_l = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
}
if (isset($rows[1]) && $rows[1]['image']) {
    $image_r = Common::getImageUploaded('article/medium/' . $rows[1]['image']);
} else {
    $image_r = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
}
if (isset($rows[2]) && $rows[2]['image']) {
    $image3 = Common::getImageUploaded('article/small/' . $rows[2]['image']);
} else {
    $image3 = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
}
?>
        <div class="box-news-content home-GIAI-TRI">	
            <div class="box-style-4">	
                <ul class="style">
                    <li>
                        <a title="<?php echo $rows[0]['title']; ?>" href="<?php echo $rows[0]['url']; ?>">
                            <img width="132" height="97" alt="<?php echo $rows[0]['title']; ?>" src="<?php echo $image_l; ?>" class="bo img_lazy" original="<?php echo $image_l; ?>" style="display: inline;">
                        </a>
                        <a title="<?php echo $rows[0]['title']; ?>" class="title" href="<?php echo $rows[0]['url']; ?>"><?php echo $rows[0]['title']; ?></a>
<!--                        <p><?php echo $rows[0]['intro']; ?></p>-->
                    </li>
<?php if (isset($rows[1])) : ?>
                    <li>
                        <a title="<?php echo $rows[1]['title']; ?>" href="<?php echo $rows[1]['url']; ?>">
                            <img width="132" height="97" alt="<?php echo $rows[1]['title']; ?>" src="<?php echo $image_r; ?>" class="bo img_lazy" original="<?php echo $image_r; ?>" style="display: inline;">
                        </a>
                        <a title="<?php echo $rows[1]['title']; ?>" class="title" href="<?php echo $rows[1]['url']; ?>"><?php echo $rows[1]['title']; ?></a>
<!--                        <p> <?php echo $rows[1]['intro']; ?></p>-->
                    </li>
<?php endif; ?>
                </ul>
<?php if (isset($rows[2])) : ?>
                <div class="news-other">
                    <div class="news-more">
                        <a title="<?php echo $rows[2]['title']; ?>" href="<?php echo $rows[2]['url']; ?>">
                        <img class="bo img_lazy" src="<?php echo $image3; ?>" alt="<?php echo $rows[2]['title']; ?>" width="105" height="78" original="<?php echo $image3; ?>" style="display: block; ">
                        </a>
                        <a title="<?php echo $rows[2]['title']; ?>" href="<?php echo $rows[2]['url']; ?>"><?php echo $rows[2]['title']; ?></a>
                        <div class="clear"></div>
                    </div>
    <?php if (isset($rows[3])) : ?>
                    <ul class="others">
        <?php foreach ($rows as $key => $article): ?>
            <?php if ($key >= 3 AND $key < 7): ?>
                        <li <?php if ($key == 2): ?>class="first"<?php endif; ?>>
                            <a title="<?php echo $article['title']; ?>" href="<?php echo $article['url']; ?>"><?php echo $article['title']; ?></a>
                        </li>
            <?php endif; ?>
        <?php endforeach; ?>
                    </ul>
    <?php endif; ?>
                </div>
<?php endif; ?>
                <div class="clear"></div>
            </div>        
        </div>
