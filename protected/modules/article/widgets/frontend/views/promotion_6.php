<div class="box-news-hot">
    <!-- News slider -->
    <?php $this->widget('article.widgets.frontend.focus',array()) ?>
    <!-- //End News slider -->
    <div class="news-hot-other ct-news-hot" style="margin-bottom:10px;">
        <?php
        $this->widget('ads.widgets.frontend.ads', array(
            'category' => 93,
        )); //Home_right_L
        ?>

<!--        <div class="ct-ads-hot-news">
            <a title="Thuỷ sản việt nam" href="<?php echo Yii::app()->request->baseUrl; ?>"><img title="Thủy sản việt nam" alt="Thủy sản việt nam" src="<?php echo Yii::app()->theme->baseUrl ?>/images/thuysanvietnam.png" alt=""></a>
        </div>-->
        <div class="main-box-head">
            <h2><span style="padding: 3px 40px 0 10px"><a>Tin mới</a></span></h2>
            <small>Tin mới</small>
        </div>
        <?php $this->widget('article.widgets.frontend.lastest', array('view' => 'lastest_1', 'limit' => 11, 'show_home' => 1)); ?>
    </div>
            <!--Start newest -->
            <div class="box-catnew box-category" style="clear:both;">
                <!-- News item 1 -->
<!--                --><?php //$this->widget('article.widgets.frontend.lastest', array('category' => 14, 'view' => 'list_style4', 'title' => 'Tiêu điểm')); ?>
                <!-- //End news item 1 -->
                <!-- News item 2 -->
<!--                --><?php //$this->widget('article.widgets.frontend.lastest', array('category' => 21, 'view' => 'list_style4', 'title' => 'Đời sống ngư dân')); ?>
                <!-- //End news item 2 -->
                <!-- News item 3 -->
<!--                --><?php //$this->widget('article.widgets.frontend.lastest', array('category' => 15, 'view' => 'list_style4', 'title' => 'Hoạt động doanh nghiệp')); ?>
                <!-- //End news item 3 -->
                <!-- News item 4 -->
<!--                --><?php //$this->widget('article.widgets.frontend.lastest', array('category' => 8, 'view' => 'list_style4', 'title' => 'Giải trí')); ?>
                <!-- //End news item 4 -->
				
				<?php
				$cs = Yii::app()->getClientScript();
				$cs->registerCssFile(Yii::app()->theme->baseUrl . '/css/ct_slider.css?ver=10.231');
				$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/jquery.tools.min.js', CClientScript::POS_END);
				?>
				<div id="social-slider" style="width:644px;">
					<a title="Tin phía sau" class="back prev browse left">Tin phía sau</a>
					<div class="social-slider-items">
						<ul style="left: -670px; ">
							<li style="width:670px;">
								<?php $this->widget('article.widgets.frontend.promotion', array('view' => 'promotion_3', 'limit' => 12, 'category' => !empty($category) ? $category : NULL)); ?>
							</li>
							<li style="width:670px;">
								<?php $this->widget('article.widgets.frontend.promotion', array('view' => 'promotion_4', 'limit' => 12, 'category' => !empty($category) ? $category : NULL)); ?>
							</li>
						</ul>
					</div>
					<a title="Tin tiếp theo" class="next browse right">Tin tiếp theo</a>
				</div>
				<script type="text/javascript">
					window.onload = function(){
						$( '.social-slider-items' ).scrollable({ circular: true, speed: 600 });
					};

				</script>
				
                <?php // echo $this->renderPartial('//blocks/social_slider', array('title' => 'Social Slider')); ?>
            </div>
            <!-- //End newest -->

</div>
