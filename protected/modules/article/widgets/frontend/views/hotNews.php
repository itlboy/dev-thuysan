<?php $i = 0 ;$j = 0; ?>
<div class="social-focus">
    <div class="social-box-focus">
        <div class="social-hot">
            <?php foreach($data as $item): ?>
                <?php if($i == 0 ): ?>
                    <a href="<?php echo $item['url']; ?>" title="<?php echo $item['title']; ?>">
                        <img src="<?php echo Common::getImageUploaded('article/large/' . $item['image']); ?>" alt="<?php echo $item['title']; ?>">
                    </a>
                    <div class="social-title">
                        <a href="<?php echo $item['url'] ?>" class="title" title="<?php echo $item['title'] ?>"><?php echo $item['title'] ?></a>
                        <span>Tin nổi bật</span>
                    </div>
                    <p style="text-align: justify"><?php echo $item['intro'] ?></p>
    <!--            <a title="TP. HCM: Xe máy Honda gãy đôi khi đang lưu thông" class="social-more" href="xa-hoi/diem-nong/tp-hcm-xe-may-honda-gay-doi-khi-dang-luu-thong-102692.html">TP. HCM: Xe máy Honda gãy đôi khi đang lưu thông</a>-->
                        <?php
                        break;
                    endif;
                $i++;
                endforeach;
            ?>
        </div>
        <div class="social-hot-other">
            <div class="social-hot-other-head">
                <ul>
                    <li class="current tm"><a href="xa-hoi/">Tin mới</a><span>Xem</span></li>
                    <li class="tt"><a href="xa-hoi/tin-tuc-63-tinh-thanh/">Tin tức 63 tỉnh</a><span>Xem</span></li>
                </ul>
            </div>
            <ul style="height: 373px; overflow: hidden">
                <?php foreach($data as $item) : ?>
                        <?php if($i > 0) : ?>
                            <?php $class = ($i == 1) ? 'first' : ''; ?>
                            <li class="<?php echo $class ?>"><a href="<?php echo $item['url']; ?>" title="<?php echo $item['title']; ?>"><?php echo $item['title']; ?></a></li>
                        <?php
                            endif;
                    $i++;
                    endforeach;
                ?>
            </ul>
        </div>
    </div>
    <div class="social-video">
        <p>
            <a href="http://xahoi.com.vn/bong-da/video-bong-da/"><img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/ads/origin/2012/07/02/qc1-top-right3_432465.jpg" title="Liên hệ quảng cáo" alt="Liên hệ quảng cáo" height="225" width="300"></a>
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/ads/origin/2012/07/02/qc1-top-right3_432465.jpg" title="Quảng cáo" alt="Quảng cáo" height="195" width="300" style="margin-top:10px;">
        </p>
    </div>

</div>