                            <ul class="nav nav-tabs tab-news">
                                <li class="active"><a data-toggle="tab" href="#tinmoi"><?php echo $this->title; ?></a></li>
                                <?php foreach ($data['cat'] as $key => $cat): ?>
                                <li><a data-toggle="tab" href="#<?php echo Common::convertStringToUrl($cat['name']); ?>"><?php echo $cat['name']; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                            <div class="tab-content">
                                <div id="tinmoi" class="tab-pane fade in active">
                                    <?php $this->widget('article.widgets.frontend.lastest', array(
                                        'category' => $this->category,
                                        'view' => 'lastest_gamemongtien',
                                        'limit' => $this->limit,
                                    )); ?>

                                    <p class="xem-them"><a href="<?php echo Yii::app()->createUrl('//article/frontend/list'); ?>">Xem thêm ...</a></p>
                                </div>
                                <?php foreach ($data['cat'] as $key => $cat): ?>
                                <div id="<?php echo Common::convertStringToUrl($cat['name']); ?>" class="tab-pane fade">
                                    <?php foreach ($data['article'][$cat['id']] as $row): ?>
                                    <p class="list-new"><a class="" href="" title="<?php echo $row['title']; ?>"><?php echo letText::limit_chars($row['title'], 45); ?></a><span class="date">19/10</span></p>
                                    <?php endforeach; ?>
                                    <p class="xem-them"><a href="<?php echo $cat['url']; ?>">Xem thêm ...</a></p>
                                </div>
                                <?php endforeach; ?>
                            </div>
