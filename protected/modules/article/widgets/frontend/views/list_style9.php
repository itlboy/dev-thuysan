<?php $i = 0; ?>
<div class="main-box">
    <div class="main-box-head">

        <h2><span><a href="<?php echo !empty($this->href) ? $this->href : 'javascript:;' ?>" title="<?php echo $this->title; ?>"><?php echo $this->title; ?></a></span></h2>
        <small><?php echo $this->title ?></small>
    </div>
    <div class="main-content-navbar">
        <ul class="mycontent">
            <li>
                <?php foreach($data as $item): ?>
                    <?php if($i == 0): ?>
                    <a title="<?php echo $item['title']; ?>" href="<?php echo $item['url'] ?>"><img class="bo" alt="<?php echo $item['title']; ?>" src="<?php echo Common::getImageUploaded('article/large/'.$item['image']); ?>"></a>
                    <a class="title video" title="<?php echo $item['title'] ?>" href="<?php echo $item['url'] ?>"><?php echo $item['title']; ?></a>
                    <p><?php echo $item['intro']; ?></p>
                    <?php
                        $i++;
                        break;
                        endif;
                    ?>
                <?php endforeach; ?>
                <div class="clear"></div>
                <?php foreach($data as $item): ?>
                    <?php if($i > 1): ?>
                        <a title="<?php echo $item['title'] ?>" class="x-more" href="<?php echo $item['url'] ?>"><?php echo $item['title'] ?></a>
                    <?php
                    endif;
                    $i++;
                ?>
                <?php endforeach; ?>
            </li>
        </ul>
    </div>
</div>
