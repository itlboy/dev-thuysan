<?php
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/js/slides.min.jquery.js', CClientScript::POS_END);
$idRand = 'id_' . rand(1000, 9999);
$idRand2 = 'id2_' . rand(1000, 9999);
?>
<style type="text/css" media="screen">
    #<?php echo $idRand2; ?> {
        width:300px;
        height:350px;
        overflow: hidden;
    }
    #<?php echo $idRand2; ?> div {
        width:300px;
        height:350px;
        display:block;
    }
</style>

<script>
    $(function(){
        $('#<?php echo $idRand; ?>').slides({
        preload: true,
        play: 7000,
        fadeSpeed: 2000,
        generatePagination: false,
        generateNextPrev: false
    });
});			
</script>
<div class="box-news">
    <div class="box-news-head" style="background: #0e6d2b; text-align: center; color: white;">
        <h2 style="background: none; float: none; margin: 0 auto; padding: 0; text-transform: uppercase; height: 21px; line-height: 22px;">
            <span style="background: none; padding: 0; height: 17px;">
                <a href="javascript:void(0);" style="color: #FFF; padding: 0 2px; font-size: 12px;"><?php echo $this->title ?></a>
            </span>
        </h2>
        <small style="display: none;">Down</small>
    </div>
    <div class="new-focus-bg" id="<?php echo $idRand; ?>">
        <div class="slides_container" id="<?php echo $idRand2; ?>">
            <div class="slide" style="background: #d2ecda;">
                <ul class="new-focus">
                    <!--
                    //Thêm hàm if để chọn fisrt and last for li class
                    -->
                    <?php foreach ($data as $key => $value): ?>
                        <?php
                        if ($value['image']) {
                            $a_image = Common::getImageUploaded('article/medium/' . $value['image']);
                        } else {
                            $a_image = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
                        }
                        $num_per_page = 3;
                        $mod = $key % $num_per_page;
                        ?>
                    <li <?php if ($mod == 0): ?>class="first"<?php elseif ($mod == ($num_per_page - 1)): ?>class="last"<?php endif; ?> style="height: 97px;">
                        <a href="<?php echo $value['url']; ?>" title="<?php echo $value['title']; ?>"><img src="<?php echo $a_image; ?>" alt="<?php echo $value['title']; ?>" style="width: 132px; height: 97px;" /></a>
                        <a href="<?php echo $value['url']; ?>" class="title2" title="<?php echo $value['title']; ?>" style="font-weight: bold;"><?php echo $value['title']; ?></a>
                    </li>
                    <?php if ($mod == ($num_per_page - 1) AND $key < (count($data) - 1)): ?>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="slide" style="background: #d2ecda;">
                <ul class="new-focus">
                    <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>
