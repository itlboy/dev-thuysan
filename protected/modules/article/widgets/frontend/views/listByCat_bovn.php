<?php if(!empty($data)){ ?>
<div id="widget-comment-wrapper">
    <div class="comment-items">
        <ul class="mycontent">
            <?php foreach ($data as $item): ?>
            <?php
            if ($item['image']) {
                $image_no1 = Common::getImageUploaded('article/medium/' . $item['image']);
            } else {
                $image_no1 = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
            }

            $day = '';
            switch(date('l', strtotime ($item["created_time"])))
            {
                case 'Monday':
                    $day = 'Thứ 2';
                    break;
                case 'Tuesday':
                    $day = 'Thứ 3';
                    break;
                case 'Wednesday':
                    $day = 'Thứ 4';
                    break;
                case 'Thursday':
                    $day = 'Thứ 5';
                    break;
                case 'Friday':
                    $day = 'Thứ 6';
                    break;
                case 'Saturday':
                    $day = 'Thứ 7';
                    break;
                case 'Sunday':
                    $day = 'Chủ nhật';
                    break;
            } ?>
            <li style="margin-bottom: 5px;">
                <a title="<?php echo $item['title'] ?>" href="<?php echo $item['url'] ?>"><img class="bo" title="<?php echo $item['title'] ?>" alt="<?php echo $item['title'] ?>" src="<?php echo $image_no1 ?>" width="120"></a>
                <a class="title video" title="<?php echo $item['title'] ?>" href="<?php echo $item['url'] ?>"><?php echo $item['title'] ?></a>
                <span class="time" style="color: #888888;"><?php echo $day.', '.date('d/m/Y H:i:s', strtotime ($item["created_time"])) . ' GMT+7'; ?></span>
                <div style="color: #B2B2B2;"><?php echo $item['intro']; ?></div>
                <div class="clear"></div>
            </li>
            <?php endforeach; ?>
        </ul>
        <?php if($this->page == TRUE): ?>
            <style type="text/css">
                .div_page .page a,.div_page .next a,.div_page .previous a{ color: #ffffff !important;}
                .div_page .selected a{ background: white !important; color: black !important;}
            </style>
            <div style="float: left; padding: 10px 0px; width:100%;margin-top: 5px;" class="div_page">
                <?php $this->widget("CLinkPager",array('pages'=>$page)); ?>
            </div>
        <?php endif; ?>
    </div>
</div>
        <?php }else{
            echo 'Tin tức đang được cập nhật';
        }; ?>