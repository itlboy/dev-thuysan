        <div class="box-news">
            <div class="box-news-head">
                <h2><span><a href="<?php echo $cat['url']; ?>"><?php echo $cat['name']; ?></a></span></h2>
                <p>
                    <?php foreach ($subcat as $key => $value): ?>
                        <a href="<?php echo $value['url']; ?>" title="<?php echo $value['name']; ?>"><?php echo $value['name']; ?></a>
                        <?php if ($key < (count($subcat) - 1)): ?> |<?php endif; ?>
                    <?php endforeach; ?>
                </p>
    <!--            <a class="rss" href="#rss"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icon_rss.png"></a>      -->
            </div>
			
			<div class="box-news-content">	
				<div class="box-style-1">	
					<ul class="style">
						<li>
							<?php
							if ($data[0]['image']) {
								$image_l = Common::getImageUploaded('article/medium/' . $data[0]['image']);
							} else {
								$image_l = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
							}
							if (isset($data[1]) && $data[1]['image']) {
								$image_r = Common::getImageUploaded('article/medium/' . $data[1]['image']);
							} else {
								$image_r = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
							}
							?>
							<a title="<?php echo $data[0]['title']; ?>" href="<?php echo $data[0]['url']; ?>">
								<img width="132" height="97" alt="<?php echo $data[0]['title']; ?>" src="<?php echo $image_l; ?>" class="bo" original="<?php echo $image_l; ?>" style="display: inline;">
							</a>
							<a title="<?php echo $data[0]['title']; ?>" class="title" href="<?php echo $data[0]['url']; ?>"><?php echo $data[0]['title']; ?></a>
							<p><?php echo letText::limit_chars($data[0]['intro'], 100); ?></p>
						</li>
		<?php if (isset($data[1])) : ?>
							<li>
								<a title="<?php echo $data[1]['title']; ?>" href="<?php echo $data[1]['url']; ?>">
									<img width="132" height="97" alt="<?php echo $data[1]['title']; ?>" src="<?php echo $image_r; ?>" class="bo" original="<?php echo $image_r; ?>" style="display: inline;">
								</a>
								<a title="<?php echo $data[1]['title']; ?>" class="title" href="<?php echo $data[1]['url']; ?>"><?php echo $data[1]['title']; ?></a>
								<p><?php echo letText::limit_chars($data[1]['intro'], 100); ?></p>
							</li>
					<?php endif; ?>
					</ul>
		<?php if (isset($data[2])) : ?>
						<div class="news-other" style="float:left; width:100%; margin-top: 20px;">
							<h3>Các tin khác</h3>
							<ul class="others">
								<?php foreach ($data as $key => $article): ?>
				<?php if ($key >= 2 AND $key <= 6): ?>
								<li <?php if ($key == 4): ?>class="first"<?php elseif ($key == 6): ?>class="last"<?php endif; ?>>
									<a title="<?php echo $article['title']; ?>" href="<?php echo $article['url']; ?>"><?php echo $article['title']; ?></a>
								</li>
				<?php endif; ?>
			<?php endforeach; ?>
							</ul>
						</div>
		<?php endif; ?>
					<div class="clear"></div>
				</div>        
			</div>

        </div>