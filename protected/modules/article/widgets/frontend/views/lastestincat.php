<?php foreach ($data['cat'] as $cat): ?>
    <div class="box-news">
        <div class="box-news-head">
            <h2><span><a href="#" title="<?php echo $cat['name']; ?>"><?php echo $cat['name']; ?></a></span></h2>
        </div>
        <div class="box-news-content">
            <div class="box-style-2">
                <ul class="style">
                    <?php
                    if ($data['article'][$cat['id']][0]['image']) {
                        $image_no1 = Common::getImageUploaded('article/medium/' . $data['article'][$cat['id']][0]['image']);
                    } else {
                        $image_no1 = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
                    }
                    ?>
                    <?php foreach ($data['article'][$cat['id']] as $key => $article): ?>
                        <?php if ($key <= 5): ?>
                            <?php if ($key == 0): ?>
                                <li>
                                    <a href="<?php echo $data['article'][$cat['id']][0]['url']; ?>" title="Tiêu đề 1"><img class="bo" src="<?php echo $image_no1; ?>" alt="<?php echo $data['article'][$cat['id']][0]['title']; ?>" title="<?php echo $data['article'][$cat['id']][0]['title']; ?>"></a>
                                    <a href="<?php echo $data['article'][$cat['id']][0]['url']; ?>" class="title" title="<?php echo $data['article'][$cat['id']][0]['title']; ?>"><?php echo $data['article'][$cat['id']][0]['title']; ?></a>
                                    <p>
                                        <?php echo $data['article'][$cat['id']][0]['intro']; ?>
                                    </p>
                                </li>
                            <?php else: ?>
                                <li>
                                    <a href="<?php echo $article['url']; ?>" class="more" title="<?php echo $article['title']; ?>"><?php echo $article['title']; ?></a>
                                </li>
                            <?php
                            endif;
                        endif;
                        ?>
                    <?php endforeach; ?>
                </ul>
                <div class="news-other">
                    <div class="news-more">
                        <?php if (isset($data['article'][$cat['id']][6])) : ?>
                            <?php
                            if ($data['article'][$cat['id']][6]['image']) {
                                $a_image = Common::getImageUploaded('article/small/' . $data['article'][$cat['id']][6]['image']);
                            } else {
                                $a_image = $image_no1 = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
                            }
                            ?>
                            <a title="<?php echo $data['article'][$cat['id']][6]['title']; ?>" href="<?php echo $data['article'][$cat['id']][6]['url']; ?>"><img class="bo" title="<?php echo $data['article'][$cat['id']][6]['title']; ?>" src="<?php echo $a_image; ?>" alt="<?php echo $data['article'][$cat['id']][6]['title']; ?>"></a>
                            <a class="title2" title="<?php echo $data['article'][$cat['id']][6]['title']; ?>" href="#"><?php echo $data['article'][$cat['id']][6]['title']; ?></a>
                            <div class="clear">
                            </div>
                        <?php endif; ?>
                    </div>
                    <ul class="others">
                        <?php foreach ($data['article'][$cat['id']] as $key => $article): ?>
                            <?php if ($key > 6): ?>
                                <li class="first">
                                    <a href="<?php echo $article['url']; ?>" title="<?php echo $article['title']; ?>"><?php echo $article['title']; ?></a>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>