<?php foreach ($data['cat'] as $cat): ?>
	<?php if ( !in_array($cat['id'], array(10, 100))): ?>
    <?php
    if (in_array( (int) $cat['id'], array(3, 9, 12))) $style = '2';
    elseif (in_array( (int) $cat['id'], array(8))) $style = '3';
    elseif (in_array( (int) $cat['id'], array(10))) $style = '4';
    elseif (in_array( (int) $cat['id'], array(6))) $style = '5';
    else $style = '1';
    ?>
        <div class="box-news">
            <div class="box-news-head">
                <h2><span><a href="<?php echo $cat['url']; ?>"><?php echo $cat['name']; ?></a></span></h2>
                <p>
                    <?php foreach ($data['subcat'][$cat['id']] as $key => $subcat): ?>
                        <a href="<?php echo $subcat['url']; ?>" title="<?php echo $subcat['name']; ?>"><?php echo $subcat['name']; ?></a>
                        <?php if ($key < (count($data['subcat'][$cat['id']]) - 1)): ?> |<?php endif; ?>
                    <?php endforeach; ?>
                </p>
    <!--            <a class="rss" href="#rss"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icon_rss.png"></a>      -->
            </div>
            <?php echo $this->render('lastestByCat_style' . $style, array('rows' => $data['article'][$cat['id']])); ?>
        </div>
    <?php endif; ?>
    
	<?php
	$adsMap = array(
		'1' => '94',
		'2' => '103',
	);	
	?>
    <?php if (in_array( (int) $cat['id'], array_keys($adsMap))): ?>
    	<div style="margin:5px 0;">
			<?php
            $this->widget('ads.widgets.frontend.ads', array(
                'category' => $adsMap[$cat['id']],
            ));
            ?>
        </div>
    <?php endif; ?>
<?php endforeach; ?>