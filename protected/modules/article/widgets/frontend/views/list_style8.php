<div class="ct-widget">
    <div class="ct-widget-header">
        <h4><span><a>Doanh Nghiệp</a></span></h4>
    </div>
    <div class="ct-widget-content">
        <p style="margin-top: 10px;margin-bottom: 5px"><a href="<?php echo $data[0]['url'] ?>" style="font-weight: bold;"><?php echo !empty($data[0]['title']) ? $data[0]['title'] : ''; ?></a></p>
        <a href="<?php echo $data[0]['url'] ?>"><img style="margin-bottom: 10px" src="<?php echo Common::getImageUploaded('article/large/' . $data[0]['image']); ?>" width="100%" /> </a>
        <p style="margin-bottom: 10px;"><?php echo !empty($data[0]['intro']) ? $data[0]['intro'] : '' ?></p>
        <div class="clear"></div>
    </div>
</div>