<?php foreach ($data as $key => $value): ?>
    <?php if ($key > 5): ?>
        <div class="social-slider-item">
            <a href="#">&nbsp;</a>                
            <a href="<?php echo $value['url']; ?>" title="<?php echo $value['title']; ?>">
                <img src="<?php echo Common::getImageUploaded('article/medium/' . $value['image']); ?>" alt="<?php echo $value['title']; ?>" title="<?php echo $value['title']; ?>" width="132" height="97" />
            </a>
            <a href="<?php echo $value['url']; ?>" title="<?php echo $value['title']; ?>" class="title2"><?php echo $value['title']; ?></a>
        </div>
    <?php endif; ?>
<?php endforeach; ?>