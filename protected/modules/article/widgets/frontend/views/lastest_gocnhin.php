<div style="border: 1px #ccc solid; margin-bottom: 10px;">
	<div style="padding:5px; font-weight: bold; color: #be0435; background: #ededed;">
		<?php if (empty($this->category)): ?>
			<?php echo $this->title; ?>
		<?php else: ?>
			<a href="<?php echo $cat['url']; ?>" style="color: #be0435;"><?php echo $cat['name']; ?></a>
		<?php endif; ?>
	</div>
	<?php foreach ($data as $key => $value): ?>
	<div style="padding: 5px;">
		<div style="float: left; width: 49%;"><a href="<?php echo $value['url']; ?>" title="<?php echo $value['title']; ?>">
			<i><?php echo $value['title']; ?></i></a>
		</div>
		<div style="float: right; width: 49%;">
			<a href="<?php echo $value['url']; ?>" title="<?php echo $value['title']; ?>"><img src="<?php echo Common::getImageUploaded('article/medium/' . $value['image']); ?>" width="100%" /></a>
			<div style="margin-top: 5px;"><strong><?php echo $value['author']; ?></strong></div>
		</div>
		<div class="clear"></div>
	</div>
	<?php endforeach; ?>
</div>

