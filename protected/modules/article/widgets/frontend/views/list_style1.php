<div class="box-news">
    <div class="main-box-head">
        <h2><span><a><?php echo $this->title ?></a></span></h2>
        <small><?php echo $this->title ?></small>
    </div>
    <div class="new-focus-bg">
        <ul class="new-focus">
            <!--
            //Thêm hàm if để chọn fisrt and last for li class
            -->
            <?php foreach ($data as $key => $value): ?>
                <?php
                if ($value['image']) {
                    $a_image = Common::getImageUploaded('article/small/' . $value['image']);
                } else {
                    $a_image = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
                }
                ?>
                <li <?php if ($key == 0): ?>class="first"<?php elseif ($key == 4): ?>class="last"<?php endif; ?>>
                    <a href="<?php echo $value['url']; ?>" title="<?php echo $value['title']; ?>"><img src="<?php echo $a_image; ?>" alt="<?php echo $value['title']; ?>"/></a>
                    <a href="<?php echo $value['url']; ?>" class="title2" title="<?php echo $value['title']; ?>"><?php echo $value['title']; ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="clear">
        </div>
    </div>
</div>