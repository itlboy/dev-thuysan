<style>
    .bg_2 .list_2 a.row_a:hover .row {background:#fab10c;}
    .border_2 {border:1px #f6b84e solid; border-width: 4px 1px;}
    .underline_2 {background:url('<?php echo Yii::app()->theme->baseUrl . '/images/underline_2.png'; ?>') left bottom repeat-x; padding-bottom:10px;}

    .list_1 {overflow:hidden; padding-top:5px;}
    .list_1 .row {overflow:hidden; width:200px; height: 260px; margin:0 20px 0 0; overflow: hidden;}
    .list_1 .row img {width:190px; height: 250px;}
    .list_1.margin2 .row {margin:0 27px 0 0;}
    .list_1 .row.last {margin:0 0 0 0;}
    .list_1 .row.center {margin:0 auto;}
    .list_1 .thumb {padding:3px; text-align:center;}
    .list_1 .row .title {text-align:center; overflow:hidden; margin-bottom:3px;}

    .list_2 {overflow:hidden;}
    .list_2 .row {overflow:hidden;}
    .list_2 a.row_a .row {padding:5px;}
    .list_2 a.row_a:hover .row {background:#b8d1ff;}
    .bg_2 .list_2 a.row_a:hover .row {background:#fab10c;}
    .list_2 .row.last {margin:0 0 0 0;}
    .list_2 .row.center {margin:0 auto;}
    .list_2 .thumb {float:left; padding:3px; text-align:center; width:45px;}
    .list_2 .row .title, .list_2 .row .star {width: 225px; margin-left:60px; overflow:hidden; margin-bottom:3px; font-size:1.3em;}

    .divider3 {background:url('<?php echo Yii::app()->theme->baseUrl . '/images/divider3.png'; ?>') center top no-repeat; height:28px; margin:0px 0 5px 0;}

    .border_2 {border-color: #f6b84e;}
    .bg_2 {background:#fed56b;}
    .thumb {background:#fcfbfb; border:1px #b7b7b7 solid; border-width: 3px 1px 1px 1px;}
    .list_1 .row .title, .list_1 .row .title a {font:bold 11px Tahoma, Geneva, sans-serif; color:#333333;}
    .list_1 .row .title a:hover {color:#003399;}

    .list_2 .row .title, .list_2 .row .title a {font:bold 11px Tahoma, Geneva, sans-serif; color:#333333;}
    .list_2 .row .title a:hover {color:#003399;}
    .underline_2, .underline_2 a {color:#7f5c22; text-align: center;}
</style>
<div class="bg_2 border_2 margin_top" style="margin-bottom: 5px;">
    <div style="margin:5px 20px 20px 20px;">
        <!-- Bar -->
        <div class="bar underline_2">
            <h2 class="fl"><a href="javascript:void(0);" rel="nofollow"><?php echo $this->title ?></a></h2>
            <div class="nguago"></div>
        </div>
        <!-- END Bar -->

        <?php $id = 'id_'.rand(10000,90000); ?>
        <!-- Items List -->
        <div class="list_1">
            <div class="row center">
                <div class="shadow"><div class="shadow_r">
                        <div class="thumb"><img id="<?php echo $id; ?>" class="loaded2" src="<?php echo Common::getImageUploaded('article/large/' . $data[0]['image']); ?>" style="display: inline; "></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Items List -->

        <div class="divider3" style="margin:5px 0 0 0;"></div>
        <!-- Items List -->
        <div class="list_2">
            <!-- S-loop -->
            <?php foreach ($data as $key => $value): ?>
            <a href="<?php echo $value['url']; ?>" class="row_a" title="<?php echo $value['title']; ?>" onmouseover="$('#<?php echo $id; ?>').attr('src', '<?php echo Common::getImageUploaded('article/large/' . $value['image']); ?>');">
                        <div class="row">
                            <div class="thumb"><img class="loaded2" src="<?php echo Common::getImageUploaded('article/small/' . $value['image']); ?>" width="43" height="55" alt="<?php echo $value['title']; ?>" original="<?php echo Common::getImageUploaded('article/small/' . $value['image']); ?>" style="display: inline; "></div>
                            <div class="title" style="width: 185px;"><?php echo $value['title']; ?></div>
                            <div class="star" style="width: 185px; font: normal 11px Arial;"><?php echo letText::limit_chars($value['intro'], 60); ?></div>
                            <div class="star" style="width: 185px;">
                                <div class="off"><div class="on" style="width: 96%;"></div></div>
                            </div>
                            <div class="nguago"></div>
                        </div>
                    </a>
            <?php endforeach; ?>
            <!-- E-loop --> 
        </div>
    </div>
    <!-- END Items List -->
</div>