<div class="box-news">
    <div class="box-news-content">
        <div class="main-cat-contentleft">
            <ul class="mycontent">
                <?php foreach ($data as $item): ?>
                <?php
                if ($item['image']) {
                    $image_no1 = Common::getImageUploaded('article/medium/' . $item['image']);
                } else {
                    $image_no1 = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
                }

                $day = '';
                switch(date('l', strtotime ($item["created_time"])))
                {
                    case 'Monday':
                        $day = 'Thứ 2';
                        break;
                    case 'Tuesday':
                        $day = 'Thứ 3';
                        break;
                    case 'Wednesday':
                        $day = 'Thứ 4';
                        break;
                    case 'Thursday':
                        $day = 'Thứ 5';
                        break;
                    case 'Friday':
                        $day = 'Thứ 6';
                        break;
                    case 'Saturday':
                        $day = 'Thứ 7';
                        break;
                    case 'Sunday':
                        $day = 'Chủ nhật';
                        break;
                } ?>
                <li>
                    <a title="<?php echo $item['title'] ?>" href="<?php echo $item['url'] ?>"><img class="bo" title="<?php echo $item['title'] ?>" alt="<?php echo $item['title'] ?>" src="<?php echo $image_no1 ?>"></a>
                    <a class="title video" title="<?php echo $item['title'] ?>" href="<?php echo $item['url'] ?>"><?php echo $item['title'] ?></a>
                    <span class="time"><?php echo $day.', '.date('d/m/Y H:i:s', strtotime ($item["created_time"])) . ' GMT+7'; ?></span>
                    <p><?php echo $item['intro']; ?></p>
                    <div class="clear"></div>
                </li>

                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>