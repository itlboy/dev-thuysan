<?php
if ($rows[0]['image']) {
    $image_l = Common::getImageUploaded('article/large/' . $rows[0]['image']);
} else {
    $image_l = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
}
?>

        <div class="box-news-content home-du-lich">
            <div class="box-style-5">	                    
                <div class="slider">
                    <ul>                    
                        <li>
                            <a href="<?php echo $rows[0]['url']; ?>" title="<?php echo $rows[0]['title']; ?>"><img class="bo" src="<?php echo $image_l; ?>" alt="<?php echo $rows[0]['title']; ?>" width="309" height="227"></a>
                            <p><a href="<?php echo $rows[0]['url']; ?>" class="title" title="<?php echo $rows[0]['title']; ?>"><?php echo $rows[0]['title']; ?></a></p>
                        </li>                          
                    </ul>                    
                </div>
<?php if (isset($rows[1])) : ?>
                <div class="news-other">
                    <ul class="others">
        <?php foreach ($rows as $key => $article): ?>
            <?php if ($key >= 1 AND $key < 4): ?>
                        
<?php
if ($article['image']) {
    $image = Common::getImageUploaded('article/small/' . $article['image']);
} else {
    $image = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
}
?>
                        <li<?php if ($key == 1): ?> class="first"<?php endif; ?>>
                            <a href="<?php echo $article['url']; ?>" title="<?php echo $article['title']; ?>"><img class="bo img_lazy" src="<?php echo $image; ?>" alt="<?php echo $article['title']; ?>" width="63" height="63" original="<?php echo $image; ?>" style="display: block; "></a>
                            <a href="<?php echo $article['url']; ?>" class="title2" title="<?php echo $article['title']; ?>"><?php echo $article['title']; ?></a>
                        </li>
            <?php endif; ?>
        <?php endforeach; ?>
                    </ul>
                </div>
<?php endif; ?>
                <div class="clear"></div>
            </div> 

        </div>
