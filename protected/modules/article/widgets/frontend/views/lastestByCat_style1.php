        <div class="box-news-content">	
            <div class="box-style-1">	
                <ul class="style">
                    <li>
                        <?php
                        if ($rows[0]['image']) {
                            $image_l = Common::getImageUploaded('article/medium/' . $rows[0]['image']);
                        } else {
                            $image_l = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
                        }
                        if (isset($rows[1]) && $rows[1]['image']) {
                            $image_r = Common::getImageUploaded('article/medium/' . $rows[1]['image']);
                        } else {
                            $image_r = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
                        }
                        ?>
                        <a title="<?php echo $rows[0]['title']; ?>" href="<?php echo $rows[0]['url']; ?>">
                            <img width="132" height="97" alt="<?php echo $rows[0]['title']; ?>" src="<?php echo $image_l; ?>" class="bo img_lazy" original="<?php echo $image_l; ?>" style="display: inline;">
                        </a>
                        <a title="<?php echo $rows[0]['title']; ?>" class="title" href="<?php echo $rows[0]['url']; ?>"><?php echo $rows[0]['title']; ?></a>
                        <p><?php echo letText::limit_chars($rows[0]['intro'], 100); ?></p>
                    </li>
    <?php if (isset($rows[1])) : ?>
                        <li>
                            <a title="<?php echo $rows[1]['title']; ?>" href="<?php echo $rows[1]['url']; ?>">
                                <img width="132" height="97" alt="<?php echo $rows[1]['title']; ?>" src="<?php echo $image_r; ?>" class="bo img_lazy" original="<?php echo $image_r; ?>" style="display: inline;">
                            </a>
                            <a title="<?php echo $rows[1]['title']; ?>" class="title" href="<?php echo $rows[1]['url']; ?>"><?php echo $rows[1]['title']; ?></a>
                            <p><?php echo letText::limit_chars($rows[1]['intro'], 100); ?></p>
                        </li>
                <?php endif; ?>
                </ul>
    <?php if (isset($rows[2])) : ?>
                    <div class="news-other">
                        <h3>Các tin khác</h3>
                        <ul class="others">
                            <?php foreach ($rows as $key => $article): ?>
            <?php if ($key >= 2 AND $key <= 6): ?>
                            <li <?php if ($key == 4): ?>class="first"<?php elseif ($key == 6): ?>class="last"<?php endif; ?>>
                                <a title="<?php echo $article['title']; ?>" href="<?php echo $article['url']; ?>"><?php echo $article['title']; ?></a>
                            </li>
            <?php endif; ?>
        <?php endforeach; ?>
                        </ul>
                    </div>
    <?php endif; ?>
                <div class="clear"></div>
            </div>        
        </div>
