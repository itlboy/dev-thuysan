<?php $i = 0; ?>
<div class="main-cat-focus">
    <div class="main-cat-on">
        <?php foreach($data as $item): ?>
            <?php if($i == 0): ?>
                <?php
                    $day = '';
                    switch(date('l', strtotime ($item["created_time"])))
                    {
                        case 'Monday':
                            $day = 'Thứ 2';
                            break;
                        case 'Tuesday':
                            $day = 'Thứ 3';
                            break;
                        case 'Wednesday':
                            $day = 'Thứ 4';
                            break;
                        case 'Thursday':
                            $day = 'Thứ 5';
                            break;
                        case 'Friday':
                            $day = 'Thứ 6';
                            break;
                        case 'Saturday':
                            $day = 'Thứ 7';
                            break;
                        case 'Sunday':
                            $day = 'Chủ nhật';
                            break;
                    }
                ?>
                <a title="<?php echo $item['title'] ?>" href="<?php echo $item['url']; ?>"><img style="margin-right: 10px;" title="<?php echo $item['title']; ?>" alt="<?php echo $item['title'] ?>" src="<?php echo Common::getImageUploaded('article/origin/'.$item['image']); ?>" class="bo"></a>
                <div class="sapo" style=" padding-right: 10px; color: #333">
                    <a title="<?php echo $item['title']; ?>" class="title" href="<?php echo $item['url']; ?>"><?php echo $item['title'] ?></a>
                    <span class="time"><?php echo $day.', '.date('d/m/Y H:i:s', strtotime ($item["created_time"])) . ' GMT+7'; ?></span>
                    <p style="text-align: justify"><?php echo $item['intro']; ?></p>

                </div>
            <?php   $i++;
                    break;
                endif;

            ?>
        <?php endforeach; ?>
        <div class="clear"></div>
    </div>


    <div class="main-cat-onother">
        <ul>
        <?php foreach($data as $item): ?>
            <?php if($i > 1): ?>
                <li>
                    <a title="<?php echo $item['title']; ?>" href="<?php echo $item['url']; ?>"><img title="<?php echo $item['title'] ?>" alt="<?php echo $item['title']; ?>" style="max-height: 100px;" width="132px" src="<?php echo Common::getImageUploaded('article/large/'.$item['image']); ?>" class="bo"></a>
                    <a class="title" title="<?php echo $item['title']; ?>" href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a>
                </li>
            <?php endif; ?>
            <?php $i++; ?>
        <?php endforeach; ?>
        </ul>
        <div class="clear"></div>
    </div>
</div>