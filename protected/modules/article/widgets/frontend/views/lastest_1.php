<div class="u-others">
    <div class="u-others-items">
        <ul>
            <?php foreach ($data as $key => $value): ?>
                <li><a href="<?php echo $value['url']; ?>" title="<?php echo $value['title']; ?>"><?php echo $value['title']; ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
