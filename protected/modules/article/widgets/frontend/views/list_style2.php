<div class="let_slider">
    <div class="let_slider_inner clearfix" id="let_carousel">
        <ul class="clearfix">
            <?php foreach ($data as $key => $value): ?>
                <li class="let_item">
                    <div class="let_item_inner">
                        <div class="let_location">&nbsp;</div>
                        <a href="<?php echo $value['url']; ?>"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/samples/slider1.jpg" alt="new-right.jpg" /></a>
                        <a href="<?php echo $value['url']; ?>"><?php echo $value['title']; ?></a>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <a href="javascript:void(0)" class="let_slider_prev"></a>
    <a href="javascript:void(0)" class="let_slider_next"></a>
</div>
