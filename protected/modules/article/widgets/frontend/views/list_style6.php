<div class="main-box">
    <div class="main-box-head">
        <h2><span><a href="#" title="<?php echo $this->title ?>"><?php echo $this->title ?></a></span></h2>
        <small><?php echo $this->title ?></small>  
    </div>
    <div class="main-content-navbar">
        <ul class="mycontent">
            <?php foreach ($data as $key => $value): ?>
                <?php
                if ($data[0]['image']) {
                    $a_image = Common::getImageUploaded('article/medium/' . $data[0]['image']);
                } else {
                    $a_image = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
                }
                ?>
                <li>
                    <?php if ($key == 0): ?>
                        <a href="<?php echo $data[0]['url']; ?>" title="<?php echo $data[0]['title']; ?>"><img src="<?php echo $a_image; ?>" alt="<?php echo $data[0]['title']; ?>" class="bo"></a>
                        <a href="<?php echo $data[0]['url']; ?>" title=<?php echo $data[0]['title']; ?>" class="title video"><?php echo $data[0]['title']; ?></a>
                        <p><?php echo $data[0]['intro']; ?></p>
                        <div class="clear"></div>
                    <?php else: ?>

                        <a href="<?php echo $value['url']; ?>" class="x-more" title="<?php echo $value['title']; ?>"><?php echo $value['title']; ?></a>                  
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>                
</div>