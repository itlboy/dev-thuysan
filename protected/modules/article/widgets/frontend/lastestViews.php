<?php

class lastestViews extends Widget {

    public $view = '';
    public $category = NULL;
    public $title = '';
    public $limit = 10;
    public $href = '';

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
        Yii::import('article.models.KitArticle');
        Yii::import('cms.models.KitStats');
    }

    public function run() {

        $result = KitStats::getLastest('article',2,$this->category,$this->limit);
        if(empty($result))
            return FALSE;
        $articles = array();
        foreach($result as $item){
            $articles[] = $item['item_id'];
        }
        // Lay danh sach tin

        $criteria = new CDbCriteria();
        $criteria->select = Common::getFieldInTable(KitArticle::model()->getAttributes(), 't.');
        $criteria->condition = '';
        $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
        $criteria->condition = Common::addWhere($criteria->condition, 't.id IN ('.implode(',', $articles).')');
        $criteria->order = 'FIELD(t.id,'.implode(',',$articles).')';
        $result = KitArticle::model()->findAll($criteria);
        $result = KitArticle::treatment($result);

        $this->render($this->view, array(
            'data' => $result,
        ));
    }

}