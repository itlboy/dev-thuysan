<div class="box grid_16" style="margin-top: 10px;">
    <div class="toggle_container">
        <div class="block">
            <h2 class="section"><?php echo Yii::t('global', 'General'); ?></h2>

            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'title'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'title'); ?>
                    <?php echo $form->error($model, 'title'); ?>
                    <div class="required_tag tooltip hover left" title="<?php echo Yii::t('global', 'Required'); ?>"></div>
                </div>
            </fieldset>
                                    
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'focus'); ?></label>
                <div>
                    <div class="jqui_radios">
                        <?php echo $form->radioButtonList($model, 'focus', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                        <?php echo $form->error($model, 'focus'); ?>
                    </div>
                </div>
            </fieldset>
                                    
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'has_brand'); ?></label>
                <div>
                    <div class="jqui_radios">
                        <?php echo $form->radioButtonList($model, 'has_brand', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                        <?php echo $form->error($model, 'has_brand'); ?>
                    </div>
                </div>
            </fieldset>
                                    
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'is_home'); ?></label>
                <div>
                    <div class="jqui_radios">
                        <?php echo $form->radioButtonList($model, 'is_home', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                        <?php echo $form->error($model, 'is_home'); ?>
                    </div>
                </div>
            </fieldset>
                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'author'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'author'); ?>
                    <?php echo $form->error($model, 'author'); ?>
                </div>
            </fieldset>
                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'source'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'source'); ?>
                    <?php echo $form->error($model, 'source'); ?>
                </div>
            </fieldset>
                                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'intro'); ?></label>
                <div class="clearfix">
                    <?php echo $form->textArea($model, 'intro', array('class' => 'tooltip', 'title' => 'Nhập thông tin...')); ?>
                    <?php echo $form->error($model, 'intro'); ?>
                </div>
            </fieldset>
                                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'tags'); ?></label>
                <div class="clearfix">
                    <?php echo $form->textArea($model, 'tags', array('class' => 'tooltip', 'title' => 'Nhập thông tin...')); ?>
                    <?php echo $form->error($model, 'tags'); ?>
                </div>
            </fieldset>
                                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'layout_id'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'layout_id'); ?>
                    <?php echo $form->error($model, 'layout_id'); ?>
                </div>
            </fieldset>
                                                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'categories'); ?></label>
                <div>
                    <?php
                    echo $form->checkBoxList($model, 'categories', CHtml::listData(KitCategory::model()->getCategoryOptions($this->module->id), 'id', 'name'), array(
                        'separator' => '',
                        'template' => '<div>{input} {label}</div>'));
                    ?>
                    <?php echo $form->error($model, 'categories'); ?>
                </div>
            </fieldset>

        </div>
    </div>
</div>

<div class="box grid_16 round_all">
    <div class="toggle_container">
        <div class="block">
            <h2 class="section">Đặt giờ xuất bản</h2>
            <div class="columns clearfix">
                <div class="col_50">
                    <fieldset class="label_side">
                        <label><?php echo $form->labelEx($model, 'from_time'); ?></label>
                        <div>
                            <?php echo $form->textField($model, 'from_time', array('class' => 'datepicker')); ?>
                            <?php echo $form->error($model, 'from_time'); ?>
                        </div>
                    </fieldset>
                </div>
                <div class="col_50">
                    <fieldset class="label_side">
                        <label><?php echo $form->labelEx($model, 'to_time'); ?></label>
                        <div>
                            <?php echo $form->textField($model, 'to_time', array('class' => 'datepicker')); ?>
                            <?php echo $form->error($model, 'to_time'); ?>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="box grid_16">
    <div class="toggle_container">
        <div class="block">
            <div class="columns clearfix">
                <div class="col_25">
                    <fieldset>
                        <label><?php echo $form->labelEx($model, 'sorder'); ?></label>
                        <div>
                            <?php echo $form->textField($model, 'sorder', array('class' => 'tooltip autogrow', 'title' => 'Số càng nhỏ thì thứ tự càng cao. Mặc định: 500', 'placeholder' => 'Nhập 1 số')); ?>
                            <?php echo $form->error($model, 'sorder'); ?>
                        </div>
                    </fieldset>
                </div>
                <div class="col_25">
                    <fieldset>
                        <label><?php echo $form->labelEx($model, 'promotion'); ?></label>
                        <div>
                            <div class="jqui_radios">
                                <?php echo $form->radioButtonList($model, 'promotion', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                                <?php echo $form->error($model, 'promotion'); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col_25">
                    <fieldset>
                        <label><?php echo $form->labelEx($model, 'status'); ?></label>
                        <div>
                            <div class="jqui_radios">
                                <?php echo $form->radioButtonList($model, 'status', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                                <?php echo $form->error($model, 'status'); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col_25">
                    <fieldset>
                        <label><?php echo $form->labelEx($model, 'trash'); ?></label>
                        <div>
                            <div class="jqui_radios">
                                <?php echo $form->radioButtonList($model, 'trash', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                                <?php echo $form->error($model, 'trash'); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="box grid_16">
<!--    --><?php //echo $form->error($model,'content'); ?>
<!--    --><?php //echo $form->textArea($model, 'content', array('class' => 'tinymce')); ?>
<!--    --><?php //$this->widget('ext.richtext.TinyMCE'); ?>
    <?php echo $form->error($model,'content'); ?>
    <?php        //echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50, 'class' => 'tinymce'));
    $this->widget('ext.elrtef.elRTE', array(
        'model' => $model,
        'attribute' => 'content',
        'options' => array(
            'doctype'=>'js:\'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\'',
            'cssClass' => 'el-rte',
            'cssfiles' => array('css/elrte-inner.css'),
            'absoluteURLs'=>true,
            'allowSource' => true,
            'lang' => 'vi',
            'styleWithCss'=>'',
            'height' => 400,
            'fmAllow'=>true, //if you want to use Media-manager
            'fmOpen'=>'js:function(callback) {$("<div id=\"elfinder\" />").elfinder(%elfopts%);}',//here used placeholder for settings
            'toolbar' => 'maxi',
        ),
        'elfoptions' => array( //elfinder options
            'url'=>'auto',  //if set auto - script tries to connect with native connector
            'passkey'=>'mypass', //here passkey from first connector`s line
            'lang'=>'vi',
            'dialog'=>array('width'=>'900','modal'=>true,'title'=>'Media Manager'),
            'closeOnEditorCallback'=>true,
            'editorCallback'=>'js:callback'
        ),
    ));
    ?>
</div>
