<?php
/**
 * Module: article
 * Auth:
 * Date: 2012-03-16 11:24:06
 */
class DefaultController extends ControllerFrontend
{
	public function actionIndex()
	{
        $id = $_GET['id'];
        if(!$category = KitCategory::model()->findByPk($id)) throw new CHttpException(404,'Not found.');
		$this->render('index');
	}
}