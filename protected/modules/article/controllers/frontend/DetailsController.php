<?php
/**
 * Module: article
 * Auth:
 * Date: 2012-03-16 11:24:06
 */
Yii::import('application.modules.cms.models.KitStats');
class DetailsController extends ControllerFrontend
{
	public function actionIndex()
	{
        $id = Yii::app()->request->getParam('id', NULL);
//        $assign['jgjh'] =  $this->_getCategory($id);
        $result = KitArticleCategory::checkCategory($id);
        $arrCategory = array();
        if($result !== NULL){
            foreach($result as $item )
            {
                $arrCategory[] = $item->attributes["category_id"];
            }
        }

        $cache_name = md5(__METHOD__ . '_' . $id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
//            if(!$assign['data'] = KitArticle::model()->findByPk($id)) throw new CHttpException(404,'Not found.');
            // Get details

            $assign['data'] = KitArticle::getDetails($id);
            if(!empty($assign['data']) AND ($assign['data']->status == 1) AND ($assign['data']->trash ==0)){
                $assign['data'] = KitArticle::treatment($assign['data']);
                $assign['user'] = KitAccount::model()->findByPk($assign['data']['creator']);
                $assign['user'] = CJSON::decode(CJSON::encode($assign['user']));
            } else {
                $this->redirect(Yii::app()->getBaseUrl(true));
            }
//            // Category
//            $categorys = $assign['data']->kitArticleCategory;
//            foreach ($categorys as $category) {
//                $assign['breadcrumb'][] = KitCategory::getBreadcrumb($category->id);
//            }
            
            $assign['data'] = KitArticle::treatment($assign['data']);
            $assign['arrCategory'] = $arrCategory ;
            Yii::app()->cache->set($cache_name, $assign); // Set cache
        } else $assign = $cache;
        // Meta
        if (isset($assign['data']['title'])) $this->pageTitle = $assign['data']['title'];
        if (isset($assign['data']['intro'])) Yii::app()->clientScript->registerMetaTag($assign['data']['intro']);

        if(isset($assign['data']['id']))
            KitStats::updateValue($assign['data']['id'],'article');
        //thong ke
        if(isset($assign['data']['id'])){
            $assign['stats'] = KitStats::getDetails('article',$assign['data']['id']);
            KitStats::setStats('article',$id);
        }


        // Lay bo dem so nguoi xem
//        $assign['view_count'] = KitStats::model()->find('item_id=:itemId',array(':itemId' => $id));

        $this->render('index', $assign);
	}
	
    public function action(){
        return array('captcha' => array(
            'class' => 'CcaptchaACtion',
            'backColor' => '0xffffff',
            'minLength' => '2',
            'maxLength' => '3',
        ));
    }
    public function _getCategory($article_id){
        $data = KitArticleCategory::model()->findAll('article_id=:articleId',array(':articleId' => $article_id));
        $category = array();
        if(!empty($data)){
            foreach($data as $category_article){
                $category[] = KitCategory::model()->findByPk($category_article['category_id']);
                if(!empty($category)){
                    foreach($category as $category_parent){
                        if($category_parent['parent_id'] != $category_parent['id']){
                            $category_parent1['id'] = $category_parent['parent_id'];

                        }
                    }
                }
            }
        }
    }
}