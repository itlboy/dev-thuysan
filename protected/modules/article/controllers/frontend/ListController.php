<?php

class ListController extends ControllerFrontend {

    public function actionIndex() {
        $assign['category'] = letArray::get($_GET, 'id', NULL);
        $assign['details'] = KitCategory::getDetails($assign['category']);
        $assign['details'] = KitCategory::treatment($assign['details']);

        // phân trang
        $page = '';
        if(isset($_GET['page'])){
            $page = letArray::get($_GET, 'page');
        }
        $limit = 20;
        $link = Yii::app()->createUrl('//article/frontend/list');
        if(!empty($page)){
            $limit_wg = $limit.','.$page * $limit;
            $assign['old'] = Yii::app()->createUrl('//article/frontend/list', array('page' => $page +1));
            if(($page - 1) != 0){
                $assign['new'] = Yii::app()->createUrl('//article/frontend/list', array('page' => $page -1));
            } else {
                $assign['new'] = $link;
            }
            $link = Yii::app()->createUrl('//article/frontend/list', array('page' => $page));
        } else {
            $limit_wg = $limit;
            $assign['old'] = Yii::app()->createUrl('//article/frontend/list', array('page' => $page +1));
        }

        $assign['limit_wg'] = $limit_wg;
        
        // Children
        $result = KitCategory::getListInParent('article', $assign['category']);
        $assign['category_child'] = KitCategory::treatment($result);

        $this->render('index', $assign);
    }

}
