<?php

class AjaxController extends ControllerFrontend{

    public function actionRating(){
        $id = letArray::get($_GET,'id');
        $model = KitArticle::model()->find('id=:id',array(':id' => $id));
        if(empty($model)){
            return false;
        }
        $aver = (@round($model->rate_sum  / $model->rate_count,1)) * 20;
        $tdtb = round($model->rate_sum / $model->rate_count,1);
        echo json_encode(array(

            'aver'=>$aver,
            'mess'=>" Số lần đánh giá:<span style='font-weight: bold;color: red'>$model->rate_count </span>! Điểm <span style='font-weight: bold;color: red'>$tdtb/5</span>",
        ));
    }
    public function actionRate(){
        $id = letArray::get($_GET,'id');
        $rating = letArray::get($_GET,'rating');
        $model = KitArticle::model()->find('id=:id',array(':id' => $id));
        $model->rate_sum = $model->rate_sum + $rating;
        $model->rate_count = $model->rate_count + 1;
        if($model->save()){
        }else{
            Yii::app()->user->setFlash('error','Rate không thành công');
        }
    }

}
?>