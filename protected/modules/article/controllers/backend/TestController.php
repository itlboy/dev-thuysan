<?php

class TestController extends ControllerBackend
{
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id) {
		$model=KitArticle::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

    public function actionIndex() {
        $model = new KitArticle('search');
        $model->unsetAttributes();  // clear any default values
        
        // Category
        if (Yii::app()->request->getQuery('category_id') !== NULL) {
            $model->category_id = Yii::app()->request->getQuery('category_id');
        }
        
        if (isset($_GET['KitArticle'])) {
            $model->attributes = $_GET['KitArticle'];
        }

        // page size drop down changed
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$model = new KitArticle;
        
        $categories = array();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$this->save($model);

		$this->render('edit',array(
			'model'=>$model,
            'categories' => $categories,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id) {
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

        $this->save($model);

		$this->render('edit',array(
			'model' => $model,
            'categories' => $categories,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionInfo($id) {
        $model = KitArticle::getDetails($id);
        
		$this->render('info',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	protected function save($model) {
		if(isset($_POST['KitArticle'])) {
            // Upload image
			if ($_POST['KitArticle']['image'] !== NULL AND $_POST['KitArticle']['image'] !== '') {
                $_POST['KitArticle']['image'] = Common::createThumb($this->module->getName() , $_POST['KitArticle']['image'], letArray::get($_POST['KitArticle'], 'title', ''));
            }
            
			if ($_POST['KitArticle']['from_time'] == NULL OR $_POST['KitArticle']['from_time'] == '') {
                unset($_POST['KitArticle']['from_time']);
            }

            if ($_POST['KitArticle']['to_time'] == NULL OR $_POST['KitArticle']['to_time'] == '') {
                unset($_POST['KitArticle']['to_time']);
            }
            
            $model->attributes = $_POST['KitArticle'];

            $categoriesNew = letArray::get($_POST['KitArticle'], 'categories', array());
            $categories = KitArticleCategory::model()->getArticle2Category($model->id);

            if ($model->validate()) {
                if($model->save()) {
                    $itemId = $model->id;
                    if ($model->isNewRecord)
                        KitArticleCategory::deleteArticleCategory($categories, $itemId);
                    KitArticleCategory::createArticleCategory($categoriesNew, $itemId);

                    if (Yii::app()->request->getPost('apply') == 0)
                         $this->redirect(array('index'));
                    else $this->redirect(array('update', 'id' => $model->id));
                }
            }
		}
	}
    
	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model) {
		if(isset($_POST['ajax']) && $_POST['ajax']==='kit-article-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
