<?php

class ArticleModule extends BaseModule
{
    public $category = TRUE; // Xác định Module có category hay không
    public $option = FALSE; // Xác định Module có option hay không
    public $comment = TRUE; // Xác định Module có dùng comment hay không
    public $media = FALSE; // Xác định Module có dùng media hay không
    public $menuList = array(
        array(
            'name' => 'Tạo danh mục',
            'url' => '/category/default/create/module/article',            
            'icon' => '/images/icons/small/grey/coverflow.png',
            'type' => 'dialog',
            'grid' => '',
            'items' => array(
                array(
                    'name' => 'Quản lý danh mục',
                    'url' => 'category/default/index/module/article',
                    'icon' => '/images/icons/small/grey/coverflow.png',
                    'type' => 'direct',
                    'items' => array(
                        array(
                            'name' => 'Quản lý danh mục',
                            'url' => '/category/default/index/module/article',
                            'icon' => '/images/icons/small/grey/coverflow.png',
                            'type' => 'direct',
                        )
                    )
                )
            )
        ),
        array(
            'name' => 'Quản lý danh mục',
            'url' => '/category/default/index/module/article',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Quản lý danh mục bài viết',
            'type' => 'direct'
        ),
        array(
            'name' => 'Tạo bài viết',
            'url'  => '/article/default/create',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Tạo mới bài viết',
            'type' => 'direct'
        ),
        array(
            'name' => 'Quản lý bài viết',
            'url'  => '/article/default/index',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Quản lý bài viết',
            'type' => 'direct'
        ),
    );
    public $image = array(
        'origin' => array(
            'type' => 'move',
            'folder' => 'origin',
        ),
        'large' => array(
            'type' => 'resize',
            'width' => 350,
            'height' => 255,
            'cropWidth' => NULL,
            'cropHeight' => NULL,
            'cropTop' => NULL,
            'cropLeft' => NULL,
            'rotate' => 0, // Xoay anh
            'flip' => NULL, //[integer] Direction. Horizontal = 5, Vertical = 6
            'quality' => 100,
            'folder' => 'large',
        ),
        'medium' => array(
            'type' => 'resize',
            'width' => 150,
            'height' => 150,
            'cropWidth' => NULL,
            'cropHeight' => NULL,
            'cropTop' => NULL,
            'cropLeft' => NULL,
            'rotate' => 0, // Xoay anh
            'flip' => NULL, //[integer] Direction. Horizontal = 5, Vertical = 6
            'quality' => 100,
            'folder' => 'medium',
        ),
        'small' => array(
            'type' => 'crop',
            'width' => 75,
            'height' => 75,
            'cropWidth' => NULL,
            'cropHeight' => NULL,
            'cropTop' => NULL,
            'cropLeft' => NULL,
            'rotate' => 0, // Xoay anh
            'flip' => NULL, //[integer] Direction. Horizontal = 5, Vertical = 6
            'quality' => 100,
            'folder' => 'small',
        ),
    );

    public function init() {
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'article.models.*',
			'article.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
