<?php
return array (
  'moduleName' => 'mail',
  'tablePrefix' => 'let_',
  'main_tableName' => 'let_kit_mail_sender',
  'main_controller' => 'sender',
  'main_baseClass' => 'ActiveRecord',
  'hasCategory' => '0',
  'hasOption' => '0',
  'hasComment' => '0',
  'hasMedia' => '0',
  'hasChildren' => '0',
  'children_tableName' => 'let_kit_film_episode',
  'children_controller' => 'episode',
  'children_baseClass' => 'ActiveRecord',
  'children_fieldCreate' => 'title,sorder,link,status',
  'children_fieldUpdate' => 'title,sorder,link,status,intro,hot,promotion,status,trash',
);