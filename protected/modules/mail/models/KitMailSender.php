<?php
Yii::import('application.modules.mail.models.db.BaseKitMailSender');
Yii::import('application.extensions.mail.ESmtp');
class KitMailSender extends BaseKitMailSender{
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public static function getList()
    {
        $cache_name = md5('mail_sender_getlist');
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE OR TRUE){
            $sender = self::model()->findAll('count_today < :countToday AND status = 1',array(
                ':countToday' => Yii::app()->params['mail_sender_count_today'],
            ));
            if(empty($sender))
                return FALSE;
            $sender = CJSON::decode ((CJSON::encode($sender)));
            Yii::app()->cache->set($cache_name,$sender,(60*15));
        } else $sender = $cache;

        return $sender;
    }

    public static function selectSender(){
        // get sender list
        $sender = self::getList();

        $senderTotal = count($sender);

        // lay key cua sender hien tai trong mail list
        $cache_name = 'mail_sender_current_sender';
        $cache = Yii::app()->cache->get($cache_name); // Get cache

        if ($cache === FALSE OR (int)$cache >= ($senderTotal-1)) {
            $currentSenderKey = 0;
        } else $currentSenderKey = (int)$cache + 1;
        return $sender = array(
            'key' => $currentSenderKey,
            'data' => $sender[$currentSenderKey],
        );
    }

    /**
     * @static Send mail
     * @param array $data
     * @param string $type 'smtp' OR 'mail'
     */
    public static function sendMail($params = array(), $type = 'smtp', $data = array()){
        $sender = self::selectSender();

        Yii::app()->cache->set('mail_sender_current_sender', $sender['key']);
        
        $from = letArray::get($sender['data'], 'email', '');
        $password = letArray::get($sender['data'], 'password', '');
        
        if(isset($params['email_code']) && !empty($params['email_code'])){
            // Get email template
            $emailTemp = KitEmailTemplate::model()->find("code = '{$params['email_code']}' AND status = " . KitEmailTemplate::STATUS_ENABLED);
            if($emailTemp){
                $to = $params['to_email'];
                $subject = $emailTemp->subject;
                
                //Build body
                $html = $emailTemp->body; //HTML Body
                if ($params && is_array($params)) {
                    foreach ($params as $key => $value) {
                        $html = str_replace("{{$key}}", $value, $html);
                    }
                }
                
                $text = $emailTemp->title;
                $extra = NULL;
            }
            
        }else{
            $to = letArray::get($data, 'to', array());
            $subject = letArray::get($data, 'subject', '');
            $html = letArray::get($data, 'html', ''); //HTML Body
            $text = letArray::get($data, 'text', ''); //Text Body
            $extra = letArray::get($data, 'extra', NULL);
        }
        
        if($type == 'smtp'){
            $mail = new ESmtp;
            $mail->setServer(Yii::app()->params['mail_smtp_server'], Yii::app()->params['mail_smtp_port'], $from, $password);
            $mail->setSender(Yii::app()->params['mail_fake'], Yii::app()->params['mail_fake_name']);
            if($mail->send($subject, $html, $text, $to, $extra)){
                return TRUE;
            }
            else{
                //$msg = "Send mail to {$to[0]} error (" . date('d-m-Y H:i:s') . ') ' . $subject . '<br/>' . $html . '<br/><hr/>';
                //Yii::log($msg, 'error');
                return FALSE;
            }
        } else return FALSE;
    }
}