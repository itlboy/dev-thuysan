<?php

Yii::import('application.modules.mail.models.db.BaseKitEmailTemplate');
class KitEmailTemplate extends BaseKitEmailTemplate
{
    var $className = __CLASS__;
    
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}