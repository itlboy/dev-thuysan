
<div class="box grid_16" style="margin-top: 10px;">
    <div class="toggle_container">
        <div class="block">
            <h2 class="section"><?php echo Yii::t('global', 'General'); ?></h2>
            
                
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'title'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'title'); ?>
                    <?php echo $form->error($model, 'title'); ?>
                </div>
            </fieldset>
                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'email'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'email'); ?>
                    <?php echo $form->error($model, 'email'); ?>
                </div>
            </fieldset>
                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'password'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'password'); ?>
                    <?php echo $form->error($model, 'password'); ?>
                </div>
            </fieldset>
                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'count'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'count'); ?>
                    <?php echo $form->error($model, 'count'); ?>
                </div>
            </fieldset>
                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'count_today'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'count_today'); ?>
                    <?php echo $form->error($model, 'count_today'); ?>
                </div>
            </fieldset>
                                        

        </div>
    </div>
</div>



