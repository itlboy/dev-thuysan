<li data-role="list-divider"><?php echo $this->view_title; ?></li>
<?php foreach ($data as $item): ?>
<li>
    <a href="<?php echo $item['url']; ?>">
        <img src="<?php echo Common::getImageUploaded('game/medium/' . $item['image']); ?>" alt="<?php echo $item['title']; ?>" style="width:100%" />
        <h3><?php echo $item['title']; ?></h3>
        <p><?php echo $item['intro']; ?></p>
    </a>
    <a href="#purchase" data-rel="popup" data-position-to="window" data-transition="pop" data-icon="arrow-d">Tải game này</a>
</li>
<?php endforeach; ?>	
        