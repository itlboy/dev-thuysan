<div data-role="content" data-theme="c">	
    <div class="content-primary">
    <form method="post">
        <input type="search" name="keyword" id="search-basic" value="<?php echo $this->keyword; ?>" />
        <input type="submit" value="Tìm" />
    </form>
    <ul data-role="listview" data-inset="true" data-split-icon="gear">
        <?php foreach ($data as $item): ?>
        <li>
            <a href="<?php echo $item['url']; ?>">
                <?php /* <img src="<?php echo Yii::app()->theme->baseUrl; ?>/plugins/jquery.mobile/demos/_assets/img/album-bb.jpg" /> */ ?>
                <img src="<?php echo Common::getImageUploaded('game/medium/' . $item['image']); ?>" alt="<?php echo $item['title']; ?>" style="width:100%"/>
                <h3><?php echo $item['title']; ?></h3>
                <p><?php echo $item['intro']; ?></p>
            </a>
            <a href="#purchase" data-rel="popup" data-position-to="window" data-transition="pop" data-icon="arrow-d">Tải game này</a>
        </li>
        <?php endforeach; ?>	
        
        <?php
        $this->widget('CListPager', array(
            'currentPage' => $page->getCurrentPage(),
            'itemCount' => $total,
            'pageSize' => $this->limit,
            'header' => '',
        ));
//        $this->widget('CLinkPager', array(
//            'pages' => $page,
//            'cssFile' => TRUE,
//            'header' => '',
//            'firstPageLabel' => '&lt;&lt;',
//            'prevPageLabel' => '&lt;',
//            'nextPageLabel' => '&gt;',
//            'lastPageLabel' => '&gt;&gt;',
//        ));
        ?>
    </ul>
    </div><!--/content-primary -->		

</div><!-- /content -->
