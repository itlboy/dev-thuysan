<?php

class game_list extends Widget {
    
    public $view = '';
    public $view_title = 'HOT'; // Tên group
    public $category = NULL;
    public $operating_system = NULL;    
    public $title = '';
    public $item = '';
    public $limit = 10;
    public $where = '';
    public $order_by = 'id DESC';
    public $group_count = 0;
    public $keyword = '';

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
        Yii::import('game.models.KitGame');
    }

    public function run() {        
        $pageCurrent = Yii::app()->request->getQuery('page', 0);
        $cache_name = md5(__METHOD__ . '_' . $this->category . '_' . $this->operating_system . '_' . $this->where . '_' . $this->order_by . '_' . $this->item . '_' . $this->limit . '_' . $pageCurrent . '_' . $this->keyword);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $result = array();
            
            $criteria = new CDbCriteria;
            $criteria->select = 't.*';
            $criteria->condition = 't.status = 1';

            if ($this->category !== NULL AND $this->category >= 0) {
                $catListObject = KitCategory::getListInParent('game', $this->category);
                $catList = array($this->category);
                foreach ($catListObject as $cat) {
                    $catList[] = $cat->id;
                }
                $criteria->join = "INNER JOIN {{kit_game_category}} t2 ON t.id=t2.game_id";
                $criteria->addCondition('t2.category_id IN ('.implode(',', $catList).')');
            }  
            
            if ($this->operating_system !== NULL AND $this->operating_system != 0) {
                $criteria->addCondition('t.operating_system = ' . $this->operating_system);
            }
            
            // Keyword
            if ($this->keyword !== '') {
                $keyword = addcslashes($this->keyword, '%_');
                $criteria->addCondition('title LIKE :keyword OR intro LIKE :keyword');
                $criteria->params[':keyword'] = "%$keyword%";
            }
            
            if ($this->where !== '') {
                $criteria->addCondition($this->where);
            }
			
            $criteria->order = $this->order_by;            
            
            //Paging
            $result['total'] = $count = KitGame::model()->count($criteria);
            $page = new CPagination($count);

            // results per page
            $page->pageSize = $this->limit;
            $page->applyLimit($criteria);
            
            $criteria->limit = $this->limit;            
            
            $result['page'] = $page;
            $result['data'] = KitGame::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else $result = $cache;
        
        if (!empty($result['data'])) {
            $result['data'] = KitGame::treatment($result['data']);
        }
        
        //$data = array_slice($data, 0, $this->limit);
        $this->render($this->view, array(
            'data' => $result['data'],
            'page' => $result['page'],
            'total' => $result['total']
        ));

    }

}