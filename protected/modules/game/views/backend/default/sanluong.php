<?php
$moduleName = 'game';
$modelName = 'KitGame';
$formID = 'kit_'.$moduleName.'_form';
$gridID = 'kit_'.$moduleName.'_grid';

$this->breadcrumbs = array(
    Yii::t('backend', $moduleName) => $this->createUrl('/'.$moduleName.'/default/index'),
    Yii::t('global', 'List') . ' ' . Yii::t('backend', $moduleName),
);
?>

<div class="grid_12">
    <div class="flat_area">
        <h2><?php echo Yii::t('backend', 'List'); ?> <?php echo Yii::t('backend', $moduleName); ?></h2>
    </div>
    <button class="blue small" onclick="ajaxChangeBooleanValue('<?php echo $moduleName; ?>', '<?php echo $modelName; ?>', 'status', 1, '<?php echo Yii::app()->createUrl('cms/ajax/ChangeBooleanValue'); ?>', '<?php echo $gridID; ?>');">
        <div class="ui-icon ui-icon-check"></div>
        <span><?php echo Yii::t('global', 'Publish'); ?></span>
    </button>
    <button class="light small" onclick="ajaxChangeBooleanValue('<?php echo $moduleName; ?>', '<?php echo $modelName; ?>', 'status', 0, '<?php echo Yii::app()->createUrl('cms/ajax/ChangeBooleanValue'); ?>', '<?php echo $gridID; ?>');">
        <div class="ui-icon ui-icon-cancel"></div>
        <span><?php echo Yii::t('global', 'Unpublish'); ?></span>
    </button>
    <button class="blue small" onclick="ajaxChangeBooleanValue('<?php echo $moduleName; ?>', '<?php echo $modelName; ?>', 'promotion', 1, '<?php echo Yii::app()->createUrl('cms/ajax/ChangeBooleanValue'); ?>', '<?php echo $gridID; ?>');">
        <div class="ui-icon ui-icon ui-icon-arrowthick-1-n"></div>
        <span><?php echo Yii::t('global', 'Promotion'); ?></span>
    </button>
    <button class="light small" onclick="ajaxChangeBooleanValue('<?php echo $moduleName; ?>', '<?php echo $modelName; ?>', 'promotion', 0, '<?php echo Yii::app()->createUrl('cms/ajax/ChangeBooleanValue'); ?>', '<?php echo $gridID; ?>');">
        <div class="ui-icon ui-icon-arrowthick-1-s"></div>
        <span><?php echo Yii::t('global', 'Demotion'); ?></span>
    </button>
    <button class="red small" onclick="ajaxChangeBooleanValue('<?php echo $moduleName; ?>', '<?php echo $modelName; ?>', 'trash', 1, '<?php echo Yii::app()->createUrl('cms/ajax/ChangeBooleanValue'); ?>', '<?php echo $gridID; ?>');">
        <div class="ui-icon ui-icon-trash"></div>
        <span><?php echo Yii::t('global', 'Trash'); ?></span>
    </button>
    <button class="red small" onclick="ajaxDeleteMultiRecord('<?php echo $moduleName; ?>', '<?php echo $modelName; ?>', '<?php echo Yii::app()->createUrl('cms/ajax/DeleteMultiRecord'); ?>', '<?php echo $gridID; ?>')">
        <div class="ui-icon ui-icon-close"></div>
        <span><?php echo Yii::t('global', 'Delete'); ?></span>
    </button>
    <?php    
    $this->widget('GridView', array(
        'id' => $gridID,
        'dataProvider' => $dataProvider,
        //'filter' => $model,
        'columns' => array(
            array(
                'class' => 'CCheckBoxColumn',
                'id' => 'chkIds',
                'selectableRows' => 2,
                'htmlOptions' => array('width' => '20px', 'align' => 'center')
            ),
//            array(
//                'name' => 'id',
//                'value' => '$data->id'
//            ),
                    
            array(
                'name' => 'creatorUser.username',
                'value' => '$data->creatorUser->username'
            ),
            
            array(
                'name' => 'sanluong',
                'value' => '$data->sanluong'
            ),                                                  
        ),
    ));
    ?>
</div>
<div class="grid_4 box">
    <?php    $this->widget('category.widgets.treeCategory', array(
        'module' => $moduleName,
    ));
    ?>
</div>
