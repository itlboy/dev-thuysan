<?php

class DefaultController extends ControllerBackend {

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = KitGame::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionIndex() {
        $model = new KitGame('search');
        $model->unsetAttributes();  // clear any default values
        // Category
        if (Yii::app()->request->getQuery('category_id') !== NULL) {
            $model->category_id = Yii::app()->request->getQuery('category_id');
        }

        if (isset($_GET['KitGame'])) {
            $model->attributes = $_GET['KitGame'];
        }

        // page size drop down changed
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new KitGame;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        $this->save($model);

        $this->render('edit', array(
            'model' => $model
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $categoriesOld = $model->categories = KitGameCategory::model()->get($model->id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        $this->save($model, $categoriesOld);

        $this->render('edit', array(
            'model' => $model
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionInfo($id) {
        $model = KitGame::getDetails($id);

        $this->render('info', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));

            echo json_encode(array('status' => 'success'));
        }
        else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    protected function save($model, $categoriesOld = NULL) {
        $fileOld = $model->file;

        if (isset($_POST['KitGame'])) {
            // Upload image
            if ($_POST['KitGame']['image'] !== NULL AND $_POST['KitGame']['image'] !== '') {
                $_POST['KitGame']['image'] = Common::createThumb($this->module->getName(), $_POST['KitGame']['image'], letArray::get($_POST['KitGame'], 'title', ''));

                // Remove Image
                Common::removeImage($this->module->id, $model->image);
            } else {
                unset($_POST['KitGame']['image']);
            }

            if ($_POST['KitGame']['from_time'] == NULL OR $_POST['KitGame']['from_time'] == '') {
                unset($_POST['KitGame']['from_time']);
            }

            if ($_POST['KitGame']['to_time'] == NULL OR $_POST['KitGame']['to_time'] == '') {
                unset($_POST['KitGame']['to_time']);
            }

            $model->attributes = $_POST['KitGame'];
            $uploadFile = CUploadedFile::getInstance($model, 'file');

            $categoriesNew = $model->categories;

            if ($model->validate()) {
                if ($model->save()) {

//                    // Xử lý upload file
//                    if ($uploadFile != NULL) {
//                        $moduleId = $this->module->id;
//
//                        // Xóa bỏ file cũ
//                        if (!$model->isNewRecord) {
//                            MFile::remove(MFile::autoPathStore($fileOld, $moduleId, 'file'));
//                        }
//
//                        // Upload load file mới, lưu thông tin vào database
//                        $fileDate = date('Y/m/d');
//                        $fileNew = MFile::autoName($model->title) . '.' . $uploadFile->getExtensionName();
//                        if ($uploadFile->saveAs(MFile::autoPathStore($fileNew, $moduleId, 'file', $fileDate))) {
//                            $model->updateByPk($model->id, array('file' => MFile::autoPathSave($fileNew, $fileDate)));
//                        }
//                    }

                    $itemId = $model->id;
                    if (!$model->isNewRecord)
                        KitGameCategory::del($categoriesOld, $itemId);
                    KitGameCategory::create($categoriesNew, $itemId);

                    if (Yii::app()->request->getPost('apply') == 0)
                        $this->redirect(array('index'));
                    else
                        $this->redirect(array('update', 'id' => $model->id));
                }
            }
        }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'kit-game-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    public function actionSanluong() {
        $criteria = new CDbCriteria;
        $criteria->select = 't.*, (SELECT COUNT(*) FROM let_kit_game g WHERE g.creator = t.creator) AS sanluong';
        $criteria->group = 't.creator';

        $dataProvider = new CActiveDataProvider('KitGame', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', 20),
                'pageVar' => 'page'),
            'sort' => array(
                'defaultOrder' => 't.creator ASC'),
        ));
        
        $this->render('sanluong', array(
            'dataProvider' => $dataProvider
        ));
    }

}
