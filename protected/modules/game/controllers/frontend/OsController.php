<?php

/**
 * Module: game
 * Auth: nguago@let.vn
 * Website: http://let.vn
 */
class OsController extends ControllerFrontend {

    public function actionIndex($id) {
        $this->render('index', array('id' => $id));
    }

}