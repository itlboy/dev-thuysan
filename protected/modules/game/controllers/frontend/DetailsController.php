<?php

Yii::import('application.modules.cms.models.KitStats');
class DetailsController extends ControllerFrontend {

    public function actionIndex() {
        Yii::import('application.modules.webservice.models.Ditech');
        Yii::import('application.modules.game.models.KitGamePay');
        
        $id = Yii::app()->request->getParam('id', NULL);
        $result = KitGameCategory::checkCategory($id);
        $category = array();
        if ($result !== NULL) {
            foreach ($result as $item) {
                $category[] = $item->attributes["category_id"];
            }
        }

        $cache_name = md5(__METHOD__ . '_' . $id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            // Get details
            $assign['data'] = KitGame::getDetails($id);
            if (!empty($assign['data']) AND ($assign['data']->status == 1) AND ($assign['data']->trash == 0)) {
                $assign['data'] = KitGame::treatment($assign['data']);
                $assign['user'] = KitAccount::model()->findByPk($assign['data']['creator']);
                $assign['user'] = CJSON::decode(CJSON::encode($assign['user']));
            } else {
                $this->redirect(Yii::app()->getBaseUrl(true));
            }
            $assign['data'] = KitGame::treatment($assign['data']);

            // Get category list
            $criteria = new CDbCriteria();
            $criteria->alias = 't';
            $criteria->join = "INNER JOIN {{kit_game_category}} t2 ON t.id = t2.category_id";
            $criteria->condition = Common::addWhere($criteria->condition, 't2.game_id =' . $id);
            $assign['category'] = KitCategory::model()->findAll($criteria);
            $assign['category'] = KitCategory::treatment($assign['category']);

            Yii::app()->cache->set($cache_name, $assign);
        } else $assign = $cache;
        
        // Meta
        if (isset($assign['data']['title']))
            $this->pageTitle = $assign['data']['title'];
        if (isset($assign['data']['intro']))
            Yii::app()->clientScript->registerMetaTag($assign['data']['intro']);

        if (isset($assign['data']['id']))
            KitStats::updateValue($assign['data']['id'], 'game');
        
        //thong ke
        if (isset($assign['data']['id'])) {
            $assign['stats'] = KitStats::getDetails('game', $assign['data']['id']);
            KitStats::setStats('game', $id);
        }

        // GET SO DIEN THOAI
        $currentPhone = Yii::app()->request->getParam('msisdn', '');
        // Neu khong ton tai so phone truyen tu $_GET
        if(!empty($currentPhone)) { 
            $currentPhone = Yii::app()->session['currentPhone'];
             // Neu khong ton tai so phone luu trong SESSION. $currentPhone !== '' de xet truong hop $currentPhone da get roi nhung khong thanh cong nen luu vao SESSION gia tri rong
            if(empty($currentPhone) AND $currentPhone !== ''){
                $this->redirect(Ditech::$wapCharging['handlePhoneUrl'] . "backUrl=" . $assign['data']['url']);
            }
        } else
            Yii::app()->session['currentPhone'] = $currentPhone;
        // GET SO DIEN THOAI - END
        
        // Start xử lý charging và download
        $isDownload = Yii::app()->request->getParam('download', FALSE);
        if($isDownload){
            $currentPhone = Yii::app()->session['currentPhone'];
            if(!empty($currentPhone)){ //Đã xác định được số phone hiện tại
            
                //Kiểm tra số lần chariging trong ngày hiện tại của số phone hiện tại(limit = 10)
                $dateNow = date('Y-m-d');
                $condition = "phone = '{$currentPhone}' AND DATE(time) = {$dateNow}";
                $chargingTotal = KitGamePay::model()->count($condition);
                if($chargingTotal < 10){
                    //Tạo các tham số charging cần thiết 
                    $transId = (string)date('YmdHms');
                    $key = md5($transId . Ditech::$wapCharging['username'] . Ditech::$wapCharging['password']);
                    $productName = $assign['data']['title'];
                    if(strlen($productName) > 50){
                        $productName = substr($productName, 0, 50);
                    }
                    $price = $assign['data']['price'];
                    $productId = $assign['data']['id'];
                    $cType = $assign['category'][0]['id'];
                    $cName = $assign['category'][0]['name'];
                    if(strlen($cName) > 20){
                        $cName = substr($cName, 0, 20);
                    }
                    
                    $params = array(
                        'price' => $price, //Giá game
                        'transId' => $transId, 
                        'productId' => $productId, //Mã game
                        'productName' => $productName, //Tên game
                        'cType'=> $cType, //Mã thể loại
                        'cName' => $cName, //Tên thể loại
                        'partnerId' => Ditech::$wapCharging['partnerId'], //Mã đối tác
                        'backUrl' => Ditech::getCancelWapChargingUrl(), // cancle Url
                        'key' => $key,
                        'returnUrl' => Ditech::getHandleResultUrl() //Link nhận kết quả
                        
                    );
                    $chargingUrl = Ditech::makeWapChargingUrl($params);
                    
                    //Gửi yêu cầu charging
                    $this->redirect($chargingUrl);
                }
                else{
                    //header to download
                    $file = $assign['data']['file'];
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename='.basename($file));
                    header('Content-Transfer-Encoding: binary');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($file));
                    ob_clean();
                    flush();
                    readfile($file);
                    exit; 
                }
            }
        }
        // End xử lý charging và download
        
        // Lay bo dem so nguoi xem
//        $assign['view_count'] = KitStats::model()->find('item_id=:itemId',array(':itemId' => $id));

        $this->render('index', $assign);
    }

    public function action() {
        return array('captcha' => array(
                'class' => 'CcaptchaACtion',
                'backColor' => '0xffffff',
                'minLength' => '2',
                'maxLength' => '3',
        ));
    }

}