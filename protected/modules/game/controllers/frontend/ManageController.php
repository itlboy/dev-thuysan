<?php
/**
 * Module: game
 * Auth: nguago@let.vn
 * Website: http://let.vn
 */

Yii::import('application.components.backend.BackendFunctions');

class ManageController extends ControllerFrontend
{
    
    public function beforeAction($action) {
        if (parent::beforeAction($action)) {   
            $groups = json_decode(Yii::app()->user->getState('group'), TRUE);
                        
            if (($action->id != 'list' AND $action->id != 'info') AND ($groups == NULL OR (!in_array(8, $groups) AND !in_array(66, $groups)))) {
                $this->redirect(Yii::app()->homeUrl);
            }
            return true;
        } else
            return false;
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
    {
		$model=KitGame::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function actionIndex()
	{
        $model = new KitGame('search');
        $model->unsetAttributes();  // clear any default values        

        if (isset($_GET['KitGame'])) {
            $model->attributes = $_GET['KitGame'];
        }
        
        if (isset($_GET['status'])) {
            $model->status = intval($_GET['status']);
        }
        
        $model->creator = Yii::app()->user->id;
        Yii::app()->user->setState('pageSize', 20);

        $this->render('index', array(
            'model' => $model,
        ));
	}    

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
    {
		$model = new KitGame;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$this->save($model);

		$this->render('edit',array(
			'model'=>$model
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
    {
		$model = $this->loadModel($id);
        if ($model->creator != Yii::app()->user->id)
            $this->redirect(array('list'));

        $categoriesOld = $model->categories = KitGameCategory::model()->get($model->id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

            $this->save($model, $categoriesOld);

		$this->render('edit',array(
			'model' => $model
		));
	}

	protected function save($model, $categoriesOld = NULL) {
		if(isset($_POST['KitGame'])) {
            // Upload image
			if ($_POST['KitGame']['image'] !== NULL AND $_POST['KitGame']['image'] !== '') {
                $_POST['KitGame']['image'] = Common::createThumb($this->module->getName() , $_POST['KitGame']['image'], letArray::get($_POST['KitGame'], 'title', ''));

                // Remove Image
                Common::removeImage($this->module->id, $model->image);
            } else unset ($_POST['KitGame']['image']);


            $model->attributes = $_POST['KitGame'];

            $categoriesNew = $model->categories;


            if ($model->validate()) {
                if($model->save()) {
                    $itemId = $model->id;
                    if (!$model->isNewRecord)
                        KitGameCategory::del($categoriesOld, $itemId);
                    KitGameCategory::create($categoriesNew, $itemId);

                    if (Yii::app()->request->getPost('apply') == 0)
                         $this->redirect(array('index'));
                    else $this->redirect(array('update', 'id' => $model->id));
                }
            }
		}
	}
    
    /**
     * Liet ke danh sach tat ca cac phim 
     */
	public function actionList()
	{
        $model = new KitGame('search');
        $model->unsetAttributes();  // clear any default values        

        if (isset($_GET['KitGame'])) {
            $model->attributes = $_GET['KitGame'];
        }
        
        if (isset($_GET['status'])) {
            $model->status = intval($_GET['status']);
        }
                
        Yii::app()->user->setState('pageSize', 20);

        $this->render('list', array(
            'model' => $model,
        ));
	}
    
	public function actionInfo($id)
    {
		$model = $this->loadModel($id);
        $model->categories = KitGameCategory::model()->get($model->id);
                
		$this->render('info',array(
			'model' => $model
		));
	}
}