<?php

Yii::import('application.modules.game.models.db.BaseKitGame');

class KitGame extends BaseKitGame {

    var $className = __CLASS__;
    public $category_id;
    public $categories;
    
    public $sanluong;    
    
    public $file_path; // Hiển thị đường link, cho biết game đã có upload file hay chưa

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function attributeLabels() {
        return array_merge(parent::attributeLabels(), array(
            'title' => 'Tiêu đề',
            'status' => 'Kích hoạt ?',
            'trash' => 'Thùng rác',
            'image' => 'Ảnh đại diện',
            'intro' => 'Giới thiệu',
            'content' => 'Nội dung',
            'from_time' => 'Thời gian bắt đầu',
            'to_time' => 'Thời gian kết thúc',
            'promotion' => 'Tiêu điểm',
            'sorder' => 'Thứ tự',
            'tags' => 'Thẻ',
            'focus' => 'Tin hot ?',
            'author' => 'Tác giả',
            'source' => 'Nguồn',
            'categories' => 'Danh mục',
            'file' => 'Tập tin',
            'operating_system' => 'Hệ điều hành',
            'type' => 'Thể loại',
            'price' => 'Giá game',
        ));
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return CMap::mergeArray(
            parent::rules(), array(
                array('categories', 'safe'),
                array('file', 'file', 'types' => 'jpg, jpeg, gif, png, swf, zip, rar, jar', 'allowEmpty' => true, 'maxSize' => 10485760), // 10MB
            )
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'KitGameCategory' => array(self::MANY_MANY, 'KitCategory',
                '{{kit_game_category}}(game_id, category_id)'),
            'creatorUser' => array(self::BELONGS_TO, 'KitAccount', 'creator', 'select' => 'username'),
            'editorUser' => array(self::BELONGS_TO, 'KitAccount', 'editor', 'select' => 'username'),
        );
    }        

    protected function beforeValidate() {
        $this->editor = Yii::app()->user->id;
        $this->updated_time = Common::getNow();
        if ($this->isNewRecord) {
            $this->creator = $this->editor;
            $this->created_time = $this->updated_time;
            $this->view_count = 0;
            $this->comment_count = 0;
            $this->sorder = 500;
        }

        return parent::beforeValidate();
    }
    
    protected function afterFind() {
        parent::afterFind();        
        //$this->file_path = ($this->file != NULL) ? MFile::autoPathView($this->file, Yii::app()->controller->module->id, 'file') : NULL;        
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($creator = NULL)
	{        
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;
        
        if ($creator != NULL)
            $this->creator = $creator;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('focus',$this->focus);
		$criteria->compare('has_brand',$this->has_brand);
		$criteria->compare('author',$this->author,true);
		$criteria->compare('source',$this->source,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('intro',$this->intro,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('view_count',$this->view_count);
		$criteria->compare('comment_count',$this->comment_count);
		$criteria->compare('from_time',$this->from_time,true);
		$criteria->compare('to_time',$this->to_time,true);
		$criteria->compare('seo_title',$this->seo_title,true);
		$criteria->compare('seo_url',$this->seo_url,true);
		$criteria->compare('seo_desc',$this->seo_desc,true);
		$criteria->compare('creator',$this->creator);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('editor',$this->editor,true);
		$criteria->compare('updated_time',$this->updated_time,true);
		$criteria->compare('sorder',$this->sorder,true);
		$criteria->compare('promotion',$this->promotion);
		$criteria->compare('status',$this->status);
		$criteria->compare('trash',$this->trash);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('operating_system',$this->operating_system);
		$criteria->compare('type',$this->type);
		$criteria->compare('price',$this->price);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
		));
	}
    
    public function getOperatingSystemOptions() {
        return array(
            1 => 'Android',
            2 => 'iOS',
            3 => 'Java',
            4 => 'RIM',
            5 => 'Windows Phone',
            6 => 'Symbian',
            7 => 'Bada',
            8 => 'Windows Mobile',
        );
    }
    public function parseOperatingSystemOptions($key = NULL) {
        if ($key == NULL)
            $key = $this->operating_system;
        $list = $this->getOperatingSystemOptions();
        if (array_key_exists($key, $list))
            return $list[$key];
    }

    public function getTypeOptions() {
        return array(
            'mmorpg' => 'mmorpg',
            'mmoslg' => 'mmoslg',
            'action' => 'action',
        );
    }

    public function getPriceOptions() {
        return array(
            1000 => 1000,
            2000 => 2000,
            3000 => 3000,
            5000 => 5000,
            10000 => 10000,
            15000 => 15000,
        );
    }

    //public function parseCategory() {}
    ///////////////////////////////// Frontend /////////////////////////////////
    /**
     * treatment
     * @param mixed $data
     * @return array 
     */
    public static function treatment($data)
    {
        $data = CJSON::decode ((CJSON::encode($data))); // Neu la object thi chuyen thanh array
        if (!isset ($data[0]))
            $data = self::treatmentRow($data);
        else {
            foreach ($data as $key => $value) {
                $data[$key] = self::treatmentRow($value);
            }
        }
        return $data;
    }
    
    public static function treatmentRow($row)
    {
        $urlTitle = Common::convertStringToUrl($row['title']);
        $row['url'] = Yii::app()->createUrl($urlTitle.'-game-'.$row['id']);
        $row['title'] = str_replace (array ('"', "'"), array ('&quot;', '&apos;'), $row['title'] );
        return $row;
    }


    /**
     * Get Details
     * @param int $id
     * @return object 
     */
    public static function getDetails($id) {

        $id = intval($id);
        $cache_name = md5(__METHOD__ . '_' . $id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache

        if ($cache === FALSE) {
            $result = self::model()->findByPk($id, array(
                'select' => Common::getFieldInTable(self::model()->getAttributes(), 't.'),
                'with' => array('creatorUser', 'editorUser')
            ));
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
    
    public static function getField($id, $field){
        $row = KitGame::getDetails($id);
        $row = KitGame::treatment($row);
        return letArray::get($row, $field, '');
    }
       
}
