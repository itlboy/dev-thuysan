<?php

/**
 * This is the model class for table "{{kit_game}}".
 *
 * The followings are the available columns in table '{{kit_game}}':
 * @property string $id
 * @property string $title
 * @property integer $focus
 * @property integer $hot
 * @property integer $has_brand
 * @property string $author
 * @property string $source
 * @property string $image
 * @property string $intro
 * @property string $content
 * @property string $tags
 * @property integer $view_count
 * @property integer $comment_count
 * @property string $from_time
 * @property string $to_time
 * @property string $seo_title
 * @property string $seo_url
 * @property string $seo_desc
 * @property string $creator
 * @property string $created_time
 * @property string $editor
 * @property string $updated_time
 * @property string $sorder
 * @property integer $promotion
 * @property integer $status
 * @property integer $trash
 * @property string $file
 * @property integer $operating_system
 * @property string $type
 * @property integer $price
 * @property string $video
 */
class BaseKitGame extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitGame the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_game}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('focus, hot, has_brand, view_count, comment_count, promotion, status, trash, operating_system, price', 'numerical', 'integerOnly'=>true),
			array('title, author, source, image, seo_url, file', 'length', 'max'=>255),
			array('intro, tags', 'length', 'max'=>500),
			array('seo_title', 'length', 'max'=>70),
			array('seo_desc', 'length', 'max'=>160),
			array('creator, editor', 'length', 'max'=>11),
			array('sorder', 'length', 'max'=>10),
			array('type', 'length', 'max'=>50),
			array('content, from_time, to_time, created_time, updated_time, video', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, focus, hot, has_brand, author, source, image, intro, content, tags, view_count, comment_count, from_time, to_time, seo_title, seo_url, seo_desc, creator, created_time, editor, updated_time, sorder, promotion, status, trash, file, operating_system, type, price, video', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('focus',$this->focus);
		$criteria->compare('hot',$this->hot);
		$criteria->compare('has_brand',$this->has_brand);
		$criteria->compare('author',$this->author,true);
		$criteria->compare('source',$this->source,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('intro',$this->intro,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('view_count',$this->view_count);
		$criteria->compare('comment_count',$this->comment_count);
		$criteria->compare('from_time',$this->from_time,true);
		$criteria->compare('to_time',$this->to_time,true);
		$criteria->compare('seo_title',$this->seo_title,true);
		$criteria->compare('seo_url',$this->seo_url,true);
		$criteria->compare('seo_desc',$this->seo_desc,true);
		$criteria->compare('creator',$this->creator,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('editor',$this->editor,true);
		$criteria->compare('updated_time',$this->updated_time,true);
		$criteria->compare('sorder',$this->sorder,true);
		$criteria->compare('promotion',$this->promotion);
		$criteria->compare('status',$this->status);
		$criteria->compare('trash',$this->trash);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('operating_system',$this->operating_system);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('video',$this->video,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
		));
	}
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitGame::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitGame::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitGame::getLastest_'));
        Yii::app()->cache->delete(md5('KitGame::getPromotion_'));
    }

}
