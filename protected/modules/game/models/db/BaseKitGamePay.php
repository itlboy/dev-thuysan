<?php

/**
 * This is the model class for table "{{kit_game_pay}}".
 *
 * The followings are the available columns in table '{{kit_game_pay}}':
 * @property string $id
 * @property string $phone
 * @property string $game_id
 * @property string $service
 * @property string $network_name
 * @property string $time
 */
class BaseKitGamePay extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitGamePay the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_game_pay}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('phone, game_id, service, time', 'required'),
			array('phone, service, network_name', 'length', 'max'=>64),
			array('game_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, phone, game_id, service, network_name, time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('game_id',$this->game_id,true);
		$criteria->compare('service',$this->service,true);
		$criteria->compare('network_name',$this->network_name,true);
		$criteria->compare('time',$this->time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
		));
	}
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitGamePay::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitGamePay::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitGamePay::getLastest_'));
        Yii::app()->cache->delete(md5('KitGamePay::getPromotion_'));
    }

}
