<?php

Yii::import('application.modules.game.models.db.BaseKitGameCategory');
class KitGameCategory extends BaseKitGameCategory{

    var $className = __CLASS__;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function get($itemId) {
        $result = array();
        $data = self::findAll('game_id=:itemId', array(':itemId' => $itemId));
        foreach ($data as $item2Type)
            $result[] = $item2Type->category_id;
        return $result;
    }

    /**
     * Create
     * @param array $categories
     * @param int $itemId 
     */
    public static function create($categories = array(), $itemId) {
        if (! is_array($categories)) return FALSE;
        foreach ($categories as $categoryId) {
            $item2Cat = new KitGameCategory;
            $item2Cat->category_id = $categoryId;
            $item2Cat->game_id = $itemId;
            $item2Cat->save();
        }
    }

    /**
     * Delete
     * @param array $categories
     * @param int $itemId 
     */
    public static function del($categories = array(), $itemId) {
        if (! is_array($categories)) return FALSE;
        foreach ($categories as $categoryId) {
            self::model()->deleteAll('category_id=:categoryId AND game_id=:gameId', array(
                ':categoryId' => $categoryId,
                ':gameId' => $itemId,
            ));
        }
    }
    
    public static function checkCategory($id)
    {
        $id = intval($id);
        $result = self::model()->findAll('game_id=:gameId', array(':gameId' => $id));
        return $result;
    }
    
    public function listCategory() {
        $cache_name = md5('game.category.listCategory');
        $cache = Yii::app()->cache->get($cache_name);
        if ($cache === FALSE) {
            $category = KitCategory::model()->getCategoryOptions('game');
            $category = KitCategory::treatment($category);
            Yii::app()->cache->set($cache_name, $category);
            return $category;
        } else {
            return $cache;
        }
    }
}