<?php

class lastestByCat extends Widget
{
    public $category = 0;
//    public $parent_id = '';

    public function init()
    {
        Yii::import('article.models.KitArticle');
        Yii::import('category.models.KitCategory');
    }
 
    public function run()
    {
        // Get category
        $data['cat'] = KitCategory::getListInParent('article', $this->category);
        if (empty ($data['cat'])) return FALSE;
        
        $data['cat'] = KitCategory::treatment($data['cat']);
        
        foreach ($data['cat'] as $key => $cat) {
            // Get sub category
            $data['subcat'][$cat['id']] = KitCategory::getListInParent('article', $cat['id']);
            $data['subcat'][$cat['id']] = KitCategory::treatment($data['subcat'][$cat['id']]);
        }
        
        foreach ($data['cat'] as $key => $cat) {
            // Get Article
            $data['article'][$cat['id']] = array();
            $articleList = KitArticle::getLastest($cat['id']);
            if (!empty ($articleList)) {
                $data['article'][$cat['id']] = KitArticle::treatment($articleList);
            } else unset($data['cat'][$key]);
        }
        $this->render(__CLASS__, array(
            'data' => $data,
        ));
    }
}