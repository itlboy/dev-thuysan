<?php

class lastest extends Widget
{
    public $view = '';
    public $category = NULL;
    public $title = '';
    public $limit = 10;

    public function init()
    {
        if ($this->view == '') $this->view = __CLASS__;
        Yii::import('property.models.KitProperty');
    }
 
    public function run()
    {
        $data = KitProperty::getLastest($this->category);
        if (empty ($data)) return FALSE;
        $data = KitProperty::treatment($data);
        $this->render($this->view, array(
            'data' => $data,
        ));
    }
}