<div class="let_title1 let_border_b1"><?php echo $this->title; ?></div>
<div class="let_equal let_equal4 clearfix">
    <?php foreach ($data as $key => $value): ?>
        <?php if ($key % 4 == 4): ?>
        </div>
        <div class="let_equal let_equal4 clearfix">
        <?php endif; ?>
        <div class="let_box first">
            <div class="let_inner">
                <div class="let_title5">
                    <div class="ip-homepage-item">
                        <a href="<?php echo $value['url']; ?>" rel="popover" data-content="<?php echo $value['intro']; ?>" data-original-title="<?php echo $value['title']; ?>">
                            <img src="<?php echo Common::getImageUploaded('property/medium/'.$value['image']); ?>" alt="<?php echo $value['title']; ?>" class="ip_recent_img" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
