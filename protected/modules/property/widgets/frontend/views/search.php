<div class="let_maincontent clearfix">
    <div class="let_inner">
        <div class="let_title1">
            Tổng cộng tìm thấy (<?php echo count($results); ?> kết quả) 
        </div>
        <div class="let_search clearfix">
            <form action="">
                <label for="">Search</label>
                <input type="text" name="keyword" value="<?php echo (isset($condition['keyword'])) ? $condition['keyword'] : ''; ?>" onblur="if(this.value=='') this.value='Keyword...';" onfocus="if(this.value=='Keyword...') this.value='';" />                
                <?php
                    $roomList = array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => '5+');                    
                    $priceList = array(50 => '$50', 100 => '$100', 150 => '$150', 200 => '$200', 250 => '$250', 300 => '$300', 350 => '$350', 400 => '$400', 450 => '$450', 500 => '$500', 550 => '$550', 600 => '$600', 650 => '$650', 700 => '$700', 750 => '$750', 800 => '$800', 850 => '$850', 900 => '$900', 950 => '$950', 1000 => '$1,000', 1050 => '$1,050', 1100 => '$1,100', 1150 => '$1,150', 1200 => '$1,200', 1250 => '$1,250', 1300 => '$1,300', 1350 => '$1,350', 1400 => '$1,400', 1450 => '$1,450', 1500 => '$1,500');

                    echo CHtml::dropDownList( 'category', (isset($condition['category'])) ? intval($condition['category']) : NULL, CHtml::listData(KitCategory::getList('property'), 'id', 'name'), array('empty' => 'Select a category'));                    
                    echo CHtml::dropDownList( 'bedrooms_min', (isset($condition['bedrooms_min'])) ? intval($condition['bedrooms_min']) : NULL, $roomList, array('empty' => 'Bedrooms'));
                    echo CHtml::dropDownList( 'bathrooms_min', (isset($condition['bathrooms_min'])) ? intval($condition['bathrooms_min']) : NULL, $roomList, array('empty' => 'Bathrooms'));
                    echo CHtml::dropDownList('price_min', (isset($condition['price_min'])) ? intval($condition['price_min']) : NULL, $priceList, array('empty' => 'Rental Price From'));
                    echo CHtml::dropDownList('price_max', (isset($condition['price_max'])) ? intval($condition['price_max']) : NULL, $priceList, array('empty' => 'Rental Price To'));
                ?>
                <?php /*
                <select name="category">
                    <?php $this->widget('category.widgets.frontend.treeCategory', array('view' => 'treeCategory_house_select', 'module' => 'property')); ?>
                </select>
                <select name="bedrooms_min">
                    <option value="0">Bedrooms</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5	+</option>
                </select>
                <select name="bathrooms_min">
                    <option value="0">Bathrooms</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5	+</option>
                </select>                 
                <select name="price_min">
                    <option selected="" value="">Rental Price From</option>									
                    <option value="50">$50</option>
                    <option value="100">$100</option>
                    <option value="150">$150</option>
                    <option value="200">$200</option>
                    <option value="250">$250</option>
                    <option value="300">$300</option>
                    <option value="350">$350</option>
                    <option value="400">$400</option>
                    <option value="450">$450</option>
                    <option value="500">$500</option>
                    <option value="550">$550</option>
                    <option value="600">$600</option>
                    <option value="650">$650</option>
                    <option value="700">$700</option>
                    <option value="750">$750</option>
                    <option value="800">$800</option>
                    <option value="850">$850</option>
                    <option value="900">$900</option>
                    <option value="950">$950</option>
                    <option value="1000">$1,000</option>
                    <option value="1050">$1,050</option>
                    <option value="1100">$1,100</option>
                    <option value="1150">$1,150</option>
                    <option value="1200">$1,200</option>
                    <option value="1250">$1,250</option>
                    <option value="1300">$1,300</option>
                    <option value="1350">$1,350</option>
                    <option value="1400">$1,400</option>
                    <option value="1450">$1,450</option>
                    <option value="1500">$1,500</option>
                </select>
                <select name="price_max">
                    <option selected="" value="">Rental Price To</option>									
                    <option value="50">$50</option>
                    <option value="100">$100</option>
                    <option value="150">$150</option>
                    <option value="200">$200</option>
                    <option value="250">$250</option>
                    <option value="300">$300</option>
                    <option value="350">$350</option>
                    <option value="400">$400</option>
                    <option value="450">$450</option>
                    <option value="500">$500</option>
                    <option value="550">$550</option>
                    <option value="600">$600</option>
                    <option value="650">$650</option>
                    <option value="700">$700</option>
                    <option value="750">$750</option>
                    <option value="800">$800</option>
                    <option value="850">$850</option>
                    <option value="900">$900</option>
                    <option value="950">$950</option>
                    <option value="1000">$1,000</option>
                    <option value="1050">$1,050</option>
                    <option value="1100">$1,100</option>
                    <option value="1150">$1,150</option>
                    <option value="1200">$1,200</option>
                    <option value="1250">$1,250</option>
                    <option value="1300">$1,300</option>
                    <option value="1350">$1,350</option>
                    <option value="1400">$1,400</option>
                    <option value="1450">$1,450</option>
                    <option value="">$1,500+</option>
                </select>
                 */ ?>
                <input type="submit" value="re-search" class="let_button" />
            </form>
        </div>
        <div class="let_properties">            

            <!-- Start -->
            <?php foreach ($results as $key => $value): ?>
                <div class="let_item clearfix">
<!--                    <div class="let_title2"><a href="<?php echo $value['url'] ?>" title="<?php echo $value['title'] ?>"><?php echo $value['title'] ?></a></div>-->
                    <div class="let_item_content clearfix">
                        <div class="let_fl let_item_image">
                            <img src="<?php echo Common::getImageUploaded('property/medium/'.$value['image']); ?>" alt="feature1" />
                        </div>
                        <div class="let_fr let_item_right">
                            <div class="let_properties_rent clearfix">
                                <div class="let_fl"><a href="<?php echo $value['url'] ?>" title="<?php echo $value['title'] ?>"><?php echo $value['title'] ?></a></div>
                                <div class="let_fr"><a href="javascript:void(0);"><?php echo $value['price_prefix'] . ' ' . $value['price'] . ' ' . $value['price_suffix']; ?></a></div>
                            </div>											
                            <div class="let_item_details clearfix" style="width: 685px;">
                                <div class="let_fr">
                                    <div class="let_item_info">
                                        <div class="let_icon_bed"><?php echo $value['bedrooms'] ?></div>
                                        <div class="let_icon_shower"><?php echo $value['bathrooms'] ?></div>
                                        <div class="let_icon_area"><?php echo $value['area'] ?> m2</div>
                                        <a href="<?php echo $value['url'] ?>" rel="nofollow">view</a>
                                    </div>													
                                </div>
<!--                                <div class="let_title3"><a href="<?php echo $value['url'] ?>" title="<?php echo $value['title'] ?>"><?php echo $value['title'] ?></a></div>-->
                                <p><?php echo $value['intro'] ?></p>
                            </div>

                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            <!-- //End -->
        </div>
<!--        <div class="let_pagination clearfix">
            <ul class="let_list3">
                <li><span>1</span></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>									
            </ul>
        </div>-->

    </div>
</div><!-- End let_main_content_clearfix -->