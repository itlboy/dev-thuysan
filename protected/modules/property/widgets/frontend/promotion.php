<?php

class promotion extends Widget
{
    public $view = '';
    public $category = NULL;
    public $title = '';

    public function init()
    {
        if ($this->view == '') $this->view = __CLASS__;
        Yii::import('property.models.KitProperty');
    }
 
    public function run()
    {
        $data = KitProperty::getPromotion($this->category);
        if (empty ($data)) return FALSE;
        $data = KitProperty::treatment($data);
        $this->render($this->view, array(
            'data' => $data,
        ));
    }
}