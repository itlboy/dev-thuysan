<?php

class mostPopular extends Widget
{
    public $view = '';
    public $category = NULL;
    public $title = '';

    public function init()
    {
        if ($this->view == '') $this->view = __CLASS__;
        Yii::import('article.models.KitArticle');
    }
 
    public function run()
    {
        $data = KitArticle::getMostPopular($this->category);
        if (empty ($data)) return FALSE;
        $data = KitArticle::treatment($data);
        $this->render($this->view, array(
            'data' => $data,
        ));
    }
}