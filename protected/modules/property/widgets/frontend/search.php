<?php

class search extends Widget {

    public $view = '';
    public $data = array();

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
        Yii::import('property.models.KitProperty');
        //Yii::import('category.models.KitCategory');
    }

    public function run() {
//        $criteria = new CDbCriteria;
//        $criteria->condition = '';
//        if (isset($this->data['keyword']) AND $this->data['keyword'] !== '') {
//            $criteria->condition = letFunction::addWhere($criteria->condition, "title LIKE '%" . $this->data['keyword'] . "%'");
//        }
//        if (isset($this->data['category']) AND $this->data['category'] !== '') {
//            
//            $cat_id = intval($this->data['category']);
//            //$criteria->condition = letFunction::addWhere($criteria->condition, "category ='.$cat_id.'");
//        }
//        if (isset($this->data['bedrooms_min']) AND $this->data['bedrooms_min'] !== '') {
//            $b_min_val = intval($this->data['bedrooms_min']);
//            $criteria->condition = letFunction::addWhere($criteria->condition, "bedrooms >='.$b_min_val.'");
//        }
//        if (isset($this->data['bedrooms_max']) AND $this->data['bedrooms_max'] !== '') {
//            $b_max_val = intval($this->data['bedrooms_max']);
//            $criteria->condition = letFunction::addWhere($criteria->condition, "bedrooms <='.$b_max_val.'");
//        }
//        if (isset($this->data['price_min']) AND $this->data['price_min'] !== '') {
//            $p_min_val = intval($this->data['price_min']);
//            $criteria->condition = letFunction::addWhere($criteria->condition, "price >='.$p_min_val.'");
//        }
//        if (isset($this->data['price_max']) AND $this->data['price_max'] !== '') {
//            $p_max_val = intval($this->data['price_max']);
//            $criteria->condition = letFunction::addWhere($criteria->condition, "price <='.$p_max_val.'");
//        }
        
        $params = array();
        $withs = array();
        
        $criteria = new CDbCriteria;
        $criteria->condition = 'status = 1';        
        if (isset($this->data['keyword']) AND ($this->data['keyword'] != '' AND $this->data['keyword'] != 'Keyword...')) {
            $match = addcslashes($this->data['keyword'], '%_');
            $criteria->addCondition('title LIKE :match');
            $params[':match'] = "%$match%";
        }
        if (isset($this->data['category']) AND intval($this->data['category']) != 0) {            
            $criteria->join = 'INNER JOIN {{kit_property_category}} pc ON pc.property_id = t.id';
            $criteria->addCondition('pc.category_id = :categoryId');
            $params[':categoryId'] = intval($this->data['category']);
        }
        if (isset($this->data['bedrooms_min']) AND intval($this->data['bedrooms_min']) != 0) {
            $criteria->addCondition('bedrooms >= :bedroomsMin');
            $params[':bedroomsMin'] = intval($this->data['bedrooms_min']);
        }
//        if (isset($this->data['bedrooms_max']) AND intval($this->data['bedrooms_max']) != 0) {
//            $criteria->addCondition('bedrooms <= :bedroomsMax');
//            $params[':bedroomsMax'] = intval($this->data['bedrooms_max']);
//        }
        if (isset($this->data['bathrooms_min']) AND intval($this->data['bathrooms_min']) != 0) {
            $criteria->addCondition('bathrooms >= :bathroomsMin');
            $params[':bathroomsMin'] = intval($this->data['bathrooms_min']);
        }       
        if (isset($this->data['price_min']) AND intval($this->data['price_min']) != 0) {
            $criteria->addCondition('price >= :priceMin');
            $params[':priceMin'] = intval($this->data['price_min']);
        }
        if (isset($this->data['price_max']) AND intval($this->data['price_max']) != 0) {
            $criteria->addCondition('price <= :priceMax');
            $params[':priceMax'] = intval($this->data['price_max']);
        }

//        if (isset($this->data['numBathrooms']) AND intval($this->data['numBathrooms']) != 0) {
//            $numBathrooms = intval($this->data['numBathrooms']);
//            if ($numBathrooms < 5) {
//                $criteria->compare ('bathrooms', $numBathrooms);
//            } else {
//                $criteria->addCondition('bathrooms >= 5');
//            }
//        }       

        
        if (!empty($params)) $criteria->params = $params;
        if (!empty($withs)) $criteria->with = $withs;
        
        $results = KitProperty::model()->findAll($criteria);
        if (!empty($results))
            $results = KitProperty::treatment($results);
        
//        if (empty($results))
//            return FALSE;
        $this->render($this->view, array(
            'results' => $results,
            'condition' => $this->data
        ));
    }

}