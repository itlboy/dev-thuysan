<?php

class DefaultController extends ControllerBackend
{
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id) {
		$model=KitProperty::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

    public function actionIndex() {
        $model = new KitProperty('search');
        $model->unsetAttributes();  // clear any default values

        // Category
        if (Yii::app()->request->getQuery('category_id') !== NULL) {
            $model->category_id = Yii::app()->request->getQuery('category_id');
        }
        // Option
        if (Yii::app()->request->getQuery('option_id') !== NULL) {
            $model->option_id = Yii::app()->request->getQuery('option_id');
        }

        if (isset($_GET['KitProperty'])) {
            $model->attributes = $_GET['KitProperty'];
        }

        // page size drop down changed
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$model = new KitProperty;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$this->save($model);

		$this->render('edit',array(
			'model'=>$model
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id) {
		$model=$this->loadModel($id);
            $categoriesOld = $model->categories = KitPropertyCategory::model()->getProperty2Category($model->id);
                $optionsOld = $model->options = KitPropertyOption::model()->getProperty2Option($model->id);
    
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

            $this->save($model, $categoriesOld, $optionsOld);

		$this->render('edit',array(
			'model' => $model
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionInfo($id) {
        $model = KitProperty::getDetails($id);

		$this->render('info',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

    
	protected function save($model, $categoriesOld = NULL, $optionsOld = NULL) {
		if(isset($_POST['KitProperty'])) {
            // Upload image
			if ($_POST['KitProperty']['image'] !== NULL AND $_POST['KitProperty']['image'] !== '') {
                $_POST['KitProperty']['image'] = Common::createThumb($this->module->getName() , $_POST['KitProperty']['image'], letArray::get($_POST['KitProperty'], 'title', ''));
            } else unset ($_POST['KitProperty']['image']);

			if ($_POST['KitProperty']['from_time'] == NULL OR $_POST['KitProperty']['from_time'] == '') {
                unset($_POST['KitProperty']['from_time']);
            }

            if ($_POST['KitProperty']['to_time'] == NULL OR $_POST['KitProperty']['to_time'] == '') {
                unset($_POST['KitProperty']['to_time']);
            }

            var_dump($_POST['KitProperty']); die;
            $model->attributes = $_POST['KitProperty'];

                    $categoriesNew = $model->categories;
                            $optionsNew = $model->options;
        
            if ($model->validate()) {
                if($model->save()) {
                    $itemId = $model->id;
                    if (!$model->isNewRecord)
                        KitPropertyCategory::deletePropertyCategory($categoriesOld, $itemId);
                    KitPropertyCategory::createPropertyCategory($categoriesNew, $itemId);
                    if (!$model->isNewRecord)
                        KitPropertyOption::deletePropertyOption($optionsOld, $itemId);
                    KitPropertyOption::createPropertyOption($optionsNew, $itemId);

                    if (Yii::app()->request->getPost('apply') == 0)
                         $this->redirect(array('index'));
                    else $this->redirect(array('update', 'id' => $model->id));
                }
            }
		}
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model) {
		if(isset($_POST['ajax']) && $_POST['ajax']==='kit-property-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
