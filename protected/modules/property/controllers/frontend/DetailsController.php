<?php
class DetailsController extends ControllerFrontend
{
	public function actionIndex()
	{
        $id = Yii::app()->request->getParam('id', NULL);
        $cache_name = md5(__METHOD__ . '_' . $id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
//            if(!$assign['data'] = KitProperty::model()->findByPk($id)) throw new CHttpException(404,'Not found.');
            // Get details

            $assign['data'] = KitProperty::getDetails($id);
//            var_dump($assign['data']); die;
    
//            // Category
            $categorys = $assign['data']->KitPropertyCategory;
            $assign['data'] = KitProperty::treatment($assign['data']);
//            foreach ($categorys as $category) {
//                $assign['breadcrumb'][] = KitCategory::getBreadcrumb($category->id);
//            }
            
            Yii::app()->cache->set($cache_name, $assign); // Set cache
        } else $assign = $cache;
        //Add gallery
        
        //print_r($item_gallery);
        $this->render('index', $assign);
        //Lats ve test tiep
	}
}