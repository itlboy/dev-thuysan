<?php

Yii::import('application.modules.property.models.db.BaseKitPropertyOption');
class KitPropertyOption extends BaseKitPropertyOption {

    var $className = __CLASS__;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getProperty2Option($propertyId) {
        $result = array();
        $data = self::findAll('property_id=:propertyId', array(':propertyId' => $propertyId));
        foreach ($data as $item2Type)
            $result[] = $item2Type->option_id;
        return $result;
    }

    /**
     * Create
     * @param array $options
     * @param int $itemId 
     */
    public static function createPropertyOption($options = array(), $itemId) {
        if (! is_array($options)) return FALSE;
        foreach ($options as $optionId) {
            $item2Opt = new KitPropertyOption;
            $item2Opt->option_id = $optionId;
            $item2Opt->property_id = $itemId;
            $item2Opt->save();
        }
    }

    /**
     * Delete
     * @param array $options
     * @param int $itemId 
     */
    public static function deletePropertyOption($options = array(), $itemId) {
        if (! is_array($options)) return FALSE;
        foreach ($options as $optionId) {
            self::model()->deleteAll('option_id=:optionId AND property_id=:propertyId', array(
                ':optionId' => $optionId,
                ':propertyId' => $itemId,
            ));
        }
    }
}