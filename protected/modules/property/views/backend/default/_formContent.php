<div class="box grid_16" style="margin-top: 10px;">
    <div class="toggle_container">
        <div class="block">
            <h2 class="section"><?php echo Yii::t('global', 'General'); ?></h2>
            
                            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'title'); ?></label>
                <div>
                    <?php echo $form->textField($model,'title'); ?>
                    <?php echo $form->error($model,'title'); ?>
                                <div class="required_tag tooltip hover left" title="<?php echo Yii::t('global', 'Required'); ?>"></div>
                            </div>
            </fieldset>
                                        <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'intro'); ?></label>
                <div class="clearfix">
                    <?php echo $form->textArea($model,'intro', array('class' => 'tooltip', 'title' => 'Nhập thông tin...')); ?>
                    <?php echo $form->error($model,'intro'); ?>
                </div>
            </fieldset>
                                        <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'localtion'); ?></label>
                <div class="clearfix">
                    <?php echo $form->textArea($model,'localtion', array('class' => 'tooltip', 'title' => 'Nhập thông tin...')); ?>
                    <?php echo $form->error($model,'localtion'); ?>
                </div>
            </fieldset>
                                    <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'available'); ?></label>
                <div>
                    <div class="jqui_radios">
                        <?php echo $form->radioButtonList($model,'available', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                        <?php echo $form->error($model,'available'); ?>
                    </div>
                </div>
            </fieldset>
                                    <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'available_time'); ?></label>
                <div class="clearfix">
                    <?php echo $form->textField($model,'available_time', array('class' => 'datepicker')); ?>
                    <?php echo $form->error($model,'available_time'); ?>
                </div>
            </fieldset>
                                    <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'price_prefix'); ?></label>
                <div>
                    <?php echo $form->textField($model,'price_prefix'); ?>
                    <?php echo $form->error($model,'title'); ?>
                            </div>
            </fieldset>
                                    <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'price'); ?></label>
                <div>
                    <?php echo $form->textField($model,'price'); ?>
                    <?php echo $form->error($model,'title'); ?>
                            </div>
            </fieldset>
                                    <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'price_suffix'); ?></label>
                <div>
                    <?php echo $form->textField($model,'price_suffix'); ?>
                    <?php echo $form->error($model,'title'); ?>
                            </div>
            </fieldset>
                                    <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'bedrooms'); ?></label>
                <div>
                    <?php echo $form->textField($model,'bedrooms'); ?>
                    <?php echo $form->error($model,'title'); ?>
                            </div>
            </fieldset>
                                    <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'bathrooms'); ?></label>
                <div>
                    <?php echo $form->textField($model,'bathrooms'); ?>
                    <?php echo $form->error($model,'title'); ?>
                            </div>
            </fieldset>
                                    <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'gara'); ?></label>
                <div>
                    <div class="jqui_radios">
                        <?php echo $form->radioButtonList($model,'gara', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                        <?php echo $form->error($model,'gara'); ?>
                    </div>
                </div>
            </fieldset>
                                    <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'area'); ?></label>
                <div>
                    <?php echo $form->textField($model,'area'); ?>
                    <?php echo $form->error($model,'title'); ?>
                            </div>
            </fieldset>
                                    <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'tags'); ?></label>
                <div class="clearfix">
                    <?php echo $form->textArea($model,'tags', array('class' => 'tooltip', 'title' => 'Nhập thông tin...')); ?>
                    <?php echo $form->error($model,'tags'); ?>
                </div>
            </fieldset>
                                                    <fieldset class="label_side">
                <label><?php echo $form->labelEx($model,'layout_id'); ?></label>
                <div>
                    <?php echo $form->textField($model,'layout_id'); ?>
                    <?php echo $form->error($model,'title'); ?>
                            </div>
            </fieldset>
                                                                    
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'categories'); ?></label>
                <div>
                    <?php                    echo $form->checkBoxList($model, 'categories', CHtml::listData(KitCategory::model()->getCategoryOptions($this->module->id), 'id', 'name'), array(
                        'separator' => '',
                        'template' => '<div>{input} {label}</div>'));
                    ?>
                    <?php echo $form->error($model, 'categories'); ?>
                </div>
            </fieldset>
            
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'options'); ?></label>
                <div>
                    <?php                    echo $form->checkBoxList($model, 'options', CHtml::listData(KitOption::model()->getOptionOptions($this->module->id), 'id', 'name'), array(
                        'separator' => '',
                        'template' => '<div>{input} {label}</div>'));
                    ?>
                    <?php echo $form->error($model, 'options'); ?>
                </div>
            </fieldset>
        </div>
    </div>
</div>

<div class="box grid_16 round_all">
    <div class="toggle_container">
        <div class="block">
            <h2 class="section">Đặt giờ xuất bản</h2>
            <div class="columns clearfix">
                <div class="col_50">
                    <fieldset class="label_side">
                        <label><?php echo $form->labelEx($model,'from_time'); ?></label>
                        <div>
                            <?php echo $form->textField($model,'from_time', array('class' => 'datepicker')); ?>
                            <?php echo $form->error($model,'from_time'); ?>
                        </div>
                    </fieldset>
                </div>
                <div class="col_50">
                    <fieldset class="label_side">
                        <label><?php echo $form->labelEx($model,'to_time'); ?></label>
                        <div>
                            <?php echo $form->textField($model,'to_time', array('class' => 'datepicker')); ?>
                            <?php echo $form->error($model,'to_time'); ?>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="box grid_16">
    <div class="toggle_container">
        <div class="block">
            <div class="columns clearfix">
                <div class="col_25">
                    <fieldset>
                        <label><?php echo $form->labelEx($model,'sorder'); ?></label>
                        <div>
                            <?php echo $form->textField($model,'sorder', array('class' => 'tooltip autogrow', 'title' => 'Số càng nhỏ thì thứ tự càng cao. Mặc định: 500', 'placeholder' => 'Nhập 1 số')); ?>
                            <?php echo $form->error($model,'sorder'); ?>
                        </div>
                    </fieldset>
                </div>
                <div class="col_25">
                    <fieldset>
                        <label><?php echo $form->labelEx($model,'promotion'); ?></label>
                        <div>
                            <div class="jqui_radios">
                                <?php echo $form->radioButtonList($model,'promotion', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                                <?php echo $form->error($model,'promotion'); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col_25">
                    <fieldset>
                        <label><?php echo $form->labelEx($model,'status'); ?></label>
                        <div>
                            <div class="jqui_radios">
                                <?php echo $form->radioButtonList($model,'status', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                                <?php echo $form->error($model,'status'); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col_25">
                    <fieldset>
                        <label><?php echo $form->labelEx($model,'trash'); ?></label>
                        <div>
                            <div class="jqui_radios">
                                <?php echo $form->radioButtonList($model,'trash', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                                <?php echo $form->error($model,'trash'); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>
    
<div class="box grid_16">
    <?php echo $form->error($model,'content'); ?>
    <?php        //echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50, 'class' => 'tinymce'));
        $this->widget('ext.elrtef.elRTE', array(
            'model' => $model,
            'attribute' => 'content',
            'options' => array(
                    'doctype'=>'js:\'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\'',
                    'cssClass' => 'el-rte',
                    'cssfiles' => array('css/elrte-inner.css'),
                    'absoluteURLs'=>true,
                    'allowSource' => true,
                    'lang' => 'vi',
                    'styleWithCss'=>'',
                    'height' => 400,
                    'fmAllow'=>true, //if you want to use Media-manager
                    'fmOpen'=>'js:function(callback) {$("<div id=\"elfinder\" />").elfinder(%elfopts%);}',//here used placeholder for settings
                    'toolbar' => 'maxi',
            ),
            'elfoptions' => array( //elfinder options
                'url'=>'auto',  //if set auto - script tries to connect with native connector
                'passkey'=>'mypass', //here passkey from first connector`s line
                'lang'=>'vi',
                'dialog'=>array('width'=>'900','modal'=>true,'title'=>'Media Manager'),
                'closeOnEditorCallback'=>true,
                'editorCallback'=>'js:callback'
            ),
        ));
    ?>
</div>
