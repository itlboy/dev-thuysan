<?php

Yii::import('application.modules.property.models.db.BaseKitPropertyCategory');
class KitPropertyCategory extends BaseKitPropertyCategory {

    var $className = __CLASS__;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getProperty2Category($propertyId) {
        $result = array();
        $data = self::findAll('property_id=:propertyId', array(':propertyId' => $propertyId));
        foreach ($data as $item2Type)
            $result[] = $item2Type->category_id;
        return $result;
    }

    /**
     * Create
     * @param array $categories
     * @param int $itemId 
     */
    public static function createPropertyCategory($categories = array(), $itemId) {
        if (! is_array($categories)) return FALSE;
        foreach ($categories as $categoryId) {
            $item2Cat = new KitPropertyCategory;
            $item2Cat->category_id = $categoryId;
            $item2Cat->property_id = $itemId;
            $item2Cat->save();
        }
    }

    /**
     * Delete
     * @param array $categories
     * @param int $itemId 
     */
    public static function deletePropertyCategory($categories = array(), $itemId) {
        if (! is_array($categories)) return FALSE;
        foreach ($categories as $categoryId) {
            self::model()->deleteAll('category_id=:categoryId AND property_id=:propertyId', array(
                ':categoryId' => $categoryId,
                ':propertyId' => $itemId,
            ));
        }
    }
}