<?php

class PropertyModule extends CWebModule
{
    public $category = TRUE; // Xác định Module có category hay không
    public $option = TRUE; // Xác định Module có option hay không
    public $menuList = array(
        array(
            'name' => 'Tạo danh mục',
            'url' => '/category/default/create/module/property',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'type' => 'dialog',
            'grid' => '',
        ),
        array(
            'name' => 'Quản lý danh mục',
            'url' => '/category/default/index/module/property',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Quản lý danh mục bài viết',
            'type' => 'direct'
        ),
        array(
            'name' => 'Tạo property',
            'url'  => '/property/default/create',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Tạo property',
            'type' => 'direct'
        ),
        array(
            'name' => 'Quản lý property',
            'url'  => '/property/default/index',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Quản lý property',
            'type' => 'direct'
        ),
    );
    public $image = array(
        'origin' => array(
            'type' => 'move',
            'folder' => 'origin',
        ),
        'large' => array(
            'type' => 'resize',
            'width' => 625,
            'height' => 345,
            'cropWidth' => NULL,
            'cropHeight' => NULL,
            'cropTop' => NULL,
            'cropLeft' => NULL,
            'rotate' => 0, // Xoay anh
            'flip' => NULL, //[integer] Direction. Horizontal = 5, Vertical = 6
            'quality' => 70,
            'folder' => 'large',
        ),
        'medium' => array(
            'type' => 'resize',
            'width' => 204,
            'height' => 140,
            'cropWidth' => NULL,
            'cropHeight' => NULL,
            'cropTop' => NULL,
            'cropLeft' => NULL,
            'rotate' => 0, // Xoay anh
            'flip' => NULL, //[integer] Direction. Horizontal = 5, Vertical = 6
            'quality' => 80,
            'folder' => 'medium',
        ),
        'small' => array(
            'type' => 'resize',
            'width' => 92,
            'height' => 51,
            'cropWidth' => NULL,
            'cropHeight' => NULL,
            'cropTop' => NULL,
            'cropLeft' => NULL,
            'rotate' => 0, // Xoay anh
            'flip' => NULL, //[integer] Direction. Horizontal = 5, Vertical = 6
            'quality' => 100,
            'folder' => 'small',
        ),
    );

    public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'property.models.*',
			'property.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
