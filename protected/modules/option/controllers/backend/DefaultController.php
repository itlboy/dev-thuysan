<?php

class DefaultController extends ControllerBackend
{
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id) {
		$model=KitOption::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

    /**
     * Manages all models.
     */
    public function actionIndex() {
        $model = new KitOption('search');
        $model->unsetAttributes();  // clear any default values


        if (isset($_GET['KitOption'])) {
            $model->attributes = $_GET['KitOption'];
        }

        // page size drop down changed
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$model = new KitOption;
        $model->module = Yii::app()->request->getQuery('module');
        
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

        if (isset($_POST['KitOption'])) {
            $model->attributes = $_POST['KitOption'];
            if ($model->validate()) {
                if ($model->parent_id == 0) {
                    $model->saveNode();
                } else {
                    $root = KitOption::model()->findByPk($model->parent_id);
                    $model->appendTo($root);
                }

                $this->getAjaxStatus('success', 'Data saved');
                Yii::app()->end();
            } else {
                $errors = CActiveForm::validate($model);
                if (!empty($errors)) {
                    $this->getAjaxStatus('error', 'Check data', CJSON::encode($model), $errors);
                    Yii::app()->end();
                }
            }
        }

		$this->layout = false;
        $this->render('edit',array(
			'model'=>$model,
		));
	}
       
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $parentOld = $model->parent_id;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['KitOption'])) {
            $model->attributes = $_POST['KitOption'];
            if ($model->validate()) {
                if ($model->saveNode()) {
                    // Cập nhật vị trí node nếu có thay đỗi
                    if ($parentOld != $model->parent_id) {
                        $modelCurrent = KitOption::model()->findByPk($model->id);

                        if ($model->parent_id == 0) {
                            $modelCurrent->moveAsRoot();
                        } else {
                            $modelParent = KitOption::model()->findByPk($model->parent_id);
                            $modelCurrent->moveAsLast($modelParent);
                        }
                    }

                    $this->getAjaxStatus('success', 'Thông tin đã được lưu');
                    Yii::app()->end();
                }
            } else {
                $errors = CActiveForm::validate($model);
                if (!empty($errors)) {
                    $this->getAjaxStatus('error', 'Vui lòng kiểm tra lại thông tin trước khi lưu', CJSON::encode($model), $errors);
                    Yii::app()->end();
                }
            }
        }

        $this->layout = false;
        $this->render('edit', array(
            'model' => $model,
        ));
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionInfo($id) {
        $model = KitOption::getDetails($id);
        
		$this->render('info',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->deleteNode();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }
    
    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDeleteMultiRecord() {
        if (!Yii::app()->request->isPostRequest)
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');

        $moduleName = Yii::app()->request->getPost('module');
        $modelName = Yii::app()->request->getPost('model');
        $field = Yii::app()->request->getPost('field');
        $value = (int) Yii::app()->request->getPost('value');
        $trash = Yii::app()->request->getPost('trash');
        $ids = Yii::app()->request->getPost('ids');

        if (array_key_exists($moduleName, Yii::app()->modules) AND is_array($ids) AND !empty($ids)) {

            Yii::import('application.modules.'.$moduleName.'.models.'.$modelName);
            $model = CActiveRecord::model($modelName);

            if ($trash AND $field !== NULL) {
                foreach ($ids as $id) {
                    // dung truoc tiep bien $model se bao loi tren SQL
                    $modelFind = $model->findByPk((int)$id);
                    if ($modelFind !== NULL) {
                        $descendants = $modelFind->descendants()->findAll();
                        foreach ($descendants as $child)
                            $modelFind->updateByPk((int)$child->id, array($field => $value));
                        $modelFind->updateByPk((int)$id, array($field => $value));
                    }
                }
            } else {
                foreach ($ids as $id) {
                    // cho nay hinh nhu co gi do ko on
                    $modelFind = $model->findByPk((int)$id);
                    if ($modelFind !== NULL) {
                        $descendants = $modelFind->descendants()->findAll();
                        foreach ($descendants as $child)
                            $modelFind->deleteByPk((int)$child->id);
                        $modelFind->deleteNode();
                    }
                }
            }
            
            echo 'success';
        }
    }
    
    // Khong co tac dung voi root. Chac phai dung them truong sort cho cac root
    public function actionSort() {
        if (!Yii::app()->request->isAjaxRequest)
            Yii::app()->end();

        $id = Yii::app()->request->getPost('id');
        $sort = Yii::app()->request->getPost('sort');

        $data = Yii::app()->db->createCommand()
                ->select('prev.id prevId, curr.id currId, next.id nextId')
                ->from('{{kit_option}} curr')
                ->leftJoin('{{kit_option}} prev', 'prev.rgt = curr.lft-1')
                ->leftJoin('{{kit_option}} next', 'next.lft = curr.rgt+1')
                ->where('curr.id=:id', array(':id' => $id))
                ->queryRow();

        if ($data == NULL)
            throw new CHttpException(404,'The requested page does not exist.');

        $modelCurr = KitOption::model()->findByPk((int) $data['currId']);
        if ($sort == 'up' && $data['prevId']) {
            $modelPrev = KitOption::model()->findByPk((int) $data['prevId']);
            $modelCurr->moveBefore($modelPrev);
        }
        if ($sort == 'down' && $data['nextId']) {
            $modelNext = KitOption::model()->findByPk((int) $data['nextId']);
            $modelCurr->moveAfter($modelNext);
        }

        //MCore::getAjaxStatus('success');
        echo true;
        Yii::app()->end();
    }
   
    private function getAjaxStatus($status, $message='', $jrvalue='', $jrvalid='') {
        echo json_encode(array(
            'status' => $status,                // trạng thái success, warning, error
            'message' => $message,              // thông tin đã được lưu
            'jrvalue' => $jrvalue,              // danh sách các ID trả về
            'jrvalid' => $jrvalid,              // thông tin model valid error
        ));
    }
    
	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model) {
		if(isset($_POST['ajax']) && $_POST['ajax']==='kit-option-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
