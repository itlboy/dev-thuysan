<?php

Yii::import('application.modules.option.models.db.BaseKitOption');
class KitOption extends BaseKitOption{
    var $className = __CLASS__;
    var $children = array();
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function behaviors() 
    {
        return array(
            'NestedSetBehavior' => array(
                'class' => 'NestedSetBehavior',
                'hasManyRoots' => true,
                'leftAttribute' => 'lft',
                'rightAttribute' => 'rgt',
                'levelAttribute' => 'level',
            )
        );
    }
    
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
        return CMap::mergeArray(
            parent::rules(),
            array(
            )
        );
	}
    
    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'creatorUser' => array(self::BELONGS_TO, 'KitAccount', 'creator', 'select' => 'username'),
            'editorUser' => array(self::BELONGS_TO, 'KitAccount', 'editor', 'select' => 'username'),
		);
	} 
    
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;        
        $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');
        
        $module = Yii::app()->request->getQuery('module');
        if ($module !== null) {
            $this->module = $module;
        }
        
		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.name',$this->name,true);
		$criteria->compare('t.alias',$this->alias,true);
		$criteria->compare('t.parent_id',$this->parent_id,true);
		$criteria->compare('t.description',$this->description,true);
		$criteria->compare('t.layout',$this->layout,true);
		$criteria->compare('t.module',$this->module,true);
		$criteria->compare('t.lft',$this->lft,true);
		$criteria->compare('t.rgt',$this->rgt,true);
		$criteria->compare('t.level',$this->level);
		$criteria->compare('t.root',$this->root,true);
		$criteria->compare('t.created_time',$this->created_time,true);
		$criteria->compare('t.updated_time',$this->updated_time,true);
		$criteria->compare('t.sorder',$this->sorder,true);
		$criteria->compare('t.promotion',$this->promotion);
		$criteria->compare('t.status',$this->status);
		$criteria->compare('t.trash',$this->trash);
        

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
            'sort' => array(
                'defaultOrder' => 'root ASC, t.lft ASC',
            ),
		));
	}
    
    protected function beforeValidate() {
        $this->updated_time = Common::getNow();
        if ($this->isNewRecord) {
            $this->created_time = $this->updated_time;
        }

        if (isset($_POST['KitOption']) AND $_POST['KitOption']['alias'] == NULL) {
            $this->alias = Common::convertStringToUrl($this->name);
        }

        return parent::beforeValidate();
    }
    
                

    ///////////////////////////////// Frontend /////////////////////////////////
    /**
     * treatment
     * @param mixed $data
     * @return array 
     */
    public static function treatment($data)
    {
        $data = CJSON::decode ((CJSON::encode($data))); // Neu la object thi chuyen thanh array
        if (!isset ($data[0]))
            $data = self::treatmentRow($data);
        else {
            foreach ($data as $key => $value) {
                $data[$key] = self::treatmentRow($value);
            }
        }
        return $data;
    }
    
    public static function treatmentRow($row)
    {
        $urlTitle = Common::convertStringToUrl($row['name']);//WTF @@
        $row['url'] = Yii::app()->createUrl($urlTitle.'-option'.$row['id']);
        return $row;
    }
    
    /**
     * Get Details
     * @param int $id
     * @return object 
     */
    public static function getDetails($id) {
        $id = intval($id);
        $cache_name = md5(__METHOD__ . '_' . $id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $result = self::model()->findByPk($id, array(
                'select' => Common::getFieldInTable(self::model()->getAttributes(), 't.'),
                'with' => array('creatorUser', 'editorUser')
            ));
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
    
    /**
     * Get list by module
     * @param string $module
     * @param string $type
     * @return mixed
     */
    public static function getList($module = '', $promotion = NULL, $type = 'object') {
        $promotionStr = ($promotion == NULL) ? 'NULL' : $promotion;
        $cache_name = md5(__METHOD__ . '_' . $module . '_' . $promotionStr . '_' . $type);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = implode(',', array_keys(self::model()->getAttributes()));
            $criteria->condition = '';
            if ($module !== '')
                $criteria->condition = letFunction::addWhere($criteria->condition, "module = '" . $module . "'");
            if ($promotion !== NULL)
                $criteria->condition = letFunction::addWhere($criteria->condition, "promotion = '" . $promotion . "'");
            $criteria->order = 'parent_id ASC, sorder ASC, id ASC';
            $list = self::model()->findAll($criteria);

            if ($type == 'json')
                $result = json_encode($list);
            elseif ($type == 'array')
                $result = CJSON::decode((CJSON::encode($list)));
            else
                $result = $list;
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else
            return $cache;
        return $result;
    }


    
    
    
    
    
    
    /**
     * Lay danh sach Module su dung option
     * @return array
     */
    public static function getModuleList() {
        $arr = array();
        foreach (Yii::app()->modules as $moduleName => $moduleDetails) {
            if (isset(Yii::app()->getModule($moduleName)->option) AND Yii::app()->getModule($moduleName)->option == TRUE) {
                $arr[$moduleName] = $moduleName;
            }
        }
        return $arr;
    }
    
    /**
     * get all option and dump it into an array
     * @return Array Of kitOption
     * Each item is an instance of KitOption onject, include fields as follow:
     *  - id: id of option
     *  - name: name of option
     *  - children: array of children option, each item is an instance of KitOption object (include id and name field)
     */
    public static function dumpAllOptionToArray($module = NULL, $parentId = 0, $maxLevel = 0) {
        $tree = array();
        $criteria = new CDbCriteria;
        $criteria->select = "id, name, alias, sorder";
        $criteria->condition = "parent_id=$parentId";
        $criteria->order = "sorder, id";
        
        if ($module !== NULL)
            $criteria->addSearchCondition ('module', $module);
        
        $options = self::model()->findAll($criteria);
        foreach ($options as $option) {
            self::dumpOptionChildren($option, 1, $maxLevel);
            array_push($tree, $option);
        }

        return $tree;
    }

    public static function dumpOptionChildren(&$parent, $level = 1, $maxLevel = 0) {
        $criteria = new CDbCriteria;
        $criteria->select = "id, name, alias";
        $criteria->condition = "parent_id=" . $parent->id;
        $criteria->order = "sorder, id";

        if (!$maxLevel || ($maxLevel && $level < $maxLevel)) {
            $children = self::model()->findAll($criteria);
            if ($children) {
                $parent->children = array();
                foreach ($children as $child) {
                    self::dumpOptionChildren($child, $level + 1, $maxLevel);
                    array_push($parent->children, $child);
                }
            }
        }
    }
    
    public function getSortIcon($action, $grid, $id) {
        $url = Yii::app()->createUrl('//' . Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/' . $action);

        $imageUp = '<img src="' . Yii::app()->theme->baseUrl . '/images/icons/system/up_16.png" height="16" width="16" title="' . Yii::t('default', 'Up') . '">';
        $imageDown = '<img src="' . Yii::app()->theme->baseUrl . '/images/icons/system/down_16.png" height="16" width="16" title="' . Yii::t('default', 'Down') . '">';

        $html = CHtml::link($imageUp, 'javascript:;', array('onclick' => "ajaxGridSort('{$grid}', '{$id}', '{$url}', 'up')"));
        $html .= ' ';
        $html .= CHtml::link($imageDown, 'javascript:;', array('onclick' => "ajaxGridSort('{$grid}', '{$id}', '{$url}', 'down')"));
        return $html;
    }
    
    public function getOptionOptions($module, $parentId = 0) {
        $criteria = new CDbCriteria;
        $criteria->select = 'id, CONCAT(REPEAT("-- ", level - 1), name) AS name';
        $criteria->order = 'root ASC, lft ASC';
        $criteria->condition = 'level<=:level AND status=1 AND module=:module';
        $criteria->params = array(
            ':level' => 5,
            ':module' => $module
        );
        if ($parentId != 0) {
            $model = self::model()->findByPk((int) $parentId);
            $descendants = $model->descendants()->findAll(array(
                'select' => 'id',
                'condition' => 'status!=0'
//                'condition' => 'id!=root AND status!=0'
            ));
            $children = array();
            $children[] = $parentId;
            foreach ($descendants as $child)
                $children[] = $child->id;

            $criteria->addNotInCondition('id', $children);
        }

        return self::model()->findAll($criteria);
    }
    
}