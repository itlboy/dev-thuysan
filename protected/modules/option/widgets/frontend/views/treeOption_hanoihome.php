<?php foreach ($data as $key => $parent): ?>
    <?php if ($parent['parent_id'] == 0) : ?>
        <div class="let_box3">
            <div class="let_box_title"><?php echo $parent['name']; ?></div>
            <?php if (isset($data[0])) : ?>
                <div class="let_box_content clearfix">
                    <ul class="let_list2">
                        <?php foreach ($data as $children): ?>
                            <?php if ($children['parent_id'] == $parent['id']) : ?>
                                <li><?php echo $children['name']; ?></li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>
<?php endforeach; ?>
