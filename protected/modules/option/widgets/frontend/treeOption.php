<?php

class treeOption extends Widget
{
    public $view = '';
    public $module = '';
    public $promotion = NULL;

    public function init()
    {
        if ($this->view == '') $this->view = __CLASS__;
        Yii::import('option.models.KitOption');
    }
 
    public function run()
    {
        $data = KitOption::getList($this->module, $this->promotion, 'array');
        $data = KitOption::treatment($data);
        $this->render($this->view, array(
            'data' => $data,
        ));
    }
}