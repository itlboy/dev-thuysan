<?php

class Media extends Widget {

    public $module;
    public $itemId;
    public $limit;
        
    public function run() {
        $moduleName = 'media';
        $modelName = 'KitMedia';
        $gridID = 'kit_'.$moduleName.'_grid';

        Yii::import('application.modules.media.models.KitMedia');
        $criteria = new CDbCriteria;
        $criteria->condition = 'module = :module AND item_id = :itemId';
        $criteria->params = array(
            ':module' => $this->module,
            ':itemId' => $this->itemId
        );
        
		$dataProvider = new CActiveDataProvider($modelName, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => $this->limit,
            ),
        ));               
        
//        echo '<a href="javascript:;" onclick="js:loadAjaxDialog(\'' . Yii::app()->createUrl('media/default/upload') . '\', \'\')">Upload</a>';        
                    
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => $gridID,
            'dataProvider' => $dataProvider,
            //'filter' => $model,
            'columns' => array(
                array(
                    'class' => 'CCheckBoxColumn',
                    'id' => 'chkIds',
                    'selectableRows' => 2,
                    'htmlOptions' => array('width' => '20px', 'align' => 'center')
                ),
                array(
                    'name' => 'id',
                    'value' => '$data->id',
                    'htmlOptions' => array('width' => '100px', 'align' => 'center')
                ),
                array(
                    'filter' => '',
                    'name' => 'url',
                    'type' => 'raw',
                    'value' => 'CHtml::image(Common::getImageUploaded($data->url), "", array("height" => "100"))',
                    'htmlOptions' => array('width' => '100px', 'align' => 'center')
                ),
                'content',
                array(
                    'filter' => '',
                    'name' => 'status',
                    'value' => 'BackendFunctions::getStatusLink("status", $data->id, $data->status)',
                    'htmlOptions' => array('width' => '50px', 'align' => 'center')
                ),
                /*
                'created_time',
                'updated_time',
                'sorder',
                'status',
                */        
                array(
                    'class' => 'CButtonColumn',
                    'template' => '{gridUpdate} {gridDelete}',
                    'buttons' => array(
                        'gridUpdate' => array(
                            'label' => Yii::t('default', 'Update'),
                            'url' => 'Yii::app()->createUrl("/media/default/update", array("id" => $data->id))',
                            'imageUrl'=>Yii::app()->theme->baseUrl.'/images/icons/system/edit_16.png',
                            //'options' => array('onclick' => "js:ajaxDialogForm('".$gridID."', 'Cập nhật', jQuery(this).attr('href'), 'auto', 500); return false")
                        ),
                        'gridDelete' => array(
                            'label' => Yii::t('default', 'Delete'),
                            'url' => 'Yii::app()->createUrl("/media/default/delete", array("id" => $data->id))',
                            'imageUrl'=>Yii::app()->theme->baseUrl.'/images/icons/system/delete_16.png',
                            'options' => array('onclick' => "js:ajaxGridDelete('".$moduleName."', '".$modelName."', '".$gridID."', jQuery(this).attr('href'), 'status', 2); return false"),
                        ),
                    ),
                ),
            ),
        ));
        
    }
    
}
