<?php

class DefaultController extends ControllerBackend
{
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id) {
		$model=KitMedia::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

    public function actionIndex() {
        $model = new KitMedia('search');
        $model->unsetAttributes();  // clear any default values
        
        
        if (isset($_GET['KitMedia'])) {
            $model->attributes = $_GET['KitMedia'];
        }

        // page size drop down changed
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$model = new KitMedia;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$this->save($model);

		$this->render('edit',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id) {
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

        $this->save($model);

		$this->render('edit',array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionInfo($id) {
        $model = KitMedia::getDetails($id);
        
		$this->render('info',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model = $this->loadModel($id);
            if ($model !== NULL) {
                $src = Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.Yii::app()->params['uploadDir'].DIRECTORY_SEPARATOR.$model->url;
                if (file_exists($src) AND is_file($src))
                    unlink($src);
                
                if ($model->delete())
                    echo 'success';
            }
            
            Yii::app()->end();
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	protected function save($model) {
		if(isset($_POST['KitMedia'])) {
            // Upload image
			if ($_POST['KitMedia']['image'] !== NULL AND $_POST['KitMedia']['image'] !== '') {
                $_POST['KitMedia']['image'] = Common::createThumb($this->module->getName() , $_POST['KitMedia']['image'], letArray::get($_POST['KitMedia'], 'title', ''));
            }
            
			if ($_POST['KitMedia']['from_time'] == NULL OR $_POST['KitMedia']['from_time'] == '') {
                unset($_POST['KitMedia']['from_time']);
            }

            if ($_POST['KitMedia']['to_time'] == NULL OR $_POST['KitMedia']['to_time'] == '') {
                unset($_POST['KitMedia']['to_time']);
            }
            
            $model->attributes = $_POST['KitMedia'];

            if ($model->validate()) {
                if($model->save()) {
                    $itemId = $model->id;

                    if (Yii::app()->request->getPost('apply') == 0)
                         $this->redirect(array('index'));
                    else $this->redirect(array('update', 'id' => $model->id));
                }
            }
		}
	}
    
	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model) {
		if(isset($_POST['ajax']) && $_POST['ajax']==='kit-media-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    
    public function actionUpload() {        
        $module = Yii::app()->request->getPost('module');
        $itemId = Yii::app()->request->getPost('itemId');        
        $title = Yii::app()->request->getPost('title');
        $media = Yii::app()->request->getPost('media');
        
        
        $date = date('Y/m/d/');
        // Duong dan file luu o thu muc tam
        $tmpFile = Common::getImageUploaded($media, 'PATH');

        // Lay thong tin file tam va tao ra ten file moi
        $tmpFileInfo = pathinfo($tmpFile);
        $fileName = Common::convertStringToUrl($title) . '_' . rand(100000, 999999) . '.' .$tmpFileInfo['extension'];

        // Thu muc luu file
        $fileDir = $module . '/media/' . $date;
        
        // Duong dan file
        $filePath = Common::getImageUploaded($fileDir . $fileName, 'PATH');

        // Tao dir theo path neu dir chua ton tai
        Common::mkPath($fileDir, Common::getImageUploaded('', 'PATH'));

        if (copy($tmpFile, $filePath)) {
            if (file_exists($tmpFile) AND is_file($tmpFile)) unlink ($tmpFile);
            
            $model = new KitMedia;
            $model->module = $module;
            $model->item_id = $itemId;
            $model->title = $title;        
            $model->url = $fileDir . $fileName;
            $model->save();
        }
    }
}
