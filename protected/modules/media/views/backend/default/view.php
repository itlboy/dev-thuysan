<?php
$this->breadcrumbs=array(
	'Kit Articles'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List KitArticle', 'url'=>array('index')),
	array('label'=>'Create KitArticle', 'url'=>array('create')),
	array('label'=>'Update KitArticle', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete KitArticle', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage KitArticle', 'url'=>array('admin')),
);
?>

<h1>View KitArticle #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
		'intro',
		'type',
		'image',
		'content',
		'tags',
		'view_count',
		'comment_count',
		'from_time',
		'to_time',
		'creator',
		'created_time',
		'editor',
		'updated_time',
		'layout_id',
		'promotion',
		'sorder',
		'status',
	),
)); ?>
