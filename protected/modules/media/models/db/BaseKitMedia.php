<?php

/**
 * This is the model class for table "{{kit_media}}".
 *
 * The followings are the available columns in table '{{kit_media}}':
 * @property string $id
 * @property integer $item_id
 * @property string $module
 * @property string $type
 * @property string $url
 * @property string $title
 * @property string $image
 * @property string $intro
 * @property string $content
 * @property string $tags
 * @property integer $view_count
 * @property integer $comment_count
 * @property string $from_time
 * @property string $to_time
 * @property string $layout_id
 * @property string $seo_title
 * @property string $seo_url
 * @property string $seo_desc
 * @property string $creator
 * @property string $created_time
 * @property string $editor
 * @property string $updated_time
 * @property string $sorder
 * @property integer $promotion
 * @property integer $status
 * @property integer $trash
 */
class BaseKitMedia extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitMedia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_media}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('item_id, view_count, comment_count, promotion, status, trash', 'numerical', 'integerOnly'=>true),
			array('module', 'length', 'max'=>20),
			array('type, sorder', 'length', 'max'=>10),
			array('url', 'length', 'max'=>1000),
			array('title, image, seo_url', 'length', 'max'=>255),
			array('intro, tags', 'length', 'max'=>500),
			array('layout_id', 'length', 'max'=>45),
			array('seo_title', 'length', 'max'=>70),
			array('seo_desc', 'length', 'max'=>160),
			array('creator, editor', 'length', 'max'=>11),
			array('content, from_time, to_time, created_time, updated_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, item_id, module, type, url, title, image, intro, content, tags, view_count, comment_count, from_time, to_time, layout_id, seo_title, seo_url, seo_desc, creator, created_time, editor, updated_time, sorder, promotion, status, trash', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('module',$this->module,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('intro',$this->intro,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('view_count',$this->view_count);
		$criteria->compare('comment_count',$this->comment_count);
		$criteria->compare('from_time',$this->from_time,true);
		$criteria->compare('to_time',$this->to_time,true);
		$criteria->compare('layout_id',$this->layout_id,true);
		$criteria->compare('seo_title',$this->seo_title,true);
		$criteria->compare('seo_url',$this->seo_url,true);
		$criteria->compare('seo_desc',$this->seo_desc,true);
		$criteria->compare('creator',$this->creator,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('editor',$this->editor,true);
		$criteria->compare('updated_time',$this->updated_time,true);
		$criteria->compare('sorder',$this->sorder,true);
		$criteria->compare('promotion',$this->promotion);
		$criteria->compare('status',$this->status);
		$criteria->compare('trash',$this->trash);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
		));
	}
}
