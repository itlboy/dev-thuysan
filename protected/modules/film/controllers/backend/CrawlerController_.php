<?php
Yii::import('application.vendors.libs.videoInfo');
class CrawlerController extends ControllerBackend{

    public function actionIndex(){
        $this->render('index');
    }
    public function actionAjaxSearch()
    {
        if(!empty($_POST)){
            $keyword = letArray::get($_POST,'keyword');
            $site = letArray::get($_POST,'site');
            $episode_from = letArray::get($_POST,'episode_from');
            $filmId = letArray::get($_POST,'filmId');
            $keyword = str_replace('{{episode}}', $episode_from, $keyword);

            $url = 'http://www.google.com/search?hl=en&q=site:'.$site.' '.$keyword.'&btnI=I%27m+Feeling+Lucky';
            $url = str_replace(' ', '+', $url);
            $result = videoInfo::youtube($url);
            $result['sorder'] = $episode_from * 10;// Số thứ tự * 10
            $result['server'] = !empty($result['link']) ? videoInfo::getNameServer($result['link']) : '';

//          Lấy danh sách ID YTube của phim
            $cache_name = md5('film_'.$filmId);
            $cache = Yii::app()->cache->get($cache_name);
            if($cache === FALSE){
                $data = KitFilmEpisode::model()->findAll('item_id=:itemId AND server=:server',array(
                    ':itemId' => $filmId,
                    ':server' => $result['server']
                ));
                $data = KitFilmEpisode::treatment($data);
                foreach($data as $item){
                    $MovieId[] = videoInfo::getYoutubeId($item['link']);
                }
                Yii::app()->cache->set($cache_name,$MovieId);
            } else {
                $MovieId = $cache;
            }

//          Kiểm tra
            $result['check'] = '';
            if(!empty($MovieId)){
                if(in_array($result['id'],$MovieId))
                {
                    $result['check'] = '<img height="20px" src="'.Yii::app()->theme->baseUrl.'/images/checktrue.png" />';
                }
            }
            echo json_encode($result);
            return;
        }
    }
    public function actionAjaxSaveItem()
    {
        $filmid = letArray::get($_POST,'filmid');
        $filmdata = letArray::get($_POST,'filmdata');
        if(Yii::app()->user->isGuest)
            return FALSE;
        $id = Yii::app()->user->id;
        if(count($filmdata) > 0){
            $_db = Yii::app()->db;
            $command = $_db->createCommand();
            $date = date('Y-m-d H:i:s');
            foreach($filmdata as $item){
                $insert = $command->insert('let_kit_film_episode',array(
                    'item_id' => $filmid,
                    'title' => $item['episode'],
                    'server' => $item['server'],
                    'link' => $item['link'],
                    'creator' => $id,
                    'editor' => $id,
                    'sorder' => $item['sorder'],
                    'status' => 1,
                    'created_time' => $date,
                    'updated_time' => $date,
                ));
                $command->reset();
            }
            echo json_encode(array(
                'result' => 'true',
                'mess' => 'Lưu thành công',
            ));
        }
        return;
    }
    public function actionCheckFilm()
    {
        $filmId = letArray::get($_POST,'id');
        $result = KitFilm::getDetails($filmId);
        if(empty($result)){
            echo json_encode(array(
                'result' => 'false',
                'title' => 'Phim ID:<strong>'.$filmId.'</strong> không tồn tại ',
            ));
        } else {
            $result = KitFilm::treatment($result);
            echo json_encode(array(
                'result' => 'true',
                'title' => '<a href="'.Yii::app()->createUrl('film/default/update/',array('id' => $filmId)).'" >Phim : <strong>'. $result['title'].'</strong>&nbsp;<span style="color:red;text-decoration: underline">Edit</span></a>',
            ));
        }
        return;
    }
    public function actionCheckLink(){

        $this->render('checklink');
    }

}