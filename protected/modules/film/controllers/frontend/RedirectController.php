<?php
class RedirectController extends ControllerFrontend
{
	public function actionIndex()
	{
        $url = Yii::app()->getBaseUrl(true);;
        $id = Yii::app()->request->getParam('id', NULL);
        $assign['data'] = KitFilm::getDetails($id);
    
        if (! empty($assign['data'])){
            $assign['data'] = KitFilm::treatment($assign['data']);
            $url = $assign['data']['url'];
        }
        
        Header( "HTTP/1.1 301 Moved Permanently" ); 
        Header( "Location: " . $url); 
	}
}