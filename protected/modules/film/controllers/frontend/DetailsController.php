<?php
Yii::import('application.modules.cms.models.KitStats');
class DetailsController extends ControllerFrontend
{
	public function actionIndex()
	{
        $id = intval(Yii::app()->request->getParam('id', NULL));

        $cache_name = md5('film_details_' . $id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
        
            // Get details
            $assign['data'] = KitFilm::getDetails($id);
            if(!empty($assign['data']) AND ($assign['data']->status == 1) AND ($assign['data']->trash ==0)){
                $assign['data'] = KitFilm::treatment($assign['data']);
                $assign['user'] = KitAccount::model()->findByPk($assign['data']['creator']);
                $assign['user'] = CJSON::decode(CJSON::encode($assign['user']));
            } else {
                $this->redirect(Yii::app()->getBaseUrl(true));
            }
            // Get category list
            $criteria= new CDbCriteria();
            $criteria->alias = 't';
            $criteria->join = "INNER JOIN {{kit_film_category}} t2 ON t.id = t2.category_id";
            $criteria->condition = Common::addWhere($criteria->condition, 't2.film_id ='.$id);
            $assign['category'] = KitCategory::model()->findAll($criteria);
            $assign['category'] = KitCategory::treatment($assign['category']);

            $episode = KitFilmEpisode::model()->findAll('default_video=:defaultVideo AND item_id=:itemId',array(
                ':itemId' => $id,
                ':defaultVideo' => 1,
            ));
            if(!empty($episode)){
                $episode = KitFilmEpisode::treatment($episode);
                $assign['episode_default'] = $episode[0];
            }

            Yii::app()->cache->set($cache_name, $assign); // Set cache
        } else $assign = $cache;

//            // Category
//            $categorys = $assign['data']->kitFilmCategory;
//            foreach ($categorys as $category) {
//                $assign['breadcrumb'][] = KitCategory::getBreadcrumb($category->id);
//            }
            

        // Meta
        if (isset($assign['data']['title'])) $this->pageTitle = $assign['data']['title'] . ' - ' . $assign['data']['title_english'];
        if (isset($assign['data']['intro'])) Yii::app()->clientScript->registerMetaTag($assign['data']['intro'],'description');

		Yii::app()->clientScript->registerMetaTag('100001754856259', NULL, NULL, array('property'=>'fb:admins'));
//        Yii::app()->clientScript->registerMetaTag('280563077863', NULL, NULL, array('property'=>'fb:app_id'));
        Yii::app()->clientScript->registerMetaTag($assign['data']['title'].' - '.$assign['data']['title_english'], NULL, NULL, array('property'=>'og:title'));
        Yii::app()->clientScript->registerMetaTag(Common::getImageUploaded('film/large/'.$assign['data']['image']), NULL, NULL, array('property'=>'og:image'));
        Yii::app()->clientScript->registerMetaTag(Yii::app()->getBaseUrl(true).$assign['data']['url'], NULL, NULL, array('property'=>'og:url'));
        Yii::app()->clientScript->registerMetaTag('Phim.let.vn', NULL, NULL, array('property'=>'og:site_name'));
        Yii::app()->clientScript->registerMetaTag($assign['data']['title'].' - '.$assign['data']['title_english'], 'title');
        Yii::app()->clientScript->registerLinkTag('image_src', NULL, Common::getImageUploaded('film/large/' . $assign['data']['image']));
        Yii::app()->clientScript->registerLinkTag('canonical', NULL, Yii::app()->getBaseUrl(true).$assign['data']['url']);

//        $assign['interest'] = KitFilmInterest::getInfoItem($assign['data']['id']);
        //'http://phim.let.vn/xem-phim-' . Common::convertStringToUrl($assign['data']['title']).'-online-film-' . $assign['data']['id'].'.let'
//        Yii::app()->clientScript->registerMetaTag('website', NULL, NULL, array('property'=>'og:type'));
        // Like
        $assign['like'] = array(
            'id' => 0,
            'like' => 0,
            'follow' => 0
        );

        if(!Yii::app()->user->isGuest)
        {
            $user_id = Yii::app()->user->id;
            $checkLike = KitFilmInterest::getDetails($user_id,$id);
            if($checkLike !== NULL)
            {
                $like = $checkLike->attributes;
                $assign['like'] = $like;
            }
        }

        $assign['episode'] = Yii::app()->request->getParam('episode', NULL);

        if(isset($assign['data']['id'])){
//            KitStats::updateValue($assign['data']['id'],'film');
            $assign['stats'] = KitStats::getDetails('film',$assign['data']['id']);
            KitStats::setStats('film',$assign['data']['id']);
        }

        $this->render('index', $assign);
	}
    public function actionEpisode(){

        $this->render('episode');
    }
}