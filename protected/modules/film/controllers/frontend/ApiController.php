<?php
class ApiController extends ControllerFrontend
{
	public function actionIndex()
	{
	}
    
    /**
     * /film/api/details/id/{$id}
     */
    public function actionDetails() {
        echo $id = Yii::app()->request->getParam('id', NULL);
        $cache_name = md5('DetailsController::actionIndex_' . $id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            // Get details

            $assign['data'] = KitFilm::getDetails($id);
    
//            // Category
//            $categorys = $assign['data']->kitFilmCategory;
//            foreach ($categorys as $category) {
//                $assign['breadcrumb'][] = KitCategory::getBreadcrumb($category->id);
//            }
            
            $assign['data'] = KitFilm::treatment($assign['data']);
            Yii::app()->cache->set($cache_name, $assign); // Set cache
        } else $assign = $cache;
        
        if (isset($assign['data']['id'])) {
            $result['query'] = 'ok';
            $result['status'] = 'Tải dữ liệu thành công.';
        } else {
            $result['query'] = 'fail';
            $result['status'] = 'Tải dữ liệu thất bại.';
        }
        $result['result'] = $assign['data'];

        echo json_encode($result);
    }
    
    /**
     * /film/api/list/type/{$type}/category/{$category}/limit/{$limit}/page/{$page}
     * http://localhost/letkit/film/api/list/type/lastest/category/3/limit/10/page/2
     */
    public function actionList() {
        $type = Yii::app()->request->getParam('type', 'lastest');
        $category = Yii::app()->request->getParam('category', NULL);
        $limit = (int) Yii::app()->request->getParam('limit', NULL);
        $page = (int) Yii::app()->request->getParam('page', 1);
        $cache_name = md5(__METHOD__ . $type . '_' . $category . '_' . $limit . '_' . $page);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            /**
             * Em code vào đây nhé
             * - $type (lastest || hot || mostPopular || promotion)
             * - $category Nếu có thì đưa vào điều kiện, nếu không có (tức là NULL) thì ko đưa vào.
             * - $limit Nếu có và > 0 thì đưa vào điều kiện
             * - $page Nếu có thì tính ra offset để đưa vào điều kiện ($offset = $limit * ($page - 1))
             * @return: trả về 1 mảng json giống như hàm details bên trên
             */
            
            
            /**
             * Điều kiện lọc và sắp xếp dữ liệu khi $type = (lastest || mostPopular)
             * Kết quả trả về mảng JSON, trong document có 1 vài chỗ ko biết lấy kết quả ở đâu:
             * - id: id
             * - title: title
             * - publish: from_time
             * - upload: creator
             * - like: 
             * - thumb: image
             * Chỗ $criteria->select nếu bỏ đi, nó sẽ lấy tất cả các Field trong table. Nhưng khi encode sẽ sinh lỗi, có thể do lỗi dấu ' của Field content
             */
            $criteria = new CDbCriteria;
            $criteria->select = 't.id, title, from_time, creator, image';
            $criteria->condition = 'status = 1';
            $criteria->order = 't.id DESC';
            
            // Hot
            if ($type == 'hot') {
                $criteria->addCondition('hot = 1');
            } elseif ($type == 'promotion') {
                $criteria->addCondition('promotion = 1');
            } elseif ($type == 'mostPopular') {
                //$criteria->addCondition($criteria);
            }
            
            $criteria->order = 'id DESC';
            
            // Category
            if ($category !== NULL) {
                $criteria->addCondition('fc.category_id = :category');
                $criteria->params = array(
                    ':category' => $category
                );
                $criteria->join = 'LEFT JOIN {{kit_film_category}} fc ON fc.film_id = t.id';        
                $criteria->group = 't.id';
            }
            
            if ($limit > 0)
                $criteria->limit = $limit;
            
            if ($page > 0)
                $criteria->offset = $limit * ($page - 1);      
                
            $assign['data'] = NULL;
            $model = KitFilm::model()->findAll($criteria);            
            if ($model != NULL)
                $assign['data'] = KitFilm::treatment($model);
            
//            // Get details
//            $assign['data'] = KitFilm::getDetails($id);
//            $assign['data'] = KitFilm::treatment($assign['data']);
            
            Yii::app()->cache->set($cache_name, $assign); // Set cache
        } else $assign = $cache;
        
        //if (isset($assign['data']['id'])) {
        if (isset($assign['data'])) {
            $result['query'] = 'ok';
            $result['status'] = 'Tải dữ liệu thành công.';
        } else {
            $result['query'] = 'fail';
            $result['status'] = 'Tải dữ liệu thất bại.';
        }
        $result['result'] = $assign['data'];

        echo json_encode($result);
    }
}