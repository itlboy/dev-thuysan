<?php
class SitemapController extends ControllerFrontend
{
	public function actionIndex()
	{
        $limit = intval(Yii::app()->request->getParam('limit', 500));
        $offset = intval(Yii::app()->request->getParam('offset', 0));
        $hascache = intval(Yii::app()->request->getParam('cache', 0));
        $hascache = $hascache > 0 ? TRUE : FALSE;
        
        Yii::import('application.vendors.libs.sitemap');
        
        $cache_name = md5(__METHOD__ . '_' . $limit . '_' . $offset);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE OR $hascache) {

            Yii::import('film.models.KitFilm');
            $criteria=new CDbCriteria;
            $criteria->select = Common::getFieldInTable(KitFilm::model()->getAttributes(), 't.');
            $criteria->condition = 't.status = 1';
            $criteria->order = 'id DESC';
            $criteria->limit = $limit;
            $criteria->offset = $offset;
            $rows = KitFilm::model()->findAll($criteria);
            $rows = KitFilm::treatment($rows);
            
            foreach ($rows as $row) {
                $items[] = array(
                    'loc' => Yii::app()->getBaseUrl(true) . $row['url'],
                    'lastmod' => date('Y-m-d'),
                    'changefreq' => 'daily',
                    'priority' => '0.8',
                );
            }
            $result = sitemap::render($items);
            
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else $result = $cache;
        
        sitemap::header();
        echo $result;
	}
}