<?php
Yii::import('application.modules.cms.models.KitStats');
class AjaxController extends ControllerFrontend
{
	public function actionIndex()
	{
		$this->render('index');
	}
    public function actionLastest()
    {
        //$category_id = Yii::app()->request->getQuery("category_id");
        $data['limit'] = Yii::app()->request->getQuery("limit");
        $data['type'] = Yii::app()->request->getQuery("type");
        $data['view'] = Yii::app()->request->getQuery("view");
        
        $this->widget('film.widgets.frontend.film_lastest',array(
            'limit' => $data['limit'],
            'type' => $data['type'],
            'view' => $data['view'],
        ));
    }

    public function actionLike()
    {
        $film_id = letArray::get($_POST,'item');
//        $id = letArray::get($_POST,'id');
        $like = letArray::get($_POST,'like');
        $follow = letArray::get($_POST,'follow');
        $is_ajax = letArray::get($_POST,'is_ajax');

        if(!Yii::app()->user->isGuest)
        {
            $user_id = Yii::app()->user->id;
            if((int)$like == 1){
                KitStats::updateValueItem($film_id,'film','like',1);
            } elseif((int)$like == 2){
                KitStats::updateValueItem($film_id,'film','dislike',1);
            }

            $checkLike = KitFilmInterest::getDetails($user_id,$film_id);
            if($checkLike === NULL)
            {
                $Interest = new KitFilmInterest();
                $Interest->user_id = $user_id ;
                $Interest->item_id = $film_id ;
                if($is_ajax == 'follow'){
                    $Interest->follow = $follow ;
                } else {
                    $Interest->like = $like ;
                    if((int)$follow == 0 AND (int)$like == 1)
                        $Interest->follow = 1 ;
                }
                if($Interest->save())
                {
                    echo json_encode(array(
                        'status' => 'true'
                    ));
                }
            } else {
                $Interest =KitFilmInterest::model()->findByPk($checkLike->id);
                $Interest->user_id = $user_id ;
                $Interest->item_id = $film_id ;
                //print_r($follow);
                if($is_ajax == 'follow'){
                    $Interest->follow = $follow ;
                } else {
                    $Interest->like = $like ;
                    if((int)$follow == 0 AND (int)$like == 1)
                        $Interest->follow = 1 ;
                }
                if($Interest->save())
                {
                    echo json_encode(array(
                        'status' => 'true'
                    ));
                }
            }
            Yii::app()->cache->delete('stats_details_film_'.$film_id);
            Yii::app()->cache->delete(md5('KitFilmInterest::getDetails_'.$film_id.'_'.Yii::app()->user->id));
        } else {
            echo json_encode(array(
                'status' => 'login'
            ));
        }

    }
    public function actionEpisode(){
        $id_item = letArray::get($_GET,'id_item');
        $title = letArray::get($_GET,'title');
        $episode_current = letArray::get($_GET,'episode_current');
        if($episode_current !== NULL){
            $this->widget('film.widgets.frontend.episodeDetails', array(
                'id' => $episode_current,
                'item_title' => $title,
            ));
        }
        $this->widget('film.widgets.frontend.episodeList', array(
            'item_id' => $id_item,
            'item_title' => $title,
            'episode_current' => $episode_current,
        ));
    }
    public function actionReport(){
        if(!Yii::app()->user->isGuest){
            $attributes = array();
            $attributes['item_id'] = letArray::get($_POST,'item');
            $attributes['episode_id'] = letArray::get($_POST,'episode');
            $attributes['title'] = letArray::get($_POST,'title');
            $attributes['content'] = letArray::get($_POST,'content');
            $attributes['creator'] = Yii::app()->user->id;
            $attributes['created_time'] = date('Y-m-d H:i:s');
            $model = KitFilmReport::model()->find('item_id=:itemId AND episode_id=:episodeId AND creator=:uid',array(
                'itemId' => $attributes['item_id'],
                'episodeId' => $attributes['episode_id'],
                'uid' => $attributes['creator'],
            ));
            if(count($model) == 0){
                $model = new KitFilmReport();
                $model->attributes = $attributes;
                if($model->save()){
                    echo json_encode(array(
                        'result' => 'true',
                        'mess' => 'Gửi thành công '
                    ));
                } else {
                    echo json_encode(array(
                        'result' => 'false',
                        'mess' => 'Có lỗi trong quá trình xử lý. Xin hảy thử lại !',
                    ));
                }
            } else {
                echo json_encode(array(
                    'result' => 'exists',
                    'mess' => 'Bạn đã gửi thông báo này rồi .',
                ));
            }

        } else {
            echo json_encode(array(
                'result' => 'none',
                'mess' => 'Bạn chưa đăng nhập'
            ));
        }
    }

}