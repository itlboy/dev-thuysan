<?php
class SearchController extends ControllerFrontend
{
	public function actionIndex()
	{
        $assign = $this->_getParam();
        $this->render('index', $assign);
	}
    
    public function actionList()
	{
        $assign = $this->_getParam();
        $this->renderPartial('list', $assign);
    }
    
    public function _getParam()
	{
        $assign['options'] = isset($_GET) ? $_GET : array();
		if (isset($assign['options']['keyword']))
        	$assign['options']['keyword'] = str_ireplace('_', ' ', $assign['options']['keyword']);
        
        if (isset($_GET['filter_length'])) {
            $assign['options'][$_GET['filter_length']] = 1;
        }
        if (isset($_GET['filter_type'])) {
            $assign['options'][$_GET['filter_type']] = 1;
        }

        return $assign;
    }
}