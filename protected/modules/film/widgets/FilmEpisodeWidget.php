<?php

class FilmEpisodeWidget extends Widget {

    public $itemId;
    public $limit = 100;
        
    public function run() {
        $moduleName = 'film';
        $modelName = 'KitFilmEpisode';
        $gridID = 'kit_film_episode_grid';

        Yii::import('application.modules.film.models.KitFilmEpisode');
        
        $criteria = new CDbCriteria;
        $criteria->condition = 'item_id = :itemId';
        $criteria->params = array(
            ':itemId' => $this->itemId
        );
        $criteria->order = 'server DESC, sorder ASC, title ASC';
        
		$dataProvider = new CActiveDataProvider($modelName, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => $this->limit,
            ),
        ));               
                
        echo '<button class="green small" onclick="ajaxDialogForm(\'\', \'Thêm tập phim\', \'' . Yii::app()->createUrl('film/episode/create', array('item' => $this->itemId)) . '\', \'auto\', \'700\'); return false;">Thêm tập phim</button>';
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => $gridID,
            'dataProvider' => $dataProvider,
//            'filter' => $model,
            'columns' => array(
                array(
                    'class' => 'CCheckBoxColumn',
                    'id' => 'chkIds',
                    'selectableRows' => 2,
                    'htmlOptions' => array('width' => '20px', 'align' => 'center')
                ),
                array(
                    'name' => 'id',
                    'value' => '$data->id',
                    'htmlOptions' => array('width' => '50px', 'align' => 'center')
                ),
                array(
                    'name' => 'title',
                    'value' => '$data->title',
                    'htmlOptions' => array('width' => '200px')
                ),
                array(
                    'name' => 'server',
                    'value' => '$data->server',
                    'htmlOptions' => array('width' => '90px')
                ),
                'link',              
                array(
                    'filter' => '',
                    'name' => 'status',
                    'type' => 'raw',
                    'value' => 'BackendFunctions::getBooleanIcon("'.$moduleName.'", "'.$modelName.'", $data->id, "status", $data->status, "'.$gridID.'")',
                    'htmlOptions' => array('width' => '50px', 'align' => 'center')
                ),
                /*
                'created_time',
                'updated_time',
                'sorder',
                'status',
                */        
                array(
                    'class' => 'CButtonColumn',
                    'template' => '{gridUpdate} {gridDelete}',
                    'buttons' => array(
                        'gridUpdate' => array(
                            'label' => Yii::t('default', 'Update'),
                            'url' => 'Yii::app()->createUrl("/film/episode/update", array("id" => $data->id))',
                            'imageUrl'=>Yii::app()->theme->baseUrl.'/images/icons/system/edit_16.png',
                            'options' => array('onclick' => "js:ajaxDialogForm('".$gridID."', 'Cập nhật', jQuery(this).attr('href'), 'auto', 700); return false")
                        ),
                        'gridDelete' => array(
                            'label' => Yii::t('default', 'Delete'),
                            'url' => 'Yii::app()->createUrl("/film/episode/delete", array("id" => $data->id))',
                            'imageUrl'=>Yii::app()->theme->baseUrl.'/images/icons/system/delete_16.png',
                            'options' => array('onclick' => "js:ajaxGridDelete('".$moduleName."', '".$modelName."', '".$gridID."', jQuery(this).attr('href'), 'status', 2); return false"),
                        ),
                    ),
                ),
            ),
        ));
        
    }
    
}
