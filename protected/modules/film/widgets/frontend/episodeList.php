<?php

class episodeList extends Widget {

    public $view = '';
    public $item_id = NULL;
    public $item_title = '';
    public $episode_current = NULL;

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
        Yii::import('film.models.KitFilmEpisode');
        Yii::import('account.models.KitAccountConfig');
    }

    public function run() {
		$this->item_id = intval($this->item_id);
        $cache_name = md5('film_episode_list_' . $this->item_id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(KitFilmEpisode::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
            $criteria->condition = Common::addWhere($criteria->condition, 't.trash = 0');
            $criteria->condition = Common::addWhere($criteria->condition, 't.item_id = ' . $this->item_id);

            $criteria->order = 't.server DESC, t.sorder ASC, t.title ASC';
            $result = KitFilmEpisode::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
            $data = $result;
        } else $data = $cache;
        
//		echo 111;
//		echo "<pre>";
//		var_dump($data);
//		echo "</pre>";
		
        if (empty($data))
            return FALSE;
        $data = KitFilmEpisode::treatment($data, $this->item_title);

        // Lay thong tin limit cua user
        if(!Yii::app()->user->isGuest){
            $unlimit = KitAccountConfig::getDetails(Yii::app()->user->id);
            if(!empty($unlimit)){
                $unlimit = KitAccountConfig::treatment($unlimit);
            }
        }

        $this->render($this->view, array(
            'data' => $data,
            'unlimit' => !empty($unlimit) ? $unlimit : NULL,
        ));
    }

}