<?php

class searchNew extends Widget {

    public $view = '';
    //public $category = NULL;
    public $title = '';
    public $limit = 20;
    public $page = 1; // $offset = $limit * ($page - 1) Nếu $page > 1 thì thêm điều kiện cho offset. Không dùng CPagination
    public $options = array(
        'category' => NULL, // ID danh mục
        'length_0' => 0, // Phim lẻ: 0 là không, 1 là có
        'length_1' => 0, // Phim bộ: 0 là không, 1 là có
        'theater' => 0, // Chiếu rạp: 1 là có, 0 là không đưa vào điều kiện
        'hot' => 0, // Phim hot: 1 là có, 0 là không đưa vào điều kiện
        'promotion' => 0, // Phim đề cử: 1 là có, 0 là không đưa vào điều kiện
        'keyword' => '', // Từ khóa so sánh với trường title, title_english, tags, intro, content, rỗng thì không cho vào điều kiện

        'order_by_direction' => 'lastest',
        'creator' => NULL,
    );

    private $_optionsDefault = array(
        'category' => NULL,
        'length_0' => 0,
        'length_1' => 0,
        'theater' => 0,
        'hot' => 0,
        'promotion' => 0,
        'keyword' => '',
        'order_by_direction' => 'lastest',
        'creator' => NULL,
    );

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
        Yii::import('film.models.KitFilm');
        $this->options = array_merge($this->_optionsDefault, $this->options);
        $this->options['length_0'] = intval($this->options['length_0']);
        $this->options['length_1'] = intval($this->options['length_1']);
//        if ($this->options['length_0'] == 0 AND $this->options['length_1'] == 0)
//            $this->options['length_0'] = $this->options['length_1'] = 1;

        $this->options['creator'] = (!empty($this->options['creator'])) ? intval($this->options['creator']) : NULL;
    }

    public function run() {
        /**
         * vào http://localhost/letkit/hanh-dong-c43.html để check kết quả
         * Phân trang tham khảo: http://www.yiiframework.com/extension/yiinfinite-scroll/ (tính sau cùng)
         * Widget này có nhiệm vụ get dữ liệu theo các tham số truyền vào
         * tham số chủ yếu lấy từ mảng $this->options
         * nếu length_0 = 1 thì lấy phim lẻ, length_1 = 1 là lấy cả phim bộ. Đây là tập hợp chứ không phải chọn 1 trong 2. Nên nếu tồn tại cả 2 giá trị thì sẽ lấy cả phim lẻ và phim bộ.
         * theater, hot, promotion nếu có thì đưa vào điều kiện
         * keyword so sánh với trường title, title_english, tags, intro, content, rỗng thì không cho vào điều kiện
         */

        $offset = 0;
        $params = array();
        $criteria = new CDbCriteria;
        $criteria->select = 't.*';
        $criteria->condition = 't.status = 1';

        if ($this->options['creator'] !== NULL) {
            $criteria->addCondition ('creator = ' . $this->options['creator']);
        }

        // length
        if ($this->options['length_0'] == 1 AND $this->options['length_1'] == 0) {
            $criteria->addCondition ('length = 0');
        } elseif ($this->options['length_0'] == 0 AND $this->options['length_1'] == 1) {
            $criteria->addCondition ('length = 1');
        } //ngược lại nếu length đều =1 hoặc đều =0 thì ko đưa vào dk lọc
        if ($this->options['theater'] == 1)
            $criteria->addCondition ('theater = 1');
        if ($this->options['hot'] == 1)
            $criteria->addCondition ('hot = 1');
        if ($this->options['promotion'] == 1)
            $criteria->addCondition ('promotion = 1');

        if (isset($this->options['keyword']) AND $this->options['keyword'] != '') {
            $keyword = addcslashes($this->options['keyword'], '%_');
            $criteria->addCondition('title LIKE :keyword OR title_english LIKE :keyword OR tags LIKE :keyword OR intro LIKE :keyword');
            $params[':keyword'] = "%$keyword%";
        }

        // Category
        if ($this->options['category'] !== NULL AND (int)$this->options['category'] > 0) {
            $category = (int)$this->options['category'];
            $catListObject = KitCategory::getListInParent('film', $category);
            $catList = array($category);
            foreach ($catListObject as $cat) {
                $catList[] = $cat->id;
            }
            $criteria->join = "INNER JOIN {{kit_film_category}} t2 ON t.id=t2.film_id";
            $criteria->addCondition('t2.category_id IN ('.implode(',', $catList).')');
        }

        if ( !isset($this->options['order_by_direction']))
            $this->options['order_by_direction'] = 'lastest';
        if ($this->options['order_by_direction'] == 'oldest') {
            $criteria->order = 't.id ASC';
        } elseif ($this->options['order_by_direction'] == 'random') {
            $criteria->order = 'RAND()';
        } else
            $criteria->order = 't.id DESC';

        if (!empty($params))
            $criteria->params = $params;

        $criteria->group = 't.id';

        $total = KitFilm::model()->count($criteria);

        $criteria->limit = $this->limit;

        if (isset($_POST['offset'])) {
            $offset = (int) $_POST['offset'] + $this->limit;
        }
        $criteria->offset = $offset;

        $assign['url'] = Yii::app()->request->requestUri;
        $assign['offset'] = $offset;
        $assign['total'] = $total;
        $assign['rows'] = KitFilm::model()->findAll($criteria);
        if ($assign['rows'] !== array())
            $assign['rows'] = KitFilm::treatment($assign['rows']);

        $baseUrl = Yii::app()->theme->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        $cs->registerScriptFile($baseUrl . '/plugins/scrollPaging/js/jquery.tcScrollPaging.js');
        $cs->registerCssFile($baseUrl . '/plugins/scrollPaging/css/style.css');

        $cs->registerScript('FilmSearch', "
            jQuery(function(){
                jQuery('#widget-comment-wrapper').tcScrollPaging({
                    'heightOffset': 100,
                    //'pageUrl': jQuery('.comment-url').last().text(),
                    'pageOffset': " . $offset . ",
                    'scrollFinishMsg': 'Đã load xong'
                });
            });
        ");
        $this->render($this->view, $assign);
    }
}
