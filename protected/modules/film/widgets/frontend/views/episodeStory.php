<div class="episode">
    <div class="watch-sidebar-body">
        <div id="watch-context">
            <h4 class="context-head">
                <div class="context-link" >
                    <?php if(Yii::app()->user->getState('level')): ?>
                    <img class="context-icon-guide-feed" style="float:left; margin-right: 10px;" src="<?php echo KitAccountLevel::getField(Yii::app()->user->getState('level'),'image_url') ?>" alt="">
                    <?php endif; ?>
                    <strong class="context-title" style="font-size: 18px" dir="ltr">Tập truyện</strong>
                </div>
            </h4>
            <div class="yt-horizontal-rule "><span class="first"></span><span class="second"></span><span class="third"></span></div>

            <div class="context-body">
                <table border="0" cellspacing="1" cellpadding="0">
                <tr>
                    <td class="episode_source" width="1%"><?php echo $data[0]['server'] ?></td>
                <td>
                <div class="yt-uix-pager">
                    <?php foreach ($data as $key => $value): ?>
                    <!--  yt-uix-button-toggled &nbsp; -->

                    <a rel="nofollow" data-item="<?php echo $this->item_id; ?>" data-episode="<?php echo $value['id']; ?>" href="<?php echo $value['url']; ?>" class="yt-uix-button yt-uix-pager-page-num yt-uix-pager-button yt-uix-button-default<?php if ($this->episode_current == $value['id']): ?> yt-uix-button-toggled<?php endif; ?>"><span class="yt-uix-button-content"><?php echo $value['title']; ?></span></a>

                    <?php if ($key < (count($data)-1) AND $data[$key]['server'] !== $data[$key+1]['server']): ?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="episode_source" width="1%"><?php echo $data[$key+1]['server']?></td>
                                    <td>
                                        <div class="yt-uix-pager">
    <?php endif; ?>
                    <?php endforeach; ?>
                </div>
                </td>
                </tr>
                </table>
            </div>
        </div>
    </div>
</div>
