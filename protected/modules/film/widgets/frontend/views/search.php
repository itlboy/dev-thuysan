<?php
$width = 190;
$height = 190;
?>
<div style="text-align: right; margin: -22px 0 5px 0;">Tìm thấy <?php echo $total; ?> kết quả.</div>
<div id="posts">
    <?php foreach ($rows as $row): ?>
        <div class="post">
            
            <div class="item" style="width: <?php echo $width; ?>px; height: <?php echo $height; ?>px;">
                <div class="item_content">
                    <div class="item_info">
                        <div class="item_button">
        <!--                    <div class="item_row">
                                <?php if (rand(0,1) == 0): ?>
                                <span class="label label-info"><i class="icon-thumbs-up icon-white"></i> Like</span>
                                <?php else: ?>
                                <span class="label label-inverse"><i class="icon-thumbs-down icon-white"></i> Dislike</span>
                                <?php endif; ?>
                            </div>-->
                            <div class="item_row">
                                <?php if (rand(0,1) == 0): ?>
                                <span class="label label-info"><i class="icon-ok icon-white"></i> Theo đuôi</span>
                                <?php else: ?>
                                <span class="label label-inverse"><i class="icon-remove icon-white"></i> Hủy theo</span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="item_stats">
                            <div><i class="icon-eye-open"></i> <?php echo number_format($row['view_count'], 0, ',', '.'); ?></div>
                            <div><i class="icon-thumbs-up"></i> <?php echo number_format(rand(100, 50000), 0, ',', '.'); ?></div>
                            <div><i class="icon-thumbs-down"></i> <?php echo number_format(rand(100, 50000), 0, ',', '.'); ?></div>
                            <div><i class="icon-comment"></i> <?php echo number_format(rand(100, 50000), 0, ',', '.'); ?></div>
                        </div>
                    </div>
                    <div class="item_thumb">
                        <img class="lazy" alt="" src="<?php echo Common::getImageUploaded('film/medium/' . $row['image']); ?>" />
                        <?php if ($row['length'] == '1'): ?>
                            <?php if(!empty($row['episode_current']) OR !empty($row['episode_total'])): ?>
                                <span class="label label-inverse">Tập <?php echo !empty($row['episode_current']) ? $row['episode_current'] : '?' ?><?php echo !empty($row['episode_total']) ? '/'.$row['episode_total'] : ''; ?></span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                    <div class="item_title"><a href="<?php echo $row['url']; ?>"><?php echo $row['title']; ?> <?php if (isset($row['title_english']) AND $row['title_english'] !== '' AND $row['title_english'] !== $row['title']) echo '(' . $row['title_english'] . ')'; ?></a></div>
                </div>
            </div>

        </div>
    <?php endforeach; ?>
</div>
<?php
$this->widget('ext.yiinfinite-scroll.YiinfiniteScroller', array(
    'contentSelector' => '#posts',
    'itemSelector' => 'div.post',
    'loadingText' => 'Loading...',
    'donetext' => 'This is the end... my only friend, the end',
    'pages' => $pages,
));
?>