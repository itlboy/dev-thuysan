<?php if($this->tabs !== array()): ?>
<?php $divId = 'result_' . rand(1000000, 9999999); ?>
<script type="text/javascript">
    $('#<?php echo $divId ?>_tabs li').live('click',function(){
        var link = $(this).data("link");
        ajaxLoadFile(link,'<?php echo $divId; ?>');
        $('#<?php echo $divId ?>_tabs li').removeClass('active');
        $(this).addClass('active');
        return false;
    });
</script>

<div class="heading_s1 heading_tabs"><?php echo $this->title; ?></div>
<div id="<?php echo $divId ?>_tabs">
    <?php $this->widget('bootstrap.widgets.TbMenu', array(
        'type' => 'tabs', // '', 'tabs', 'pills' (or 'list')
        'stacked' => false, // whether this is a stacked menu
        'items' => $this->tabs ,
    )); ?>
</div>
<div id="<?php echo $divId; ?>">
<?php endif; ?>
    <ul class="thumbnails thumbnails_s1 list_s1 let_kit_film_top" >
        <?php $i = 1; ?>
        <?php foreach($data as $item): ?>
        <li class="thumb_s1 row">
            <a data-title="<?php echo $item["title"]; ?>" rel="tooltip" class="thumbnail" href="<?php echo $item['url']; ?>" data-original-title="">
                <img alt="" height="185" src="<?php echo Common::getImageUploaded('film/medium/' . $item['image']); ?>" />
                <?php if(intval($item['length']) == 1): ?>
                    <?php if(!empty($item['episode_current']) OR !empty($item['episode_total'])): ?>
                        <span class="sp-episode"><?php echo !empty($item['episode_display']) ? 'Tập '.$item['episode_display'] : '' ?></span>
                    <?php endif; ?>
                <?php endif; ?>
            </a>
            <a href="<?php echo $item['url']; ?>">
                <div class="line_info"><?php echo $item["title"] ?></div>
    <!--            <div class="line_info">A Gentleman's Dignity</div>-->
            </a>
        </li>
        <?php if($i % 3 == 0 and $i > 0) : ?>
            <li style="width:100%;"></li>
        <?php endif; ?>
        <?php $i++; ?>
        <?php endforeach; ?>
    </ul>
    <div class="line_s1"></div>
<?php if($this->tabs !== array()): ?>
</div>
<?php endif; ?>

