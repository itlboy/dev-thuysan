<?php if($this->tabs !== array()): ?>
<?php $divId = 'result_' . rand(1000000, 9999999); ?>
<script type="text/javascript">
    $('#<?php echo $divId ?>_tabs a').live('click',function(){
        var link = $(this).data("link");
        ajaxLoadFile(link,'<?php echo $divId; ?>');
        $('#<?php echo $divId ?>_tabs a').removeClass('active');
        $(this).addClass('active');
        return false;
    });
</script>
<div id="<?php echo $divId; ?>_tabs">
<?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
    'htmlOptions' =>array(),
    'buttons'=> $this->tabs,
)); ?>
</div>
<div class="line_s1"></div>
<div id="<?php echo $divId; ?>">
<?php endif; ?>
    <div class="video-sidebar">
        <ul class="let_kit_film_hot" >
            <?php foreach ($data as $item): ?>
            <li class="video-list-item recommended-video-item">
                <a class="related-video yt-uix-sessionlink" href="<?php echo $item['url']; ?>">
                    <span class="ux-thumb-wrap contains-addto ">
                        <span class="video-thumb ux-thumb thumb_s2">
                            <span class="yt-thumb-clip">
                                <span class="yt-thumb-clip-inner">
                                    <img width="" alt="<?php echo $item["title"]; ?>" src="<?php echo Common::getImageUploaded('film/small/' . $item['image']); ?>">
                                    <span class="vertical-align"></span>
                                </span>
                            </span>
                        </span>
                        <?php if(intval($item['length']) == 1): ?>
                            <?php if(!empty($item['episode_current']) OR !empty($item['episode_total'])): ?>
                                <span class="video-time"><?php echo !empty($item['episode_display']) ? $item['episode_display'] : '' ?></span>
                            <?php endif; ?>
                        <?php endif; ?>
<!--                        <button role="button" class="addto-button video-actions addto-watch-later-button yt-uix-button yt-uix-button-default yt-uix-button-short yt-uix-tooltip" type="button" title="Xem sau" onclick=";return false;" data-tooltip-text="Xem sau">
                            <span class="yt-uix-button-content">
                                <span class="addto-label"> Xem sau </span>
                                <span class="addto-label-error"> Lỗi </span>
                                <img src="//s.ytimg.com/yt/img/pixel-vfl3z5WfW.gif">
                            </span>
                        </button>-->
                    </span>
                    <span style="color: #333; font-weight: bold;" title="<?php echo $item["title"]; ?><?php echo !empty($item['episode_display']) ? ' - Tập '.$item['episode_display'] : '' ?>" class="title" dir="ltr"><?php echo $item["title"]; ?><?php echo !empty($item['episode_display']) ? ' - Tập '.$item['episode_display'] : '' ?></span>
                    <span class="stat view-count" style="white-space: normal;"> <?php echo letText::limit_chars($item['intro'], 50); ?></span></a>
<!--                    <span class="stat attribution">bởi <span dir="ltr" class="yt-user-name ">admin</span></span>
                    <span class="stat view-count"> <?php echo number_format($item['view_count'],0,'.',','); ?> lượt xem</span></a>-->
<!--                    <span class="recommended-video-dismiss">-->
<!--                        <img alt="xĂ³a" src="//s.ytimg.com/yt/img/pixel-vfl3z5WfW.gif" class="close-small">-->
<!--                    </span>-->
            </li>
            <?php endforeach ; ?>
        </ul>
    </div>
<?php if($this->tabs !== array()): ?>
</div>
<?php endif; ?>
