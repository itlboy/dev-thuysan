<?php
$width = '10%';
$height = '31%';
$cs = Yii::app()->getClientScript();
$cs->registerCssFile(Yii::app()->theme->baseUrl.'/css/style_mobile.css',CClientScript::POS_END);
?>
<div style="text-align: right; margin: -22px 0 5px 0;">Tìm thấy <?php echo $total; ?> kết quả.</div>
<div id="widget-comment-wrapper">
    <div class="comment-items">
        <?php foreach ($rows as $row): ?>
        <a href="<?php echo $row['url']; ?>">
            <div class="comment-item">
                <div class="cont01">
                    <div class="imgAl">
                        <img width="96" alt="" src="<?php echo Common::getImageUploaded('film/large/' . $row['image']); ?>"/>
                    </div>
                    <div class="txtAlbum"><p><?php echo $row['title']; ?> <?php if (isset($row['title_english']) AND $row['title_english'] !== '' AND $row['title_english'] !== $row['title']) echo '(' . $row['title_english'] . ')'; ?></p>
                        <?php if ($row['length'] == '1'): ?>
                            <?php if(!empty($row['episode_current']) OR !empty($row['episode_total'])): ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        <span class="label label-inverse">Tập <?php echo !empty($row['episode_display']) ? $row['episode_display'] : '' ?></span>
                    </div>
                </div>
                <br class="clr">
            </div>
        </a>
        <?php endforeach; ?>
        <a href="javascript:;" style="display:none" class="comment-url" rel="<?php echo $url; ?>">URL</a>
        <a href="javascript:;" style="display:none" class="comment-offset" rel="<?php echo $offset; ?>">OFFSET</a>
    </div>
    <div class="comment-load"></div>
    <div class="comment-finish"></div>
</div>

