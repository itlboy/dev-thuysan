<div class="grid_24 padding_bottom">
    <div class="box_s1 clearfix border_red bg_white">
        <div class="clearfix">
            <div class="grid_2 alpha title"><?php echo $this->title; ?></div>
<!--            <div class="grid_16 omega box_link_extra fr" style="text-align: right;">
                <a href="javascript:void(0):">Hành động</a><span>|</span>
                <a href="javascript:void(0):">Phiêu lưu</a><span>|</span>
                <a href="javascript:void(0):">Hoạt hình</a><span>|</span>
                <a href="javascript:void(0):">Hài hước</a><span>|</span>
                <a href="javascript:void(0):">Gia đình</a><span>|</span>
                <a href="javascript:void(0):">Phá án</a><span>|</span>
                <a href="javascript:void(0):">Tâm lý</a><span>|</span>
                <a href="javascript:void(0):">Kinh dị</a><span>|</span>
                <a href="javascript:void(0):">...</a>
            </div>
-->        </div>
        <div class="mainContent">
            <!-- Video list -->
            <div class="grid_24 alpha omega list_video_s1">
                <ul class="list maggin_left_20">
                	<?php foreach($data as $key => $item): ?>
                        <li class="item clearfix<?php if ($key % 6 == 5): ?> last<?php endif; ?>">
                            <div class="thumb mouseover relative">
                                <a title="" href="<?php echo $item["url"]; ?>">
                                    <img alt="" src="<?php echo Common::getImageUploaded('film/medium/' . $item['image']); ?>" width="130" height="160" />
                                </a>
                                <?php if (!empty($item['episode_display'])): ?>
                                <div class="thumb_title opacity">
                                    <div style="padding: 0 5px;">Tập <?php echo $item['episode_display']; ?></div>
                                </div>
                                <?php endif; ?>
                            </div>
                            <div class="item_txtbox">
                                <h6>
                                    <a class="item_title" title="<?php echo $item["title"]; ?>" href="<?php echo $item["url"]; ?>"><?php echo $item["title"]; ?></a>
                                </h6>
                                <p><span><a target="_blank" href="<?php echo $item["url"]; ?>" class="gray" title="<?php echo $item["title_english"]; ?>"><?php echo $item["title_english"]; ?></a></span></p>
                                <p>
                                    <span class="icon_play"><s title="">&nbsp;</s><em><?php echo number_format($item['view_count'],0,'.',','); ?></em></span>
                                </p>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <!-- END Video list -->
        </div>
    </div>
</div>
                <?php if(intval($item['length']) == 1): ?>
                    <?php if(!empty($item['episode_current']) OR !empty($item['episode_total'])): ?>
                        <!--<span class="sp-episode"><?php echo !empty($item['episode_display']) ? 'Tập '.$item['episode_display'] : '' ?></span>-->
                    <?php endif; ?>
                <?php endif; ?>
