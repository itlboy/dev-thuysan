<?php
$time_limit = time();
if(isset($unlimit) AND !empty($unlimit['film_unlimit'])){
    $time_limit = strtotime($unlimit['film_unlimit']);
}
?>

    <div class="grid_16 alpha omega maggin_bottom_15">
        <div class="box_s1 clearfix bg_white padding_20">
            <table border="0" cellspacing="1" cellpadding="0" class="episode_list">
                <tbody>
                    <tr>
                        <td class="episode_source column1" width="1%"><?php echo $data[0]['server'] ?></td>
                        <td>
<?php foreach ($data as $key => $value): ?>
<?php
//    var_dump($value['id']);
$class = array('button');
if (!empty($this->episode_current) AND $this->episode_current == $value['id']) {
    $class[] = 'active';
}
$class = implode(' ', $class);
?>
                            <a rel="nofollow" href="<?php echo $value['url']; ?>" class="<?php echo $class; ?>"><span><?php echo $value['title']; ?></span></a>
	<?php if ($key < (count($data)-1) AND $data[$key]['server'] !== $data[$key+1]['server']): ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="episode_source column1" width="1%"><?php echo $data[$key+1]['server']?></td>
                        <td>
	<?php endif; ?>
<?php endforeach; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
