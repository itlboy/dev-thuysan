<div class="let_left clearfix">
    <div class="let_column">
        <div class="let_column_inner">
            <div class="let_featured_highlight">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/samples/tu-tu21.jpg" alt="tu-tu21.jpg" />
                <div class="let_title"><a href="#">Ba người tự tử vì vỡ hụi 50 tỉ đồng</a></div>
                <div class="let_description">Ngày 16/3, Công an tỉnh Lâm Đồng cho biết đã nhận được 26 đơn tố cáo vợ chồng Trần Văn Đẳng và Bùi Thị Mỹ Dung (tạm trú phường 9, TP Đà Lạt) có hành vi lừa đảo chiếm đoạt 50 tỉ đồng.</div>
                <div class="let_related">
                    <a href="#">Phá đường dây buôn người xuyên quốc gia </a>
                </div>
            </div>
        </div>
    </div>

    <div class="let_column">
        <div class="let_column_inner">
            <div class="let_featured_list clearfix">
                <div class="let_featured_list_header clearfix">
                    <span>Tin mới</span>
                </div>
                <ul>
                    <?php foreach ($data as $key => $value): ?>
                        <li><a href="<?php echo $value['url']; ?>"><?php echo $value['title']; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>