<?php

class lastest extends Widget {

    public $view = '';
    public $category = NULL;
    public $title = '';
    public $limit = 10;
    public $where = array();

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
        Yii::import('article.models.KitArticle');
    }

    public function run() {
        $data = KitArticle::getLastest($this->category, $this->where);
        if (empty($data))
            return FALSE;
        $data = KitArticle::treatment($data);
        $data = array_slice($data, 0, $this->limit);
        $this->render($this->view, array(
            'data' => $data,
        ));
    }

}