<?php

Yii::import('application.components.backend.BackendFunctions');
class Episode extends Widget {

    public $itemId;
    public $limit = 100;
    public $params = array();

    public function run() {
        $moduleName = 'film';
        $modelName = 'KitFilmEpisode';
        $gridID = 'kit_film_episode_grid';

        Yii::import('application.modules.film.models.KitFilmEpisode');
        
        $model = new KitFilmEpisode;

        $criteria = new CDbCriteria;
        $criteria->condition = 'item_id = :itemId';
        $criteria->params = array(
            ':itemId' => $this->itemId
        );

        $criteria->order = '';
        $criteria->order .= 'server DESC,';
        $criteria->order .= 'sorder ASC, title ASC';
        
        if (isset($this->params['KitFilmEpisode']) AND !empty($this->params['KitFilmEpisode'])) {
            $model->attributes = $this->params['KitFilmEpisode'];            
            foreach (array_keys($this->params['KitFilmEpisode']) as $field) {
                $criteria->compare('t.'.$field, $model->$field, true);
            }
        }

		$dataProvider = new CActiveDataProvider($modelName, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->request->getQuery('pageSize', $this->limit),
            ),
        ));

        $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'buttons' => array(
                array(
                    'label' => 'Thêm mới',
                    'url' => 'javascript:;',
                    'htmlOptions' => array('onclick' => "ajaxDialogForm('', 'Thêm mới', '" . Yii::app()->createUrl('film/episode/ajaxCreate', array('item' => $this->itemId)) . "', 'auto', '550'); return false")
                ),
                array(
                    'label' => 'Lọc dữ liệu',
                    'url' => 'javascript:;',
                    'htmlOptions' => array('onclick' => "jQuery.fn.yiiGridView.update('" . $gridID . "', {data: jQuery(this).parent().find('.filters').find('input').serialize()}); return false")
                ),
                array(
                    'label' => 'Reset bộ lọc',
                    'url' => 'javascript:;',
                    'htmlOptions' => array('onclick' => "jQuery.fn.yiiGridView.update('" . $gridID . "'); return false")
                ),
            ),
        ));

        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => $gridID,
            'dataProvider' => $dataProvider,
            'filter' => $model,
            'ajaxUrl' => Yii::app()->createUrl('//film/episode/filter', array('item' => $this->itemId)),
            'columns' => array(
                array(
                    'class' => 'CCheckBoxColumn',
                    'id' => 'chkIds',
                    'selectableRows' => 2,
                    'htmlOptions' => array('width' => '20px', 'align' => 'center')
                ),

                array(
                    'name' => 'id',
                    'value' => '$data->id',
                    'htmlOptions' => array('width' => '50px', 'align' => 'center')
                ),
            
                array(
                    'name' => 'title',
                    'value' => '$data->title',
                    'htmlOptions' => array('width' => '200px')
                ),
                        
                array(
                    'name' => 'server',
                    'value' => '$data->server',
                    'htmlOptions' => array('width' => '90px')
                ),
                        
                array(
                    'name' => 'link',
                    'value' => '$data->link'
                ),
                        
                array(
                    'filter' => '',
                    'name' => 'vip',
                    'value' => '$data->vip',
                    'htmlOptions' => array('width' => '50px', 'align' => 'center')
                ),
                
                array(
                    'filter' => '',
                    'name' => 'limit',
                    'type' => 'raw',
                    'value' => 'BackendFunctions::getBooleanIcon("'.$moduleName.'", "'.$modelName.'", $data->id, "limit", $data->limit, "'.$gridID.'")',
                    'htmlOptions' => array('width' => '50px', 'align' => 'center')
                ),
                        
                array(
                    'filter' => '',
                    'name' => 'trash',
                    'type' => 'raw',
                    'value' => 'BackendFunctions::getBooleanIcon("'.$moduleName.'", "'.$modelName.'", $data->id, "trash", $data->trash, "'.$gridID.'")',
                    'htmlOptions' => array('width' => '50px', 'align' => 'center')
                ),
            
                /*
                'created_time',
                'updated_time',
                'sorder',
                'status',
                */
                array(
                    'class' => 'CButtonColumn',
//                    'header' => CHtml::dropDownList('pageSize',
//                        Yii::app()->request->getQuery('pageSize', $this->limit),
//                        array(5 => 5, 10 => 10, 20 => 20, 30 => 30, 40 => 40, 50 => 50, 100 => 100, 200 => 200, 500 => 500, 1000 => 1000),
//                        array('onchange' => "jQuery.fn.yiiGridView.update('".$gridID."',{ data:{pageSize: jQuery(this).val() }})")
//                    ),
                    'template' => '{gridUpdate} {gridDelete}',
                    'buttons' => array(
                        'gridUpdate' => array(
                            'label' => Yii::t('default', 'Update'),
                            'url' => 'Yii::app()->createUrl("/film/episode/ajaxUpdate", array("id" => $data->id, "ajax" => "'.$gridID.'"))',
                            'imageUrl'=>Yii::app()->theme->baseUrl.'/images/icons/system/edit_16.png',
                            'options' => array('onclick' => "js:ajaxDialogForm('".$gridID."', 'Cập nhật', jQuery(this).attr('href'), 'auto', 700); return false")
                        ),
                        'gridDelete' => array(
                            'label' => Yii::t('default', 'Delete'),
                            'url' => 'Yii::app()->createUrl("/film/episode/delete", array("id" => $data->id, "ajax" => "'.$gridID.'"))',
                            'imageUrl'=>Yii::app()->theme->baseUrl.'/images/icons/system/delete_16.png',
                            'options' => array('onclick' => "js:ajaxGridDelete('".$moduleName."', '".$modelName."', '".$gridID."', jQuery(this).attr('href'), 'status', 2); return false"),
                        ),
                    ),
                ),
            ),
        ));
    }
}
