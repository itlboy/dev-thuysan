<?php

class episodeDetails extends Widget {

    public $view = '';
    public $id = NULL;
    public $item_title = '';

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
        Yii::import('film.models.KitFilmEpisode');
    }

    public function run() {
        $this->id = intval($this->id);
        $data = KitFilmEpisode::getDetails($this->id);
        if (empty($data))
            return FALSE;
        $data = KitFilmEpisode::treatment($data, $this->item_title);

        // Lay thong tin limit cua user
        if(!Yii::app()->user->isGuest){
            $unlimit = KitAccountConfig::getDetails(Yii::app()->user->id);
            if(!empty($unlimit)){
                $unlimit = KitAccountConfig::treatment($unlimit);
            }
        }

        $this->render($this->view, array(
            'data' => $data,
            'unlimit' => !empty($unlimit) ? $unlimit : NULL,
        ));
    }

}