<?php

class film_lastest extends Widget {

    public $view = '';
    public $category = NULL;
    public $title = '';
    public $limit = 10;
	public $where = '';
	public $order_by = 'id DESC';
    public $group_count = 0;

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
        Yii::import('film.models.KitFilm');
    }

    public function run() {
        $cache_name = md5(__METHOD__ . '_' . $this->category.'_'.$this->where.'_'.$this->order_by);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            //$criteria->select = Common::getFieldInTable(KitFilm::model()->getAttributes(), 't.');
            //$criteria->addCondition('t.status = 1');
            
            $criteria->select = 't.*';
            $criteria->condition = 't.status = 1';

            if ($this->category !== NULL AND $this->category >= 0) {
                $catListObject = KitCategory::getListInParent('film', $this->category);
                $catList = array($this->category);
                foreach ($catListObject as $cat) {
                    $catList[] = $cat->id;
                }
                $criteria->join = "INNER JOIN {{kit_film_category}} t2 ON t.id=t2.film_id";
                $criteria->addCondition('t2.category_id IN ('.implode(',', $catList).')');
            }                      
            
            if ($this->where !== '') {
                $criteria->addCondition($this->where);
            }
			
            $criteria->order = $this->order_by;
            $criteria->limit = 24;
            $data = KitFilm::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $data); // Set cache
        } else $data = $cache;
        if (empty($data))
            return FALSE;
        $data = KitFilm::treatment($data);
        $data = array_slice($data, 0, $this->limit);
        $this->render($this->view, array(
            'data' => $data
        ));

    }

}