<?php
class LastestArticleWidget extends BaseBlockWidget {
    var $title = "Lấy danh sách bài viết mới nhất";
    var $views = array(
        "lastestDefault" => "Hiển thị danh sách dạng list",
        "lastestBlog"    => "Hiển thị danh sách dạng blog",
    );
    var $activatedView = "lastestDefault"; // view được sử dụng
    var $catId = 0; // lấy theo 1 danh mục cụ thể, 0: Lấy theo tất cả các danh mục
    var $limit = 20; // giới hạn số lượng tin được lấy
    var $paging = FALSE; // có hiển thị phân trang hay không
    
    public function run() {
        $articles = $this->getArticles();
        
        $this->render($this->activatedView, array(
            "articles" => $articles,
        ));
    }
    
    public function getArticles() {
        Yii::import('application.modules.article.models.KitArticle');
        $criteria = new CDbCriteria();
        if($catId = intval($this->catId))
        {
            $criteria->join = "INNER JOIN {{kit_article_category}} t2 ON t.id=t2.article_id";
            $criteria->condition = "t2.category_id=".$catId;
        }
        
        $criteria->order = "id DESC";
        $criteria->limit = $this->limit;
        return KitArticle::model()->findAll($criteria);
    }
}
?>