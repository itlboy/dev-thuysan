<?php echo $form->errorSummary($model); ?>
<div id="message"></div>

<div class="box grid_16" style="margin-top: 10px;">
    <div class="toggle_container">
        <div class="block">
            <h2 class="section">SEO</h2>
            
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'seo_title'); ?><span>Ký tự nhỏ hơn 70</span></label>
                <div>
                    <?php echo $form->textField($model, 'seo_title', array('size' => 30, 'maxlength' => 70, 'class' => 'tooltip autogrow', 'title' => 'Nhập tiêu đề', 'placeholder' => 'Tiêu đề thể hiện trên kết quả tìm kiếm...')); ?>
                    <?php echo $form->error($model, 'seo_title'); ?>
                </div>
            </fieldset>
                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'seo_url'); ?><span>Ký tự nhỏ hơn 255</span></label>
                <div>
                    <?php echo $form->textField($model, 'seo_url', array('size' => 30, 'maxlength' => 255, 'class' => 'tooltip autogrow', 'title' => 'Nhập Alias', 'placeholder' => 'Nhập Alias để tạo link...')); ?>
                    <?php echo $form->error($model,'seo_url'); ?>
                </div>
            </fieldset>
                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'seo_desc'); ?><span>Ký tự nhỏ hơn 160</span></label>
                <div class="clearfix">
                    <?php echo $form->textArea($model, 'seo_desc', array('class' => 'tooltip', 'title' => 'Mô tả ngắn được hiển thị trên kết quả tìm kiếm')); ?>
                    <?php echo $form->error($model, 'seo_desc'); ?>
                </div>
            </fieldset>
            
        </div>
    </div>
</div>
