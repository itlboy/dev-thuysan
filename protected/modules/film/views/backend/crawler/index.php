<div class="grid_16">
    <button id="btnSaveFilm" class="green small" onclick="return saveData();" >
        <div class="ui-icon ui-icon-check"></div>
        <span><?php echo Yii::t('global', 'Save'); ?></span>
    </button>
<!--    <button class="green small" >-->
<!--        <div class="ui-icon ui-icon-check"></div>-->
<!--        <span>--><?php //echo Yii::t('global', 'Apply'); ?><!--</span>-->
<!--    </button>-->

<!--    <button class="blue small" >-->
<!--        <img src="--><?php //echo Yii::app()->theme->baseUrl; ?><!--/images/icons/small/white/chart_8.png" />-->
<!--        <span>--><?php //echo Yii::t('global', 'View Info and stats'); ?><!--</span>-->
<!--    </button>-->
<!--    <button class="red small">-->
<!--        <div class="ui-icon ui-icon-trash"></div>-->
<!--        <span>--><?php //echo Yii::t('global', 'Trash'); ?><!--</span>-->
<!--    </button>-->
</div>

<span style="letter-spacing: normal; margin-left: 10px; margin: 10px;" id="lbconfirm"></span>

<!-- Title -->
<div class="flat_area grid_16" style="opacity: 1;">
    <h2>Film</h2>

</div>
<!-- END Title -->
<div class="box grid_16" style="opacity: 1;">
    <h2 class="box_head">Form Elements</h2>
    <a class="grabber" href="#">&nbsp;</a>
    <a class="toggle" href="#">&nbsp;</a>
    <div class="toggle_container">
        <div class="block" style="opacity: 1;">
            <h2 class="section">Search Film</h2>

            <div class="columns clearfix">
                <div class="col_50">
                    <fieldset class="label_side">
                        <label>Site : </label>
                        <div>
                            <select class="select_box" style="opacity: 0;" id="sl-list">
                                <option value="youtube.com">Youtube.com</option>
                            </select>
                        </div>
                    </fieldset>
                </div>
                <div class="col_50">
                    <fieldset class="label_side">

                        <label>ID Film : </label>
                        <div>
                            <input type="text" class="text" id="film_id">
                            <div style="float: left;font-size: 11px;font-style: italic;color: #999;margin-top: 5px;" id="confirmId">

                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <fieldset class="label_side">
                <label>Tên phim :<span>quy định : tên phim tập {{epsiode}}</span></label>
                <div>
                    <input type="text"  class="text" id="keyword" value="Tan Thuy Hu 2011 Ep0{{episode}} Vietsub">
                    <span style="float: left;font-size: 11px;font-style: italic;color: #999;margin-top: 2px" > Ví dụ :Tan Thuy Hu 2011 Ep0{{episode}} Vietsub</span>
                </div>
            </fieldset>
            <fieldset class="label_side">
                <label>Từ tập :<span></span></label>
                <div>
                    <input type="text" class="text" id="episode_from">
                </div>
            </fieldset>
            <fieldset class="label_side">
                <label>Đến tập :<span></span></label>
                <div>
                    <input type="text" class="text" id="episode_to">
                </div>
            </fieldset>

            <div class="button_bar clearfix">
                <button class="green dark img_icon has_text" id="btn_search" onclick="$('.rows-data').html(''); return searchFilm($('#episode_from').val(),$('#episode_to').val());">
                    <img src="images/icons/small/white/bended_arrow_right.png">
                    <span>Tìm kiếm</span>
                </button>
                <span style="margin: 6px 10px 10px;float: left;" class="sp-loadding"></span>
            </div>
        </div>
    </div>

    <div class="block" style="margin-top: 10px">
        <table class="static">
            <thead>
            <tr>
                <th><input type="checkbox" id="checkAll"></th>
                <th>Title</th>
                <th>Url</th>
                <th>Time</th>
                <th>Check</th>
                <th>Name Episode</th>
                <th width="60px" style="text-align: center" align="center">POS</th>
            </tr>
            </thead>
            <tbody class="rows-data">


            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    /**
     * Tìm film
     * @param episode_from
     * @param episode_to
     */
        function searchFilm(episode_from, episode_to){
            $('#checkAll').removeAttr('checked');
            if($('#episode_from').val() == '' || $('#episode_to').val() == '' || $('#film_id').val() == '' )
            {
                alert('Bạn phải nhập cả 2 trường tập phim, có thể bạn chưa nhập ID film');
                return false;
            }

            episode_from = parseInt(episode_from);
            episode_to = parseInt(episode_to);
            if(episode_from > episode_to){
                alert('Bạn không thể nhập từ Tập : '+episode_from+ ' Đến tập : '+episode_to);
                return false;
            }
            var site = $('#sl-list').val();
            var keyword = $('#keyword').val();
            var filmId = $('#film_id').val();
            $(document).ready(function(){
                if(site != ''){
                    $.ajax({
                        type:'POST',
                        url:'<?php echo Yii::app()->createUrl('film/crawler/ajaxsearch'); ?>',
                        data:{
                            site:site,
                            keyword:keyword,
                            episode_from:episode_from,
                            filmId:filmId
                        },
                        beforeSend:function() {
                                $('.sp-loadding').html('<img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/loading.gif"/>');
                        },
                        success:function(data){
                            if(data != 'false'){
                                $('.sp-loadding').html('');
                                eval('data = '+data);
//                                if(data.id){
                                    $str = '<tr>';
                                        $str +='<td>';
                                        if(data.id){
                                        $str +='<input type="checkbox" class="list_check" value="'+data.id+'">';
                                        }
                                        $str +='</td>';
                                        $str +='<td id="title-'+data.id+'" data-title="'+data.title+'">'+data.title+'</td>';
                                        $str +='<td><a id="link-'+data.id+'" href="'+data.link+'" data-link="'+data.link+'" target="_blank">'+data.link+'</a></td>';
                                        $str +='<td><span id="time-'+data.id+'" data-time="'+data.time+'" >'+data.time+'</span>s</td>';
                                        $str +='<td>'+data.check+'</td>';
                                        $str +='<td align="center"><input type="text" data-server="'+data.server+'" id="episode-'+data.id+'" value="Tập '+episode_from+'" class="text" style="width: 50px !important;text-align: center;padding: 0;" ></td>';
                                        $str +='<td align="center"><input type="text" id="sorder-'+data.id+'" value="'+data.sorder+'" class="text" style="width: 50px !important;text-align: center;padding: 0;" ></td>';
                                    $str +='</tr>';
                                    $('.rows-data').append($str);
//                                }
                            }
//                            return false;
                            var episode_next = episode_from + 1;
                            if (episode_next > episode_to){
                                $('.sp-loadding').html('success');
                                setTimeout(function(){
                                    $('.sp-loadding').html('');
                                },2000);
                                return false;
                            }
                            searchFilm(episode_next, episode_to);
                        }
                    });
                } else {
                    return false;
                }
            });
        };

        function invalid(data)
        {
            var reg=/[0-9]/;
            return reg.test(data);
        }
        $("#episode_from,#episode_to").live("keyup", function (e){
            var data=$(this).val();
            var obj=$(this);
            if(data.indexOf('.')!=data.lastIndexOf('.'))
            {
                obj.val(data.substr(0,data.length-1));
            }
            if(!invalid(data))
            {
                obj.val(data.substr(0,data.length-1));
            }
        });
        /**
         * Sự kiện click checkall
         */
        $('#checkAll').live('click',function(){
            if($('.rows-data tr').length == 0) {
                alert('Chưa có link nào ');
                return false;
            }
            if($(this).is(':checked')){
                $('.list_check').attr('checked','checked');
            } else {
                $('.list_check').removeAttr('checked');
            }
        });
        $('.list_check').live('click',function(){
            if($(this).not(':checked')){
                $('#checkAll').removeAttr('checked');
            }
        });

        function saveData(){
            if($('#film_id').val() == '' || $('.rows-data tr').length == 0){
                alert('Bạn chưa nhập ID của film Hoặc chưa có link film nào ');
                return false;
            }
            var filmId = $('#film_id').val();
            var data = Array();
            $('.list_check').each(function(){
                if($(this).is(':checked')){
                    var id = $(this).val();
                    var title = $('#title-'+id).data('title');
                    var link = $('#link-'+id).data('link');
                    var time = $('#time-'+id).data('time');
//                    var thumbnail = $('#thumbnail-'+id).data('thumbnail');
                    var sorder = $('#sorder-'+id).val();
                    var episode = $('#episode-'+id).val();
                    var server = $('#episode-'+id).data('server');
                    data.push({id:id,title:title,link:link,sorder:sorder,episode:episode,server:server});
                }
            });
            if(data.length == 0){
                alert('Bạn chưa chọn link phim nào');
                return false;
            }
            $(document).ready(function(){
                $.ajax({
                    type : 'POST',
                    url : '<?php echo Yii::app()->createUrl('film/crawler/AjaxSaveItem'); ?>',
                    data : {
                        filmid : filmId,
                        filmdata : data
                    },
                    beforeSend:function(){
                        $('#lbconfirm').html('saving...');
                    },
                    success:function(data){
                        eval('data = '+data);
                        if(data.result == 'true'){
                            $('#lbconfirm').html(data.mess);
                            $('.rows-data,#confirmId').html('');
                            $('#episode_from,#episode_to,#keyword,#film_id').val('');
                            $('#checkAll').removeAttr('checked');
                            setTimeout(function(){
                                $('#lbconfirm').html('');
                            },2000)
                        }
                    }
                })
            });
        }
        $('#film_id').live('keyup',function(){
            var filmId = $(this).val();
            $.ajax({
                type : 'POST',
                url : '<?php echo Yii::app()->createUrl('film/crawler/CheckFilm'); ?>',
                data : {id:filmId},
                success:function(data){
                    eval('data = '+data);
                    if(data.result == 'true'){
                        $('#confirmId').html(data.title).css({'color':'blue'});
                        $('#btnSaveFilm,#btn_search').show();
                    } else {
                        $('#confirmId').html(data.title).css({'color':'red'});
                        $('#btnSaveFilm,#btn_search').hide();
                    }
//                    console.log(data);
                }
            });
        });
</script>



