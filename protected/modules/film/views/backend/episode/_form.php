<?php
$moduleName = 'film';
$modelName = 'KitFilmEpisode';
$formID = 'kit_film_episode_form';
//$formID = 'kit_'.$moduleName.'_form';
//$gridID = 'kit_'.$moduleName.'_grid';

$form = $this->beginWidget('CActiveForm', array(
	'id' => $formID,
	'enableAjaxValidation'=>false,
    'htmlOptions' => array(
        'onSubmit' => 'js:ajaxSaveForm("'.$modelName.'" ,jQuery(this).attr("id"), jQuery(this).attr("action")); return false',
    )
)); ?>
    <div id="" class="dialog_content">
        <div class="block" style="opacity: 1;">
            <div class="section">
                <h2>Thông tin tập phim</h2>
<!--                <div class="alert dismissible alert_black">
                    <img width="24" height="24" src="images/icons/small/white/alert_2.png">
                    <strong>All the form fields</strong> can be just as easily used in a dialog.
                </div>-->
            </div>

        <?php if ($model->isNewRecord) : ?>
            <a href="javascript:;" id="copyForm" rel="0" onclick="copyForm(jQuery(this))">Add</a>
            <div id="nameOrigin">
                <div class="columns clearfix">
                    <div class="col_60">
                        <fieldset>
                            <label><?php echo $model->getAttributeLabel('title'); ?></label>
                            <div>
                                <?php echo CHtml::textField('KitFilmEpisode[0][title]', $model->title, array('class' => 'tooltip right text')); ?>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col_40">
                        <fieldset>
                            <label><?php echo $model->getAttributeLabel('sorder'); ?></label>
                            <div>
                                <?php echo CHtml::textField('KitFilmEpisode[0][sorder]', $model->title, array('class' => 'tooltip right text')); ?>			
                            </div>
                        </fieldset>
                    </div>
                </div>
                
                <fieldset>
                    <label><?php echo $model->getAttributeLabel('link'); ?></label>
                    <div>
                        <?php echo CHtml::textField('KitFilmEpisode[0][link]', $model->title, array('class' => 'tooltip right text')); ?>
                    </div>
                </fieldset>

                <?php /*
                <fieldset class="label_side">
                    <?php echo $form->labelEx($model,'status'); ?>
                    <div>
                        <div class="selector" id="uniform-undefined"><span style="-moz-user-select: none;"><?php echo Yii::t('BackEnd', "-- Select a status --"); ?></span>
                            <?php echo CHtml::dropdownList('KitFilmEpisode[0][status]', NULL, BackendFunctions::getStatusOptions(), array(
                                'class' => 'select_box',
                                'style' => 'opacity:0',
                            )) ?>
                        </div>
                    </div>
                </fieldset> */ ?>
            </div>
            <div id="nameExtra"></div>
        <?php endif; ?>

        <?php if (!$model->isNewRecord) : ?>
            <fieldset class="label_side">
                <?php echo $form->labelEx($model, 'title'); ?>
                <div>
                    <?php echo $form->textField($model, 'title', array('class' => 'tooltip right text')); ?>
                </div>
            </fieldset>

            <fieldset class="label_side">
                <?php echo $form->labelEx($model, 'link'); ?>
                <div>
                    <?php echo $form->textField($model,'link',array('class' => 'tooltip right text', 'size' => 30)); ?>
                </div>
            </fieldset>            

            <fieldset class="label_side">
                <?php echo $form->labelEx($model, 'sorder'); ?>
                <div>
                    <?php echo $form->textField($model,'sorder',array('class' => 'tooltip right text', 'size' => 30)); ?>
                </div>
            </fieldset>

            <fieldset class="label_side">
                <?php echo $form->labelEx($model, 'embed'); ?>
                <div>
                    <?php echo $form->textField($model,'embed',array('class' => 'tooltip right text', 'size' => 30)); ?>
                </div>
            </fieldset>

            <fieldset class="label_side">
                <?php echo $form->labelEx($model,'status'); ?>
                <div>
                    <div class="selector" id="uniform-undefined"><span style="-moz-user-select: none;"><?php echo Yii::t('BackEnd', "-- Select a status --"); ?></span>
                        <?php echo $form->dropdownList($model, 'status', BackendFunctions::getStatusOptions(), array(
                            'class' => 'select_box',
                            'style' => 'opacity:0',
                        )) ?>
                    </div>
                </div>
            </fieldset>
        <?php endif; ?>

            <div class="button_bar clearfix">
                <button class="dark green close_dialog">
                    <div class="ui-icon ui-icon-check"></div>
                    <span>Submit</span>
                </button>
<!--                <button class="dark red close_dialog">
                    <div class="ui-icon ui-icon-closethick"></div>
                    <span>Cancel</span>
                </button>-->
            </div>
        </div>
    </div>

<?php $this->endWidget(); ?>

<script>
    function copyForm(obj) {
        nextFormId = parseInt(obj.attr('rel')) + 1;
        html = jQuery('#nameOrigin').html();
        html = html.replace(new RegExp('_0_', 'g'), '_'+nextFormId+'_');
        html = html.replace(new RegExp('\\[0\\]', 'g'), '['+nextFormId+']');
        obj.attr('rel', nextFormId);
        jQuery('#nameExtra').append(html);
    }
</script>
