<?php
$moduleName = 'category';
$modelName = 'KitCategory';
$formID = 'kit_'.$moduleName.'_form';
$gridID = 'kit_'.$moduleName.'_grid';

$this->breadcrumbs = array(
    Yii::t('backend', $moduleName) => $this->createUrl('/'.$moduleName.'/default/index'),
    Yii::t('backend', 'Manager') . ' ' . Yii::t('backend', $moduleName),
);
?>

<div class="grid_16">
    <div class="flat_area">
        <h2><?php echo Yii::t('backend', 'Manager'); ?> <?php echo Yii::t('backend', $moduleName); ?></h2>
    </div>

    <button class="blue small" onclick="ajaxChangeBooleanValue('<?php echo $moduleName; ?>', '<?php echo $modelName; ?>', 'status', 1, '<?php echo Yii::app()->createUrl('cms/ajax/ChangeBooleanValue'); ?>', '<?php echo $gridID; ?>');">
        <div class="ui-icon ui-icon-check"></div>
        <span><?php echo Yii::t('global', 'Publish'); ?></span>
    </button>
    <button class="light small" onclick="ajaxChangeBooleanValue('<?php echo $moduleName; ?>', '<?php echo $modelName; ?>', 'status', 0, '<?php echo Yii::app()->createUrl('cms/ajax/ChangeBooleanValue'); ?>', '<?php echo $gridID; ?>');">
        <div class="ui-icon ui-icon-cancel"></div>
        <span><?php echo Yii::t('global', 'Unpublish'); ?></span>
    </button>
    <button class="blue small" onclick="ajaxChangeBooleanValue('<?php echo $moduleName; ?>', '<?php echo $modelName; ?>', 'promotion', 1, '<?php echo Yii::app()->createUrl('cms/ajax/ChangeBooleanValue'); ?>', '<?php echo $gridID; ?>');">
        <div class="ui-icon ui-icon ui-icon-arrowthick-1-n"></div>
        <span><?php echo Yii::t('global', 'Promotion'); ?></span>
    </button>
    <button class="light small" onclick="ajaxChangeBooleanValue('<?php echo $moduleName; ?>', '<?php echo $modelName; ?>', 'promotion', 0, '<?php echo Yii::app()->createUrl('cms/ajax/ChangeBooleanValue'); ?>', '<?php echo $gridID; ?>');">
        <div class="ui-icon ui-icon-arrowthick-1-s"></div>
        <span><?php echo Yii::t('global', 'Demotion'); ?></span>
    </button>
    <button class="red small" onclick="ajaxChangeBooleanValue('<?php echo $moduleName; ?>', '<?php echo $modelName; ?>', 'trash', 1, '<?php echo Yii::app()->createUrl('cms/ajax/ChangeBooleanValue'); ?>', '<?php echo $gridID; ?>');">
        <div class="ui-icon ui-icon-trash"></div>
        <span><?php echo Yii::t('global', 'Trash'); ?></span>
    </button>
    <button class="red small" onclick="ajaxDeleteMultiRecord('<?php echo $moduleName; ?>', '<?php echo $modelName; ?>', '<?php echo Yii::app()->createUrl('cms/ajax/DeleteMultiRecord'); ?>', '<?php echo $gridID; ?>')">
        <div class="ui-icon ui-icon-close"></div>
        <span><?php echo Yii::t('global', 'Delete'); ?></span>
    </button>

    <?php        
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => $gridID,
        'dataProvider' => $model->search(),
        //'filter' => $model,
        'columns' => array(
            array(
                'class' => 'CCheckBoxColumn',
                'id' => 'chkIds',
                'selectableRows' => 2,
                'htmlOptions' => array('width' => '20px', 'align' => 'center')
            ),
            array(
                'name' => 'name',
                'value' => '$data->id',
                'htmlOptions' => array('class' => 'minwidth', 'align' => 'center')
            ),
            array(
                'name' => 'name',
                'value' => 'str_repeat("-- ", $data->level - 1) . $data->name'
            ),
            'alias',
            'parent_id',
            'description',
            'module',
            'layout',                
            array(
                'filter' => '',
                'type' => 'raw',
                'name' => 'sorder',
                'value' => '$data->getSortIcon("sort", "'.$gridID.'", $data->id)',
                'htmlOptions' => array('width' => '50px', 'align' => 'center')
            ),
            array(
                'filter' => CHtml::activeDropDownList($model, 'promotion', BackendFunctions::getStatusOptions(), array('prompt' => Yii::t('global', 'Select all'))),
                'type' => 'raw',
                'name' => 'promotion',
                'value' => 'BackendFunctions::getBooleanIcon("'.$moduleName.'", "'.$modelName.'", $data->id, "promotion", $data->promotion, "'.$gridID.'")',
                'htmlOptions' => array('align' => 'center')
            ),
            array(
                'filter' => CHtml::activeDropDownList($model, 'status', BackendFunctions::getStatusOptions(), array('prompt' => Yii::t('global', 'Select all'))),
                'type' => 'raw',
                'name' => 'status',
                'value' => 'BackendFunctions::getBooleanIcon("'.$moduleName.'", "'.$modelName.'", $data->id, "status", $data->status, "'.$gridID.'")',
                'htmlOptions' => array('align' => 'center')
            ),
            /*
            'created_time',
            'updated_time',
            'sorder',
            'status',
            */        
            array(
                'class' => 'CButtonColumn',
                'template' => '{gridUpdate} {gridDelete}',
                'buttons' => array(
                    'gridUpdate' => array(
                        'label' => Yii::t('default', 'Update'),
                        'url' => 'Yii::app()->createUrl("/category/default/update", array("id" => $data->id))',
                        'imageUrl'=>Yii::app()->theme->baseUrl.'/images/icons/system/edit_16.png',
                        'options' => array('class' => 'btnGridUpdate', 'onclick' => "js:ajaxDialogForm('".$gridID."', 'Cập nhật', $(this).attr('href')); return false")
                    ),
                    'gridDelete' => array(
                        'label' => Yii::t('default', 'Delete'),
                        'url' => 'Yii::app()->createUrl("/category/default/delete", array("id" => $data->id))',
                        'imageUrl'=>Yii::app()->theme->baseUrl.'/images/icons/system/delete_16.png',
                        'options' => array('onclick' => "js:ajaxDeleteRecord('".$moduleName."', '".$modelName."', $(this).attr('href'), 'status', 2); return false"),
                    ),
                ),
            ),
        ),
    ));
    ?>
</div>
    
<script>
/*
 * Hien dialog xac nhan, cho phep cap nhat trang thai nhieu row trong gridview
 * @grid id cua gridview
 * @url url den action thuc hien chuc nang cap nhat status
 * @status trang thai can cap nhat (1/0)
 * @column cot trong table se cap nhat
 */
function ajaxChangeBooleanValue (module, model, field, value) {
    var url = '<?php echo Yii::app()->createUrl('cms/default/ChangeBooleanValue'); ?>';
    var grid = '<?php echo $gridID; ?>';
    var ajaxDialog = jQuery('#dialogManage');
    if (ajaxDialog.length == 0)
        ajaxDialog = jQuery('<div id="dialogManage" style="display:none"></div>').appendTo('body');

    ajaxDialog.empty();
    ajaxDialog.html('Are you sure you want to perform this action?');
    ajaxDialog.dialog({
        'autoOpen' :false,
        'modal': true,
        'minHeight': 140,
        'minWidth': 230,
        'title': 'Confirm',
        'buttons':[{
            text:'Yes',
            click:function() {
                jQuery(this).dialog('close');
                jQuery.ajax({
                    'url': url,
                    'type':'POST',
                    'cache':false,
                    'data':{
                        'module' : module,
                        'model' : model,
                        'field' : field,
                        'value' : value,
                        'ids' : jQuery.fn.yiiGridView.getChecked(grid,'chkIds'),
                        'YII_CSRF_TOKEN':YII_CSRF_TOKEN
                    },
                    'beforeSend':function() {

                    },'success':function() {
                        jQuery('#'+grid).yiiGridView.update(grid);
                    }
                });
            }
        },{
            text:'No',
            click:function() {
                jQuery(this).dialog('close');
            }
        }]
    });
    ajaxDialog.dialog('open');
    return false;
}

/*
 * Hien dialog xac nhan, cho phep xoa cung luc nhieu row trong gridview
 * @grid id cua gridview
 * @url url den action thuc hien chuc nang cap nhat status
 */
function ajaxDeleteRecord(module, model, url, field, value, trash) {
    trash = typeof(trash) != 'undefined' ? trash : 0;

    var grid = '<?php echo $gridID; ?>';
    var ajaxDialog = jQuery('#dialogConfirm');
    if (ajaxDialog.length == 0)
        ajaxDialog = jQuery('<div id="dialogConfirm" style="display:none"></div>').appendTo('body');

    ajaxDialog.empty();
    ajaxDialog.html('Are you sure you want to delete?');
    ajaxDialog.dialog({
        'autoOpen' :false,
        'modal': true,
        'minHeight': 140,
        'minWidth': 230,
        'title': 'Confirm',
        'buttons':[{
            text:'Delete',
            click:function() {
                jQuery(this).dialog('close');                
                jQuery.ajax({
                    'url':url,
                    'type':'POST',
                    'cache':false,
                    'data':{
                        'module':module,
                        'model':model,
                        'field':field,
                        'value':value,
                        'trash':trash,
                        'YII_CSRF_TOKEN':YII_CSRF_TOKEN
                    },'beforeSend':function() {
                    },'success':function() {                        
                        jQuery('#'+grid).yiiGridView.update(grid);                        
                    }
                });
            }
        },{
            text:'No',
            click:function() {
                jQuery(this).dialog('close');
            }
        }]
    });
    ajaxDialog.dialog('open');
    return false;
}

/*
 * Hien dialog xac nhan, cho phep xoa cung luc nhieu row trong gridview
 * @grid id cua gridview
 * @url url den action thuc hien chuc nang cap nhat status
 */
 function ajaxDeleteMultiRecord(module, model, field, value, trash) {
    var url = '<?php echo Yii::app()->createUrl('category/default/DeleteMultiRecord'); ?>';
    var grid = '<?php echo $gridID ?>';
    var ajaxDialog = jQuery('#dialogConfirm');
    trash = typeof(trash) != 'undefined' ? trash : 0;
    
    if (ajaxDialog.length == 0)
        ajaxDialog = jQuery('<div id="dialogManage" style="display:none"></div>').appendTo('body');

    ajaxDialog.empty();
    ajaxDialog.html('Are you sure you want to perform this action?');
    ajaxDialog.dialog({
        'autoOpen' :false,
        'modal': true,
        'minHeight': 140,
        'minWidth': 230,
        'title': 'Confirm',
        'buttons':[{
            text:'Delete',
            click:function() {
                jQuery(this).dialog('close');
                jQuery.ajax({
                    'url':url,
                    'type':'POST',
                    'cache':'false',
                    'data':{         
                        'module' : module,
                        'model' : model,
                        'field':field,
                        'value':value,
                        'trash':trash,
                        'ids':jQuery.fn.yiiGridView.getChecked(grid,'chkIds'),
                        'YII_CSRF_TOKEN':YII_CSRF_TOKEN
                    },
                    'beforeSend':function() {
                        jQuery('.wait').show();
                    },'success':function() {
                        jQuery('#'+grid).yiiGridView.update(grid);
                    }
                });
            }
        },{
            text:'Cancel',
            click:function() {
                jQuery(this).dialog('close');
            }
        }]
    });
    ajaxDialog.dialog('open');
    return false;
}

function ajaxLoadDialog(title, url, height, width) {
    var ajaxDialog = jQuery('#ajaxPageDialog');
    height = typeof(height) != 'undefined' ? height : jQuery(window).height() - 20;
    width  = typeof(width) != 'undefined' ? width : jQuery(window).width() - 20;

    if (ajaxDialog.length == 0)
        ajaxDialog = jQuery('<div id="ajaxPageDialog" style="display:none"></div>').appendTo('body');

    jQuery('.wait').show();
    ajaxDialog.empty();
    ajaxDialog.dialog({
        'autoOpen' :false,
        'modal': true,
        'height': height,
        'width': width,
        'title': title,
        'close': function(event, ui) {}
    });
    ajaxDialog.load(url, function() {        
        ajaxDialog.dialog('open');
    });
    return false;
}

</script>
