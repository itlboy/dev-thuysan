<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'kit_film_episode_form',
	'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'onSubmit' => 'js:ajaxSaveForm("KitFilmEpisode" ,jQuery(this).attr("id"), jQuery(this).attr("action")); return false',
    )
)); ?>
    <div id="" class="dialog_content">
        <div class="block" style="opacity: 1;">
            <div class="section">
                <h2>Thông tin tập phim</h2>
                <!--
                <div class="alert dismissible alert_black">
                    <img width="24" height="24" src="images/icons/small/white/alert_2.png">
                    <strong>All the form fields</strong> can be just as easily used in a dialog.
                </div>
                -->
            </div>

                

                   
            
                        
                <fieldset class="label_side">
                    <?php echo $form->labelEx($model, 'title'); ?>
                    <div>
                        <?php echo $form->textField($model, 'title', array('class' => 'tooltip right text')); ?>
                    </div>
                </fieldset>
                                    

                   
            
                        
                <fieldset class="label_side">
                    <?php echo $form->labelEx($model, 'sorder'); ?>
                    <div>
                        <?php echo $form->textField($model, 'sorder', array('class' => 'tooltip right text')); ?>
                    </div>
                </fieldset>
                                    

                   
            
                        
                <fieldset class="label_side">
                    <?php echo $form->labelEx($model, 'link'); ?>
                    <div>
                        <?php echo $form->textField($model, 'link', array('class' => 'tooltip right text')); ?>
                    </div>
                </fieldset>
                                    

                   
            
                        
                <fieldset class="label_side">
                    <label><?php echo $model->getAttributeLabel('limit'); ?></label>
                    <div>
                        <div class="selector" id="uniform-undefined"><span style="-moz-user-select: none;">-- Select a status --</span>
                            <?php echo $form->dropdownList($model, 'limit', BackendFunctions::getStatusOptions(), array(
                                'class' => 'select_box',
                                'style' => 'opacity:0',
                            )) ?>
                        </div>
                    </div>
                </fieldset>
                                    

                   
            
                        
                <fieldset class="label_side">
                    <?php echo $form->labelEx($model, 'vip'); ?>
                    <div>
                        <?php echo $form->textField($model, 'vip', array('class' => 'tooltip right text')); ?>
                    </div>
                </fieldset>
                                    

                   
            
                        
                <fieldset class="label_side">
                    <?php echo $form->labelEx($model, 'intro'); ?>
                    <div>
                        <?php echo $form->textField($model, 'intro', array('class' => 'tooltip right text')); ?>
                    </div>
                </fieldset>
                                    

                   
            
                        
                <fieldset class="label_side">
                    <label><?php echo $model->getAttributeLabel('hot'); ?></label>
                    <div>
                        <div class="selector" id="uniform-undefined"><span style="-moz-user-select: none;">-- Select a status --</span>
                            <?php echo $form->dropdownList($model, 'hot', BackendFunctions::getStatusOptions(), array(
                                'class' => 'select_box',
                                'style' => 'opacity:0',
                            )) ?>
                        </div>
                    </div>
                </fieldset>
                                    

                   
            
                        
                <fieldset class="label_side">
                    <label><?php echo $model->getAttributeLabel('promotion'); ?></label>
                    <div>
                        <div class="selector" id="uniform-undefined"><span style="-moz-user-select: none;">-- Select a status --</span>
                            <?php echo $form->dropdownList($model, 'promotion', BackendFunctions::getStatusOptions(), array(
                                'class' => 'select_box',
                                'style' => 'opacity:0',
                            )) ?>
                        </div>
                    </div>
                </fieldset>
                                    

                   
            
                        
                <fieldset class="label_side">
                    <label><?php echo $model->getAttributeLabel('status'); ?></label>
                    <div>
                        <div class="selector" id="uniform-undefined"><span style="-moz-user-select: none;">-- Select a status --</span>
                            <?php echo $form->dropdownList($model, 'status', BackendFunctions::getStatusOptions(), array(
                                'class' => 'select_box',
                                'style' => 'opacity:0',
                            )) ?>
                        </div>
                    </div>
                </fieldset>
                                    

                   
            
                        
                <fieldset class="label_side">
                    <label><?php echo $model->getAttributeLabel('trash'); ?></label>
                    <div>
                        <div class="selector" id="uniform-undefined"><span style="-moz-user-select: none;">-- Select a status --</span>
                            <?php echo $form->dropdownList($model, 'trash', BackendFunctions::getStatusOptions(), array(
                                'class' => 'select_box',
                                'style' => 'opacity:0',
                            )) ?>
                        </div>
                    </div>
                </fieldset>
                        
            <div class="button_bar clearfix">
                <button class="dark green close_dialog">
                    <div class="ui-icon ui-icon-check"></div>
                    <span>Submit</span>
                </button>

                <!--
                <button class="dark red close_dialog">
                    <div class="ui-icon ui-icon-closethick"></div>
                    <span>Cancel</span>
                </button>
                -->
            </div>
        </div>
    </div>

<?php $this->endWidget(); ?>
