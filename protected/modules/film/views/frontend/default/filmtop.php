<?php $i = 1; ?>
<?php foreach($dataFilm as $item ): ?>
    <li class="thumb_s1 row">
        <a data-title="<?php echo $item["title"]; ?>" rel="tooltip" class="thumbnail" href="#" data-original-title="">
            <img alt="" src="http://phim.let.vn/uploads/film/<?php echo $item["id"]; ?>_normal.jpg" />
        </a>
        <a>
            <div class="line_info"><?php echo $item["title"] ?></div>
            <!--            <div class="line_info">A Gentleman's Dignity</div>-->
        </a>
    </li>
    <?php if($i % 3 == 0 and $i > 0) : ?>
        <li style="width:100%;"></li>
    <?php endif; ?>
<?php $i++; ?>
<?php endforeach; ?>