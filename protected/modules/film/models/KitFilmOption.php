<?php

Yii::import('application.modules.film.models.db.BaseKitFilmOption');
class KitFilmOption extends BaseKitFilmOption {

    var $className = __CLASS__;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getFilm2Option($filmId) {
        $result = array();
        $data = self::findAll('film_id=:filmId', array(':filmId' => $filmId));
        foreach ($data as $item2Type)
            $result[] = $item2Type->option_id;
        return $result;
    }

    /**
     * Create
     * @param array $options
     * @param int $itemId 
     */
    public static function createFilmOption($options = array(), $itemId) {
        if (! is_array($options)) return FALSE;
        foreach ($options as $optionId) {
            $item2Opt = new KitFilmOption;
            $item2Opt->option_id = $optionId;
            $item2Opt->film_id = $itemId;
            $item2Opt->save();
        }
    }

    /**
     * Delete
     * @param array $options
     * @param int $itemId 
     */
    public static function deleteFilmOption($options = array(), $itemId) {
        if (! is_array($options)) return FALSE;
        foreach ($options as $optionId) {
            self::model()->deleteAll('option_id=:optionId AND film_id=:filmId', array(
                ':optionId' => $optionId,
                ':filmId' => $itemId,
            ));
        }
    }
}