<?php

Yii::import('application.modules.film.models.db.BaseKitFilmReport');
class KitFilmReport extends BaseKitFilmReport
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}