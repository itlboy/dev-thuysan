<?php

Yii::import('application.modules.film.models.db.BaseKitFilmInterest');
class KitFilmInterest extends BaseKitFilmInterest {

    var $className = __CLASS__;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    /**
     * Get Details
     * @param int $id
     * @return object
     */
    public static function getDetails($user_id , $film_id) {
        $film_id = intval($film_id);
        $user_id = intval($user_id);
        $cache_name = md5(__METHOD__ . '_' . $film_id . '_' . $user_id);

        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $result = self::model()->find('item_id=:itemId AND user_id=:userId', array(
                'itemId' => $film_id ,
                'userId' => $user_id,
            ));
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

    public static function getInfoItem($film_id){
        $film_id = intval($film_id);
        $cache_name = md5(__METHOD__ . '_' . $film_id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE OR TRUE) {
            $like_sql = 'SELECT COUNT(id) FROM let_kit_film_interest WHERE item_id='.$film_id.' AND `like`=1';
            $dislike_sql = 'SELECT COUNT(id) FROM let_kit_film_interest WHERE item_id='.$film_id.' AND `like`=2';
            $like = Yii::app()->db->createCommand($like_sql)->queryScalar();
            $dislike = Yii::app()->db->createCommand($dislike_sql)->queryScalar();
            $total = ($like+$dislike);
            if(!empty($total)){
                $point = round(($like/$total)*10,1);
                $result = array(
                    'like' => $like,
                    'dislike' => $dislike,
                    'total_interest' => $total,
                    'point' => $point,
                );
            } else {
                $result = array(
                    'like' => 0,
                    'dislike' => 0,
                    'total_interest' => 0,
                    'point' => 0,
                );
            }
            Yii::app()->cache->set($cache_name, $result, 60*60); // Set cache
        } else return $cache;
        return $result;
    }
}