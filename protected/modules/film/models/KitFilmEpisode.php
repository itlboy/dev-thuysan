<?php
Yii::import('application.modules.film.models.db.BaseKitFilmEpisode');
class KitFilmEpisode extends BaseKitFilmEpisode{
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'creatorUser' => array(self::BELONGS_TO, 'KitAccount', 'creator', 'select' => 'username'),
            'editorUser' => array(self::BELONGS_TO, 'KitAccount', 'editor', 'select' => 'username'),
		);
	}

    public function attributeLabels() {
        return array_merge(parent::attributeLabels(), array(
            'limit' => 'Giới hạn',
        ));
    }

    /**
     * treatment
     * @param mixed $data
     * @return array 
     */
    public static function treatment($data, $title = '')
    {
        $data = CJSON::decode ((CJSON::encode($data))); // Neu la object thi chuyen thanh array
        if (!isset ($data[0]))
            $data = self::treatmentRow($data, $title);
        else {
            foreach ($data as $key => $value) {
                $data[$key] = self::treatmentRow($value, $title);
            }
        }
        return $data;
    }
    
    public static function treatmentRow($row, $title = '')
    {
        if ($title !== '')
            $urlTitle = Common::convertStringToUrl($title);
        else
            $urlTitle = Common::convertStringToUrl($row['title']);
        $row['url'] = Yii::app()->createUrl('xem-phim-' . $urlTitle.'-online-film-' . $row['item_id'] . '-' . $row['id']);
        return $row;
    }
    
    protected function beforeValidate() {
        $this->editor = Yii::app()->user->id;
        $this->updated_time = Common::getNow();                    
        if ($this->isNewRecord) {
            $this->creator = $this->editor;
            $this->created_time = $this->checked_time = $this->updated_time;
        }
        
        $this->title = trim($this->title);
        $this->link = trim($this->link);
        
        if ($this->vip > 0) {
            $this->server = 'vip';
        } else {
            Yii::import('application.vendors.libs.grabVideo');
            $this->server = grabVideo::getNameServer($this->link);
        }
        
        return parent::beforeValidate();
    }
}