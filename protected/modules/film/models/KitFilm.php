<?php
Yii::import('application.modules.film.models.db.BaseKitFilm');
Yii::import('application.modules.cms.models.KitStats');
class KitFilm extends BaseKitFilm{
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function attributeLabels() {
        return array_merge(parent::attributeLabels(), array(
            'title' => 'Tên phim',
            'title_english' => 'Tên Tiếng Anh',
            'title_english' => 'Tên Tiếng Anh',
            'imdb_grab' => 'Có IMDB?',
            'image' => 'Ảnh đại diện',
            'intro' => 'Giới thiệu',
            'content' => 'Nội dung',
            'actor' => 'Diễn viên',
            'director' => 'Đạo diễn',
            'country' => 'Quốc gia',
            'theater' => 'Chiếu rạp',
            'length' => 'Phim bộ?',
            'complete' => 'Hoàn thành',
            'sorder' => 'Thứ tự',
            'hot' => 'Phim hot',
            'promotion' => 'Đề cử',
            'status' => 'Kích hoạt',
            'trash' => 'Thùng rác',
        ));
    }
    
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
        return array_merge(parent::relations(), array(
            'stats' => array(self::HAS_ONE, 'KitStats', 'item_id',
                'condition' => 'module = "film"'),
        ));
	}
    
    public function caches($id = 0, $categorys = array())
	{
        return array_merge(parent::caches($id, $category), array(
            'aaaaa'
        ));
	}

    public static function treatmentRow($row)
    {
		// URL
        if (trim($row['seo_url']) !== '') $title = $row['seo_url'];
        else $title = $row['title'] . '-' . $row['title_english'];
        $urlTitle = Common::convertStringToUrl($title);
        $row['url'] = Yii::app()->createUrl('xem-phim-' . $urlTitle.'-online-film-'.$row['id']);
        $row['title'] = str_replace (array ('"', "'"), array ('&quot;', '&apos;'), $row['title'] );
		// Tap phim
        // Tai day em check xem cac truong hop nao la "12", "12/13", "?/13", con chu "Tap" dang truoc thi anh dien vao sau. Cai nao khong co tap gi ca thi de la rong nhu gia tri anh config o day. Lam xong cai nay thi em thay het cac cho nao can hien thi tap phim nhe.
        if(!empty($row['episode_current']) AND !empty($row['episode_total'])){
            $row['episode_display'] = $row['episode_current'].'/'.$row['episode_total'];
        } elseif(empty($row['episode_current']) AND !empty($row['episode_total'])){
            $row['episode_display'] = '?/'.$row['episode_total'];
        } else {
            $row['episode_display'] = '';
        }

        return $row;
    }
    
    protected function beforeValidate() {
        $this->editor = Yii::app()->user->id;
        $this->updated_time = Common::getNow();                    
        if ($this->isNewRecord) {
            $this->creator = $this->editor;
            $this->created_time = $this->updated_time;
            $this->view_count = 0;
            $this->comment_count = 0;
            $this->sorder = 500;
        }
        
        return parent::beforeValidate();
    }
    
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($creator = NULL)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;
        $criteria->select = Common::getFieldInTable(KitFilm::model()->getAttributes(), 't.');
        
        if ($creator != NULL)
            $this->creator = $creator;
        
        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.title',$this->title,true);
        $criteria->compare('t.title_english',$this->title_english,true);
        $criteria->compare('t.imdb_grab',$this->imdb_grab);
        $criteria->compare('t.imdb_id',$this->imdb_id,true);
        $criteria->compare('t.imdb_point',$this->imdb_point);
        $criteria->compare('t.image',$this->image,true);
        $criteria->compare('t.duration',$this->duration);
        $criteria->compare('t.intro',$this->intro,true);
        $criteria->compare('t.content',$this->content,true);
        $criteria->compare('t.actor',$this->actor,true);
        $criteria->compare('t.director',$this->director,true);
        $criteria->compare('t.year',$this->year);
        $criteria->compare('t.country',$this->country,true);
        $criteria->compare('t.theater',$this->theater);
        $criteria->compare('t.length',$this->length);
        $criteria->compare('t.vip',$this->vip);
        $criteria->compare('t.episode_total',$this->episode_total);
        $criteria->compare('t.episode_current',$this->episode_current);
        $criteria->compare('t.complete',$this->complete);
        $criteria->compare('t.comment_count',$this->comment_count);
        $criteria->compare('t.view_count',$this->view_count);
        $criteria->compare('t.tags',$this->tags,true);
        $criteria->compare('t.from_time',$this->from_time,true);
        $criteria->compare('t.to_time',$this->to_time,true);
        $criteria->compare('t.seo_title',$this->seo_title,true);
        $criteria->compare('t.seo_url',$this->seo_url,true);
        $criteria->compare('t.seo_desc',$this->seo_desc,true);
        $criteria->compare('t.creator',$this->creator);
        $criteria->compare('t.created_time',$this->created_time,true);
        $criteria->compare('t.editor',$this->editor,true);
        $criteria->compare('t.updated_time',$this->updated_time,true);
        $criteria->compare('t.sorder',$this->sorder,true);
        $criteria->compare('t.hot',$this->hot);
        $criteria->compare('t.promotion',$this->promotion);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.trash',$this->trash);

        if ($this->category_id !== NULL) {
            $categories = array();
            $categories[] = $this->category_id;
            $category = KitCategory::model()->findByPk($this->category_id);
            $descendants = $category->descendants()->findAll();
            foreach ($descendants as $cat) {
                $categories[] = $cat->id;
            }
            $criteria->addInCondition('ic.category_id', $categories);
        }
        
        $criteria->join = 'LEFT JOIN {{kit_film_category}} ic ON ic.film_id = t.id';
        
        $criteria->group = 't.id';
        
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
            'sort' => array(
                'defaultOrder' => 'id DESC',
            ),
		));
	}
    
}