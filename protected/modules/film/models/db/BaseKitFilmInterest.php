<?php

/**
 * This is the model class for table "{{kit_film_category}}".
 *
 * The followings are the available columns in table '{{kit_film_category}}':
 * @property string $id
 * @property string $film_id
 * @property string $category_id
 */
class BaseKitFilmInterest extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitFilmCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_film_interest}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
	}
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitFilmInterest::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitFilmInterest::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitFilmInterest::getLastest_'));
        Yii::app()->cache->delete(md5('KitFilmInterest::getPromotion_'));
    }

}
