<?php
/**
 * This is the model class for table "kit_film".
 *
 * The followings are the available columns in table 'kit_film':
 * @property string $id
 * @property string $title
 * @property string $title_english
 * @property integer $imdb_grab
 * @property string $imdb_id
 * @property double $imdb_point
 * @property string $image
 * @property integer $duration
 * @property string $intro
 * @property string $content
 * @property string $actor
 * @property string $director
 * @property integer $year
 * @property string $country
 * @property integer $theater
 * @property integer $length
 * @property integer $vip
 * @property integer $episode_total
 * @property integer $episode_current
 * @property integer $complete
 * @property integer $comment_count
 * @property integer $view_count
 * @property string $tags
 * @property string $from_time
 * @property string $to_time
 * @property string $seo_title
 * @property string $seo_url
 * @property string $seo_desc
 * @property string $creator
 * @property string $created_time
 * @property string $editor
 * @property string $updated_time
 * @property string $sorder
 * @property integer $hot
 * @property integer $promotion
 * @property integer $status
 * @property integer $trash
 */

class BaseKitFilm extends ActiveRecord {
    var $className = __CLASS__;
    
    public $category_id;
    public $categories;
    
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitFilm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_film}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, title_english', 'required'),
			array('imdb_grab, duration, year, theater, length, vip, episode_total, episode_current, complete, comment_count, view_count, hot, promotion, status, trash', 'numerical', 'integerOnly'=>true),
			array('imdb_point', 'numerical'),
			array('title, title_english, image, actor, country, seo_url', 'length', 'max'=>255),
			array('imdb_id', 'length', 'max'=>32),
			array('intro, tags', 'length', 'max'=>500),
			array('director', 'length', 'max'=>100),
			array('seo_title', 'length', 'max'=>70),
			array('seo_desc', 'length', 'max'=>160),
			array('creator, editor', 'length', 'max'=>11),
			array('sorder', 'length', 'max'=>10),
			array('content, from_time, to_time, created_time, updated_time', 'safe'),
            array('categories', 'safe'),
			// The following rule is used by search(). Please remove those attributes that should not be searched.
			array('id, title, title_english, imdb_grab, imdb_id, imdb_point, image, duration, intro, content, actor, director, year, country, theater, length, vip, episode_total, episode_current, complete, comment_count, view_count, tags, from_time, to_time, seo_title, seo_url, seo_desc, creator, created_time, editor, updated_time, sorder, hot, promotion, status, trash', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related class name for the relations automatically generated below.
		return array(
            'KitFilmCategory' => array(KitFilm::MANY_MANY, 'KitCategory',
                '{{kit_film_category}}(film_id, category_id)'),
            'creatorUser' => array(KitFilm::BELONGS_TO, 'KitAccount', 'creator', 'select' => 'username'),
            'editorUser' => array(KitFilm::BELONGS_TO, 'KitAccount', 'editor', 'select' => 'username'),
            'stats' => array(self::HAS_ONE, 'KitStats', 'item_id',
                'condition' => 'module = "film"'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;
        $criteria->select = Common::getFieldInTable(KitFilm::model()->getAttributes(), 't.');
        
        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.title',$this->title,true);
        $criteria->compare('t.title_english',$this->title_english,true);
        $criteria->compare('t.imdb_grab',$this->imdb_grab);
        $criteria->compare('t.imdb_id',$this->imdb_id,true);
        $criteria->compare('t.imdb_point',$this->imdb_point);
        $criteria->compare('t.image',$this->image,true);
        $criteria->compare('t.duration',$this->duration);
        $criteria->compare('t.intro',$this->intro,true);
        $criteria->compare('t.content',$this->content,true);
        $criteria->compare('t.actor',$this->actor,true);
        $criteria->compare('t.director',$this->director,true);
        $criteria->compare('t.year',$this->year);
        $criteria->compare('t.country',$this->country,true);
        $criteria->compare('t.theater',$this->theater);
        $criteria->compare('t.length',$this->length);
        $criteria->compare('t.vip',$this->vip);
        $criteria->compare('t.episode_total',$this->episode_total);
        $criteria->compare('t.episode_current',$this->episode_current);
        $criteria->compare('t.complete',$this->complete);
        $criteria->compare('t.comment_count',$this->comment_count);
        $criteria->compare('t.view_count',$this->view_count);
        $criteria->compare('t.tags',$this->tags,true);
        $criteria->compare('t.from_time',$this->from_time,true);
        $criteria->compare('t.to_time',$this->to_time,true);
        $criteria->compare('t.seo_title',$this->seo_title,true);
        $criteria->compare('t.seo_url',$this->seo_url,true);
        $criteria->compare('t.seo_desc',$this->seo_desc,true);
        $criteria->compare('t.creator',$this->creator,true);
        $criteria->compare('t.created_time',$this->created_time,true);
        $criteria->compare('t.editor',$this->editor,true);
        $criteria->compare('t.updated_time',$this->updated_time,true);
        $criteria->compare('t.sorder',$this->sorder,true);
        $criteria->compare('t.hot',$this->hot);
        $criteria->compare('t.promotion',$this->promotion);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.trash',$this->trash);

        if ($this->category_id !== NULL) {
            $categories = array();
            $categories[] = $this->category_id;
            $category = KitCategory::model()->findByPk($this->category_id);
            $descendants = $category->descendants()->findAll();
            foreach ($descendants as $cat) {
                $categories[] = $cat->id;
            }
            $criteria->addInCondition('ic.category_id', $categories);
        }
        
        $criteria->join = 'LEFT JOIN {{kit_film_category}} ic ON ic.film_id = t.id';
        
        $criteria->group = 't.id';
        
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
            'sort' => array(
                'defaultOrder' => 'id DESC',
            ),
		));
	}
    
    protected function beforeValidate() {
        $this->editor = Yii::app()->user->id;
        $this->updated_time = Common::getNow();                    
        if ($this->isNewRecord) {
            $this->creator = $this->editor;
            $this->created_time = $this->updated_time;
            $this->view_count = 0;
            $this->comment_count = 0;
        }
        return parent::beforeValidate();
    }
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitFilm::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitFilm::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitFilm::getLastest_'));
        Yii::app()->cache->delete(md5('KitFilm::getPromotion_'));
    }
    
    public function parseCategory($data, $delimiter = '') {
        $arr = array();
        foreach ($data as $value) {
            $arr[] = $value->name;                                     
        }
        echo implode($arr, $delimiter);    
    }
                

    ///////////////////////////////// Frontend /////////////////////////////////
    /**
     * treatment
     * @param mixed $data
     * @return array 
     */
    public static function treatment($data)
    {
        $data = CJSON::decode ((CJSON::encode($data))); // Neu la object thi chuyen thanh array
        if (empty($data))
            return array();
        elseif (!isset ($data[0]))
            $data = KitFilm::treatmentRow($data);
        else {
            foreach ($data as $key => $value) {
                $data[$key] = KitFilm::treatmentRow($value);
            }
        }
        return $data;
    }
    
    public static function treatmentRow($row)
    {
        $urlTitle = Common::convertStringToUrl($row['title']);
        $row['url'] = Yii::app()->createUrl($urlTitle.'-film-'.$row['id']);
        return $row;
    }
    
    public static function countRecord($from_time = NULL, $to_time = NULL) {
        if ($from_time == NULL) $from_time = '0000-00-00 00:00:00';
        if ($to_time == NULL) $to_time = date('Y-m-d H:i:s');
        
        $result = KitFilm::model()->count("created_time BETWEEN '".$from_time."' AND '".$to_time."'");
        return $result;
    }

    
    /**
     * Get Details
     * @param int $id
     * @return object 
     */
    public static function getDetails($id) {
        $id = intval($id);
        $cache_name = md5(__METHOD__ . '_' . $id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $result = KitFilm::model()->findByPk($id, array(
                'select' => Common::getFieldInTable(KitFilm::model()->getAttributes(), 't.'),
                'with' => array('creatorUser', 'editorUser')
            ));
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
    
    public static function getField($id, $field){
        $row = KitFilm::getDetails($id);
        $row = KitFilm::treatment($row);
        return letArray::get($row, $field, '');
    }

    public static function getLastest ($category = NULL, $limit = 20){
        $cache_name = md5(__METHOD__ . '_' . $category);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(KitFilm::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');


            if ($category !== NULL AND $category >= 0) {
                $catListObject = KitCategory::getListInParent('film', $category);
                $catList = array($category);
                foreach ($catListObject as $cat) {
                    $catList[] = $cat->id;
                }
                $criteria->join = "INNER JOIN {{kit_film_category}} t2 ON t.id=t2.film_id";
//                $criteria->group = "t.id";
                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', $catList).')');
            }

            $criteria->order = 't.id DESC';
            $criteria->limit = $limit;
            $result = KitFilm::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
    
    public static function getPromotion ($category = NULL, $limit = 20){
        $cache_name = md5(__METHOD__ . '_' . $category);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(KitFilm::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
            $criteria->condition = Common::addWhere($criteria->condition, 't.promotion = 1');

            if ($category !== NULL AND $category >= 0) {
                $criteria->join = "INNER JOIN {{kit_film_category}} t2 ON t.id=t2.film_id";
                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', KitCategory::getListInParent('film', $category)).')');
            }
            $criteria->order = 't.id DESC';
            $criteria->limit = $limit;
            $result = KitFilm::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

}
