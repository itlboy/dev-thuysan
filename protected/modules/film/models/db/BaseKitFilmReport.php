<?php

/**
 * This is the model class for table "{{kit_film_report}}".
 *
 * The followings are the available columns in table '{{kit_film_report}}':
 * @property string $id
 * @property integer $item_id
 * @property integer $episode_id
 * @property string $title
 * @property string $content
 * @property string $creator
 * @property string $created_time
 */
class BaseKitFilmReport extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitFilmReport the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_film_report}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item_id, title', 'required'),
			array('item_id, episode_id', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			array('creator', 'length', 'max'=>11),
			array('content, created_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, item_id, episode_id, title, content, creator, created_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('episode_id',$this->episode_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('creator',$this->creator,true);
		$criteria->compare('created_time',$this->created_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
		));
	}
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitFilmReport::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitFilmReport::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitFilmReport::getLastest_'));
        Yii::app()->cache->delete(md5('KitFilmReport::getPromotion_'));
    }

}
