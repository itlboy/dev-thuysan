<?php
/**
 * This is the model class for table "kit_film_episode".
 *
 * The followings are the available columns in table 'kit_film_episode':
 * @property string $id
 * @property integer $item_id
 * @property string $title
 * @property string $intro
 * @property string $embed
 * @property string $link
 * @property string $server
 * @property integer $default_video
 * @property integer $vip
 * @property integer $limit
 * @property string $limit_to
 * @property string $creator
 * @property string $created_time
 * @property string $editor
 * @property string $updated_time
 * @property string $checked_time
 * @property integer $is_die
 * @property string $sorder
 * @property integer $hot
 * @property integer $promotion
 * @property integer $status
 * @property integer $trash
 */

class BaseKitFilmEpisode extends ActiveRecord {
    var $className = __CLASS__;
    
    public $category_id;
    public $categories;
    
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitFilmEpisode the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_film_episode}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item_id, title', 'required'),
			array('item_id, default_video, vip, limit, is_die, hot, promotion, status, trash', 'numerical', 'integerOnly'=>true),
			array('title, link', 'length', 'max'=>255),
			array('intro', 'length', 'max'=>500),
			array('server', 'length', 'max'=>100),
			array('creator, editor', 'length', 'max'=>11),
			array('sorder', 'length', 'max'=>10),
			array('embed, limit_to, created_time, updated_time, checked_time', 'safe'),
            array('categories', 'safe'),
			// The following rule is used by search(). Please remove those attributes that should not be searched.
			array('id, item_id, title, intro, embed, link, server, default_video, vip, limit, limit_to, creator, created_time, editor, updated_time, checked_time, is_die, sorder, hot, promotion, status, trash', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related class name for the relations automatically generated below.
		return array(
            'creatorUser' => array(KitFilmEpisode::BELONGS_TO, 'KitAccount', 'creator', 'select' => 'username'),
            'editorUser' => array(KitFilmEpisode::BELONGS_TO, 'KitAccount', 'editor', 'select' => 'username'),
            'stats' => array(self::HAS_ONE, 'KitStats', 'item_id',
                'condition' => 'module = "film"'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;
        $criteria->select = Common::getFieldInTable(KitFilmEpisode::model()->getAttributes(), 't.');
        
        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.item_id',$this->item_id);
        $criteria->compare('t.title',$this->title,true);
        $criteria->compare('t.intro',$this->intro,true);
        $criteria->compare('t.embed',$this->embed,true);
        $criteria->compare('t.link',$this->link,true);
        $criteria->compare('t.server',$this->server,true);
        $criteria->compare('t.default_video',$this->default_video);
        $criteria->compare('t.vip',$this->vip);
        $criteria->compare('t.limit',$this->limit);
        $criteria->compare('t.limit_to',$this->limit_to,true);
        $criteria->compare('t.creator',$this->creator,true);
        $criteria->compare('t.created_time',$this->created_time,true);
        $criteria->compare('t.editor',$this->editor,true);
        $criteria->compare('t.updated_time',$this->updated_time,true);
        $criteria->compare('t.checked_time',$this->checked_time,true);
        $criteria->compare('t.is_die',$this->is_die);
        $criteria->compare('t.sorder',$this->sorder,true);
        $criteria->compare('t.hot',$this->hot);
        $criteria->compare('t.promotion',$this->promotion);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.trash',$this->trash);
        
        $criteria->group = 't.id';
        
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
            'sort' => array(
                'defaultOrder' => 'id DESC',
            ),
		));
	}
    
    protected function beforeValidate() {
        $this->editor = Yii::app()->user->id;
        $this->updated_time = Common::getNow();                    
        if ($this->isNewRecord) {
            $this->creator = $this->editor;
            $this->created_time = $this->updated_time;
        }
        return parent::beforeValidate();
    }
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitFilmEpisode::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitFilmEpisode::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitFilmEpisode::getLastest_'));
        Yii::app()->cache->delete(md5('KitFilmEpisode::getPromotion_'));
    }
    
    public function parseCategory($data, $delimiter = '') {
        $arr = array();
        foreach ($data as $value) {
            $arr[] = $value->name;                                     
        }
        echo implode($arr, $delimiter);    
    }
                

    ///////////////////////////////// Frontend /////////////////////////////////
    /**
     * treatment
     * @param mixed $data
     * @return array 
     */
    public static function treatment($data)
    {
        $data = CJSON::decode ((CJSON::encode($data))); // Neu la object thi chuyen thanh array
        if (empty($data))
            return array();
        elseif (!isset ($data[0]))
            $data = KitFilmEpisode::treatmentRow($data);
        else {
            foreach ($data as $key => $value) {
                $data[$key] = KitFilmEpisode::treatmentRow($value);
            }
        }
        return $data;
    }
    
    public static function treatmentRow($row)
    {
        $urlTitle = Common::convertStringToUrl($row['title']);
        $row['url'] = Yii::app()->createUrl($urlTitle.'-film-'.$row['id']);
        return $row;
    }
    
    public static function countRecord($from_time = NULL, $to_time = NULL) {
        if ($from_time == NULL) $from_time = '0000-00-00 00:00:00';
        if ($to_time == NULL) $to_time = date('Y-m-d H:i:s');
        
        $result = KitFilmEpisode::model()->count("created_time BETWEEN '".$from_time."' AND '".$to_time."'");
        return $result;
    }

    
    /**
     * Get Details
     * @param int $id
     * @return object 
     */
    public static function getDetails($id) {
        $id = intval($id);
        $cache_name = md5(__METHOD__ . '_' . $id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $result = KitFilmEpisode::model()->findByPk($id, array(
                'select' => Common::getFieldInTable(KitFilmEpisode::model()->getAttributes(), 't.'),
                'with' => array('creatorUser', 'editorUser')
            ));
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
    
    public static function getField($id, $field){
        $row = KitFilmEpisode::getDetails($id);
        $row = KitFilmEpisode::treatment($row);
        return letArray::get($row, $field, '');
    }

    public static function getLastest ($category = NULL, $limit = 20){
        $cache_name = md5(__METHOD__ . '_' . $category);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(KitFilmEpisode::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');


            if ($category !== NULL AND $category >= 0) {
                $catListObject = KitCategory::getListInParent('film', $category);
                $catList = array($category);
                foreach ($catListObject as $cat) {
                    $catList[] = $cat->id;
                }
                $criteria->join = "INNER JOIN {{kit_film_category}} t2 ON t.id=t2.film_id";
//                $criteria->group = "t.id";
                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', $catList).')');
            }

            $criteria->order = 't.id DESC';
            $criteria->limit = $limit;
            $result = KitFilmEpisode::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
    
    public static function getPromotion ($category = NULL, $limit = 20){
        $cache_name = md5(__METHOD__ . '_' . $category);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(KitFilmEpisode::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
            $criteria->condition = Common::addWhere($criteria->condition, 't.promotion = 1');

            if ($category !== NULL AND $category >= 0) {
                $criteria->join = "INNER JOIN {{kit_film_category}} t2 ON t.id=t2.film_id";
                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', KitCategory::getListInParent('film', $category)).')');
            }
            $criteria->order = 't.id DESC';
            $criteria->limit = $limit;
            $result = KitFilmEpisode::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

}
