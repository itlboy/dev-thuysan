<?php
class DefaultController extends ControllerBackend
{
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id) {
		$model=KitPin::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

    public function actionIndex() {
        $model = new KitPin('search');
        $model->unsetAttributes();  // clear any default values

        // Category
        if (Yii::app()->request->getQuery('category_id') !== NULL) {
            $model->category_id = Yii::app()->request->getQuery('category_id');
        }

        if (isset($_GET['KitPin'])) {
            $model->attributes = $_GET['KitPin'];
        }

        // page size drop down changed
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$model = new KitPin;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$this->save($model);

		$this->render('edit',array(
			'model'=>$model
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id) {
		$model=$this->loadModel($id);
        $categoriesOld = $model->categories = KitPinCategory::model()->get($model->id);
        
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

            $this->save($model, $categoriesOld);

		$this->render('edit',array(
			'model' => $model
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionInfo($id) {
        $model = KitPin::getDetails($id);

		$this->render('info',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
                
            echo json_encode(array('status' => 'success'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

    
	protected function save($model, $categoriesOld = NULL) {
		if(isset($_POST['KitPin'])) {
            // Upload image
			if ($_POST['KitPin']['image'] !== NULL AND $_POST['KitPin']['image'] !== '') {
                $_POST['KitPin']['image'] = Common::createThumb($this->module->getName() , $_POST['KitPin']['image'], letArray::get($_POST['KitPin'], 'title', ''));
                
                // Remove Image
                Common::removeImage($this->module->id, $model->image);
            } else unset ($_POST['KitPin']['image']);

			if ($_POST['KitPin']['from_time'] == NULL OR $_POST['KitPin']['from_time'] == '') {
                unset($_POST['KitPin']['from_time']);
            }

            if ($_POST['KitPin']['to_time'] == NULL OR $_POST['KitPin']['to_time'] == '') {
                unset($_POST['KitPin']['to_time']);
            }

            $model->attributes = $_POST['KitPin'];

            $categoriesNew = $model->categories;
            
                
            if ($model->validate()) {
                if($model->save()) {
                    $itemId = $model->id;
                    if (!$model->isNewRecord)
                        KitPinCategory::del($categoriesOld, $itemId);
                    KitPinCategory::create($categoriesNew, $itemId);
					
                    if (Yii::app()->request->getPost('apply') == 0)
                         $this->redirect(array('index'));
                    else $this->redirect(array('update', 'id' => $model->id));
                }
            }
		}
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model) {
		if(isset($_POST['ajax']) && $_POST['ajax']==='kit-pin-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    
        
}
