<?php
/**
 * Module: pin
 * Auth: nguago@let.vn
 * Website: http://let.vn
 */
class ManageController extends ControllerFrontend
{
//    public $layout = '//layouts/wall';

    public function actionIndex() {
        $this->render('index');
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new KitPin;

        $this->save($model);
        
		$this->render('update',array(
			'model' => $model
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id) {
        $model = $this->loadModel($id);
        
        // Ko dùng RBAC đành check quyền owner thế này
        if ($model->creator !== Yii::app()->user->id)
            throw new CHttpException(404,'The requested page does not exist.');

        $categoriesOld = $model->categories = KitPinCategory::model()->get($model->id);

        $this->save($model, $categoriesOld);

		$this->render('update',array(
			'model' => $model
		));
	}
    
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
        $model = $this->loadModel($id);

        if ($model->creator != Yii::app()->user->id)
            throw new CHttpException(404,'The requested page does not exist.');

        $model->delete();
        Yii::app()->controller->redirect(array('//pin/wall'));
	}
    
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id) {
		$model = KitPin::model()->findByPk($id);
		if($model === NULL)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
    

	protected function save($model, $categoriesOld = NULL) {
		if(isset($_POST['KitPin'])) {
            // Upload image
			if ($_POST['KitPin']['image'] !== NULL AND $_POST['KitPin']['image'] !== '') {
                $_POST['KitPin']['image'] = Common::createThumb($this->module->getName() , $_POST['KitPin']['image'], letArray::get($_POST['KitPin'], 'title', ''));

                // Remove Image
                Common::removeImage($this->module->id, $model->image);
            } else unset ($_POST['KitPin']['image']);

			if (isset($_POST['KitPin']['from_time']) AND ($_POST['KitPin']['from_time'] == NULL OR $_POST['KitPin']['from_time'] == '')) {
                unset($_POST['KitPin']['from_time']);
            }

            if (isset($_POST['KitPin']['from_time']) AND ($_POST['KitPin']['to_time'] == NULL OR $_POST['KitPin']['to_time'] == '')) {
                unset($_POST['KitPin']['to_time']);
            }

            $model->attributes = $_POST['KitPin'];

            $categoriesNew = $model->categories;

            if ($model->validate()) {
                if($model->save()) {
                    $itemId = $model->id;
                    if (!$model->isNewRecord)
                        KitPinCategory::del($categoriesOld, $itemId);
                    KitPinCategory::create($categoriesNew, $itemId);

                    if (Yii::app()->request->getPost('apply') == 0)
                         Yii::app()->controller->redirect(array('//pin/wall'));
                    else $this->redirect(array('update', 'id' => $model->id));
                }
            }
		}
	}
}