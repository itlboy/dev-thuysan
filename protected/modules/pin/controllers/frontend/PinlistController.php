<?php
/**
 * Module: pin
 * Auth: nguago@let.vn
 * Website: http://let.vn
 */
class PinlistController extends ControllerFrontend
{
	public function actionIndex()
	{       
		$this->render('index', array(
            'category' => Yii::app()->request->getQuery('category')
        ));
	}
    
	public function actionType()
	{             
		$this->render('type', array(
            'type' => Yii::app()->request->getQuery('type'),
            'category' => Yii::app()->request->getQuery('category'),
        ));
	}
}