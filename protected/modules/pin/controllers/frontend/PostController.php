<?php

class PostController extends ControllerFrontend {

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('image', 'video', 'story', 'update', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }

    // Title, Image, Content
    public function actionImage() {
        $model = new KitPin('image');
        $model->type = 'image';
        $this->_post($model);
    }

    // Title, Video, Content
    public function actionVideo() {
        $model = new KitPin('video');
        $model->type = 'video';
        $this->_post($model);
    }

    // Title, Content
    public function actionStory() {
        $model = new KitPin('text');
        $model->type = 'text';
        $this->_post($model);
    }

    private function _post($model, $categoriesOld = NULL) {
        if (isset($_POST['KitPin'])) {
            // Upload image
            if ($_POST['KitPin']['image'] !== NULL AND $_POST['KitPin']['image'] !== '') {
                $_POST['KitPin']['image'] = Common::createThumb($this->module->getName(), $_POST['KitPin']['image'], letArray::get($_POST['KitPin'], 'title', ''));

                // Remove Image
                Common::removeImage($this->module->id, $model->image);
            }
            else
                unset($_POST['KitPin']['image']);

            if (isset($_POST['KitPin']['from_time']) AND ($_POST['KitPin']['from_time'] == NULL OR $_POST['KitPin']['from_time'] == '')) {
                unset($_POST['KitPin']['from_time']);
            }

            if (isset($_POST['KitPin']['from_time']) AND ($_POST['KitPin']['to_time'] == NULL OR $_POST['KitPin']['to_time'] == '')) {
                unset($_POST['KitPin']['to_time']);
            }

            $model->attributes = $_POST['KitPin'];

            $categoriesNew = array($model->categories);

            if ($model->validate()) {
                if ($model->save()) {
                    $itemId = $model->id;
                    if (!$model->isNewRecord)
                        KitPinCategory::del($categoriesOld, $itemId);
                    KitPinCategory::create($categoriesNew, $itemId);
                    
                    // Post to facebook
                    $this->_postToFacebook($model->title, $model->content, Yii::app()->request->hostInfo.Yii::app()->createUrl(Common::convertStringToUrl($model->title).'-pin-'.$model->id));

                    if (Yii::app()->request->getPost('apply') == 0)
                        Yii::app()->controller->redirect(Yii::app()->baseUrl . '/wall-' . $model->creator . '.let');
                    else
                        $this->redirect(array('update', 'id' => $model->id));
                }
            }
        }

        $this->render('form', array(
            'model' => $model,
            'user_id' => Yii::app()->user->id
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {        
        $model = $this->loadModel($id);
        if ($model->type == 'image') {
            $model->scenario = 'image';
        } elseif ($model->type == 'video') {
            $model->scenario = 'video';
        } elseif ($model->type == 'text') {
            $model->scenario = 'text';
        }

        // Ko dùng RBAC đành check quyền owner thế này
        if ($model->creator !== Yii::app()->user->id)
            throw new CHttpException(404, 'The requested page does not exist.');

        $categoriesOld = KitPinCategory::model()->get($model->id);
        if (!empty($categoriesOld))
            $model->categories = $categoriesOld[0];

        $this->_post($model, $categoriesOld);
    }

//    /**
//     * Deletes a particular model.
//     * If deletion is successful, the browser will be redirected to the 'admin' page.
//     * @param integer $id the ID of the model to be deleted
//     */
//    public function actionDelete($id) {
//        $model = $this->loadModel($id);
//
//        if ($model->creator != Yii::app()->user->id)
//            throw new CHttpException(404, 'The requested page does not exist.');
//
//        $model->delete();
//
//        Yii::app()->controller->redirect(Yii::app()->baseUrl . '/wall-' . $model->creator . '.let');
//    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = KitPin::model()->findByPk($id);
        if ($model === NULL)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    private function _postToFacebook($name, $content, $link) {
        Yii::import('application.vendors.fbsdk.*');
        require_once 'facebook.php';
        
        $hoauth = require_once Yii::getPathOfAlias('application.config') . '/hoauth.php';        
        $facebook = new Facebook(array(
            'appId' => $hoauth['providers']['Facebook']['keys']['id'],
            'secret' => $hoauth['providers']['Facebook']['keys']['secret'],
        ));

        $userId = $facebook->getUser();
        if ($userId) {
            try {
                $response = $facebook->api('/me/feed', 'POST', array(
                    'name' => $name,
                    'link' => $link,
                    'message' => $content
                ));
                //echo '<pre>Post ID: ' . $response['id'] . '</pre>';
            } catch (FacebookApiException $e) {}
        } else {
            //$login_url = $facebook->getLoginUrl( array( 'scope' => 'publish_stream' ) );
            //echo 'Please <a href="' . $login_url . '">login.</a>';
        }              
    }

}
