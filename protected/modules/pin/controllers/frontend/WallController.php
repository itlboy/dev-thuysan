<?php
//Yii::import('application.modules.cms.models.KitStats');
class WallController extends ControllerFrontend
{

	public function actionIndex()
	{
        $user_id = (string)intval(Yii::app()->request->getParam('id', NULL));

        $url = '';
        if (empty($user_id) OR $user_id <= 0) { // Khong ton tai user ID
            if (!Yii::app()->user->isGuest) { // Da dang nhap
                $url = Yii::app()->createUrl('//wall-' . Yii::app()->user->id);
            } else { // Chua dang nhap
                $url = Yii::app()->getBaseUrl(true);
            }
        }
        if ($url !== '') $this->redirect($url);

        $this->render('index', array(
            'user_id' => $user_id,
        ));
	}

    // Trang hình ảnh của thành viên
	public function actionImage()
	{
        $style = 1;
        if (isset($_GET['style'])) {
            $style = intval($_GET['style']);
            Yii::app()->request->cookies['pin_wall_image_style'] = new CHttpCookie('pin_wall_image_style', $style);
        } else if (isset(Yii::app()->request->cookies['pin_wall_image_style'])) {
            $style = Yii::app()->request->cookies['pin_wall_image_style']->value;
        }

        $viewImage = 'image_' . $style;
        $user_id = intval(Yii::app()->request->getParam('id', NULL));
        $this->render('image', array(
            'user_id' => $user_id,
            'viewImage' => $viewImage
        ));
	}

    // Trang video của thành viên
	public function actionVideo()
	{
        $user_id = intval(Yii::app()->request->getParam('id', NULL));
        $this->render('video', array(
            'user_id' => $user_id,
        ));
	}

    // Trang truyện cười của thành viên
	public function actionStory()
	{
        $user_id = intval(Yii::app()->request->getParam('id', NULL));
        $this->render('story', array(
            'user_id' => $user_id,
        ));
	}

    // Trang Chat của thành viên
	public function actionChat()
	{
        $user_id = intval(Yii::app()->request->getParam('id', NULL));
        $this->render('chat', array(
            'user_id' => $user_id,
        ));
	}

	public function actionCreate()
	{
        $this->render('form');
	}

	public function actionList()
	{
        $this->widget('application.modules.pin.widgets.frontend.pinwall', array(
            'creator' => Yii::app()->request->getQuery('creator'),
            'limit' => Yii::app()->request->getQuery('limit'),
            'type' => Yii::app()->request->getQuery('type'),
            'view' => Yii::app()->request->getQuery('type'),
        ));
	}

    public function actionStats() {
        Yii::import('application.vendors.fbsdk.*');
        require_once 'facebook.php';

        // Do cấu hình chính xác nên ko cần check 2 cái id, secret có tồn tại hay chưa.
        $hoauth = require_once Yii::getPathOfAlias('application.config') . '/hoauth.php';
        $facebook = new Facebook(array(
            'appId' => $hoauth['providers']['Facebook']['keys']['id'],
            'secret' => $hoauth['providers']['Facebook']['keys']['secret'],
        ));

        $urlList = array(
            '"http://cuoi.let.vn/phu-vl-pin-5593.let"',
            '"http://cuoi.let.vn/hai-pin-5587.let"',
            '"http://cuoi.let.vn/do-ban-khong-cuoi-duoc-day-pin-5595.let"',
        );

        if (empty($urlList))
            return;

        $fql = 'SELECT url, like_count, share_count, comment_count, click_count, total_count
                FROM link_stat
                WHERE url IN (' . implode(', ', $urlList) . ')';

        $result = $facebook->api(array(
            'method' => 'fql.query',
            'query' => $fql
        ));
        var_dump($result);
        die;
    }
}
