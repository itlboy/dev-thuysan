<?php

//Yii::import('application.modules.cms.models.KitStats');
class DetailsController extends ControllerFrontend {

    public function actionIndex() {
        $id = intval(Yii::app()->request->getParam('id', NULL));
//        $host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
//        if ($host !== 'vailua.vn') {
//            $this->redirect('http://vailua.vn/letphim-pin-'.$id.'.vl'); die;
//        }
        
        $cache_name = md5('pin_details_' . $id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {

            // Get details
            $assign['data'] = KitPin::getDetails($id);
            if (!empty($assign['data']) AND ($assign['data']->status == 1) AND ($assign['data']->trash == 0)) {
                $assign['data'] = KitPin::treatment($assign['data']);
                $assign['user'] = KitAccount::model()->findByPk($assign['data']['creator']);
                $assign['user'] = CJSON::decode(CJSON::encode($assign['user']));
            } else {
                $this->redirect(Yii::app()->getBaseUrl(true));
            }
            // Get category list
            $criteria = new CDbCriteria();
            $criteria->alias = 't';
            $criteria->join = "INNER JOIN {{kit_pin_category}} t2 ON t.id = t2.category_id";
            $criteria->condition = Common::addWhere($criteria->condition, 't2.pin_id =' . $id);
            $assign['category'] = KitCategory::model()->findAll($criteria);
            $assign['category'] = KitCategory::treatment($assign['category']);

            Yii::app()->cache->set($cache_name, $assign); // Set cache
        }
        else
            $assign = $cache;

//            // Category
//            $categorys = $assign['data']->KitPinCategory;
//            foreach ($categorys as $category) {
//                $assign['breadcrumb'][] = KitCategory::getBreadcrumb($category->id);
//            }
        // Meta
        if (isset($assign['data']['title']))
            $this->pageTitle = $assign['data']['title'];
        if (isset($assign['data']['intro']))
            Yii::app()->clientScript->registerMetaTag($assign['data']['intro'], 'content');

        Yii::app()->clientScript->registerMetaTag('100001754856259', NULL, NULL, array('property' => 'fb:admins'));
//		if (isset($assign['data']['facebook_id']) AND $assign['data']['facebook_id'] !== '')
//        	Yii::app()->clientScript->registerMetaTag($assign['data']['facebook_id'], NULL, NULL, array('property'=>'fb:page_id'));

        Yii::app()->clientScript->registerMetaTag('website', NULL, NULL, array('property' => 'og:type'));
        Yii::app()->clientScript->registerMetaTag($assign['data']['title'], NULL, NULL, array('property' => 'og:title'));

        if ($assign['data']['type'] == 'image') {
            Yii::app()->clientScript->registerMetaTag(Common::getImageUploaded('pin/medium/' . $assign['data']['image']), NULL, NULL, array('property' => 'og:image'));
            Yii::app()->clientScript->registerLinkTag('image_src', NULL, Common::getImageUploaded('pin/medium/' . $assign['data']['image']));
        } elseif ($assign['data']['type'] == 'video') {
            Yii::import('application.vendors.libs.grabVideo');
            Yii::app()->clientScript->registerMetaTag('http://img.youtube.com/vi/' . grabVideo::getYoutubeId($assign['data']['video']) . '/0.jpg', NULL, NULL, array('property' => 'og:image'));
            Yii::app()->clientScript->registerLinkTag('image_src', NULL, 'http://img.youtube.com/vi/' . grabVideo::getYoutubeId($assign['data']['video']) . '/0.jpg');
            Yii::app()->clientScript->registerMetaTag('http://www.youtube.com/embed/' . grabVideo::getYoutubeId($assign['data']['video']), NULL, NULL, array('property' => 'og:video'));
        }

        Yii::app()->clientScript->registerMetaTag($assign['data']['url'], NULL, NULL, array('property' => 'og:url'));
        Yii::app()->clientScript->registerMetaTag('cuoi.let.vn', NULL, NULL, array('property' => 'og:site_name'));

        Yii::app()->clientScript->registerMetaTag($assign['data']['title'], 'title');
        Yii::app()->clientScript->registerLinkTag('canonical', NULL, $assign['data']['url']);

        if (isset($assign['data']['id'])) {
//            KitStats::updateValue($assign['data']['id'],'pin');
            $assign['stats'] = KitStats::getDetails('pin', $assign['data']['id']);
            KitStats::setStats('pin', $assign['data']['id']);
        }

        $this->render('index', $assign);
    }

    public function actionEpisode() {
        $this->render('episode');
    }

}