<?php

Yii::import('application.modules.pin.models.db.BaseKitPinUserinfo');
class KitPinUserinfo extends BaseKitPinUserinfo
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}