<?php

/**
 * This is the model class for table "{{kit_pin_userinfo}}".
 *
 * The followings are the available columns in table '{{kit_pin_userinfo}}':
 * @property string $id
 * @property integer $user_id
 * @property string $username
 * @property integer $comment_count
 * @property integer $view_count
 * @property integer $like_count
 * @property integer $share_count
 */
class BaseKitPinUserinfo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitPinUserinfo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_pin_userinfo}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id', 'required'),
			array('user_id, comment_count, view_count, like_count, share_count', 'numerical', 'integerOnly'=>true),
			array('username', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, username, comment_count, view_count, like_count, share_count', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('comment_count',$this->comment_count);
		$criteria->compare('view_count',$this->view_count);
		$criteria->compare('like_count',$this->like_count);
		$criteria->compare('share_count',$this->share_count);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
		));
	}
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitPinUserinfo::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitPinUserinfo::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitPinUserinfo::getLastest_'));
        Yii::app()->cache->delete(md5('KitPinUserinfo::getPromotion_'));
    }

}
