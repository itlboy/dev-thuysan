<?php
Yii::import('application.modules.pin.models.db.BaseKitPin');
class KitPin extends BaseKitPin{

    public $username;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
        return CMap::mergeArray(
            parent::rules(),
            array(
                array('title', 'checkTitle'),
                array('categories', 'required'),
                array('image', 'required', 'on' => 'image'),
                array('video', 'required', 'on' => 'video'),
                array('content', 'required', 'on' => 'text'),
            )
        );
	}
    
    public function checkTitle($attribute, $params) {
        $value = $this->$attribute;
        if (mb_strlen($value) < 6)
            $this->addError($attribute, 'Tiêu đề phải lớn hơn 5 ký tự');
        else if (!preg_match('/^[\w][\w]+/u', $value)) {
            $this->addError($attribute, 'Tiêu đề không hợp lệ. Chứa quá nhiều ký tự đặc biệt');
        }
    }
    
    protected function beforeSave()
    {
        if (!empty($this->video)) {
            $this->type = 'video';
        } elseif (!empty($this->image)) {
            $this->type = 'image';
        } else {
            $this->type = 'text';
        }

        return parent::beforeSave();
    }

    /**
     * Action after Save
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }

    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }

    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitPin::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitPin::getPromotion_' . $category->id));

            Yii::app()->cache->delete(md5('pin_list_' . $category->id . '_'));
            
            Yii::app()->cache->delete(md5('pin_wall_image_' . $category->id . '_'));
            Yii::app()->cache->delete(md5('pin_wall_video_' . $category->id . '_'));
            Yii::app()->cache->delete(md5('pin_wall_text_' . $category->id . '_'));
        }
        Yii::app()->cache->delete(md5('KitPin::getLastest_'));
        Yii::app()->cache->delete(md5('KitPin::getPromotion_'));

        Yii::app()->cache->delete(md5('pin_filter_hot'));
        Yii::app()->cache->delete(md5('pin_list_'));
        Yii::app()->cache->delete(md5('pin_list__' . $this->creator));
        
        Yii::app()->cache->delete(md5('pin_wall_image'));
        Yii::app()->cache->delete(md5('pin_wall_video'));
        Yii::app()->cache->delete(md5('pin_wall_text'));        
        Yii::app()->cache->delete(md5('pin_wall_image_' . $this->creator));
        Yii::app()->cache->delete(md5('pin_wall_video_' . $this->creator));
        Yii::app()->cache->delete(md5('pin_wall_text_' . $this->creator));        
    }

    public function listTypeOptions() {
        return array(
            'text' => 'Text',
            'image' => 'Image',
            'video' => 'Video',
        );
    }

    public static function treatmentRow($row)
    {
        $urlTitle = Common::convertStringToUrl($row['title']);
        $row['url'] = Yii::app()->request->hostInfo.Yii::app()->createUrl($urlTitle.'-pin-'.$row['id']);
        $row['title'] = str_replace (array ('"', "'"), array ('&quot;', '&apos;'), $row['title'] );
//                $row['title'] = str_replace ( array ( '&', '"', "'", '<', '>', '�' ), array ( '&amp;' , '&quot;', '&apos;' , '&lt;' , '&gt;', '&apos;' ), $row['title'] );
//        $row['url'] = 'http://vailua.vn'.Yii::app()->createUrl($urlTitle.'-pin-'.$row['id']);
//        $row['url'] = preg_replace('/\.let$/', '.vl', $row['url']);
        return $row;
    }


    public function getWallUrl() {
        // Chua dung den
    }

}