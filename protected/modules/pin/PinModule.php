<?php
class PinModule extends CWebModule
{
    public $category = TRUE; // Xác định Module có category hay không
    public $menuList = array(
        array(
            'name' => 'Tạo danh mục',
            'url' => '/category/default/create/module/pin',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'type' => 'dialog',
            'grid' => '',
        ),
        array(
            'name' => 'Quản lý danh mục',
            'url' => '/category/default/index/module/pin',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Quản lý danh mục bài viết',
            'type' => 'direct'
        ),
        array(
            'name' => 'Tạo pin',
            'url'  => '/pin/default/create',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Tạo pin',
            'type' => 'direct'
        ),
        array(
            'name' => 'Quản lý pin',
            'url'  => '/pin/default/index',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Quản lý pin',
            'type' => 'direct'
        ),
    );
    public $image = array(
        'origin' => array(
            'type' => 'move',
            'folder' => 'origin',
        ),
        'large' => array(
            'type' => 'resize',
            'width' => 600,
            'height' => 10000,
            'cropWidth' => NULL,
            'cropHeight' => NULL,
            'cropTop' => NULL,
            'cropLeft' => NULL,
            'rotate' => 0, // Xoay anh
            'flip' => NULL, //[integer] Direction. Horizontal = 5, Vertical = 6
            'quality' => 100,
            'folder' => 'large',
//			'addlogo' => 'themes/frontend/vailua/images/logo_bind.png',
//			'addtext' => 'Nguago - let.vn',
        ),
        'medium' => array(
            'type' => 'crop',
            'width' => 130,
            'height' => 130,
            'cropWidth' => NULL,
            'cropHeight' => NULL,
            'cropTop' => NULL,
            'cropLeft' => NULL,
            'rotate' => 0, // Xoay anh
            'flip' => NULL, //[integer] Direction. Horizontal = 5, Vertical = 6
            'quality' => 80,
            'folder' => 'medium',
        ),
        'small' => array(
            'type' => 'crop',
            'width' => 70,
            'height' => 70,
            'cropWidth' => NULL,
            'cropHeight' => NULL,
            'cropTop' => NULL,
            'cropLeft' => NULL,
            'rotate' => 0, // Xoay anh
            'flip' => NULL, //[integer] Direction. Horizontal = 5, Vertical = 6
            'quality' => 100,
            'folder' => 'small',
        ),
    );

    public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'pin.models.*',
			'pin.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
