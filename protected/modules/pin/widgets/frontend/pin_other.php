<?php

class pin_other extends Widget {

    public $view = '';
    public $title = '';
    public $limit = 10;
    public $group_count = 0;

    public $category = array();
    public $creator = 0;
    public $creator_deny = 0;
    public $where = '';
    public $order_by = 'id DESC';

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
        Yii::import('pin.models.KitPin');
    }

    public function run() {
		
        $cache_name = md5(__METHOD__ . '_' . implode('-', $this->category) . '_' . $this->creator . '_' . $this->creator_deny . '_' . $this->where . '_' . $this->order_by);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            //$criteria->select = Common::getFieldInTable(KitPin::model()->getAttributes(), 't.');
            //$criteria->condition = 't.status = 1';
            $criteria->select = 't.*';
            $criteria->condition = 't.status = 1';

            if (is_array($this->category) AND !empty($this->category)) {
                $criteria->join = "INNER JOIN {{kit_pin_category}} t2 ON t.id=t2.pin_id";
                $criteria->addInCondition ('t2.category_id', $this->category);                                                                             
            }

            if ($this->creator > 0) {
                $criteria->addCondition('creator = ' . $this->creator);
            }

            if ($this->creator_deny > 0) {
                $criteria->addCondition('creator != ' . $this->creator);
            }

            if ($this->where !== '') {
                $criteria->addCondition($this->where);
            }
            
            $criteria->order = $this->order_by;
            $criteria->limit = 20;
            $criteria->group = 't.id';
            $data = KitPin::model()->findAll($criteria);
            if ($this->order_by = 'RAND()')
                $cache_time = 60 * 60;
            else
                $cache_time = 0;
            Yii::app()->cache->set($cache_name, $data, $cache_time); // Set cache
        }
        else
            $data = $cache;
        if (empty($data))
            return FALSE;
        $data = KitPin::treatment($data);
        $data = array_slice($data, 0, $this->limit);
        $this->render($this->view, array(
            'data' => $data
        ));
    }

}