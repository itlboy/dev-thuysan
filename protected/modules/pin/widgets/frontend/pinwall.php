<?php

class pinwall extends Widget {

    public $creator = NULL;
    public $category = NULL;    // ID danh mục
    public $limit = 30;
    public $type = NULL;    // image | video | text
    public $view = 'image';
    public $viewImage = 'image_1';

    public $request = FALSE;    // Xác định sẽ lấy URI mặc định (FALSE) hay URI hiện tại

    public function init() {
        Yii::import('pin.models.KitPin');

        if ($this->view == '')
            $this->view = __CLASS__;
    }

    public function run() {
        $offset = 0;
        $criteria = new CDbCriteria;
        $criteria->select = 't.*';
        $criteria->condition = 't.status = 1';

        $cacheCreator = NULL;
        if ($this->creator !== NULL) {
            $cacheCreator = (int) $this->creator;
            $criteria->addCondition ('creator = ' . $cacheCreator);
        }

        // Type
        if ($this->type != NULL AND in_array($this->type, array('image', 'video', 'text'))) {
            $criteria->compare('t.type', $this->type);
        }

        // Category
        $cacheCategory = NULL;
        if ($this->category !== NULL AND (int) $this->category > 0) {
            $cacheCategory = $category = (int) $this->category;
            $catListObject = KitCategory::getListInParent('pin', $category);
            $catList = array($category);
            foreach ($catListObject as $cat) {
                $catList[] = $cat->id;
            }
            $criteria->join = "INNER JOIN {{kit_pin_category}} t2 ON t.id=t2.pin_id";
            $criteria->addCondition('t2.category_id IN ('.implode(',', $catList).')');
        }

//        $total = KitPin::model()->count($criteria);

        // Chắc tách ra 2 widget
        if ($this->type == 'image' AND isset($_GET['page'])) {
            $offset = (((int) $_GET['page'] - 1) * $this->limit) + $this->limit;
        } else if ($this->type == 'image' AND isset($_POST['offset'])) {
            $offset = (int) $_POST['offset'] + $this->limit;
        } else if ($this->type == 'video' AND isset($_POST['offset'])) {
            $offset = (int) $_POST['offset'] + $this->limit;
        }
        $criteria->offset = $offset;

        $criteria->limit = $this->limit;
        $criteria->order = 't.id DESC';

//        $url = ($this->request == FALSE) ? Yii::app()->createUrl('//pin/wall/list', array('creator' => $this->creator, 'type' => $this->type)) : Yii::app()->request->requestUri;
        $url = Yii::app()->createUrl('//pin/wall/list', array('creator' => $this->creator, 'type' => $this->type, 'limit' => $this->limit));

        // user for video
        $assign['url'] = $url;
//        $assign['wall'] = $this->wall;
        $assign['offset'] = $offset;
//        $assign['total'] = $total;

        $cacheId = NULL;
        if ($offset == 0) {
            if ($cacheCreator != NULL AND $cacheCategory == NULL) {
                $cacheId = md5('pin_wall_' . $this->type . '_' . $cacheCreator);
            } elseif ($cacheCreator == NULL AND $cacheCategory != NULL) {
                $cacheId = md5('pin_wall_' . $this->type . '_' . $cacheCategory . '_');
            } else {
                $cacheId = md5('pin_wall_' . $this->type);
            }
        }

        $model = ($cacheId != NULL)
            ? $this->getData($cacheId, $criteria)
            : KitPin::model()->findAll($criteria);

        $assign['rows'] = $model;
        if ($assign['rows'] !== array())
            $assign['rows'] = KitPin::treatment($assign['rows']);

        $baseUrl = Yii::app()->theme->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');

        if (isset(Yii::app()->request->cookies['pin_wall_image_style']))
            $this->viewImage = 'image_' . (int)Yii::app()->request->cookies['pin_wall_image_style']->value;
        
        // RegisterScript for Image
        if ($this->viewImage == 'image_1') {
            $cs->registerScriptFile($baseUrl . '/plugins/finalTilesGallery/js/jquery.finalTilesGallery.js');
            $cs->registerCssFile($baseUrl . '/plugins/finalTilesGallery/css/ftg.css');

            $cs->registerScriptFile($baseUrl . '/plugins/prettyPhoto/jquery.prettyPhoto.js');
            $cs->registerCssFile($baseUrl . '/plugins/prettyPhoto/jquery.prettyPhoto.css');

            $cs->registerScript('PinWall', "
                jQuery('#cnt').finalTilesGallery({
                    minTileWidth: 180,
                    margin: 26,
                    gridCellSize: 50,
                    autoLoadURL: '" . $url . "',
                    autoLoadOffset: 500,
                    onUpdate: function() {
                        jQuery('.tile a[rel^=\"gallery[Photo]\"]').prettyPhoto({animation_speed:'normal', slideshow:3000, autoplay_slideshow: false, social_tools:'', gallery_markup:''});
                    }
                });
                jQuery('.tile a[rel^=\"gallery[Photo]\"]').prettyPhoto({animation_speed:'normal', slideshow:3000, autoplay_slideshow: false, social_tools:'', gallery_markup:''});
            ");
        }

        if ($this->type == 'video' OR $this->viewImage == 'image_2') {
            $cs->registerScriptFile($baseUrl . '/plugins/scrollPaging/js/jquery.tcScrollPaging.js');
            $cs->registerCssFile($baseUrl . '/plugins/scrollPaging/css/style.css');

            $cs->registerScript('PinSearch', "
                jQuery(function(){
                    jQuery('#pin-list').tcScrollPaging({
                        heightOffset: 100,
                        pageOffset: " . $offset . ",
                        scrollFinishMsg: 'Đã load xong',
                        scrollUrl: '.pin-url',
                        scrollItem: '.pin-item',
                        scrollItems: '.pin-items',
                        scrollLoading: '.pin-load',
                        scrollOffset: '.pin-offset',
                        scrollFinish: '.pin-finish',
                    });
                });
            ");
        }

        $this->render($this->view, $assign);
    }

    private function getData($cacheId, $criteria) {
        $cache = Yii::app()->cache->get($cacheId);
        //echo 'RunCache';
        if ($cache === FALSE) {
            //echo 'RunSQL';
            $model = KitPin::model()->findAll($criteria);
            if ($model !== array())
                $model = KitPin::treatment($model);

            Yii::app()->cache->set($cacheId, $model);
            return $model;
        } else return $cache;
    }

}
