<?php

class pinfilter extends Widget {

    public $view = '';
    public $limit = 10;
	public $title = 'Bài mới';
	public $creator = NULL;
	public $category = NULL;
	public $from_time = NULL;
	public $to_time = NULL;
	public $order = 'id DESC';
	public $cache_time = 0;

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
        Yii::import('pin.models.KitPin');
    }

    public function run() {
        $cacheId = md5('pin_filter_'. $this->creator .'_'. $this->category .'_'. $this->from_time .'_'. $this->to_time .'_'. $this->order .'_'. $this->limit);
        $cache = Yii::app()->cache->get($cacheId);
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->condition = '';
            $criteria->addCondition("type != 'text'");
			if (!empty($this->from_time))
				$criteria->addCondition("created_time >= '".$this->from_time." 00:00:00'");
			if (!empty($this->to_time))
                $criteria->addCondition("created_time <= '".$this->to_time."' 23:59:59");
			if (!empty($this->creator))
                $criteria->compare('creator', $this->creator);
			if (!empty($this->category)) {
                $catListObject = KitCategory::getListInParent('pin', $this->category);
                $catList = array($this->category);
                foreach ($catListObject as $cat) {
                    $catList[] = $cat->id;
                }
                $criteria->join = "INNER JOIN {{kit_pin_category}} t2 ON t.id=t2.pin_id";
                $criteria->addInCondition('t2.category_id', $catList);
            }
				
			$criteria->order = $this->order;
			$criteria->limit = $this->limit;           

            $model = KitPin::model()->findAll($criteria);
            if ($model !== array())
                $model = KitPin::treatment($model);
            Yii::app()->cache->set($cacheId, $model, $this->cache_time);
        } else {
            $model = $cache;
        }

        $assign['rows'] = $model;
		
		if (!empty($assign['rows']))
	        $this->render($this->view, $assign);
    }

}
