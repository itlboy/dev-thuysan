<?php

class pinlist extends Widget {

    public $view = '';
    public $limit = 10;
    public $page = 1;
    public $wall = FALSE;
    public $request = FALSE;    // Xác định sẽ lấy URI mặc định (FALSE) hay URI hiện tại

    public $type = NULL;    // image | video
    public $category = NULL;    // ID danh mục
    public $creator = NULL;
    public $keyword = NULL;

    public function init() {
        Yii::import('pin.models.KitPin');

        if ($this->view == '')
            $this->view = __CLASS__;

//        $this->options['creator'] = ($this->options['creator'] != NULL) ? intval($this->options['creator']) : NULL;
    }

    /**
     * Chỉ set cache khi offset = 0 (trang 1)
     * pin_list_{category}_{creator}
     * pin_list_10_                     CAT = 10        CREATOR = NULL
     * pin_list__1                      CAT = NULL      CREATOR = 1
     */
    public function run() {
        $offset = 0;
        $criteria = new CDbCriteria;
        $criteria->select = Common::getFieldInTable(KitPin::model()->getAttributes(), 't.');
        $criteria->condition = 't.status = 1';

        $cacheCreator = NULL;
        if ($this->creator !== NULL) {
            $cacheCreator = (int) $this->creator;
            $criteria->addCondition ('creator = ' . $cacheCreator);
        }

        if ($this->keyword != NULL OR $this->keyword != '') {
            $keyword = addcslashes($this->keyword, '%_');
            $criteria->addCondition('title LIKE :keyword OR tags LIKE :keyword OR intro LIKE :keyword');
            $criteria->params[':keyword'] = "%$keyword%";
        }

        // Type
        if ($this->type != NULL AND in_array($this->type, array('image', 'video'))) {
            $criteria->compare('t.type', $this->type);
        }

        // Category
        $cacheCategory = NULL;
        if ($this->category !== NULL AND (int) $this->category > 0) {
            $cacheCategory = $category = (int) $this->category;
            $catListObject = KitCategory::getListInParent('pin', $category);
            $catList = array($category);
            foreach ($catListObject as $cat) {
                $catList[] = $cat->id;
            }
            $criteria->join = "INNER JOIN {{kit_pin_category}} t2 ON t.id=t2.pin_id";
            $criteria->addCondition('t2.category_id IN ('.implode(',', $catList).')');
        }

        $total = KitPin::model()->count($criteria);

        if (isset($_POST['offset']))
            $offset = (int) $_POST['offset'] + $this->limit;
        $criteria->offset = $offset;

        $criteria->limit = $this->limit;
        $criteria->order = 't.id DESC';

        $assign['url'] = ($this->request == FALSE) ? Yii::app()->createUrl('//pin/pinlist/index') : Yii::app()->request->requestUri;
        $assign['wall'] = $this->wall;
        $assign['offset'] = $offset;
        $assign['total'] = $total;

        $cacheId = NULL;
        if ($offset == 0) {
            if ($this->type == NULL) {
                if ($this->keyword == NULL AND $cacheCreator != NULL AND $cacheCategory == NULL) {
                    $cacheId = md5('pin_list__' . $cacheCreator);
                } elseif ($this->keyword == NULL AND $cacheCreator == NULL AND $cacheCategory != NULL) {
                    $cacheId = md5('pin_list_' . $cacheCategory . '_');
                } elseif ($this->keyword == NULL) {
                    $cacheId = md5('pin_list_');
                }
            } else {
                if ($this->keyword == NULL AND $cacheCreator != NULL AND $cacheCategory == NULL) {
                    $cacheId = md5('pin_list_' . $this->type . '_' . $cacheCreator);
                } elseif ($this->keyword == NULL AND $cacheCreator == NULL AND $cacheCategory != NULL) {
                    $cacheId = md5('pin_list_' . $this->type . '' . $cacheCategory . '_');
                } elseif ($this->keyword == NULL) {
                    $cacheId = md5('pin_list_' . $this->type);
                }
            }
        }

        $model = ($cacheId != NULL)
            ? $this->getData($cacheId, $criteria)
            : KitPin::model()->findAll($criteria);

        $assign['rows'] = $model;
        if ($assign['rows'] !== array())
            $assign['rows'] = KitPin::treatment($assign['rows']);

        $baseUrl = Yii::app()->theme->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        $cs->registerScriptFile($baseUrl . '/plugins/scrollPaging/js/jquery.tcScrollPaging.js');
        $cs->registerCssFile($baseUrl . '/plugins/scrollPaging/css/style.css');

        $cs->registerScript('PinSearch', "
            jQuery(function(){
                jQuery('#pin-list').tcScrollPaging({
                    heightOffset: 100,
                    pageOffset: " . $offset . ",
                    scrollFinishMsg: 'Đã load xong',
                    scrollUrl: '.pin-url',
                    scrollItem: '.pin-item',
                    scrollItems: '.pin-items',
                    scrollLoading: '.pin-load',
                    scrollOffset: '.pin-offset',
                    scrollFinish: '.pin-finish',
                });
            });
        ");
        $this->render($this->view, $assign);
    }

    private function getData($cacheId, $criteria) {
        $cache = Yii::app()->cache->get($cacheId);
        //echo 'RunCache';
        if ($cache === FALSE) {
            //echo 'RunSQL';
            $model = KitPin::model()->findAll($criteria);
            if ($model !== array())
                $model = KitPin::treatment($model);

            Yii::app()->cache->set($cacheId, $model);
            return $model;
        } else return $cache;
    }

}
