<?php

//Cần khai báo thêm $view để sử dụng các template khác theo param danh mục
class lastestByCat extends Widget {

    public $category = 0;
//    public $parent_id = '';
    public $limit = 12;
    public $view = '';
    public $href = '';
    public function init() {
        if ($this->view == '') $this->view = __CLASS__;
        Yii::import('pin.models.KitPin');
        Yii::import('category.models.KitCategory');
    }

    public function run() {
        // Get category
        $data['cat'] = KitCategory::getListInParent('pin', $this->category);
        if (empty($data['cat']))
            return FALSE;

        $data['cat'] = KitCategory::treatment($data['cat']);

        foreach ($data['cat'] as $key => $cat) {
            // Get sub category
            $data['subcat'][$cat['id']] = KitCategory::getListInParent('pin', $cat['id']);
            $data['subcat'][$cat['id']] = KitCategory::treatment($data['subcat'][$cat['id']]);
        }

        foreach ($data['cat'] as $key => $cat) {
            // Get Pin
            $data['pin'][$cat['id']] = array();
            $pinList = KitPin::getLastest($cat['id']);
            if (!empty($pinList)) {
                $data['pin'][$cat['id']] = KitPin::treatment($pinList);
                $data['pin'][$cat['id']] = array_slice($data['pin'][$cat['id']], 0, $this->limit);
            } else
                unset($data['cat'][$key]);
        }
        $this->render($this->view, array(
            'data' => $data,
        ));
    }

}