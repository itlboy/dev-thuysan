        <div class="box_s1 clearfix border_red bg_white maggin_bottom_10">
            <div class="clearfix">
                <div class="grid_2 alpha title" style="padding-left:0;"><?php echo $this->title; ?></div>
            </div>
            <div class="mainContent">
                <!-- Video list -->
                <div class="grid_8 alpha omega list_video_s1">
                    <ul class="list">
                        <?php foreach($data as $key => $item): ?>
                            <?php if ($item["type"] == 'image'): ?>
                            <li class="item clearfix<?php if ($key % 3 == 2): ?> last<?php endif; ?>" style="width: 85px;">
                                <div class="thumb mouseover relative">
                                    <a title="" href="<?php echo $item["url"] ?>">
                                        
                                        <img alt="" src="<?php echo Common::getImageUploaded('pin/medium/' . $item['image']); ?>" width="130" height="130" />
                                    </a>
                                </div>
                                <div class="item_txtbox">
                                    <h6>
                                        <a class="item_title" title="" href="<?php echo $item["url"] ?>"><?php echo $item["title"] ?></a>
                                    </h6>
                                    <p>
                                        <span class="icon_play"><s title="">&nbsp;</s><em><?php echo number_format($item['view_count'],0,'.',','); ?></em></span>
                                    </p>
                                </div>
                            </li>
                            <?php elseif ($item["type"] == 'video'): ?>
                            <li class="item clearfix<?php if ($key % 2 == 1): ?> last<?php endif; ?>" style="width: 142px;">
                                <div class="thumb mouseover relative">
                                    <a title="" href="<?php echo $item["url"] ?>">
                                        <?php
                                        Yii::import('application.vendors.libs.grabVideo');
                                        $youtubeId = grabVideo::getYoutubeId($item['video']);
                                        ?>
                                        <img alt="<?php echo $item['title']; ?>" src="http://img.youtube.com/vi/<?php echo $youtubeId; ?>/1.jpg" width="130" height="95" />
                                    </a>
                                </div>
                                <div class="item_txtbox">
                                    <h6>
                                        <a class="item_title" title="" href="<?php echo $item["url"] ?>"><?php echo $item["title"] ?></a>
                                    </h6>
                                    <p>
                                        <span class="icon_play"><s title="">&nbsp;</s><em><?php echo number_format($item['view_count'],0,'.',','); ?></em></span>
                                    </p>
                                </div>
                            </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <!-- END Video list -->
            </div>
        </div>
