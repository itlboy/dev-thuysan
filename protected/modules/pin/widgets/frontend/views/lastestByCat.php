<?php foreach ($data['cat'] as $key => $cat): ?>
<?php
if (in_array( (int) $cat['id'], array(87))) $style = 'image';
elseif (in_array( (int) $cat['id'], array(88))) $style = 'video';
elseif (in_array( (int) $cat['id'], array(89))) $style = 'story';
else $style = 'image';
?>

<div class="grid_24 padding_bottom maggin_top_10">
    <div class="box_s1 clearfix border_red bg_white">
        <div class="clearfix">
            <h3 class="grid_2 alpha title"><a href="<?php echo $cat['url']; ?>" title="<?php echo $cat['name']; ?>"><?php echo $cat['name']; ?></a></h3>
            <?php if (!empty($data['subcat'][$cat['id']])): ?>
            <div class="grid_16 omega box_link_extra fr clearfix" style="text-align: right;">
                <?php foreach ($data['subcat'][$cat['id']] as $key => $subcat): ?>
                <h3 class="fr"><a href="<?php echo $subcat['url']; ?>" title="<?php echo $subcat['name']; ?>"><?php echo $subcat['name']; ?></a></h3><span class="fr">|</span>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>
        </div>
        <div class="mainContent">
            <!-- Video list -->
            <?php echo $this->render('lastestByCat_' . $style, array('data' => $data, 'cat' => $cat)); ?>
            <!-- END Video list -->
        </div>
    </div>
</div>
<?php endforeach; ?>
