<div class="pin">
    
    <?php                    
    Yii::import("ext.xupload.models.XUploadForm");
    $uploadModel = new XUploadForm;
    $this->widget('ext.xupload.XUploadWidget', array(
        'url' => Yii::app()->createUrl('cms/upload/upload', array('module' => 'pin')),
        'model' => $uploadModel,
        'attribute' => 'file',
        'multiple' => false,
        'options' => array(
            'beforeSend' => 'js:function (event, files, index, xhr, handler, callBack) {
                $("#UploadImageStatus").val(1);
                callBack();
            }',
            'onComplete' => 'js:function (event, files, index, xhr, handler, callBack) {
                if (handler.response.error !== 0) alert(handler.response.error);
                else {
                    $("#UploadImageStatus").val(2);
                    $("#KitPin_image").val(handler.response.name);
                    $("#ajax_result_image").html();
                    $("#ajax_result_image").html(\'<li><a rel="collection" href="' . Common::getImageUploaded() . '\'+handler.response.name+\'"><img src="' . Common::getImageUploaded() . '\'+handler.response.name+\'?update='.time().'" height="84" width="148"/><span class="name">Ảnh gốc</span></a></li>\');
                }
            }',
        )
    ));
    ?>
    <form method="post" style="margin-top: 10px;" onsubmit="return pinValid();">
        <?php echo CHtml::hiddenField('UploadImageStatus', 0); ?>
        <?php echo CHtml::activeHiddenField($model, 'image', array('value' => '')); ?>
        <div><?php echo CHtml::activeTextField($model, 'title', array('style' => 'width: 98%; margin-bottom: 5px', 'placeholder' => 'Tiêu đề')); ?></div>        
        <div id="contentLong" style="display:none; margin-bottom: 5px"><?php echo CHtml::activeTextArea($model, 'content', array('style' => 'width: 98%; margin-bottom: 5px', 'placeholder' => 'Nội dung')); ?></div>
        <div><?php echo CHtml::activeTextField($model, 'video', array('style' => 'width: 98%; margin-bottom: 5px', 'placeholder' => 'Đường link youtube (Nếu có)')); ?></div>
        <div id="contentShort"><a href="javascript:;">Nhấn vào đây nếu bài viết của bạn quá dài</a></div>
        <div class="pin_end">
            <!-- <p class="stats colorless">
                <span>120 ký tự</span>
            </p>-->
            <p class="description">
                <?php
                echo $model->getError('categories');
                echo CHtml::activeCheckBoxList($model, 'categories', CHtml::listData(KitCategory::model()->getCategoryOptions('pin'), 'id', 'name'), array(
                    'separator' => '',
                    'template' => '<div>{input} {label}</div>'));
                ?>
            </p>
        </div>
        <div class="pin_end" style="text-align: center;">
            <button class="Button WhiteButton Button18 userform_submit editpage_submit" type="submit">Đăng</button>
        </div>
    </form>
</div>

<script>
// 1. Ảnh có đang được upload hay không
// 2. Có tiêu đề hay chưa
// 3. Tiêu đề phải lớn hơn 10 ký tự
// 4. Phải chọn danh mục rồi
// nếu thông qua 4 cái tiêu chí đó mới cho up
// e làm đc ko?
    function pinValid() {
        $error = [];
        $valid = false;           
        $inputTitle = jQuery('#KitPin_title').val();
        if (jQuery("#UploadImageStatus").val() == 1) {
            $error.push('Đang upload ảnh. Vui lòng chờ');
        }
        if ($inputTitle.length == 0) {
            $error.push('Vui lòng nhập tiêu đề')
        } else if ($inputTitle.length <= 10) {
            $error.push('Tiêu đề không nhỏ hơn 10 ký tự');
        }
        if (jQuery('[name="KitPin[categories][]"]:checkbox:checked').length == 0) {
            $error.push('Vui lòng chọn danh mục');
        }
        
        if ($error.length !== 0) {
            $errorText = '';
            jQuery.each($error, function(index, value) {
                $errorText += '- ' + value + "\n";                
            });
            alert($errorText);
        } else {
            $valid = true;
        }
        
        return $valid;
    }
</script>
