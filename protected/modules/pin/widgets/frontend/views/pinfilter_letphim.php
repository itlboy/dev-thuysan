            <div class="list_s2 clearfix">
                <?php foreach ($rows as $row): ?>
                <?php $row['url'] .= '?utm_source=phim.let.vn&utm_medium=widget&utm_campaign=vailua'; ?>
                <div class="item fl">
                    <?php if ($row['type'] == 'video'): ?>
                    <?php Yii::import('application.vendors.libs.grabVideo'); ?>
                    <div class="thumb">
                        <a href="<?php echo $row['url']; ?>" target="_blank" title="<?php echo $row['title']; ?>">
                            <img src="http://img.youtube.com/vi/<?php echo grabVideo::getYoutubeId($row['video']); ?>/1.jpg" alt="<?php echo $row['title']; ?>">
                        </a>
                    </div>
                    <?php elseif ($row['type'] == 'image'): ?>
                    <div class="thumb">
                        <a href="<?php echo $row['url']; ?>" target="_blank" title="<?php echo $row['title']; ?>">
                            <img src="<?php echo Common::getImageUploaded('pin/medium/' . $row['image']); ?>" alt="<?php echo $row['title']; ?>">
                        </a>
                    </div>
                    <?php endif; ?>
                    <div class="title"><a href="<?php echo $row['url']; ?>" target="_blank" title="<?php echo $row['title']; ?>"><?php echo letText::limit_chars($row['title'], 25); ?></a></div>
                </div>
                <?php endforeach; ?>
            </div>

