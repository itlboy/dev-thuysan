    <div class="grid_16 alpha omega maggin_bottom_15">
        <div class="box_s1 clearfix border_red" style="background: #F6F6F6;">
            <div class="clearfix">
                <div class="grid_15 alpha title"><?php echo $this->title; ?></div>
            </div>
            <div class="mainContent">
                <!-- Video list -->
                <div class="grid_16 alpha omega list_video_s1">
                    <ul class="list maggin_left_20">
                    <?php foreach($data as $key => $item): ?>
                        <li class="item clearfix<?php if ($key % 4 == 3): ?> last<?php endif; ?>">
                            <div class="thumb mouseover">
                                <a title="" href="<?php echo $item["url"] ?>">
                                    <?php if ($item["type"] == 'image'): ?>
                                    <img alt="" src="<?php echo Common::getImageUploaded('pin/medium/' . $item['image']); ?>" width="130" height="130" />
                                    <?php elseif ($item["type"] == 'video'): ?>
                                    <?php
                                    Yii::import('application.vendors.libs.grabVideo');
                                    $youtubeId = grabVideo::getYoutubeId($item['video']);
                                    ?>
                                    <img alt="<?php echo $item['title']; ?>" src="http://img.youtube.com/vi/<?php echo $youtubeId; ?>/1.jpg" width="130" height="95" />
                                    <?php endif; ?>
                                </a>
                            </div>
                            <div class="item_txtbox">
                                <h6>
                                    <a class="item_title" title="" href="<?php echo $item["url"] ?>"><?php echo $item["title"] ?></a>
                                </h6>
                                <p>
                                    <span class="icon_play"><s title="">&nbsp;</s><em><?php echo number_format($item['view_count'],0,'.',','); ?></em></span>
                                </p>
                            </div>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                </div>
                <!-- END Video list -->
            </div>
        </div>
    </div>
