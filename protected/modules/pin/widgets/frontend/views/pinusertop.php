<table border="0" cellspacing="1" cellpadding="0" class="maggin_bottom_10">
    <tbody>
        <tr>
            <td class="column1" width="1%" nowrap><strong>Xếp hạng</strong></td>
            <td>
                <strong>Thành viên</strong>
            </td>
            <td>
                <strong>Điểm</strong>
            </td>
        </tr>
        <?php foreach ($model as $key => $data) : ?>
        <tr>
            <td class="column1" width="1%" nowrap>Top <?php echo $key + 1; ?></td>
            <td>
                <a href="<?php echo Yii::app()->createUrl('//wall-'.$data->id); ?>"><?php echo $data->username; ?></a>
            </td>
            <td>
                <?php echo $data->rank_point; ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
