<div class="pin">
    <div class="title_s2" style="margin-top: -5px;"><?php echo $this->title; ?></div>
    <div class="comments colormuted">
        <?php foreach ($rows as $row): ?>      

            <div class="comment convo clearfix">
				<?php if ($row['type'] == 'video'): ?>
                <?php Yii::import('application.vendors.libs.grabVideo'); ?>
                <a href="<?php echo $row['url']; ?>" class="ImgLink" target="_blank" title="<?php echo $row['title']; ?>">
                    <img src="http://img.youtube.com/vi/<?php echo grabVideo::getYoutubeId($row['video']); ?>/1.jpg" class="profile user_image" alt="<?php echo $row['title']; ?>">
                </a>
				<?php elseif ($row['type'] == 'image'): ?>
                <a href="<?php echo $row['url']; ?>" class="ImgLink" target="_blank" title="<?php echo $row['title']; ?>">
                    <img src="<?php echo Common::getImageUploaded('pin/small/' . $row['image']); ?>" class="profile user_image" alt="<?php echo $row['title']; ?>">
                </a>
                <?php endif; ?>
                
                <p><a href="<?php echo $row['url']; ?>" class="title_s1"><?php echo $row['title']; ?></a></p>
                <p class="stats colorless">
                    <span class="LikesCount">
                    <?php echo $row['like_count'];?> likes </span>
                    <span class="CommentsCount">
                    <?php echo $row['comment_count'];?> comments </span>
                    <span class="RepinsCount">
                    <?php echo $row['share_count'];?> shared </span>
                </p>
            </div>

        <?php endforeach; ?>
    </div>
</div>
