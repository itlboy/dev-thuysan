<div id="pin-list">
    <div class="pin-items">
        <?php foreach ($rows as $row): ?>            
            <div class="pin pin-item">
                
                <!-- TEST LINK Update/Delete -->
                <?php if ($wall == TRUE AND $row['creator'] == Yii::app()->user->id) : ?>
                <!--
                <a href="<?php echo Yii::app()->createUrl('//pin/manage/update', array('id' => $row['id'])); ?>">Update</a>
                <a href="<?php echo Yii::app()->createUrl('//pin/manage/delete', array('id' => $row['id'])); ?>">Delete</a>
                -->
                <?php endif; ?>
            
                <div class="title_box clearfix">
                	<div class="fl" style="width:500px;">
                        <div class="title_s2"><a href="<?php echo $row['url']; ?>" target="_blank"><?php echo $row['title']; ?></a></div>
                        <div class="fb-like" data-href="<?php echo $row['url'];?>" data-send="true" data-layout="button_count" data-width="620" data-show-faces="true"></div>
                    </div>
                    <div class="fr" style="width:100px;"></div>
                </div>
                <div style="margin-top: 10px;">
                    <p class="description"><?php echo $row['intro']; ?></p>
                    <p class="stats colorless">
                        <span><?php echo $row['like_count'];?> likes </span>
                        <span><?php echo $row['comment_count'];?> comments </span>
                        <span><?php echo $row['share_count'];?> shared </span>
                        <span><?php echo $row['view_count'];?> viewed </span>
                        <span>Đăng vào <?php echo letDate::fuzzy_span(strtotime($row['created_time'])); ?> bởi <a href="<?php echo Yii::app()->createUrl('//wall-'.$row['creator']); ?>"><?php echo KitAccount::getField($row['creator'] ,'username'); ?></a></span>
                    </p>
                </div>
                <div style="margin: 10px 0; font-size: 13px;"><?php echo $row['content']; ?></div>
                <div class="PinHolder pin_end">
                    <?php if ($row['type'] == 'video'): ?>
                    <?php
                    Yii::import('application.vendors.libs.grabVideo');
                    echo grabVideo::youtubePlayer($row['video'], 620, 400);
                    ?>
                    <?php elseif ($row['type'] == 'image'): ?>
                    <a href="<?php echo $row['url']; ?>" class="PinImage ImgLink" target="_blank">
                    <img src="<?php echo Common::getImageUploaded('pin/large/' . $row['image']); ?>" alt="<?php echo $row['title']; ?>" class="PinImageImg" />
                    </a>
                    <?php else: ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
        <a href="javascript:;" style="display:none" class="pin-url" rel="<?php echo $url; ?>">URL</a>
        <a href="javascript:;" style="display:none" class="pin-offset" rel="<?php echo $offset; ?>">OFFSET</a>
    </div> 
    
    <div class="pin-load"></div>
    <div class="pin-finish"></div>
</div>

