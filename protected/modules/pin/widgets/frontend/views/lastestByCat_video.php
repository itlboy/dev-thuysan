            <div class="grid_24 alpha omega list_video_s1">
                <ul class="list maggin_left_20">
                    <?php foreach ($data['pin'][$cat['id']] as $key => $item): ?>
                        <li class="item clearfix<?php if ($key % 6 == 5): ?> last<?php endif; ?>">
                            <div class="thumb mouseover">
                                <a title="<?php echo $item['title']; ?>" href="<?php echo $item['url']; ?>">
                                    <?php
                                    Yii::import('application.vendors.libs.grabVideo');
                                    $youtubeId = grabVideo::getYoutubeId($item['video']);
                                    ?>
                                    <img alt="<?php echo $item['title']; ?>" src="http://img.youtube.com/vi/<?php echo $youtubeId; ?>/1.jpg" width="130" height="95" />
                                </a>
                            </div>
                            <div class="item_txtbox">
                                <h6>
                                    <a class="item_title" title="<?php echo $item['title']; ?>" href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a>
                                </h6>
                                <div>bởi <a href="<?php echo Yii::app()->createUrl('//wall-'.$item['creator']); ?>"><?php echo KitAccount::getField($item['creator'], 'username'); ?></a></div>
                                <p>
                                    <span class="icon_play"><s title="">&nbsp;</s><em><?php echo number_format($item['view_count'], 0, ',', '.'); ?></em></span>
                                    <span class="icon_comment"><s title="">&nbsp;</s><em><?php echo number_format($item['comment_count'], 0, ',', '.'); ?></em></span>
                                </p>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
