<?php

class pinwallall extends Widget {

    public $creator = NULL;
    public $limit = 20;
    public $view = '';

    public function init() {
        Yii::import('pin.models.KitPin');

        if ($this->view == '')
            $this->view = __CLASS__;
    }

    public function run() {
        $criteria = new CDbCriteria;
        $criteria->select = 't.*';
        $criteria->condition = 't.status = 1';

        $cacheCreator = NULL;
        if ($this->creator !== NULL) {
            $cacheCreator = (int) $this->creator;
            $criteria->addCondition ('creator = ' . $cacheCreator);
        }

        $dataProvider = new CActiveDataProvider('KitPin', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => $this->limit,
                'pageVar' => 'page'),
            'sort' => array(
                'defaultOrder' => 't.id DESC'
            ),
        ));

        $this->widget('zii.widgets.CListView', array(
            'dataProvider' => $dataProvider,
            'itemView' => $this->view,
            'pager' => array(
                'cssFile' => false,
                'class' => 'LinkPager',
                'header' => '',
                'firstPageLabel' => 'Về đầu',
                'prevPageLabel' => '&lt;',
                'nextPageLabel' => '&gt;',
                'lastPageLabel' => 'Về cuối',
                'selectedPageCssClass' => 'paging_pn_current',
                'maxButtonCount' => 10,
                'htmlOptions' => array(
                    'class' => 'paging_pn'
                )
            ),
            'template' => '
                <table border="0" cellspacing="1" cellpadding="0" class="episode_list">
                    <thead>
                        <tr>
                            <td>Tên</td>
                            <td width="10%">Lượt Xem</td>
                            <td width="10%">Lượt Like</td>
                            <td width="10%">Lượt Share</td>
                            <td width="10%">Bình luận</td>
                            <td class="column1" width="1%" nowrap>Chức năng</td>
                        </tr>
                    </thead>
                    <tbody>{items}</tbody>
                </table>
                {pager}
            ',
        ));
    }
}
