<?php

class pinpost extends Widget {
    
    public $view = '';

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
        Yii::import('pin.models.KitPin');
        Yii::import('pin.models.KitPinCategory');
    }
    
    public function run() {
        if (!Yii::app()->user->isGuest) {
            $model = new KitPin;        
            if (isset($_POST['KitPin'])) {
                // Upload image
                if ($_POST['KitPin']['image'] !== NULL AND $_POST['KitPin']['image'] !== '') {
                    $_POST['KitPin']['image'] = Common::createThumb('pin' , $_POST['KitPin']['image'], letArray::get($_POST['KitPin'], 'title', ''));

                    // Remove Image
                    Common::removeImage('pin', $model->image);
                } else unset ($_POST['KitPin']['image']);

                $model->attributes = $_POST['KitPin']; 

                if ($model->save()) {
                    KitPinCategory::create($model->categories, $model->id);

                    Yii::app()->controller->redirect(array('//pin/wall'));
                }
            }

            $cs = Yii::app()->clientScript;
            $cs->registerScriptFile('http://assets.let.vn/js/tinymce/tiny_mce.js');
            $cs->registerScriptFile('http://assets.let.vn/js/tinymce/jquery.tinymce.js');
            $cs->registerScript('TinyMCEJs', '
                jQuery(function(){
                    jQuery("#contentShort a").live("click", function(){      
                        tinyMCE.init({
                            mode : "textareas",
                            theme : "advanced",
                            plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
                            theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote",
                            theme_advanced_buttons2 : "undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
                            theme_advanced_buttons3 : "styleselect,formatselect,fontselect,fontsizeselect",
                            theme_advanced_toolbar_location : "top",
                            theme_advanced_toolbar_align : "left",
                            theme_advanced_statusbar_location : "bottom",
                            theme_advanced_resizing : true,
                            width : "100%",
                            height : "250px",
                        });
                        jQuery("#contentShort").hide();
                        jQuery("#contentLong").show();
                    });
                })
            ');
  
            $this->render($this->view, array(
                'model' => $model
            ));
        }
    }

}
?>

