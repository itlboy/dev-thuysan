<?php

/**
<?php $this->widget('application.modules.pin.widgets.frontend.pinusertop', array(
    'blacklist' => array(1),
    'limit' => 10
)); ?> 
 */
class pinusertop extends Widget {

    public $blacklist = array();
    public $limit = 10;
    public $view = '';

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
    }

    public function run() {      
        $this->render($this->view, array(
            'model' => $this->getData()
        ));
    }
    
    private function getData() {
        $cacheId = md5(__CLASS__);        
        $cacheData = Yii::app()->cache->get($cacheId);
        if ($cacheData === FALSE) {  
            $criteria = new CDbCriteria;
            $criteria->condition = 'rank > 0';
            $criteria->order = 'rank ASC';
            $criteria->limit = $this->limit;

            if (!empty($this->blacklist)) {
                $criteria->addNotInCondition('id', $this->blacklist);
            }

            $model = KitAccount::model()->findAll($criteria);
            Yii::app()->cache->set($cacheId, $model, 60*60*24);
            return $model;
        } else {
            return $cacheData;
        }
    }
}
