<?php

/**
 * Module: article
 * Auth:
 * Date: 2012-03-16 11:24:06
 */
class TagController extends ControllerFrontend {

    public function actionIndex() {
        $assign = $this->_getParam();
        $link = 'tim-kiem/'.str_replace(' ','_',$assign['options']['keyword']).'/';
        $limit = 10;
        $page = letArray::get($_GET, 'page', NULL);
        if(!empty($page)){
            $limit_wg = $limit.','.$page * $limit;
            $find['old'] = $link.'trang-'.($page +1);
            if(($page - 1) != 0){
                $find['new'] = $link.'trang-'.($page -1);
            } else {
                $find['new'] = $link;
            }
            $link .='trang-'.$page;
        } else {
            $limit_wg = $limit;
            $find['old'] = $link.'trang-'.($page +1);
        }
        $find['limit_wg'] = $limit_wg;

        $this->render('index', array(
            'data' => $assign,
            'find' => $find,
        ));
    }
    public function _getParam()
    {
        $assign['options'] = isset($_GET) ? $_GET : array();
        if (isset($assign['options']['keyword']))
            $assign['options']['keyword'] = str_ireplace('_', ' ', $assign['options']['keyword']);
        return $assign;
    }
}