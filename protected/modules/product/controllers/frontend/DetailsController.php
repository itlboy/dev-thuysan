<?php
    Class DetailsController extends ControllerFrontend {

        public function actionIndex(){

            $product_id = Yii::app()->request->getQuery('id');
            $data = KitProduct::getDetails($product_id);
            if(empty($data))
                $this->redirect(Yii::app()->getBaseUrl(true));
            $category = $data->KitProductCategory;
            $category = CJSON::decode(CJSON::encode($category));
            if(!empty($data) AND ($data->status == 1) AND ($data->trash == 0)){
                $data = KitProduct::treatment($data);
            }else{
                $this->redirect(Yii::app()->getBaseUrl(true));
            }
            $this->render('index',array(
                'data' => $data,
                'category' => $category
            ));
        }
    }

?>