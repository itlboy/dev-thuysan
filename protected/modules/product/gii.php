<?php
return array (
  'moduleName' => 'product',
  'tablePrefix' => 'let_',
  'main_tableName' => 'let_kit_product',
  'main_controller' => 'default',
  'main_baseClass' => 'ActiveRecord',
  'hasCategory' => '1',
  'hasOption' => '0',
  'hasComment' => '1',
  'hasMedia' => '1',
  'hasChildren' => '0',
  'children_tableName' => 'let_kit_film_episode',
  'children_controller' => 'episode',
  'children_baseClass' => 'ActiveRecord',
  'children_fieldCreate' => 'title,sorder,link,status',
  'children_fieldUpdate' => 'title,sorder,link,status,intro,hot,promotion,status,trash',
);