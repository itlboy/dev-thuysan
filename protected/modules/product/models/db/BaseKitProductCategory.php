<?php
/**
 * This is the model class for table "kit_product_category".
 *
 * The followings are the available columns in table 'kit_product_category':
 * @property string $id
 * @property string $product_id
 * @property string $category_id
 */

class BaseKitProductCategory extends ActiveRecord {
    var $className = __CLASS__;
    
    public $category_id;
    public $categories;
    
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitProductCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_product_category}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_id, category_id', 'required'),
			array('product_id, category_id', 'length', 'max'=>10),
            array('categories', 'safe'),
			// The following rule is used by search(). Please remove those attributes that should not be searched.
			array('id, product_id, category_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related class name for the relations automatically generated below.
		return array(
            'KitProductCategoryCategory' => array(KitProductCategory::MANY_MANY, 'KitCategory',
                '{{kit_product_category}}(product_id, category_id)'),
            'creatorUser' => array(KitProductCategory::BELONGS_TO, 'KitAccount', 'creator', 'select' => 'username'),
            'editorUser' => array(KitProductCategory::BELONGS_TO, 'KitAccount', 'editor', 'select' => 'username'),
            'stats' => array(self::HAS_ONE, 'KitStats', 'item_id',
                'condition' => 'module = "product"'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;
        $criteria->select = Common::getFieldInTable(KitProductCategory::model()->getAttributes(), 't.');
        
        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.product_id',$this->product_id,true);
        $criteria->compare('t.category_id',$this->category_id,true);

        if ($this->category_id !== NULL) {
            $categories = array();
            $categories[] = $this->category_id;
            $category = KitCategory::model()->findByPk($this->category_id);
            $descendants = $category->descendants()->findAll();
            foreach ($descendants as $cat) {
                $categories[] = $cat->id;
            }
            $criteria->addInCondition('ic.category_id', $categories);
        }
        
        $criteria->join = 'LEFT JOIN {{kit_product_category}} ic ON ic.product_id = t.id';
        
        $criteria->group = 't.id';
        
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
            'sort' => array(
                'defaultOrder' => 'id DESC',
            ),
		));
	}
    
    protected function beforeValidate() {
        if ($this->isNewRecord) {
        }
        return parent::beforeValidate();
    }
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitProductCategory::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitProductCategory::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitProductCategory::getLastest_'));
        Yii::app()->cache->delete(md5('KitProductCategory::getPromotion_'));
    }
    
    public function parseCategory($data, $delimiter = '') {
        $arr = array();
        foreach ($data as $value) {
            $arr[] = $value->name;                                     
        }
        echo implode($arr, $delimiter);    
    }
                

    ///////////////////////////////// Frontend /////////////////////////////////
    /**
     * treatment
     * @param mixed $data
     * @return array 
     */
    public static function treatment($data)
    {
        $data = CJSON::decode ((CJSON::encode($data))); // Neu la object thi chuyen thanh array
        if (!isset ($data[0]))
            $data = KitProductCategory::treatmentRow($data);
        else {
            foreach ($data as $key => $value) {
                $data[$key] = KitProductCategory::treatmentRow($value);
            }
        }
        return $data;
    }
    
    public static function treatmentRow($row)
    {
        $urlTitle = Common::convertStringToUrl($row['title']);
        $row['url'] = Yii::app()->createUrl($urlTitle.'-product-'.$row['id']);
        return $row;
    }
    
    public static function countRecord($from_time = NULL, $to_time = NULL) {
        if ($from_time == NULL) $from_time = '0000-00-00 00:00:00';
        if ($to_time == NULL) $to_time = date('Y-m-d H:i:s');
        
        $result = KitProductCategory::model()->count("created_time BETWEEN '".$from_time."' AND '".$to_time."'");
        return $result;
    }

    
    /**
     * Get Details
     * @param int $id
     * @return object 
     */
    public static function getDetails($id) {
        $id = intval($id);
        $cache_name = md5(__METHOD__ . '_' . $id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $result = KitProductCategory::model()->findByPk($id, array(
                'select' => Common::getFieldInTable(KitProductCategory::model()->getAttributes(), 't.'),
                'with' => array('creatorUser', 'editorUser')
            ));
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
    
    public static function getField($id, $field){
        $row = KitProductCategory::getDetails($id);
        $row = KitProductCategory::treatment($row);
        return letArray::get($row, $field, '');
    }

    public static function getLastest ($category = NULL, $limit = 20){
        $cache_name = md5(__METHOD__ . '_' . $category);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(KitProductCategory::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');


            if ($category !== NULL AND $category >= 0) {
                $catListObject = KitCategory::getListInParent('film', $category);
                $catList = array($category);
                foreach ($catListObject as $cat) {
                    $catList[] = $cat->id;
                }
                $criteria->join = "INNER JOIN {{kit_film_category}} t2 ON t.id=t2.film_id";
//                $criteria->group = "t.id";
                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', $catList).')');
            }

            $criteria->order = 't.id DESC';
            $criteria->limit = $limit;
            $result = KitProductCategory::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
    
    public static function getPromotion ($category = NULL, $limit = 20){
        $cache_name = md5(__METHOD__ . '_' . $category);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(KitProductCategory::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
            $criteria->condition = Common::addWhere($criteria->condition, 't.promotion = 1');

            if ($category !== NULL AND $category >= 0) {
                $criteria->join = "INNER JOIN {{kit_film_category}} t2 ON t.id=t2.film_id";
                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', KitCategory::getListInParent('film', $category)).')');
            }
            $criteria->order = 't.id DESC';
            $criteria->limit = $limit;
            $result = KitProductCategory::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

}
