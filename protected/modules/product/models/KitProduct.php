<?php
Yii::import('application.modules.product.models.db.BaseKitProduct');
class KitProduct extends BaseKitProduct{
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    public function attributeLabels() {
        return array_merge(parent::attributeLabels(), array(
            'title' => 'Tiêu đề',
            'status' => 'Kích hoạt',
            'trash' => 'Thùng rác',
            'image' => 'Ảnh đại diện',
            'intro' => 'Giới thiệu',
            'color' => 'màu sắc',
            'product_year' => 'Năm sản xuất',
            'content' => 'Nội dung',
            'from_time' => 'Thời gian bắt đầu',
            'to_time' => 'Thời gian kết thúc',
            'promotion' => 'Tiêu điểm',
            'sorder' => 'Thứ tự',
            'tags' => 'Thẻ',
            'focus' => 'Tin hot',
            'author' => 'Tác giả',
            'price' => 'Giá hiện tại',
            'price_old' => 'Giá cũ',
            'quantity' => 'Số lượng',
            'categories' => 'Danh mục'
        ));
    }

}