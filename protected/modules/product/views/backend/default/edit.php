<?php
$moduleName = 'product';
$modelName = 'KitProduct';
$formID = 'kit_'.$moduleName.'_form';
$gridID = 'kit_'.$moduleName.'_grid';

$this->breadcrumbs = array(
    Yii::t('backend', $moduleName) => $this->createUrl('/'.$moduleName.'/default/index'),
    Yii::t('global', 'Edit') . ' ' . Yii::t('backend', $moduleName),
);
?>

<div class="flat_area grid_16">
    <h2><?php echo (!$model->isNewRecord) ? ((isset($model->title) AND $model->title !== NULL) ? $model->title : Yii::t('global', 'Update') . ' ' . Yii::t('backend', $moduleName)) : Yii::t('global', 'Create') . ' ' . Yii::t('backend', $moduleName); ?></h2>
</div>

<div class="grid_16">
    <button class="green small" onclick="js:jQuery('#apply').val(0);jQuery('#<?php echo $modelName;  ?>_content').elrte('updateSource'); document.forms['<?php echo $formID; ?>'].submit();">
        <div class="ui-icon ui-icon-check"></div>
        <span><?php echo Yii::t('global', 'Save'); ?></span>
    </button>
    <button class="green small" onclick="js:jQuery('#apply').val(1);jQuery('#<?php echo $modelName;  ?>_content').elrte('updateSource'); document.forms['<?php echo $formID; ?>'].submit();">
        <div class="ui-icon ui-icon-check"></div>
        <span><?php echo Yii::t('global', 'Apply'); ?></span>
    </button>
    <?php if (!$model->isNewRecord): ?>
    <button class="blue small" onclick="window.location = '<?php echo Yii::app()->createUrl($moduleName . '/' . $this->id . '/info', array('id' => $model->id)); ?>'">
        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icons/small/white/chart_8.png" />
        <span><?php echo Yii::t('global', 'View Info and stats'); ?></span>
    </button>
    <button class="red small">
        <div class="ui-icon ui-icon-trash"></div>
        <span><?php echo Yii::t('global', 'Trash'); ?></span>
    </button>
    <?php endif; ?>
</div>

<!-- Image -->
<div class="box grid_16">
    <div class="toggle_container">
        <div class="block">
            <h2 class="section"><?php echo Yii::t('global', 'image'); ?></h2>
            <fieldset class="label_side">
                <label><span>Chọn hoặc kéo thả ảnh để upload</span></label>
                <div>
                    <?php                    
                    Yii::import("ext.xupload.models.XUploadForm");
                    $uploadModel = new XUploadForm;
                    $this->widget('ext.xupload.XUploadWidget', array(
                        'url' => Yii::app()->createUrl('cms/upload/upload', array('module' => $moduleName)),
                        'model' => $uploadModel,
                        'attribute' => 'file',
                        'multiple' => false,
                        'options' => array(
                            'onComplete' => 'js:function (event, files, index, xhr, handler, callBack) {
                                if (handler.response.error !== 0) alert(handler.response.error);
                                else {
                                    $("#'.$modelName.'_image").val(handler.response.name);
                                    $("#ajax_result_image").html();
                                    $("#ajax_result_image").html(\'<li><a rel="collection" href="' . Common::getImageUploaded() . '\'+handler.response.name+\'"><img src="' . Common::getImageUploaded() . '\'+handler.response.name+\'?update='.time().'" height="84" width="148"/><span class="name">Ảnh gốc</span></a></li>\');
                                }
                            }',
                        )
                    ));
                    ?>
                    <div class="clearfix"></div>
                </div>
            </fieldset>

            <?php if (isset($model->image) AND $model->image !== NULL AND $model->image !== ''): ?>
            <!-- Image list -->
            <div class="columns clearfix">
                <div class="indent gallery fancybox">
                    <ul class="clearfix" id="ajax_result_image">
                        <?php                        
                        $imageKeys = array_keys(Yii::app()->controller->getModule()->image);
                        ?>
                        <?php foreach ($imageKeys as $key): ?>
                        <li>
                            <?php $imageLink = Common::getImageUploaded($this->module->getName() . '/' . $key . '/' .$model->image); ?>
                            <a rel="collection" href="<?php echo $imageLink; ?>">
                                <img src="<?php echo $imageLink; ?>" height="84" width="148"/>
                                <span class="name"><?php echo $key; ?></span>
<!--                                        <span class="size">71234kb</span>-->
                            </a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <!-- END Image list -->
            <?php endif; ?>

        </div>
    </div>
</div>
<!-- END Image -->

<?php if (!$model->isNewRecord) : ?>
<!-- Media -->
<div class="box grid_16">
    <div class="toggle_container">
        <div class="block">
            <h2 class="section">Media</h2>
            <fieldset class="label_side">
                <label><span>Chọn hoặc kéo thả ảnh để upload</span></label>
                <div>
                    <?php                    
                    Yii::import("ext.xupload.models.XUploadForm");
                    $uploadModel = new XUploadForm;
                    $this->widget('ext.xupload.XUploadWidget', array(
                        'url' => Yii::app()->createUrl('cms/upload/upload', array('module' => $moduleName, 'attribute' => 'file_media')),
                        'model' => $uploadModel,
                        'attribute' => 'file_media',
                        'multiple' => true,
                        'options' => array(
                            'onComplete' => 'js:function (event, files, index, xhr, handler, callBack) {
                                if (handler.response.error !== 0) alert(handler.response.error);
                                else {

                                    jQuery.ajax({
                                        url: "' . Yii::app()->createUrl('media/default/upload') . '",
                                        type: "POST",
                                        data:{
                                            "module": "'.$this->module->id.'",
                                            "itemId": "'.$model->id.'",
                                            "title": "'.$model->title.'",
                                            "media": handler.response.name
                                        },                                        
                                        success: function(data){
                                            jQuery.fn.yiiGridView.update("kit_media_grid");
                                        }
                                    });

                                }
                            }',
                        )
                    ));
                    ?>
                    <div class="clearfix"></div>
                </div>
            </fieldset>

        </div>
    </div>
</div>
<!-- END Media -->
<?php endif; ?>

<?php
$form=$this->beginWidget('CActiveForm', array(
    'id' => $formID,
    'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype'=>'multipart/form-data'),
));
?>
<?php echo $this->renderPartial('//cms/default/_errorSummary', array('error' => $form->errorSummary($model))); ?>

<?php echo CHtml::hiddenField('apply', 0); ?>
<?php echo $form->hiddenField($model, 'image', array('value' => '')); ?>

<div class="box grid_16 tabs">
    <ul class="tab_header clearfix">
        <li><a href="#tab_general"><?php echo Yii::t('global', 'General'); ?></a></li>
        <?php if (!$model->isNewRecord) : ?>
                <li><a href="#tab_media"><?php echo Yii::t('global', 'Media'); ?></a></li>
                        <li><a href="#tab_comment"><?php echo Yii::t('global', 'Comment'); ?></a></li>
                    <?php endif; ?>
                <li><a href="#tab_seo"><?php echo Yii::t('global', 'SEO'); ?></a></li>
        </ul>
    <a href="#" class="grabber"></a>
    <a href="#" class="toggle"></a>
    <div class="toggle_container">
        <div id="tab_general" class="block">
            <?php echo $this->renderPartial('_formContent', array('form' => $form, 'model' => $model), true); ?>
        </div>
        <?php if (!$model->isNewRecord) : ?>
            
        <div id="tab_media" class="block">
            <?php $this->widget('application.modules.media.widgets.backend.Media', array(
                'module' => $this->module->id,
                'itemId' => $model->id,
            )); ?>
        </div>
                    
        <div id="tab_comment" class="block">
            <?php $this->widget('application.modules.comment.widgets.backend.Comment', array(
                'module' => $this->module->id,
                'itemId' => $model->id,
            )); ?>
        </div>
                    <?php endif; ?>
                <div id="tab_seo" class="block">
            <?php echo $this->renderPartial('_formSEO', array('form' => $form, 'model' => $model), true); ?>
        </div>
    </div>
    </div>
<?php $this->endWidget(); ?>

