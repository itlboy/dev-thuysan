<?php

    class product_other extends Widget{

        public $view = '';
        public $category = NULL;
        public $limit = 10;
        public $title = '';
        public function init(){
            if($this->view == '')
                $this->view = __CLASS__;
            Yii::import('product.models.KitProduct');
        }
        public function run(){

            $data = KitProduct::getLastest($this->category,$this->limit);
            if(!empty($data)){
                $data = KitProduct::treatment($data);
            }else{
                return false;
            }
            $this->render($this->view,array(
                'data' => $data
            ));
        }
    }

?>