<?php
    Class listSlide extends Widget{

        public $limit = 10;
        public $view = '';
        public $category = NULL;

        public function init(){
            if($this->view == '')
                $this->view =__CLASS__;
            Yii::import('product.models.KitProduct');
        }
        public function run(){
            $data = KitProduct::getLastest($this->category,$this->limit);
            if(!empty($data)){
                $data = KitProduct::treatment($data);
            }else{
                return false;
            }
            $this->render($this->view,array(
                'data' => $data,
            ));
        }
    }
?>