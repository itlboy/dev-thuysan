<?php

Class listProduct extends Widget{

    public $category = NULL;
    public $view = '';
    public $title = '';
    public $limit = 10;
    public $url = '';
    public $index = 0;
    public $user_id = NULL;
    public $page = FALSE;

    public function init(){
        if($this->view == '')
            $this->view = __CLASS__;
        Yii::import('product.models.KitProduct');
        Yii::import('category.models.KitCategory');
    }
    public function run(){
        $page = (isset($_GET['page']) AND !empty($_GET['page'])) ? $_GET['page'] : '';
        $cache_name = md5(__METHOD__ . '_' .$this->category.'_'.$this->limit.'_'.$this->user_id.'_'.$page);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE OR TRUE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(KitProduct::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
            if(!empty($this->user_id)){
                $criteria->condition = Common::addWhere($criteria->condition, 't.creator ='.$this->user_id);
            }


            if ($this->category !== NULL AND $this->category > 0) {
                $catListObject = KitCategory::getListInParent('product', $this->category);
                $catList = array($this->category);
                foreach ($catListObject as $cat) {
                    $catList[] = $cat->id;
                }
                $criteria->join = "INNER JOIN {{kit_product_category}} t2 ON t.id=t2.product_id";
                //                $criteria->group = "t.id";
                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', $catList).')');
            }
            // Phan trang
            $count = KitProduct::model()->count($criteria);
            $page = new CPagination();
            $page->setItemCount($count);
            $page->setPageSize($this->limit);
            $page->applyLimit($criteria);
            //          ------------------

            $criteria->order = 't.id DESC';
            $result = KitProduct::model()->findAll($criteria);
            if(empty($result)) {
                '<p style="color: #39B54A;text-align: center;margin: 10px;">Hiện sản phẩm này đã hết</p>';
                return FALSE;
            }
            $result['data'] = KitProduct::treatment($result);
            $result['page'] = $page;
            $result['post'] = new KitProduct();

            Yii::app()->cache->set($cache_name, $result, (60*60)); // Set cache
        } else $result = $cache;

        $this->render($this->view,array(
            'data' => $result['data'],
            'post' => $result['post'],
            'page' => $result['page'],
        ));
//
//        $data = KitProduct::getLastest($this->category);
//
//        if(!empty($data)){
//            $data = KitProduct::treatment($data);
//        }else{
//            echo '<p style="color: #39B54A;text-align: center;margin: 10px;">Hiện sản phẩm này đã hết</p>';
//            return false;
//        }
//
//        $this->render($this->view,array(
//            'data' => $data
//        ));
    }
}

?>