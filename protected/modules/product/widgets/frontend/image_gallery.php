<?php

    Class image_gallery extends Widget {

        public $view = '';
        public $category = NULL;
        public $limit = 10;
        public function init(){

            if($this->view == '')
                $this->view = __CLASS__;
            Yii::import('product.models.Kitproduct');
        }
        public function run(){
            $idcate = $this->category;
            $command = Yii::app()->db->CreateCommand();

            $command->select('t1.image')
                ->from('let_kit_gallery t1')
                ->leftJoin('let_kit_gallery_category t2','t1.id = t2.gallery_id')
                ->leftJoin('let_kit_category t3','t3.id = t2.category_id')
                ->where('t3.id ='.$idcate.' AND t1.status = 1 AND t1.trash = 0')
                ->order('t1.id DESC')
                ->limit($this->limit);
            $data = $command->queryAll();

            $data = CJSON::decode(CJSON::encode($data));
           $this->render($this->view,array(
               'data' => $data
           ));
        }
    }