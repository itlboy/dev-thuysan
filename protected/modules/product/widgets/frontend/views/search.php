<span style="color: #ffffff;">Kết quả tìm kiếm :</span> <strong style="color: #39B54A">" <?php echo $this->options['keyword']; ?> "</strong><span style="color: #ffffff;"> có <strong style="color: #39B54A"><?php echo $total; ?></strong> sản phẩm được tìm thấy</span>
<ul style="margin-top: 10px;">
    <?php if(!empty($rows)): ?>
    <?php foreach ($rows as $key => $item): ?>
        <?php
        if ($item['image']) {
            $image_no1 = Common::getImageUploaded('product/medium/' . $item['image']);
        } else {
            $image_no1 = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
        }

        $day = '';
        switch(date('l', strtotime ($item["created_time"])))
        {
            case 'Monday':
                $day = 'Thứ 2';
                break;
            case 'Tuesday':
                $day = 'Thứ 3';
                break;
            case 'Wednesday':
                $day = 'Thứ 4';
                break;
            case 'Thursday':
                $day = 'Thứ 5';
                break;
            case 'Friday':
                $day = 'Thứ 6';
                break;
            case 'Saturday':
                $day = 'Thứ 7';
                break;
            case 'Sunday':
                $day = 'Chủ nhật';
                break;
        } ?>
        <li>
            <a title="<?php echo $item['title'] ?>" href="<?php echo $item['url'] ?>">
                <img style="float: left;margin: 4px 5px" alt="<?php echo $item['title'] ?>" src="<?php echo $image_no1 ?>" width="150"></a>
            <a style="color:#39B54A;font-size: 12px;font-weight: bold; " title="<?php echo $item['title'] ?>" href="<?php echo $item['url'] ?>">
                <?php echo $item['title'] ?></a>
            <p style="color: #ffffff;"><?php echo $item['intro']; ?></p>
            <div class="clear"></div>
        </li>

        <?php endforeach; ?>
    <?php endif; ?>
</ul>
<!--<div class="view-all" style="padding: 0px; margin: 5px 0px;">-->
<!--    --><?php //if(!empty($this->new)): ?>
<!--    <a title="Mơi hơn" style="background: none;float: left;padding: 0px" href="--><?php //echo Yii::app()->createUrl($this->new); ?><!--"><< Mới hơn</a>-->
<!--    --><?php //endif; ?>
<!--    --><?php //if($total != 0): ?>
<!--    --><?php //if(!empty($this->old)): ?>
<!--        <a title="Cũ hơn" style="background: none;float: right;" href="--><?php //echo Yii::app()->createUrl($this->old); ?><!--">Cũ hơn >></a>-->
<!--        --><?php //endif ?>
<!--    --><?php //endif; ?>
<!--    <div class="clear"></div>-->
<!--</div>-->