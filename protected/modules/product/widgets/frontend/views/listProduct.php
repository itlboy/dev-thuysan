<style type="text/css">
    .tooltip {
        display:none;
        position:absolute;
        border:1px solid #333;
        background-color:#161616;
        border-radius:5px;
        padding:10px;
        color:#fff;
        font-size:12px ;
    }
</style>
<?php if(!empty($data)): ?>
<div id="widget-comment-wrapper">
    <div class="comment-items">
        <table>
            <tr>
                <?php for ($i = 1; $i <= count($data); $i++): ?>
                <td style='width: 156px; height:165px; padding: 0px 4px 4px; margin: 0px;'>
                    <table style="width: 155px; height:150px;background:#cccccc;">
                        <tr>
                            <td style="height:155px; vertical-align: bottom;">
                                <a href="<?php echo $data[$i - 1]['url']; ?>">
                                    <div style="height:155px; width: 100%;">
                                        <img src="<?php echo Common::getImageUploaded('product/medium/'.$data[$i - 1]['image']); ?>" width="100%" class="masterTooltip" title="<?php echo !empty($data[$i - 1]['intro'])?$data[$i - 1]['intro']: 'Đang cập nhật'; ?>" />
                                    </div>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding-left: 10px;">
                                <a href="<?php echo $data[$i - 1]['url']; ?>" style="margin-top: 5px;">
                                    <b><?php echo letText::limit_chars($data[$i - 1]['title'],15); ?></b>
                                </a>
                            </td>
                        </tr>
                    </table>
                </td>
                <?php if ($i % 4 == 0): ?>
                            </tr>
                            <tr>
                            <?php endif; ?>
                <?php endfor; ?>
            </tr>
        </table>
        <?php if($this->page == TRUE): ?>
        <style type="text/css">
            .div_page .page a,.div_page .next a,.div_page .previous a{ color: #ffffff !important;}
            .div_page .selected a{ background: white !important; color: black !important;}
        </style>
        <div style="float: left; padding: 10px 0px; width:100%;margin-top: 5px;" class="div_page">
            <?php $this->widget("CLinkPager",array('pages'=>$page)); ?>
        </div>
        <?php endif; ?>
    </div>
</div>
<?php endif; ?>
<script type="text/javascript">
    $(document).ready(function() {
        // Tooltip only Text
        $('.masterTooltip').hover(function(){
            // Hover over code
            var title = $(this).attr('title');
            $(this).data('tipText', title).removeAttr('title');
            $('<p class="tooltip"></p>')
                .text(title)
                .appendTo('body')
                .fadeIn('slow');
        }, function() {
            // Hover out code
            $(this).attr('title', $(this).data('tipText'));
            $('.tooltip').remove();
        }).mousemove(function(e) {
                var mousex = e.pageX + 20; //Get X coordinates
                var mousey = e.pageY + 10; //Get Y coordinates
                $('.tooltip')
                    .css({ top: mousey, left: mousex })
            });
    });
</script>