<div style="width: 100%; overflow: hidden;" id="scroller">
    <div class="slide-image-bottom">
        <div class="image-left"></div>
        <div class="image-center">
            <div id="left">
                <?php
                    if(!empty($data)):
                        foreach($data as $value){ ?>
                            <a href='<?php echo $value['url']; ?>' title='<?php echo $value['title']; ?>'><img width="128" src='<?php echo Common::getImageUploaded('product/large/'.$value['image']); ?>' /></a>
                        <?php }; ?>
                    <?php endif; ?>
            </div>
        </div>
        <div class="image-right"></div>
    </div>
</div>
