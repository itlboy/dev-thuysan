<?php if(!empty($data)): ?>
    <style type="text/css">
        .tbl_product tr td{
            padding-right: 10px;
            text-align: center;
        }
    </style>
    <div class="prod_desc">
        <?php if(!empty($this->title)): ?>
        <h2><?php echo $this->title; ?></h2>
        <?php endif; ?>
            <span class="prdtable">
                <table style="width: 100%; margin: auto;" class="tbl_product">
                    <tbody>
                    <tr >
                        <td nowrap="nowrap"  style="text-align: left">Sản phẩm</td>
                        <td nowrap="nowrap" >Màu Sắc</td>
                        <td nowrap="nowrap" >Năm sản xuất</td>
                        <td nowrap="nowrap" >Giá cũ</td>
                        <td nowrap="nowrap" >Giá mới</td>
                    </tr>
                <?php foreach($data as $value): ?>
                    <?php if(isset($_GET['id']) AND $_GET['id'] == $value['id']): ?>
                        <tr class="clickr">
                            <td class="listprod1" style="text-align: left;">
                                <strong><a href="<?php echo $value['url']; ?>">
                                    <?php echo $value['title']; ?>
                                </a></strong>
                            </td>
                            <td nowrap="nowrap" style="font-weight: bold;">
                                <?php echo $value['color']; ?>
                            </td>
                            <td nowrap="nowrap" style="text-align: center">
                                <strong><?php echo $value['product_year']; ?></strong>
                            </td>
                            <td nowrap="nowrap" style="text-align: right">
                                <strong><?php echo $value['price_old']; ?></strong>
                            </td>
                            <td nowrap="nowrap" style="text-align: right">
                                <strong><?php echo $value['price']; ?></strong>
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
                <?php foreach($data as $value): ?>
                    <?php if(isset($_GET['id']) AND $_GET['id'] !== $value['id']): ?>
                        <tr class="clickr">
                            <td nowrap="nowrap"  class="listprod1" style="text-align: left;">
                                <a href="<?php echo $value['url']; ?>">
                                    <?php echo $value['title']; ?>
                                </a>
                            </td>
                            <td  nowrap="nowrap">
                                <?php echo $value['color']; ?>
                            </td>
                            <td nowrap="nowrap" style="text-align: center">
                                <?php echo $value['product_year']; ?>
                            </td>
                            <td nowrap="nowrap" style="text-align: right">
                                <?php echo $value['price_old']; ?>
                            </td>
                            <td nowrap="nowrap" style="text-align: right;">
                                <?php echo $value['price']; ?>
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
                    </tbody>
                </table>
            </span>
    </div>
<?php endif; ?>