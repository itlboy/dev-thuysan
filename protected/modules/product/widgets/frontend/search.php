<?php

class search extends Widget{
    public $view = '';
    public $options = array();
    public $limit = 20;
    public $old = NULL;
    public $new = NULL;
    public function init(){
        if($this->view == ''){
            $this->view = __CLASS__;
            Yii::import('product.models.KitProduct');
        }
    }
    public function run(){
        $params = array();
        $criteria = new CDbCriteria();
        $criteria->select = '*';
        if (isset($this->options['keyword']) AND $this->options['keyword'] != '') {
            $keyword = addcslashes($this->options['keyword'], '%_');
            $criteria->addCondition('title LIKE :keyword OR tags LIKE :keyword OR intro LIKE :keyword');
            $params[':keyword'] = "%$keyword%";
        }

        $criteria->order = 't.id DESC';
        if (!empty($params))
            $criteria->params = $params;
        $criteria->group = 't.id';
        $lm = explode(',',$this->limit);
        if(count($lm) > 1)
        {
            $criteria->limit = $lm[0];
            $criteria->offset = $lm[1];
        } else {
            $criteria->limit = $lm[0];
        }

        $assign['total'] = KitProduct::model()->count($criteria);
        $assign['rows'] = KitProduct::model()->findAll($criteria);
        if(!empty($assign['rows'])){
            $assign['rows'] = KitProduct::treatment($assign['rows']);
        } else {
            $assign['rows'] = NULL;
        }

        $this->render($this->view,$assign);
    }
}