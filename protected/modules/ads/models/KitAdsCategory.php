<?php

Yii::import('application.modules.ads.models.db.BaseKitAdsCategory');
class KitAdsCategory extends BaseKitAdsCategory {

    var $className = __CLASS__;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getAds2Category($adsId) {
        $result = array();
        $data = self::findAll('ads_id=:adsId', array(':adsId' => $adsId));
        foreach ($data as $item2Type)
            $result[] = $item2Type->category_id;
        return $result;
    }

    /**
     * Create
     * @param array $categories
     * @param int $itemId 
     */
    public static function createAdsCategory($categories = array(), $itemId) {
        if (! is_array($categories)) return FALSE;
        foreach ($categories as $categoryId) {
            $item2Cat = new KitAdsCategory;
            $item2Cat->category_id = $categoryId;
            $item2Cat->ads_id = $itemId;
            $item2Cat->save();
        }
    }

    /**
     * Delete
     * @param array $categories
     * @param int $itemId 
     */
    public static function deleteAdsCategory($categories = array(), $itemId) {
        if (! is_array($categories)) return FALSE;
        foreach ($categories as $categoryId) {
            self::model()->deleteAll('category_id=:categoryId AND ads_id=:adsId', array(
                ':categoryId' => $categoryId,
                ':adsId' => $itemId,
            ));
        }
    }
}