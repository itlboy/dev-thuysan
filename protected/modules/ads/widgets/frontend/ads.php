<?php
class ads extends Widget
{
    public $view = '';
    public $category = NULL;
    public $title = '';
    public $limit = 5;

    public function init()
    {
        if ($this->view == '') $this->view = __CLASS__;
        Yii::import('ads.models.KitAds');
    }
 
    public function run()
    {
        $data = KitAds::getLastest($this->category);
		if (empty ($data)) return FALSE;
        $data = KitAds::treatment($data);
//        $data = array_slice($data, 0, $this->limit);
        $this->render($this->view, array(
            'data' => $data,
        ));
    }
}