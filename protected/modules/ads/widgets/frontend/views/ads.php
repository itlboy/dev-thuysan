<?php if(!empty($this->title)): ?>
    <p class="ads-bar" style="width: 100%;padding: 0px;"><?php echo $this->title; ?></p>
<?php endif; ?>
<?php foreach ($data as $key => $value): ?>
    <?php if (trim($value['content']) == '') : ?>
        <a href="<?php echo $value['url_target']; ?>" target="_blank"><img src="<?php echo Common::getImageUploaded('ads/origin/'.$value['image']); ?>" /></a>
    <?php else: ?>
        <?php echo $value['content']; ?>
    <?php endif; ?>
    
<?php endforeach; ?>