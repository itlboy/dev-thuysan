<?php

class AdsModule extends CWebModule
{
    public $category = TRUE; // Xác định Module có category hay không
    public $option = TRUE; // Xác định Module có option hay không
    public $menuList = array(
        array(
            'name' => 'Tạo danh mục',
            'url' => '/category/default/create/module/ads',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'type' => 'dialog',
            'grid' => '',
        ),
        array(
            'name' => 'Quản lý danh mục',
            'url' => '/category/default/index/module/ads',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Quản lý danh mục bài viết',
            'type' => 'direct'
        ),
        array(
            'name' => 'Tạo option',
            'url' => '/option/default/create/module/ads',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'type' => 'dialog',
            'grid' => '',
        ),
        array(
            'name' => 'Quản lý option',
            'url' => '/option/default/index/module/ads',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Quản lý option bài viết',
            'type' => 'direct'
        ),
        array(
            'name' => 'Tạo ads',
            'url'  => '/ads/default/create',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Tạo ads',
            'type' => 'direct'
        ),
        array(
            'name' => 'Quản lý ads',
            'url'  => '/ads/default/index',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Quản lý ads',
            'type' => 'direct'
        ),
    );
    public $image = array(
        'origin' => array(
            'type' => 'move',
            'folder' => 'origin',
        ),
//        'large' => array(
//            'type' => 'resize',
//            'width' => 250,
//            'height' => 370,
//            'cropWidth' => NULL,
//            'cropHeight' => NULL,
//            'cropTop' => NULL,
//            'cropLeft' => NULL,
//            'rotate' => 0, // Xoay anh
//            'flip' => NULL, //[integer] Direction. Horizontal = 5, Vertical = 6
//            'quality' => 70,
//            'folder' => 'large',
//        ),
//        'medium' => array(
//            'type' => 'resize',
//            'width' => 130,
//            'height' => 185,
//            'cropWidth' => NULL,
//            'cropHeight' => NULL,
//            'cropTop' => NULL,
//            'cropLeft' => NULL,
//            'rotate' => 0, // Xoay anh
//            'flip' => NULL, //[integer] Direction. Horizontal = 5, Vertical = 6
//            'quality' => 80,
//            'folder' => 'medium',
//        ),
//        'small' => array(
//            'type' => 'resize',
//            'width' => 43,
//            'height' => 55,
//            'cropWidth' => NULL,
//            'cropHeight' => NULL,
//            'cropTop' => NULL,
//            'cropLeft' => NULL,
//            'rotate' => 0, // Xoay anh
//            'flip' => NULL, //[integer] Direction. Horizontal = 5, Vertical = 6
//            'quality' => 100,
//            'folder' => 'small',
//        ),
    );

    public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'ads.models.*',
			'ads.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
