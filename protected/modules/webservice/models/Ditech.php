<?php
/**
 * Description of Ditech
 *
 * @author Qvv
 */
class Ditech {
    
    //SMS variables
    public static $serviceSmsPrices = array(
        '6046' => '500',
        '6146' => '1000',
        '6246' => '2000',
        '6346' => '3000',
        '6446' => '4000',
        '6546' => '50000',
        '6646' => '10000',
        '6746' => '15000'
    );
    
    public static $partnerSmsIps = array("127.0.0.1"); //Thay bằng ip của partner SMS
    
    public static $partnerWapIps = array("127.0.0.1"); //Thay bằng ip của partner Wap
    
    //Wap variables
    public static $wapCharging = array(
        'username' => 'test', // được DT cung cấp
        'password' => 'testbed', // được DT cung cấp
        'partnerId' => '14',
        'url' => 'http://wap.ditech.vn?',
        'handlePhoneUrl' => 'http://wap.ditech.vn/getMSISDN.aspx?'
    );
    
    public static function makeWapChargingUrl($params = array()){
        $strParams = "";
        foreach($params as $key => $value){
            $strParams .= "&{$key}={$value}";
        }
        $url = self::$wapCharging['url'] . $strParams;
        
        return $url;
    }
    
    public static function getHandleResultUrl(){
        $url = Yii::app()->createAbsoluteUrl('webservice/ditechWap/handleResult');
        
        return $url;
    }
    
    public static function getCancelWapChargingUrl(){
        $url = Yii::app()->createAbsoluteUrl('webservice/ditechWap/cancle');
        
        return $url;
    }
    
}

?>
