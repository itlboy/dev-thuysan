<?php
class DitechwapController extends ControllerFrontend{
    
    /**
     *Hàm đón & xử lý kết quả trả về từ DT 
     */
    public function actionHandleResult(){
		$productId = Yii::app()->request->getParam('productId', '');
        $transId = Yii::app()->request->getParam('transId', '');
        $cType = Yii::app()->request->getParam('cType', '');
        $key = Yii::app()->request->getParam('key', ''); //key = Md5(productId + transId + cType + rs + User + Pass)
        $result = Yii::app()->request->getParam('rs', ''); //0: Thành công. 1: Thất bại
        $phone = Yii::app()->request->getParam('msisdn', '');
        
        if($result === 0){ //Charging thành công
            //Get file url 
            $game = KitGame::getDetails($productId);
            if($game){
                //Lưu giao dịch vào DB
                $row = new KitGamePay();
                $row->phone = $phone;
                $row->game_id = $game->id;
                $row->service = 'wap';
                $row->time = date('Y-m-d H:m:s');
                $row->save();
                
                //Header to download
                $file = $game->file;
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename='.basename($file));
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                ob_clean();
                flush();
                readfile($file);
                exit; 
            }
        }
        
	}
    
    /**
     *Hàm xử lý khi giao dịch có sự cố 
     *(sai tham số hoặc khách hàng chủ động từ chối charging...)
     */
    public function actionCancle(){
		//do somthing....
        
	}
    
}
