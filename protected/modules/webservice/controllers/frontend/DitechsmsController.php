<?php
class DitechsmsController extends ControllerFrontend{
    
//    6046, 6146, 6246, 6346, 6446, 6546, 6646, 6746
//    1K, 2K, 3K, 4K, 5K, 10K, 15k
//    Tin nhắn nhận mã OTP: LET OTP gửi Đầu số SMS
//    Tin nhắn tìm lại mật khẩu: LET MK username gửi Đầu số SMS
//    thêm cái nạp tiền nữa: LET NAP username
//URL này để gửi cho DiTech
//echo Yii::app()->createAbsoluteUrl('webservice/ditechsms', array("UserId"=>"0912338165","ServiceId"=>"SERVICE_ID","CommandCode"=>"COMMAND_CODE","Message"=>"LET NAP qvv", "MobileOperator"=>"MOBILE_OPERATOR", "RequestId"=>"REQUEST_ID"));
//http://api.let.vn/webservice/ditechsms/index/UserId/0912338165/ServiceId/6746/CommandCode/1/Message/LET+OTP/MobileOperator/vinaphone/RequestId/10000.let
    
    /**
     * Hàm nhận và xử lý kết quả trả về từ DiTech
     */
    public function actionIndex(){
        //Kiểm tra ip truy cập
//        $remoteIP = Common::getClientIp();
//        $allowIps = Ditech::$partnerSmsIps;
//        if(in_array($remoteIP, $allowIps)){
//            $commandCode = letArray::get($_GET, 'CommandCode');
            $mobieOperator = letArray::get($_GET, 'MobileOperator');
            $requestId = letArray::get($_GET, 'RequestId');
            $userId = letArray::get($_GET, 'UserId');//phone number
            $message = letArray::get($_GET, 'Message');
            $serviceId = letArray::get($_GET, 'ServiceId'); //6746...
            $prices = array(
                '6046' => 500,
                '6146' => 1000,
                '6246' => 2000,
                '6346' => 3000,
                '6446' => 4000,
                '6546' => 5000,
                '6646' => 10000,
                '6746' => 15000,
            );
            $price = letArray::get($prices, $serviceId, NULL);

            if (empty($price)) {
                echo '0|Dau so khong hop le'; die;
            }
            
            $result = FALSE;
            if(!empty($message) AND (strlen($message) <= 160) AND $userId != ''){
                
                $message = strtoupper($message);
                $message = explode(" ", $message);
                
                if(is_array($message) AND $message[0] == "LET"){
                    if (isset($message[1]) AND $message[1] === 'OTP') {
                        //Khởi tạo member
                        $model = KitAccount::model()->find("phone = '{$userId}'");
                        if($model){
                            //Make OTP code
                            $otpCode = Common::randomString(8);

                            //Save OTP to database
                            $accountOTP = new KitAccountOtp();
                            $accountOTP->user_id = $model->id;
                            $accountOTP->username = $model->username;
                            $accountOTP->email = $model->email;
                            $accountOTP->phone = $userId;
                            $accountOTP->otp = $otpCode;
                            $accountOTP->type = "phone";
                            $accountOTP->time = date("Y-m-d H:i:s");
                            if($accountOTP->save()){
                                $result = "1|Chao ban, ma OTP cua ban la: {$otpCode}";
                            } else {
                                $result = "0|Khong tao duoc ma OTP do loi he thong. Vui long thu lai";
                            }
                        } else {
                            $result = "0|Tai khoan khong ton tai.";
                        }
                    } elseif (isset($message[1]) AND $message[1] === 'NAP') {

                        Yii::import('application.modules.currency.models.KitCurrencyPay');
                        Yii::import('application.modules.currency.models.KitCurrencySms');
                        Yii::import('application.modules.currency.models.KitCurrencyGold');
                        Yii::import('application.modules.account.models.KitAccountStats');

                        $goldExchange = 0;
                        $moneyValue = Ditech::$serviceSmsPrices[$serviceId];
                        if($moneyValue === '15000'){
                            $goldExchange = 7;
                        }
                        
                        if(isset($message[2])){ //username
                            //Khởi tạo member
                            $member = KitAccount::model()->find("username = '{$message[2]}'");
                            if($member){
                                // check tồn tại giao dịch
                                $found = KitCurrencyPay::model()->find("checknum = '{$requestId}' AND money = {$moneyValue} AND payment = 'sms'");
                                if(!$found){
                                    // Lưu vao bảng lịch sử giao dịch
                                    $insert = new KitCurrencyPay();
                                    $insert->username = $member->username;
                                    $insert->user_id = $member->id;
                                    $insert->money = $moneyValue;
                                    $insert->gold = $goldExchange;
                                    $insert->payment = 'sms';
                                    $insert->content = "{$member->username} nap tien qua {$serviceId}";
                                    $insert->checknum = $requestId;
                                    $insert->time = date('Y-m-d H:i:s');
                                    $insert->save();

                                    // Lưu vào bảng chứa lịch sử nạp, sử dụng xu
                                    $insert = new KitCurrencyGold();
                                    $insert->username = $member->username;
                                    $insert->user_id = $member->id;
                                    $insert->gold = $goldExchange;
                                    $insert->payment = 'sms';
                                    $insert->content = "{$member->username} nap tien thanh cong qua {$mobieOperator}";
                                    $insert->checknum = $requestId;
                                    $insert->time = date('Y-m-d H:i:s');
                                    $insert->save();

                                    // Lưu vào bảng chứa thông tin sms
                                    $insert = new KitCurrencySms();
                                    $insert->money = $moneyValue;
                                    $insert->gold = $goldExchange;
                                    $insert->phone = $userId;
                                    $strMessage = implode(" ", $message);
                                    $insert->message = $strMessage;
                                    $insert->service = $serviceId;
                                    $insert->checknum = $requestId;
                                    $insert->content = "{$member->username} nap tien thanh cong qua {$mobieOperator}";
                                    $insert->username = $member->username;
                                    $insert->created_time = date('Y-m-d H:i:s');
                                    $insert->save();

                                    // Cập nhật số xu trong tài khoản người dùng
                                    KitAccountStats::updateStats($member->id, array('gold_current','gold_paid','money_paid'));

                                    $result = "1|Nap xu thanh cong. Tai khoan cua ban da duoc nap 7xu.";
                                } else {
                                    $result = "0|Giao dich nap tien da ket thuc.";
                                }
                            } else {
                                $result = "0|Tai khoan {$message[2]} khong ton tai.";
                            }
                        }
                    } elseif (isset($message[1]) AND $message[1] == 'MK') {
                        //Resset passwrod for member
                        $model = KitAccount::model()->find("phone = '{$userId}'");
                        if($model){
                            //Make new password
                            $returnPass = Common::randomString(6);
                            $newPassword = LetIdentityId::encodePassword($returnPass, $model->salt);
                            $model->password_old = $newPassword;
                            $model->password_origin = $returnPass;
                            if($model->update()){
                                $result = "1|Mat khau cua ban tren id.let.vn la: {$returnPass}";
                            }
                            else {
                                $result = "0|He thong dang tam nghi, hay thu lai sau it phut.";
                            }
                        }
                        else{
                            $result = "0|Tai khoan khong ton tai.";
                        }
                    } elseif (isset($message[1]) AND $message[1] == 'KH') {
                        $id2 = isset($message[1]) ? $message[1] : '';
                        $model = KitAccount::model()->find("id2 = '{$id2}'");
                        if($model){
                            //Update phone number for member
                            $model->phone = $userId;
                            if($model->update()){
                                $result = "1|Chuc mung, ban da kich hoat so dien thoai thanh cong.";
                            }
                            else {
                                $result = "0|He thong dang tam nghi, hay thu lai sau it phut.";
                            }
                        }
                        else{
                            $result = "0|Tai khoan khong ton tai.";
                        }
                    } elseif (isset($message[1]) AND $message[1] == 'DL') {
                        $id_app = letArray::get($message, 2, NULL);
    
                        $dir_root = '/home/data.kinggame.info';
                        $file = KitGame::getField($id_app, 'file');
                        $file = $dir_root . '/' . $file;
                        
                        if (!empty($id_app))
                            $result = '1|Ban da download thanh cong ung dung co ID la:' . $id_app . ' voi muc gia ' . $price . ' VND. Bạn co the download tai: ';
                    }
                }
            }
            if ($result == FALSE)
                $result = "0|Tin nhan sai cu phap hoac dich vu khong ton tai.";
//        }
//        else{
//            $message = "Not allow.";
//        }
        
        //Trả lại kết quả
        echo $result;
        
//        Yii::app()->end();
    }
    
}
