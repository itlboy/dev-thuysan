<?php
return array (
  'moduleName' => 'library',
  'tablePrefix' => 'let_',
  'main_tableName' => 'let_kit_library',
  'main_controller' => 'default',
  'main_baseClass' => 'ActiveRecord',
  'hasCategory' => '1',
  'hasOption' => '0',
  'hasComment' => '1',
  'hasMedia' => '1',
  'hasChildren' => '0',
  'children_tableName' => '',
  'children_controller' => '',
  'children_baseClass' => 'ActiveRecord',
  'children_fieldCreate' => '',
  'children_fieldUpdate' => '',
);