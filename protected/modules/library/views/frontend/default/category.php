<div class="content">
    <div class="title-wrapper">
        <ul>
            <li>
                <a href="/">Trang chủ</a>
            </li>
            <i class="logo_item">

            </i>
            <li>
                <a href=""><?php echo $category->name ?></a>
            </li> 
        </ul>
    </div>
    <?php include "treeMenu.php" ?>
    <div class="menu-right">
        <?php if ($dataprovider->getData()): ?>
            <ul>
                <?php foreach ($dataprovider->getData() as $library): ?>
                    <li>
                        <a href="<?php echo $library->getLink(); ?>"><?php echo $library->title ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php else: ?>
        <div class="empty">Hiện chưa có bài viết nào trong danh mục này</div>
        <?php endif; ?>
    </div>
    <div class="paging-block">
        <?php
        $this->widget('CLinkPager', array(
            'pages' => $dataprovider->getPagination(),
            'firstPageCssClass' => 'first',
            'hiddenPageCssClass' => 'hidden',
            'internalPageCssClass' => 'internal',
            'lastPageCssClass' => 'last',
            'nextPageCssClass' => 'next',
            'previousPageCssClass' => 'previous',
            'selectedPageCssClass' => 'selected',
            'htmlOptions' => array('class' => ''),
            'firstPageLabel' => '<<',
            'prevPageLabel' => '<',
            'nextPageLabel' => '>',
            'lastPageLabel' => '>>'
        ))
        ?>
    </div>
</div>
