<div class="content">
    <div class="title-wrapper">
        <ul>
            <li>
                <a href="">Trang chủ</a>
            </li>
            <i class="logo_item">
            </i>
            <li>
                <a href="">Heo</a>
            </li>
        </ul>
    </div>
    <div class="Detailed-page">
       <?php include 'treeMenu.php'; ?>
        <div class="text-right">
            <div class="title-text1">
                <a href="">
                    Bệnh viêm phổi màng phổi ở heo
                </a>
            </div>
            <span>20/11/2015 12:00</span>
            <h4>Văn chương là khái niệm dùng để chỉ một ngành nghệ thuật – nghệ thuật ngôn từ. Văn chương dùng ngôn từ làm chất liệu để xây dựng hình tượng, phản ánh và biểu hiện đời sống. Khái niệm văn chương và văn học thường bị dùng lẫn lộn.</h4>
            <p>
                Lần đầu tiên là năm 1953, ngôi nhà được cứu sau trận càn quét, phóng hỏa của thực dân Pháp. Lần thứ hai, ngôi nhà suýt bị đem xẻ lấy gỗ nếu cụ Trần Thế Lễ bấy giờ mua được, nhưng may thay, ngôi nhà đã được một Việt kiều mua lại để định cư.

                Cũng từ cuốn truyện ngắn nổi tiếng của Nam Cao, người ta còn nhắc đến Vũ Đại với bát cháo hành tình nghĩa và vườn chuối sau nhà. Có lẽ bấy giờ, đây chính là đặc sản mộc mạc, chân quê của người dân Vũ Đại.

                Bây giờ

                Làng Vũ Đại ngày ấy bây giờ nằm cách trung tâm thành phố Phủ Lý, Hà Nam chừng 40 km theo đường tỉnh lộ 972. Nghề dệt vải vẫn được bà con nơi đây gìn giữ, phát huy. Nhưng đến đây, bạn không còn nghe thấy tiếng thoi lách cách, mà thay vào đó là âm thanh máy dệt vải bán công nghiệp khắp xóm làng.

                Đến thăm nhà Bá Kiến nằm ngay sát bên con đường đất liên thôn ở xóm 11 xã Hòa Hậu, bạn không khỏi ngạc nhiên khi trải qua hơn 100 năm dãi dầu mưa nắng, mái ngói của ngôi nhà vẫn dù chưa một lần tu sửa vẫn phẳng lỳ, không dột nát. Các hoa văn chạm khắc chữ nho, hình rồng ở văng, kèo, li tô dường như vẫn còn nguyên. 

                Mỗi bước chân đi qua từng hàng gạch, cửa nhà, từng tình tiết về làng Vũ Đại xoay quanh ngôi nhà Bá Kiến như hiện ra rõ nét. Đến đây, bạn còn được nghe những câu chuyện thăng trầm về ngôi nhà trăm tuổi từ những bậc cao niên trong làng.
                Lần đầu tiên là năm 1953, ngôi nhà được cứu sau trận càn quét, phóng hỏa của thực dân Pháp. Lần thứ hai, ngôi nhà suýt bị đem xẻ lấy gỗ nếu cụ Trần Thế Lễ bấy giờ mua được, nhưng may thay, ngôi nhà đã được một Việt kiều mua lại để định cư.

                Cũng từ cuốn truyện ngắn nổi tiếng của Nam Cao, người ta còn nhắc đến Vũ Đại với bát cháo hành tình nghĩa và vườn chuối sau nhà. Có lẽ bấy giờ, đây chính là đặc sản mộc mạc, chân quê của người dân Vũ Đại.

                Bây giờ

                Làng Vũ Đại ngày ấy bây giờ nằm cách trung tâm thành phố Phủ Lý, Hà Nam chừng 40 km theo đường tỉnh lộ 972. Nghề dệt vải vẫn được bà con nơi đây gìn giữ, phát huy. Nhưng đến đây, bạn không còn nghe thấy tiếng thoi lách cách, mà thay vào đó là âm thanh máy dệt vải bán công nghiệp khắp xóm làng.

                Đến thăm nhà Bá Kiến nằm ngay sát bên con đường đất liên thôn ở xóm 11 xã Hòa Hậu, bạn không khỏi ngạc nhiên khi trải qua hơn 100 năm dãi dầu mưa nắng, mái ngói của ngôi nhà vẫn dù chưa một lần tu sửa vẫn phẳng lỳ, không dột nát. Các hoa văn chạm khắc chữ nho, hình rồng ở văng, kèo, li tô dường như vẫn còn nguyên. 

                Mỗi bước chân đi qua từng hàng gạch, cửa nhà, từng tình tiết về làng Vũ Đại xoay quanh ngôi nhà Bá Kiến như hiện ra rõ nét. Đến đây, bạn còn được nghe những câu chuyện thăng trầm về ngôi nhà trăm tuổi từ những bậc cao niên trong làng.
                Lần đầu tiên là năm 1953, ngôi nhà được cứu sau trận càn quét, phóng hỏa của thực dân Pháp. Lần thứ hai, ngôi nhà suýt bị đem xẻ lấy gỗ nếu cụ Trần Thế Lễ bấy giờ mua được, nhưng may thay, ngôi nhà đã được một Việt kiều mua lại để định cư.

                Cũng từ cuốn truyện ngắn nổi tiếng của Nam Cao, người ta còn nhắc đến Vũ Đại với bát cháo hành tình nghĩa và vườn chuối sau nhà. Có lẽ bấy giờ, đây chính là đặc sản mộc mạc, chân quê của người dân Vũ Đại.

                Bây giờ

                Làng Vũ Đại ngày ấy bây giờ nằm cách trung tâm thành phố Phủ Lý, Hà Nam chừng 40 km theo đường tỉnh lộ 972. Nghề dệt vải vẫn được bà con nơi đây gìn giữ, phát huy. Nhưng đến đây, bạn không còn nghe thấy tiếng thoi lách cách, mà thay vào đó là âm thanh máy dệt vải bán công nghiệp khắp xóm làng.

                Đến thăm nhà Bá Kiến nằm ngay sát bên con đường đất liên thôn ở xóm 11 xã Hòa Hậu, bạn không khỏi ngạc nhiên khi trải qua hơn 100 năm dãi dầu mưa nắng, mái ngói của ngôi nhà vẫn dù chưa một lần tu sửa vẫn phẳng lỳ, không dột nát. Các hoa văn chạm khắc chữ nho, hình rồng ở văng, kèo, li tô dường như vẫn còn nguyên. 

                Mỗi bước chân đi qua từng hàng gạch, cửa nhà, từng tình tiết về làng Vũ Đại xoay quanh ngôi nhà Bá Kiến như hiện ra rõ nét. Đến đây, bạn còn được nghe những câu chuyện thăng trầm về ngôi nhà trăm tuổi từ những bậc cao niên trong làng.
                Lần đầu tiên là năm 1953, ngôi nhà được cứu sau trận càn quét, phóng hỏa của thực dân Pháp. Lần thứ hai, ngôi nhà suýt bị đem xẻ lấy gỗ nếu cụ Trần Thế Lễ bấy giờ mua được, nhưng may thay, ngôi nhà đã được một Việt kiều mua lại để định cư.

                Cũng từ cuốn truyện ngắn nổi tiếng của Nam Cao, người ta còn nhắc đến Vũ Đại với bát cháo hành tình nghĩa và vườn chuối sau nhà. Có lẽ bấy giờ, đây chính là đặc sản mộc mạc, chân quê của người dân Vũ Đại.

                Bây giờ

                Làng Vũ Đại ngày ấy bây giờ nằm cách trung tâm thành phố Phủ Lý, Hà Nam chừng 40 km theo đường tỉnh lộ 972. Nghề dệt vải vẫn được bà con nơi đây gìn giữ, phát huy. Nhưng đến đây, bạn không còn nghe thấy tiếng thoi lách cách, mà thay vào đó là âm thanh máy dệt vải bán công nghiệp khắp xóm làng.

                Đến thăm nhà Bá Kiến nằm ngay sát bên con đường đất liên thôn ở xóm 11 xã Hòa Hậu, bạn không khỏi ngạc nhiên khi trải qua hơn 100 năm dãi dầu mưa nắng, mái ngói của ngôi nhà vẫn dù chưa một lần tu sửa vẫn phẳng lỳ, không dột nát. Các hoa văn chạm khắc chữ nho, hình rồng ở văng, kèo, li tô dường như vẫn còn nguyên. 

                Mỗi bước chân đi qua từng hàng gạch, cửa nhà, từng tình tiết về làng Vũ Đại xoay quanh ngôi nhà Bá Kiến như hiện ra rõ nét. Đến đây, bạn còn được nghe những câu chuyện thăng trầm về ngôi nhà trăm tuổi từ những bậc cao niên trong làng.

            </p>
            <div class="fb-like"></div>
            <div class="title-block">
                <a href="">Tin mới cùng chuyên mục</a>
            </div>
            <div class="menu-right1">
                <ul>
                    <li>
                        <a href="">Bệnh đẻ khó ở lợn</a>
                    </li>
                    <li>
                        <a href="">Bệnh đẻ khó ở lợn</a>
                    </li>
                    <li>
                        <a href="">Bệnh đẻ khó ở lợn</a>
                    </li>
                    <li>
                        <a href="">Bệnh đẻ khó ở lợn</a>
                    </li>
                    <li>
                        <a href="">Bệnh đẻ khó ở lợn</a>
                    </li>
                    <li>
                        <a href="">Bệnh đẻ khó ở lợn</a>
                    </li>
                    <li>
                        <a href="">Bệnh đẻ khó ở lợn</a>
                    </li>
                    <li>
                        <a href="">Bệnh đẻ khó ở lợn</a>
                    </li>
                    <li>
                        <a href="">Bệnh đẻ khó ở lợn</a>
                    </li>
                    <li>
                        <a href="">Bệnh đẻ khó ở lợn</a>
                    </li>
                    <li>
                        <a href="">Bệnh đẻ khó ở lợn</a>
                    </li>
                    <li>
                        <a href="">Bệnh đẻ khó ở lợn</a>
                    </li>

                </ul>
            </div>
        </div>

    </div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
</div>
