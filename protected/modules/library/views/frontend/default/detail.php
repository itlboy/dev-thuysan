<?php
/**
 * @var KitLibrary $library
 */
?>
<div class="content">
    <div class="title-wrapper">
        <ul>
            <li>
                <a href="">Trang chủ</a>
            </li>
            <i class="logo_item">
            </i>
            <li>
                <a href="">Heo</a>
            </li>
        </ul>
    </div>
    <div class="Detailed-page">
        <?php include 'treeMenu.php'; ?>
        <?php if ($library): ?>
            <div class="text-right">
                <div class="title-text1">
                    <a href=" <?php echo $library->getLink(); ?>">
                        <?php echo $library->title ?>
                    </a>
                </div>
                <span><?php echo $library->created_time ?></span>
                <h4><?php echo $library->intro ?></h4>
                <p>
                    <?php echo $library->content; ?>
                </p>
                <div class="fb-like"></div>
                <div class="title-block">
                    <a href="">Tin mới cùng chuyên mục</a>
                </div>
                <div class="menu-right1">
                    <ul>
                        <?php foreach ($library->getRelatedLibs() as $relatedLib): ?>
                            <li>
                                <a href="<?php echo $relatedLib->getLink(); ?>"><?php echo $relatedLib->title ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        <?php else: ?>
            <div class="empty">Bài viết này không tồn tại hoặc đã bị xóa !</div>
        <?php endif; ?>

    </div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
</div>
