<?php
$moduleName = 'library';
$categories = KitCategory::getTree('library');
?>
<div class="menu-left">
    <div class="title-menu-left">
        <a href="">
            Danh mục
        </a>
    </div>
    <div class="menu-left-block">
        <ul>
            <?php foreach ($categories as $category): ?>
                <li>
                    <a href="javascript:void(0);">
                        <div class="item-mn"></div><?php echo $category['text']; ?></a>
                    <ul class="menu-cap2 active-menu">
                        <?php foreach ($category['children'] as $child): ?>
                            <li>
                                <?php echo $child['text']; ?>

                                <ul class="menu-cap3">
                                    <?php foreach ($child['children'] as $kid): ?>
                                        <li>
                                            <?php echo $kid['text']; ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>