<?php
$moduleName = 'library';
$categories = KitCategory::getTree('library');
$cs = Yii::app()->clientScript;
//$cs->registerScriptFile('//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js');
//$cs->registerCssFile('//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css');
//$cs->registerScriptFile(Yii::app()->theme->baseUrl . '/plugins/jsTree/jstree.js');
//$cs->registerCssFile(Yii::app()->theme->baseUrl . '/plugins/jsTree/themes/default/style.css');
?>
<div class="library category content">
    <?php foreach ($categories as $category): ?>
        <div class="parent-category"><?php echo $category['text']; ?></div>
        <?php foreach ($category['children'] as $child): ?>
            <div class="child-category"><?php echo $child['text']; ?></div>
        <?php endforeach; ?>
    <?php endforeach; ?>
</div>

<script>
$('.child-category').click(function(e){
    window.location = $(this).children('a').attr('href');
}) 
</script>