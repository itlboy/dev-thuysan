<?php

/**
 * Module: library
 * Auth: nguago@let.vn
 * Website: http://let.vn
 */
class DefaultController extends ControllerFrontend {

    public $pageSize = 25;

    public function actionIndex() {
        $categoryId = Yii::app()->request->getParam('category_id');
        $detail = Yii::app()->request->getParam('detail');
        if (!$categoryId && !$detail)
            $this->render('index');
        else {
            if ($detail) {
                //Trang chi tiet
                $library = KitLibrary::model()->findByPk($detail);
//                CVarDumper::dump($library,10,true);
//                die();
//                die($library->getCategoryId());
                $this->render('detail', array('library' => $library));
            } else {
                //Trang danh muc
                $lirary = new KitLibrary();
                $lirary->category_id = $categoryId;
                $dataprovider = $lirary->search($this->pageSize);
                $category = KitCategory::model()->findByPk($categoryId);
                $this->render('category', array('category' => $category, 'dataprovider' => $dataprovider));
            }
        }
    }

    public function actionCategory2() {
        $this->render('category2');
    }

    public function actionLienhe() {
        $this->render('lienhe');
    }

    public function actionThuvienchitiet() {
        $this->render('thuvienchitiet');
    }

}
