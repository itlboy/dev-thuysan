<?php

Yii::import('application.modules.library.models.db.BaseKitLibrary');

class KitLibrary extends BaseKitLibrary {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getByCategory($categoryId) {
////        $librariesCategory = KitLibraryCategory::model()->findAllByAttributes(
////                array('category_id' => $categoryId
////        ));
////        if ($librariesCategory) {
//           $lirary = new self();
//           $lirary->category_id = $categoryId;
//           $dataprovider = $lirary->search();
//           CVarDumper::dump($dataprovider->getData());
////        } else {
////            return null;
////        }
    }

    public function getLink() {
        return Yii::app()->createUrl('/library/default/index/detail/' . $this->id);
    }

    public function getCategoryId() {
        return $this->KitLibraryCategory[0]->id;
    }

    public function getRelatedLibs($number = 10) {
        $model = new self();
        $model->id = "$this->id";
        $model->category_id = $this->getCategoryId();
        return $model->search($number,true)->getData();
    }

}
