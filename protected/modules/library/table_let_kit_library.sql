-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 28, 2014 at 07:26 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `letkit`
--
-- CREATE DATABASE IF NOT EXISTS `letkit` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
-- USE `letkit`;

-- --------------------------------------------------------

--
-- Table structure for table `let_kit_library`
--

CREATE TABLE IF NOT EXISTS `let_kit_library` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `focus` tinyint(1) DEFAULT '0',
  `has_brand` tinyint(1) NOT NULL DEFAULT '0',
  `is_home` tinyint(1) NOT NULL DEFAULT '0',
  `author` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT '',
  `intro` varchar(500) DEFAULT '',
  `content` text,
  `tags` varchar(500) DEFAULT '',
  `view_count` int(11) DEFAULT NULL COMMENT 'so luong view',
  `comment_count` int(11) DEFAULT NULL COMMENT 'so luong comment',
  `from_time` datetime DEFAULT NULL,
  `to_time` datetime DEFAULT NULL,
  `layout_id` varchar(45) DEFAULT NULL COMMENT 'Layout hien thi',
  `seo_title` varchar(70) DEFAULT NULL,
  `seo_url` varchar(255) DEFAULT NULL,
  `seo_desc` varchar(160) DEFAULT NULL,
  `creator` int(11) unsigned DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `editor` int(11) unsigned DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `sorder` int(10) unsigned DEFAULT '500' COMMENT 'Thu tu sap xep uu tien',
  `promotion` tinyint(1) unsigned DEFAULT '0' COMMENT 'trang thai set lam tieu diem hay khong',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT 'trang thai hien thi: 0 = khong hien thi \\n 1 = hien thi',
  `trash` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_sorder` (`sorder`),
  KEY `idx_status` (`status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `let_kit_library`
--

INSERT INTO `let_kit_library` (`id`, `title`, `focus`, `has_brand`, `is_home`, `author`, `source`, `image`, `intro`, `content`, `tags`, `view_count`, `comment_count`, `from_time`, `to_time`, `layout_id`, `seo_title`, `seo_url`, `seo_desc`, `creator`, `created_time`, `editor`, `updated_time`, `sorder`, `promotion`, `status`, `trash`) VALUES
(1, 'Test 01', 0, 0, 0, '', '', '', '', '', '', 0, 0, NULL, NULL, '', '', '', '', 1, '2014-02-28 14:23:09', 1, '2014-02-28 14:23:32', 500, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `let_kit_library_category`
--

CREATE TABLE IF NOT EXISTS `let_kit_library_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `library_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_library_id` (`library_id`),
  KEY `idx_category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `let_kit_library_category`
--

INSERT INTO `let_kit_library_category` (`id`, `library_id`, `category_id`) VALUES
(1, 1, 109);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
