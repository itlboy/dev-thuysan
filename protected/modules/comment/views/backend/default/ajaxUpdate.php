<?php
$moduleName = 'comment';
$modelName = 'KitComment';
$formID = 'kit_'.$moduleName.'_form';
$gridID = 'kit_'.$moduleName.'_grid';

Yii::import('application.modules.article.models.KitArticle');
$form = $this->beginWidget('CActiveForm', array(
	'id' => $formID,
	'enableAjaxValidation'=>false,
    'htmlOptions' => array(
        'onSubmit' => 'js:ajaxSaveForm("'.$modelName.'" ,jQuery(this).attr("id"), jQuery(this).attr("action")); return false',
    )
));
echo $form->hiddenField($model, 'approve');
?>

<div id="" class="dialog_content">
    <div class="block" style="opacity: 1;">
        <fieldset>
            <?php echo $form->labelEx($model, 'content'); ?>
            <div class="clearfix">
                <?php echo $form->textArea($model, 'content', array('class' => 'tooltip textarea autogrow')); ?>
            </div>
        </fieldset>
        <fieldset>
            <?php echo $form->labelEx($model,'status'); ?>
            <div class="clearfix">
                <div class="selector" id="uniform-undefined"><span style="-moz-user-select: none;"><?php echo Yii::t('BackEnd', "-- Select a status --"); ?></span>
                    <?php echo $form->dropdownList($model, 'status', BackendFunctions::getStatusOptions(), array(
                        'class' => 'select_box',
                        'style' => 'opacity:0',
                    )) ?>
                </div>
            </div>
        </fieldset>
        <div class="columns clearfix">
            <div class="col_50">
                <fieldset>
                    <?php echo $form->labelEx($model, 'author_email'); ?>
                    <div>
                        <?php echo $model->author_email; ?>
                    </div>
                </fieldset>
            </div>
            <div class="col_50">
                <fieldset>
                    <?php echo $form->labelEx($model, 'created_time'); ?>
                    <div>
                        <?php echo $model->created_time; ?>
                    </div>
                </fieldset>
            </div>
        </div>
        <div class="columns clearfix">
            <div class="col_50">
                <fieldset>
                    <?php echo $form->labelEx($model, 'approved_by'); ?>
                    <div>
                        <?php echo (isset($model->approvedUser->username)) ? $model->approvedUser->username : ''; ?>
                    </div>
                </fieldset>
            </div>
            <div class="col_50">
                <fieldset>
                    <?php echo $form->labelEx($model, 'approved_time'); ?>
                    <div>
                        <?php echo $model->approved_time; ?>
                    </div>
                </fieldset>
            </div>
        </div>

        <div class="button_bar clearfix">
            <button class="dark green close_dialog">
                <div class="ui-icon ui-icon-check"></div>
                <span>Lưu</span>
            </button>
            <button class="dark green close_dialog" onclick="jQuery('#KitComment_approve').val(1)">
                <div class="ui-icon ui-icon-check"></div>
                <span>Lưu và Xác nhận</span>
            </button>
<!--                <button class="dark red close_dialog">
                <div class="ui-icon ui-icon-closethick"></div>
                <span>Cancel</span>
            </button>-->
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>
