
<div class="box grid_16" style="margin-top: 10px;">
    <div class="toggle_container">
        <div class="block">
            <h2 class="section"><?php echo Yii::t('global', 'General'); ?></h2>
            
                
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'author_name'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'author_name'); ?>
                    <?php echo $form->error($model, 'author_name'); ?>
                </div>
            </fieldset>
                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'author_email'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'author_email'); ?>
                    <?php echo $form->error($model, 'author_email'); ?>
                    <div class="required_tag tooltip hover left" title="<?php echo Yii::t('global', 'Required'); ?>"></div>
                </div>
            </fieldset>
                            
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'module'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'module'); ?>
                    <?php echo $form->error($model, 'module'); ?>
                </div>
            </fieldset>
                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'item_id'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'item_id'); ?>
                    <?php echo $form->error($model, 'item_id'); ?>
                </div>
            </fieldset>
                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'reply_to'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'reply_to'); ?>
                    <?php echo $form->error($model, 'reply_to'); ?>
                    <div class="required_tag tooltip hover left" title="<?php echo Yii::t('global', 'Required'); ?>"></div>
                </div>
            </fieldset>
                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'lineage'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'lineage'); ?>
                    <?php echo $form->error($model, 'lineage'); ?>
                </div>
            </fieldset>
                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'deep'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'deep'); ?>
                    <?php echo $form->error($model, 'deep'); ?>
                </div>
            </fieldset>
                                            
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'approve'); ?></label>
                <div>
                    <div class="jqui_radios">
                        <?php echo $form->radioButtonList($model, 'approve', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                        <?php echo $form->error($model, 'approve'); ?>
                    </div>
                </div>
            </fieldset>
                                    
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'approved_time'); ?></label>
                <div class="clearfix">
                    <?php echo $form->textField($model, 'approved_time', array('class' => 'datepicker')); ?>
                    <?php echo $form->error($model, 'approved_time'); ?>
                </div>
            </fieldset>
                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'approved_by'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'approved_by'); ?>
                    <?php echo $form->error($model, 'approved_by'); ?>
                    <div class="required_tag tooltip hover left" title="<?php echo Yii::t('global', 'Required'); ?>"></div>
                </div>
            </fieldset>
                

        </div>
    </div>
</div>



<div class="box grid_16">
    <?php echo $form->error($model,'content'); ?>
    <?php        //echo $form->textArea($model, 'content', array('rows' => 6, 'cols' => 50, 'class' => 'tinymce'));
        $this->widget('ext.elrtef.elRTE', array(
            'model' => $model,
            'attribute' => 'content',
            'options' => array(
                    'doctype' => 'js:\'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\'',
                    'cssClass' => 'el-rte',
                    'cssfiles' => array('css/elrte-inner.css'),
                    'absoluteURLs'=>true,
                    'allowSource' => true,
                    'lang' => 'vi',
                    'styleWithCss'=>'',
                    'height' => 400,
                    'fmAllow'=>true, //if you want to use Media-manager
                    'fmOpen'=>'js:function(callback) {$("<div id=\"elfinder\" />").elfinder(%elfopts%);}',//here used placeholder for settings
                    'toolbar' => 'maxi',
            ),
            'elfoptions' => array( //elfinder options
                'url' => 'auto',  //if set auto - script tries to connect with native connector
                'passkey' => 'mypass', //here passkey from first connector`s line
                'lang' => 'vi',
                'dialog' => array('width' => '900','modal' => true, 'title' => 'Media Manager'),
                'closeOnEditorCallback' => true,
                'editorCallback' => 'js:callback'
            ),
        ));
    ?>
</div>
