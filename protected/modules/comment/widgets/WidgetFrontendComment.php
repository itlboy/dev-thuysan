<?php

Yii::import('application.modules.comment.models.KitComment');

class WidgetFrontendComment extends Widget {

    public $module;
    public $itemId;
    public $limit;

    public function run() {
        $criteria = new CDbCriteria;
        $criteria->condition = 'module = :module AND item_id = :itemId';
        $criteria->params = array(
            ':module' => $this->module,
            ':itemId' => $this->itemId,            
        );
       
        // yiinfinite-scroll
        $total = KitComment::model()->count($criteria);
        
        $pages = new CPagination($total);
        $pages->pageSize = $this->limit;
        $pages->applyLimit($criteria);
        
        $assign['rows'] = KitComment::model()->findAll($criteria);
        $assign['pages'] = $pages;
        $assign['total'] = $total;

        $this->render('WidgetFrontendComment', $assign);
    }

}
