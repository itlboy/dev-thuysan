<?php

class lastestComment extends Widget {

    public $view = '';
    public $title = '';
    public $limit = 10;

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
        Yii::import('comment.models.KitComment');
    }

    public function run() {
        $data = KitComment::getLastest();
        if (empty($data))
            return FALSE;
        $data = KitComment::treatment($data);
        $data = array_slice($data, 0, $this->limit);
        $this->render($this->view, array(
            'data' => $data,
        ));
    }

}