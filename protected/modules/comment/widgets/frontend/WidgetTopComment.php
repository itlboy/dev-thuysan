<?php

Yii::import('application.modules.comment.models.KitComment');
Yii::import('application.modules.account.models.KitAccountStats');

class WidgetTopComment extends CWidget {

    public $itemId;
    public $module;
    public $limit = 5;
    public $approve = 0;
    public $view = '';
    public $title = '';

    public function init()
    {
        if ($this->view == '') $this->view = __CLASS__;
    }
    public function run() {

        $cache_name = md5(__METHOD__ . '_' . $this->limit);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(KitAccountStats::model()->getAttributes(), 't.');
            $criteria->order = 't.comment DESC';
            $criteria->limit = $this->limit;
            $result = KitAccountStats::model()->findAll($criteria);
            if(!empty($result)){
                $result = KitAccountStats::treatment($result);
            }
            Yii::app()->cache->set($cache_name, $result, (60*60*12)); // Set cache
        } else $result = $cache;
        $this->render($this->view,array(
            'data' => $result,
        ));
    }

}
