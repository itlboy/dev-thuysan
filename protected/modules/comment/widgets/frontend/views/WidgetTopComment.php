<div>
    <?php if(!empty($this->title)): ?>
        <h1><?php echo $this->title; ?></h1>
    <?php endif; ?>
    <?php if(!empty($data)): ?>
    <ul class="">
        <?php foreach($data as $item): ?>
        <li>
            <a target="_blank" title="Cấp độ: <?php echo KitAccountLevel::getField(KitAccount::getField((int)$item['user_id'],'level'),'title'); ?>" data-placement="bottom" rel="tooltip" data-original-title="Cấp độ: <?php echo KitAccountLevel::getField(KitAccount::getField((int)$item['user_id'],'level'),'title'); ?>" href="http://id.let.vn/account/level/index.let" style="padding: 0px;margin: 0px;height: 16px;width:16px;"><img style="vertical-align: middle;height: 15px; margin: 3px 2px;" src="<?php echo KitAccountLevel::getField(KitAccount::getField((int)$item['user_id'],'level'),'image_url') ?>" height="20" /></a>
            <a href="#"><?php echo KitAccount::getField((int)$item['user_id'],'username'); ?></a>
            <span style="font-size: 11px; font-style: italic;">Bình luận: <?php echo number_format($item['comment'],0,',','.'); ?></span>
        </li>
        <?php endforeach; ?>
    </ul>
    <?php endif; ?>
</div>
