<script type="text/javascript">
    $(document).ready(function(){
        $('.bt-comment').live('click',function(){
            var content = $('#KitComment_content').val();
            if(content == ""){
                alert('Bạn chưa nhập nội dung comment');
                return false;
            }
            $.ajax({
                type:'POST',
                url:'<?php echo Yii::app()->CreateUrl('comment/ajax/changtext'); ?>',
                data:{content:content},
                success:function(data){
                    eval('data='+data);
                    if(data.error){
                        alert(data.data);
                        return false;
                    }
                }
            });
        });
    });
</script>
<?php

$author_email = '';
$author_name = '';
if(!empty(Yii::app()->request->cookies["anthor_comm"])):
    $author_comm = json_decode(Yii::app()->request->cookies["anthor_comm"]->value);
    $author_email = $author_comm->author_email;
    $author_name = $author_comm->author_name;
endif;


$showCommentForm = true;
if ($requireUser == true) {
    $showCommentForm = (Yii::app()->user->isGuest) ? true : false;
}
?>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'kit-comment-form',
    'action' => $urlComment,
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'onSubmit' => 'ajaxSubmitComment(jQuery(this).attr("id"), jQuery(this).attr("action"), "' . $urlCaptcha .'"); return false',
    )
));
echo $form->hiddenField($modelComment, 'module');
echo $form->hiddenField($modelComment, 'item_id');
echo $form->hiddenField($modelComment, 'reply_to');
echo $form->hiddenField($modelComment, 'lineage');
?>
    <div class="comment-msg"></div>
    <?php if ($this->requireUser == false): ?>
        <?php if(Yii::app()->user->isGuest): ?>
            <div>Email</div>
            <div><input type="text" class="cm-content" maxlength="150" name="KitComment[author_email]" style="width:97%;" id="KitComment_author_email" value="<?php echo $author_email; ?>"></div>
            <div>Họ tên</div>
            <div><input type="text" class="cm-content" maxlength="150" name="KitComment[author_name]" style="width:97%;" id="KitComment_author_name" value="<?php echo $author_name; ?>" ></div>
        <?php endif; ?>
        <div>Nội dung</div>
        <div><?php echo $form->textArea($modelComment, 'content',array('style' => 'width:97%', 'class' => 'cm-content')); ?></div>
        <div><?php echo CHtml::submitButton('Gửi bình luận',array('class' => 'bt-comment')); ?></div>
    <?php else: ?>
        <?php if(!Yii::app()->user->isGuest): ?>
            <div><?php echo $modelComment->getAttributeLabel('content'); ?></div>
            <div><?php echo $form->textArea($modelComment, 'content',array('style' => 'width:97%','class' => 'cm-content' ,'value' => '')); ?></div>
            <div><?php echo CHtml::submitButton('Gửi bình luận',array('class' => 'bt-comment')); ?></div>
        <?php endif; ?>
    <?php endif; ?>
<?php $this->endWidget(); ?>
<div id="widget-comment-wrapper" class="comment">
    <div class="comment-items content comment-list" >
        <?php foreach ($model as $data) : ?>
        <div class="comment-item">
            <div class="comment yt-tile-default ">
                <div class="comment-body">
                    <div class="content-container">
                        <div class="content" style="width: 98%;">
                            <div class="comment-text" dir="ltr">
                                <a href="javascript:;" class="yt-user-name " dir="ltr">
                                    <?php if(!empty($data->author_name)){
                                    echo $data->author_name;
                                } else {
                                    echo $data->author_email;
                                };
                                    ?>
                                </a>
                                <span class="time" style="float: right;font-style: italic;">
                                    <?php echo !empty($data->created_time) ? letDate::fuzzy_span(strtotime($data->created_time)) : ''; ?>
                                </span>
                            </div>
                            <p class="metadata">
                                <span class="author " style="margin-top: 5px">
                                    <?php echo $data->content ?>
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
        <a href="javascript:;" style="display:none" class="comment-url" rel="<?php echo $urlComment; ?>">URL</a>
        <a href="javascript:;" style="display:none" class="comment-offset" rel="<?php echo $offset; ?>">OFFSET</a>
    </div>
    <div class="comment-load"></div>
    <div class="comment-finish"></div>
</div>