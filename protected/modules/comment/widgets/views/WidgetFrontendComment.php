<div id="posts">
    <?php foreach ($rows as $row): ?>
        <div class="post" style="height:300px; background:green; margin-bottom:10px">            
            <?php echo $row->content; ?>
        </div>
    <?php endforeach; ?>
</div>
<?php
$this->widget('ext.yiinfinite-scroll.YiinfiniteScroller', array(
    'contentSelector' => '#posts',
    'itemSelector' => 'div.post',
    'loadingText' => 'Loading...',
    'donetext' => 'This is the end... my only friend, the end',
    'pages' => $pages,
));
