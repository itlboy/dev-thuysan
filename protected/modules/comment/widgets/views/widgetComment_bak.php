<?php
$showCommentForm = true;
if ($requireUser == true) {
    $showCommentForm = (Yii::app()->user->isGuest) ? true : false;
}
?>
<?php if (!$isAjax AND $showCommentForm) : ?>
<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'kit-comment-form',
    'action' => $urlComment,
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'onSubmit' => 'ajaxSubmitComment(jQuery(this).attr("id"), jQuery(this).attr("action"), "' . $urlCaptcha .'"); return false',
    )
));
echo $form->hiddenField($modelComment, 'module');
echo $form->hiddenField($modelComment, 'item_id');
echo $form->hiddenField($modelComment, 'reply_to');
echo $form->hiddenField($modelComment, 'lineage');
?>
    <div class="comment-msg"></div>
    <?php if(Yii::app()->user->isGuest): ?>
        <div><?php echo $modelComment->getAttributeLabel('author_email'); ?></div>
        <div><input type="text" maxlength="150" name="KitComment[author_email]" style="width:97%;" id="KitComment_author_email"></div>
    <?php endif; ?>
    <div><?php echo $modelComment->getAttributeLabel('content'); ?></div>
    <div><?php echo $form->textArea($modelComment, 'content',array('style' => 'width:97%')); ?></div>


    <?php /* if (CCaptcha::checkRequirements()): ?>
        <div>
            <?php
            echo CHtml::activeTextField($modelComment, 'verifyCode', array(
                'value' => Yii::t('default', 'Enter the code below'),
                //'onblur' => 'if(this.value=="") {this.value="' . Yii::t('ArticleModule.default', 'Enter the code below') . '";}',
                //'onfocus' => 'if(this.value=="' . Yii::t('ArticleModule.default', 'Enter the code below') . '") {this.value="";}'
            ));
            ?>
        </div>
        <div class="captcha"><?php $this->widget('Captcha', array('urlCaptcha' => $urlCaptcha)); ?></div>
    <?php endif; */ ?>

    <div><?php echo CHtml::submitButton('Gửi bình luận'); ?></div>
<?php $this->endWidget(); ?>
<?php endif; ?>

    
<div id="widget-comment-wrapper" class="comment">
    <div class="comment-items content comment-list">

        <?php foreach ($model as $data) : ?>
        <div class="comment-item">

        <!-- li -->
        <div class="comment yt-tile-default " data-author-viewing="" data-author-id="tRaku4BHaQU7l6eU35suNw" data-id="KUMwBzUbQ4mtILz1qG9Fl83FiKWvqLq4uzyD969u71o" data-score="-1">

            <div class="comment-body">
                <div class="content-container">
                    <div class="content">
                        <div class="comment-text" dir="ltr">
                            <p><?php echo $data->content ?></p>
                        </div>
                        <p class="metadata">
                            <span class="author ">
                                <a href="/user/LigaBFH" class="yt-user-name " dir="ltr">
                                    <?php if(!empty($data->creator)){
                                            echo !empty($data->author_name) ? $data->author_name : $data->author_email;
                                        } else {
                                            echo $data->author_email;
                                        };
                                    ?>
                                </a>
                            </span>
                            <span class="time">
                                <?php echo !empty($data->created_time) ? letDate::fuzzy_span(strtotime($data->created_time)) : ''; ?>
                            </span>
                        </p>
                    </div>

                    <!--
                    <div class="comment-actions">
                        <span class="yt-uix-button-group"><button type="button" class="start comment-action-vote-up comment-action yt-uix-button yt-uix-button-default yt-uix-tooltip yt-uix-button-empty" onclick=";return false;" title="Bỏ phiếu Ủng hộ" data-action="vote-up" data-tooltip-show-delay="300" role="button"><span class="yt-uix-button-icon-wrapper"><img class="yt-uix-button-icon yt-uix-button-icon-watch-comment-vote-up" src="//s.ytimg.com/yt/img/pixel-vfl3z5WfW.gif" alt="Bỏ phiếu Ủng hộ"></span></button><button type="button" class="end comment-action-vote-down comment-action yt-uix-button yt-uix-button-default yt-uix-tooltip yt-uix-button-empty" onclick=";return false;" title="Bỏ phiếu Bác" data-action="vote-down" data-tooltip-show-delay="300" role="button"><span class="yt-uix-button-icon-wrapper"><img class="yt-uix-button-icon yt-uix-button-icon-watch-comment-vote-down" src="//s.ytimg.com/yt/img/pixel-vfl3z5WfW.gif" alt="Bỏ phiếu Bác"></span></button></span><span class="yt-uix-button-group"><button type="button" class="start comment-action yt-uix-button yt-uix-button-default" onclick=";return false;" data-action="reply" role="button"><span class="yt-uix-button-content">Trả lời </span></button><button type="button" class="end flip yt-uix-button yt-uix-button-default yt-uix-button-empty" onclick=";return false;" data-button-has-sibling-menu="true" role="button" aria-pressed="false" aria-expanded="false" aria-haspopup="true" aria-activedescendant=""><img class="yt-uix-button-arrow" src="//s.ytimg.com/yt/img/pixel-vfl3z5WfW.gif" alt=""><div class="yt-uix-button-menu yt-uix-button-menu-default" style="display: none;"><ul><li class="comment-action" data-action="share"><span class="yt-uix-button-menu-item">Chia sẻ</span></li><li class="comment-action-remove comment-action" data-action="remove"><span class="yt-uix-button-menu-item">Xóa</span></li><li class="comment-action" data-action="flag"><span class="yt-uix-button-menu-item">Gắn cờ là spam</span></li><li class="comment-action-block comment-action" data-action="block"><span class="yt-uix-button-menu-item">Chặn Người dùng</span></li><li class="comment-action-unblock comment-action" data-action="unblock"><span class="yt-uix-button-menu-item">Bỏ chặn Người dùng</span></li></ul></div></button></span>
                    </div>
                    -->
                    
                </div>
            </div>
        </div>
        <!-- li -->

        </div>
        <?php endforeach; ?>
        <a href="javascript:;" style="display:none" class="comment-url" rel="<?php echo $urlComment; ?>">URL</a>
        <a href="javascript:;" style="display:none" class="comment-offset" rel="<?php echo $offset; ?>">OFFSET</a>
    </div>
    <div class="comment-load"></div>
    <div class="comment-finish"></div>
</div>
