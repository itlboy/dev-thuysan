<script type="text/javascript">
    $(document).ready(function(){
        $('.bt-comment').live('click',function(){
            var content = $('#KitComment_content').val();
            if(content == ""){
                alert('Bạn chưa nhập nội dung comment');
                return false;
            }
            $.ajax({
                type:'POST',
                url:'<?php echo Yii::app()->CreateUrl('comment/ajax/changtext'); ?>',
                data:{content:content},
                success:function(data){
                    eval('data='+data);
                    if(data.error){
                        alert(data.data);
                        return false;
                    }
                }
            });
        });
    });
</script>
<?php
$showCommentForm = true;
if ($requireUser == true) {
    $showCommentForm = (Yii::app()->user->isGuest) ? true : false;
}
?>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'kit-comment-form',
    'action' => $urlComment,
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'onSubmit' => 'ajaxSubmitComment(jQuery(this).attr("id"), jQuery(this).attr("action"), "' . $urlCaptcha .'"); return false',
    )
));
echo $form->hiddenField($modelComment, 'module');
echo $form->hiddenField($modelComment, 'item_id');
echo $form->hiddenField($modelComment, 'reply_to');
echo $form->hiddenField($modelComment, 'lineage');


?>
    <div class="comment-msg"></div>
    <?php if ($this->requireUser == false): ?>
        <?php if(Yii::app()->user->isGuest): ?>
            <div><?php echo $modelComment->getAttributeLabel('author_email'); ?></div>
            <div><input type="text" class="cm-content" maxlength="300" name="KitComment[author_email]" style="width:97%;height:100px" id="KitComment_author_email"></div>
        <?php endif; ?>
<!--        <div>Bình luận</div>-->
        <div><?php echo $form->textArea($modelComment, 'content',array('style' => 'width:97%;height:100px', 'class' => 'cm-content')); ?></div>
        <div><?php echo CHtml::submitButton('Gửi bình luận',array('class' => 'bt-comment')); ?></div>
    <?php else: ?>
        <?php if(!Yii::app()->user->isGuest): ?>
<!--            <div>Bình luận</div>-->
            <div><?php echo $form->textArea($modelComment, 'content',array('style' => 'width:97%;height:100px','class' => 'cm-content')); ?></div>
            <div>
<!--                --><?php //echo CHtml::submitButton('Gửi bình luận',array('class' => 'bt-comment')); ?>
                <button type="submit" class="btn-mini btn-primary btn">Gửi bình luận</button>
            </div>
        <?php endif; ?>
    <?php endif; ?>
<?php $this->endWidget(); ?>
<div id="widget-comment-wrapper" class="comment">
    <div class="comment-items content comment-list">
        <?php foreach ($model as $data) : ?>
        <div class="comment-item">
            <div class="comment yt-tile-default ">
                <div class="comment-body">
                    <div class="content-container">
                        <div class="avatar-members" style="float: left;margin: 5px">
                            <img style="width: 50px !important;" src="<?php echo KitAccount::getAvatar($data['creator'], 'medium'); ?>" />
                        </div>
                        <div class="content">
                            <div class="comment-text" dir="ltr">
                                <p><?php echo nl2br($data->content); ?></p>
                            </div>
                            <p class="metadata">
                            <span class="author ">
                                <a target="_blank" href="http://vailua.vn/wall-<?php echo $data['creator']; ?>.vl" class="yt-user-name " dir="ltr">
                                <?php if(!empty($data->author_name)){
                                    echo $data->author_name;
                                } else {
                                    echo $data->author_email;
                                };
                                ?>
                                </a>
                            </span>
                            <span class="time">
                                <?php echo !empty($data->created_time) ? letDate::fuzzy_span(strtotime($data->created_time)) : ''; ?>
                            </span>
                            </p>
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
        <a href="javascript:;" style="display:none" class="comment-url" rel="<?php echo $urlComment; ?>">URL</a>
        <a href="javascript:;" style="display:none" class="comment-offset" rel="<?php echo $offset; ?>">OFFSET</a>
    </div>
    <div class="comment-load"></div>
    <div class="comment-finish"></div>
</div>
