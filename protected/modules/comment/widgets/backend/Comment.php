<?php

class Comment extends Widget {

    public $module;
    public $itemId;
    public $limit;
        

    public function run() {
        $moduleName = 'comment';
        $modelName = 'KitComment';
        $formID = 'kit_'.$moduleName.'_form';
        $gridID = 'kit_'.$moduleName.'_grid';

        Yii::import('application.modules.comment.models.KitComment');
        $criteria = new CDbCriteria;
        $criteria->condition = 'module = :module AND item_id = :itemId';
        $criteria->params = array(
            ':module' => $this->module,
            ':itemId' => $this->itemId
        );
        
		$dataProvider = new CActiveDataProvider($modelName, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => $this->limit,
            ),
        ));               
        
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => $gridID,
            'dataProvider' => $dataProvider,
            //'filter' => $model,
            'columns' => array(
                array(
                    'class' => 'CCheckBoxColumn',
                    'id' => 'chkIds',
                    'selectableRows' => 2,
                    'htmlOptions' => array('width' => '20px', 'align' => 'center')
                ),                
                array(
                    'name' => 'author_email',
                    'value' => '$data->author_email',
                    'htmlOptions' => array('class' => 'minwidth')
                ),
                'content', 
                array(
                    'filter' => '',
                    'name' => 'approve',
                    'type' => 'raw',
                    'value' => 'BackendFunctions::getBooleanIcon("'.$moduleName.'", "'.$modelName.'", $data->id, "approve", $data->approve, "'.$gridID.'")',
                    'htmlOptions' => array('width' => '50px', 'align' => 'center')
                ),
                array(
                    'filter' => '',
                    'name' => 'status',
                    'type' => 'raw',
                    'value' => 'BackendFunctions::getBooleanIcon("'.$moduleName.'", "'.$modelName.'", $data->id, "status", $data->status, "'.$gridID.'")',
                    'htmlOptions' => array('width' => '50px', 'align' => 'center')
                ),
                /*
                'created_time',
                'updated_time',
                'sorder',
                'status',
                */        
                array(
                    'class' => 'CButtonColumn',
                    'template' => '{gridUpdate} {gridDelete}',
                    'buttons' => array(
                        'gridUpdate' => array(
                            'label' => Yii::t('default', 'Update'),
                            'url' => 'Yii::app()->createUrl("/comment/default/ajaxUpdate", array("id" => $data->id))',
                            'imageUrl'=>Yii::app()->theme->baseUrl.'/images/icons/system/edit_16.png',
                            'options' => array('onclick' => "js:ajaxDialogForm('".$gridID."', 'Cập nhật', jQuery(this).attr('href'), 'auto', 700); return false")
                        ),
                        'gridDelete' => array(
                            'label' => Yii::t('default', 'Delete'),
                            'url' => 'Yii::app()->createUrl("/comment/default/delete", array("id" => $data->id, "ajax" => "'.$gridID.'"))',
                            'imageUrl'=>Yii::app()->theme->baseUrl.'/images/icons/system/delete_16.png',
                            'options' => array('onclick' => "js:ajaxGridDelete('".$moduleName."', '".$modelName."', '".$gridID."', jQuery(this).attr('href'), 'status', 2); return false"),
                        ),
                    ),
                ),
            ),
        ));
        
//        $this->widget('zii.widgets.CListView', array(
//            'dataProvider' => $dataProvider,
//            'itemView' => 'CommentWidget',
//            'ajaxUpdate' => true,
//            'emptyText' => '',
//            'pager' => array(
//                'cssFile' => false,
//                'class' => 'CLinkPager',
//                'header' => '',
//                'firstPageLabel' => '&lt;&lt;',
//                'prevPageLabel' => '&lt;',
//                'nextPageLabel' => '&gt;',
//                'lastPageLabel' => '&gt;&gt;',
//            ),
//            'template' => "{pager}\n{items}",
//        ));
        
    }
    
}
