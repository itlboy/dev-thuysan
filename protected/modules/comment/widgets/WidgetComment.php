<?php

Yii::import('application.modules.comment.models.KitComment');
Yii::import('application.modules.comment.components.Captcha');
Yii::import('application.modules.comment.components.CaptchaAction');
Yii::import('application.modules.cms.models.KitStats');

class WidgetComment extends CWidget {

    public $itemId;
    public $module;
    public $limit = 5;
    public $params = array();
    public $requireUser = false;
    public $approve = 0;
    public $view = '';
//    public $scrollItems = '.comment-items';
//    public $scrollLoading = '.comment-load';
//    public $scrollFinish = '.comment-finish';
    public function init()
    {
        if ($this->view == '') $this->view = __CLASS__;
    }
    public function run() {
        $cache_name = md5(__CLASS__.'_'.$this->itemId.'_'.$this->module);
        $cache = Yii::app()->cache->get($cache_name);
        $urlComment = Yii::app()->createUrl('//comment/frontend/ajax/index', array('module' => $this->module, 'item' => $this->itemId, 'limit' => $this->limit, 'approve' => $this->approve));
        $urlCaptcha = Yii::app()->createUrl('//comment/frontend/ajax/captcha', array('refresh' => '1'));
        $offset = 0;
        $isAjax = (!empty($this->params) AND isset($this->params['offset'])) ? true : false;
        $modelComment = new KitComment;
        $modelComment->module = $this->module;
        $modelComment->item_id = $this->itemId;
        if (isset($_POST['KitComment']) AND !empty($_POST['KitComment'])) {
            $modelComment->attributes = $_POST['KitComment'];
            if(Yii::app()->user->isGuest){
                $modelComment->author_email = $_POST['KitComment']['author_email'];
                $modelComment->author_name = $_POST['KitComment']['author_name'];
                //set cookie
                $arr_Cmm = array('author_email' => $modelComment->author_email , 'author_name' => $modelComment->author_name );
                $cookie = new CHttpCookie("anthor_comm",json_encode($arr_Cmm));
                $cookie->expire = time()+(60*60*24);
                Yii::app()->request->cookies["anthor_comm"] = $cookie;
            } else {
                $account = KitAccount::model()->findByAttributes(array('username' => Yii::app()->user->name));
                $account = $account->attributes;
                $modelComment->creator = $account['id'];
                $modelComment->author_email = $account['email'];
                $modelComment->author_name = $account['username'];

            }

            $modelComment->content = KitComment::changeText($_POST['KitComment']['content']);
            $modelComment->approve = $this->approve;
            $modelComment->status = 1;

            if ($modelComment->validate()) {

                $modelCommentSpam = KitComment::model()->find(array(
                    'select' => 'id, content, creator',
                    'condition' => 'module = :module AND item_id = :item',
                    'params' => array(
                        ':module' => $this->module,
                        ':item' => $this->itemId
                    ),
                    'order' => 'id DESC'
                ));

                if (!Yii::app()->user->isGuest AND $modelCommentSpam !== NULL AND (isset($account) AND !empty($account))) {
                    if ($modelCommentSpam->creator == $account['id']) {  
                        $modelCommentSpamContent = $modelCommentSpam->content
                            . '<br>--------------------------------------------------<br>'
                            . $modelComment->content;
                        if (KitComment::model()->updateByPk($modelCommentSpam->id, array('content' => $modelCommentSpamContent))) {
                            KitStats::updateValue($this->itemId,$this->module,1,'comment');
                            Yii::app()->cache->delete($cache_name);
                            echo CJSON::encode(array(
                                'status' => 'success',
                                'message' => 'Bình luận của bạn đã được lưu'
                            ));
                            Yii::app()->end();
                        }
                    }
                }

                if ($modelComment->save()) {
                    KitStats::updateValue($this->itemId,$this->module,1,'comment');
                    $lineage = ($modelComment->reply_to == 0) ? $modelComment->id : $modelComment->lineage . '-' . $modelComment->id;
                    $modelComment->updateByPk($modelComment->id, array(
                        'lineage' => $lineage,
                        'depth' => substr_count($lineage, '-')
                    ));
                    Yii::app()->cache->delete($cache_name);
                    echo CJSON::encode(array(
                        'status' => 'success',
                        'message' => 'Bình luận của bạn đã được lưu'
                    ));
                }
            } else {
                echo CJSON::encode(array(
                    'status' => 'error',
                    'message' => CHtml::errorSummary($modelComment)
                ));
            }

            Yii::app()->end();
        }

//        if($cache === FALSE)
//        {
            $criteria = new CDbCriteria;
            $criteria->condition = 'module = :module AND item_id = :itemId AND status = 1 AND approve = 1';
            $criteria->params = array(
                ':module' => $this->module,
                ':itemId' => $this->itemId,
            );
            $criteria->order = '(lineage+0) DESC';
            $criteria->limit = $this->limit;
            if ($isAjax) {
                $offset = $this->params['offset'] + $this->limit;
                $criteria->offset = $offset;
            }
            $model = KitComment::model()->findAll($criteria);
//            Yii::app()->cache->set($cache_name,$model);
//        } else {
//            $model = $cache;
//        }

        if ($isAjax AND count($model) == 0)
            Yii::app()->end();
        if ($isAjax) {
            $this->render($this->view, array(
                'model' => $model,
                'modelComment' => $modelComment,
                'urlComment' => $urlComment,
                'urlCaptcha' => $urlCaptcha,
                'isAjax' => $isAjax,
                'offset' => $offset,
                'requireUser' => $this->requireUser
            ));
            Yii::app()->end();
        }

        $baseUrl = Yii::app()->theme->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        $cs->registerScriptFile($baseUrl . '/plugins/scrollPaging/js/jquery.tcScrollPaging.js');
        $cs->registerScriptFile($baseUrl . '/plugins/scrollPaging/js/script.js');
        $cs->registerCssFile($baseUrl . '/plugins/scrollPaging/css/style.css');

        $cs->registerScript('Comment' . $this->module, "
            jQuery(function(){
                jQuery('#widget-comment-wrapper').tcScrollPaging({
                    'heightOffset': 100,
                    'pageOffset': " . $offset . "
                });
            });
        ");
        $this->render($this->view, array(
            'model' => $model,
            'modelComment' => $modelComment,
            'urlComment' => $urlComment,
            'urlCaptcha' => $urlCaptcha,
            'isAjax' => $isAjax,
            'offset' => $offset,
            'requireUser' => $this->requireUser
        ));
    }

}
