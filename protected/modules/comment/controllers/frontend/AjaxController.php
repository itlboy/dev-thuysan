<?php
/**
 * Module: comment
 * Auth:
 * Date: 2012-03-16 11:24:54
 */
class AjaxController extends ControllerFrontend
{
    public function actions() {
        return array(
            'captcha' => array(
                'class' => 'CaptchaAction',
                'urlCaptcha' => '//comment/frontend/ajax/captcha'
            ),
        );
    }
    
	public function actionIndex($module, $item, $limit, $approve)
	{
		$this->widget('application.modules.comment.widgets.WidgetComment', array(
            'params' => $_POST,
            'module' => $module,
            'itemId' => intval($item),
            'limit' => intval($limit),
            'approve' => intval($approve)
        ));
	}
    public function actionChangText(){
        $return = array();
        $content = letArray::get($_POST,'content');
        $irList = array('v1vn', 'xemphimhan', 'onphim', 'vivo', 'phimvang', 'phimhdhay', 'xixam', 'soha', 'kst', 'kites');
        foreach ($irList as $site) {
            if (preg_match('/'.$site.'/i', $content)) {
                $return['error'] = 1;
                $return['data'] = 'Nội dung không cho phép nhắc đến tên website khác ('.$site.')!';
            }
        }
        echo json_encode($return);
        return ;
    }
}