<?php
/**
 * Module: comment
 * Auth:
 * Date: 2012-03-16 11:24:54
 */
class DefaultController extends ControllerFrontend
{
	public function actionIndex($module, $item, $limit)
	{
		$this->widget('application.modules.comment.widgets.WidgetComment', array(
            'params' => $_POST,
            'module' => $module,
            'itemId' => intval($item),
            'limit' => intval($limit)
        ));
	}
    public function actionTest(){
        $text = 'v1vn';
        echo KitComment::changeText($text);
    }
}