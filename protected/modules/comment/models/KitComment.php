<?php
Yii::import('application.modules.comment.models.db.BaseKitComment');
class KitComment extends BaseKitComment{
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    public function attributeLabels() {
        return array_merge(parent::attributeLabels(), array(
            'content' => 'Nội dung comment',
            'author_email' => 'Email người gửi',
            'status' => 'Kích hoạt'
        ));
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
        return CMap::mergeArray(
            parent::rules(),
            array(
                array('author_email', 'email'),
            )
        );
	}
    
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        return CMap::mergeArray(
            parent::relations(),
            array(
                'item' => array(self::BELONGS_TO, 'KitArticle', 'item_id', 'select' => 'id, title'),
            )
        );
	}
    
    protected function beforeValidate() {
        if ($this->isNewRecord) {
            $this->created_time = Common::getNow();
        }

        if (isset($_POST['KitComment'])) {
            if ($this->approve == 1) {
                $this->approved_by = Yii::app()->user->id;
                $this->approved_time = Common::getNow();
            }  
            
            $htmlPurifier = new CHtmlPurifier();
            $htmlPurifier->options = array(
                'Filter.YouTube' => true
            );            
            $this->content = $htmlPurifier->purify($this->content);
        }

        return parent::beforeValidate();
    }
    
    public static function getLastest ($limit = 20){
        $cache_name = md5(__METHOD__ . '_' . $limit);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1 AND t.approve = 1');

            $criteria->order = 't.id DESC';
            $criteria->limit = $limit;
            $result = self::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

    public static function changeText($content) {
        $changeText = array(
            'Địt' => 'địt',
            'đỵt' => 'địt',
            'chung may' => 'các bạn',
            'kon me' => 'con mẹ',
            'kon mẹ' => 'con mẹ',
            'đỉ mẹ mày' => 'dcm',
            'địt con mẹ' => 'dcm',
            'con mẹ' => 'dcm',
            'lồn con mẹ' => 'dcm',
            'địt con mẹ' => 'dcm',
            'chúng mày' => 'các bạn',
            'mày' => 'bạn',
            'con chó' => 'con mèo',
            'fuck' => 'love',
            'địt mẹ' => 'yêu',
            'địt' => 'yêu',
            'lồn mẹ' => 'đáng yêu',
            'lồn' => 'đáng yêu',
            'buồi' => 'đẹp lắm',
            'dcm' => 'ôi cám ơn',
            'đéo' => 'đíu',
            'đĩ' => 'yêu',
            'cứt' => 'cơm',
            'đái' => 'bia',
            'Són' => 'Sone',
            'Sone' => 'Girl Generation',
        );
        foreach ($changeText as $text => $change) {
            $content = preg_replace ('/'.$text.'/i', $change, $content);
        }
        return $content;
    }
    
    public static function collapseContent($str, $limit = 300, $allow = '<br><br/>') {
        $str = strip_tags($str, $allow);
        if (strlen($str) <= $limit) {
            return $str;
        } else {
            if (strpos($str, ' ', $limit) > $limit) {
                $newLimit = strpos($str, ' ', $limit);
                $newStr = substr($str, 0, $newLimit) . '..';
                return $newStr;
            }
            $newStr = substr($str, 0, $limit) . '..';
            return $newStr;
        }
    }

    public static function getCountComment($user_id = NULL){
        if($user_id == NULL) return FALSE;
        $db = Yii::app()->db;
        $where = Common::addWhere('', 'creator='.$user_id);
        $sql = 'SELECT COUNT(id) as total_comment FROM let_kit_comment WHERE '.$where;
        $result = $db->CreateCommand($sql)->queryRow();
        return $result;
    }

    public static function getCommentCount($module = NULL, $item_id = NULL,$user_id = NULL){
        $cache_name = md5(__METHOD__ . '_' . $module.'_'.$item_id.'_'.$user_id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $sql = !empty($user_id) ? 'user_id='.$user_id : '';
            $sql = !empty($module) ? $sql.' module="'.$module.'" ' : $sql;
            $sql = !empty($item_id) ? $sql.' item_id='.$item_id  : $sql;
            $criteria->condition = $sql;
            $result = self::model()->count($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
}