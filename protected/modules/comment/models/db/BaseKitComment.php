<?php
/**
 * This is the model class for table "kit_comment".
 *
 * The followings are the available columns in table 'kit_comment':
 * @property string $id
 * @property string $author_name
 * @property string $author_email
 * @property string $content
 * @property string $module
 * @property integer $item_id
 * @property string $reply_to
 * @property string $lineage
 * @property integer $deep
 * @property integer $creator
 * @property string $created_time
 * @property integer $approve
 * @property string $approved_time
 * @property string $approved_by
 * @property integer $status
 */

class BaseKitComment extends ActiveRecord {
    var $className = __CLASS__;
    
    public $category_id;
    public $categories;
    
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitComment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_comment}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('author_email, content', 'required'),
			array('item_id, deep, creator, approve, status', 'numerical', 'integerOnly'=>true),
			array('author_name', 'length', 'max'=>100),
			array('author_email', 'length', 'max'=>150),
			array('module', 'length', 'max'=>20),
			array('reply_to, approved_by', 'length', 'max'=>10),
			array('lineage', 'length', 'max'=>255),
			array('created_time, approved_time', 'safe'),
            array('categories', 'safe'),
			// The following rule is used by search(). Please remove those attributes that should not be searched.
			array('id, author_name, author_email, content, module, item_id, reply_to, lineage, deep, creator, created_time, approve, approved_time, approved_by, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related class name for the relations automatically generated below.
		return array(
            'creatorUser' => array(KitComment::BELONGS_TO, 'KitAccount', 'creator', 'select' => 'username'),
            'approvedUser' => array(KitComment::BELONGS_TO, 'KitAccount', 'approved_by', 'select' => 'username'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;
        $criteria->select = Common::getFieldInTable(KitComment::model()->getAttributes(), 't.');
        
        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.author_name',$this->author_name,true);
        $criteria->compare('t.author_email',$this->author_email,true);
        $criteria->compare('t.content',$this->content,true);
        $criteria->compare('t.module',$this->module,true);
        $criteria->compare('t.item_id',$this->item_id);
        $criteria->compare('t.reply_to',$this->reply_to,true);
        $criteria->compare('t.lineage',$this->lineage,true);
        $criteria->compare('t.deep',$this->deep);
        $criteria->compare('t.creator',$this->creator);
        $criteria->compare('t.created_time',$this->created_time,true);
        $criteria->compare('t.approve',$this->approve);
        $criteria->compare('t.approved_time',$this->approved_time,true);
        $criteria->compare('t.approved_by',$this->approved_by,true);
        $criteria->compare('t.status',$this->status);
        
        $criteria->group = 't.id';
        
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
            'sort' => array(
                'defaultOrder' => 'id DESC',
            ),
		));
	}
    
//    protected function beforeValidate() {
//        if ($this->isNewRecord) {
//            $this->creator = $this->editor;
//            $this->created_time = $this->updated_time;
//        }
//        return parent::beforeValidate();
//    }
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitComment::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitComment::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitComment::getLastest_'));
        Yii::app()->cache->delete(md5('KitComment::getPromotion_'));
    }
    
    public function parseCategory($data, $delimiter = '') {
        $arr = array();
        foreach ($data as $value) {
            $arr[] = $value->name;                                     
        }
        echo implode($arr, $delimiter);    
    }
                

    ///////////////////////////////// Frontend /////////////////////////////////
    /**
     * treatment
     * @param mixed $data
     * @return array 
     */
    public static function treatment($data)
    {
        $data = CJSON::decode ((CJSON::encode($data))); // Neu la object thi chuyen thanh array
        if (!isset ($data[0]))
            $data = KitComment::treatmentRow($data);
        else {
            foreach ($data as $key => $value) {
                $data[$key] = KitComment::treatmentRow($value);
            }
        }
        return $data;
    }
    
    public static function treatmentRow($row)
    {
//        $urlTitle = Common::convertStringToUrl($row['title']);
//        $row['url'] = Yii::app()->createUrl($urlTitle.'-comment-'.$row['id']);
        return $row;
    }
    
    /**
     * Get Details
     * @param int $id
     * @return object 
     */
    public static function getDetails($id) {
        $id = intval($id);
        $cache_name = md5(__METHOD__ . '_' . $id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $result = KitComment::model()->findByPk($id, array(
                'select' => Common::getFieldInTable(KitComment::model()->getAttributes(), 't.'),
                'with' => array('creatorUser', 'editorUser')
            ));
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }    

}
