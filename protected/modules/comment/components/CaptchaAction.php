<?php

class CaptchaAction extends CCaptchaAction {

    public $testLimit = 3;
    public $width = 120;
    public $height = 50;
    public $padding = 2;
    public $backColor = 0xF2F2F2;
    public $foreColor = 0x0A67B2;
    public $transparent = true;
    public $minLength = 3;
    public $maxLength = 5;
    
    public $urlCaptcha = null;

    /**
     * Runs the action.
     */
    public function run() {
        if (isset($_GET[self::REFRESH_GET_VAR])) {  // AJAX request for regenerating code
            $code = $this->getVerifyCode(true);
            echo CJSON::encode(array(
                'hash1' => $this->generateValidationHash($code),
                'hash2' => $this->generateValidationHash(strtolower($code)),
                // we add a random 'v' parameter so that FireFox can refresh the image
                // when src attribute of image tag is changed
                //'url' => $this->getController()->createUrl($this->getId(), array('v' => uniqid())),
                'url' => Yii::app()->createUrl($this->urlCaptcha, array('v' => uniqid())),
            ));
        }
        else
            $this->renderImage($this->getVerifyCode());
        Yii::app()->end();
    }

}
