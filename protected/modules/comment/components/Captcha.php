<?php

class Captcha extends CCaptcha {

    public $clickableImage = true;
    public $showRefreshButton = false;
    public $urlCaptcha = null;

    /**
     * Renders the CAPTCHA image.
     */
    protected function renderImage() {
        if (!isset($this->imageOptions['id']))
            $this->imageOptions['id'] = $this->getId();

        //$url = $this->getController()->createUrl($this->captchaAction, array('v' => uniqid()));
        // trumcode.com
        $url = ($this->urlCaptcha == null) ? $this->getController()->createUrl($this->captchaAction, array('v' => uniqid())) : Yii::app()->createUrl($this->urlCaptcha, array('v' => uniqid()));

        $alt = isset($this->imageOptions['alt']) ? $this->imageOptions['alt'] : '';
        echo CHtml::image($url, $alt, $this->imageOptions);
    }

    /**
     * Registers the needed client scripts.
     */
    public function registerClientScript() {
        $cs = Yii::app()->clientScript;
        $id = $this->imageOptions['id'];
        //$url = $this->getController()->createUrl($this->captchaAction, array(CaptchaAction::REFRESH_GET_VAR => true));
        // trumcode.com
        $url = ($this->urlCaptcha == null) ? $this->getController()->createUrl($this->captchaAction, array(CaptchaAction::REFRESH_GET_VAR => true)) : Yii::app()->createUrl($this->urlCaptcha, array(CaptchaAction::REFRESH_GET_VAR => true));

        $js = "";
        if ($this->showRefreshButton) {
            $cs->registerScript('Yii.CCaptcha#' . $id, 'dummy');
            $label = $this->buttonLabel === null ? Yii::t('yii', 'Get a new code') : $this->buttonLabel;
            $options = $this->buttonOptions;
            if (isset($options['id']))
                $buttonID = $options['id'];
            else
                $buttonID = $options['id'] = $id . '_button';
            if ($this->buttonType === 'button')
                $html = CHtml::button($label, $options);
            else
                $html = CHtml::link($label, $url, $options);
            $js = "jQuery('#$id').after(" . CJSON::encode($html) . ");";
            $selector = "#$buttonID";
        }

        if ($this->clickableImage)
            $selector = isset($selector) ? "$selector, #$id" : "#$id";

        if (!isset($selector))
            return;

        $js.="
            jQuery('$selector').live('click',function(){
            	jQuery.ajax({
                    url: " . CJSON::encode($url) . ",
                    dataType: 'json',
                    cache: false,
                    success: function(data) {
                        jQuery('#$id').attr('src', data['url']);
                        jQuery('body').data('{$this->captchaAction}.hash', [data['hash1'], data['hash2']]);
                    }
            	});
            	return false;
            });
        ";
        $cs->registerScript('Yii.CCaptcha#' . $id, $js);
    }

}

