<?php

class lastest1 extends Widget
{
    public $view = '';
    public $title = '';
    public $limit = 10;

    public function init()
    {
        if ($this->view == '') $this->view = __CLASS__;
        Yii::import('video.models.KitVideo');
    }
 
    public function run()
    {
        $data = KitVideo::getLastest();
		if (empty ($data)) return FALSE;
        $data = KitVideo::treatment($data);
        $data = array_slice($data, 0, $this->limit);
        $this->render($this->view, array(
            'data' => $data,
        ));
    }
}