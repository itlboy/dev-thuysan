<?php

/**
 * This is the model class for table "{{kit_video}}".
 *
 * The followings are the available columns in table '{{kit_video}}':
 * @property string $id
 * @property string $title
 * @property string $title_real
 * @property string $image
 * @property string $intro
 * @property string $content
 * @property string $actor
 * @property string $director
 * @property integer $country_id
 * @property integer $vote
 * @property double $vote_total
 * @property integer $vote_hit
 * @property double $vote_inweek
 * @property integer $vote_currentweek
 * @property integer $theater
 * @property integer $length
 * @property integer $complete
 * @property integer $comment_count
 * @property integer $view_count
 * @property string $tags
 * @property string $from_time
 * @property string $to_time
 * @property string $layout_id
 * @property string $seo_title
 * @property string $seo_url
 * @property string $seo_desc
 * @property string $creator
 * @property string $created_time
 * @property string $editor
 * @property string $updated_time
 * @property string $sorder
 * @property integer $promotion
 * @property integer $status
 * @property integer $trash
 */
class BaseKitVideo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitVideo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_video}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, title_real', 'required'),
			array('country_id, vote, vote_hit, vote_currentweek, theater, length, complete, comment_count, view_count, promotion, status, trash', 'numerical', 'integerOnly'=>true),
			array('vote_total, vote_inweek', 'numerical'),
			array('title, title_real, image, actor, seo_url', 'length', 'max'=>255),
			array('intro, tags', 'length', 'max'=>500),
			array('director', 'length', 'max'=>100),
			array('layout_id', 'length', 'max'=>45),
			array('seo_title', 'length', 'max'=>70),
			array('seo_desc', 'length', 'max'=>160),
			array('creator, editor', 'length', 'max'=>11),
			array('sorder', 'length', 'max'=>10),
			array('content, from_time, to_time, created_time, updated_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, title_real, image, intro, content, actor, director, country_id, vote, vote_total, vote_hit, vote_inweek, vote_currentweek, theater, length, complete, comment_count, view_count, tags, from_time, to_time, layout_id, seo_title, seo_url, seo_desc, creator, created_time, editor, updated_time, sorder, promotion, status, trash', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('title_real',$this->title_real,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('intro',$this->intro,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('actor',$this->actor,true);
		$criteria->compare('director',$this->director,true);
		$criteria->compare('country_id',$this->country_id);
		$criteria->compare('vote',$this->vote);
		$criteria->compare('vote_total',$this->vote_total);
		$criteria->compare('vote_hit',$this->vote_hit);
		$criteria->compare('vote_inweek',$this->vote_inweek);
		$criteria->compare('vote_currentweek',$this->vote_currentweek);
		$criteria->compare('theater',$this->theater);
		$criteria->compare('length',$this->length);
		$criteria->compare('complete',$this->complete);
		$criteria->compare('comment_count',$this->comment_count);
		$criteria->compare('view_count',$this->view_count);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('from_time',$this->from_time,true);
		$criteria->compare('to_time',$this->to_time,true);
		$criteria->compare('layout_id',$this->layout_id,true);
		$criteria->compare('seo_title',$this->seo_title,true);
		$criteria->compare('seo_url',$this->seo_url,true);
		$criteria->compare('seo_desc',$this->seo_desc,true);
		$criteria->compare('creator',$this->creator,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('editor',$this->editor,true);
		$criteria->compare('updated_time',$this->updated_time,true);
		$criteria->compare('sorder',$this->sorder,true);
		$criteria->compare('promotion',$this->promotion);
		$criteria->compare('status',$this->status);
		$criteria->compare('trash',$this->trash);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
		));
	}
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitVideo::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitVideo::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitVideo::getLastest_'));
        Yii::app()->cache->delete(md5('KitVideo::getPromotion_'));
    }

}
