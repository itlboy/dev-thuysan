<?php

Yii::import('application.modules.video.models.db.BaseKitVideo');
class KitVideo extends BaseKitVideo{
    var $className = __CLASS__;
    
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    public function attributeLabels() {
        return array_merge(parent::attributeLabels(), array(
            'title' => 'Tiêu đề',
            'status' => 'Kích hoạt',
            'trash' => 'Thùng rác',
            'image' => 'Ảnh đại diện',
            'intro' => 'Giới thiệu',
            'content' => 'Nội dung',
            'from_time' => 'Thời gian bắt đầu',
            'to_time' => 'Thời gian kết thúc',
            'promotion' => 'Tiêu điểm',
            'sorder' => 'Thứ tự',
            'tags' => 'Thẻ',
            'categories' => 'Danh mục',
            'actor' => 'Diễn viên',
            'director' => 'Đạo diễn',
            'country_id' => 'Mã nước',
            'vote' => 'Phiếu',
            'vote_total' => 'Tổng số phiếu',
            'vote_inweek' => 'Số phiếu trong tuần',
            'vote_currentweek' => 'Số phiếu hiện tại',
        ));
    }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
        return CMap::mergeArray(
            parent::rules(),
            array(
                                    )
        );
	}
    
    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'creatorUser' => array(self::BELONGS_TO, 'KitAccount', 'creator', 'select' => 'username'),
            'editorUser' => array(self::BELONGS_TO, 'KitAccount', 'editor', 'select' => 'username'),
		);
	} 
    
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;
        $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');
        
		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.title',$this->title,true);
		$criteria->compare('t.title_real',$this->title_real,true);
		$criteria->compare('t.image',$this->image,true);
		$criteria->compare('t.intro',$this->intro,true);
		$criteria->compare('t.content',$this->content,true);
		$criteria->compare('t.actor',$this->actor,true);
		$criteria->compare('t.director',$this->director,true);
		$criteria->compare('t.country_id',$this->country_id);
		$criteria->compare('t.vote',$this->vote);
		$criteria->compare('t.vote_total',$this->vote_total);
		$criteria->compare('t.vote_hit',$this->vote_hit);
		$criteria->compare('t.vote_inweek',$this->vote_inweek);
		$criteria->compare('t.vote_currentweek',$this->vote_currentweek);
		$criteria->compare('t.theater',$this->theater);
		$criteria->compare('t.length',$this->length);
		$criteria->compare('t.complete',$this->complete);
		$criteria->compare('t.comment_count',$this->comment_count);
		$criteria->compare('t.view_count',$this->view_count);
		$criteria->compare('t.tags',$this->tags,true);
		$criteria->compare('t.from_time',$this->from_time,true);
		$criteria->compare('t.to_time',$this->to_time,true);
		$criteria->compare('t.layout_id',$this->layout_id,true);
		$criteria->compare('t.seo_title',$this->seo_title,true);
		$criteria->compare('t.seo_url',$this->seo_url,true);
		$criteria->compare('t.seo_desc',$this->seo_desc,true);
		$criteria->compare('t.creator',$this->creator,true);
		$criteria->compare('t.created_time',$this->created_time,true);
		$criteria->compare('t.editor',$this->editor,true);
		$criteria->compare('t.updated_time',$this->updated_time,true);
		$criteria->compare('t.sorder',$this->sorder,true);
		$criteria->compare('t.promotion',$this->promotion);
		$criteria->compare('t.status',$this->status);
		$criteria->compare('t.trash',$this->trash);
        
        
        $criteria->group = 't.id';
        
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
            'sort' => array(
                'defaultOrder' => 'id DESC',
            ),
		));
	}
    
    protected function beforeValidate() {
        $this->editor = Yii::app()->user->id;
        $this->updated_time = Common::getNow();                    
        if ($this->isNewRecord) {
            $this->creator = $this->editor;
            $this->created_time = $this->updated_time;
            $this->view_count = 0;
            $this->comment_count = 0;
            $this->sorder = 500;
        }
        
        return parent::beforeValidate();
    }
    

                

    ///////////////////////////////// Frontend /////////////////////////////////
    /**
     * treatment
     * @param mixed $data
     * @return array 
     */
    public static function treatment($data)
    {
        $data = CJSON::decode ((CJSON::encode($data))); // Neu la object thi chuyen thanh array
        if (!isset ($data[0]))
            $data = self::treatmentRow($data);
        else {
            foreach ($data as $key => $value) {
                $data[$key] = self::treatmentRow($value);
            }
        }
        return $data;
    }
    
    public static function treatmentRow($row)
    {
        $urlTitle = Common::convertStringToUrl($row['title']);
        $row['url'] = Yii::app()->createUrl($urlTitle.'-video'.$row['id']);
        return $row;
    }
    
    /**
     * Get Details
     * @param int $id
     * @return object 
     */
    public static function getDetails($id) {
        $id = intval($id);
        $cache_name = md5(__METHOD__ . '_' . $id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $result = self::model()->findByPk($id, array(
                'select' => Common::getFieldInTable(self::model()->getAttributes(), 't.'),
                'with' => array('creatorUser', 'editorUser')
            ));
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
    
    public static function getLastest ($category = NULL, $limit = 20){
        $cache_name = md5(__METHOD__ . '_' . $category);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');


            if ($category !== NULL AND $category >= 0) {
                $catListObject = KitCategory::getListInParent('video', $category);
                $catList = array($category);
                foreach ($catListObject as $cat) {
                    $catList[] = $cat->id;
                }
                $criteria->join = "INNER JOIN {{kit_video_category}} t2 ON t.id=t2.video_id";
//                $criteria->group = "t.id";
                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', $catList).')');
            }

            $criteria->order = 't.id DESC';
            $criteria->limit = $limit;
            $result = self::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
    
    public static function getPromotion ($category = NULL, $limit = 20){
        $cache_name = md5(__METHOD__ . '_' . $category);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
            $criteria->condition = Common::addWhere($criteria->condition, 't.promotion = 1');

            if ($category !== NULL AND $category >= 0) {
                $criteria->join = "INNER JOIN {{kit_video_category}} t2 ON t.id=t2.video_id";
                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', KitCategory::getListInParent('video', $category)).')');
            }
            $criteria->order = 't.id DESC';
            $criteria->limit = $limit;
            $result = self::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
}