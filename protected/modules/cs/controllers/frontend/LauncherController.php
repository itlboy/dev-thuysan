<?php
/**
 * Module: cs
 * Auth: nguago@let.vn
 * Website: http://let.vn
 */

//Yii::import('application.modules.account.models.KitAccountStats');
//Yii::import('application.modules.account.models.KitAccountLevel');
Yii::import('application.modules.cs.models.KitCsCharacter');
class LauncherController extends ControllerFrontend
{
//    public $defaultAction = 'index';

    public function actionIndex()
    {
        $this->layout = '//layouts/launcher';
        $model = new KitAccount();
        if(!Yii::app()->user->isGuest)
        {
            $this->render('info',array('model' => $model));
            
        } else {
            $this->render('login',array('model' => $model));
        }
    }
    
    public function actionLogin()
    {
        if(isset($_POST['KitAccount'])) {
            $modelLogin = new LoginFormMembers();
            $modelLogin->attributes = $_POST['KitAccount'];
            if($modelLogin->validate() AND $modelLogin->login() )
            {
				Yii::import('application.modules.cs.models.KitCsCharacter');
				KitCsCharacter::switchRootdbToGamedb(Yii::app()->user->id);
//                KitAccountStats::updateStats(Yii::app()->user->id);
//                KitAccountLevel::updateLevel(Yii::app()->user->id);
//                $this->_clearCache(Yii::app()->user->id);
                $data['status'] = 'success';
            } else {
                $data['status'] = 'error';
            }
            echo $data['status'];
        }
    }
    
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->createUrl('cs/launcher'));
    }
    
    /**
     * code: 0 - Chua dang nhap; 1 - Nhan vat khong ton tai; 2 - Get thanh cong
     */
    public function actionGetCharacter()
    {
        // Kiem tra xem da dang nhap chua
        if(!Yii::app()->user->isGuest) {
            // Lay thong tin Character
            $row = KitCsCharacter::get(Yii::app()->user->id);
            $row = KitCsCharacter::treatment($row);
            // Neu ton tai Character
            if (!empty($row)) {
                $data['character'] = $row['title'];
                $data['password'] = $row['password'];
                $data['code'] = 2; // Get thanh cong
            } else $data['code'] = 1; // Nhan vat khong ton tai
        } else $data['code'] = 0; // Chua dang nhap
        
        echo "[LetCS]\r\n";
        echo "Code=" . $data['code'] . "\r\n";
        if ($data['code'] == 2) {
            echo "Name=" . $data['character'] . "\r\n";
            echo "Pass=" . $data['password'] . "\r\n";
        }
    }

    public function _clearCache($user_id){
        Yii::app()->cache->delete('config_unlimit_'.$user_id);
    }
}