<?php
/**
 * Module: cs
 * Auth: nguago@let.vn
 * Website: http://let.vn
 */

Yii::import('application.modules.cs.models.KitCsCharacter');
class AjaxController extends ControllerFrontend
{

    public function actionIndex()
    {
    }
    
    public function actionCreateCharacter()
    {
        if(!Yii::app()->user->isGuest) {
            if(isset($_POST['character_name'])) {
                $data = KitCsCharacter::get(Yii::app()->user->id);
                if (empty($data)) {
                    // Kiểm tra xem có Character chưa
                    $character = trim($_POST['character_name']);
                    $row = KitCsCharacter::model()->find("title = '" . $character . "'");
                    if (empty($row)) {
                        // Tạo nhân vật
                        $model = new KitCsCharacter;
                        $model->title = $character;
                        $model->password = substr(md5(rand(0,99999999999)), 0, 6);
                        $model->access = 'z';
                        $model->flags = 'a';
                        $model->user_id = Yii::app()->user->id;
                        $model->status = 1;
                        $model->trash = 0;
                        $model->vip = 0;
                        $model->creator = $model->editor = Yii::app()->user->id;
                        $model->created_time = $model->updated_time = Common::getNow();
                        $model->save();
                        Yii::app()->cache->delete(md5('character_'.Yii::app()->user->id));
                        $msg = 'Tạo nhân vật thành công !';
                    } else $msg = 'Tên nhân vật đã tồn tại, hãy chọn tên khác!';
                } else $msg = 'Tài khoản này đã có nhân vật, không thể tạo thêm';
            } else $msg = 'Lỗi không xác định !';
        } else $msg = 'Bạn chưa đăng nhập, hãy đăng nhập và tạo nhân vật.';
        echo $msg;
    }
}