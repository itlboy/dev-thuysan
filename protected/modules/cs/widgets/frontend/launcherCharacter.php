<?php

class launcherCharacter extends Widget {

    public $view = '';
    public $title = '';

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
        Yii::import('cs.models.KitCsCharacter');
    }

    public function run() {
        $data = KitCsCharacter::get(Yii::app()->user->id);
        $data = KitCsCharacter::treatment($data);
        
        if (empty($data)) {
            $this->view .= '_create';
        }
        
        $this->render($this->view, array(
            'data' => $data
        ));
    }
}