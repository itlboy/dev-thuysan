<div class="character">
    <form class="well form-search" id="characterForm" action="/yii-bootstrap/" method="post">
        <div class="input-prepend">
            <span class="add-on">@</span>
            <input class="input-small" placeholder="Tên nhân vật" name="character_name" id="character_name" type="text" style="width: 115px;">
        </div>
        <button class="btn" type="button" name="submit" onclick="ajaxCreateCharacter();">Tạo</button>
    </form>
</div>
<div id="msg" style="text-align: center;"></div>
<script>
function ajaxCreateCharacter() {
    jQuery.ajax({
        'url': '<?php echo Yii::app()->createUrl('//cs/frontend/ajax/createcharacter'); ?>',
        'type':'POST',
        'data':jQuery('#characterForm').serialize(),
//        'dataType':'JSON',
        'beforeSend':function() {
            jQuery('#msg').html('<img src="http://phim.let.vn/themes/frontend/letphim/images/ajax-loader.gif" />').show();
        },
        'success':function(data) {
            alert(data);
            jQuery('#msg').html('').hide();
            location.reload();
        }
    });
}
</script>
