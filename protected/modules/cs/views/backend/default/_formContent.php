
<div class="box grid_16" style="margin-top: 10px;">
    <div class="toggle_container">
        <div class="block">
            <h2 class="section"><?php echo Yii::t('global', 'General'); ?></h2>
            
                
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'title'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'title'); ?>
                    <?php echo $form->error($model, 'title'); ?>
                    <div class="required_tag tooltip hover left" title="<?php echo Yii::t('global', 'Required'); ?>"></div>
                </div>
            </fieldset>
                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'password'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'password'); ?>
                    <?php echo $form->error($model, 'password'); ?>
                </div>
            </fieldset>
                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'user_id'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'user_id'); ?>
                    <?php echo $form->error($model, 'user_id'); ?>
                    <div class="required_tag tooltip hover left" title="<?php echo Yii::t('global', 'Required'); ?>"></div>
                </div>
            </fieldset>
                                    
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'vip'); ?></label>
                <div>
                    <div class="jqui_radios">
                        <?php echo $form->radioButtonList($model, 'vip', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                        <?php echo $form->error($model, 'vip'); ?>
                    </div>
                </div>
            </fieldset>
                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'access'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'access'); ?>
                    <?php echo $form->error($model, 'access'); ?>
                </div>
            </fieldset>
                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'flags'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'flags'); ?>
                    <?php echo $form->error($model, 'flags'); ?>
                </div>
            </fieldset>
                                        

        </div>
    </div>
</div>



