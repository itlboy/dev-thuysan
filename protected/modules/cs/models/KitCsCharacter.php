<?php
/*
 * Quy trình update Rule
 * 1. Dùng hàm getRole để lấy thông tin role
 * 2. Dùng các hàm addRole và delRole để sửa Role
 * 3. Dùng hàm saveRole để lưu toàn bộ dữ liệu
 */


Yii::import('application.modules.cs.models.db.BaseKitCsCharacter');
class KitCsCharacter extends BaseKitCsCharacter{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Lay thong tin nhan vat cua user o data goc
     * @param int $user_id
     * @return object
     */
    public static function get($user_id) {
        $user_id = intval($user_id);
        $cache_name = md5('character_' . $user_id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $result = KitCsCharacter::model()->find('user_id = ' . $user_id);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

    /**
     * Lay thong tin nhan vat cua user o data game
     * @param int $user_id
     * @return object
     */
    public static function getCharacterFromGame($user_id) {
        $command2 = Yii::app()->db2->createCommand();
        $character = $command2->select(array('auth', 'password', 'access', 'flags', 'user_id'))
            ->from('admins t')
            ->where('user_id = :userId', array(':userId' => $user_id))
            ->queryRow();
        return $character;
    }

    public static function setVip($user_id) {
        // 1. Set thong tin vao bang "admins" cua DB phu
        $role = self::getRole($user_id);
        $role = self::addRole($user_id, 'b', $role);
        $role = self::delRole($user_id, 'z', $role);
        self::saveRole($user_id, $role);

        // 2. Set thông tin vao bang "let_kit_cs_character" cua DB chinh
        $model = self::model()->find('user_id = "' . $user_id . '"');
        $model->access = $role;
        $model->vip = 1;
        $model->save();
	}

    /**
     * Bảng chính "let_kit_cs_character"
     * Bảng phụ "admins"
     * Kiểm tra xem user đã có bên bảng phụ hay chưa dựa trên user_id
     * - Nếu chưa có thì thêm mới
     * - Nếu đã có thì cập nhật
     *
     * @param type $user_id
     */
    public static function switchRootdbToGamedb($user_id) {
        $user_id = intval($user_id);

        $characterRoot = Yii::app()->db->createCommand()
            ->select('*')
            ->from('let_kit_cs_character t')
            ->where('user_id = :userId', array(':userId' => $user_id))
            ->queryRow();

        if ($characterRoot == FALSE)
            return FALSE;

        $characterGame = self::getCharacterFromGame($user_id);

        $data = array(
            'auth' => $characterRoot['title'],
            'password' => $characterRoot['password'],
            'access' => $characterRoot['access'],
            'flags' => $characterRoot['flags'],
        );

		$command2 = Yii::app()->db2->createCommand();
        if (empty($characterGame)) {
            $data['user_id'] = $characterRoot['user_id'];
            $command2->insert('admins', $data);
        } else {
            $command2->update('admins', $data, 'user_id = :userId', array(':userId' => $user_id));
        }
    }

    /**
     * Thêm vai trò vào nhân vật dựa trên $user_id
     * @param type $user_id
     * @param type $role Là các chữ cái đại diện cho quyền. b là vip
     * @param type $sourceRole
     * Ví dụ: Quyền hiện giờ là "asjc" thì sau khi add vào sẽ là "asjcb"
     * Nếu quyền đã có "b" rồi, ví dụ: "abhc" thì khi add vào vẫn là "abhc". Tức là sau khi add chỉ cần chắc chắn trong quyền có chữ đó là được
     */
    public static function addRole($user_id, $addRole = 'b', $sourceRole) {
        // Xoa het nhung role muon them vao khoi role goc de khi them vao khong bi trung lap
        $sourceRole = self::delRole($user_id, $addRole, $sourceRole);
        
        // Them role
        $sourceRole .= $addRole;
        
        return $sourceRole;
    }

    /**
     * Loại bỏ vai trò vào nhân vật dựa trên $user_id
     * @param type $user_id
     * @param type $role Là các chữ cái đại diện cho quyền. b là vip
     * @param type $sourceRole
     * Tương tự trường hợp add, chỉ cần sau khi xóa quyền, trong quyền không còn chữ đó là được
     */
    public static function delRole($user_id, $delRole = 'b', $sourceRole) {
        // Chuyen nhung role muon xoa vao thanh dang mang
        $roles = str_split($delRole);
        
        foreach ($roles as $role) {
            $sourceRole = str_replace($role, '', $sourceRole);
        }
        return $sourceRole;
    }
    
    /**
     * Lấy vai trò của nhân vật dựa trên $user_id
     * @param type $user_id
     * @param type $role Có 2 loại Role, "access" và "flags"
     * @return string 
     */
    public static function getRole($user_id, $role = 'access') {
        $character = self::getCharacterFromGame($user_id);
        if (isset($character[$role])) return $character[$role];
        else return '';
    }

    /**
     * Luu role sau khi da xu ly
     * @param type $user_id
     * @param type $value
     * @param type $role 
     */
    public static function saveRole($user_id, $value = '', $role = 'access') {
        // Cap nhat vao bang phu
        Yii::app()->db2->createCommand()->update('admins', array($role => $value), 'user_id = :userId', array(':userId' => $user_id));
    }

}
