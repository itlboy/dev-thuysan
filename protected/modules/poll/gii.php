<?php
return array (
  'moduleName' => 'poll',
  'tablePrefix' => 'let_',
  'main_tableName' => 'let_kit_poll',
  'main_controller' => 'default',
  'main_baseClass' => 'ActiveRecord',
  'hasCategory' => '1',
  'hasOption' => '0',
  'hasComment' => '0',
  'hasMedia' => '0',
  'hasChildren' => '1',
  'children_tableName' => 'let_kit_poll_answer',
  'children_controller' => 'answer',
  'children_baseClass' => 'ActiveRecord',
  'children_fieldCreate' => 'title,sorder,link,status',
  'children_fieldUpdate' => 'title,sorder,link,status,intro,hot,promotion,status,trash',
);