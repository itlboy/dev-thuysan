<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'kit_poll_answer_form',
	'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'onSubmit' => 'js:ajaxSaveForm("KitPollAnswer" ,jQuery(this).attr("id"), jQuery(this).attr("action")); return false',
    )
)); ?>
    <div id="" class="dialog_content">
        <div class="block" style="opacity: 1;">
            <div class="section">
                <h2>Thông tin</h2>
                <!--
                <div class="alert dismissible alert_black">
                    <img width="24" height="24" src="images/icons/small/white/alert_2.png">
                    <strong>All the form fields</strong> can be just as easily used in a dialog.
                </div>
                -->
            </div>

            <a href="javascript:;" id="copyForm" rel="0" onclick="copyForm(jQuery(this))">Add</a>
            <div id="nameOrigin">
                        
                
                                        
                                                
                <fieldset class="label_side">
                    <label><?php echo $model->getAttributeLabel('title'); ?></label>
                    <div>
                        <?php echo CHtml::textField('KitPollAnswer[0][title]', $model->title, array('class' => 'tooltip right text')); ?>
                    </div>
                </fieldset>
                                                    
                
                                        
                                                
                <fieldset class="label_side">
                    <label><?php echo $model->getAttributeLabel('sorder'); ?></label>
                    <div>
                        <?php echo CHtml::textField('KitPollAnswer[0][sorder]', $model->sorder, array('class' => 'tooltip right text')); ?>
                    </div>
                </fieldset>
                                                    
                
                                                
                
                                        
                                
                <fieldset class="label_side">
                    <label><?php echo $model->getAttributeLabel('status'); ?></label>
                    <div>
                        <div class="selector" id="uniform-undefined"><span style="-moz-user-select: none;">-- Select a status --</span>
                            <?php echo CHtml::dropdownList('KitPollAnswer[0][status]', NULL, BackendFunctions::getStatusOptions(), array(
                                'class' => 'select_box',
                                'style' => 'opacity:0',
                            )) ?>
                        </div>
                    </div>
                </fieldset>
                                                                
            </div>
            <div id="nameExtra"></div>

            <div class="button_bar clearfix">
                <button class="dark green close_dialog">
                    <div class="ui-icon ui-icon-check"></div>
                    <span>Submit</span>
                </button>

                <!--
                <button class="dark red close_dialog">
                    <div class="ui-icon ui-icon-closethick"></div>
                    <span>Cancel</span>
                </button>
                -->
            </div>
        </div>
    </div>

<?php $this->endWidget(); ?>

<script>
    function copyForm(obj) {
        nextFormId = parseInt(obj.attr('rel')) + 1;
        html = jQuery('#nameOrigin').html();
        html = html.replace(new RegExp('_0_', 'g'), '_'+nextFormId+'_');
        html = html.replace(new RegExp('\\[0\\]', 'g'), '['+nextFormId+']');
        obj.attr('rel', nextFormId);
        jQuery('#nameExtra').append(html);
    }
</script>
