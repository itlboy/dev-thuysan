<?php
class Answer extends Widget {

    public $itemId;
    public $limit = 100;
    public $params = array();

    public function run() {
        $moduleName = 'poll';
        $modelName = 'KitPollAnswer';
        $gridID = 'kit_poll_answer_grid';

        Yii::import('application.modules.poll.models.KitPollAnswer');
        
        $model = new KitPollAnswer;

        $criteria = new CDbCriteria;
        $criteria->condition = 'item_id = :itemId';
        $criteria->params = array(
            ':itemId' => $this->itemId
        );

        $criteria->order = '';
            $criteria->order .= 'sorder ASC, title ASC';
        
        if (isset($this->params['KitPollAnswer']) AND !empty($this->params['KitPollAnswer'])) {
            $model->attributes = $this->params['KitPollAnswer'];            
            foreach (array_keys($this->params['KitPollAnswer']) as $field) {
                $criteria->compare('t.'.$field, $model->$field, true);
            }
        }

		$dataProvider = new CActiveDataProvider($modelName, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->request->getQuery('pageSize', $this->limit),
            ),
        ));

        // anh xử cái button này nhe
        echo '<button class="green small" onclick="ajaxDialogForm(\'\', \'Thêm mới\', \'' . Yii::app()->createUrl('poll/answer/ajaxCreate', array('item' => $this->itemId)) . '\', \'auto\', \'700\'); return false;">Thêm mới</button>';
        echo '<button class="green small" onclick="jQuery.fn.yiiGridView.update(\''.$gridID.'\', {data: jQuery(this).parent().find(\'.filters\').find(\'input\').serialize()}); return false">Lọc dữ liệu</button>';
        echo '<button class="green small" onclick="jQuery.fn.yiiGridView.update(\''.$gridID.'\'); return false">Reset bộ lọc</button>';

        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => $gridID,
            'dataProvider' => $dataProvider,
            'filter' => $model,
            'ajaxUrl' => Yii::app()->createUrl('//poll/backend/answer/filter', array('item' => $this->itemId)),
            'columns' => array(
                array(
                    'class' => 'CCheckBoxColumn',
                    'id' => 'chkIds',
                    'selectableRows' => 2,
                    'htmlOptions' => array('width' => '20px', 'align' => 'center')
                ),

                array(
                    'name' => 'id',
                    'value' => '$data->id',
                    'htmlOptions' => array('width' => '50px', 'align' => 'center')
                ),
            
                array(
                    'name' => 'title',
                    'value' => '$data->title',
                    'htmlOptions' => array('width' => '200px')
                ),
                                                
                array(
                    'filter' => '',
                    'name' => 'status',
                    'type' => 'raw',
                    'value' => 'BackendFunctions::getBooleanIcon("'.$moduleName.'", "'.$modelName.'", $data->id, "status", $data->status, "'.$gridID.'")',
                    'htmlOptions' => array('width' => '50px', 'align' => 'center')
                ),
            
                /*
                'created_time',
                'updated_time',
                'sorder',
                'status',
                */
                array(
                    'class' => 'CButtonColumn',
                    'header' => CHtml::dropDownList('pageSize',
                        Yii::app()->request->getQuery('pageSize', $this->limit),
                        array(5 => 5, 10 => 10, 20 => 20, 30 => 30, 40 => 40, 50 => 50, 100 => 100, 200 => 200, 500 => 500, 1000 => 1000),
                        array('onchange' => "jQuery.fn.yiiGridView.update('".$gridID."',{ data:{pageSize: jQuery(this).val() }})")
                    ),
                    'template' => '{gridUpdate} {gridDelete}',
                    'buttons' => array(
                        'gridUpdate' => array(
                            'label' => Yii::t('default', 'Update'),
                            'url' => 'Yii::app()->createUrl("/poll/answer/ajaxUpdate", array("id" => $data->id, "ajax" => "'.$gridID.'"))',
                            'imageUrl'=>Yii::app()->theme->baseUrl.'/images/icons/system/edit_16.png',
                            'options' => array('onclick' => "js:ajaxDialogForm('".$gridID."', 'Cập nhật', jQuery(this).attr('href'), 'auto', 700); return false")
                        ),
                        'gridDelete' => array(
                            'label' => Yii::t('default', 'Delete'),
                            'url' => 'Yii::app()->createUrl("/poll/answer/delete", array("id" => $data->id, "ajax" => "'.$gridID.'"))',
                            'imageUrl'=>Yii::app()->theme->baseUrl.'/images/icons/system/delete_16.png',
                            'options' => array('onclick' => "js:ajaxGridDelete('".$moduleName."', '".$modelName."', '".$gridID."', jQuery(this).attr('href'), 'status', 2); return false"),
                        ),
                    ),
                ),
            ),
        ));
    }
}
