<?php

class poll extends Widget {
    public $view = '';
    public $poll_id = NULL;
    public $title = '';
    public $url ='';
    public $captcha = FALSE;
    public $htmlOptions='';
    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
        Yii::import('poll.models.KitPollAnswer');
    }
    public function run() {

        if(!empty($this->poll_id))
        {
            $data= KitPoll::getDetails($this->poll_id);
            if($data !== NULL){
                $data = $data->attributes;
                $model = new KitPollAnswer();
                $data["answer"] = KitPollAnswer::getItem($this->poll_id);
                $data["url"] = !empty($this->url) ? $this->url : '';
                $data["htmlOptions"] = $this->htmlOptions;
                $data["captcha"] = $this->captcha;
                $client = Yii::app()->clientScript;
                $bu = Yii::app()->assetManager->publish(dirname(__FILE__).'/views/assets/');
//                $client->registerScriptFile($bu.'/thickbox.js');
//                $client->registerCssFile($bu.'/thickbox.css');
                $client->registerCssFile($bu.'/statistic.css');
            }
        }else{
            $data = '';
            $model = '';
        }

        $this->render($this->view, array(
            'data' => $data,
            "model"=>$model
        ));
    }

}