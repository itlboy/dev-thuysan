<?php
$moduleName = "poll_list";
$formID = "kit_".$moduleName.'_vote';
?>
<script type="text/javascript">
    $(document).ready(function(){
        $('.btn_vote').live('click',function(){
            var poll_id = $(this).data("id");
            if(!$('#ul_poll_'+poll_id+' :radio').is(':checked'))
            {
                alert('Bạn chưa chọn đáp án ');
                return false;
            }
            else
            {
                var answer_id = $('input[name=answer'+poll_id+']:checked').val();
                var captcha ='';
                var use_captcha;
                if($('#txtCaptcha'+poll_id).length)
                {
                    captcha = $('#txtCaptcha'+poll_id).val();
                    use_captcha = 1;
                    if($('.div_captcha').attr('active') == 0){
                        $('.div_captcha').show();
                        $('.div_captcha').attr('active',1);
                        return false;
                    }
                }else{
                    captcha = '';
                    use_captcha = 0 ;
                }
                $.ajax({
                    type:"POST",
                    url:"<?php echo $data["url"]; ?>",
                    data:{poll_id:poll_id,answer_id:answer_id,use_captcha:use_captcha,captcha:captcha,type:'<?php echo $data["type"]; ?>'},
                    success:function(data){
                        if(data == "false")
                        {
                            $('#thongbao'+poll_id).html('Bạn nhập mã không đúng , mời bạn nhập lại');
                            $('#btnrefresh'+poll_id).click();
                            $('#txtCaptcha'+poll_id).val('');
                            $('#txtCaptcha'+poll_id).focus();
                        }else{
                            $('#show_'+poll_id).html(data).show();
                            $('#let_kit_poll_vote_'+poll_id).remove();
                        }
                    }
                });
            }
        });
        $('.btn_kq').live('click',function(){
            var poll_id  = $(this).data('id');
            $.get('<?php echo Yii::app()->createUrl("/poll/default/boxpoll") ?>',{id:$(this).data('id')},function(data){
                $('#let_kit_poll_vote_'+poll_id).hide();
                $('#show_'+poll_id).html(data).show()
                $('#show_'+poll_id).find('table tr td.poll_result').append("<span class='btn_poll_back' data-id='"+poll_id+"'>Bình chọn</span>");
            });
        });
        $('.btn_poll_back').live('click',function(){
            var poll_id = $(this).data('id');
            $('#let_kit_poll_vote_'+poll_id).show();
            $('#show_'+poll_id).html('').hide();
        });
    });
</script>
<?php
$arr_id = !empty(Yii::app()->request->cookies["poll"]->value) ? json_decode(Yii::app()->request->cookies["poll"]->value) : array();
?>
<?php if(!empty($data)){ ?>
    <?php foreach($data["poll"] as $item) {
        ?>
        <?php if(!in_array($item["data"]["id"],$arr_id))
        {   ?>
        <div class="box_1">
            <div  class="let_kit_poll_vote" id="<?php echo 'let_kit_poll_vote_'.$item["data"]["id"]; ?>">
                <p class="title_poll" id="title_poll_<?php echo $item["data"]["id"]; ?>" data-id="<?php echo $item["data"]["id"]; ?>" ><?php echo !empty($item["data"]["title"]) ? $item["data"]["title"] : ''; ?></p>
                <?php if(count($item["answers"])>0) {  ?>
                    <ul class="ul_poll" id="ul_poll_<?php echo $item["data"]["id"]; ?>">
                        <?php foreach($item["answers"] as $item_answer){ ?>
                            <li style=""><input type="radio" class="answer_item" name="answer<?php echo $item["data"]["id"]; ?>" value="<?php echo $item_answer["id"] ?>" id="answer<?php echo $item_answer["id"]; ?>" /><label for="answer<?php echo $item_answer["id"]; ?>"><?php echo $item_answer["title"]; ?></label></li>
                        <?php } ?>
                    </ul>
                <?php } ?>
                <?php if($data["captcha"]) { ?>
                    <div class="div_captcha" active="0" style="display: none">
                        <?php $this->widget("CCaptcha",array(
                        "buttonLabel" => '',
                        "clickableImage" => true,
                        "buttonOptions"=>array(
                            "class" => "btnrefresh",
                            "id" => "btnrefresh".$item["data"]["id"],
                            "data-id" => $item["data"]["id"],
                        ),
                        "imageOptions"=>array(
                            "class"=>"imgrefresh",
                        ),
                        'captchaAction' => '//poll/default/captcha',
                    )); ?>
                        <div style="clear: both;"></div>
                        <?php
                        echo  CHtml::activeTextField($model,"textCaptcha",array("class"=>"textCaptcha","id"=>"txtCaptcha".$item["data"]["id"]));
                        ?>
                        <p><span class="thongbao" id="thongbao<?php echo $item["data"]["id"]; ?>"></span></p>
                    </div>
                <?php } ?>
                <p style="margin: 9px 0px 0px 0px">
                    <input type="button" class="btn_vote" value="Bình chọn" data-id="<?php echo $item["data"]["id"]; ?>" />
                    <input type="button" class="btn_kq" data-id="<?php echo $item["data"]["id"]; ?>" value="Xem kết quả"  />
                </p>
            </div>
            <div id="show_<?php echo $item["data"]["id"]; ?>" class="show_vote" style="display: none;">
            </div>
        </div>
        <?php
        }
        else
        {
        ?>
<script type="text/javascript">
    $(document).ready(function(){
            $.get('<?php echo Yii::app()->createUrl("/poll/default/boxpoll") ?>',{id:<?php echo $item["data"]["id"] ?>},function(data){
                    $('#show_<?php echo $item["data"]["id"]; ?>').html(data);
            });
    });
</script>
        <div id="show_<?php echo $item["data"]["id"]; ?>" class="show_vote">
        </div>
        <?php
        }
        ?>
    <?php } ?>
<?php } ?>