<?php
$arr_id = !empty(Yii::app()->request->cookies["poll"]->value) ? json_decode(Yii::app()->request->cookies["poll"]->value) : array();
?>

<?php if(in_array($data["id"],$arr_id)) { ?>
<script type="text/javascript">
    $(document).ready(function(){
        $.get("<?php echo Yii::app()->createUrl("/poll/default/boxpoll") ?>",{id:<?php echo $data["id"]; ?>},function(data){
            $('#statistic').html(data).show();
            $('#let_kit_poll_vote').remove();
        });
    });
</script>
<?php }else{ ?>

<?php $url = Yii::app()->request->baseUrl; ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn_vote').live('click',function(){
            if(!$(':radio').is(':checked'))
            {
                //nếu chưa chọn xuất hiện thông báo
                alert('Bạn chưa chọn đáp án ');
                return false;
            }else{
                var poll_id = $('#title_poll').data("id");
                var answer_id = $('input[name=answer]:checked').val();
                var captcha ='';
                var use_captcha;
                if($('#txtCaptcha'+poll_id).length)
                {
                    captcha = $('#txtCaptcha'+poll_id).val();
                    use_captcha = 1;
                }else{
                    captcha = '';
                    use_captcha = 0 ;
                }
                $.ajax({
                    type:"POST",
                    url:"<?php echo $data["url"]; ?>",
                    data:{poll_id:poll_id,answer_id:answer_id,captcha:captcha,use_captcha:use_captcha},
                    success:function(data){
                        if(data == "nosave")
                        {
                            $('.thongbao').html('');
                            $('#btnrefresh').click();
                            $('#txtCaptcha').val('');
                            $('.thongbao').html('Có lỗi');
                        }else if(data == "false"){
                            $('#btnrefresh').click();
                            $('#txtCaptcha').val('');
                            $('#txtCaptcha').focus();
                            $('.thongbao').html('Bạn nhập mã không đúng , mời bạn nhập lại');
                        }else{
                            $('#btnrefresh').click();
                            $('#statistic').html(data).show();
                            $('#let_kit_poll_vote').remove();
                        }
                    }
                });
                return false;
            }
        });
        $('#btn_kq').live('click',function(){
                var question_id = $('#title_poll').data("id");
                $.get("<?php echo Yii::app()->createUrl("/poll/default/boxpoll") ?>",{id:question_id},function(data){
                    $('#statistic').html(data).show().append("<span class='btn_poll_back' data-id='"+question_id+"'>Bình chọn</span>");
                    $('#let_kit_poll_vote').hide();
                });
        });
        $('.btn_back').live('click',function(){
            $('#let_kit_poll_vote').show();
            $('#statistic').html('').hide();
        });
    });
</script>
<?php
    $moduleName = "poll";
    $formID = "kit_".$moduleName.'_vote';
?>
<?php
    $form = $this->beginWidget("CActiveForm",array(
       "id" =>$formID,
       "enableAjaxValidation" =>false,
       "htmlOptions"=>array()
    ));
    echo "\n";
?>


<?php //if(empty(Yii::app()->request->cookies["cookies_vote"]->value)) { ?>
<?php if(!empty($data)){ ?>
<div class="let_kit_poll_vote box_1">
    <p class="title_poll" id="title_poll" data-id="<?php echo $data["id"]; ?>" ><?php echo !empty($data["title"]) ? $data["title"] : ''; ?></p>

    <?php if(count($data["answer"])>0) {  ?>
    <ul class="ul_poll">
        <?php foreach($data["answer"] as $item) { ?>
        <li><input type="radio" class="answer_item" name="answer" value="<?php echo $item["id"] ?>" id="answer<?php echo $item["id"]; ?>" /><label for="answer<?php echo $item["id"]; ?>"><?php echo $item["title"]; ?></label></li>
        <?php } ?>
    </ul>
    <?php }?>
        <?php if($data["captcha"] == TRUE){ ?>
        <div class="div_captcha">
            <?php
            $this->widget("CCaptcha",array(
                "buttonLabel"=>'',
                "clickableImage"=>true,
                "buttonOptions"=>array(
                    "id"=>"btnrefresh",
                ),
                "imageOptions"=>array(
                    "id"=>"imgrefresh"
                )
            ));
            ?>
            <div style="clear: both;"></div>

                <?php
                echo  CHtml::activeTextField($model,"textCaptcha",array("class"=>"textCaptcha","id"=>"txtCaptcha".$data["id"]));
                ?>

            <p><span class="thongbao"></span></p>
        <?php } ?>
        <input type="button" id="btn_vote" value="Bình chọn" />
        <input type="button" id="btn_kq" value="Xem kết quả"  />
    </div>

</div>
<?php } ?>
<?php $this->endWidget(); echo "\n"; ?>
<?php } ?>
<div id="statistic" style="display: none;"></div>


