<?php

class pollList extends Widget {

    /**
     * list : Liet ke dang list
     * random : random tung cai, cai nao da binh chon roi thi khong hien thi nua
     * sorder : liet ke tung cai tu tren xuong duoi, cai nao da binh chon roi thi khong hien thi nua
     */
    public $type = 'list'; // list, random, sorder
    public $view = '';
    public $category = NULL;
    public $title = '';
    public $url = '';
    public $captcha = FALSE;

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
        Yii::import('poll.models.KitPollAnswer');
        Yii::import('poll.models.KitPoll');
        Yii::import('poll.models.KitPollCategory');
    }

    public function run() {
        /**
         * Query poll list with category
         */
//
        $category = new KitPollCategory();
        $poll = new KitPoll();
        $answer = new KitPollAnswer();
        $list_poll_id = $category->getCategory($this->category, $this->type);
        if (empty($list_poll_id))
            return FALSE;
        if ($list_poll_id !== NULL) {

            $arrPoll = array();
            foreach ($list_poll_id as $item) {
                if (is_array($item)) {
                    $arrPoll[] = $item['poll_id'];
                }
//                $data["poll"][$item["poll_id"]] = $poll->getDetails($item["poll_id"])->attributes;
//                $data["poll"][$item["poll_id"]]["answers"] = $answer->getItem($item["poll_id"]);
            }
            if (!empty($arrPoll)) {
                $dataPoll = KitPoll::getListPoll($arrPoll);
            }
            if (!empty($dataPoll)) {
                $dataPoll = CJSON::decode(CJSON::encode($dataPoll));
                foreach ($dataPoll as $item) {
                    $data["poll"][$item["id"]]['data'] = $item;
                    $data["poll"][$item["id"]]["answers"] = $answer->getItem($item["id"]);
                }
            }
            $data["type"] = $this->type;
            $data["url"] = $this->url;
            $data["captcha"] = $this->captcha;
            if (!defined('DEV_MODE'))
                $bu = Yii::app()->assetManager->publish(dirname(__FILE__) . '/views/assets/');
            $client = Yii::app()->clientScript;
            //        $client->registerScriptFile($bu.'/thickbox.js');
            //        $client->registerCssFile($bu.'/thickbox.css');
            $client->registerCssFile($bu . '/statistic.css');
            $this->render($this->view, array("data" => $data, "model" => $answer));
        }
    }

}