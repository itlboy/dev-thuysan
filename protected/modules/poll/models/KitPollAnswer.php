<?php
Yii::import('application.modules.poll.models.db.BaseKitPollAnswer');
class KitPollAnswer extends BaseKitPollAnswer{
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public static  function getItem($poll_id)
    {
        $model = self::model()->findAll("item_id=:item_id order by id",array(":item_id"=>$poll_id));
        $data = array();
        foreach($model as $item)
        {
            $data[] = $item->attributes;
        }
        return $data;
    }
}