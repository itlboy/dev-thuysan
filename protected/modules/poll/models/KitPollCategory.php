<?php

Yii::import('application.modules.poll.models.db.BaseKitPollCategory');
class KitPollCategory extends BaseKitPollCategory{

    var $className = __CLASS__;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function get($itemId) {
        $result = array();
        $data = self::findAll('poll_id=:itemId', array(':itemId' => $itemId));
        foreach ($data as $item2Type)
            $result[] = $item2Type->category_id;
        return $result;
    }

    /**
     * Create
     * @param array $categories
     * @param int $itemId 
     */
    public static function create($categories = array(), $itemId) {
        if (! is_array($categories)) return FALSE;
        foreach ($categories as $categoryId) {
            $item2Cat = new KitPollCategory;
            $item2Cat->category_id = $categoryId;
            $item2Cat->poll_id = $itemId;
            $item2Cat->save();
        }
    }

    /**
     * Delete
     * @param array $categories
     * @param int $itemId 
     */
    public static function del($categories = array(), $itemId) {
        if (! is_array($categories)) return FALSE;
        foreach ($categories as $categoryId) {
            self::model()->deleteAll('category_id=:categoryId AND poll_id=:pollId', array(
                ':categoryId' => $categoryId,
                ':pollId' => $itemId,
            ));
        }
    }

    public static function getCategory($category_id,$type='list')
    {
        $result = array();
        $criteria = new CDbCriteria;
        $criteria->select = Common::getFieldInTable(self::model()->getAttributes());

        if ($category_id !== NULL AND $category_id >= 0) {
            $criteria->condition = Common::addWhere($criteria->condition, 'category_id='.$category_id.'');
        }
        $data = array();
        if(!empty($type))
        {
            switch($type)
            {
                case "sorder":
                    $criteria->limit = 1;
                    break;
                case "random":
                    $criteria->order ='RAND()';
                    $criteria->limit = 1;
                    break;
                default :

                    break;
            }
        }
        $result = self::model()->findAll($criteria);
        if($result !== NULL)
        {
            foreach($result as $item)
            {
                $data[] = $item->attributes;
            }
        }
        return $data;
    }


}