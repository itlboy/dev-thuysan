<?php
Yii::import('application.modules.poll.models.db.BaseKitPoll');
class KitPoll extends BaseKitPoll{
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    public function attributeLabels() {
        return array_merge(parent::attributeLabels(), array(
            'title' => 'Tiêu đề',
            'status' => 'Kích hoạt ?',
            'trash' => 'Thùng rác',
            'image' => 'Ảnh đại diện',
            'intro' => 'Giới thiệu',
            'content' => 'Nội dung',
            'from_time' => 'Thời gian bắt đầu',
            'to_time' => 'Thời gian kết thúc',
            'promotion' => 'Tiêu điểm',
            'sorder' => 'Thứ tự',
            'tags' => 'Thẻ',
            'focus' => 'Tin hot ?',
            'author' => 'Tác giả',
            'source' => 'Nguồn',
            'categories' => 'Danh mục'
        ));
    }

    public static function getListPoll($arrPoll)
    {

        $cache_name = md5(__METHOD__ . '_' . json_encode($arrPoll));
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(KitPoll::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
            if(!empty($arrPoll)){
                $criteria->condition = Common::addWhere($criteria->condition, 't.id IN('.implode(',',$arrPoll).')');
            }
            $criteria->order = 't.id DESC';

            $result = KitPoll::model()->findAll($criteria);

            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

}