<?php

/**
 * This is the model class for table "{{kit_poll_vote}}".
 *
 * The followings are the available columns in table '{{kit_poll_vote}}':
 * @property string $id
 * @property string $item_id
 * @property string $answer_id
 * @property string $ip
 * @property string $creator
 * @property string $created_time
 * @property string $editor
 * @property string $updated_time
 */
class BaseKitPollVote extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitPollVote the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_poll_vote}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item_id, answer_id, creator, editor', 'length', 'max'=>11),
			array('ip', 'length', 'max'=>15),
			array('created_time, updated_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, item_id, answer_id, ip, creator, created_time, editor, updated_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('item_id',$this->poll_id,true);
		$criteria->compare('answer_id',$this->answer_id,true);
		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('creator',$this->creator,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('editor',$this->editor,true);
		$criteria->compare('updated_time',$this->updated_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
		));
	}
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitPollVote::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitPollVote::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitPollVote::getLastest_'));
        Yii::app()->cache->delete(md5('KitPollVote::getPromotion_'));
    }

}
