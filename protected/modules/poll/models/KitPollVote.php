<?php

Yii::import('application.modules.poll.models.db.BaseKitPollVote');
class KitPollVote extends BaseKitPollVote
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    public static  function getDetails($poll_id,$answer_id)
    {
        $model = self::model()->findAll("item_id=:item_id and answer_id=:answer_id",array(":item_id"=>$poll_id,":answer_id"=>$answer_id));
        $data = array();

        if($model !== NULL)
        {
            foreach($model as $item)
            {

                $data[] = $item->attributes;
            }
        }
        return $data;
    }
}