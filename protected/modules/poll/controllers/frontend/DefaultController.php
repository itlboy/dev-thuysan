<?php
/**
 * Module: poll
 * Auth: nguago@let.vn
 * Website: http://let.vn
 */
class DefaultController extends ControllerFrontend
{
	public function actionIndex()
	{
		$this->render('index');
	}
    public function actionPollList()
    {
        $this->render("polllist");
    }
    public function actionUnsetCookies()
    {
        unset(Yii::app()->request->cookies["poll"]);
        $this->redirect(Yii::app()->createUrl("/poll/default/polllist"));
    }
    public function actionAjaxPoll()
    {
        $model = new KitPollAnswer();
        $poll_id = letArray::get($_POST, 'poll_id');
        $answer_id = letArray::get($_POST,'answer_id');
        $captcha = letArray::get($_POST,'captcha');
        $type = letArray::get($_POST,"type");
        $use_captcha = letArray::get($_POST,"use_captcha");
        $check = false;
        if(intval($use_captcha) == 1){
            if(!empty($captcha))
            {
                $model->textCaptcha = $captcha;
                if($model->validate())
                {
                    $check = true;
                }else{
                    $check = false;
                }
            }
        }
        $data = array(
            "ip" => $_SERVER["SERVER_ADDR"],
            "item_id" => $poll_id,
            "answer_id" => $answer_id
        );
        if($check == true or $use_captcha == 0 )
        {
            $vote = $this->saveVote($data);
            if($vote=="true")
            {
                $arr_id = !empty(Yii::app()->request->cookies["poll"]->value) ? json_decode(Yii::app()->request->cookies["poll"]->value) : array() ;
                $arr_id[] = $poll_id;
                //                $cookie = new CHttpCookie("poll",json_encode($arr_id));
                //                $cookie->expire = time()+(60*5);
                //                Yii::app()->request->cookies["poll"] = $cookie;
                $this->redirect(Yii::app()->createUrl("/poll/default/boxpoll",array("id"=>$poll_id)));


            }else{
                echo $vote;
            }
        }else{
            echo "false";
        }
    }
    function saveVote($data)
    {
        if(!Yii::app()->user->isGuest){
            $model = new KitPollVote();
            $model->answer_id = $data["answer_id"];
            $model->item_id = $data["item_id"];
            $model->ip = $data["ip"];
            $model->creator = Yii::app()->user->id;
            $kq = 'nosave';
            if($model->save())
            {
                $kq = 'true';
            }
        }
        $answers = KitPollAnswer::model()->findByPk($data['answer_id']);
        if(!empty($answers)){
            $answers->vote_total +=1;
            $kq = 'nosave';
            if($answers->save())
            {
                $kq = 'true';
            }
        }

        $poll = KitPoll::model()->findByPk($data['item_id']);
        if(!empty($poll)){
            $poll->vote_total +=1;
            if($poll->save()){
                $kq = 'true';
            }
        }

//        $total = 0;
//        $db = Yii::app()->db;
//        $command = $db->createCommand();
//        if(!empty($answers)){
//            foreach($answers as $answer){
//                // Lay gia tri vote voi creator khac NULL
//                $count = KitPollVote::model()->count('answer_id =:answer_id AND item_id =:item_id AND creator IS NOT NULL',array(
//                    ':answer_id' => $answer->attributes['id'],
//                    ':item_id' => $data['item_id'],
//                ));
//                // Lay tat ca vote
//                $total += KitPollVote::model()->count('answer_id =:answer_id AND item_id =:item_id',array(
//                    ':answer_id' => $answer->attributes['id'],
//                    ':item_id' => $data['item_id'],
//                ));
//
//                $update = $command->update('let_kit_poll_answer',array(
//                    'vote_total' => $count,
//                ),'item_id=:itemId AND id=:id',array(
//                    ':itemId' => $data['item_id'],
//                    ':id' => $answer['id'],
//                ));
//                $command->reset();
//            }
//        }
//
//        // luu tong so vote
//        $update = $command->update('let_kit_poll',array(
//            'vote_total' => $total,
//        ),'id=:id',array(
//            ':id' => $data['item_id'],
//        ));
//        $command->reset();

        return $kq;
    }

    public function actionBoxPoll($id)
    {
        $model = new KitPoll();
        $answer = new KitPollAnswer();
        $vote = new KitPollVote();
        $data = $model->findbypk($id);

        if($data !== NULL){
            $data = $data->attributes;
            $data["answer"] = $answer->getItem($id);
            $data["total"] = $data['vote_total'];
            foreach($data["answer"] as $item)
            {
//                $data_vote = $vote->getDetails($id,$item["id"]);
                $data["answer"]["vote"][$item["id"]] =  $item['vote_total'];
//                $data["total"] += count($data_vote);
            }
        }
        $this->layout = false;
        Yii::app()->clientScript->reset();
        $this->render("box",array("data"=>$data));
    }
    public function actions()
    {
        return array(
            "captcha"=>array(
                "class"=>"CCaptchaAction",
                "backColor"=>0xffffff,
                "minLength"=>2,
                "maxLength"=>3,
            )
        );
    }
}