<?php
/**
 * This is the model class for table "kit_date".
 *
 * The followings are the available columns in table 'kit_date':
 * @property string $id
 * @property string $title
 * @property string $image
 * @property string $content
 * @property integer $join_sex
 * @property integer $location_id
 * @property integer $view_count
 * @property integer $join_count
 * @property integer $comment_count
 * @property string $address
 * @property string $from_time
 * @property string $to_time
 * @property string $creator
 * @property string $created_time
 * @property string $sorder
 * @property integer $promotion
 * @property integer $status
 * @property integer $trash
 */

class BaseKitDate extends ActiveRecord {
    var $className = __CLASS__;
    
    public $category_id;
    public $categories;
    
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitDate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_date}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('join_sex, location_id, view_count, comment_count, join_count, promotion, status, trash', 'numerical', 'integerOnly'=>true),
			array('title, image', 'length', 'max'=>255),
			array('creator', 'length', 'max'=>11),
			array('sorder', 'length', 'max'=>10),
			array('content, address, from_time, to_time, created_time', 'safe'),
            array('categories', 'safe'),
			// The following rule is used by search(). Please remove those attributes that should not be searched.
			array('id, title, image, content, join_sex, location_id, view_count, join_count, comment_count, from_time, to_time, creator, created_time, sorder, promotion, status, trash', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related class name for the relations automatically generated below.
		return array(
            'creatorUser' => array(KitDate::BELONGS_TO, 'KitAccount', 'creator', 'select' => 'username'),
            'editorUser' => array(KitDate::BELONGS_TO, 'KitAccount', 'editor', 'select' => 'username'),
            'stats' => array(self::HAS_ONE, 'KitStats', 'item_id',
                'condition' => 'module = "date"'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;
        $criteria->select = Common::getFieldInTable(KitDate::model()->getAttributes(), 't.');
        
        $criteria->compare('t.id',$this->id,true);
        $criteria->compare('t.title',$this->title,true);
        $criteria->compare('t.image',$this->image,true);
        $criteria->compare('t.content',$this->content,true);
        $criteria->compare('t.address',$this->address,true);
        $criteria->compare('t.join_sex',$this->join_sex);
        $criteria->compare('t.location_id',$this->location_id);
        $criteria->compare('t.view_count',$this->view_count);
        $criteria->compare('t.comment_count',$this->comment_count);
        $criteria->compare('t.join_count',$this->join_count);
        $criteria->compare('t.from_time',$this->from_time,true);
        $criteria->compare('t.to_time',$this->to_time,true);
        $criteria->compare('t.creator',$this->creator,true);
        $criteria->compare('t.created_time',$this->created_time,true);
        $criteria->compare('t.sorder',$this->sorder,true);
        $criteria->compare('t.promotion',$this->promotion);
        $criteria->compare('t.status',$this->status);
        $criteria->compare('t.trash',$this->trash);
        
        $criteria->group = 't.id';
        
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
            'sort' => array(
                'defaultOrder' => 'id DESC',
            ),
		));
	}
    
    protected function beforeValidate() {
        if ($this->isNewRecord) {
            $this->creator = Yii::app()->user->id;
            $this->created_time = date('Y-m-d H:i:s');
            $this->view_count = 0;
            $this->comment_count = 0;
        }
        return parent::beforeValidate();
    }
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitDate::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitDate::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitDate::getLastest_'));
        Yii::app()->cache->delete(md5('KitDate::getPromotion_'));
    }
    
    public function parseCategory($data, $delimiter = '') {
        $arr = array();
        foreach ($data as $value) {
            $arr[] = $value->name;                                     
        }
        echo implode($arr, $delimiter);    
    }
                

    ///////////////////////////////// Frontend /////////////////////////////////
    /**
     * treatment
     * @param mixed $data
     * @return array 
     */
    public static function treatment($data)
    {
        $data = CJSON::decode ((CJSON::encode($data))); // Neu la object thi chuyen thanh array
        if (!isset ($data[0]))
            $data = KitDate::treatmentRow($data);
        else {
            foreach ($data as $key => $value) {
                $data[$key] = KitDate::treatmentRow($value);
            }
        }
        return $data;
    }
    
    public static function treatmentRow($row)
    {
        $urlTitle = Common::convertStringToUrl($row['title']);
        $row['url'] = Yii::app()->createUrl($urlTitle.'-date-'.$row['id']);
        return $row;
    }
    
    /**
     * Get Details
     * @param int $id
     * @return object 
     */
    public static function getDetails($id) {
        $id = intval($id);
        $cache_name = md5(__METHOD__ . '_' . $id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $result = KitDate::model()->findByPk($id, array(
                'select' => Common::getFieldInTable(KitDate::model()->getAttributes(), 't.'),
                'with' => array('creatorUser', 'editorUser')
            ));
            Yii::app()->cache->set($cache_name, $result, (60*60)); // Set cache
        } else return $cache;
        return $result;
    }
    
    public static function getLastest ($category = NULL, $limit = 20){
        $cache_name = md5(__METHOD__ . '_' . $category);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(KitDate::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');


            if ($category !== NULL AND $category >= 0) {
                $catListObject = KitCategory::getListInParent('film', $category);
                $catList = array($category);
                foreach ($catListObject as $cat) {
                    $catList[] = $cat->id;
                }
                $criteria->join = "INNER JOIN {{kit_film_category}} t2 ON t.id=t2.film_id";
//                $criteria->group = "t.id";
                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', $catList).')');
            }

            $criteria->order = 't.id DESC';
            $criteria->limit = $limit;
            $result = KitDate::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
    
    public static function getPromotion ($category = NULL, $limit = 20){
        $cache_name = md5(__METHOD__ . '_' . $category);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(KitDate::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
            $criteria->condition = Common::addWhere($criteria->condition, 't.promotion = 1');

            if ($category !== NULL AND $category >= 0) {
                $criteria->join = "INNER JOIN {{kit_film_category}} t2 ON t.id=t2.film_id";
                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', KitCategory::getListInParent('film', $category)).')');
            }
            $criteria->order = 't.id DESC';
            $criteria->limit = $limit;
            $result = KitDate::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

}
