<?php
Yii::import('application.modules.date.models.db.BaseKitDate');
class KitDate extends BaseKitDate{

    public $textCaptcha;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function attributeLabels() {
        return array_merge(parent::attributeLabels(), array(
            'title' => 'Tiêu đề',
            'image' => 'Ảnh của bạn',
            'content' => 'Nội dung',
            'from_time' => 'Thời gian cuộc hẹn diễn ra',
            'join_sex' => 'Cuộc hẹn với Nam/ Nữ',
            'textCaptcha' => 'Mã xác nhận',
        ));
    }

    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return CMap::mergeArray(
            parent::rules(),
            array(
                array("content","required"),
                array("content","length","min"=>20),
                array("textCaptcha","captcha","allowEmpty"=>!CCaptcha::checkRequirements(), 'on' => 'frontend')
            )
        );
    }



}