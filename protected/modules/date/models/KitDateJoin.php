<?php

Yii::import('application.modules.date.models.db.BaseKitDateJoin');
class KitDateJoin extends BaseKitDateJoin
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @static
     * @param $date_id
     * @param null $limit
     * @return array|mixed
     */

    public static function getListJoin($date_id,$limit = NULL){
        $cache_name = md5(__METHOD__ . '_' . $date_id.'_'.$limit);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->addCondition('date_id=:dateId');
            $criteria->params = array(
                ':dateId' => $date_id,
            );
            $criteria->order = 't.id DESC';
            if(!empty($limit)){
                $criteria->limit = $limit;
            }
            $count = self::model()->count($criteria);
            $result = self::model()->findAll($criteria);


            if(!empty($result)){
                $result = CJSON::decode(CJSON::encode($result));
                $result['count'] = $count;
            }
            Yii::app()->cache->set($cache_name, $result, (60*60)); // Set cache
        } else return $cache;
        return $result;
    }

    public static function getListDateOfUser($user_id,$date_id = NULL){
        $cache_name = md5(__METHOD__ . '_' . $user_id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->addCondition('user_id=:userId');
            $criteria->params = array(
                ':userId' => $user_id,
            );
            if(!empty($date_id)){
                $criteria->addInCondition('t.id', array($date_id));
            }
            $criteria->order = 't.id DESC';
            if(!empty($limit)){
                $criteria->limit = $limit;
            }
            $count = self::model()->count($criteria);
            $result = self::model()->findAll($criteria);
            if(!empty($result)){
                $result = CJSON::decode(CJSON::encode($result));
                $result['count'] = $count;
            }
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

    public static function getIdDate($user_id){
        $data = self::getListDateOfUser($user_id);
        $id = array();
        foreach($data as $value){
            if(is_array($value)){
                $id[] = $value['date_id'];
            }
        }
        return $id;
    }
}