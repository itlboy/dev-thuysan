<?php

class listJoin extends Widget
{
    public $view = '';
    public $title = '';
    public $limit = 10;
    public $date_id = NULL;
    public function init()
    {
        if ($this->view == '') $this->view = __CLASS__;
        Yii::import('date.models.KitDate');
        Yii::import('date.models.KitDateJoin');
    }

    public function run()
    {
        $cache_name = md5(__METHOD__ . '_' . $this->date_id.'_'.$this->limit);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE OR TRUE) {
            $comm = Yii::app()->db->createCommand()
                    ->select('u.username, u.phone, u.email, u.yahoo, u.avatar, u.id,
                     u2.username as creator_username, u2.id as creator_id, u2.avatar as creator_avatar, u2.phone as creator_phone, u2.email as creator_email, u2.yahoo as creator_yahoo')
                    ->from('let_kit_date t')
                    ->leftJoin('let_kit_date_join j','t.id = j.date_id')
                    ->leftJoin('let_kit_account u','u.id = j.user_id')
                    ->leftJoin('let_kit_account u2','u2.id = t.creator');
            $comm->where('t.id=:Id',array(
                ':Id' => $this->date_id
            ));
            $result = $comm->queryAll();
            if(empty($result)) return FALSE;
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else $result = $cache;
        $this->render($this->view, array(
            'data' => $result,
        ));
    }
}