<?php

class date_lastest extends Widget
{
    public $category = NULL;
    public $view = '';
    public $title = '';
    public $limit = 10;
    public $type = NULL; // start: sap dien ra | end: da dien ra | me : toi da tham gia
    public $url = '';
    public $index = 0;
    public $user_id = NULL;
    public $page_size = 5; // so record hien thi
    public $tabs = array();

    public function init()
    {
        if ($this->view == '') $this->view = __CLASS__;
        Yii::import('date.models.KitDate');
        Yii::import('date.models.KitDateJoin');
    }

    public function run()
    {
        $page = (isset($_GET['page']) AND !empty($_GET['page'])) ? $_GET['page'] : '';
        $cache_name = md5(__METHOD__ . '_' . $this->type.'_'.$this->category.'_'.$this->limit.'_'.$this->user_id.'_'.$page);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(KitDate::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
            if(!empty($this->user_id)){
                $criteria->condition = Common::addWhere($criteria->condition, 't.creator ='.$this->user_id);
            }
            switch($this->type){
                case 'start':
                    $criteria->condition = Common::addWhere($criteria->condition,'t.from_time  >= NOW()');
                    break;
                case 'end':
                    $criteria->condition = Common::addWhere($criteria->condition,'t.from_time  < NOW()');
                    break;
                case 'me':
                    $iddate = KitDateJoin::getIdDate(Yii::app()->user->id);
                    if(empty($iddate)) return FALSE;
                    $criteria->condition = Common::addWhere($criteria->condition, 't.id IN ('.implode(',', $iddate).')');
                    $criteria->order = 'FIELD(t.id,'.implode(',',$iddate).')';
                    break;
                default:

                    break;
            }

            // Phan trang
            $count = KitDate::model()->count($criteria);
            $page = new CPagination();
            $page->setItemCount($count);
            $page->setPageSize($this->page_size);
            $page->applyLimit($criteria);
//          ------------------

            $criteria->order = 't.id DESC';
            $result = KitDate::model()->findAll($criteria);
            if(empty($result)) return FALSE;
            $result['data'] = KitDate::treatment($result);
            $result['page'] = $page;
            $result['post'] = new KitDate();

            Yii::app()->cache->set($cache_name, $result, (60*60)); // Set cache
        } else $result = $cache;


        if(!Yii::app()->user->isGuest){
            $user = KitAccount::model()->findByPk(Yii::app()->user->id);
            if(empty($user->fullname) OR empty($user->phone) OR empty($user->email) OR empty($user->birthday)){
                Yii::app()->user->setFlash('message','Bạn cần nhập đầy đủ thông tin. Hãy <a href="http://id.let.vn/account/profile/edit.let" rel="nofollow" target="_blank">cập nhật</a> ngay bây giờ');
            }
        }

        $this->render($this->view, array(
            'data' => $result['data'],
            'post' => $result['post'],
            'page' => $result['page'],
            'user' => (isset($user) AND !empty($user)) ? $user : NULL,
        ));
    }
}