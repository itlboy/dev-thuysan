<div  class="clearfix">
    <?php if(!empty($data)): ?>
        <?php foreach($data as $value): ?>
        <?php
            $listJoin = KitDateJoin::getListJoin($value['id']);
            $listUser = array();
            if(!empty($listJoin)){
                foreach($listJoin as $join){
                    $listUser[] = $join['user_id'];
                }
            }
        ?>
        <div class="data-item fl">
            <?php if($value['creator'] != Yii::app()->user->id): ?>
                <?php if(!Yii::app()->user->isGuest): ?>
                    <?php if(in_array(Yii::app()->user->id,$listUser)): ?>
                        <button type="button" class="btn-join btn-mini label join-active" data-toggle="button" style="right: 20px; left: auto;">Đã tham gia</button>
                    <?php else: ?>
                        <button  type="button" <?php echo (Yii::app()->user->hasFlash('message')) ? 'href="#updateInfo"' : 'onclick="return Join('.$value['id'].',this);"'; ?>   class="btn btn-join btn-mini btn-success" data-toggle="modal" data-toggle="button" style="right: 20px; left: auto;">Tham gia</button>
                    <?php endif; ?>
                <?php else: ?>
                <button type="button"  href="#loginModal" data-toggle="modal" class="btn btn-join btn-mini btn-success" data-toggle="button" style="right: 20px; left: auto;">Tham gia</button>
                <?php endif; ?>
            <?php endif; ?>
            <?php if(!Yii::app()->user->isGuest): ?>
            <?php if($value['creator'] == Yii::app()->user->id OR in_array(8,json_decode(Yii::app()->user->getState('group')))): ?>
                <a href="<?php echo Yii::app()->createUrl('date/default/update',array('id' => $value['id'])); ?>"  class="btn btn-join btn-mini" data-toggle="button">Sửa</a>
            <?php endif; ?>
            <?php if(in_array(8,json_decode(Yii::app()->user->getState('group')))): ?>
                <a href="javascript:;" onclick="return deleteItem(<?php echo $value['id'] ?>,this);"  class="btn btn-join btn-mini btn-danger" data-toggle="button" style="left: 60px">Xóa</a>
            <?php endif; ?>
            <?php endif; ?>
            <div class="data-item-child" >
                <div class="item-content">
                    <a title="<?php echo $value['title']; ?>" href="<?php echo $value['url']; ?>" style="border: 0px;" class="link-item" >
                        <img src="<?php echo Common::getImageUploaded('date/medium/' . $value['image']); ?>" width="100%" />
                    </a>
                    <div align="left" class="titledeals2">
                        <a title="<?php echo $value['title']; ?>" href="<?php echo $value['url']; ?>"><?php echo $value['title']; ?></a>
                    </div>
                    <div class="v6TopBorder">
                        <div class="fl">
                            <div class="v6ItemHotPriceSale"><?php echo letDate::fuzzy_span(strtotime($value['from_time'])); ?></div>
                            <div class="v6ItemSaveText"><?php echo date('d/m/Y H:i',strtotime($value['from_time'])); ?></div>
                        </div>
                        <div align="right" class="fr">
                            <div  class="v6ItemHotPriceSale" style="text-align: right">
                                <span style="color: #888; " class="join_count_<?php echo $value['id']; ?>"><?php echo (isset($listJoin['count']) AND !empty($listJoin['count'])) ? $listJoin['count'] : 0; ?></span>
                            </div>
                            <div class="v6ItemSaveText">Người đã tham gia</div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>
        </div>
        <?php endforeach; ?>

    <?php $this->widget("CLinkPager",array('pages'=>$page)); ?>
    <?php endif; ?>
</div>
<script type="text/javascript">
    function deleteItem(id,obj){
        if(confirm('Bạn muốn xóa ?')){
            $.ajax({
                type:'POST',
                url:'<?php echo Yii::app()->createUrl('date/ajax/delete'); ?>',
                data:{id:id},
                success:function(data){
                    if(data == 'true')
                    {
                        $(obj).parent().remove();
                    }
                }
            });
        }
    }

    function Join(id,obj){
        $.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl('date/ajax/join'); ?>',
            data:{id:id},
            beforeSend:function(){
                $(obj).attr('disabled','disabled');
            },
            success:function(data){
                eval('data='+data);
                if(data.result == 'true'){
                    $(obj).removeClass('btn-success btn').addClass('label join-active');
                    $(obj).html('Đã tham gia');
                    $(obj).removeAttr('disabled');
                    $('.join_count_'+id).html(data.count);
                    $(obj).removeAttr('onclick');

                } else {
                    alert('Không thể tham gia. Bạn hãy thử lại');
                }
            }
        });
    }
</script>
<div class="modal" style="display: none" id="updateInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Thông báo</h3>
    </div>
    <div class="modal-body">
        <p>Để tham gia cuộc hẹn. Bạn cần cập nhật đầy đủ thông tin liên lạc: điện thoại, họ tên,...</p>
        <div style="margin-top: 10px;">
            <div class="alert alert-block alert-error fade in">
                <p><b>Họ và tên:</b> <?php echo !empty($user->fullname) ? $user->fullname : '<a href="http://id.let.vn/account/profile/edit.let" rel="nofollow" target="_blank">Chưa cập nhật</a>'; ?></p>
                <p><b>Điện thoại:</b> <?php echo !empty($user->phone) ? $user->phone : '<a href="http://id.let.vn/account/profile/edit.let" rel="nofollow" target="_blank">Chưa cập nhật</a>'; ?></p>
                <p><b>Email:</b> <?php echo !empty($user->email) ? $user->email : '<a href="http://id.let.vn/account/profile/edit.let" rel="nofollow" target="_blank">Chưa cập nhật</a>'; ?></p>
                <p><b>Ngày sinh:</b> <?php echo !empty($user->birthday) ? date('d/m/Y',strtotime($user->birthday)) : '<a href="http://id.let.vn/account/profile/edit.let" rel="nofollow" target="_blank">Chưa cập nhật</a>'; ?></p>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Đóng</button>
        <a target="_blank" rel="nofollow" class="btn btn-primary" href="http://id.let.vn/account/profile/edit.let">Đồng ý</a>
    </div>
</div>