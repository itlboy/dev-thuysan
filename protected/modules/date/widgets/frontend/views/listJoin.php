<p class="line-date">Tham gia</p>
<div class="join_info">
    <ul>
        <?php if(!empty($data[0])): ?>
        <li style="border-bottom: 1px solid #e3e3e3;padding: 0px 0px;">
            <div style="float: left; width: 100%; overflow: hidden;padding: 5px 0px; background: #FFF8F2">
                <img src="<?php echo (!empty($data[0]['creator_avatar'])) ? Common::getImageUploaded('account/large/'.$data[0]['creator_avatar']) : 'http://forum.let.vn/uc_server/avatar.php?uid='.$data[0]['creator_id'].'&size=big'; ?>" width="100" alt="">

                <div class="date-item">
                    <span class="first-date">Tham gia:</span>
                    <div class="last-date"><?php echo (!empty($data[0]['creator_username'])) ? $data[0]['creator_username'] : 'Chưa có thông tin'; ?></div>
                </div>
                <div class="date-item">
                    <span class="first-date">Email: </span>
                    <div class="last-date"><?php echo (!empty($data[0]['creator_email'])) ? $data[0]['creator_email'] : 'Chưa có thông tin'; ?></div>
                </div>
                <div class="date-item">
                    <span class="first-date">Điện thoại: </span>
                    <div class="last-date"><?php  echo (!empty($data[0]['creator_phone'])) ? $data[0]['creator_phone'] : 'Chưa có thông tin'; ?></div>
                </div>
                <div class="date-item">
                    <span class="first-date">Yahoo: </span>
                    <div class="last-date"><?php echo (!empty($data[0]['creator_yahoo'])) ? $data[0]['creator_yahoo'] : 'Chưa có thông tin'; ?></div>
                </div>
            </div>
        </li>
        <?php endif; ?>
    </ul>
</div>
<div class="join_info">
    <ul>
        <?php foreach($data as $item): ?>
        <?php if(!empty($item['username'])): ?>
            <li>
                <img src="<?php echo (!empty($item['avatar'])) ? Common::getImageUploaded('account/large/'.$item['avatar']) : 'http://forum.let.vn/uc_server/avatar.php?uid='.$item['id'].'&size=big'; ?>" width="100" alt="">

                <div class="date-item">
                    <span class="first-date">Tham gia:</span>
                    <div class="last-date"><?php echo (!empty($item['username'])) ? $item['username'] : 'Chưa có thông tin'; ?></div>
                </div>
                <div class="date-item">
                    <span class="first-date">Email: </span>
                    <div class="last-date"><?php echo (!empty($item['email'])) ? $item['email'] : 'Chưa có thông tin'; ?></div>
                </div>
                <div class="date-item">
                    <span class="first-date">Điện thoại: </span>
                    <div class="last-date"><?php  echo (!empty($item['phone'])) ? $item['phone'] : 'Chưa có thông tin'; ?></div>
                </div>
                <div class="date-item">
                    <span class="first-date">Yahoo: </span>
                    <div class="last-date"><?php echo (!empty($item['yahoo'])) ? $item['yahoo'] : 'Chưa có thông tin'; ?></div>
                </div>
            </li>
        <?php endif; ?>
        <?php endforeach; ?>
    </ul>
</div>