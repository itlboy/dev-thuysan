<?php
return array (
  'moduleName' => 'date',
  'tablePrefix' => 'let_',
  'main_tableName' => 'let_kit_date',
  'main_controller' => 'default',
  'main_baseClass' => 'ActiveRecord',
  'hasCategory' => '0',
  'hasOption' => '0',
  'hasComment' => '1',
  'hasMedia' => '0',
  'hasChildren' => '1',
  'children_tableName' => 'let_kit_date_join',
  'children_controller' => 'join',
  'children_baseClass' => 'ActiveRecord',
  'children_fieldCreate' => '',
  'children_fieldUpdate' => '',
);