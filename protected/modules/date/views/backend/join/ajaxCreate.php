<?php
$form = $this->beginWidget('CActiveForm', array(
	'id' => 'kit_date_join_form',
	'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'onSubmit' => 'js:ajaxSaveForm("KitDateJoin" ,jQuery(this).attr("id"), jQuery(this).attr("action")); return false',
    )
)); ?>
    <div id="" class="dialog_content">
        <div class="block" style="opacity: 1;">
            <div class="section">
                <h2>Thông tin</h2>
                <!--
                <div class="alert dismissible alert_black">
                    <img width="24" height="24" src="images/icons/small/white/alert_2.png">
                    <strong>All the form fields</strong> can be just as easily used in a dialog.
                </div>
                -->
            </div>

            <a href="javascript:;" id="copyForm" rel="0" onclick="copyForm(jQuery(this))">Add</a>
            <div id="nameOrigin">
                        
                
                                                            
            </div>
            <div id="nameExtra"></div>

            <div class="button_bar clearfix">
                <button class="dark green close_dialog">
                    <div class="ui-icon ui-icon-check"></div>
                    <span>Submit</span>
                </button>

                <!--
                <button class="dark red close_dialog">
                    <div class="ui-icon ui-icon-closethick"></div>
                    <span>Cancel</span>
                </button>
                -->
            </div>
        </div>
    </div>

<?php $this->endWidget(); ?>

<script>
    function copyForm(obj) {
        nextFormId = parseInt(obj.attr('rel')) + 1;
        html = jQuery('#nameOrigin').html();
        html = html.replace(new RegExp('_0_', 'g'), '_'+nextFormId+'_');
        html = html.replace(new RegExp('\\[0\\]', 'g'), '['+nextFormId+']');
        obj.attr('rel', nextFormId);
        jQuery('#nameExtra').append(html);
    }
</script>
