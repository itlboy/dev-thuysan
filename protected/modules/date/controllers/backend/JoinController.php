<?php
class JoinController extends ControllerBackend
{
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id) {
		$model=KitDateJoin::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

    public function actionIndex() {
        $model = new KitDateJoin('search');
        $model->unsetAttributes();  // clear any default values

        if (isset($_GET['KitDateJoin'])) {
            $model->attributes = $_GET['KitDateJoin'];
        }

        // page size drop down changed
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$model = new KitDateJoin;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$this->save($model);

		$this->render('edit',array(
			'model'=>$model
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id) {
		$model=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

            $this->save($model);

		$this->render('edit',array(
			'model' => $model
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionInfo($id) {
        $model = KitDateJoin::getDetails($id);

		$this->render('info',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
                
            echo json_encode(array('status' => 'success'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

    
	protected function save($model, $categoriesOld = NULL) {
		if(isset($_POST['KitDateJoin'])) {



            $model->attributes = $_POST['KitDateJoin'];

                
            if ($model->validate()) {
                if($model->save()) {
                    if (Yii::app()->request->getPost('apply') == 0)
                         $this->redirect(array('index'));
                    else $this->redirect(array('update', 'id' => $model->id));
                }
            }
		}
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model) {
		if(isset($_POST['ajax']) && $_POST['ajax']==='kit-date-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    
            /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionAjaxCreate($item) {
        $model = new KitDateJoin;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['KitDateJoin']) AND !empty($_POST['KitDateJoin'])) {           
            foreach (array_keys($_POST['KitDateJoin']) as $i) {
                if (isset($_POST['KitDateJoin'][$i])) {
                    $modelSave = new KitDateJoin;
                    $modelSave->attributes = $_POST['KitDateJoin'][$i];
                    $modelSave->item_id = intval($item);
                    if ($modelSave->validate()) {                        
                        $modelSave->save();
                    }
                }
            }

            echo json_encode(array('status' => 'success'));
            Yii::app()->end();
        }

        $this->layout = false;
        $this->render('ajaxCreate', array(
            'model' => $model,
        ));
    }    
    
    
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionAjaxUpdate($id) {
		$model = $this->loadModel($id);
        
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
       
        if (isset($_POST['KitDateJoin'])) {           
            $model->attributes = $_POST['KitDateJoin'];
            if ($model->validate()) {                        
                $model->save();
            }
            
            echo json_encode(array('status' => 'success'));
            Yii::app()->end();
        }

        $this->layout = false;
        $this->render('ajaxUpdate', array(
            'model' => $model,
        ));
	}
    
    public function actionFilter($item) {
        $this->widget('application.modules.date.widgets.backend.Join', array('itemId' => intval($item), 'params' => $_GET));
    }    
        
}
