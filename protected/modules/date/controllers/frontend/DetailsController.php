<?php
Yii::import('cms.models.KitStats');
class DetailsController extends ControllerFrontend{

    public function actionIndex(){
        $id = Yii::app()->request->getParam('id', NULL);
        $data = KitDate::getDetails($id);
        if(empty($data) OR ($data->status == 0) OR ($data->trash ==1)){
            $this->redirect(Yii::app()->getBaseUrl(true));
        }
        $user = $data->creatorUser;
        $user = CJSON::decode(CJSON::encode($user));
        $data = KitDate::treatment($data);
        $join = KitDateJoin::getListJoin($id);

        if(isset($data['id'])){
            $stats = KitStats::getDetails('date',$data['id']);
            KitStats::setStats('date',$id);
        }
        // Meta
        if (isset($data['title'])) $this->pageTitle = $data['title'];

        if(!Yii::app()->user->isGuest){
            $user = KitAccount::model()->findByPk(Yii::app()->user->id);
            if(empty($user->fullname) OR empty($user->phone) OR empty($user->email) OR empty($user->birthday)){
                Yii::app()->user->setFlash('message','Bạn cần nhập đầy đủ thông tin. Hãy <a href="http://id.let.vn/account/profile/edit.let" rel="nofollow" target="_blank">cập nhật</a> ngay bây giờ');
            }
        }

        $this->render('index',array(
            'data' => $data,
            'user' => $user,
            'join' => $join,
            'stats' => !empty($stats) ? $stats : NULL,
            'user' => (isset($user) AND !empty($user)) ? $user : NULL,
        ));
    }
}