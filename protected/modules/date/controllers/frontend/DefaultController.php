<?php
/**
 * Module: date
 * Auth: nguago@let.vn
 * Website: http://let.vn
 */
Yii::import('application.modules.location.models.KitLocation');
class DefaultController extends ControllerFrontend
{

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model=KitDate::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

	public function actionIndex()
	{
		$this->render('index',array(

        ));
	}

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        if(Yii::app()->user->isGuest){
            $this->redirect(Yii::app()->getBaseUrl());
        }

        $model = new KitDate;
        $data = KitAccount::model()->findByPk(Yii::app()->user->id);
        if(empty($data)){
            $this->redirect(Yii::app()->getBaseUrl());
        }
        if(empty($data->fullname) OR empty($data->phone) OR empty($data->email) OR empty($data->birthday)){
            Yii::app()->user->setFlash('mess','Bạn cần nhập đầy đủ thông tin. Hãy <a href="http://id.let.vn/account/profile/edit.let" rel="nofollow" target="_blank">cập nhật</a> ngay bây giờ');
        }



        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        $this->save($model);

        $this->render('edit',array(
            'model'=>$model,
            'data' => $data,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model=$this->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $data = KitAccount::model()->findByPk(Yii::app()->user->id);
        $this->save($model);

        $this->render('edit',array(
            'model' => $model,
            'data' => $data,
        ));
    }


    protected function save($model, $categoriesOld = NULL) {
        if(isset($_POST['KitDate'])) {
            // Upload image
            if ($_POST['KitDate']['image'] !== NULL AND $_POST['KitDate']['image'] !== '') {
                $_POST['KitDate']['image'] = Common::createThumb($this->module->getName() , $_POST['KitDate']['image'], letArray::get($_POST['KitDate'], 'title', ''));

                // Remove Image
                Common::removeImage($this->module->id, $model->image);
            } else unset ($_POST['KitDate']['image']);

            $model->textCaptcha = $_POST['KitDate']['textCaptcha'];
            $model->attributes = $_POST['KitDate'];
            $model->scenario = ('frontend');

            if ($_POST['KitDate']['from_time'] == NULL OR $_POST['KitDate']['from_time'] == '') {

                unset($_POST['KitDate']['from_time']);
            } else {
                $time = letArray::get($_POST['KitDate'],'from_time');
                $time = strtotime($time);
                if(($time - time()) < (60*60*24)){
                    Yii::app()->user->setFlash('error','Thời gian hẹn phải sau 24 tiếng so với mốc giờ hiện tại.');
                    return;
                }
            }

            if ($model->validate()) {
                if($model->save()) {
//                    if (Yii::app()->request->getPost('apply') == 0)
                        $this->redirect(array('index'));
//                    else $this->redirect(array('update', 'id' => $model->id));
                }
            }
        }
    }

    public function actions()
    {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                "backColor"=>0xffffff,
                "minLength"=>2,
                "maxLength"=>3,
            ),
        );
    }
}