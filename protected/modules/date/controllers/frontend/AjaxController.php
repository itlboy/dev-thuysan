<?php
/**
 * Module: classiads
 * Auth:
 * Date: 2012-07-19 12:52:51
 */
class AjaxController extends ControllerFrontend
{
	public function actionDelete()
	{
        $id = letArray::get($_POST,'id');
        $model = KitDate::model()->findByPk($id);
        if($model->delete()){
            Yii::app()->cache->delete(md5('date_lastest::run___10__'));
            Yii::app()->cache->delete(md5('date_lastest::run_start__10__'));
            Yii::app()->cache->delete(md5('date_lastest::run_end__10__'));
            Yii::app()->cache->delete(md5('date_lastest::run___10_'.Yii::app()->user->id.'_'));
            echo 'true';
            return;
        }
	}

    public function actionJoin(){
        $id = letArray::get($_POST,'id');
        $model = KitDateJoin::getListDateOfUser(Yii::app()->user->id,$id);
        if(!empty($model)){
            $join = KitDateJoin::getListJoin($id);
            echo json_encode(array(
                'result' => 'true',
                'count' => $join['count'],
            ));
            return;
        }
        $model = new KitDateJoin();
        $model->user_id = Yii::app()->user->id;
        $model->date_id = $id;
        $model->created_time = date('Y-m-d H:i:s');

        if($model->save()){
            $join = KitDateJoin::getListJoin($id);
            echo json_encode(array(
                'result' => 'true',
                'count' => $join['count'],
            ));

        } else {
            echo json_encode(array(
                'result' => 'false',
            ));
        }
    }

    public function actionLoadDate(){
        $id = letArray::get($_POST,'id');
        $this->widget('date.widgets.frontend.listJoin',array(
            'date_id' => $id,
        ));
    }

    public function actionDateNew(){
        $limit = letArray::get($_GET,'limit');
        $view = letArray::get($_GET,'view');
        $this->widget('date.widgets.frontend.date_lastest_new',array(
            'view' => $view,
            'limit' => $limit,
        ));
    }
}