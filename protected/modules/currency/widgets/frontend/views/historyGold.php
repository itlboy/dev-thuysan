                <div class='row-fluid'>
                    <div class='span12 box bordered-box green-border' style='margin-bottom:0;'>
                        <div class='box-header green-background'>
                            <div class='title'><?php echo $this->title ; ?> (<a href='javascript:void(0):'>chi tiết</a>)</div>
                            <div class='actions'>
                                <a href="#" class="btn box-remove btn-mini btn-link"><i class='icon-remove'></i>
                                </a>
                                <a href="#" class="btn box-collapse btn-mini btn-link"><i></i>
                                </a>
                            </div>
                        </div>
                        <div class='box-content box-no-padding'>
                            <table class='table table-striped' style='margin-bottom:0;'>
                                <thead>
                                    <tr>
                                        <th>Thời gian</th>
                                        <th>Dịch vụ</th>
                                        <th>Số <?php echo KitCmsConfig::getValue('currency_gold_name'); ?> đã dùng</th>
                                        <th>Trạng thái</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($rows as $row): ?>
                                    <tr>
                                        <td>10/05/2013 20:34</td>
                                        <td>Đảo Hải Tặc</td>
                                        <td> <?php echo $row['gold'] . ' ' . KitCmsConfig::getValue('currency_gold_name'); ?></td>
                                        <td>
                                            <span class='label label-success'>Thành công</span>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
