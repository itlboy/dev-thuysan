<?php

class historyGold extends Widget {

    public $view = '';
    //public $category = NULL;
    public $title = 'Lịch sử quy đổi';
    public $limit = 10;
    public $page = 1; // $offset = $limit * ($page - 1) Nếu $page > 1 thì thêm điều kiện cho offset. Không dùng CPagination
    public $user_id;

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
        Yii::import('currency.models.KitCurrencyGold');
    }

    public function run() {
        $criteria = new CDbCriteria;
        $criteria->select = 't.*';
        $criteria->condition = 't.user_id = ' . $this->user_id;
//        $criteria->addCondition ('creator = ' . $this->options['creator']);
        $criteria->order = 't.id DESC';
        $criteria->limit = $this->limit;

        $assign['rows'] = KitCurrencyGold::model()->findAll($criteria);
        if ($assign['rows'] !== array()) {
            $assign['rows'] = KitCurrencyGold::treatment($assign['rows']);
            $this->render($this->view, $assign);
        }
    }
}
