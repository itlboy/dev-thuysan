<?php
Yii::import('application.modules.currency.models.KitCurrencyCard');
Yii::import('application.modules.currency.models.KitCurrencyHistory');
Yii::import('application.modules.currency.models.KitCurrencyCardfail');
Yii::import('application.modules.account.models.KitAccountStats');
Yii::import('application.modules.account.models.KitAccountLevel');

class WebserviceController extends ControllerFrontend
{
    public function actions()
    {
        return array(
            'quote'=>array(
                'class'=>'CWebServiceAction',
            ),
        );
    }

    /**
     * @param string $transaction_info
     * @param string $order_code
     * @param string $payment_id
     * @param string $payment_type
     * @param string $secure_code
     * @return string
     * @soap
     */

    function UpdateOrder($transaction_info, $order_code, $payment_id, $payment_type, $secure_code)
    {
        $error = 'Chưa xác minh';

        $pascode = 'songkinhthay'; // mật khẩu đăng ký merchant

        $md5 = $transaction_info." ".$order_code." ".$payment_id." ".$payment_type." ".$pascode;
        //$mahoa=md5($md5);
        $model = KitCurrencyBanking::model()->find('checknum="'.$order_code.'"');

        //
        $money = $model->money;
        $user_id = $model->creator;
        $type = $model->type;
        $payment = $model->payment;
        $gold = KitCmsConfig::convertMoneyToGold($money,$payment);

        $check = FALSE;

        // Kiem tra ma gia dich
        if (md5($md5) == strtolower($secure_code)) {
//        if (md5($md5) != strtolower($secure_code)) {
            $model->status = 1;
            $model->payment_type = $payment_type;
            $model->payment_id = $payment_id;
            $model->transaction_info = $transaction_info;
            $model->checksum = $secure_code;
            $error = 'Thành công';
            $check = TRUE;

            if($type == 'film_unlimit'){
            // Luu giao dich vao History
                $history = new KitCurrencyHistory();
                $history->money = $money;
                $history->gold = $gold;
                $history->user_id = $user_id;
                $history->content = 'Nạp tiền vào tài khoản.';
                $history->payment = $payment;
                $history->time = date('Y-m-d H:i:s');
                $history->save();
            }

        } else {
            $error = 'Tham số truyền bị thay đổi';
            $check = FALSE;
        }
        $model->content = $error;
        $model->save();

        // Cap nhat  history
        if($check){
            $message = 'Nạp tiền vào tài khoản.';
            KitCurrencyHistory::updateConfig($user_id,$type,$gold,$payment,$message);
        }


//        return array('error'=>$error);
    }


}