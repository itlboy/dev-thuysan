<?php

Class PayController extends ControllerFrontend {

    private function _getConfigs($key = NULL) {
        $configs['card'] = array(
            'm_PartnerID' => 'letvn',
            'm_MPIN' => 'gejqtbsum',
            'm_UserName' => 'letvn',
            'm_Pass' => 'nlcfkutmr',
            'm_PartnerCode' => '00401',
            'webservice' => 'http://charging-test.megapay.net.vn:10001/CardChargingGW_V2.0/services/Services?wsdl',
            'uri' => 'http://113.161.78.134/VNPTEPAY/',
        );

        if ($key == NULL OR !isset($configs[$key]))
            return $configs;
        else
            return $configs[$key];
    }

    public function actionIndex() {
        $this->checkLogin = TRUE;

        $config = $this->_getConfigs('card');
        $assign['msg'] = array();
        $assign['type'] = Yii::app()->request->getParam('type', 'viettel');
        $assign['providers'] = $this->_getCardProviders();

        if (isset($_POST['submit'])) {
            $card_number = trim(letArray::get($_POST, 'card_number'));
            $serial_number = trim(letArray::get($_POST, 'serial_number'));
            $assign['messages'] = $this->_getCardMessages();

            Yii::import('application.vendors.vnpt.libs.nusoap');

            $webservice = $config['webservice'];
            $soapClient = new SoapClient(null, array('location' => $webservice, 'uri' => $config['uri']));
            $cardCharging = $this->_cardCharging($soapClient, $serial_number, $card_number, $assign['providers'][$assign['type']]);

            // Xử lý báo lỗi
            foreach (Yii::app()->user->getFlashes() as $key => $message) {
                $assign['msg'][$key] = $message;
            }
            $messages = $this->_getCardMessages();
            if ($cardCharging->m_Status == '1')
                $assign['msg']['success'][] = $assign['messages'][$cardCharging->m_Status];
            else
                $assign['msg']['error'][] = 'Mã lỗi ' . $cardCharging->m_Status . ': ' . $assign['messages'][$cardCharging->m_Status];
        }
        $this->render('card', $assign);    }

    private function _cardCharging($soapClient, $serial_number, $card_number, $serviceProvider) {
        $config = $this->_getConfigs('card');

        Yii::import('application.vendors.vnpt.entries.CardCharging');
        Yii::import('application.vendors.vnpt.entries.CardChargingResponse');

        $m_Target = Yii::app()->user->id;

        $CardCharging = new CardCharging();
        $CardCharging->m_UserName = $config['m_UserName'];
        $CardCharging->m_PartnerID = $config['m_PartnerID'];
        $CardCharging->m_MPIN = $config['m_MPIN'];
        $CardCharging->m_Target = $m_Target;
        $CardCharging->m_Card_DATA = $serial_number . ":" . $card_number . ":" . "0" . ":" . $serviceProvider;
        $CardCharging->m_SessionID = "";
        $CardCharging->m_Pass = $config['m_Pass'];
        $CardCharging->soapClient = $soapClient;

        $transid = $config['m_PartnerCode'] . date("YmdHms"); //gen transaction id

        $CardCharging->m_TransID = $transid;
//        echo "<br/>Transaction id: " . $transid . "<br/>";
        //var_dump($CardCharging);
        //print_r($CardCharging -> m_SessionID);
        $CardChargingResponse = new CardChargingResponse();
        $CardChargingResponse = $CardCharging->CardCharging_();
//	print_r($CardChargingResponse);
//        print_r("Ket qua thuc hien giao dich: ");
//        echo "</br>";
//        print_r("Trang thai giao dich: " . $CardChargingResponse->m_Status);
//        echo "</br>";
//        print_r("Menh gia tra ve: " . $CardChargingResponse->m_RESPONSEAMOUNT);
//        echo "</br>";
//        print_r("Transaction id: " . $CardChargingResponse->m_TRANSID);
//        echo "<br/>----------------Ket thuc charging-----------------------<br/>";
        return $CardChargingResponse;
    }

    private function _getCardProviders() {
        return $providers = array(
            'viettel' => 'VTT',
            'mobifone' => 'VMS',
            'vinaphone' => 'VNP',
            'vcoin' => 'VTC',
            'fptgate' => 'FPT',
            'megacard' => 'MGC',
            'oncash' => 'ONC',
        );
    }

    private function _getCardMessages() {
        return $providers = array(
            '-24' => 'Dữ liệu carddata không đúng',
            '-11' => 'Nhà cung cấp không tồn tại',
            '-10' => 'Mã thẻ sai định dạng. Chú ý không nhập các ký tự "-" trong phần mã thẻ nếu có !',
            '0' => 'Giao dịch thất bại',
            '1' => 'Giao dịch thành công',
            '3' => 'Sai Session',
            '4' => 'Thẻ không sử dụng được',
            '5' => 'Bạn nhập sai mã thẻ quá 5 lần. Liên hệ với ban quản trị để được trợ giúp',
            '7' => 'Phiên giao dịch hết hạn, hãy bấm F5',
            '8' => 'Sai IP, Kiểm tra lại IP sử dụng để kết nối tới VNPT EPAY GW',
            '9' => 'Tạm thời khóa kênh nạp tiền Mobifone',
            '10' => 'Hệ thống nhà mạng gặp lỗi, hãy quay lại sau vài phút',
            '11' => 'Kết nối với nhà mạng tạm thời gián đoạn, hãy quay lại sau vài phút',
            '12' => 'Trùng transactionID với một giao dịch trước đó',
            '13' => 'Hệ thống tạm thời bận',
            '-2' => 'Thẻ đã bị khóa',
            '-3' => 'Thẻ đã hết hạn sử dụng',
            '50' => 'Thẻ đã sử dụng hoặc không tồn tại',
            '51' => 'Serial thẻ không đúng',
            '52' => 'Mã thẻ và serial không khớp',
            '53' => 'Mã thẻ hoặc serial không đúng',
            '55' => 'Thẻ tạm thời bị khóa 24h, hãy sử dụng lại sau khi được mở khóa',
            '62' => 'Mật khẩu hệ thống sử dụng để kết nối đến nhà mạng bị sai',
            '57' => 'Mpin mà hệ thống truyền sang nhà mạng không chính xác',
            '58' => 'Tham số hệ thống truyền sang nhà mạng không chính xác',
            '59' => 'Mã thẻ chưa được kích hoạt',
            '60' => 'Partnerid mà hệ thống truyền sang nhà mạng không chính xác',
            '61' => 'User mà hệ thống truyền sang nhà mạng không chính xác',
            '56' => 'TargetAccount tạm thời bị khóa do Charging sai nhiều lần.',
            '63' => 'Không tìm thấy giao dịch này (Hàm kiểm tra trạng thái gd)',
            '64' => 'Giải mã dữ liệu không thành công',
            '65' => 'Bạn kết nối đến nhà mạng nhiều hơn mức cho phép.',
            '99' => 'Chưa nhận được kết quả trả về từ nhà cung cấp mã thẻ. Liên hệ ban quản trị để được trợ giúp',
        );
    }

}

?>