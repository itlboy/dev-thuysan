<?php

Yii::import('application.modules.currency.models.KitCurrencyService');
Class ExchangeController extends ControllerFrontend {
    public $checkLogin = TRUE; // Kiểm tra đăng nhập
    
    public function actionIndex() {
        $assign['msg'] = array();
        $assign['services'] = KitCurrencyService::getList();
        $assign['services'] = KitCurrencyService::treatment($assign['services']);
        $this->render('index', $assign);
    }
    
    public function actionService() {
        $assign['code'] = Yii::app()->request->getParam('code', NULL);
        $assign['msg'] = array();
        
        $assign['service'] = KitCurrencyService::getDetails($assign['code']);
        $assign['service'] = KitCurrencyService::treatment($assign['service']);

        if (isset($_POST['submit'])) {
            // Xử lý phần chuyển đổi sau khi có Game
            
            $assign['msg']['success'][] = 'Bạn đã chuyển đổi thành công';
        }
        
        $this->render('service', $assign);
    }
}