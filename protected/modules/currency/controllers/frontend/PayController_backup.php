<?php
Yii::import('application.modules.currency.libs.adodb.adodb');
Yii::import('application.modules.currency.libs.class.gateWay');
Yii::import('application.modules.currency.models.KitCurrencyCard');
Yii::import('application.modules.currency.models.KitCurrencySms');
Yii::import('application.modules.currency.models.KitCurrencyHistory');
Yii::import('application.modules.currency.models.KitCurrencyCardfail');
Yii::import('application.modules.account.models.KitAccountStats');
Yii::import('application.modules.account.models.KitAccountLevel');
Yii::import('application.modules.account.models.KitAccount');
Yii::import('application.modules.currency.libs.class.BaoKim');
Yii::import('application.modules.currency.libs.class.nganluong');
Yii::import('application.modules.currency.libs.class.nusoap');
Yii::import('application.modules.cms.models.KitCmsConfig');
Class PayController extends ControllerFrontend{

    public function actionIndex(){
        if(Yii::app()->user->isGuest){
            $this->redirect(Yii::app()->getBaseUrl(true));
        } else {
            $userStats = KitAccountStats::model()->findByPk(Yii::app()->user->id);
            if(isset($_POST['payment_type'])){
                $type = $_POST['payment_type'];
                $field = letArray::get($_POST,'type');

                // Ten dich vu
                $title_payment = 'Thanh toán dịch vụ với Let.vn';
                if($field == 'money'){
                    $title_payment = 'Nạp xu';
                } elseif($field == 'film_unlimit'){
                    $title_payment = 'Gia hạn xem phim';
                } elseif($field == 'cs_vip'){
                    $title_payment = 'Gia hạn VIP cho Counter-Strike (http://cs.let.vn)';
                }

                // Xu ly nhap tien
                switch($type){
                    case 'card':
                        $data['serial'] = letArray::get($_POST[$type], 'serial');
                        $data['code'] = letArray::get($_POST[$type], 'code');
                        $data['type']	= letArray::get($_POST[$type], 'type');
                        $data['time']	= date('Y-m-d H:i:s',time());
                        if(!empty($data['code']) AND !empty($data['serial'])){
                            if(KitCurrencyCard::processCard($data)){
                                $money = KitCurrencyCard::getMoney();
                                $gold = KitCmsConfig::convertMoneyToGold($money, 'card');
                                if(in_array($field, array('film_unlimit', 'cs_vip'))){
                                    KitCurrencyHistory::updateConfig(Yii::app()->user->id,$field,$money,$type);
                                }
                            };
                        } else {
                            Yii::app()->user->setFlash('error','Bạn chưa nhập đầy đủ thông tin thẻ card');
                        }
                    break;
                    case 'money':
                        $userStats = KitAccountStats::model()->findByPk(Yii::app()->user->id);
                        $money_balance = !empty($userStats->money_balance) ? $userStats->money_balance : 0;
                        $gold = letArray::get($_POST,'type_money');
                        $money = KitCmsConfig::convertGoldToMoney($gold,'default');
                        if(empty($money)){
                            Yii::app()->user->setFlash('error','Bạn chưa nhập số '.Yii::t('global','letgold').' cần nạp');
                        } elseif(!preg_match('/([0-9]+)/i',$money)){
                            Yii::app()->user->setFlash('error','Chỉ nhập số 0-9');
                        } elseif((int)$money < 0){
                            Yii::app()->user->setFlash('error','Giá trị nhập phải > 0');
                        } elseif(((int)$money > (int)$money_balance) OR ((int)$gold < 10)){
                            Yii::app()->user->setFlash('error','Số '.Yii::t('global','letgold').' nhập vào phải nhỏ hơn số '.Yii::t('global','letgold').' đang có, và tối thiểu 10 '.Yii::t('global','letgold').'');
                        } else {
                            $this->_updateConfig(Yii::app()->user->id,$field,$money, 'money');
                            Yii::app()->user->setFlash('success','Nạp '.Yii::t('global','letgold').' thành công');
                        }
                    break;
                    case 'baokim':
                        $baokim = new BaoKim();
                        $money = letArray::get($_POST,'money');

                        if($money >= 10000){
                            $checknum = md5(Yii::app()->user->name.time());
                            $data = array(
                                'creator' => Yii::app()->user->id,
                                'type' => $field,
                                'payment' => 'baokim',
                                'money' => $money,
                                'send_to' => 'nguago@let.vn',
                                'content' => 'Chưa thanh toán',
                                'created_time' => date('Y-m-d H:i:s'),
                                'checknum' => $checknum,
                                'status' => 0,
                            );
                            // Luu thong tin giao dich Trạng thái chưa được kích hoạt
                            $this->_saveBanking($data);

                            $url = $baokim->createRequestUrl(
                                $checknum,
                                'nguago@let.vn',
                                (int)$money,
                                0,
                                0,
                                $title_payment,
                                'http://id.let.vn/currency/pay/index',
                                'http://id.let.vn/currency/pay/index',
                                'http://phim.let.vn'
                            );
                            echo 'Bạn chờ 1 xíu ...<script type="text/javascript">
                                        window.location.href = "'.$url.'";
                                  </script>';
//                            $this->redirect($url);
                        } else {
                            Yii::app()->user->setFlash('error','Số tiền bạn nhập phải >= 10.000');
                        }
                    break;
                    case 'nganluong':
                        $nganluong = new nganluong();
                        $money = letArray::get($_POST,'money');

                        if($money >= 2000){
                            $checknum = md5(Yii::app()->user->name.time());
                            $data = array(
                                'creator' => Yii::app()->user->id,
                                'type' => $field,
                                'payment' => 'nganluong',
                                'money' => $money,
                                'send_to' => 'nguago@let.vn',
                                'content' => 'Chưa thanh toán',
                                'created_time' => date('Y-m-d H:i:s'),
                                'checknum' => $checknum,
                                'status' => 0,
                            );

                            // Luu thong tin giao dich Trạng thái chưa được kích hoạt
                            $this->_saveBanking($data);

                            $return_url = 'http://id.let.vn/currency/pay/index';
                            $order_description = $title_payment;
                            $receiver = 'nguago@let.vn';
                            $order_code = $checknum;
                            $price = $money;
                            //tao link thanh toan den  ngan hang
                            $url = $nganluong->buildCheckoutUrlExpand($return_url, $receiver, $transaction_info = '', $order_code, $price, $currency = 'vnd', $quantity = 1, $tax = 0, $discount = 0, $fee_cal = 0, $fee_shipping = 0, $order_description, $buyer_info = '', $affiliate_code = '');
//                            $this->redirect($url);
                            echo 'Bạn chờ 1 xíu ...<script type="text/javascript">
                                        window.location.href = "'.$url.'";
                                  </script>';
                        } else {
                            Yii::app()->user->setFlash('error','Số tiền bạn nhập phải >= 10.000');
                        }
                    break;
                }
                $userStats = KitAccountStats::model()->findByPk(Yii::app()->user->id);

                $this->_clearCache(Yii::app()->user->id);
            }

            $this->render('index',array(
                'user' => $userStats,
            ));
        }
    }

    public function actionSMS(){
        // Lấy nội dung tin nhắn
        $message =$_REQUEST['message']; // Nội dung tin
        $phone= $_REQUEST['phone']; // số điện thoại của KH
        $service=$_REQUEST['service']; // mã dịch vụ
        $port =$_REQUEST['port'];  // đầu số
        $main =$_REQUEST['main'];  //keyword
        $sub =$_REQUEST['sub'];  // prefix
        // Hết lấy nội dung tin nhắn

        $checknum = md5(uniqid(rand(), true));// id duy nhất để gán cho tin trả về



        if($port !== '8771' AND $port !== '8671' AND $port !== '8571')// kiểm tra xem có đúng đầu số không?
        {
            // trường hợp nhắn sai đầu số
            echo '
			<ClientResponse>
				<Message>
					<PhoneNumber>'.$phone.'</PhoneNumber>
					<Message>Ban da nhan sai dau so</Message>
					<SMSID>'.$checknum.'</SMSID>
					<ServiceNo>'.$service.'</ServiceNo>
				</Message>
			</ClientResponse>';

        }
        else // xử lý tin
        {
            $message= strtoupper($message);
            $tmp=explode(" ",$message);//cắt nội dung tin ra làm 3 phần
//            $tmp=explode("_",$message);//cắt nội dung tin ra làm 3 phần
            if($tmp[0] == 'LET' && // phần thứ nhất chứa keyword
                (@$tmp[1] == 'NAP' OR @$tmp[1] == 'PHIM')) //phần thứ 2 chứa Prefix
            {
                // Kiem tra gia tri hop le cua $tmp[2]
                if(isset($tmp[2]) AND (int)$tmp[2] > 0){

                    $user_id = $tmp[2];
                    $data = KitAccount::getDetails($user_id);

                    // Kiem tra ID nay co ton tai hay khong
                    if(empty($data)){
                        $messageSend = 'ID khong co ton tai. Ban kiem tra lai';
                    } else {

                        // Config cho cho tung dau so
                        if($port == '8571'){
                            $money = 5000;
                        } elseif($port == '8671'){
                            $money = 10000;
                        } elseif($port == '8771'){
                            $money = 15000;
                        }

                        // Lay thong tin config
                        $gold = KitCmsConfig::convertMoneyToGold($money,'sms');


                        // get username
                        $username = KitAccount::getField($user_id,'username');

                        $time = date('Y-m-d H:i:s');

                        // Them 1 record nap tien vao tai khoan
                        $history = new KitCurrencyHistory();
                        $history->money = $money;
                        $history->gold = $gold;
                        $history->username = $username;
                        $history->content = 'Nạp tiền vào tài khoản';
                        $history->payment = 'sms';
                        $history->user_id = $user_id;
                        $history->time = $time;
                        $history->checknum = $checknum;
                        $history->save();

                        // Luu thong tin Nap tai khoan bang SMS
                        $model = new KitCurrencySms();
                        $model->checknum = $checknum;
                        $model->money = $money;
                        $model->gold = $gold;
                        $model->message = $message;
                        $model->sub = $sub;
                        $model->service = $service;
                        $model->port = $port;
                        $model->phone = $phone;
                        $model->main = $main;
                        $model->username = $username;
                        $model->creator = $user_id;
                        $model->created_time = $time;
                        $model->status = 1;
                        $model->content = 'Thành công';
                        $model->save();

                        if(@$tmp[1] == 'PHIM'){
                            // Them 1 record o history tru phi dich vu
                            KitCurrencyHistory::updateConfig($user_id,'film_unlimit',$money,'sms');
                        } elseif(@$tmp[1] == 'CS') {
                            // Them 1 record o history tru phi dich vu
                            KitCurrencyHistory::updateConfig($user_id,'cs_vip',$money,'sms');
                        } elseif(@$tmp[1] == 'NAP') {
                            // Update thong tin money cua user trong bang let_kit_account_stats
                            KitAccountStats::updateStats($user_id);
                        }
                        $messageSend = 'Ban da nap '.$money.' vao tai khoan. Cam on ban da su dung dich vu cua chung toi. Chuc ban vui ve.';

                    };
                } else {
                    $messageSend = 'Ban nhan sai cu phap. Cu phap NAP: LET NAP {ID} gui 8771. Trong đó ID là id tai khoan cua ban.';
                }

                echo '
		<ClientResponse>
			<Message>
				<PhoneNumber>'.$phone.'</PhoneNumber>
				<Message>'.$messageSend.'</Message>
				<SMSID>'.$checknum.'</SMSID>
				<ServiceNo>'.$service.'</ServiceNo>
			</Message>
		</ClientResponse>';

            }
            else // nếu sai thi trả về hướng dẫn nhắn lại cho đúng cú pháp
            {
                echo '
		<ClientResponse>
			<Message>
				<PhoneNumber>'.$phone.'</PhoneNumber>
				<Message>Ban nhan sai cu phap. Cu phap NAP: LET NAP {ID} gui 8771. Trong đó ID là id tai khoan cua ban.</Message>
				<SMSID>'.$checknum.'</SMSID>
				<ServiceNo>'.$service.'</ServiceNo>
			</Message>
		</ClientResponse>';

            }
        }
    }

    public function actionBaokim(){
        //Lay thong tin tu Baokim POST sang
        $req = '';
        foreach ( $_POST as $key => $value ) {
            $value = urlencode ( stripslashes ( $value ) );
            $req .= "&$key=$value";
        }

        $ch = curl_init();

        //Dia chi chay BPN test
        //curl_setopt($ch, CURLOPT_URL,'http://sandbox.baokim.vn/bpn/verify');

        //Dia chi chay BPN that
        curl_setopt($ch, CURLOPT_URL,'https://www.baokim.vn/bpn/verify');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        $result = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $error = curl_error($ch);

        //        if($result != '' && strstr($result,'VERIFIED') && $status==200){
        //        if($result == 'INVALID'){
        //thuc hien update hoa don
        //            fwrite($fh, ' => VERIFIED');

        $order_id = $_POST['order_id'];
        $transaction_id = $_POST['transaction_id'];
        $transaction_status = $_POST['transaction_status'];
//        //
//        //            //Mot so thong tin khach hang khac
        $customer_name = $_POST['customer_name'];
        $customer_address = $_POST['customer_address'];
        $checksum = $_POST['checksum'];
        $payment_type = $_POST['payment_type'];

        //Nghiep vu: kiem tra xem order_id nay co ton tai trong he thong khong

        /**
         * Neu co thi update thong tin thanh toan vao
         */
        //update thong tin thanh toan

        $model = KitCurrencyBanking::model()->find('checknum="'.$order_id.'"');
        $money = $model->money;
        $user_id = $model->creator;
        $type = $model->type;
        $payment = $model->payment;


        // Lay thong tin config
        $gold = KitCmsConfig::convertMoneyToGold($money,'baokim');

        // get username
        $username = KitAccount::getField($user_id,'username');

        $model->username = $username;
        $model->gold = $gold;
        $model->payment_type = $payment_type;
        $model->transaction_status = $transaction_status;
        $model->transaction_id = $transaction_id;
        $model->checksum = $checksum;

        //kiem tra trang thai giao dich
        if ($transaction_status == 4){
//                        if ($transaction_status !== 4){
            $model->status = 1;

            $error = 'Thành công';
            // Luu giao dich vao History
            $history = new KitCurrencyHistory();
            $history->money = $money;
            $history->gold = $gold;
            $history->username = $username;
            $history->user_id = $user_id;
            $history->content = 'Nạp tiền vào tài khoản.';
            $history->time = date('Y-m-d H:i:s');
            $history->payment = $payment;
            $history->checknum = $order_id; // $order_id = $checknum
            $history->save();

            if(in_array($type, array('film_unlimit', 'cs_vip'))){
                KitCurrencyHistory::updateConfig($user_id,$type,$money,$payment);
            } else {
                KitAccountStats::updateStats($user_id);
            }
            $model->content = $error;

            // Cap nhat history
            if($model->save()){
                $message = 'Nạp tiền vào tài khoản.';
            }
        } elseif($transaction_status == 13){
            $model->content = 'Giao dịch bị tạm giữ';
            $model->save();
        }
    }

    /**
     * Webservice Thanh toan bang Ngan Luong Ten ham bat buoc UpdateOrder(...)
     * @param string $transaction_info
     * @param string $order_code
     * @param string $payment_id
     * @param string $payment_type
     * @param string $secure_code
     * @return string
     * @soap
     */

    public function UpdateOrder($transaction_info, $order_code, $payment_id, $payment_type, $secure_code){
        $error = 'Chưa xác minh';

        $pascode = 'songkinhthay'; // mật khẩu đăng ký merchant

        $md5 = $transaction_info." ".$order_code." ".$payment_id." ".$payment_type." ".$pascode;
        //$mahoa=md5($md5);
        $model = KitCurrencyBanking::model()->find('checknum="'.$order_code.'"');

        //
        $money = $model->money;
        $user_id = $model->creator;
        $type = $model->type;
        $payment = $model->payment;


        // Kiem tra ma gia dich
        if (md5($md5) == strtolower($secure_code)) {
//        if (md5($md5) != strtolower($secure_code)) {

            // Lay thong tin config
            $gold = KitCmsConfig::convertMoneyToGold($money,'nganluong');

            // get username
            $username = KitAccount::getField($user_id,'username');

            $model->username = $username;
            $model->gold = $gold;
            $model->status = 1;
            $model->payment_type = $payment_type;
            $model->payment_id = $payment_id;
            $model->transaction_info = $transaction_info;
            $model->checksum = $secure_code;
            $model->checknum = $order_code;

            $error = 'Thành công';

            $history = new KitCurrencyHistory();
            $history->money = $money;
            $history->gold = $gold;
            $history->user_id = $user_id;
            $history->username = $username;
            $history->content = 'Nạp tiền vào tài khoản.';
            $history->payment = $payment;
            $history->checknum = $order_code;
            $history->time = date('Y-m-d H:i:s');
            $history->save();


            if(in_array($type, array('film_unlimit', 'cs_vip'))){
                KitCurrencyHistory::updateConfig($user_id,$type,$money,$payment);
            } else {
                KitAccountStats::updateStats($user_id);
            }
        } else {
            $error = 'Tham số truyền bị thay đổi';
        }
        $model->content = $error;
        $model->save();

        //        return array('error'=>$error);
    }

    public static function _updateConfig($user_id,$field,$money, $payment = ''){
        // Xu ly tru tien
        $currencyHistory = new KitCurrencyHistory();
        $currencyHistory->user_id = $user_id;
        $currencyHistory->time = date('Y-m-d H:i:s');
        $gold = KitCmsConfig::convertMoneyToGold($money,$payment);
        
        if ($field == 'film_unlimit') {
            $type = 'datetime';
            $currencyHistory->content = 'Gia hạn xem phim';
            $currencyHistory->money = -$money;
            $currencyHistory->gold = -$gold;
        } elseif ($field == 'cs_vip') {
            $type = 'datetime';
            $currencyHistory->content = 'Gia hạn VIP cho Counter-Strike';
            $currencyHistory->money = -$money;
            $currencyHistory->gold = -$gold;
        } else {
            $currencyHistory->money = $money;
            $currencyHistory->gold = -$gold;
        }

        $currencyHistory->validate();
        $currencyHistory->save();
        //


        $exchange = KitAccountConfig::exchange($field,$gold);
        KitAccountConfig::updateConfig($user_id,$field,$exchange,$type);

        KitAccountStats::updateStats($user_id);
    }

    public function _clearCache($user_id){
        Yii::app()->cache->delete('config_unlimit_'.$user_id);
        Yii::app()->cache->delete(md5('account_stats_details_' . $user_id));
    }

    public function _saveBanking($data = array()){
        $model = new KitCurrencyBanking();
        if(empty($data)) return FALSE;
        $model->attributes = $data;
        if($model->validate()){
            if($model->save()){
                return TRUE;
            } else return FALSE;
        } else return FALSE;
    }

    public function actions(){
        return array(
            'quote'=>array(
                'class'=>'CWebServiceAction',
            ),
        );
    }
}
?>