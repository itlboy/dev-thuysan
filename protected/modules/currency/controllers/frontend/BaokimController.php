<?php
Yii::import('application.modules.currency.models.KitCurrencyCard');
Yii::import('application.modules.currency.models.KitCurrencyHistory');
Yii::import('application.modules.currency.models.KitCurrencyCardfail');
Yii::import('application.modules.account.models.KitAccountStats');
Yii::import('application.modules.cms.models.KitCmsConfig');
Yii::import('application.modules.account.models.KitAccountLevel');

class BaokimController extends ControllerFrontend
{
    public function actionComplete(){

        //Lay thong tin tu Baokim POST sang
        $req = '';
        foreach ( $_POST as $key => $value ) {
            $value = urlencode ( stripslashes ( $value ) );
            $req .= "&$key=$value";
        }

        $ch = curl_init();

        //Dia chi chay BPN test
        //curl_setopt($ch, CURLOPT_URL,'http://sandbox.baokim.vn/bpn/verify');

        //Dia chi chay BPN that
        curl_setopt($ch, CURLOPT_URL,'https://www.baokim.vn/bpn/verify');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        $result = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $error = curl_error($ch);

//        if($result != '' && strstr($result,'VERIFIED') && $status==200){
//        if($result == 'INVALID'){
            //thuc hien update hoa don
//            fwrite($fh, ' => VERIFIED');

            $order_id = $_POST['order_id'];
            $transaction_id = $_POST['transaction_id'];
            $transaction_status = $_POST['transaction_status'];
//
//            //Mot so thong tin khach hang khac
            $customer_name = $_POST['customer_name'];
            $customer_address = $_POST['customer_address'];
            $checksum = $_POST['checksum'];
            $payment_type = $_POST['payment_type'];



            //Nghiep vu: kiem tra xem order_id nay co ton tai trong he thong khong


            /**
             * Neu co thi update thong tin thanh toan vao
             */
            //update thong tin thanh toan


            $model = KitCurrencyBanking::model()->find('checknum="'.$order_id.'"');
            $money = $model->money;
            $user_id = $model->creator;
            $type = $model->type;
            $payment = $model->payment;
            $gold = KitCmsConfig::convertMoneyToGold($money,$payment);

            $model->payment_type = $payment_type;
            $model->transaction_status = $transaction_status;
            $model->transaction_id = $transaction_id;
            $model->checksum = $checksum;

            //kiem tra trang thai giao dich
            if ($transaction_status == 4){
//            if ($transaction_status !== 4){
                $model->status = 1;

                $error = 'Thành công';

                if($type == 'film_unlimit'){
                    // Luu giao dich vao History
                    $history = new KitCurrencyHistory();
                    $history->money = $money;
                    $history->gold = $gold;
                    $history->user_id = $user_id;
                    $history->content = 'Nạp tiền vào tài khoản.';
                    $history->time = date('Y-m-d H:i:s');
                    $history->payment = $payment;
                    $history->save();
                }
                $model->content = $error;

            // Cap nhat history
            if($model->save()){
                $message = 'Nạp tiền vào tài khoản.';
                KitCurrencyHistory::updateConfig($user_id,$type,$money,$payment,$message);
            }
        } elseif($transaction_status == 13){
                $model->content = 'Giao dịch bị tạm giữ';
                $model->save();
        }

    }

    public function actionTest(){
        $rate = KitCmsConfig::getCurrencyRate(10000,'currency_rate','sms');
    }
}