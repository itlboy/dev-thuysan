<?php

Yii::import('application.modules.currency.libs.class.vnpt');
Yii::import('application.modules.currency.libs.class.OnePay');
Class PayController extends ControllerFrontend {
    public $checkLogin = TRUE; // Kiểm tra đăng nhập
    
    public function actionIndex() {
        $assign['msg'] = array();
        $assign['providers'] = vnpt::$providers;
        $assign['banks'] = OnePay::$banks;
        
        $this->render('index', $assign);
    }

    public function actionCard() {
        $assign['msg'] = array();
        $assign['type'] = Yii::app()->request->getParam('type', 'viettel');
        $assign['providers'] = vnpt::$providers;

        if (isset($_POST['submit'])) {
            $card_number = trim(letArray::get($_POST, 'card_number'));
            $serial_number = trim(letArray::get($_POST, 'serial_number'));
            $assign['messages'] = vnpt::$messages;

            Yii::import('application.vendors.vnpt.libs.nusoap');

            $webservice = vnpt::$configs['webservice'];
            $soapClient = new SoapClient(null, array('location' => $webservice, 'uri' => vnpt::$configs['uri']));
            $cardCharging = vnpt::cardCharging($soapClient, $serial_number, $card_number, $assign['providers'][$assign['type']]);

            // Xử lý báo lỗi
            foreach (Yii::app()->user->getFlashes() as $key => $message) {
                $assign['msg'][$key] = $message;
            }
            if ($cardCharging->m_Status == '1') {
                Yii::import('application.modules.currency.models.KitCurrencyPay');
                Yii::import('application.modules.currency.models.KitCurrencyCard');
                Yii::import('application.modules.currency.models.KitCurrencyGold');
                Yii::import('application.modules.account.models.KitAccountStats');
                
                $goldExchange = $cardCharging->m_RESPONSEAMOUNT / 1000;
                
                // Lưu vao bảng lịch sử giao dịch
                $insert = new KitCurrencyPay;
                $insert->username = Yii::app()->user->name;
                $insert->user_id = Yii::app()->user->id;
                $insert->money = $cardCharging->m_RESPONSEAMOUNT;
                $insert->gold = $goldExchange;
                $insert->payment = 'card';
                $insert->checknum = $cardCharging->m_TRANSID;
                $insert->time = date('Y-m-d H:i:s');
                $insert->save();
                
                // Lưu vào bảng chứa lịch sử nạp, sử dụng xu
                $insert = new KitCurrencyGold;
                $insert->username = Yii::app()->user->name;
                $insert->user_id = Yii::app()->user->id;
                $insert->gold = $goldExchange;
                $insert->payment = 'card';
                $insert->checknum = $cardCharging->m_TRANSID;
                $insert->time = date('Y-m-d H:i:s');
                $insert->save();
                
                // Lưu vào bảng chứa thông tin card
                $insert = new KitCurrencyCard;
                $insert->username = Yii::app()->user->name;
                $insert->user_id = Yii::app()->user->id;
                $insert->type = $assign['providers'][$assign['type']];
                $insert->card_number = $card_number;
                $insert->serial_number = $serial_number;
                $insert->money = $cardCharging->m_RESPONSEAMOUNT;
                $insert->gold = $goldExchange;
                $insert->checknum = $cardCharging->m_TRANSID;
                $insert->time = date('Y-m-d H:i:s');
                $insert->save();
                
                // Cập nhật số xu trong tài khoản người dùng
                KitAccountStats::updateStats(Yii::app()->user->id, array('gold_current','gold_paid','money_paid'));
                
                $assign['msg']['success'][] = $assign['messages'][$cardCharging->m_Status];
            } else {
                Yii::import('application.modules.currency.models.KitCurrencyCardfail');
                $msg = 'Mã lỗi ' . $cardCharging->m_Status . ': ' . $assign['messages'][$cardCharging->m_Status];
                
                // Lưu thông tin giao dịch lỗi
                $paycardfail = new KitCurrencyCardfail;
                $paycardfail->username = Yii::app()->user->name;
                $paycardfail->user_id = Yii::app()->user->id;
                $paycardfail->type = $assign['providers'][$assign['type']];
                $paycardfail->card_number = $card_number;
                $paycardfail->serial_number = $serial_number;
                $paycardfail->error_code = $cardCharging->m_Status;
                $paycardfail->error_msg = $msg;
                $paycardfail->checknum = $cardCharging->m_TRANSID;
                $paycardfail->time = date('Y-m-d H:i:s');
                $paycardfail->save();
                
                $assign['msg']['error'][] = $msg;
            }
        }
        $this->render('card', $assign);
    }

    public function actionHistoryPay() {
        $assign['msg'] = array();
        
        Yii::import('application.modules.currency.models.KitCurrencyPay');
        
        $criteria=new CDbCriteria();
        $criteria->condition = 'user_id = ' . Yii::app()->user->id;
        $count = KitCurrencyPay::model()->count($criteria);
        $pages = new CPagination($count);

        // results per page
        $pages->pageSize=10;
        $pages->applyLimit($criteria);
        $models = KitCurrencyPay::model()->findAll($criteria);
        
        $assign['models'] = $models;
        $assign['pages'] = $pages;
        
        $this->render('historypay', $assign);
    }

    public function actionHistoryGold() {
//        $assign['msg'] = array();
//        
//        Yii::import('application.modules.currency.models.KitCurrencyCard');
//        
//        $criteria=new CDbCriteria();
//        $count = KitCurrencyCard::model()->count($criteria);
//        $pages = new CPagination($count);
//
//        // results per page
//        $pages->pageSize=10;
//        $pages->applyLimit($criteria);
//        $models = KitCurrencyCard::model()->findAll($criteria);
//        
//        $assign['models'] = $models;
//        $assign['pages'] = $pages;
//        
//        $this->render('historycard', $assign);
    }
    
    public function actionMakeRequestOnePay(){
        if(Yii::app()->request->isPostRequest){
            
            $vpcURL = OnePay::$configs['vpcURL'];
            $SECURE_SECRET = OnePay::$configs['SECURE_SECRET'];
            $MERCHANT_ID = OnePay::$configs['MERCHANT_ID'];
            $ACCESS_CODE = OnePay::$configs['ACCESS_CODE'];
            
            //Khởi tạo user hiện tại
            $model = KitAccount::model()->findByPk(Yii::app()->user->id);
            
            //Get Post values
            $amount = Yii::app()->request->getPost('vpc_Amount', '');
            $bankId = Yii::app()->request->getPost('slectedBankId', '');
            $bankName = Yii::app()->request->getPost('slectedBankName', '');
            
            
            // Nếu giá trị nạp nằm trong khoảng cho phép
            if(isset(OnePay::$allowValues[$amount]) AND !empty(OnePay::$allowValues[$amount])){
                $paymentId = date ( 'YmdHis' ) . rand ();
                $transactionInfo = $model->username . " Nap Tien". " - " .$bankName;
                $returnUrl = Yii::app()->createAbsoluteUrl('currency/pay/handleOnePayResult');
                
                //Save bankId to session for use later
                Yii::app()->session['bankId'] = $bankId;

                $data = array(
                    'vpc_Merchant' => $MERCHANT_ID,
                    'vpc_AccessCode' => $ACCESS_CODE,
                    'vpc_MerchTxnRef' => $paymentId,
                    'vpc_OrderInfo' => $transactionInfo,
                    'vpc_Amount' => ($amount*100),
                    'vpc_ReturnURL' => $returnUrl,
                    'vpc_Version' => 2,
                    'vpc_Command' => 'pay',
                    'vpc_Locale' => 'vn',
                    'vpc_Currency' => 'VND',
                    'vpc_Customer_Phone' => $model->phone,
                    'vpc_Customer_Email' => $model->email,
                    'vpc_Customer_Id' => $model->username,
                    'vpc_Bank' => $bankId

                );

                //Khởi tạo chuỗi dữ liệu mã hóa trống
                $stringHashData = "";

                //Sắp xếp dữ liệu theo thứ tự a-z trước khi nối lại
                ksort($data);

                //Đặt tham số đếm = 0
                $appendAmp = 0;

                foreach($data as $key => $value) {

                    // tạo chuỗi đầu dữ liệu những tham số có dữ liệu
                    if (strlen($value) > 0) {
                        // this ensures the first paramter of the URL is preceded by the '?' char
                        if ($appendAmp == 0) {
                            $vpcURL .= urlencode($key) . '=' . urlencode($value);
                            $appendAmp = 1;
                        } else {
                            $vpcURL .= '&' . urlencode($key) . "=" . urlencode($value);
                        }
                        //$stringHashData .= $value; *****************************sử dụng cả tên và giá trị tham số để mã hóa*****************************
                        if ((strlen($value) > 0) && ((substr($key, 0,4)=="vpc_") || (substr($key,0,5) =="user_"))) {
                            $stringHashData .= $key . "=" . $value . "&";
                        }
                    }
                }
                //*****************************xóa ký tự & ở thừa ở cuối chuỗi dữ liệu mã hóa*****************************
                $stringHashData = rtrim($stringHashData, "&");
                // Create the secure hash and append it to the Virtual Payment Client Data if
                // the merchant secret has been provided.
                // thêm giá trị chuỗi mã hóa dữ liệu được tạo ra ở trên vào cuối url
                if (strlen($SECURE_SECRET) > 0) {
                    //$vpcURL .= "&vpc_SecureHash=" . strtoupper(md5($stringHashData));
                    // *****************************Thay hàm mã hóa dữ liệu*****************************
                    $vpcURL .= "&vpc_SecureHash=" . strtoupper(hash_hmac('SHA256', $stringHashData, pack('H*',$SECURE_SECRET)));
                }

                // chuyển trình duyệt sang cổng thanh toán theo URL được tạo ra
                header("Location: ".$vpcURL);
            }else{
                throw new CHttpException(400, 'Xin lỗi, số tiền nạp không hợp lệ.');
            } 
        } else{
            throw new CHttpException(400, 'Truy vấn không hợp lệ.');
        }
    }
    
    public function actionHandleOnePayResult() {

        $msg = array();
        $SECURE_SECRET = OnePay::$configs['SECURE_SECRET'];
        
        // Standard Receipt Data
        $amount = letArray::get($_GET, 'vpc_Amount');
        $message = letArray::get($_GET, 'vpc_Message');
        $orderInfo = letArray::get($_GET, 'vpc_OrderInfo');
        $transactionNo = letArray::get($_GET, 'vpc_TransactionNo');
        $txnResponseCode = letArray::get($_GET, 'vpc_TxnResponseCode');
        
//        $command = letArray::get($_GET, 'vpc_Command');
//        $version =  letArray::get($_GET, 'vpc_Version');
//        $locale = letArray::get($_GET, 'vpc_Locale');
//        $merchantID = letArray::get($_GET, 'vpc_Merchant');
//        $cardType =  letArray::get($_GET, 'vpc_Card');
//        $receiptNo = letArray::get($_GET, 'vpc_ReceiptNo');
//        $authorizeID = letArray::get($_GET, 'vpc_AuthorizeId');
//        $merchTxnRef = letArray::get($_GET, 'vpc_MerchTxnRef');
//        $acqResponseCode = letArray::get($_GET, 'vpc_AcqResponseCode');
        
        // get and remove the vpc_TxnResponseCode code from the response fields as we
        // do not want to include this field in the hash calculation
        $vpc_Txn_Secure_Hash = letArray::get($_GET, 'vpc_SecureHash');
        unset ( $_GET ["vpc_SecureHash"] );

        if (strlen ( $SECURE_SECRET ) > 0 && $txnResponseCode != "7" && $txnResponseCode != NULL) {

            //$stringHashData = $SECURE_SECRET;
            //*****************************khởi tạo chuỗi mã hóa rỗng*****************************
            $stringHashData = "";

            // sort all the incoming vpc response fields and leave out any with no value
            foreach ( $_GET as $key => $value ) {
        //        if ($key != "vpc_SecureHash" or strlen($value) > 0) {
        //            $stringHashData .= $value;
        //        }
        //      *****************************chỉ lấy các tham số bắt đầu bằng "vpc_" hoặc "user_" và khác trống và không phải chuỗi hash code trả về*****************************
                if ($key != "vpc_SecureHash" && (strlen($value) > 0) && ((substr($key, 0,4)=="vpc_") || (substr($key,0,5) =="user_"))) {
                    $stringHashData .= $key . "=" . $value . "&";
                }
            }
        //  *****************************Xóa dấu & thừa cuối chuỗi dữ liệu*****************************
            $stringHashData = rtrim($stringHashData, "&");	


        //    if (strtoupper ( $vpc_Txn_Secure_Hash ) == strtoupper ( md5 ( $stringHashData ) )) {
        //    *****************************Thay hàm tạo chuỗi mã hóa*****************************
            if (strtoupper ( $vpc_Txn_Secure_Hash ) == strtoupper(hash_hmac('SHA256', $stringHashData, pack('H*',$SECURE_SECRET)))) {
                // Secure Hash validation succeeded, add a data field to be displayed
                // later.
                $hashValidated = "CORRECT";
            } else {
                // Secure Hash validation failed, add a data field to be displayed
                // later.
                $hashValidated = "INVALID HASH";
            }
        } else {
            // Secure Hash was not validated, add a data field to be displayed later.
            $hashValidated = "INVALID HASH";
        }
        
        //Get response message
        $responseMessage = OnePay::getResponseMessage($txnResponseCode);
       
        if($hashValidated === "CORRECT" AND $txnResponseCode === "0"){
            Yii::import('application.modules.currency.models.KitCurrencyPay');
            Yii::import('application.modules.currency.models.KitCurrencyBank');
            Yii::import('application.modules.currency.models.KitCurrencyGold');
            Yii::import('application.modules.account.models.KitAccountStats');

            $goldExchange = $amount / 1000;
            // check tồn tại
            $found = KitCurrencyPay::model()->find("checknum = '{$transactionNo}' AND money = {$amount} AND payment = 'bank'");
            if(!$found){ 
                // Lưu vao bảng lịch sử giao dịch
                $insert = new KitCurrencyPay;
                $insert->username = Yii::app()->user->name;
                $insert->user_id = Yii::app()->user->id;
                $insert->money = $amount;
                $insert->gold = $goldExchange;
                $insert->payment = 'bank';
                $insert->content = $orderInfo;
                $insert->checknum = $transactionNo;
                $insert->time = date('Y-m-d H:i:s');
                $insert->save();
                
                // Lưu vào bảng chứa lịch sử nạp, sử dụng xu
                $insert = new KitCurrencyGold;
                $insert->username = Yii::app()->user->name;
                $insert->user_id = Yii::app()->user->id;
                $insert->gold = $goldExchange;
                $insert->payment = 'bank';
                $insert->content = $orderInfo;
                $insert->checknum = $transactionNo;
                $insert->time = date('Y-m-d H:i:s');
                $insert->save();

                // Lưu vào bảng chứa thông tin bank
                $insert = new KitCurrencyBank;
                $insert->username = Yii::app()->user->name;
                $insert->user_id = Yii::app()->user->id;
                $bankId = isset(Yii::app()->session['bankId']) ? Yii::app()->session['bankId'] : FALSE;
                if($bankId)
                    $bankName = OnePay::$banks[$bankId];
                else
                    $bankName = '';
                $insert->type = $bankName;
                $insert->money = $amount;
                $insert->gold = $goldExchange;
                $insert->checknum = $transactionNo;
                $insert->response_code = $txnResponseCode;
                $insert->time = date('Y-m-d H:i:s');
                $insert->save();

                // Cập nhật số xu trong tài khoản người dùng
                KitAccountStats::updateStats(Yii::app()->user->id, array('gold_current','gold_paid','money_paid'));
                
                $historyUrl = Yii::app()->createAbsoluteUrl('currency/pay/historypay');
                $message = "Giao dịch thành công! Bạn có thể xem lại thông tin giao dịch <a href='{$historyUrl}'>tại đây.</a>";
                $msg['success'][] = $message;
            }else {
                $msg['warning'][] = 'Giao dịch đã kết thúc.';
            }
        }elseif ($hashValidated === "INVALID HASH" AND $txnResponseCode === "0"){
            $msg['warning'][] = "Giao dịch Pendding";
        }else {
            $msg['error'][] = $responseMessage;
        }
                
        //Thông báo tới member
        $this->render('onepayresult', array(
            'msg'=>$msg
        ));
    }

}