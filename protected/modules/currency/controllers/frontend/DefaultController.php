<?php
/**
 * Module: currency
 * Auth:
 * Date: 2012-09-18 04:08:29
 */
Yii::import('application.modules.currency.libs.class.Paypal');
Yii::import('application.modules.currency.libs.class.paypalfunctions');

class DefaultController extends ControllerFrontend
{
	public function actionIndex()
	{
		$this->render('index');
	}
}