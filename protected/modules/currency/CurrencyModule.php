<?php

class CurrencyModule extends CWebModule
{
    public $category = FALSE; // Xác định Module có category hay không
    public $option = FALSE; // Xác định Module có option hay không
    public $menuList = array(
        array(
            'name' => 'Thẻ đã nạp',
            'url' => '/category/default/create/module/currency',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'type' => 'dialog',
            'grid' => '',
        ),
        array(
            'name' => 'Thẻ bị lỗi',
            'url' => '/category/default/create/module/currency',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'type' => 'dialog',
            'grid' => '',
        ),
        array(
            'name' => 'Lịch sử giao dịch',
            'url' => '/category/default/create/module/currency',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'type' => 'dialog',
            'grid' => '',
        ),
    );
    public $image = array();

    public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'currency.models.*',
			'currency.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
