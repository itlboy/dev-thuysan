<?php
Yii::import('application.modules.currency.libs.class.vnpt.entries.ClsCryptor');
Class login {
    Public $m_UserName;
    Public $m_Pass;
    Public $m_PartnerID;
    public $soapClient;

    function login_() {
        $Obj = new loginResponse();
        $RSAClass = new ClsCryptor();
        //Ham thuc hien lay public key cua EPAy tu file pem
        $RSAClass->GetpublicKeyFrompemFile("protected/modules/currency/libs/class/vnpt/Key/Epay_Public_key.pem");
        try {
            $EncrypedPass = $RSAClass->encrypt($this->m_Pass);
        } catch (Exception $ex) {
            
        }
        $Pass = base64_encode($EncrypedPass);
        //print_r($Pass);
        //$soapClient = new Soapclient("http://192.168.0.85:10001/CardChargingGW_0108/services/Services?wsdl");

        try {
            $result = $this->soapClient->login($this->m_UserName, $Pass, $this->m_PartnerID); // goi ham login de lay session id du lieu tra ve la mot mang voi cac thong tin message, sessionid,status,transid
            //print_r($result);
        } catch (Exception $ex) {
            echo "Xay ra loi khi thuc hien login: " . $ex;
        }
        $Obj->m_Sessage = $result->message;

        //$Obj->m_SessionID = $result->sessionid;
        $Obj->m_Status = $result->status;

        //Ham thuc hien lay private key cua doi tac tu file pem
        $RSAClass->GetPrivatekeyFrompemFile("protected/modules/currency/libs/class/vnpt/Key/private_key.pem");
        try {
            $Session_Decryped = $RSAClass->decrypt(base64_decode($result->sessionid));
            $Obj->m_SessionID = $this->Hextobyte($Session_Decryped);
        } catch (Exception $ex) {
            echo"<br/><h1>Co loi khi thuc hien giai ma session:<h1/><br/>" . $ex;
        }
        //print_r($Obj->m_SessionID) ;

        $Obj->m_TransID = $result->transid;

        return $Obj;
    }

    function Hextobyte($strHex) {
        $string = '';
        for ($i = 0; $i < strlen($strHex) - 1; $i+=2) {
            $string .= chr(hexdec($strHex[$i] . $strHex[$i + 1]));
        }
        return $string;
    }

    function ByteToHex($strHex) {
        return bin2hex($strHex);
    }

}


