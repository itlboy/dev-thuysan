<?php

class ClsCryptor {

    private $RsaPublicKey;
    private $RsaPrivateKey;
    private $TripDesKey;

    public function GetpublicKeyFromCertFile($filePath) {
        $fp = fopen($filePath, "r");
        $pub_key = fread($fp, filesize($filePath));
        fclose($fp);
        openssl_get_publickey($pub_key);

        $this->RsaPublicKey = $pub_key;
    }

    public function GetpublicKeyFrompemFile($filePath) {
        $fp = fopen($filePath, "r");
        $pub_key = fread($fp, filesize($filePath));
        fclose($fp);
        openssl_get_publickey($pub_key);
        //print_r($pub_key);
        $this->RsaPublicKey = $pub_key;
        //print_r($this-> RsaPublicKey);
    }

    public function GetPrivatekeyFrompemFile($filePath) {
        $fp = fopen($filePath, "r");
        $pub_key = fread($fp, filesize($filePath));
        fclose($fp);
        $this->RsaPrivateKey = $pub_key;
    }

    public function GetPrivate_Public_KeyFromPfxFile($filePath, $Passphase) {
        $p12cert = array();
        $fp = fopen($filePath, "r");
        $p12buf = fread($fp, filesize($filePath));
        fclose($fp);
        openssl_pkcs12_read($p12buf, $p12cert, $Passphase);
        $this->RsaPrivateKey = $p12cert['pkey'];
        $this->RsaPublicKey = $p12cert['cert'];
    }

    function encrypt($source) {
        //path holds the certificate path present in the system
        $pub_key = $this->RsaPublicKey;
        //$source="sumanth";
        $j = 0;
        $x = strlen($source) / 10;
        $y = floor($x);
        $crt = '';
        //print_r($pub_key) ;
        for ($i = 0; $i < $y; $i++) {
            $crypttext = '';

            openssl_public_encrypt(substr($source, $j, 10), $crypttext, $pub_key);
            $j = $j + 10;
            $crt.=$crypttext;
            $crt.=":::";
        }
        if ((strlen($source) % 10) > 0) {
            openssl_public_encrypt(substr($source, $j), $crypttext, $pub_key);
            $crt.=$crypttext;
        }
        return($crt);
    }

    //Decryption with private key
    function decrypt($crypttext) {
        $priv_key = $this->RsaPrivateKey;
        $tt = explode(":::", $crypttext);
        $cnt = count($tt);
        $i = 0;
        $str = '';
        while ($i < $cnt) {
            openssl_private_decrypt($tt[$i], $str1, $priv_key);
            $str.=$str1;
            $i++;
        }
        return $str;
    }

}
