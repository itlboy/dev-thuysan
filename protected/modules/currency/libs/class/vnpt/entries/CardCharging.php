<?php
Yii::import('application.modules.currency.libs.class.vnpt.entries.login');
Yii::import('application.modules.currency.libs.class.vnpt.entries.loginResponse');
Yii::import('application.modules.currency.libs.class.vnpt.entries.TriptDes');
Class CardCharging {

    Public $m_TransID;
    Public $m_UserName;
    Public $m_PartnerID;
    Public $m_MPIN;
    Public $m_Target;
    Public $m_Card_DATA;
    public $m_Pass;
    var $SessionID;
    var $soapClient;

    function CardCharging_() {
//        echo"session get result: " . $this->SessionID;
//        echo"login de lay session";
        if ($this->SessionID == null || $this->SessionID == "") {
            $login = new login();
            $login->m_UserName = $this->m_UserName;
            $login->m_Pass = $this->m_Pass;
            $login->m_PartnerID = $this->m_PartnerID;
            $login->soapClient = $this->soapClient;
            //var_dump($login);
            $loginresponse = new loginResponse();
            $loginresponse = $login->login_();
            if ($loginresponse->m_Status == "1") {
//                echo"<br/>----Login thanh cong-----<br/>";
                //print_r($loginresponse);						
                //Nen luu lai bien SessionID de thuc hien cac ham charging tiep theo
                //Tranh viec moi giao dich charging lai login 1 lan nhu vay giao dich se rat cham.
                $this->SessionID = bin2hex($loginresponse->m_SessionID);
                //session_start();     //start the session						
//                echo("<br/>Sessionid login clear text: " . $this->SessionID);
//                echo"<br/>----Login thanh cong-----<br/>";
            } else {
//                echo"<br/>----Login fail-----------<br/>";
//                echo"<br/>----Khong tiep tuc thuc hien charging-----------<br/>";
                //Login fail khong thuc hien charging
//                echo "Trang thai login: " . $loginresponse->m_Status;
//                echo "Message: " . $loginresponse->m_Sessage;
//                echo"<br/>----Login fail-----------<br/>";
                return;
            }
        }
//        echo "<br/>----------------Bat dau thuc hien charging-----------------------<br/>";
        ///Bat dau thuc hien charging
        $Ojb = new CardChargingResponse();
        $key = $this->Hextobyte($this->SessionID);
        //print_r($this-> m_SessionID);
        //print_r($this->m_MPIN);
        //$keytesst = base64_encode($key);
        //print_r($keytesst);
        $ObjTriptDes = new TriptDes($key);
        try {
            $strEncreped = $ObjTriptDes->encrypt($this->m_MPIN);
            //print_r ($strEncreped);
            //$decode =  $ObjTriptDes->decrypt(  $strEncreped);
            //print_r ($decode);
            $Mpin = $this->ByteToHex($strEncreped);

            $Card_DATA = $this->ByteToHex($ObjTriptDes->encrypt($this->m_Card_DATA));
            //print_r($Card_DATA);
        } catch (Exception $ex) {
            Yii::app()->user->setFlash('error', "Co loi xay ra khi ma hoa mpin: " . $ex);
        }
        //$soapClient = new Soapclient("http://192.168.0.85:10001/CardChargingGW_0108/services/Services?wsdl");
        try {
            //print_r($this);

            $result = $this->soapClient->cardCharging($this->m_TransID, $this->m_UserName, $this->m_PartnerID, $Mpin, $this->m_Target, $Card_DATA, md5($this->SessionID)); // goi ham login de lay session id du lieu tra ve la mot mang voi cac thong tin message, sessionid,status,transid
//            print_r($result); die;
        } catch (Exception $ex) {
            Yii::app()->user->setFlash('error', "Co loi xay ra khi thuc hien charging: " . $ex);
        }
//        var_dump($result->status); die;
        $Obj = new stdClass();
        $Obj->m_Status = $result->status;
        $Obj->m_Message = $result->message;
        $Obj->m_TRANSID = $result->transid;
        $Obj->m_AMOUNT = $result->amount;
        $resAmount = $ObjTriptDes->decrypt($this->Hextobyte($result->responseamount));
        $Obj->m_RESPONSEAMOUNT = $resAmount; //$result->responseamount;
        if ($Obj->m_Status == 3 || $Obj->m_Status == 7)
            $this->SessionID = null;
//        print_r($Obj);
        return $Obj;
    }

    function Hextobyte($strHex) {
        $string = '';
        for ($i = 0; $i < strlen($strHex) - 1; $i+=2) {
            $string .= chr(hexdec($strHex[$i] . $strHex[$i + 1]));
        }
        return $string;
    }

    function ByteToHex($strHex) {
        return bin2hex($strHex);
    }

}
