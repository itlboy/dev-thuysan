﻿<?php
class vnpt
{
    /* Test
        'm_PartnerID' => 'letvn',
        'm_MPIN' => 'gejqtbsum',
        'm_UserName' => 'letvn',
        'm_Pass' => 'nlcfkutmr',
        'm_PartnerCode' => '00401',
        'webservice' => 'http://charging-test.megapay.net.vn:10001/CardChargingGW_V2.0/services/Services?wsdl',
        'uri' => 'http://113.161.78.134/VNPTEPAY/',
     */

    /* Chạy live
        'm_PartnerID' => 'letvn',
        'm_MPIN' => 'xlpvzqcay',
        'm_UserName' => 'letvn',
        'm_Pass' => 'iesaswtqj',
        'm_PartnerCode' => '00519',
        'webservice' => 'http://charging-service.megapay.net.vn/CardChargingGW_V2.0/services/Services?wsdl',
        'uri' => 'http://113.161.78.134/VNPTEPAY/',
     */
    
    public static $configs = array(
        'm_PartnerID' => 'letvn',
        'm_MPIN' => 'gejqtbsum',
        'm_UserName' => 'letvn',
        'm_Pass' => 'nlcfkutmr',
        'm_PartnerCode' => '00401',
        'webservice' => 'http://charging-test.megapay.net.vn:10001/CardChargingGW_V2.0/services/Services?wsdl',
        'uri' => 'http://113.161.78.134/VNPTEPAY/',
    );

    public static $providers = array(
        'viettel' => 'VTT',
        'mobifone' => 'VMS',
        'vinaphone' => 'VNP',
        'vcoin' => 'VTC',
        'fptgate' => 'FPT',
        'megacard' => 'MGC',
        'oncash' => 'ONC',
    );

    public static $messages = array(
        '-24' => 'Dữ liệu carddata không đúng',
        '-11' => 'Nhà cung cấp không tồn tại',
        '-10' => 'Mã thẻ sai định dạng. Chú ý không nhập các ký tự "-" trong phần mã thẻ nếu có !',
        '0' => 'Giao dịch thất bại',
        '1' => 'Giao dịch thành công',
        '3' => 'Sai Session',
        '4' => 'Thẻ không sử dụng được',
        '5' => 'Bạn nhập sai mã thẻ quá 5 lần. Liên hệ với ban quản trị để được trợ giúp',
        '7' => 'Phiên giao dịch hết hạn, hãy bấm F5',
        '8' => 'Sai IP, Kiểm tra lại IP sử dụng để kết nối tới VNPT EPAY GW',
        '9' => 'Tạm thời khóa kênh nạp tiền Mobifone',
        '10' => 'Hệ thống nhà mạng gặp lỗi, hãy quay lại sau vài phút',
        '11' => 'Kết nối với nhà mạng tạm thời gián đoạn, hãy quay lại sau vài phút',
        '12' => 'Trùng transactionID với một giao dịch trước đó',
        '13' => 'Hệ thống tạm thời bận',
        '-2' => 'Thẻ đã bị khóa',
        '-3' => 'Thẻ đã hết hạn sử dụng',
        '50' => 'Thẻ đã sử dụng hoặc không tồn tại',
        '51' => 'Serial thẻ không đúng',
        '52' => 'Mã thẻ và serial không khớp',
        '53' => 'Mã thẻ hoặc serial không đúng',
        '55' => 'Thẻ tạm thời bị khóa 24h, hãy sử dụng lại sau khi được mở khóa',
        '62' => 'Mật khẩu hệ thống sử dụng để kết nối đến nhà mạng bị sai',
        '57' => 'Mpin mà hệ thống truyền sang nhà mạng không chính xác',
        '58' => 'Tham số hệ thống truyền sang nhà mạng không chính xác',
        '59' => 'Mã thẻ chưa được kích hoạt',
        '60' => 'Partnerid mà hệ thống truyền sang nhà mạng không chính xác',
        '61' => 'User mà hệ thống truyền sang nhà mạng không chính xác',
        '56' => 'TargetAccount tạm thời bị khóa do Charging sai nhiều lần.',
        '63' => 'Không tìm thấy giao dịch này (Hàm kiểm tra trạng thái gd)',
        '64' => 'Giải mã dữ liệu không thành công',
        '65' => 'Bạn kết nối đến nhà mạng nhiều hơn mức cho phép.',
        '99' => 'Chưa nhận được kết quả trả về từ nhà cung cấp mã thẻ. Liên hệ ban quản trị để được trợ giúp',
    );

    public static function cardCharging($soapClient, $serial_number, $card_number, $serviceProvider) {
        Yii::import('application.modules.currency.libs.class.vnpt.entries.CardCharging');
        Yii::import('application.modules.currency.libs.class.vnpt.entries.CardChargingResponse');

        $m_Target = Yii::app()->user->id;

        $CardCharging = new CardCharging();
        $CardCharging->m_UserName = self::$configs['m_UserName'];
        $CardCharging->m_PartnerID = self::$configs['m_PartnerID'];
        $CardCharging->m_MPIN = self::$configs['m_MPIN'];
        $CardCharging->m_Target = $m_Target;
        $CardCharging->m_Card_DATA = $serial_number . ":" . $card_number . ":" . "0" . ":" . $serviceProvider;
        $CardCharging->m_SessionID = "";
        $CardCharging->m_Pass = self::$configs['m_Pass'];
        $CardCharging->soapClient = $soapClient;

        $transid = self::$configs['m_PartnerCode'] . date("YmdHms") . rand(1111, 9999); //gen transaction id

        $CardCharging->m_TransID = $transid;
//        echo "<br/>Transaction id: " . $transid . "<br/>";
        //var_dump($CardCharging);
        //print_r($CardCharging -> m_SessionID);
        $CardChargingResponse = new CardChargingResponse();
        $CardChargingResponse = $CardCharging->CardCharging_();
//	print_r($CardChargingResponse);
//        print_r("Ket qua thuc hien giao dich: ");
//        echo "</br>";
//        print_r("Trang thai giao dich: " . $CardChargingResponse->m_Status);
//        echo "</br>";
//        print_r("Menh gia tra ve: " . $CardChargingResponse->m_RESPONSEAMOUNT);
//        echo "</br>";
//        print_r("Transaction id: " . $CardChargingResponse->m_TRANSID);
//        echo "<br/>----------------Ket thuc charging-----------------------<br/>";
        return $CardChargingResponse;
    }
}
?>