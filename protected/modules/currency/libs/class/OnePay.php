<?php
/**
 * Description of OnePay
 *
 * @author Qvv
 */
class OnePay {
    //Các giá trị được cấp bởi OnePay
    public static $configs = array(
        'vpcURL' => "http://mtf.onepay.vn/onecomm-pay/vpc.op?",
        'SECURE_SECRET' => "A3EFDFABA8653DF2342E8DAC29B51AF0",
        'MERCHANT_ID' => 'ONEPAY',
        'ACCESS_CODE' => 'D67342C2' 
    );

    public static $banks = array(
        '1' => 'Vietcombank',
        '2' => 'Techcombank',
        '3' => 'Tienphongbank',
        '4' => 'Vietinbank',
        '5' => 'VIB',
        '6' => 'DongAbank',
        '7' => 'HDBank',
        '8' => 'MB',
        '9' => 'VietA',
        '10' => 'MSB',
        '11' => 'EXIM',
        '12' => 'SHB',
        '14' => 'VPBANK',
        '15' => 'An Binh Bank',
        '16' => 'SACOMBANK',
        '17' => 'NAB',
        '18' => 'OCEANBANK',
        '19' => 'BIDV',
        '20' => 'SEABANK',
        '22' => 'BACA'
    );
    
    public static $allowValues = array(
        '50000' => '50.000',
        '100000' => '100.000',
        '200000' => '200.000',
        '500000' => '500.000',
        '1000000' => '1.000.000'
    );
    
    public static function getResponseMessage($responseCode){
        $message = '';
        switch ($responseCode) {
            case "0" :
                $historyUrl = Yii::app()->createAbsoluteUrl('currency/pay/historypay');
                $message = "Giao dịch thành công! Bạn có thể xem lại thông tin giao dịch <a href='{$historyUrl}'>tại đây.</a>";
                break;
            case "1" :
                $message = "Ngân hàng từ chối giao dịch - Bank Declined";
                $msg['error'][] = $message;
                break;
            case "3" :
                $message = "Mã đơn vị không tồn tại - Merchant not exist";
                $msg['error'][] = $message;
                break;
            case "4" :
                $message = "Không đúng access code - Invalid access code";
                $msg['error'][] = $message;
                break;
            case "5" :
                $message = "Số tiền không hợp lệ - Invalid amount";
                $msg['error'][] = $message;
                break;
            case "6" :
                $message = "Mã tiền tệ không tồn tại - Invalid currency code";
                $msg['error'][] = $message;
                break;
            case "7" :
                $message = "Lỗi không xác định - Unspecified Failure ";
                $msg['error'][] = $message;
                break;
            case "8" :
                $message = "Số thẻ không đúng - Invalid card Number";
                $msg['error'][] = $message;
                break;
            case "9" :
                $message = "Tên chủ thẻ không đúng - Invalid card name";
                $msg['error'][] = $message;
                break;
            case "10" :
                $message = "Thẻ hết hạn/Thẻ bị khóa - Expired Card";
                $msg['error'][] = $message;
                break;
            case "11" :
                $message = "Thẻ chưa đăng ký sử dụng dịch vụ - Card Not Registed Service(internet banking)";
                break;
            case "12" :
                $message = "Ngày phát hành/Hết hạn không đúng - Invalid card date";
                break;
            case "13" :
                $message = "Vượt quá hạn mức thanh toán - Exist Amount";
                break;
            case "21" :
                $message = "Số tiền không đủ để thanh toán - Insufficient fund";
                break;
            case "99" :
                //$message = "Người sủ dụng hủy giao dịch - User cancel";
                $message = "Bạn đã hủy giao dịch.";
                break;
            default :
                $message = "Giao dịch thất bại - Failured";
        }
        
        return $message;
    }
    
    public static function null2unknown($data){
        if ($data == ""){
            return "No Value Returned";
        } else {
            return $data;
        }
    }
}
?>
