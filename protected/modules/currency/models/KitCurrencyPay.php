<?php

Yii::import('application.modules.currency.models.db.BaseKitCurrencyPay');
class KitCurrencyPay extends BaseKitCurrencyPay
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * Hàm get giá trị gold của user
     * @param type $user_id
     * @param type $addition Nếu giá trị là TRUE là get những giao dịch nạp tiền
     * @param type $subtraction Nếu giá trị là TRUE là get những giao dịch trừ tiền
     * @return type
     */
    public static function getSumMoney($user_id){
        $criteria=new CDbCriteria;
        $criteria->select='sum(money) as money';
        $criteria->condition='user_id = '.$user_id;
        $result = self::model()->query($criteria);
        return $result->money;
    }
    
    /**
     * treatment
     * @param mixed $data
     * @return array 
     */
    public static function treatment($data)
    {
        $data = CJSON::decode ((CJSON::encode($data))); // Neu la object thi chuyen thanh array
        if (!isset ($data[0]))
            $data = KitFilm::treatmentRow($data);
        else {
            foreach ($data as $key => $value) {
                $data[$key] = KitFilm::treatmentRow($value);
            }
        }
        return $data;
    }
    
    public static function treatmentRow($row)
    {
        $urlTitle = Common::convertStringToUrl($row['title']);
        $row['url'] = Yii::app()->createUrl($urlTitle.'-film-'.$row['id']);
        return $row;
    }

}