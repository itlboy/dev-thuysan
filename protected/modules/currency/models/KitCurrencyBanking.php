<?php

Yii::import('application.modules.currency.models.db.BaseKitCurrencyBanking');
class KitCurrencyBanking extends BaseKitCurrencyBanking
{
    var $className = __CLASS__;
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}