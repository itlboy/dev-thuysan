<?php

Yii::import('application.modules.currency.models.db.BaseKitCurrencyCard');
Yii::import('application.modules.account.models.KitAccountStats');
Yii::import('application.modules.account.models.KitAccountLevel');
Yii::import('application.modules.currency.models.KitCurrencyHistory');
Yii::import('application.modules.currency.models.KitCurrencyCardfail');
Yii::import('application.modules.cms.models.KitCmsConfig');
class KitCurrencyCard extends BaseKitCurrencyCard
{
    var $className = __CLASS__;
    public static $money = 0;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getList($user_id = NULL){
        $cache_name = md5(__METHOD__ . '_' . $user_id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.user_id ='.$user_id);
            $criteria->order = 't.id';
            $result = self::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

    public static function getMoney(){
        return self::$money;
    }

    public static function processCard($data){
        $sussces = 0;
        # Cấu hình ketnoipay.com
        $config_ketnoipay = array(
            # Sau khi đăng nhập ở http://id.cbviet.net/ketnoipay , bạn có thể lấy được PartnerID tại menu thông tin tài khoản , rồi điền vào đây
            'TxtPartnerId'  => 406,
            # Sau khi đăng nhập ở http://id.cbviet.net/ketnoipay , bạn hãy thiết lập chữ kí giao dịch signal , rồi điền vào đây
            'TxtSignal' 	=> 'songkinhthay'
        );

        # Cấu hình database
        $config_server = array(
            'hosting'	 => true // Nếu sử dụng hosting điền true , sử dụng server điền false
        );

//            if(isset($_POST['submit'])){
//                $data['serial'] = letArray::get($_POST, 'serial');
//                $data['code'] = letArray::get($_POST, 'code');
//                $data['type']	= letArray::get($_POST, 'type');
//                $data['time']	= date('y-m-d H:i:s',time());
            # Thiết lập loại thẻ và cổng kết nối
        if($config_server['hosting']) {
            switch($data['type']) {
                case 'VTT':
                    $TxtUrl  = 'http://api2.cbviet.net/VIETTEL';
                    break;
                case 'VMS':
                    $TxtUrl  = 'http://api2.cbviet.net/VINAMOBI';
                    break;
                case 'VNP':
                    $TxtUrl  = 'http://api2.cbviet.net/VINAMOBI';
                    break;
                case 'GATE':
                    $TxtUrl  = 'http://api2.cbviet.net/GATE';
                    break;
                case 'VTC':
                    $TxtUrl  = 'http://api2.cbviet.net/VTC';
                    break;
            }
        } else {
            switch($data['type']) {
                case 'VTT':
                    $TxtUrl  = 'http://api2.cbviet.net:64990';
                    break;
                case 'VMS':
                    $TxtUrl  = 'http://api2.cbviet.net:64980';
                    break;
                case 'VNP':
                    $TxtUrl  = 'http://api2.cbviet.net:64980';
                    break;
                case 'GATE':
                    $TxtUrl  = 'http://api2.cbviet.net:64986';
                    break;
                case 'VTC':
                    $TxtUrl  = 'http://api2.cbviet.net:64987';
                    break;
            }
        }
        # Gửi thẻ lên máy chủ FPAY
        $TxtKey   = md5(trim($config_ketnoipay['TxtPartnerId'].$data['type'].$data['code'].$config_ketnoipay['TxtSignal']));
        $gateWay  = new gateWay($config_ketnoipay['TxtPartnerId'],$data['type'],$data['code'],$data['serial'],'',$TxtKey,$TxtUrl);
        $response = $gateWay->ReturnResult();
        # Xử lý kết quả
        if(strpos($response,'RESULT:10') !== false) // thẻ đúng
//        if(strpos($response,'RESULT:10') === false) // thẻ đúng
        {
            $sussces = 1;
            $TxtMenhGia	   = intval(str_replace('RESULT:10@','',$response));

            $TienDuocHuong = $TxtMenhGia;
//            $TienDuocHuong = 10000;
            // luu thong tin card nap thanh cong

            $gold = KitCmsConfig::convertMoneyToGold($TienDuocHuong,'card');

            $checknum = md5(Yii::app()->user->name.time());

            $card = new KitCurrencyCard();
            $card->user_id = Yii::app()->user->id;
            $card->username = Yii::app()->user->name;
            $card->type = $data['type'];
            $card->serial = $data['serial'];
            $card->code = $data['code'];
            $card->money = $TienDuocHuong;
            $card->time = $data['time'];
            $card->gold = $gold;
            $card->checknum = $checknum;
            $card->save();


            // luu thong tin lich su giao dich
            $history = new KitCurrencyHistory();
            $history->user_id = Yii::app()->user->id;
            $history->username = Yii::app()->user->name;
            $history->gold = $gold;
            $history->money = $TienDuocHuong;
            $history->content = 'Nạp tiền vào tài khoản';
            $history->time = $data['time'];
            $history->payment = 'card';
            $history->checknum = $checknum;
            $history->save();

            KitAccountStats::updateStats(Yii::app()->user->id);
            KitAccountLevel::updateLevel(Yii::app()->user->id);

            $result = 'Thẻ đúng và có mệnh giá '.$TxtMenhGia;
            Yii::app()->user->setFlash('success', $result);
            self::$money = $TienDuocHuong;
            return TRUE;
        }elseif(strpos($response,'RESULT:03') !== false || strpos($response,'RESULT:05') !== false || strpos($response,'RESULT:07') !== false || strpos($response,'RESULT:06') !== false) // thẻ sai
        {
            $result = 'Mã thẻ cào hoặc seri không chính xác.';
        } elseif (strpos($response,'RESULT:08') !== false)
        {
            $result = 'Thẻ đã gửi sang hệ thống rồi. Không gửi thẻ này nữa.';
        } elseif (strpos($response,'RESULT:12') !== false)
        {
            $result = 'Bạn phải nhập seri thẻ.';
        } elseif (strpos($response,'RESULT:11') !== false)
        {
            $result = 'Thẻ đã gửi sang hệ thống nhưng bị trễ.';
        } elseif (strpos($response,'RESULT:99') !== false || strpos($response,'RESULT:00') !== false || strpos($response,'RESULT:01') !== false || strpos($response,'RESULT:04') !== false || strpos($response,'RESULT:09') !== false)
        {
            $result = 'Hệ thống nạp thẻ đang bảo trì. Mã bảo trì là '.$response;
        } else {
            $result = 'Có lỗi xảy ra trong quá trình nạp thẻ. Vui lòng quay lại sau.';
        }

        if((int)$sussces == 0){
            $cardfail = new KitCurrencyCardfail();
            $cardfail->user_id = Yii::app()->user->id;
            $cardfail->username = Yii::app()->user->name;
            $cardfail->type = $data['type'];
            $cardfail->serial = $data['serial'];
            $cardfail->code = $data['code'];
            $cardfail->time = $data['time'];
            $cardfail->content = $result;
            $cardfail->save();
        }
        Yii::app()->user->setFlash('error', $result);
        return FALSE;
    }
}