<?php

Yii::import('application.modules.currency.models.db.BaseKitCurrencyBank');
class KitCurrencyBank extends BaseKitCurrencyBank
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}