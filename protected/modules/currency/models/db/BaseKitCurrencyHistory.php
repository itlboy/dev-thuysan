<?php

/**
 * This is the model class for table "{{kit_currency_history}}".
 *
 * The followings are the available columns in table '{{kit_currency_history}}':
 * @property integer $id
 * @property string $username
 * @property integer $user_id
 * @property double $money
 * @property double $gold
 * @property string $payment
 * @property string $content
 * @property string $checknum
 * @property string $time
 */
class BaseKitCurrencyHistory extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitCurrencyHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_currency_history}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('money, gold', 'numerical'),
			array('username, content, checknum', 'length', 'max'=>255),
			array('payment', 'length', 'max'=>50),
			array('time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, user_id, money, gold, payment, content, checknum, time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('money',$this->money);
		$criteria->compare('gold',$this->gold);
		$criteria->compare('payment',$this->payment,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('checknum',$this->checknum,true);
		$criteria->compare('time',$this->time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
		));
	}
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitCurrencyHistory::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitCurrencyHistory::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitCurrencyHistory::getLastest_'));
        Yii::app()->cache->delete(md5('KitCurrencyHistory::getPromotion_'));
    }

}
