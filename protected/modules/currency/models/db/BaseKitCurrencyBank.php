<?php

/**
 * This is the model class for table "{{kit_currency_bank}}".
 *
 * The followings are the available columns in table '{{kit_currency_bank}}':
 * @property integer $id
 * @property string $username
 * @property integer $user_id
 * @property string $type
 * @property integer $money
 * @property double $gold
 * @property string $checknum
 * @property integer $response_code
 * @property string $time
 */
class BaseKitCurrencyBank extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitCurrencyBank the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_currency_bank}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id', 'required'),
			array('user_id, money, response_code', 'numerical', 'integerOnly'=>true),
			array('gold', 'numerical'),
			array('username, checknum', 'length', 'max'=>255),
			array('type', 'length', 'max'=>250),
			array('time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, user_id, type, money, gold, checknum, response_code, time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('money',$this->money);
		$criteria->compare('gold',$this->gold);
		$criteria->compare('checknum',$this->checknum,true);
		$criteria->compare('response_code',$this->response_code);
		$criteria->compare('time',$this->time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
		));
	}
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitCurrencyBank::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitCurrencyBank::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitCurrencyBank::getLastest_'));
        Yii::app()->cache->delete(md5('KitCurrencyBank::getPromotion_'));
    }

}
