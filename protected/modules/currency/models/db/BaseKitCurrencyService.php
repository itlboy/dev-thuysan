<?php

/**
 * This is the model class for table "{{kit_currency_service}}".
 *
 * The followings are the available columns in table '{{kit_currency_service}}':
 * @property integer $id
 * @property string $title
 * @property string $code
 * @property integer $rate
 * @property string $unit
 * @property string $exchange_type
 * @property string $api_info
 * @property string $sorder
 * @property integer $status
 */
class BaseKitCurrencyService extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitCurrencyService the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_currency_service}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('unit, api_info', 'required'),
			array('rate, status', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>200),
			array('code, unit', 'length', 'max'=>20),
			array('exchange_type, sorder', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, code, rate, unit, exchange_type, api_info, sorder, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('rate',$this->rate);
		$criteria->compare('unit',$this->unit,true);
		$criteria->compare('exchange_type',$this->exchange_type,true);
		$criteria->compare('api_info',$this->api_info,true);
		$criteria->compare('sorder',$this->sorder,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
		));
	}
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitCurrencyService::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitCurrencyService::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitCurrencyService::getLastest_'));
        Yii::app()->cache->delete(md5('KitCurrencyService::getPromotion_'));
    }

}
