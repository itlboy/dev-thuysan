<?php

/**
 * This is the model class for table "{{kit_currency_banking}}".
 *
 * The followings are the available columns in table '{{kit_currency_banking}}':
 * @property integer $id
 * @property string $type
 * @property string $payment
 * @property string $payment_id
 * @property integer $payment_type
 * @property double $money
 * @property double $gold
 * @property string $send_to
 * @property string $transaction_id
 * @property string $transaction_info
 * @property string $transaction_status
 * @property string $checksum
 * @property string $checknum
 * @property string $content
 * @property string $username
 * @property string $creator
 * @property string $created_time
 * @property string $editor
 * @property string $updated_time
 * @property integer $status
 */
class BaseKitCurrencyBanking extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitCurrencyBanking the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_currency_banking}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('payment', 'required'),
			array('payment_type, status', 'numerical', 'integerOnly'=>true),
			array('money, gold', 'numerical'),
			array('type, payment_id', 'length', 'max'=>50),
			array('payment', 'length', 'max'=>500),
			array('send_to, transaction_info, checksum, checknum, username', 'length', 'max'=>255),
			array('transaction_id', 'length', 'max'=>100),
			array('transaction_status', 'length', 'max'=>5),
			array('creator, editor', 'length', 'max'=>11),
			array('content, created_time, updated_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, type, payment, payment_id, payment_type, money, gold, send_to, transaction_id, transaction_info, transaction_status, checksum, checknum, content, username, creator, created_time, editor, updated_time, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('payment',$this->payment,true);
		$criteria->compare('payment_id',$this->payment_id,true);
		$criteria->compare('payment_type',$this->payment_type);
		$criteria->compare('money',$this->money);
		$criteria->compare('gold',$this->gold);
		$criteria->compare('send_to',$this->send_to,true);
		$criteria->compare('transaction_id',$this->transaction_id,true);
		$criteria->compare('transaction_info',$this->transaction_info,true);
		$criteria->compare('transaction_status',$this->transaction_status,true);
		$criteria->compare('checksum',$this->checksum,true);
		$criteria->compare('checknum',$this->checknum,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('creator',$this->creator,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('editor',$this->editor,true);
		$criteria->compare('updated_time',$this->updated_time,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
		));
	}
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitCurrencyBanking::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitCurrencyBanking::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitCurrencyBanking::getLastest_'));
        Yii::app()->cache->delete(md5('KitCurrencyBanking::getPromotion_'));
    }

}
