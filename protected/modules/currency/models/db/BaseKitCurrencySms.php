<?php

/**
 * This is the model class for table "{{kit_currency_sms}}".
 *
 * The followings are the available columns in table '{{kit_currency_sms}}':
 * @property integer $id
 * @property integer $money
 * @property double $gold
 * @property string $phone
 * @property string $message
 * @property string $service
 * @property string $port
 * @property string $main
 * @property string $sub
 * @property string $checksum
 * @property string $checknum
 * @property string $content
 * @property string $username
 * @property string $creator
 * @property string $created_time
 * @property string $editor
 * @property string $updated_time
 * @property integer $status
 */
class BaseKitCurrencySms extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitCurrencySms the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_currency_sms}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('money, status', 'numerical', 'integerOnly'=>true),
			array('gold', 'numerical'),
			array('phone', 'length', 'max'=>50),
			array('message, service, port, main, sub, checksum, checknum, username', 'length', 'max'=>255),
			array('creator, editor', 'length', 'max'=>11),
			array('content, created_time, updated_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, money, gold, phone, message, service, port, main, sub, checksum, checknum, content, username, creator, created_time, editor, updated_time, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('money',$this->money);
		$criteria->compare('gold',$this->gold);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('service',$this->service,true);
		$criteria->compare('port',$this->port,true);
		$criteria->compare('main',$this->main,true);
		$criteria->compare('sub',$this->sub,true);
		$criteria->compare('checksum',$this->checksum,true);
		$criteria->compare('checknum',$this->checknum,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('creator',$this->creator,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('editor',$this->editor,true);
		$criteria->compare('updated_time',$this->updated_time,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
		));
	}
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitCurrencySms::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitCurrencySms::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitCurrencySms::getLastest_'));
        Yii::app()->cache->delete(md5('KitCurrencySms::getPromotion_'));
    }

}
