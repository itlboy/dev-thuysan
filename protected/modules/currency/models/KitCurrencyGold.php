<?php

Yii::import('application.modules.currency.models.db.BaseKitCurrencyGold');
class KitCurrencyGold extends BaseKitCurrencyGold
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * Hàm get giá trị gold của user
     * @param type $user_id
     * @param type $addition Nếu giá trị là TRUE là get những giao dịch nạp tiền
     * @param type $subtraction Nếu giá trị là TRUE là get những giao dịch trừ tiền
     * @return type
     */
    public static function getSumGold($user_id, $addition = TRUE, $subtraction = TRUE){
        $criteria=new CDbCriteria;
        $criteria->select='sum(gold) as gold';
        $criteria->condition='user_id = '.$user_id;
        
        if($addition == TRUE AND $subtraction == FALSE) // Cộng tiền
            $criteria->addCondition('gold > 0');
        elseif ($addition == FALSE AND $subtraction == TRUE) // Trừ tiền
            $criteria->addCondition('gold < 0');
        elseif ($addition == FALSE AND $subtraction == FALSE) // Không cộng không trừ thì trả về giá trị 0
            return 0;

        $result = self::model()->query($criteria);
        
        return $result->gold;
    }

    /**
     * treatment
     * @param mixed $data
     * @return array 
     */
    public static function treatment($data)
    {
        $data = CJSON::decode ((CJSON::encode($data))); // Neu la object thi chuyen thanh array
        if (empty($data))
            return array();
        elseif (!isset ($data[0]))
            $data = KitCurrencyGold::treatmentRow($data);
        else {
            foreach ($data as $key => $value) {
                $data[$key] = KitCurrencyGold::treatmentRow($value);
            }
        }
        return $data;
    }
    
    public static function treatmentRow($row)
    {
        return $row;
    }

}