<?php

Yii::import('application.modules.currency.models.db.BaseKitCurrencyHistory');
Yii::import('application.modules.account.models.KitAccountStats');
Yii::import('application.modules.account.models.KitAccountConfig');
Yii::import('application.modules.cms.models.KitCmsConfig');
class KitCurrencyHistory extends BaseKitCurrencyHistory
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    public function getMoney(){

    }

    public static function getList($user_id = NULL){
        $cache_name = md5(__METHOD__ . '_' . $user_id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.user_id ='.$user_id);
            $criteria->order = 't.id';
            $result = self::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

    public static function getSumMoney($user_id, $addition = TRUE, $subtraction = TRUE){
        $cache_name = md5(__METHOD__ . '_' . $user_id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE OR TRUE) {
            $db = Yii::app()->db;
            if($addition == TRUE AND $subtraction == TRUE){ // Tat ca giao dich
                $where = 'AND (money > 0 OR money < 0)';
            } elseif ($addition == TRUE AND $subtraction == FALSE){ // Cong tien
                $where = 'AND money > 0';
            } elseif ($addition == FALSE AND $subtraction == TRUE) { // Tru tien
                $where = 'AND money < 0';
            } else { // Khong lay gia tri
                $where = 'AND 1 < 0';
            }

            $sql = 'SELECT SUM(money) AS money_total FROM let_kit_currency_history WHERE user_id='.$user_id.' '.$where.' ';
//            echo $sql.'<br/>';
            $db->active = true;
            $result = $db->CreateCommand($sql)->queryRow();
            $db->active = false;
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

    /**
     * @static
     * @param $user_id
     * @param $field film_unlimit OR money
     * @param $money so tien nap
     * @param string $payment Hinh thuc thanh toan Card OR nganluong OR Baokim OR SMS
     * @param string $message Cau thong bao
     */
    public static function updateConfig($user_id,$field,$money, $payment = '',$message = ''){
        // Xu ly tru tien
        $currencyHistory = new KitCurrencyHistory();
        $currencyHistory->user_id = $user_id;
        $currencyHistory->time = date('Y-m-d H:i:s');
        $gold = KitCmsConfig::convertMoneyToGold($money,$payment);
        
        // get username
        $username = KitAccount::getField($user_id,'username');

        $currencyHistory->username = $username;
        $type = '';
        if ($field == 'film_unlimit') {
            $type = 'datetime';
            $currencyHistory->content = 'Gia hạn xem phim';
//            $currencyHistory->money = NULL;
            $currencyHistory->gold = -$gold;
        } elseif ($field == 'cs_vip') {
            $type = 'datetime';
            $currencyHistory->content = 'Gia hạn VIP cho Counter-Strike';
//            $currencyHistory->money = NULL;
            $currencyHistory->gold = -$gold;
        } else {
            $currencyHistory->money = $money;
            $currencyHistory->content = $message;
            $currencyHistory->gold = $gold;
        }
        $currencyHistory->payment = $payment;
        $currencyHistory->validate();
        $currencyHistory->save();

        $exchange = KitAccountConfig::exchange($field,$gold);
        KitAccountConfig::updateConfig($user_id,$field,$exchange,$type);

        KitAccountStats::updateStats($user_id);
    }
}