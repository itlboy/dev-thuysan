<?php

Yii::import('application.modules.currency.models.db.BaseKitCurrencyService');
class KitCurrencyService extends BaseKitCurrencyService
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get Details
     * @param int $code
     * @return object 
     */
    public static function getDetails($code) {
        $cache_name = md5(__METHOD__ . '_' . $code);
        $cache = Yii::app()->cache->get($cache_name); // Get cache

        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');
            $criteria->condition='code = "' . $code . '"';
            $result = self::model()->find($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
    
    // Chuyển từ số vàng sang đơn vị trong dịch vụ
    public static function exchangeGoldToService($gold, $percent) {
        return $gold * $percent / 100;
    }
    
    public static function getList ($conditions = array(), $limit = 20){
        $cache_name = md5(__METHOD__);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
            foreach ($conditions as $key => $value) {
                $criteria->condition = Common::addWhere($criteria->condition, 't.' . $key . ' = ' . $value);
            }

            $criteria->order = 't.id DESC';
            $criteria->limit = $limit;
            $result = self::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
   
    /**
     * treatment
     * @param mixed $data
     * @return array 
     */
    public static function treatment($data)
    {
        $data = CJSON::decode ((CJSON::encode($data))); // Neu la object thi chuyen thanh array
        if (!isset ($data[0]))
            $data = self::treatmentRow($data);
        else {
            foreach ($data as $key => $value) {
                $data[$key] = self::treatmentRow($value);
            }
        }
        return $data;
    }
    
    public static function treatmentRow($row)
    {
        $urlTitle = Common::convertStringToUrl($row['title']);
//        $row['url'] = Yii::app()->createUrl($urlTitle.'-article-'.$row['id']);
        $row['title'] = str_replace (array ('"', "'"), array ('&quot;', '&apos;'), $row['title'] );
        return $row;
    }

}