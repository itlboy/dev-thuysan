<?php

Yii::import('application.modules.currency.models.db.BaseKitCurrencyCardfail');
class KitCurrencyCardfail extends BaseKitCurrencyCardfail
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}