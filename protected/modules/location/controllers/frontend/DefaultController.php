<?php
/**
 * Module: category
 * Auth:
 * Date: 2012-03-16 11:24:17
 */
class DefaultController extends ControllerFrontend
{
	public function actionIndex()
	{
        $id = $_GET['id']?$_GET['id']:0;
        if(!$category = KitCategory::model()->findByPk($id)) throw new CHttpException(404,'Not found.');
        
		$this->render('index', array('category' => $category));
	}
}