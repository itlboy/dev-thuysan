<?php

class DefaultController extends ControllerBackend {

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new KitLocation;
        $model->module = Yii::app()->request->getQuery('module');
//        $module = Yii::app()->request->getQuery('module');
//        if ($module)
//            $model->module = $module;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['KitLocation'])) {
            $model->attributes = $_POST['KitLocation'];
            if ($model->validate()) {
                if ($model->parent_id == 0) {
                    $model->saveNode();
                } else {
                    $root = KitLocation::model()->findByPk($model->parent_id);
                    $model->appendTo($root);
                }

                $this->getAjaxStatus('success', 'Data saved');
                Yii::app()->end();
            } else {
                $errors = CActiveForm::validate($model);
                if (!empty($errors)) {
                    $this->getAjaxStatus('error', 'Check data', CJSON::encode($model), $errors);
                    Yii::app()->end();
                }
            }
        }

//        $this->layout = '//layouts/main';
        $this->layout = false;
        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        $parentOld = $model->parent_id;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['KitLocation'])) {
            $model->attributes = $_POST['KitLocation'];
            if ($model->validate()) {
                if ($model->saveNode()) {
                    // Cập nhật vị trí node nếu có thay đỗi
                    if ($parentOld != $model->parent_id) {
                        $modelCurrent = KitLocation::model()->findByPk($model->id);

                        if ($model->parent_id == 0) {
                            $modelCurrent->moveAsRoot();
                        } else {
                            $modelParent = KitLocation::model()->findByPk($model->parent_id);
                            $modelCurrent->moveAsLast($modelParent);
                        }
                    }

                    $this->getAjaxStatus('success', 'Data saved');
                    Yii::app()->end();

                    //$this->redirect(array('view', 'id' => $model->id));
                }
            } else {
                $errors = CActiveForm::validate($model);
                if (!empty($errors)) {
                    $this->getAjaxStatus('error', 'Vui lòng kiểm tra lại thông tin trước khi lưu', CJSON::encode($model), $errors);
                    Yii::app()->end();
                }
            }
        }

        $this->layout = false;
        $this->render('update', array(
            'model' => $model,
        ));
    }

    private function getAjaxStatus($status, $message='', $jrvalue='', $jrvalid='') {
        echo json_encode(array(
            'status' => $status,                // trạng thái success, warning, error
            'message' => $message,              // thông tin đã được lưu
            'jrvalue' => $jrvalue,              // danh sách các ID trả về
            'jrvalid' => $jrvalid,              // thông tin model valid error
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (!Yii::app()->request->isPostRequest)
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');

        $moduleName = Yii::app()->request->getPost('module');
        $modelName = Yii::app()->request->getPost('model');
        $field = Yii::app()->request->getPost('field');
        $value = (int) Yii::app()->request->getPost('value');
        $trash = Yii::app()->request->getPost('trash');
        $ids = array();

        if (array_key_exists($moduleName, Yii::app()->modules)) {
            Yii::import('application.modules.'.$moduleName.'.models.'.$modelName);
            $model = CActiveRecord::model($modelName)->findByPk((int) $id);

            if ($model === null)
                throw new CHttpException(404, 'The requested page does not exist.');

            $descendants = $model->descendants()->findAll();
            foreach ($descendants as $child)
                $ids[] = $child->id;

            $trash = Yii::app()->request->getPost('trash');
            if ($trash) {
                $criteria = new CDbCriteria;
                $criteria->addInCondition('id', $ids);

                $model->updateAll(array($field => $value), $criteria);
            } else {
                $model->deleteNode();
            }
            
            echo 'success';
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDeleteMultiRecord() {
        if (!Yii::app()->request->isPostRequest)
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');

        $moduleName = Yii::app()->request->getPost('module');
        $modelName = Yii::app()->request->getPost('model');
        $field = Yii::app()->request->getPost('field');
        $value = (int) Yii::app()->request->getPost('value');
        $trash = Yii::app()->request->getPost('trash');
        $ids = Yii::app()->request->getPost('ids');

        if (array_key_exists($moduleName, Yii::app()->modules) AND is_array($ids) AND !empty($ids)) {

            Yii::import('application.modules.'.$moduleName.'.models.'.$modelName);
            $model = CActiveRecord::model($modelName);

            if ($trash AND $field !== NULL) {
                foreach ($ids as $id) {
                    // dung truoc tiep bien $model se bao loi tren SQL
                    $modelFind = $model->findByPk((int)$id);
                    if ($modelFind !== NULL) {
                        $descendants = $modelFind->descendants()->findAll();
                        foreach ($descendants as $child)
                            $modelFind->updateByPk((int)$child->id, array($field => $value));
                        $modelFind->updateByPk((int)$id, array($field => $value));
                    }
                }
            } else {
                foreach ($ids as $id) {
                    // cho nay hinh nhu co gi do ko on
                    $modelFind = $model->findByPk((int)$id);
                    if ($modelFind !== NULL) {
                        $descendants = $modelFind->descendants()->findAll();
                        foreach ($descendants as $child)
                            $modelFind->deleteByPk((int)$child->id);
                        $modelFind->deleteNode();
                    }
                }
            }
            
            echo 'success';
        }
    }

//    /**
//     * Lists all models.
//     */
//    public function actionIndexDEL() {
//        $dataProvider = new CActiveDataProvider('KitLocation');
//        $this->render('index', array(
//            'dataProvider' => $dataProvider,
//        ));
//    }

    /**
     * Manages all models.
     */
    public function actionIndex() {
        $model = new KitLocation('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['KitLocation']))
            $model->attributes = $_GET['KitLocation'];

        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = KitLocation::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'kit-Location-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionSort() {
        if (!Yii::app()->request->isAjaxRequest)
            Yii::app()->end();

        $id = Yii::app()->request->getPost('id');
        $sort = Yii::app()->request->getPost('sort');

        $data = Yii::app()->db->createCommand()
                ->select('prev.id prevId, curr.id currId, next.id nextId')
                ->from('{{kit_Location}} curr')
                ->leftJoin('{{kit_Location}} prev', 'prev.rgt = curr.lft-1')
                ->leftJoin('{{kit_Location}} next', 'next.lft = curr.rgt+1')
                ->where('curr.id=:id', array(':id' => $id))
                ->queryRow();

        if ($data == NULL)
            throw new CHttpException(404,'The requested page does not exist.');

        $modelCurr = KitLocation::model()->findByPk((int) $data['currId']);
        if ($sort == 'up' && $data['prevId']) {
            $modelPrev = KitLocation::model()->findByPk((int) $data['prevId']);
            $modelCurr->moveBefore($modelPrev);
        }
        if ($sort == 'down' && $data['nextId']) {
            $modelNext = KitLocation::model()->findByPk((int) $data['nextId']);
            $modelCurr->moveAfter($modelNext);
        }

        //MCore::getAjaxStatus('success');
        echo true;
        Yii::app()->end();
    }

    public function actionStatus() {
        if (!Yii::app()->request->isAjaxRequest)
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');

        $id = (int) Yii::app()->request->getPost('id');
        $value = (int) Yii::app()->request->getPost('value');
        $modelFind = KitLocation::model()->findByPk($id);

        if ($modelFind === null)
            throw new CHttpException(404,'The requested page does not exist.');

        $exec = KitLocation::model()->updateByPk($id, array($this->status => $value));

        // cập nhật các record con
        $descendants = $modelFind->descendants()->findAll('id!=root');
        foreach ($descendants as $child)
            $model->updateByPk((int)$child->id, array($this->status => $value));

        echo true;
        Yii::app()->end();
    }

}
