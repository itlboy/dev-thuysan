<?php

/**
 * This is the model class for table "{{kit_location}}".
 *
 * The followings are the available columns in table '{{kit_location}}':
 * @property string $id
 * @property string $type
 * @property string $name
 * @property string $alias
 * @property string $parent_id
 * @property string $description
 * @property string $layout
 * @property string $module
 * @property string $lft
 * @property string $rgt
 * @property integer $level
 * @property string $root
 * @property string $created_time
 * @property string $updated_time
 * @property string $sorder
 * @property integer $promotion
 * @property integer $status
 * @property integer $trash
 */
class BaseKitLocation extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitLocation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_location}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('level, promotion, status, trash', 'numerical', 'integerOnly'=>true),
			array('type, layout', 'length', 'max'=>45),
			array('name, alias', 'length', 'max'=>150),
			array('parent_id, lft, rgt, root, sorder', 'length', 'max'=>10),
			array('description', 'length', 'max'=>500),
			array('module', 'length', 'max'=>20),
			array('created_time, updated_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, type, name, alias, parent_id, description, layout, module, lft, rgt, level, root, created_time, updated_time, sorder, promotion, status, trash', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('parent_id',$this->parent_id,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('layout',$this->layout,true);
		$criteria->compare('module',$this->module,true);
		$criteria->compare('lft',$this->lft,true);
		$criteria->compare('rgt',$this->rgt,true);
		$criteria->compare('level',$this->level);
		$criteria->compare('root',$this->root,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_time',$this->updated_time,true);
		$criteria->compare('sorder',$this->sorder,true);
		$criteria->compare('promotion',$this->promotion);
		$criteria->compare('status',$this->status);
		$criteria->compare('trash',$this->trash);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
		));
	}
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitLocation::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitLocation::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitLocation::getLastest_'));
        Yii::app()->cache->delete(md5('KitLocation::getPromotion_'));
    }

}
