<?php

class LocationModule extends BaseModule
{    
    public $menuList = array(
        array(
            'name' => 'Tạo vị trí',
            'url' => '/location/default/create',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'type' => 'dialog',
            'grid' => 'kit_location_grid',
        ),
        array(
            'name' => 'Quản lý vị trí',
            'url' => '/location/default/index',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Quản lý danh mục vị trí',
            'type' => 'direct'
        ),
    );

	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'location.models.*',
			'location.components.*',
		));
	}
}
