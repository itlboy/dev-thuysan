<?php

class treeCategory extends Widget
{
    public $view = '';
    public $module = 'article';
    public $promotion = NULL;

    public function init()
    {
        if ($this->view == '') $this->view = __CLASS__;
        Yii::import('category.models.KitCategory');
    }
 
    public function run()
    {
        $data = KitCategory::getList($this->module, $this->promotion, 'array');
        $data = KitCategory::treatment($data);
        $this->render($this->view, array(
            'data' => $data,
        ));
    }
}