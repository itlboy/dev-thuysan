<?php
$catList = array(array('label' => 'THỂ LOẠI'));
foreach ($data as $key => $parent) {
    if ($parent['parent_id'] == 0) {
        $catList[] = array('label' => $parent['name'], 'url' => $parent['url']);
    }
}
?>
<?php $this->widget('bootstrap.widgets.TbMenu', array(
    'type'=>'list',
    'items'=>$catList,
)); ?>
