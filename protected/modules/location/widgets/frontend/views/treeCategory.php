<div id="nav">
    <ul class="tabs">
        <li class="i-menu current first"><a class="pro joc-cate-1" href="<?php echo Yii::app()->request->baseUrl; ?>">Trang chủ</a></li>
        <?php foreach ($data as $key => $parent): ?>
            <?php if ($parent['parent_id'] == 0) : ?>
                <li class="i-menu">
                    <a class="pro joc-cate-<?php echo $parent['id']; ?>" href="<?php echo $parent['url']; ?>"><?php echo $parent['name']; ?></a>
                    <ul class="joc-<?php echo $parent['id']; ?>">
                        <?php foreach ($data as $children): ?>
                            <?php if ($children['parent_id'] == $parent['id']) : ?>
                                <li><a href="<?php echo $children['url']; ?>"><?php echo $children['name']; ?></a></li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ul>
</div>
