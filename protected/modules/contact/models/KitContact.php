<?php
Yii::import('application.modules.contact.models.db.BaseKitContact');
class KitContact extends BaseKitContact{
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    public function attributeLabels() {
        return array_merge(parent::attributeLabels(), array(
            'title' => 'Tiêu đề',
            'status' => 'Kích hoạt ?',

            'fullname' => 'Họ và tên',
            'tags' => 'Thẻ',

            'categories' => 'Danh mục'
        ));
    }

}