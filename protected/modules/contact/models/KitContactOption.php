<?php

Yii::import('application.modules.contact.models.db.BaseKitContactOption');
class KitContactOption extends BaseKitContactOption {

    var $className = __CLASS__;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getContact2Option($contactId) {
        $result = array();
        $data = self::findAll('contact_id=:contactId', array(':contactId' => $contactId));
        foreach ($data as $item2Type)
            $result[] = $item2Type->option_id;
        return $result;
    }

    /**
     * Create
     * @param array $options
     * @param int $itemId 
     */
    public static function createContactOption($options = array(), $itemId) {
        if (! is_array($options)) return FALSE;
        foreach ($options as $optionId) {
            $item2Opt = new KitContactOption;
            $item2Opt->option_id = $optionId;
            $item2Opt->contact_id = $itemId;
            $item2Opt->save();
        }
    }

    /**
     * Delete
     * @param array $options
     * @param int $itemId 
     */
    public static function deleteContactOption($options = array(), $itemId) {
        if (! is_array($options)) return FALSE;
        foreach ($options as $optionId) {
            self::model()->deleteAll('option_id=:optionId AND contact_id=:contactId', array(
                ':optionId' => $optionId,
                ':contactId' => $itemId,
            ));
        }
    }
}