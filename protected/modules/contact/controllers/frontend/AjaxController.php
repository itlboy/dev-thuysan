<?php
Yii::import('application.extensions.mail.ESmtp');
Yii::import('application.vendors.helper.letText');
Yii::import('application.modules.mail.models.KitMailSender');

    Class AjaxController extends ControllerFrontend{

        public function actionIndex(){
            if(isset($_POST['Contact'])){
                $contact = new KitContact();
                $fullname = $_POST['Contact']['name'];
                $email = $_POST['Contact']['mail'];
                $title = $_POST['Contact']['title'];
                $content = $_POST['Contact']['content'];
                $time = date("Y-m-d H:i:s");
                if(Yii::app()->user->isGuest){
                   $user_id = NULL;
                }else{
                    $user_id = Yii::app()->user->id;
                }
                if($contact->validate()){
                    $contact->title = $title;
                    $contact->fullname = $fullname;
                    $contact->creator = $user_id;
                    $contact->created_time = $time;
                    $contact->content = $content;
                    $contact->email = $email;

                    if($contact->save()){
                        $mailHtml = "<b>Tiêu đề: ".$title."</b><br />"; //HTML Body
                        $mailHtml = "<b>Email: ".$email."</b><br />"; //HTML Body
                        $mailHtml .= 'Nội dung: '.$content;
                        $data = array(
                            'subject' => 'Thông báo',
                            'html' => $mailHtml,
                            'to' => array(
                                'sales@worldofbeolove.com' => 'Thông báo có người liên hệ',
                            ),
                        );
                       KitMailSender::sendMail($data);
                       echo json_encode(array(
                           'result' => 'true',
                           'mess' => 'Bạn đã gửi thành công!'
                       ));
                   }else{
                        echo json_encode(array(
                            'result' => 'false',
                            'mess' => 'Email của bạn không hợp lệ!',
                        ));
                    }
                }
            }
        }
    }

?>