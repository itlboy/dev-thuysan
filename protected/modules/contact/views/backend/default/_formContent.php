
<div class="box grid_16" style="margin-top: 10px;">
    <div class="toggle_container">
        <div class="block">
            <h2 class="section"><?php echo Yii::t('global', 'General'); ?></h2>
            
                
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'title'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'title'); ?>
                    <?php echo $form->error($model, 'title'); ?>
                </div>
            </fieldset>
                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'email'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'email'); ?>
                    <?php echo $form->error($model, 'email'); ?>
                </div>
            </fieldset>
                            
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'fullname'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'fullname'); ?>
                    <?php echo $form->error($model, 'fullname'); ?>
                </div>
            </fieldset>
                                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'categories'); ?></label>
                <div>
                    <?php
                    echo $form->checkBoxList($model, 'categories', CHtml::listData(KitCategory::model()->getCategoryOptions($this->module->id), 'id', 'name'), array(
                        'separator' => '',
                        'template' => '<div>{input} {label}</div>'));
                    ?>
                    <?php echo $form->error($model, 'categories'); ?>
                </div>
            </fieldset>

        </div>
    </div>
</div>



<div class="box grid_16">
    <?php echo $form->error($model,'content'); ?>
    <?php        //echo $form->textArea($model, 'content', array('rows' => 6, 'cols' => 50, 'class' => 'tinymce'));
        $this->widget('ext.elrtef.elRTE', array(
            'model' => $model,
            'attribute' => 'content',
            'options' => array(
                    'doctype' => 'js:\'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\'',
                    'cssClass' => 'el-rte',
                    'cssfiles' => array('css/elrte-inner.css'),
                    'absoluteURLs'=>true,
                    'allowSource' => true,
                    'lang' => 'vi',
                    'styleWithCss'=>'',
                    'height' => 400,
                    'fmAllow'=>true, //if you want to use Media-manager
                    'fmOpen'=>'js:function(callback) {$("<div id=\"elfinder\" />").elfinder(%elfopts%);}',//here used placeholder for settings
                    'toolbar' => 'maxi',
            ),
            'elfoptions' => array( //elfinder options
                'url' => 'auto',  //if set auto - script tries to connect with native connector
                'passkey' => 'mypass', //here passkey from first connector`s line
                'lang' => 'vi',
                'dialog' => array('width' => '900','modal' => true, 'title' => 'Media Manager'),
                'closeOnEditorCallback' => true,
                'editorCallback' => 'js:callback'
            ),
        ));
    ?>
</div>
