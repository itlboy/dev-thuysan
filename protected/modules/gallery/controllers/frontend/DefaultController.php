<?php
/**
 * Module: gallery
 * Auth: nguago@let.vn
 * Website: http://let.vn
 */

class DefaultController extends ControllerFrontend
{
	public function actionIndex()
	{

        $view = 'index';
        $command = Yii::app()->db->CreateCommand();

        if(isset($_GET['id'])){

            $idcate = letArray::get($_GET,'id');
            $command->select('*')
                ->from('let_kit_category')
                ->where('module="gallery" AND parent_id='.$idcate)
                ->order('name');
            $data = $command->queryAll();
            if(empty($data)) $view = 'view';
        } else {
            $command->select('*')
                ->from('let_kit_category')
                ->where('module="gallery" AND parent_id=0')
                ->order('name');
            $data = $command->queryAll();
        }
		$this->render($view,array(
            'data' => $data,
            'idcate' => !empty($idcate) ? $idcate : NULL,
        ));
	}
}