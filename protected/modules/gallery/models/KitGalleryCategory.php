<?php

Yii::import('application.modules.gallery.models.db.BaseKitGalleryCategory');
class KitGalleryCategory extends BaseKitGalleryCategory{

    var $className = __CLASS__;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function get($itemId) {
        $result = array();
        $data = self::findAll('gallery_id=:itemId', array(':itemId' => $itemId));
        foreach ($data as $item2Type)
            $result[] = $item2Type->category_id;
        return $result;
    }

    /**
     * Create
     * @param array $categories
     * @param int $itemId 
     */
    public static function create($categories = array(), $itemId) {
        if (! is_array($categories)) return FALSE;
        foreach ($categories as $categoryId) {
            $item2Cat = new KitGalleryCategory;
            $item2Cat->category_id = $categoryId;
            $item2Cat->gallery_id = $itemId;
            $item2Cat->save();
        }
    }

    /**
     * Delete
     * @param array $categories
     * @param int $itemId 
     */
    public static function del($categories = array(), $itemId) {
        if (! is_array($categories)) return FALSE;
        foreach ($categories as $categoryId) {
            self::model()->deleteAll('category_id=:categoryId AND gallery_id=:galleryId', array(
                ':categoryId' => $categoryId,
                ':galleryId' => $itemId,
            ));
        }
    }
}