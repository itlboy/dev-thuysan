<?php
Yii::import('application.modules.gallery.models.db.BaseKitGallery');
class KitGallery extends BaseKitGallery{
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    public function attributeLabels() {
    return array_merge(parent::attributeLabels(), array(
        'title' => 'Tiêu đề',
        'status' => 'Kích hoạt ?',
        'trash' => 'Thùng rác',
        'image' => 'Ảnh đại diện',
        'intro' => 'Giới thiệu',
        'content' => 'Nội dung',
        'from_time' => 'Thời gian bắt đầu',
        'to_time' => 'Thời gian kết thúc',
        'promotion' => 'Tiêu điểm',
        'sorder' => 'Thứ tự',
        'tags' => 'Thẻ',
        'focus' => 'Tin hot ?',
        'author' => 'Tác giả',
        'source' => 'Nguồn',
        'categories' => 'Danh mục'
    ));
}


}