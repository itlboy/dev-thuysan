<?php if(!empty($data)): ?>
<?php foreach($data as $value): ?>
    <a class="fancybox" href="<?php echo Common::getImageUploaded('gallery/large/'.$value['image']); ?>" data-fancybox-group="gallery" title="<?php echo $value['title']; ?>" >
        <img src="<?php echo Common::getImageUploaded('gallery/medium/'.$value['image']); ?>" alt="" style="border: 1px solid #cccccc" />
    </a>
    <?php endforeach; ?>
<?php endif;  ?>