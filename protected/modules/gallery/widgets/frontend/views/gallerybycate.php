<?php if(!empty($data)): ?>
    <?php foreach($data as $value): ?>
        <a class="fancybox" href="<?php echo Common::getImageUploaded('gallery/origin/'.$value['image']); ?>" data-fancybox-group="gallery" title="<?php echo $value['title']; ?>" >
            <img src="<?php echo Common::getImageUploaded('gallery/medium/'.$value['image']); ?>" alt="" style="border: 1px solid #cccccc" />
        </a>
    <?php endforeach; ?>
<?php endif;  ?>
<?php if($this->page == TRUE): ?>
<style type="text/css">
    .div_page .page a,.div_page .next a,.div_page .previous a{ color: #ffffff !important;}
    .div_page .selected a{ background: white !important; color: black !important;}
</style>
<div style="float: left; padding: 10px 0px; width:100%;margin-top: 5px;" class="div_page">
    <?php $this->widget("CLinkPager",array('pages'=>$page)); ?>
</div>
<?php endif; ?>