<?php

class gallery_bovn extends Widget {

    public $view = '';
    public $category = NULL;
    public function init(){
        if(empty($this->view)){
            $this->view = __CLASS__;
        }
    }
    public function run() {

        $command = Yii::app()->db->CreateCommand();
        if(isset($this->category) AND !empty($this->category)){
            $idcate = $this->category;
            $command->select('*')
                ->from('let_kit_category')
                ->where('module="gallery" AND parent_id='.$idcate)
                ->order('name');
            $data = $command->queryAll();
            $command->reset();
            if(empty($data)){
                $command->select('t1.*')
                    ->from('let_kit_gallery t1')
                    ->leftJoin('let_kit_gallery_category t2','t1.id = t2.gallery_id')
                    ->leftJoin('let_kit_category t3','t3.id = t2.category_id')
                    ->where('t3.id ='.$idcate);
                $data = $command->queryAll();
                if(empty($data)) return FALSE;
                $this->view = 'gallery_bovn_view';
                $command->reset();
            }
        } else {
            $command->select('*')
                ->from('let_kit_category')
                ->where('module="gallery" AND parent_id=0')
                ->order('name');
            $data = $command->queryAll();
        }
        $this->render($this->view,array(
            'data' => $data,
        ));
    }
}
