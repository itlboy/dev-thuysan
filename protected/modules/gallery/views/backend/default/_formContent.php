<div class="box grid_16" style="margin-top: 10px;">
    <div class="toggle_container">
        <div class="block">
            <h2 class="section"><?php echo Yii::t('global', 'General'); ?></h2>
            
                
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'title'); ?></label>
                <div>
                    <?php echo $form->textField($model, 'title'); ?>
                    <?php echo $form->error($model, 'title'); ?>
                    <div class="required_tag tooltip hover left" title="<?php echo Yii::t('global', 'Required'); ?>"></div>
                </div>
            </fieldset>
                                        
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'intro'); ?></label>
                <div class="clearfix">
                    <?php echo $form->textArea($model, 'intro', array('class' => 'tooltip', 'title' => 'Nhập thông tin...')); ?>
                    <?php echo $form->error($model, 'intro'); ?>
                </div>
            </fieldset>
                            
            <fieldset class="label_side">
                <label><?php echo $form->labelEx($model, 'categories'); ?></label>
                <div>
                    <?php
                    echo $form->checkBoxList($model, 'categories', CHtml::listData(KitCategory::model()->getCategoryOptions($this->module->id), 'id', 'name'), array(
                        'separator' => '',
                        'template' => '<div>{input} {label}</div>'));
                    ?>
                    <?php echo $form->error($model, 'categories'); ?>
                </div>
            </fieldset>

        </div>
    </div>
</div>


<div class="box grid_16">
    <div class="toggle_container">
        <div class="block">
            <div class="columns clearfix">
                <div class="col_25">
                    <fieldset>
                        <label><?php echo $form->labelEx($model, 'sorder'); ?></label>
                        <div>
                            <?php echo $form->textField($model, 'sorder', array('class' => 'tooltip autogrow', 'title' => 'Số càng nhỏ thì thứ tự càng cao. Mặc định: 500', 'placeholder' => 'Nhập 1 số')); ?>
                            <?php echo $form->error($model, 'sorder'); ?>
                        </div>
                    </fieldset>
                </div>
                <div class="col_25">
                    <fieldset>
                        <label><?php echo $form->labelEx($model, 'promotion'); ?></label>
                        <div>
                            <div class="jqui_radios">
                                <?php echo $form->radioButtonList($model, 'promotion', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                                <?php echo $form->error($model, 'promotion'); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col_25">
                    <fieldset>
                        <label><?php echo $form->labelEx($model, 'status'); ?></label>
                        <div>
                            <div class="jqui_radios">
                                <?php echo $form->radioButtonList($model, 'status', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                                <?php echo $form->error($model, 'status'); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col_25">
                    <fieldset>
                        <label><?php echo $form->labelEx($model, 'trash'); ?></label>
                        <div>
                            <div class="jqui_radios">
                                <?php echo $form->radioButtonList($model, 'trash', array(1 => 'Có', 0 => 'Không'), array('separator' => '')); ?>
                                <?php echo $form->error($model, 'trash'); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>

