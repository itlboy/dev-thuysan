<?php

class listOldNew extends Widget
{
    public $category = NULL;
    public $view = '';
    public $title = '';
    public $limit = 10;
    public $classiadsId;
    public $type = 'new'; //  { old ; new }

    public function init()
    {
        if ($this->view == '') $this->view = __CLASS__;
        Yii::import('classiads.models.KitClassiads');
    }
 
    public function run()
    {
        $result = KitClassiads::model()->findByPk($this->classiadsId);
        if(empty($result))
            return FALSE;
        $criteria = new CDbCriteria;
        $criteria->select = Common::getFieldInTable(KitClassiads::model()->getAttributes(), 't.');
        $criteria->condition = '';
        $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
        switch($this->type)
        {
            case 'new':
                $criteria->condition = Common::addWhere($criteria->condition, 't.created_time > DATE("' .$result->created_time.'")' );
                $criteria->order = 't.id ASC';
                break;
            case 'old':
                $criteria->condition = Common::addWhere($criteria->condition, 't.created_time < DATE("' .$result->created_time.'")' );
                $criteria->order = 't.id DESC';
                break;
        }
        if ($this->category !== NULL AND $this->category >= 0) {
            $catListObject = KitCategory::getListInParent('classiads', $this->category);
            $catList = array($this->category);
            foreach ($catListObject as $cat) {
                $catList[] = $cat->id;
            }
            $criteria->join = "INNER JOIN {{kit_classiads_category}} t2 ON t.id=t2.classiads_id";
            $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', $catList).')');
        }
        $criteria->limit = $this->limit;
        $data = KitClassiads::model()->findAll($criteria);
        if (empty($data))
            return FALSE;

        $data = KitClassiads::treatment($data);
        $data = array_slice($data, 0, $this->limit);
        $this->render($this->view, array(
            'data' => $data,
        ));
    }
}