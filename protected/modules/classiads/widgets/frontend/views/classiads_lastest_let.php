
<?php if(!empty($data)): ?>
<table class="items table table-striped table-bordered table-condensed">
    <tbody>
        <?php foreach($data as $value): ?>
            <tr>
                <td width="70">
                    <?php
                    if ($value['image']) {
                        $a_image = Common::getImageUploaded('classiads/small/' . $value['image']);
                    } else {
                        $a_image = Yii::app()->theme->baseUrl . "/images/no_image.jpg";
                    }
                    ?>
                    <img src="<?php echo $a_image; ?>" alt="<?php echo $value['title']; ?>" title="<?php echo $value['title']; ?>" style="width:70px;" />
                </td>
                <td>
                    <a href="<?php echo $value['url']; ?>" title="<?php echo $value['title']; ?>"><b><?php echo $value['title']; ?></b></a><span style="color: #999;font-style: italic;font-size: 11px;"><?php echo ' - '.date('d/m/Y H:i',strtotime($value['created_time'])); ?></span>
                    <p><?php echo letText::limit_chars(strip_tags($value['content']),200); ?></p>
                    <p style="font-size: 11px;"><?php
                        $stats = KitStats::getDetails('classiads',$value['id']);
                        if(!empty($stats)){
                            echo $stats['view_total'];
                        } else {
                            echo '0';
                        }
                    ?> lượt xem -
                    <?php
                        if(isset($stats['comment_total']) AND !empty($stats['comment_total'])){
                            echo $stats['comment_total'];
                        } else {
                            echo 0;
                        }

                        ?> bình luận
                    </p>
                </td>
                <?php if(!Yii::app()->user->isGuest): ?>
                    <td class="button-column">
                        <?php if(($value['creator'] == Yii::app()->user->id) OR in_array(8,json_decode(Yii::app()->user->getState('group')))): ?>
                            <a href="<?php echo $value['url']; ?>" rel="tooltip" class="view" data-original-title="View"><i class="icon-eye-open"></i></a>
                            <a href="<?php echo Yii::app()->CreateUrl('classiads/create/index',array('id' => $value['id'])); ?>" rel="tooltip" class="update" data-original-title="Update"><i class="icon-pencil"></i></a>
                        <?php endif; ?>
                        <?php if(in_array(8,json_decode(Yii::app()->user->getState('group')))): ?>
                            <a href="javascript:;" rel="tooltip" onclick="return deleteClassiads(<?php echo $value['id']; ?>,this)" class="delete" data-original-title="Delete"><i class="icon-trash"></i></a>
                        <?php endif; ?>
                    </td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
    <tr>
        <td colspan="6"><?php $this->widget("CLinkPager",array('pages'=>$page)); ?></td>
    </tr>
    </tbody>
</table>
<?php else: ?>

<?php endif; ?>
<script type="text/javascript">
    function deleteClassiads(id,obj){
        if(confirm('Bạn muốn xóa ?')){
            $.ajax({
                type:'POST',
                url:'<?php echo Yii::app()->createUrl('classiads/ajax/delete'); ?>',
                data:{id:id},
                success:function(data){
                    if(data == 'true')
                    {
                        $(obj).parent().parent().remove();
                    }
                }
            });
        }
    }
</script>