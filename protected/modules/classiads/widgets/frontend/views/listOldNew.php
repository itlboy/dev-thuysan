<div class="ct-other ct-other-left">
    <h4><span><?php echo $this->title; ?></span></h4>
    <div class="clear"></div>
    <div id="box1">
        <ul>
            <?php foreach ($data as $key => $value): ?>
            <li><a href="<?php echo $value['url']; ?>" title="<?php echo $value['title']; ?>"><?php echo $value['title']; ?></a><span>(<?php echo date('d/m/Y', strtotime($value['created_time'])); ?>)</span></li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>