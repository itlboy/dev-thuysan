<div class="ct-classiads">
    <h1 class="ct-classiads-title header_<?php echo $this->index; ?>">
        <a href="<?php echo !empty($this->url) ? $this->url : 'javascript:;' ?>" title="<?php echo $this->title; ?>"><?php echo $this->title; ?><!--<span>(200)</span>--></a>
    </h1>
    <div class="ct-classiads-content">
        <table cellpadding="0" cellspacing="0" class="tbl-classiads">
            <tr>
                <td>
                    <a href="<?php echo $this->url.'?type=sell'; ?>" class="ct-classiads-tp" title="Cần bán">Cần bán <!--<span>(100)</span>--></a>
                </td>
                <td>
                    <a href="<?php echo $this->url.'?type=buy'; ?>" class="ct-classiads-tp" title="Cần mua">Cần mua <!--<span>(100)</span>--></a>
                </td>
            </tr>
        </table>
        <ul class="ul-classiads">
            <?php if(!empty($data)): ?>
            <?php foreach($data as $item): ?>
                <li><a href="<?php echo $item['url']; ?>" title="<?php echo $item['title']; ?> ( <?php echo date('H:i:s d-m-Y',strtotime($item['created_time'])); ?> )"><?php echo $item['title']; ?></a></li>
            <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
</div>