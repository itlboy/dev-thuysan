<div class="ct-classiads-list" style="margin-top: 15px;">
    <p class="ct-classiads-list-title"><a href="javascript:;"><?php echo $this->title; ?></a></p>
    <table cellpadding="0" cellspacing="0" class="tbl-ct-classiads-list">
        <tr style="background-color: #eaf3ff">
            <td width="30px" align="center" class="first">
            </td>
            <td>
                <strong>Tiêu đề tin</strong>
            </td>
            <td width="100px" align="center" class="last">
                <strong>Ngày đăng</strong>
            </td>
<!--            <td width="100px" align="center" class="last">-->
<!--                <strong>Người đăng</strong>-->
<!--            </td>-->
        </tr>
        <?php

            if(!empty($data)):
            $i = 0;
            foreach($data as $item):
                if($i % 2 !=0 ):
                    $class = 'row1';
                else:
                    $class = '';
                endif;
            ?>
            <tr class="<?php echo $class; ?>" align="center" valign="middle">
                <td class="first">
                    <?php
                        $cls =  intval((time()-strtotime($item['created_time']))/(60*60*24));
                        if($cls < 5){
                          echo  $img = '<img src="'.Yii::app()->theme->baseUrl.'/images/new.gif" height="17px" title="New" />';
                        } else {
                          echo  $img = '<img src="'.Yii::app()->theme->baseUrl.'/images/bullet_2.gif" />';
                        }
                    ?>
                </td>
                <td align="left">
                    <a href="<?php echo $item['url']; ?>" class="a-classiad-item" title="<?php echo $item['title']; ?>"><?php echo $item['title']; ?></a>
                </td>
                <td align="center" class="last">
                    <span><?php echo date('d-m-Y',strtotime($item['created_time'])); ?></span>
                </td>
<!--                <td class="last" align="center">-->
<!--                    <span>--><?php //echo date('d-m-Y',strtotime($item['created_time'])); ?><!--</span>-->
<!--                </td>-->
            </tr>
        <?php
            $i++;
            endforeach;
            endif;
        ?>
    </table>
</div>