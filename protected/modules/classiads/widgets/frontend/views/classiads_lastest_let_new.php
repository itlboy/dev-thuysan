<?php if($this->tabs !== array()): ?>
<?php $divId = 'result_' . rand(1000000, 9999999); ?>
<script type="text/javascript">
    $('#<?php echo $divId ?>_tabs a.btn').live('click',function(){
        var link = $(this).data("link");
        ajaxLoadFile(link,'<?php echo $divId; ?>');
        $('#<?php echo $divId ?>_tabs a').removeClass('active');
        $(this).addClass('active');
        return false;
    });
</script>
<div id="<?php echo $divId ?>_tabs">
    <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
    'buttons'=> $this->tabs,
)); ?>
</div>
<div id="<?php echo $divId; ?>">
<?php endif; ?>
    <?php if(!empty($data)): ?>
        <div class="video-sidebar">
            <ul class="let_kit_film_hot" >
                <?php foreach ($data as $item): ?>
                <li class="video-list-item recommended-video-item">
                    <a class="related-video yt-uix-sessionlink" href="<?php echo $item['url']; ?>">
                        <span class="video-thumb ux-thumb yt-thumb-square-46 " style="float: left; margin-right: 10px;">
                            <span class="yt-thumb-clip">
                                <span class="yt-thumb-clip-inner">
                                    <img src="<?php echo (!empty($item['image'])) ? Common::getImageUploaded('classiads/small/'.$item['image']) : ''; ?>"  width="46" data-group-key="thumb-group-3">
                                    <span class="vertical-align"></span>
                                </span>
                            </span>
                        </span>
                        <span title="<?php echo $item["title"]; ?>" class="title" dir="ltr"><?php echo $item['title']; ?></span>
                        <span class="stat view-count">Đăng bởi: <?php echo $item['username']; ?>&nbsp;-&nbsp;<?php  echo !empty($item['view_total']) ? $item['view_total'] : 0; ?> lượt xem</span>
                    </a>
                </li>
                <?php endforeach ; ?>
            </ul>
        </div>
    <?php endif; ?>
<?php if($this->tabs !== array()): ?>
</div>
<?php endif; ?>