<div class="box-catnew-item " style="width: auto; height: auto; margin: 0; padding-bottom: 5px;">
    <div class="box-catnew-head">
        <h3><a href="<?php echo Yii::app()->createUrl('/classiads/default'); ?>" title="<?php echo $this->title; ?>"><?php echo $this->title; ?></a></h3>
        <span><?php echo $this->title; ?></span>
    </div>
    <div class="clear"></div>
    <?php foreach ($data as $key => $value): ?>
        <?php if ($key > 0 AND $key < 3): ?>
            <a href="<?php echo $value['url']; ?>" title="<?php echo $value['title']; ?>" class="more2"><?php echo $value['title']; ?></a>
        <?php endif; ?>
    <?php endforeach; ?>
</div>