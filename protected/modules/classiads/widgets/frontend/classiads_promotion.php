<?php

class classiads_promotion extends Widget {

    public $view = '';
    public $category = NULL;
    public $title = '';
    public $limit = 10;
    public $href = '';

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
        Yii::import('classiads.models.KitClassiads');
    }

    public function run() {
        $data = KitClassiads::getPromotion($this->category);
        if (empty($data))
            return FALSE;
        $data = KitClassiads::treatment($data);
        $data = array_slice($data, 0, $this->limit);

        $this->render($this->view,array(
            'data' => $data
        ));
    }

}