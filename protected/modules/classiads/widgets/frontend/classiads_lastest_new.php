<?php

class classiads_lastest_new extends Widget
{
    public $category = NULL;
    public $view = '';
    public $title = '';
    public $limit = 10;
    public $type = NULL;
    public $url = '';
    public $index = 0;
    public $user_id = NULL;
    public $page_size = 5; // so record hien thi
    public $tabs = array();

    public function init()
    {
        if ($this->view == '') $this->view = __CLASS__;
        Yii::import('classiads.models.KitClassiads');
        Yii::import('cms.models.KitStats');
        Yii::import('comment.models.KitComment');
    }

    public function run()
    {
        $cache_name = md5(__METHOD__ . '_' . $this->type.'_'.$this->category.'_'.$this->limit.'_'.$this->user_id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $comm = Yii::app()->db->createCommand()
                ->select('t.id, t.title, t.image, t.creator, t.created_time, t.type, t.content, t.sorder, t.promotion, t.status,
                          s.view_total,u.username, s.module')
                ->from('let_kit_classiads t')
                ->leftJoin('let_kit_stats s', 't.id=s.item_id')
                ->leftJoin('let_kit_account u', 't.creator=u.id');

            if ($this->category !== NULL AND $this->category >= 0) {
                $catListObject = KitCategory::getListInParent('classiads', $this->category);
                $catList = array($this->category);
                foreach ($catListObject as $cat) {
                    $catList[] = $cat->id;
                }
                $comm->leftJoin('let_kit_classiads_category c','t.id = c.classiads_id');
                $comm->where('c.category_id IN '.implode(',', $catList).'');
            }
            $comm->limit($this->limit);
            $comm->where('t.status = 1 AND s.module="classiads"');
            $comm->order('t.id DESC');
            $result = $comm->queryAll();
            if(empty($result)) return FALSE;
            $result = KitClassiads::treatment($result);
            Yii::app()->cache->set($cache_name, $result, (60*60)); // Set cache
        } else $result = $cache;
        $this->render($this->view, array(
            'data' => $result,
        ));
    }
}