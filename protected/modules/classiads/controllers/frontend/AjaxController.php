<?php

/**
 * Module: classiads
 * Auth:
 * Date: 2012-07-19 12:52:51
 */
class AjaxController extends ControllerFrontend {

    public function actionDelete() {
        $id = letArray::get($_POST, 'id');
        $model = KitClassiads::model()->findByPk($id);
        if ($model->delete()) {
            Yii::app()->cache->delete(md5('classiads_lastest::run___20__'));
            Yii::app()->cache->delete(md5('classiads_lastest::run___20_' . Yii::app()->user->id . '_'));
            echo 'true';
            return;
        }
    }

    public function actionClassiadsNew() {
        $limit = letArray::get($_GET, 'limit');
        $view = letArray::get($_GET, 'view');
        $this->widget('classiads.widgets.frontend.classiads_lastest_new', array(
            'view' => $view,
            'limit' => $limit,
        ));
    }

}