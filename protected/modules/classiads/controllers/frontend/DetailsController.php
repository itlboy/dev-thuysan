<?php
/**
 * Module: article
 * Auth:
 * Date: 2012-03-16 11:24:06
 */
Yii::import('application.modules.cms.models.KitStats');
class DetailsController extends ControllerFrontend
{
	public function actionIndex()
	{
        $id = Yii::app()->request->getParam('id', NULL);

        $Cate = KitClassiadsCategory::model()->findAll('classiads_id=:classiadsId',array(':classiadsId' => $id));

        $category_id = array();
        if(!empty($Cate))
        {
            //lay tat cac category co chua tin rao vat nay.
            foreach($Cate as $item){
                $category_id[] = $item->attributes;
            }
        }
        if(!empty($category_id))
        {
            $assign['category'] = KitCategory::getDetails($category_id[0]['category_id']);
            $assign['category'] = KitCategory::treatment($assign['category']);
        } else {
            $assign['category'] = NULL;
        }
        $cache_name = md5(__METHOD__ . '_' . $id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
//            if(!$assign['data'] = KitArticle::model()->findByPk($id)) throw new CHttpException(404,'Not found.');
            // Get details

            $assign['data'] = KitClassiads::getDetails($id);
            $assign['user'] = KitAccount::model()->findByPk($assign['data']['creator']);

//            // Category
//            $categorys = $assign['data']->kitArticleCategory;
//            foreach ($categorys as $category) {
//                $assign['breadcrumb'][] = KitCategory::getBreadcrumb($category->id);
//            }
            $assign['viewcount'] = KitStats::getDetails('classiads',$id);

            if(!empty($assign['data']) AND ($assign['data']->status == 1) AND ($assign['data']->trash ==0)){
                $assign['data'] = KitClassiads::treatment($assign['data']);
            } else {
                $this->redirect(Yii::app()->getBaseUrl(true));
            }
            Yii::app()->cache->set($cache_name, $assign); // Set cache
        } else $assign = $cache;

        // Meta
        if (isset($assign['data']['title'])) $this->pageTitle = $assign['data']['title'];
//        if (isset($assign['data']['intro'])) Yii::app()->clientScript->registerMetaTag($assign['data']['intro'],'description');


        if(isset($assign['data']['id']))
            KitStats::updateValue($assign['data']['id'],'classiads');

        $this->render('index', $assign);
	}
}