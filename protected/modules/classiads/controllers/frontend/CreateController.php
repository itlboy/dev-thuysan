<?php
/**
 * Module: classiads
 * Auth:
 * Date: 2012-07-19 12:52:51
 */
Yii::import('application.modules.location.models.KitLocation');
Yii::import('application.modules.date.models.KitDate');
class CreateController extends ControllerFrontend
{
    public function actionIndex()
    {
        if(Yii::app()->user->isGuest){
            $this->redirect(Yii::app()->CreateUrl('account/login'));
        }else{
            $model = new KitClassiads();
            $category = KitCategory::getList('classiads');
            if(isset($_GET['id'])){
                $model = KitClassiads::model()->with('KitClassiadsCategory')->findByPk(letArray::get($_GET,'id'));
                $category_active = CJSON::decode(CJSON::encode($model->KitClassiadsCategory));
                $categoriesOld = $model->categories = KitClassiadsCategory::model()->getClassiads2Category($model->id);
                if(!empty($category_active)){
                    $category_active = $category_active[0];
                }
            }
            if(!empty($category))
                $category = KitCategory::treatment($category);



            if($post = letArray::get($_POST,'KitClassiads')){
                $model->category_id = $_POST['KitClassiads']['category_id'];
                $model->textCaptcha = $_POST['KitClassiads']['textCaptcha'];
                $model->scenario = ('frontend');
                $model->status = isset($post['status']) ? $post['status'] : 0;
                if (isset($_POST['KitClassiads']['image']) AND !empty($_POST['KitClassiads']['image'])) {
                    $KitDate['image'] = $_POST['KitClassiads']['image'];
                    $_POST['KitClassiads']['image'] = Common::createThumb($this->module->getName() , $_POST['KitClassiads']['image'], letArray::get($_POST['KitClassiads'], 'title', ''));
                } else unset($_POST['KitClassiads']['image']);

                // Lay Thong Tin Date
                $ok = 1;
                if(isset($_POST['date']) AND !empty($_POST['date'])){
                    $KitDate['from_time'] = letArray::get($_POST['KitDate'],'from_time');
                    $KitDate['join_sex'] = letArray::get($_POST['KitDate'],'join_sex');
                    $KitDate['location_id'] = letArray::get($_POST['KitDate'],'location_id');
                    $KitDate['address'] = letArray::get($_POST['KitDate'],'address');
                    $KitDate['title'] = letArray::get($_POST['KitClassiads'],'title');
                    $KitDate['content'] = letArray::get($_POST['KitClassiads'],'content');
                    if(empty($KitDate['address'])){
                        Yii::app()->user->setFlash('error','Bạn chưa nhập địa chỉ cuộc hẹn');
                        $ok = 0;
                    } elseif(empty($KitDate['location_id'])){
                        Yii::app()->user->setFlash('error','Bạn chưa chọn tỉnh thành');
                        $ok = 0;
                    } elseif(isset($KitDate['from_time']) AND (strtotime($KitDate['from_time']) < time())){
                        Yii::app()->user->setFlash('error','Thời gian cuộc hẹn không hợp lệ. Bạn hãy chọn lại');
                        $ok = 0;
                    } elseif(!isset($KitDate['image']) AND empty($KitDate['image'])){
                        Yii::app()->user->setFlash('error','Bạn chưa có ảnh đại diện.');
                        $ok = 0;
                    }

                }
                if($ok == 1){
                    $model->attributes = $_POST['KitClassiads'];
                    if($model->validate())
                    {

                        if($model->save()){
                            // Luu Date
                            if(isset($_POST['date']) AND !empty($_POST['date'])){
                                $this->_saveDate($KitDate);
                            }
                            $cate = new KitClassiadsCategory();
                            $cate->classiads_id = $model->id;
                            $cate->category_id = $model->category_id;
                            if($cate->save())
                            {
                                $itemId = $model->id;
                                if (!$model->isNewRecord){
                                    if(isset($categoriesOld) AND !empty($categoriesOld)){
                                        KitClassiadsCategory::deleteClassiadsCategory($categoriesOld, $itemId);
                                    }
                                }
                                // xoa cache
                                Yii::app()->cache->delete(md5('classiads_lastest::run___20__'));
                                Yii::app()->cache->delete(md5('classiads_lastest::run___20_'.Yii::app()->user->id.'_'));
                                Yii::app()->cache->delete(md5('date_lastest::run___10__'));
                                Yii::app()->cache->delete(md5('date_lastest::run_start__10__'));
                                Yii::app()->cache->delete(md5('date_lastest::run_end__10__'));
                                Yii::app()->cache->delete(md5('date_lastest::run___10_'.Yii::app()->user->id.'_'));

    //                            Yii::app()->user->setFlash('success','Đăng tin thành công. Nội dung tin của bạn chúng tôi sẽ kiểm duyệt. Nếu nội dung vi phạm chúng tôi sẽ xóa bỏ');
                                Yii::app()->user->setFlash('success','Đăng tin thành công.');

                                $this->redirect(Yii::app()->createUrl('classiads/create'));

                            } else {
                                Yii::app()->user->setFlash('error','Bạn hãy thử lại lần nữa');
                            }
                            $this->_clearCache();
                        } else {
                            Yii::app()->user->setFlash('error','Bạn hãy thử lại lần nữa');
                        }
                    } else {
                        Yii::app()->user->setFlash('error','Đăng tin thất bại.'.CHtml::errorSummary($model));
                    }
                }
            }
            $data = KitAccount::model()->findByPk(Yii::app()->user->id);
            if(empty($data->fullname) OR empty($data->phone) OR empty($data->email) OR empty($data->birthday)){
                Yii::app()->user->setFlash('mess','Bạn cần nhập đầy đủ thông tin. Hãy <a href="http://id.let.vn/account/profile/edit.let" rel="nofollow" target="_blank">cập nhật</a> ngay bây giờ');
            }
        }
        $this->render('index',array(
                'model' => $model,
                'category' => $category,
                'category_active' => isset($category_active) ? $category_active : NULL,
                'data' => $data,
            ));

    }

    public function _saveDate($Kitdate){
        $date = new KitDate();
        if(isset($Kitdate['image']) AND !empty($Kitdate['image'])){
            $Kitdate['image'] = Common::createThumb('date' , $Kitdate['image'], letArray::get($Kitdate, 'title', ''));
        }
        $date->attributes = $Kitdate;
        if($date->validate()){
            if($date->save()){
                return TRUE;
            }
        } else return FALSE;
    }

    public function actions()
    {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                "backColor"=>0xffffff,
                "minLength"=>2,
                "maxLength"=>3,
            ),
        );
    }
    public function actionMylist(){
        if(Yii::app()->user->isGuest){
            $this->redirect(Yii::app()->CreateUrl('account/login'));
        }else{
            $model = new KitClassiads();
            $creator = Yii::app()->user->id ;

            $model = KitClassiads::model()->findAll('creator=:creator',array(':creator' => $creator));
            if(!empty($model)){
                $model = KitClassiads::treatment($model);
            }
        }
        $this->render('mylist',array('model' => $model));
    }

    public function _clearCache(){
        Yii::app()->cache->delete(md5('classiads_lastest::run___10_'));
        if(!Yii::app()->user->isGuest){
            Yii::app()->cache->delete(md5('classiads_lastest::run___10_'.Yii::app()->user->id));
        }
    }

}