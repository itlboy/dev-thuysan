<?php
/**
 * Module: classiads
 * Auth:
 * Date: 2012-07-19 12:52:51
 */
class DefaultController extends ControllerFrontend
{
    public function loadModel($id) {
        $model=KitClassiads::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

	public function actionIndex()
	{
        $assign = '';

        $assign['data'] = KitCategory::getList('classiads');

        if(!empty($assign['data'])){
            $assign['data'] = KitCategory::treatment($assign['data']);
        }
		$this->render('index',$assign);
	}
    public function actionDelete(){
        if(Yii::app()->user->isGuest){
            $this->redirect(Yii::app()->CreateUrl('account/login'));
        }else{
            $id = letArray::get($_GET,'id');
            $model = KitClassiads::model()->findByPk($id);
            if($model->delete()){
                $this->redirect(Yii::app()->CreateUrl('/classiads/create/mylist'));
            }
        }
    }

    public function actionMylist(){
        if(Yii::app()->user->isGuest){
            $this->redirect(Yii::app()->CreateUrl('account/login'));
        }else{
            $model = new KitClassiads();
            $creator = Yii::app()->user->id ;

            $data = KitClassiads::model()->findAll('creator=:creator ORDER BY created_time DESC ',array(':creator' => $creator));
            if(!empty($data)){
                $data = KitClassiads::treatment($data);
            }
        }
        $this->render('mylist',array('data' => $data));
    }
}