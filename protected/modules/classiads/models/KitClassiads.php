<?php

Yii::import('application.modules.classiads.models.db.BaseKitClassiads');
class KitClassiads extends BaseKitClassiads{
    var $className = __CLASS__;
    
    public $category_id;
    public $categories;
    public $textCaptcha;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    public function attributeLabels() {
        return array_merge(parent::attributeLabels(), array(
            'image' => 'Ảnh đại diện',
            'title' => 'Tiêu đề tin',
            'textCaptcha' => 'Mã xác nhận',
            'content' => 'Nội dung',
            'promotion' => 'Tiêu điểm',
            'status' => 'Kích hoạt',
            'intro' => 'Giới thiệu',
            'tags' => 'thẻ',
            'categories' => 'Danh mục',
            'from_time' => 'Thời gian bắt đầu',
            'to_time' => 'Thời gian kết thúc',
            'trash' => 'Thùng rác',
            'sorder' => 'Thứ tự'
        ));
    }
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
        return CMap::mergeArray(
            parent::rules(),
            array(
                    array('categories', 'safe'),
                    array("content","required"),
                    array("content","length","min"=>20),
                    array("textCaptcha","captcha","allowEmpty"=>!CCaptcha::checkRequirements(), 'on' => 'frontend')
                )
        );
	}

    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'KitClassiadsCategory' => array(self::MANY_MANY, 'KitCategory',
                '{{kit_classiads_category}}(classiads_id, category_id)'),
            'creatorUser' => array(self::BELONGS_TO, 'KitAccount', 'creator', 'select' => 'username'),
            'editorUser' => array(self::BELONGS_TO, 'KitAccount', 'editor', 'select' => 'username'),
		);
	} 
    
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;
        $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');
        
		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.title',$this->title,true);
		$criteria->compare('t.image',$this->image,true);
		$criteria->compare('t.type',$this->type,true);
		$criteria->compare('t.intro',$this->intro,true);
		$criteria->compare('t.content',$this->content,true);
		$criteria->compare('t.tags',$this->tags,true);
		$criteria->compare('t.view_count',$this->view_count);
		$criteria->compare('t.comment_count',$this->comment_count);
		$criteria->compare('t.from_time',$this->from_time,true);
		$criteria->compare('t.to_time',$this->to_time,true);
		$criteria->compare('t.layout_id',$this->layout_id,true);
		$criteria->compare('t.seo_title',$this->seo_title,true);
		$criteria->compare('t.seo_url',$this->seo_url,true);
		$criteria->compare('t.seo_desc',$this->seo_desc,true);
		$criteria->compare('t.creator',$this->creator,true);
		$criteria->compare('t.created_time',$this->created_time,true);
		$criteria->compare('t.editor',$this->editor,true);
		$criteria->compare('t.updated_time',$this->updated_time,true);
		$criteria->compare('t.sorder',$this->sorder,true);
		$criteria->compare('t.promotion',$this->promotion);
		$criteria->compare('t.status',$this->status);
		$criteria->compare('t.trash',$this->trash);
        
        if ($this->category_id !== NULL) {
            $categories = array();
            $categories[] = $this->category_id;
            $category = KitCategory::model()->findByPk($this->category_id);
            $descendants = $category->descendants()->findAll();
            foreach ($descendants as $cat) {
                $categories[] = $cat->id;
            }
            $criteria->addInCondition('ic.category_id', $categories);
        }
        
        $criteria->join = 'LEFT JOIN {{kit_classiads_category}} ic ON ic.classiads_id = t.id';
        
        $criteria->group = 't.id';
        
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
            'sort' => array(
                'defaultOrder' => 'id DESC',
            ),
		));
	}
    
    protected function beforeValidate() {
        $this->editor = Yii::app()->user->id;
        $this->updated_time = Common::getNow();                    
        if ($this->isNewRecord) {
            $this->creator = $this->editor;
            $this->created_time = $this->updated_time;
            $this->view_count = 0;
            $this->comment_count = 0;
            $this->sorder = 500;
        }
        
        return parent::beforeValidate();
    }
    
    public function parseCategory($data, $delimiter = '') {
        $arr = array();
        foreach ($data as $value) {
            $arr[] = $value->name;                                     
        }
        echo implode($arr, $delimiter);    
    }

                

    ///////////////////////////////// Frontend /////////////////////////////////
    /**
     * treatment
     * @param mixed $data
     * @return array 
     */
    public static function treatment($data)
    {
        $data = CJSON::decode ((CJSON::encode($data))); // Neu la object thi chuyen thanh array
        if (!isset ($data[0]))
            $data = self::treatmentRow($data);
        else {
            foreach ($data as $key => $value) {
                $data[$key] = self::treatmentRow($value);
            }
        }
        return $data;
    }
    
    public static function treatmentRow($row)
    {

        $urlTitle = Common::convertStringToUrl($row['title']);
        $row['url'] = Yii::app()->createUrl($urlTitle.'-classiads-'.$row['id']);
        $row['title'] = str_replace ( array ( '&', '"', "'", '<', '>', '�' ), array ( '&amp;' , '&quot;', '&apos;' , '&lt;' , '&gt;', '&apos;' ), $row['title'] );
        return $row;
    }
    
    /**
     * Get Details
     * @param int $id
     * @return object 
     */
    public static function getDetails($id) {
        $id = intval($id);
        $cache_name = md5(__METHOD__ . '_' . $id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $result = self::model()->findByPk($id, array(
                'select' => Common::getFieldInTable(self::model()->getAttributes(), 't.'),
                'with' => array('creatorUser', 'editorUser')
            ));
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
    
    public static function getLastest ($category = NULL, $limit = 20){
        $cache_name = md5(__METHOD__ . '_' . $category);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
            if ($category !== NULL AND $category >= 0) {
                $catListObject = KitCategory::getListInParent('classiads', $category);
                $catList = array($category);
                foreach ($catListObject as $cat) {
                    $catList[] = $cat->id;
                }
                $criteria->join = "INNER JOIN {{kit_classiads_category}} t2 ON t.id=t2.classiads_id";
//                $criteria->group = "t.id";
                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', $catList).')');
            }
            $criteria->order = 't.id DESC';
            $criteria->limit = $limit;
            $result = self::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
    
    public static function getPromotion ($category = NULL, $limit = 20){
        $cache_name = md5(__METHOD__ . '_' . $category);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = Common::getFieldInTable(self::model()->getAttributes(), 't.');
            $criteria->condition = '';
            $criteria->condition = Common::addWhere($criteria->condition, 't.status = 1');
            $criteria->condition = Common::addWhere($criteria->condition, 't.promotion = 1');

            if ($category !== NULL AND $category >= 0) {
                $criteria->join = "INNER JOIN {{kit_classiads_category}} t2 ON t.id=t2.classiads_id";
                $criteria->condition = Common::addWhere($criteria->condition, 't2.category_id IN ('.implode(',', KitCategory::arrCategory('classiads', $category)).')');
            }
            $criteria->order = 't.id DESC';
            $criteria->limit = $limit;
            $result = self::model()->findAll($criteria);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }
}