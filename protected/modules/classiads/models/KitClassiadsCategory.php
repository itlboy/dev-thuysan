<?php

Yii::import('application.modules.classiads.models.db.BaseKitClassiadsCategory');
class KitClassiadsCategory extends BaseKitClassiadsCategory {

    var $className = __CLASS__;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getClassiads2Category($classiadsId) {
        $result = array();
        $data = self::findAll('classiads_id=:classiadsId', array(':classiadsId' => $classiadsId));
        foreach ($data as $item2Type)
            $result[] = $item2Type->category_id;
        return $result;
    }

    /**
     * Create
     * @param array $categories
     * @param int $itemId 
     */
    public static function createClassiadsCategory($categories = array(), $itemId) {
        if (! is_array($categories)) return FALSE;
        foreach ($categories as $categoryId) {
            $item2Cat = new KitClassiadsCategory;
            $item2Cat->category_id = $categoryId;
            $item2Cat->classiads_id = $itemId;
            $item2Cat->save();
        }
    }

    /**
     * Delete
     * @param array $categories
     * @param int $itemId 
     */
    public static function deleteClassiadsCategory($categories = array(), $itemId) {
        if (! is_array($categories)) return FALSE;
        foreach ($categories as $categoryId) {
            self::model()->deleteAll('category_id=:categoryId AND classiads_id=:classiadsId', array(
                ':categoryId' => $categoryId,
                ':classiadsId' => $itemId,
            ));
        }
    }
}