<?php

Yii::import('application.modules.account.models.db.BaseKitAccountStats');
Yii::import('application.modules.currency.models.KitCurrencyHistory');
Yii::import('application.modules.currency.models.KitCurrencyCard');
Yii::import('application.modules.comment.models.KitComment');
class KitAccountStats extends BaseKitAccountStats
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public static function treatment($data)
    {
        $data = CJSON::decode ((CJSON::encode($data))); // Neu la object thi chuyen thanh array
        if (!isset ($data[0]))
            $data = KitAccountStats::treatmentRow($data);
        else {
            foreach ($data as $key => $value) {
                $data[$key] = KitAccountStats::treatmentRow($value);
            }
        }
        return $data;
    }

    public static function treatmentRow($row)
    {
        return $row;
    }

    /**
     * @static
     * @param $user_id
     * @param array $options array('gold_current','gold_paid','money_paid')
     * @return bool
     */
    public static function updateStats($user_id, $options = array()){
//        // Kiem tra user ton tai ? Thong tin account
//        if(empty(KitAccount::getDetails($user_id)))
//            return FALSE;

        $model = KitAccountStats::model()->findByPk($user_id);
        if(empty($model)){
            $model = new KitAccountStats();
        }
        $model->user_id = $user_id;

        Yii::import('application.modules.currency.models.KitCurrencyGold');
        Yii::import('application.modules.currency.models.KitCurrencyPay');

        // Cập nhật số xu hiện tại đang có
        if($options == array() OR in_array('gold_current',$options)){
            $model->gold_current = KitCurrencyGold::getSumGold($user_id, TRUE, TRUE);
        }
        
        // Cập nhật số xu đã nạp
        if($options == array() OR in_array('gold_paid',$options)){
            $model->gold_paid = KitCurrencyGold::getSumGold($user_id, TRUE, FALSE);
        }
        
        // Cập nhật số tiền đã nạp
        if($options == array() OR in_array('money_paid',$options)){
            $model->money_paid = KitCurrencyPay::getSumMoney($user_id);
        }
        
        if($model->validate()){
            $model->save();
            Yii::app()->cache->delete('account_stats_details_' . $user_id);
            return TRUE;
        } else return FALSE;
    }


//    public static function updateStatsAll(){
//        $data = KitAccount::model()->findAll('status = 1 ORDER BY id ASC');
//        if(empty($data))
//            return FALSE;
//        $data = CJSON::decode(CJSON::encode($data));
//        foreach($data as $user){
//            self::updateStats($user['id']);
//        }
//        return TRUE;
//    }
//
//    /**
//     * @static
//     * @return bool
//     */
//    public static function updateStatsAllMoney(){
//        // Query de lay ra tat ca user da giao dich (dung sql distinct voi bang history)
//        $data = KitCurrencyHistory::model()->findAll(array( 'select'=>'user_id', 'distinct'=>true ));
//        if(empty($data))
//            return FALSE;
//        $data = CJSON::decode(CJSON::encode($data));
//        foreach($data as $user){
//            self::updateStats($user['user_id']);
//        }
//        return TRUE;
//    }


    public static function getDetails($userid){
        $userid = intval($userid);
        $cache_name = md5('account_stats_details_' . $userid);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $result = self::model()->findByPk($userid);
            Yii::app()->cache->set($cache_name, $result,60*60); // Set cache
        } else return $cache;
        return $result;
    }


    public static function getField($userid, $field){
        $row = self::getDetails($userid);
        $row = self::treatment($row);
        return letArray::get($row, $field, 0);
    }


}