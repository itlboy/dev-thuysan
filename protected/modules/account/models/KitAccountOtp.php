<?php

Yii::import('application.modules.account.models.db.BaseKitAccountOtp');
class KitAccountOtp extends BaseKitAccountOtp
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}