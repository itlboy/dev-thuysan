<?php

Yii::import('application.modules.account.models.db.BaseKitAccountApplist');
class KitAccountApplist extends BaseKitAccountApplist
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public static function setInfoMembersAppList($model,$type = 'insert')
    {
        if(empty($model))
            return FALSE;
        $data = KitAccountApplist::model()->findAll('status = 1');
        $data = CJSON::decode(CJSON::encode($data));
        $applists = array();
        foreach($data as $app){
            // Check xem co config ko ?
            $appConfigs = isset($app['config']) ? json_decode($app['config'],true) : NULL;
            if(!empty($appConfigs)){
                // chuyen $appConfigs dang mang
                foreach($appConfigs as $config){
                    if(isset($config['field'])){
                        foreach($config['field'] as $fields){
                            if($type == 'password'){
                                if($fields['field_account'] == $type){
                                    $applists[$config['table']][$fields['field_account']] = $model->$fields['field_target'];
                                }
                            } else {
                                $applists[$config['table']][$fields['field_account']] = $model->$fields['field_target'];
                            }

                        }
                    }
                }
            }
        }
        $db = Yii::app()->db;
        $command = $db->CreateCommand();
        if(!empty($applists)){
            foreach($applists as $table => $fields){
                if($type == 'insert'){
                    $insert = $command->insert($table,$applists[$table]);
                } elseif($type == 'update'){
                    $update = $command->update($table,$applists[$table],'username="'.$model->username.'"');
                } else {
                    $update = $command->update($table,$applists[$table],'username="'.$model->username.'"');
                }

                $command->reset();
            }
        }
    }
}