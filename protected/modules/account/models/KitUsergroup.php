<?php

Yii::import('application.modules.account.models.db.BaseKitUsergroup');

class KitUsergroup extends BaseKitUsergroup {

    var $className = __CLASS__;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getGroupOptions() {
        return self::findAll(array(
            'select' => 'usergroup_id, usergroup_name',
            'condition' => 'usergroup_active = 1'
        ));
    }

}