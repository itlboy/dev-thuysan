<?php

Yii::import('application.modules.account.models.db.BaseForumUcenterMembers');

class ForumUcenterMembers extends BaseForumUcenterMembers {

    var $className = __CLASS__;
    
    // Duy: 20120521
    public $groups;
    public $password_old;
    public $password_repeat;
    public $let_fullname;
//    public $captcha;
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
        return CMap::mergeArray(
            parent::rules(),
            array(
                array('username', 'required'),
                array('password', 'required', 'on' => 'create'),
                array('email', 'email'),
                array('password', 'compare'),
                array('password_repeat, groups', 'safe'),
//                array("captcha","captcha","allowEmpty"=>!extension_loaded("gd"))
            )
        );
	}
    
    protected function beforeValidate() {
        if ($this->isNewRecord) {
            $this->salt = LetIdentity::createSalt();
            $this->regip = Yii::app()->request->userHostAddress;
            $this->regdate = time();            
        }
               
        return parent::beforeValidate();
    }
    
    public function beforeSave() {
        if ($this->password != NULL || $this->password != '')
            $this->password = LetIdentity::encodePassword($this->password, $this->salt);
        else
            $this->password = $this->password_old;

        return parent::beforeSave();
    }

    protected function afterFind() {
        $this->password_old = $this->password;
        $this->password = '';
        return parent::afterFind();
    }

}
