<?php

Yii::import('application.modules.account.models.db.BaseKitAccountGroup');
class KitAccountGroup extends BaseKitAccountGroup
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function getMember2Group($id) {
        $result = array();
        $data = self::findAll('user_id = :id', array(':id' => (int)$id));
        foreach ($data as $member2Group)
            $result[] = $member2Group->group_id;
        return $result;
    }
    
    /**
     * Create
     * @param array $categories
     * @param int $itemId 
     */
    public static function createMemberGroup($groups, $id, $active = 1) {
        if (is_array($groups) AND !empty($groups)) {
            foreach ($groups as $groupId) {
                $mem2Group = new KitAccountGroup;
                $mem2Group->user_id = (int)$id;
                $mem2Group->group_id = (int)$groupId;
                $mem2Group->save();
            }   
        }
    }
    
    /**
     * Delete
     * @param array $groups
     * @param int $id 
     */
    public static function deleteMemberGroup($groups = array(), $id) {
        $criteria = new CDbCriteria;
        $criteria->condition = 'user_id = :id';
        $criteria->params = array(':id' => (int)$id);
        $criteria->addInCondition('group_id', $groups);
        self::model()->deleteAll($criteria);
    }
}