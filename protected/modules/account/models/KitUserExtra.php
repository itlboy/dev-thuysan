<?php

Yii::import('application.modules.account.models.db.BaseKitUserExtra');
class KitUserExtra extends BaseKitUserExtra
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function getMember2Group($id) {
        $result = array();
        $data = self::findAll('id = :id', array(':id' => (int)$id));
        foreach ($data as $member2Group)
            $result[] = $member2Group->let_group;
        return $result;
    }
    
    /**
     * Create
     * @param array $categories
     * @param int $itemId 
     */
    public static function createMemberGroup($groups, $id, $active = 1) {
        if (is_array($groups) AND !empty($groups)) {
            foreach ($groups as $groupId) {
                $mem2Group = new KitUserExtra;
                $mem2Group->id = (int)$id;
                $mem2Group->let_group = (int)$groupId;
                $mem2Group->let_active = (int)$active;
                $mem2Group->save();
            }   
        }
    }
    
    /**
     * Delete
     * @param array $groups
     * @param int $id 
     */
    public static function deleteMemberGroup($groups = array(), $id) {
        $criteria = new CDbCriteria;
        $criteria->condition = 'id = :id';
        $criteria->params = array(':id' => (int)$id);
        $criteria->addInCondition('let_group', $groups);
        self::model()->deleteAll($criteria);
    }
}