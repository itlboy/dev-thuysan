<?php

Yii::import('application.modules.account.models.db.BaseKitAccountConfig');
class KitAccountConfig extends BaseKitAccountConfig
{
    var $className = __CLASS__;
    public static $exchange =  array(
        'film_unlimit' => 1,// 1 ngay
        'cs_vip' => 1,// 1 ngay
    );

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'creatorUser' => array(self::BELONGS_TO, 'KitAccount', 'user_id', 'select' => 'username'),
        );
    }


    public static function getField($id, $field){

        $row = KitAccountConfig::getDetails($id);
        $row = KitAccountConfig::treatment($row);
        return letArray::get($row, $field, '');
    }


    /**
     * @static Luu gioi han thoi gian su dung dich vu cua 1 user
     * @param $user_id id cua user
     * @param $field tuong ung voi ten truong trong data let_kit_account_config
     * @param $value lay gia tri tra ve tu ham exchange()
     * @param string $type kieu gia tri int OR datetime
     * @return bool
     */
    public static function updateConfig($user_id, $field, $value, $type = 'int'){
        $model = KitAccountConfig::model()->find('user_id='.$user_id);
        if(empty($model)){ // Neu khong ton tai config cua user
            $model = new KitAccountConfig();
            if($type == 'int')
                $currentValue = $value;
            elseif($type == 'datetime')
                $currentValue = date('Y-m-d H:i:s');
            else return FALSE;
        } else { // Neu ton tai config cua user
            if(isset($model->$field)){ // Neu ton tai truong
                $currentValue = $model->$field;
                if($type == 'datetime'){
                    if(time() > strtotime($model->$field)){
                        $currentValue = date('Y-m-d H:i:s');
                    }
                }
            } else $currentValue = date('Y-m-d H:i:s');
        }

        if($type == 'int')
            $valueUpdate = $currentValue + $value;
        elseif($type == 'datetime')
            $valueUpdate = date('Y-m-d H:i:s', strtotime($currentValue) + $value);
        else return FALSE;

        $model->user_id = $user_id;
        $model->$field = $valueUpdate;
        $model->save();
        
        // Cap nhat thong tin VIP cho game CS
        if ($field == 'cs_vip') {
            Yii::import('application.modules.cs.models.KitCsCharacter');
            KitCsCharacter::setVip($user_id);
        }
    }

    /**
     * @static Quy doi gia tri money thanh datetime OR int theo giá trị $exchange đã định sẵn
     * @param $field tuong ung voi ten truong trong data let_kit_account_config
     * @param $money so tien duoc su dung trong giao dich
     * @return bool
     */
    public static function exchange($field,$gold){
        $exchange = self::$exchange;
        if (!isset($exchange[$field])) return FALSE;
        return $exchange[$field] * (60*60*24*$gold);
    }

    public static function getDetails($user_id) {
        $user_id = intval($user_id);
        $cache_name = 'config_unlimit_' . $user_id;
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $result = self::model()->find('user_id ='.$user_id);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

    ///////////////////////////////// Frontend /////////////////////////////////
    /**
     * treatment
     * @param mixed $data
     * @return array
     */
    public static function treatment($data)
    {
        $data = CJSON::decode ((CJSON::encode($data))); // Neu la object thi chuyen thanh array
        if (!isset ($data[0]))
            $data = self::treatmentRow($data);
        else {
            foreach ($data as $key => $value) {
                $data[$key] = self::treatmentRow($value);
            }
        }
        return $data;
    }

    public static function treatmentRow($row)
    {
        return $row;
    }
}