<?php

Yii::import('application.modules.account.models.db.BaseKitAccount');
class KitAccount extends BaseKitAccount
{
    var $className = __CLASS__;

    public $groups;
    public $password_old;
    public $password_oauth;
    public $password_repeat;
    public $agreement; // Dieu khoan su dung
    public $checkbox ;
    public $email2;
    public $phone2;
    public $otp_code;
    public $email_code;
    public $password2_confirm;
    public $new_password2;
    public $new_password2_confirm;
    public $verifyCode;
    
    private $_max_length = 255;
    private $_min_length = 6;
    
    const STATUS_ACTIVE = 1;
    const STATUS_NONE_ACTIVE = 0;
            
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    public function attributeLabels() {
        return array_merge(parent::attributeLabels(), array(
            'fullname' => 'Họ và tên',
            'username' => 'Tên truy cập',
            'password' => 'Mật khẩu',
            'password_repeat' => 'Nhập lại mật khẩu',
            'agreement' => 'Điều khoản sử dụng',
            'education' => 'Trình độ học vấn',
            'birthprovince' => 'Nguyên quán',
            'resideprovince' => 'Thường chú',
            'bloodtype' => 'Nhóm máu',
            'idcard' => 'Số CMND',
            'constellation' => 'Cung',
            'zodiac' => 'Tuổi tử vi',
            'weight' => 'Cân nặng',
            'height' => 'Chiều cao',
            'company' => 'Công ty',
            'occupation' => 'Nghề nghiệp',
            'zipcode' => 'ZipCode',
            'interest' => 'Sở thích',
            'site' => 'Website',
            'email2' => 'Email thay thế',
            'phone2' => 'Số điện thoại thay thế',
            'password2' => 'Mật khẩu cấp 2',
            'password2_confirm' => 'Xác nhận mật khẩu cấp 2',
            'otp_code' => 'Mã OTP',
            'new_password2'=>"Mật khẩu mới",
            'new_password2_confirm'=>"Xác nhận mật khẩu mới",
            'email_code'=>'Mã xác nhận Email'
        ));
    }

    public function rules(){
        return CMap::mergeArray(
            parent::rules(),
            array(
                array('username','required'),
                array('email, password, password_repeat', 'required', 'on' => 'create'),
                array('password', 'compare', 'compareAttribute' => 'password_repeat', 'on' => 'create'),
                array('username, email', 'unique'),
                array('email', 'email'),
                array('groups', 'safe'),
                array('email2, password2', 'required', 'on' => 'changeEmail'),
                array('password2,', 'length', 'min'=>$this->_min_length),
                array('password2', 'length', 'max'=>$this->_max_length),
                array('email2', 'email'),
                array('email2', 'checkExistsEmail2', 'on'=>'changeEmail'),
                array('phone2', 'required', 'on' => 'changePhone'),
                array('password2, otp_code', 'required', 'on' => 'changePhone, changePass2'),
                array('phone2', 'checkExistsPhone2', 'on'=>'changePhone'),
                array('agreement', 'numerical', 'integerOnly' => true), 
                array('agreement', 'required', 'requiredValue' => 1, 'message' => 'Bạn chưa chấp nhận điều khoản sử dụng', 'on' => 'create'),
                array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements(), 'on' => 'create'),
                
                array('new_password2, new_password2_confirm, email_code', 'required', 'on' => 'changePass2'),
                array('new_password2,', 'length', 'min'=>$this->_min_length, 'on'=>'changePass2'),
                array('new_password2', 'length', 'max'=>$this->_max_length, 'on'=>'changePass2'),
                array('new_password2', 'compare', 'compareAttribute' => 'new_password2_confirm', 'on' => 'changePass2'),
                
                array('password2, password2_confirm', 'required', 'on' => 'makePass2'),
                array('password2,', 'length', 'min'=>$this->_min_length, 'on'=>'makePass2'),
                array('password2', 'length', 'max'=>$this->_max_length, 'on'=>'makePass2'),
                array('password2', 'compare', 'compareAttribute' => 'password2_confirm', 'on' => 'makePass2'),
            )
        );
    }
    
    public function checkExistsEmail2($attribute, $params) {
        if(!empty($this->email2)){
            $record = KitAccount::model()->findByAttributes(array('email'=>$this->email2));
            if($record)
                $this->addError('email2', 'Email thay thế đã được sử dụng.');
        }
    }
    
    public function checkExistsPhone2($attribute, $params) {
        if(!empty($this->phone2)){
            $record = KitAccount::model()->findByAttributes(array('phone'=>$this->phone2));
            if($record)
                $this->addError('phone2', 'Số điện thoại đã được sử dụng.');
        }
    }
    
    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array_merge(parent::relations(), array(
            'AccountStats' => array(self::HAS_ONE, 'KitAccountStats', 'user_id'),
            'AccountLevel' => array(self::HAS_ONE, 'KitAccountLevel', 'id'),
        ));
    }

    protected function beforeValidate(){
        if($this->isNewRecord){
            $this->salt = LetIdentityMembers::createSalt($this->username);
            $this->regip = Yii::app()->request->userHostAddress;
            $this->regdate = time();
            $this->created_time = date('Y-m-d H:i:s');
            $this->password_origin = $this->password;
        }
        return parent::beforeValidate();
    }

    public function beforeSave() {
        if ($this->password_oauth != NULL || $this->password_oauth != '') {
            $this->password = $this->password_oauth;
            $this->password_origin = $this->password_oauth;
        }

        if ($this->password != NULL || $this->password != '')
            $this->password = LetIdentityMembers::encodePassword($this->password, $this->salt);
        else
            $this->password = $this->password_old;

        // Xu ly ngay thang
        $this->birthday = MDate::store($this->birthday, 2);

        return parent::beforeSave();
    }

    protected function afterFind() {
        $this->password_old = $this->password;
        $this->password = '';

        $this->birthday = MDate::show($this->birthday, 2);
        return parent::afterFind();
    }
    // Dây la hàm lấy thông tin của account
    public static function getDetails($user_id, $updateCache = FALSE){
        $user_id = intval($user_id);
        $cache_name = md5('account_details_' . $user_id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE OR $updateCache) {

            $result = self::model()->findByPk($user_id);

            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

    public static function treatment($data)
    {
        $data = CJSON::decode ((CJSON::encode($data))); // Neu la object thi chuyen thanh array
        if (!isset ($data[0]))
            $data = self::treatmentRow($data);
        else {
            foreach ($data as $key => $value) {
                $data[$key] = self::treatmentRow($value);
            }
        }
        return $data;
    }

    public static function treatmentRow($row)
    {
        return $row;
    }

    public static function getCheckFilmUnlimit(){
        if(Yii::app()->user->isGuest) return FALSE;
        $film_unlimit = KitAccountConfig::getField(Yii::app()->user->id,'film_unlimit');
        $film_unlimit = strtotime($film_unlimit);
        if($film_unlimit > time()){
            return TRUE;
        }
        return FALSE;
    }

    public static function getAvatar($id, $size = 'large', $default = ''){
        $avatar = KitAccount::getField($id,'avatar');
        if (!empty($avatar))
            return Common::getImageUploaded('account/'.$size.'/'.$avatar);
        elseif ($default == '')
            return 'http://assets.let.vn/avatar/default/'.$size.'.png';
        else
            return $default;
    }
    
    public static function getAvatarUrl($size = 'large', $avatar = '', $default = ''){
        $url = $default;
        if (!empty($avatar))
            $url = Common::getImageUploaded('account/'.$size.'/'.$avatar);
        elseif ($default == '')
            $url = 'http://assets.let.vn/avatar/default/'.$size.'.png';
        
        return $url;
    }

    public static function getField($id, $field){
        $row = KitAccount::getDetails($id);
        $row = KitAccount::treatment($row);
        return letArray::get($row, $field, '');
    }

    public function findByEmail($email) {
        return self::model()->findByAttributes(array('email' => $email));
    }

    public function validatePassword($password) {
        return ($this->password_old == LetIdentityId::encodePassword($password, $this->salt)) ? TRUE : FALSE;
    }
    
    public static function validateOTP($phoneNumer = "", $otpCode = ""){
        $result = FALSE;
        if(!empty($phoneNumer) AND !empty($otpCode)){
            $otpAccount = KitAccountOtp::model()->findByAttributes(array('type'=>'phone', 'phone' => $phoneNumer,'otp' => $otpCode));
            if($otpAccount->id > 0){
                $result = TRUE;
                
                //Delete otp account row
                $otpAccount->delete();
            }
        }
        return $result;
    }

}