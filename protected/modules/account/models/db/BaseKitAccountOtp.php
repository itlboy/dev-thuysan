<?php

/**
 * This is the model class for table "{{kit_account_otp}}".
 *
 * The followings are the available columns in table '{{kit_account_otp}}':
 * @property string $id
 * @property integer $user_id
 * @property string $username
 * @property string $email
 * @property string $email_to_change
 * @property string $phone
 * @property string $otp
 * @property string $type
 * @property string $time
 */
class BaseKitAccountOtp extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitAccountOtp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_account_otp}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('username, otp', 'length', 'max'=>32),
			array('email, email_to_change', 'length', 'max'=>255),
			array('phone', 'length', 'max'=>20),
			array('type', 'length', 'max'=>10),
			array('time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, username, email, email_to_change, phone, otp, type, time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('email',$this->email,true);
        $criteria->compare('email_to_change',$this->email_to_change,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('otp',$this->otp,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('time',$this->time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
		));
	}
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitAccountOtp::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitAccountOtp::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitAccountOtp::getLastest_'));
        Yii::app()->cache->delete(md5('KitAccountOtp::getPromotion_'));
    }

}
