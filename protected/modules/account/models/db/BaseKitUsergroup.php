<?php

/**
 * This is the model class for table "{{kit_usergroup}}".
 *
 * The followings are the available columns in table '{{kit_usergroup}}':
 * @property integer $usergroup_id
 * @property string $usergroup_name
 * @property integer $usergroup_parentid
 * @property integer $usergroup_order
 * @property integer $usergroup_active
 */
class BaseKitUsergroup extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitUsergroup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_usergroup}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('usergroup_name', 'required'),
			array('usergroup_parentid, usergroup_order, usergroup_active', 'numerical', 'integerOnly'=>true),
			array('usergroup_name', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('usergroup_id, usergroup_name, usergroup_parentid, usergroup_order, usergroup_active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('usergroup_id',$this->usergroup_id);
		$criteria->compare('usergroup_name',$this->usergroup_name,true);
		$criteria->compare('usergroup_parentid',$this->usergroup_parentid);
		$criteria->compare('usergroup_order',$this->usergroup_order);
		$criteria->compare('usergroup_active',$this->usergroup_active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
		));
	}
}
