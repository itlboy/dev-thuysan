<?php

/**
 * This is the model class for table "{{kit_account_level}}".
 *
 * The followings are the available columns in table '{{kit_account_level}}':
 * @property integer $id
 * @property string $title
 * @property string $config
 * @property string $image_link
 * @property integer $sorder
 * @property integer $status
 */
class BaseKitAccountLevel extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitAccountLevel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_account_level}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, sorder, status', 'numerical', 'integerOnly'=>true),
			array('title, image_link', 'length', 'max'=>255),
			array('config', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, config, image_link, sorder, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('config',$this->config,true);
		$criteria->compare('image_link',$this->image_link,true);
		$criteria->compare('sorder',$this->sorder);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
		));
	}
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
//        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
//        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
//        foreach ($categorys as $category) {
//            Yii::app()->cache->delete(md5('KitAccountLevel::getLastest_' . $category->id));
//            Yii::app()->cache->delete(md5('KitAccountLevel::getPromotion_' . $category->id));
//        }
//        Yii::app()->cache->delete(md5('KitAccountLevel::getLastest_'));
//        Yii::app()->cache->delete(md5('KitAccountLevel::getPromotion_'));
    }

}
