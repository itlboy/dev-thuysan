<?php

/**
 * This is the model class for table "{{kit_account}}".
 *
 * The followings are the available columns in table '{{kit_account}}':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $password2
 * @property string $password_origin
 * @property string $salt
 * @property string $email
 * @property string $fullname
 * @property string $displayname
 * @property integer $level
 * @property integer $nextlevel
 * @property string $view_total
 * @property string $comment_total
 * @property string $share_total
 * @property string $like_total
 * @property string $post_total
 * @property string $rank
 * @property string $rank_point
 * @property string $phone
 * @property integer $sex
 * @property string $birthday
 * @property string $avatar
 * @property string $yahoo
 * @property string $education
 * @property string $bloodtype
 * @property string $idcard
 * @property string $address
 * @property string $birthprovince
 * @property string $resideprovince
 * @property string $constellation
 * @property string $zodiac
 * @property string $height
 * @property string $weight
 * @property string $company
 * @property string $occupation
 * @property integer $lookingfor
 * @property string $zipcode
 * @property string $interest
 * @property string $site
 * @property integer $location_id
 * @property string $regip
 * @property string $regdate
 * @property integer $lastloginip
 * @property string $lastlogintime
 * @property string $user_config
 * @property integer $active_phone
 * @property integer $active_email
 * @property string $created_time
 * @property string $updated_time
 * @property integer $status
 * @property integer $trash
 */
class BaseKitAccount extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return KitAccount the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kit_account}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('salt', 'required'),
			array('level, nextlevel, sex, lookingfor, location_id, lastloginip, active_phone, active_email, status, trash', 'numerical', 'integerOnly'=>true),
			array('username, password, password2, password_origin', 'length', 'max'=>32),
			array('salt', 'length', 'max'=>6),
			array('email, fullname, displayname, avatar, yahoo, education, bloodtype, idcard, address, constellation, zodiac, height, weight, company, occupation, zipcode, interest, site', 'length', 'max'=>255),
			array('view_total, comment_total, share_total, like_total, post_total, rank, rank_point, regdate, lastlogintime', 'length', 'max'=>10),
			array('phone', 'length', 'max'=>20),
			array('birthprovince, resideprovince', 'length', 'max'=>500),
			array('regip', 'length', 'max'=>15),
			array('birthday, user_config, created_time, updated_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, password, password2, password_origin, salt, email, fullname, displayname, level, nextlevel, view_total, comment_total, share_total, like_total, post_total, rank, rank_point, phone, sex, birthday, avatar, yahoo, education, bloodtype, idcard, address, birthprovince, resideprovince, constellation, zodiac, height, weight, company, occupation, lookingfor, zipcode, interest, site, location_id, regip, regdate, lastloginip, lastlogintime, user_config, active_phone, active_email, created_time, updated_time, status, trash', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return Common::loadMessages("db");
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('password2',$this->password2,true);
		$criteria->compare('password_origin',$this->password_origin,true);
		$criteria->compare('salt',$this->salt,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('fullname',$this->fullname,true);
		$criteria->compare('displayname',$this->displayname,true);
		$criteria->compare('level',$this->level);
		$criteria->compare('nextlevel',$this->nextlevel);
		$criteria->compare('view_total',$this->view_total,true);
		$criteria->compare('comment_total',$this->comment_total,true);
		$criteria->compare('share_total',$this->share_total,true);
		$criteria->compare('like_total',$this->like_total,true);
		$criteria->compare('post_total',$this->post_total,true);
		$criteria->compare('rank',$this->rank,true);
		$criteria->compare('rank_point',$this->rank_point,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('sex',$this->sex);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('avatar',$this->avatar,true);
		$criteria->compare('yahoo',$this->yahoo,true);
		$criteria->compare('education',$this->education,true);
		$criteria->compare('bloodtype',$this->bloodtype,true);
		$criteria->compare('idcard',$this->idcard,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('birthprovince',$this->birthprovince,true);
		$criteria->compare('resideprovince',$this->resideprovince,true);
		$criteria->compare('constellation',$this->constellation,true);
		$criteria->compare('zodiac',$this->zodiac,true);
		$criteria->compare('height',$this->height,true);
		$criteria->compare('weight',$this->weight,true);
		$criteria->compare('company',$this->company,true);
		$criteria->compare('occupation',$this->occupation,true);
		$criteria->compare('lookingfor',$this->lookingfor);
		$criteria->compare('zipcode',$this->zipcode,true);
		$criteria->compare('interest',$this->interest,true);
		$criteria->compare('site',$this->site,true);
		$criteria->compare('location_id',$this->location_id);
		$criteria->compare('regip',$this->regip,true);
		$criteria->compare('regdate',$this->regdate,true);
		$criteria->compare('lastloginip',$this->lastloginip);
		$criteria->compare('lastlogintime',$this->lastlogintime,true);
		$criteria->compare('user_config',$this->user_config,true);
		$criteria->compare('active_phone',$this->active_phone);
		$criteria->compare('active_email',$this->active_email);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_time',$this->updated_time,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('trash',$this->trash);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['pageSize']),
            ),
		));
	}
    
    /**
     * Action after Save 
     */
    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }
    
    /**
     * Action before Delete
     */
    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }
    
    /**
     *  clear Cache
     */
    private function clearCache() {
        $categorys = KitCategory::getList(Yii::app()->controller->module->id);
        foreach ($categorys as $category) {
            Yii::app()->cache->delete(md5('KitAccount::getLastest_' . $category->id));
            Yii::app()->cache->delete(md5('KitAccount::getPromotion_' . $category->id));
        }
        Yii::app()->cache->delete(md5('KitAccount::getLastest_'));
        Yii::app()->cache->delete(md5('KitAccount::getPromotion_'));
    }

}
