<?php

Yii::import('application.modules.account.models.db.BaseKitAccountLevel');
class KitAccountLevel extends BaseKitAccountLevel
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }



    public static function treatment($data)
    {
        $data = CJSON::decode ((CJSON::encode($data))); // Neu la object thi chuyen thanh array
        if (!isset ($data[0]))
            $data = KitAccountLevel::treatmentRow($data);
        else {
            foreach ($data as $key => $value) {
                $data[$key] = KitAccountLevel::treatmentRow($value);
            }
        }
        return $data;
    }

    public static function treatmentRow($row)
    {
        // URL
        $row['image_url'] = Yii::app()->params['account_level_image_dir'] . '/' . $row['image_link'];

        return $row;
    }
	
	public static function detect($user_id)
	{
		/*
		- Get những điều kiện của 1 user
		- Get danh sách Level và config của nó (cache)
		- Foreach level từ thấp đến cao, level nào user đạt tiêu chí thì chọn, cái sau cùng là cái level user đạt được
		*/
        $userInfo = KitAccount::getDetails($user_id);

        if(empty($userInfo))
            return FALSE;

        $AccountStats = CJSON::decode(CJSON::encode($userInfo->AccountStats));

        // Thong tin Level
        $result = array('level' => NULL,'nextlevel' => NULL);
        $levels = KitAccountLevel::model()->findAll('status = 1 ORDER BY sorder ASC');
        $levels = KitAccountLevel::treatment($levels);
        foreach($levels as $level){
            $levelConfig = json_decode($level['config'],true);
            $ok = TRUE;
            foreach($levelConfig as $config){

                $AccountStats[$config['field']] = (isset($AccountStats[$config['field']]) AND !empty($AccountStats[$config['field']])) ? $AccountStats[$config['field']] : 0;
                if(!self::convertStrToBool( $AccountStats[$config['field']].$config['comparing'].$config['conditions']))
                {
                    $ok = FALSE;
                }

            }
            if($result['level'] == $result['nextlevel'])
                $result['nextlevel'] = $level['id'];
            if($ok == TRUE){
                $result['level'] = $level['id'];
            }

        }
        return $result;
	}

    public static function convertStrToBool($string){
        $string = 'if ('.$string.') return TRUE; else return FALSE;';
        return eval($string);
    }

    /**
     * @static Update level cho User
     * @param $user_id
     * @return bool
     */
	public static function updateLevel($user_id)
	{
        $levels = self::detect($user_id);
        if(empty($levels))
            return FALSE;
        $model = KitAccount::model()->findByPk($user_id);
        $model->level = (isset($levels['level']) AND !empty($levels['level'])) ? $levels['level'] : 0;
        $model->nextlevel = (isset($levels['nextlevel']) AND !empty($levels['nextlevel'])) ? $levels['nextlevel'] : $levels['level'];

        if(!$model->save()){
//            echo 'Update level that bai: ' . $user_id."\n";
//            echo CHtml::errorSummary($model)."\n";
			Yii::log('Update level that bai: ' . $user_id, 'info');
            return FALSE;
		}
//        echo 'Update level thanh cong: ' . $user_id."\n";
		Yii::log('Update level thanh cong: ' . $user_id, 'info');
        return TRUE;
	}

    /**
     * @static
     * @return bool
     */
	public static function updateLevelAll()
	{
//        $data = KitAccountStats::model()->findAll(array(
//            'select'=>'user_id',
//            'condition'=>'money_total > 0',
//        ));
//        if(!empty($data)) {
//			$data = CJSON::decode(CJSON::encode($data));
//	        foreach($data as $key => $user){
//	            self::updateLevel($user['user_id']);
//	        }
//		}
		
//        $data = KitAccount::model()->findAll(array(
//            'select'=>'id',
//            'condition'=>'level = 0',
//        ));
//
//        if(!empty($data)){
//			$data = CJSON::decode(CJSON::encode($data));
//			foreach($data as $key => $user){
//				self::updateLevel($user['id']);
//			}
//		}

        $data = KitAccount::model()->findAll(array(
            'select'=>'id',
            'condition'=>'',
        ));

        if(!empty($data)){
			$data = CJSON::decode(CJSON::encode($data));
			foreach($data as $key => $user){
				self::updateLevel($user['id']);
			}
		}

		return TRUE;
	}
	
	public static function getDetails($id){
        $id = intval($id);
        $cache_name = md5(__METHOD__ . '_' . $id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $result = self::model()->findByPk($id);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else return $cache;
        return $result;
    }

    public static function getField($id, $field){
        $level = self::getDetails($id);
        $level = self::treatment($level);
        return letArray::get($level, $field, '');
    }
}