<?php
class ProfileController extends ControllerFrontend{
    public function actionEdit(){
        $this->checkLogin = TRUE;
        $model = KitAccount::model()->findByPk(Yii::app()->user->id);
        $msg = array();
        
        if(isset($_POST['KitAccount'])){
            $model->attributes = $_POST['KitAccount'];
            if($model->validate()){
                if($model->save()){
                    $model = KitAccount::model()->findByPk(Yii::app()->user->id);
                    $msg['success'][] = 'Đã cập nhật thành công.';
                } else {
                    //Yii::app()->user->setFlash('error','');
                    $msg['error'][] = 'Xin lỗi, bạn không thể lưu.';
                }
            }
        }

        $this->render('edit',array(
            'model' => $model,
            'msg'=>$msg
        ));
    }
    
    /**
     * Change email action
     */
    public function actionChangeEmail(){
        $this->checkLogin = TRUE;
        $msg = array();
        
        $model = KitAccount::model()->findByPk(Yii::app()->user->id);
        if($model){
            $model->setScenario('changeEmail');
            $currentPassword2 = $model->password2;
            $model->unsetAttributes(array('email2', 'password2'));
        }
        
        if(isset($_POST['KitAccount'])){
            $model->attributes = $_POST['KitAccount'];
            
            if($model->validate()){
                //Check passowrd2 
                $password2 = LetIdentityId::encodePassword($model->password2, $model->salt);
                if ($password2 !== $currentPassword2){
                    $msg['error'][] = 'Mật khẩu cấp 2 không chính xác.';
                }
                else{
                    //Tìm xem có đang đổi email mà chưa xác nhận hay không?
                    $accountOTP = KitAccountOtp::model()->findByAttributes(array('type'=>'email'));
                    if(!$accountOTP){ //Không tìm thấy
                        //Make OTP code
                        $otpCode = Common::randomString(8);
                        //Save otp code for email 1
                        $accountOTP = new KitAccountOtp();
                        $accountOTP->user_id = $model->id;
                        $accountOTP->username = $model->username;
                        $accountOTP->email = $model->email;
                        $accountOTP->email_to_change = $model->email2;
                        $accountOTP->phone = $model->phone;
                        $accountOTP->otp = $otpCode;
                        $accountOTP->type = "email";
                        $accountOTP->time = date("Y-m-d H:i:s");
                        $accountOTP->save();

                        //Save otp code for email 2
                        $accountOTP = new KitAccountOtp();
                        $accountOTP->user_id = $model->id;
                        $accountOTP->username = $model->username;
                        $accountOTP->email = $model->email2;
                        $accountOTP->email_to_change = $model->email2;
                        $accountOTP->phone = $model->phone;
                        $accountOTP->otp = $otpCode;
                        $accountOTP->type = "email";
                        $accountOTP->time = date("Y-m-d H:i:s");
                        $accountOTP->save();

                        $crypter = new Crypt();
                        //Make confirm link for email 1
                        $confirmCode = $crypter->encode($model->email."|".$otpCode);
                        $confirmUrl = Yii::app()->createAbsoluteUrl('account/profile/confirmEmail', array('confirm_code' => $confirmCode));
                        $confirmLink = '<a style="color: #ffffff !important" href="'.$confirmUrl.'">Click chuột vào đây để xác nhận thay đổi email.</a>';

                        //Make params
                        $params = array(
                            'email_code' => 'send_to_member_when_change_email',
                            'to_email' => array($model->email => $model->username),
                            'username' => $model->username,
                            'new_email' => $model->email2,
                            'confirm_url' => $confirmLink
                        );

                        //Send mail to verify email 1
                        Yii::import('application.modules.mail.models.*');
                        KitMailSender::sendMail($params);

                        //Make confirm link for email 1
                        $confirmCode = $crypter->encode($model->email2."|".$otpCode);
                        $confirmUrl = Yii::app()->createAbsoluteUrl('account/profile/confirmEmail', array('confirm_code' => $confirmCode));
                        $confirmLink = '<a style="color: #ffffff !important" href="'.$confirmUrl.'">Click chuột vào đây để xác nhận thay đổi email.</a>';
                        //Update params
                        $params['to_email'] = array($model->email2 => $model->username);
                        $params['confirm_url'] = $confirmLink;
                        
                        //Send mail to verify email 2
                        KitMailSender::sendMail($params);

                        //Alert to member
                        $msg['warning'][] = "Có 2 thư yêu cầu xác nhận địa email vừa được gửi tới 2 hòm thư của bạn.<br/>";
                        $msg['warning'][] = "Để hoàn tất việc thay đổi email, hãy kiểm tra 2 hòm thư để tiến hành xác nhận địa email là của bạn.";
                    }else { //Nếu tìm thấy account OPT

                        //Alert to member
                        $msg['warning'][] = "Bạn đã thực hiện đổi email một lần nhưng chưa xác nhận thành công. Chúng tôi đã gửi 2 thư yêu cầu xác nhận địa email tới 2 hòm thư của bạn.<br/>";
                        $msg['warning'][] = "Hãy hoàn tất việc thay đổi email bằng cách kiểm tra 2 hòm thư để tiến hành xác nhận email là của bạn ngay bây giờ.";
                        //$resendMailLink = Yii::app()->createUrl($this->route, array('resend'=>1));
                        //$msg['warning'][] = "Nếu bạn không thấy email yêu cầu xác nhận trong cả 2 hòm thư mà bạn đã khai báo. Vui lòng bấm chuột <a href='".$resendMailLink."'><strong>vào đây</strong></a> để hệ thống gửi lại thư xác nhận cho bạn.";
                    }
                }
            }else{
                //Get errors message
                $errors = $model->getErrors('email2');
                if($errors && is_array($errors)){
                    $msg['error'] = $errors;
                }
                $errors2 = $model->getErrors('password2');
                if($errors2 && is_array($errors2)){
                    foreach ($errors2 as $errorMessage){
                        $msg['error'][] = $errorMessage;    
                    }
                }
            }
        }
        
        $this->render('change_email',array(
            'model' => $model,
            'msg'=>$msg
        ));
    }
    
    public function actionConfirmEmail()
    {
        $this->checkLogin = TRUE;
        $msg = array();

        $confirmCode = Yii::app()->request->getParam('confirm_code', FALSE);
        
        if($confirmCode){
            $crypter = new Crypt();
            $confirmCode = $crypter->decode($confirmCode);
            $confirmCode = explode('|', $confirmCode);
            
            $emailToVerify = $confirmCode[0];
            $otpCode = $confirmCode[1];
            
            $otpAccount = KitAccountOtp::model()->findByAttributes(array('type'=>'email', 'email'=>$emailToVerify ,'otp' => $otpCode));
            $emailToChange = $otpAccount->email_to_change;
            
            if($otpAccount AND ($otpAccount->user_id === Yii::app()->user->id)){
                //Get current member
                $member = KitAccount::model()->findByPk($otpAccount->user_id);
                
                //Delete row otp for first email
                $otpAccount->delete();
                
                $otpAccount2 = KitAccountOtp::model()->findByAttributes(array('type'=>'email','otp' => $otpCode));
                if($otpAccount2){
                    $msg['warning'][] = "Bạn đã xác nhận email: {$emailToVerify} thành công. Để đổi email thành công bạn cần xác nhận cả email: {$otpAccount2->email} nữa."; 
                }else{ 
                    //Change email for member
                    if($member){
                        $member->active_email = KitAccount::STATUS_ACTIVE;
                        if($emailToChange)
                            $member->email = $emailToChange;
                        if($member->update()){
                            if($emailToChange){
                                $msg['success'][] = "Xin chúc mừng, Bạn đã đổi email và xác nhận email thành công."; 
                            }
                        }
                    }
                }

                $this->render('change_email',array(
                    'model' => $member,
                    'msg'=>$msg
                ));
            }else{
                $msg['warning'][] = "Xin lỗi, Thời gian xác nhận email đã vượt quá mức thời gian cho phép."; 
            }
        }
        
        $this->redirect(Yii::app()->getBaseUrl(true));
    }
    
    /**
     * Change email action
     */
    public function actionChangePhone(){
        $this->checkLogin = TRUE;
        $msg = array();
        
        $model = KitAccount::model()->findByPk(Yii::app()->user->id);
        if($model){
            $currentPassword2 = $model->password2;
            $model->unsetAttributes(array('phone2', 'password2'));
            $model->setScenario('changePhone');
        }
        
        if(empty($model->phone)){
            if(empty($model->id2)){ // Nếu chưa có id2
                $id2 = substr(md5($model->id), 0, 3);
                $randomStr = Common::randomString(3);
                $id2 = $id2.$randomStr;
                $model->id2 = $id2;
                $model->update();
            }
            
            $view = "active_phone";
        }else{
            $view = "change_phone";
        }
        
        if(isset($_POST['KitAccount'])){
            $model->attributes = $_POST['KitAccount'];
            if($model->validate()){
                //Check OTP code
                $resultOTP = KitAccount::validateOTP($model->phone2, $model->otp_code);

                if(!$resultOTP){
                    $msg['error'][] = 'Mã OTP không chính xác.';
                }else{
                    //Check passowrd2 
                    $password2 = LetIdentityId::encodePassword($model->password2, $model->salt);
                    if ($password2 !== $currentPassword2)
                        $msg['error'][] = 'Mật khẩu cấp 2 không chính xác.';
                    else{
                        //Update new phone
                        $model->phone = $model->phone2;
                        $model->active_phone = KitAccount::STATUS_ACTIVE;
                        if($model->update()){
                            //Alert to member
                            $message = "Bạn đã đổi số điện thoại thành công.";
                            $msg['success'][] = $message;
                        }
                    }
                }
            }else{
                //Get errors message
                $errors = $model->getErrors('phone2');
                if($errors && is_array($errors)){
                    $msg['error'] = $errors;
                }
                $errors3 = $model->getErrors('password2');
                if($errors3 && is_array($errors3)){
                    foreach ($errors3 as $errorMessage){
                        $msg['error'][] = $errorMessage;    
                    }
                }
                $errors2 = $model->getErrors('otp_code');
                if($errors2 && is_array($errors2)){
                    foreach ($errors2 as $errorMessage){
                        $msg['error'][] = $errorMessage;    
                    }
                }
            }
        }
        
        $this->render($view, array(
            'model' => $model,
            'msg'=>$msg
        ));
    }
    
    public function actionAjaxUploadAvatar(){
        $this->checkLogin = TRUE;
        $result = array(
            'success'=>false,
            'message'=>''
        );
        $model = KitAccount::model()->findByPk(Yii::app()->user->id);
                
        $fileElementName = 'imgAvatar';
        if(!empty($_FILES[$fileElementName]['error'])){
            switch($_FILES[$fileElementName]['error'])
            {
                case '1':
                    $result['message'] = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
                    break;
                case '2':
                    $result['message'] = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                    break;
                case '3':
                    $result['message'] = 'The uploaded file was only partially uploaded';
                    break;
                case '4':
                    $result['message'] = 'No file was uploaded.';
                    break;
                case '6':
                    $result['message'] = 'Missing a temporary folder';
                    break;
                case '7':
                    $result['message'] = 'Failed to write file to disk';
                    break;
                case '8':
                    $result['message'] = 'File upload stopped by extension';
                    break;
                case '999':
                default:
                    $result['message'] = 'No error code avaiable';
            }
        }
        else if(empty($_FILES[$fileElementName]['tmp_name']) || $_FILES[$fileElementName]['tmp_name'] == 'none'){
            $result['message'] = 'No file was uploaded.';
        }
        else {
            //upload file to temp folder
            $uploadDir = Yii::app()->params->uploadPath .DIRECTORY_SEPARATOR. Yii::app()->params->uploadDir;
            $fileName = 'avatar';
            
            //Get file extension
            $extention = strtolower(pathinfo($_FILES[$fileElementName]['name'], PATHINFO_EXTENSION));
            $now = time();
            $fileName = $now . '_' . md5($now . $fileName) . '.' . $extention;
            $tempFile = $uploadDir . DIRECTORY_SEPARATOR . $fileName;
            
            // Validate the file type
            $fileTypes = Yii::app()->params->fileTypes;
            if (in_array($extention, $fileTypes)) {
                /* Read the image file */
                if($_FILES[$fileElementName]['tmp_name']){

                    // delete temps file ago.
                    Common::deleteOldTempFiles($uploadDir, $now);

                    // move to temp folder
                    @move_uploaded_file($_FILES[$fileElementName]['tmp_name'], $tempFile);
                    
                    //create thumb
                    $fileName = Common::createThumb($this->module->getName(), $fileName, $model->username, $model->avatar);
                    
                    //Update avatar file to member
                    if($fileName){
                        $model->avatar = $fileName;
                        if($model->update()){

                            //Get avatar url
                            $fileUrl = Common::getImageUrl($this->module->getName(), 'large', $fileName);
                            $result['success'] = true;
                            $result['avatarUrl'] = $fileUrl;
                            $result['message'] = "Bạn đã đổi ảnh đại diện thành công.";
                        }
                    }
                    
                    //remove file
                    @unlink($_FILES[$fileElementName]);		
                }
            }
            // delete temp image
            if(file_exists($tempFile))
                @unlink($tempFile);
        }		
        
        echo json_encode($result);
        Yii::app()->end();
    }
    
    public function actionEditAvatar(){
        $this->checkLogin = TRUE;
        $user = KitAccount::model()->findByPk(Yii::app()->user->id);
        
        $image = array();
        $image['size'] = (string)$_GET['size'];
        $image['fileName'] = $user->avatar;
        $originAvatarUrl = KitAccount::getAvatarUrl('origin', $image['fileName'])."?v=".time();
        $image['src'] = $originAvatarUrl;
        
        //Get image config
        $imageConfig = Yii::app()->getModule($this->module->getName())->image;
        $config = $imageConfig[$image['size']];
        $image['sizeW'] = letArray::get($config, 'width');
        $image['sizeH'] = letArray::get($config, 'height');
        
        $this->layout = 'edit_image';
        $this->render('edit_avatar', array('image' => $image));
    }
    
    /**
     * @todo Crop image process
     */
    public function actionHandleCropZoom() {
        $this->checkLogin = TRUE;
        
        if (Yii::app()->request->getIsAjaxRequest()) {
            Yii::import('application.widgets.cropzoom.JCropZoom');

            $imgSize = (string) $_GET['imageSize'];
            $fileName = (string) $_GET['fileName'];
            $destFile = Common::getImageUploaded('account/'.$imgSize.'/'.$fileName, 'PATH');
            JCropZoom::getHandler()->process($destFile);

            $result = array();
            $result['newAvatarUrl'] = KitAccount::getAvatarUrl('large', $fileName)."?v=" . time();
            
            echo json_encode($result);
            Yii::app()->end();
        }
        else {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
    }

    public function _clearCache($user_id = NULL){
        Yii::app()->cache->delete(md5('account_details_' . $user_id));
    }
}