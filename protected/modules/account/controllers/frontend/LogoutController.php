<?php
// Phần này chưa dùng đến nên có thể thay đổi thoải mái
class LogoutController extends ControllerFrontend
{
    public $defaultAction = 'index';
    
    public function actionIndex()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
}