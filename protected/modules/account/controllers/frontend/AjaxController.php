<?php
Yii::import('application.vendors.helper.letText');

Yii::import('application.modules.account.models.KitAccountApplist');
class AjaxController extends ControllerFrontend
{
//    public $defaultAction = 'index';

    public function actionIndex()
    {        


    }

    public function actionLogin()
    {        
        $this->widget('application.modules.account.widgets.WidgetLoginForm', array(
            'params' => (isset($_POST['KitAccount'])) ? $_POST['KitAccount'] : NULL
        ));
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionActiveEmail()
    {
        // Get Email, kiểm tra xem email này đã được kích hoạt chưa
//        KitAccount::
        
        // Tạo mã kích hoạt và lưu vào database
        
        // Thiết lập thông tin gửi Email
        Yii::import('application.modules.mail.models.KitMailSender');
    }
    
    public function actionAjaxSignUp(){

        $model = new KitAccount();
        if(isset($_POST['KitAccount'])){
            $date = !empty($_POST['KitAccount']['date']) ? $_POST['KitAccount']['date'] : '0';
            $month = !empty($_POST['KitAccount']['month']) ? $_POST['KitAccount']['month'] : '0';
            $year = !empty($_POST['KitAccount']['year']) ? $_POST['KitAccount']['year'] : '0';
            $model->attributes = $_POST['KitAccount'];
            // Check ton tai va dinh dang ngay sinh
            if((int)$date !== 0 AND (int)$month !== 0 AND (int)$year !== 0 AND checkdate($month,$date,$year)){
                $model->birthday = $year.'-'.$month.'-'.$date;
            }

            // Kiem tra ki tu username co hop le hay ko
            $username = trim($_POST['KitAccount']['username']);
            if(!preg_match('/^[A-z0-9_]*$/', $username)){
                echo json_encode(array(
                    'result' => 'false',
                    'mess' => '<center>Tài khoản không sử dụng kí tự đặc biệt.</center>'
                ));
                return;
            }

            // kiem tra do dai cua password
            if(isset($_POST['KitAccount']['password'])){
                if(strlen($_POST['KitAccount']['password']) < 6){
                    echo json_encode(array(
                        'result' => 'false',
                        'mess' => '<center>Mật khẩu tối thiểu lớn hơn 6 kí tự.</center>'
                    ));
                    return;
                }
            }

            $model->trash = 1;
            // Check username da ton tai hay chua
            $checkUser = KitAccount::model()->find('username=:username',array(':username' => $_POST['KitAccount']['username']));
            if(!empty($checkUser)){
                echo json_encode(array(
                    'result' => 'false',
                    'mess' => '<center>Tài khoản này đã tồn tại.</center>'
                ));
                return;
            }

            if($model->validate()){

                //Check email nay da duoc su dung hay chua . email la duy nhat khong duoc su dung lai de dang ki
                $checkEmail = KitAccount::model()->find('email=:email',array(':email' => $_POST['KitAccount']['email']));
                if(!empty($checkEmail)){
                    echo json_encode(array(
                        'result' => 'false',
                        'mess' => '<center>'.$_POST['KitAccount']['email'].' đã được sử dụng </center>'
                    ));
                    return;
                }

                if($model->save()){
                    KitAccountApplist::setInfoMembersAppList($model);
                    KitAccountStats::updateStats($model->id);
                    KitAccountLevel::updateLevel($model->id);
                    echo json_encode(array(
                        'result' => 'true',
                        'mess' => '<center>Bạn đã đăng ký thành công. Ngay bây giờ bạn có thể đăng nhập</center>'
                    ));
                } else {
                    echo json_encode(array(
                        'result' => 'false',
                        'mess' => '<center>Có lỗi trong quá trình xử lý, mời bạn thử lại</center>'
                    ));
                }

            } else {
                echo json_encode(array(
                    'result' => 'false',
                    'mess' => CHtml::errorSummary($model)
                ));
            }
        }
    }

    public function actionAjaxForgot(){
        $keyword = letArray::get($_POST,'keyword');
        $emailValidate = new CEmailValidator();
        $check = FALSE;

        // Kiem tra gia tri dau vao co phai la email hay khong .
        // Neu la email thi kiem tra trong database co email nay khong.
        if($emailValidate->validateValue($keyword))
        {
            $checkEmail = KitAccount::model()->find('email=:email',array(
                ':email' => $keyword
            ));
            if(empty($checkEmail)){
                echo json_encode(array(
                    'result' => 'false',
                    'mess' => '<strong>'.$keyword.'</strong> không có email này ',
                ));
            } else {
                $check = TRUE;
            }
        } else {
            // Neu gia tri dau vao khong phai la email.
            $checkUser = KitAccount::model()->find('username=:username',array(
                ':username' => $keyword
            ));
            if(empty($checkUser)){
                echo json_encode(array(
                    'result' => 'false',
                    'mess' => '<strong>'.$keyword.'</strong> không tồn tại ',
                ));
            } else {
                $check = TRUE;
            }
        }

        if($check){
            $model = KitAccount::model()->find('username=:username OR email=:email',array(
                ':username' => $keyword,
                ':email' => $keyword,
            ));
            $code_active = letText::random(NULL,20);
            // 3 ngay
            $time = time() + Yii::app()->params['mail_code_active_expired'];
            $model->code_active = $code_active;
            $model->time_active = $time;
            if($model->save()){
                $mailTo = array(
                    $model->email =>$model->fullname,
                );

                $mailHtml = "<b>Click link bên dưới để đổi mật khẩu mới </b><br />"; //HTML Body
                $mailHtml .="<a href='http://".$_SERVER['HTTP_HOST'].Yii::app()->createUrl('account/default/changepassword',array('keyword' => $code_active))."'>http://".$_SERVER['HTTP_HOST'].Yii::app()->createUrl('account/default/changepassword',array('keyword' => $code_active))."'</a>";
                $data = array(
                    'subject' => 'Khôi phục mật khẩu',
                    'html' => $mailHtml,
                    'to' => array(
                        $model->email => $model->fullname,
                    ),
                );
                if(KitMailSender::sendMail($data) == TRUE){
                    $result = array(
                        'result' => 'true',
                        'mess' => 'Chúng tôi đã gửi một mail thay đổi mật khẩu tới mail của bạn '.$model->email.'. Vui lòng kiểm tra và làm theo hướng dẫn',
                    );
                } else {
                    $result = array(
                        'result' => 'false',
                        'mess' => '',
                    );
                };

                echo json_encode($result);
            }

        }

    }


    public function actionMember(){
        $limit = letArray::get($_GET,'limit');
        $type = letArray::get($_GET,'type');
        $view = letArray::get($_GET,'view');
        $this->widget('account.widgets.frontend.TopMember',array(
            'limit' => $limit,
            'type' => $type,
            'view' => $view,
        ));
    }
}

