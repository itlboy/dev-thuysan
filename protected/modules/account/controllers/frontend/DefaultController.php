<?php
/**
 * Module: account
 * Auth:
 * Date: 2012-03-16 11:23:48
 */
Yii::import('application.modules.mail.models.KitMailSender');
Yii::import('application.modules.account.models.KitAccountStats');
Yii::import('application.modules.account.models.KitAccountLevel');
class DefaultController extends ControllerFrontend
{

	public function actionIndex()
	{

        $model = new KitAccount();
        if(isset($_POST['KitAccount'])){
            $model->attributes = $_POST['KitAccount'];
            if($model->validate()){
                if($model->save()){
                    $this->redirect(Yii::app()->createUrl('account/default'));
                }
            } else {
                $this->redirect(Yii::app()->getBaseUrl(true).'/cms/default',array('model'=>$model));
            }
        }
		$this->render('index',array(
            'model' => $model,
        ));        
	}
    public function actionInfo(){
        $model = new KitAccount();
        $this->render('info',array(
            'model' => $model,
        ));
    }
    public function actionLoadmoney(){
        $model = new KitAccount();
        $this->render('loadmoney',array(
            'model' => $model,
        ));
    }
    public function actionEditinfo()
    {
        $model = new KitAccount();
        $this->render('editinfo',array(
            'model' => $model,
        ));
    }
    public function actions(){
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
            )
        );
    }
    public function actionInfotsvn(){

        if(Yii::app()->user->isGuest){
            $this->redirect(Yii::app()->CreateUrl('account/login'));
        }else{
            $id = Yii::app()->user->id;
            $model = KitAccount::model()->findByPk($id);
            if(isset($_POST['KitAccount'])){
                $model->attributes = $_POST['KitAccount'];
                if($model->validate()){
                    if($model->save()){
                        $this->redirect(Yii::app()->CreateUrl('account/default/infotsvn'));
                    }
                }
            }
        }
        $this->render('infotsvn',array(
            'model' => $model,
        ));
    }
    public function actionPasswordtsvn(){
        if(Yii::app()->user->isGuest){
            $this->redirect(Yii::app()->CreateUrl('account/login'));
        }else{
            $id= Yii::app()->user->id;
            $model = KitAccount::model()->findByPk($id);

            if(isset($_POST['KitAccount'])){
                if($model->password_old == LetIdentityMembers::encodePassword($_POST['KitAccount']['password_old'],$model->salt)){
                      $model->password = $_POST['KitAccount']['password_new'] ;
                      $model->password_repeat = $_POST['KitAccount']['password_repeat'];
                    if($model->validate()){
                        if($model->save()){
                            Yii::app()->user->setFlash('success','Bạn đã đổi mật khẩu thành công');
                        }else{
                            Yii::app()->user->setFlash('error','Bạn không thể đổi mật khẩu');
                        }
                    }
                }else{

                    Yii::app()->user->setFlash('error','Mật khẩu của bạn không đúng');
                }
            }

        }
        $this->render('passwordtsvn',array(
            'model' => $model,
        ));
    }

    public function actionLogin(){
        $modelLogin = new LoginFormMembers();
        $modelLogin->attributes = letArray::get($_POST,'KitAccount');
        $modelLogin->rememberMe = ($_POST['KitAccount']['checkbox'] == 1) ? true : false;

        if($modelLogin->validate() AND $modelLogin->login())
        {
            KitAccountStats::updateStats(Yii::app()->user->id);
            KitAccountLevel::updateLevel(Yii::app()->user->id);
            $this->_clearCache(Yii::app()->user->id);
            $this->redirect(Yii::app()->getBaseUrl(true));

        } else {
            Yii::app()->user->setFlash('error', $modelLogin->message);
            $this->redirect(array('//home'));
        }
    }
    
    // Đăng nhập site cuoi.let.vn
    public function actionLoginCuoi() {
        $modelLogin = new LoginFormMembers();
        $modelLogin->attributes = letArray::get($_POST, 'KitAccount');
        $modelLogin->rememberMe = (isset($_POST['KitAccount']['checkbox']) AND $_POST['KitAccount']['checkbox'] == 1) ? true : false;

        if ($modelLogin->validate() AND $modelLogin->login()) {
            KitAccountStats::updateStats(Yii::app()->user->id);
            KitAccountLevel::updateLevel(Yii::app()->user->id);
            $this->_clearCache(Yii::app()->user->id);
            echo json_encode(array(
                'status' => 'success',
                'message' => $this->renderPartial('loginCuoi', NULL, TRUE)
            ));
        } else {
            echo json_encode(array(
                'status' => 'error',
                'message' => $modelLogin->message
            ));
        }
    }

    public function actionForgot(){

        $this->render('forgot');
    }

    public function actionChangePassword(){
        $keyword = letArray::get($_GET,'keyword');

        $model = KitAccount::model()->find('code_active=:codeActive',array(
            ':codeActive' => $keyword
        ));
        if(empty($model) OR $model->time_active < time()){
            $this->redirect(Yii::app()->getBaseUrl(true));
        }
        if(isset($_POST['KitAccount'])){
            $model->attributes = letArray::get($_POST,'KitAccount');
            $model->code_active = '';
            if($model->save()){
                $modelLogin = new LoginFormMembers();
                $modelLogin->username = $model->username;
                $modelLogin->password = $_POST['KitAccount']['password'];
                if($modelLogin->validate() and $modelLogin->login())
                {
                    $this->redirect(Yii::app()->createUrl('account/profile'));
                }
            }
        }
        $this->render('changepassword',array(
            'model' => $model
        ));
    }

    public function _clearCache($user_id){
        Yii::app()->cache->delete('config_unlimit_'.$user_id);
    }
}