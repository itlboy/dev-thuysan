<?php
//Yii::import('application.modules.location.models.KitLocation');
class ProfileController extends ControllerFrontend{

//    public function actionIndex(){
//        if(Yii::app()->user->isGuest){
//            $this->redirect(array('//home'));
//        } else {
//            //Check xem Id User co trong bang account hay ko
////            $model = KitAccount::model()->findByPk(Yii::app()->user->id);
////            if(empty($model)){
////                $this->redirect(array('//home'));
////            }
//        }
//        $this->render('index');
//    }

//    public function actionInfo(){
//
//        if(Yii::app()->user->isGuest){
//            $this->redirect(Yii::app()->getBaseUrl(true));
//        }
//
//        $model = KitAccount::model()->findByPk(Yii::app()->user->id);
//        if(empty($model))
//            $this->redirect(Yii::app()->getBaseUrl(true));
//        $this->render('info',array(
//            'model' => $model,
//        ));
//    }

//    public function actionAvatar(){
//
//        if(Yii::app()->user->isGuest){
//            $this->redirect(Yii::app()->getBaseUrl(true));
//        }
//
//        $model = KitAccount::model()->findByPk(Yii::app()->user->id);
//        if(empty($model))
//            $this->redirect(Yii::app()->getBaseUrl(true));
//        if(isset($_POST['KitAccount'])) {
//            // Upload image
//            if ($_POST['KitAccount']['avatar'] !== NULL AND $_POST['KitAccount']['avatar'] !== '') {
//                $_POST['KitAccount']['avatar'] = Common::createThumb($this->module->getName() , $_POST['KitAccount']['avatar'], $model->username);
//
//                // Remove Image
//                Common::removeImage($this->module->id, $model->avatar);
//            } else unset ($_POST['KitAccount']['avatar']);
//            $model->attributes = $_POST['KitAccount'];
//            if ($model->validate()) {
//                if($model->save()) {
//                    //                    if (Yii::app()->request->getPost('apply') == 0)
//                    $this->_clearCache(Yii::app()->user->id);
//                    $this->redirect(array('avatar'));
//                    //                    else $this->redirect(array('update', 'id' => $model->id));
//                }
//            }
//        }
//        $this->render('avatar',array(
//            'model' => $model,
//        ));
//    }

//    public function actionChangePass(){
//        if(Yii::app()->user->isGuest){
//            $this->redirect(Yii::app()->getBaseUrl(true));
//        } else {
//
//            //Check xem Id User co trong bang account hay ko
//            $idUser = Yii::app()->user->id;
//            $model = KitAccount::model()->findByPk($idUser);
//            if(empty($model)){
//                $this->redirect(Yii::app()->getBaseUrl(true));
//            }
//            if(isset($_POST['KitAccount'])){
//
//                $userExtra = KitUserExtra::model()->find('id=:ID',array(':ID' => $idUser));
//                $userExtra = CJSON::decode(CJSON::encode($userExtra));
//                if(!empty($userExtra)){
//                    $userExtra = CJSON::decode(CJSON::encode($userExtra));
//                    $userGroup = KitUsergroup::model()->findbypk($userExtra['let_group']);
//                }
//
//
//                $password = letArray::get($_POST,'KitAccount');
//                if($model->password_old == LetIdentityMembers::encodePassword($password['password_old'],$model->salt)){
//                    $model->password = $password['password_new'];
//                    if($model->save()){
//                        KitAccountApplist::setInfoMembersAppList($model,'password');
//                        Yii::app()->user->setFlash('mess','<span style="color: blue">Bạn đổi mật khẩu thành công</span>');
//                    }
//                } else {
//                    Yii::app()->user->setFlash('mess','<span style="color: red">Mật khẩu cũ không đúng. Mời bạn nhập lại</span>');
//                }
//            }
//
//
//        }
//        $this->render('changepass',array(
//            'model' => $model
//        ));
//    }

    // PHP của nó đây
    public function actionEdit(){
        if(Yii::app()->user->isGuest){
            $this->redirect(Yii::app()->getBaseUrl(true));
        }
        $model = KitAccount::model()->findByPk(Yii::app()->user->id);
        if(isset($_POST['KitAccount'])){
            $model->attributes = $_POST['KitAccount'];
            if($model->validate()){
                if($model->save()){
//                    KitAccountApplist::setInfoMembersAppList($model,'update');
                    $model = KitAccount::model()->findByPk(Yii::app()->user->id);
                } else {
                    Yii::app()->user->setFlash('error','Bạn không thể lưu');
                }
            }
        }

        $this->render('edit',array(
            'model' => $model,
        ));
    }

    public function actionPassword(){
        $this->render('password');
    }
    
    public function _clearCache($user_id = NULL){
        Yii::app()->cache->delete(md5('account_details_' . $user_id));
    }
}