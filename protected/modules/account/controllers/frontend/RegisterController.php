<?php

class RegisterController extends ControllerFrontend{

    public function actionIndex()
    {
        $model = new KitAccount();
        if(isset($_POST['KitAccount'])){
            $model->attributes = $_POST['KitAccount'];
            if($model->validate()){
                $data = KitAccount::model()->findByAttributes(array('username' =>$_POST['KitAccount']['username']));
                if(!empty($data)){
                    Yii::app()->user->setFlash('error','Tài khoản đã tồn tại');
                    $this->redirect(Yii::app()->createUrl('account/register'));
                }else{
                    if($model->save()){
                        Yii::app()->user->setFlash('success','Bạn đã đăng ký thành công');
                        $this->redirect(Yii::app()->createUrl('account/register/regsuccess'));
                    }
                }
            }
        }
        $this->render('index',array(
            'model' => $model,
        ));
    }
    public function actions(){
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'maxLength' => 3,
                'minLength' => 2,
            ),
        );
    }

    public function actionRegsuccess(){
        if(!Yii::app()->user->hasFlash('success')){
            $this->redirect(Yii::app()->request->baseUrl);
        }
        $this->render('regsuccess');
    }
}