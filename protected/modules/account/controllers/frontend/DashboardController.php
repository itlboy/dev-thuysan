<?php

class DashboardController extends ControllerFrontend {

    public function actionIndex() {
        $this->checkLogin = TRUE;
        $this->render('index');
    }

}
