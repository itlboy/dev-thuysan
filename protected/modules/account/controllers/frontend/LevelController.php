<?php
/**
 * Module: account
 * Auth:
 * Date: 2012-03-16 11:23:48
 */
class LevelController extends ControllerFrontend
{

	public function actionIndex()
	{

        $data = KitAccountLevel::model()->findAll('status = 1 ORDER BY sorder ASC');
        if(empty($data)){
            $this->redirect(Yii::app()->getBaseUrl(true));
        }

        $data = KitAccountLevel::treatment($data);
        $dataUser = KitAccount::getDetails(Yii::app()->user->id);
        $level = (!empty($dataUser->level)) ? KitAccountLevel::getDetails($dataUser->level) : '';
        if(!empty($level))
            $level = KitAccountLevel::treatment($level);

        $nextLevel = (!empty($dataUser->nextlevel)) ? KitAccountLevel::getDetails($dataUser->nextlevel) : '';
        if(!empty($nextLevel)){
            $nextLevel = KitAccountLevel::treatment($nextLevel);
        }
		$this->render('index',array(
            'data' => $data,
            'level' => $level,
            'nextlevel' =>$nextLevel,
        ));
	}
    /**
     *  So sanh Cap do
     */
    public function actionCompare(){

        if(Yii::app()->user->isGuest){
            $this->redirect(Yii::app()->getBaseUrl(true));
        }
        $data = KitAccount::getDetails(Yii::app()->user->id);
        $level = (!empty($data->level)) ? KitAccountLevel::getDetails($data->level) : '';
        if(!empty($level))
            $level = KitAccountLevel::treatment($level);

        $nextLevel = (!empty($data->nextlevel)) ? KitAccountLevel::getDetails($data->nextlevel) : '';
        if(!empty($nextLevel)){
            $nextLevel = KitAccountLevel::treatment($nextLevel);
        }
        $this->render('compare',array(
            'level' => $level,
            'nextlevel' =>$nextLevel,
        ));
    }



}