<?php

Yii::import('application.modules.account.models.KitAccountStats');
Yii::import('application.modules.account.models.KitAccountLevel');
class LoginController extends ControllerFrontend
{
//    public $defaultAction = 'index';   
    
    public function actionIndex()
    {
        $model = new KitAccount();
        if(!Yii::app()->user->isGuest)
        {
            $this->redirect(Yii::app()->createUrl('account/default/infotsvn'));
        }
        else if(isset($_POST['KitAccount']))
        {
            $modelLogin = new LoginFormMembers();
            $modelLogin->attributes = $_POST['KitAccount'];
            if($modelLogin->validate() and $modelLogin->login() )
            {
                KitAccountStats::updateStats(Yii::app()->user->id);
                KitAccountLevel::updateLevel(Yii::app()->user->id);
                $this->_clearCache(Yii::app()->user->id);
                $this->redirect(Yii::app()->createUrl('account/default/infotsvn'));
            } else {
                Yii::app()->user->setFlash('error','<span class="messageError">Tài khoản hoặc mật khẩu không đúng . Mời bạn nhập lại !</span>');
            }
        }
        $this->render('index',array('model' => $model));
    }
    
    public function actionVailua()
    {
        $model = new LoginFormMembers;
        if(!Yii::app()->user->isGuest)
        {
            $this->redirect(Yii::app()->homeUrl);
        }
        else if(isset($_POST['LoginFormMembers']))
        {
            $model->attributes = $_POST['LoginFormMembers'];
            if($model->validate() and $model->login() )
            {
                KitAccountStats::updateStats(Yii::app()->user->id);
                KitAccountLevel::updateLevel(Yii::app()->user->id);
                $this->_clearCache(Yii::app()->user->id);
                $this->redirect(Yii::app()->homeUrl);
            } else {
                Yii::app()->user->setFlash('error','<span class="messageError">Tài khoản hoặc mật khẩu không đúng . Mời bạn nhập lại !</span>');
            }
        }
        $this->layout = '//layouts/login';
        $this->render('index',array('model' => $model));
    }
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->createUrl('/account/login'));
    }

    public function _clearCache($user_id){
        Yii::app()->cache->delete('config_unlimit_'.$user_id);
    }
}