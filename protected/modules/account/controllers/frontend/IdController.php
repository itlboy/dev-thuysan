<?php

class IdController extends ControllerFrontend {

    public $layout = '//layouts/login';

    public function actions() {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction'
            ),
        );
    }

    public function actionLogin() {
        $result['result'] = 'success';

        $email = letArray::get($_POST, 'email');
        $password = letArray::get($_POST, 'password');
        $identity = new LetIdentityId($email, $password);
        if ($identity->authenticate()) {
            Yii::app()->user->login($identity);
            $result['message'] = $this->renderPartial('//_blocks/leftuser', NULL, TRUE);
        } else {
            $result['result'] = 'error';
            $result['message'] = $identity->messages['error'];
        }
        echo json_encode($result);

//        $duration = $this->rememberMe ? 3600 * 24 * 30 : 0; // 30 days
//        Yii::app()->user->login($this->_identity, $duration);

    }

    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionRegister() {
        if (!Yii::app()->user->isGuest)
            $this->redirect(Yii::app()->homeUrl);

        $model = new KitAccount('create');
        if (isset($_POST['KitAccount'])) {
            $model->attributes = $_POST['KitAccount'];
            if ($model->validate()) {
                $password = $model->password;
                if ($model->save(FALSE)) {
                    Yii::app()->user->setState('register', array('username' => $model->username, 'password' => $password));
                    Yii::app()->user->setFlash('success', 'Bạn đã đăng ký thành công');
                    KitAccountApplist::setInfoMembersAppList($model);
                    $this->refresh();
                }
            }
        }

        $this->render('register', array('model' => $model));
    }

    public function actionSuccess() {
        $info = Yii::app()->user->getState('register');
        if ($info === NULL OR !is_array($info) OR !isset($info['username']) OR !isset($info['password'])) {
            $this->redirect(Yii::app()->homeUrl);
        }

        $identity = new LetIdentityId($info['username'], $info['password']);
        if ($identity->authenticate()) {
            Yii::app()->user->login($identity);
            Yii::app()->user->setState('register', NULL);
        }

        $this->redirect(Yii::app()->homeUrl);
    }

}
