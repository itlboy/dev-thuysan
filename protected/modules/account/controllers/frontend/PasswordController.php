<?php

class PasswordController extends ControllerFrontend {

    public function actionPassword1() {
        $this->checkLogin = TRUE;
        $msg = array();
        if (isset($_POST['submit'])) {
            $password = letArray::get($_POST, 'password');
            $newpassword = letArray::get($_POST, 'newpassword');
            $newpassword_confirm = letArray::get($_POST, 'newpassword_confirm');
            
            $user = KitAccount::getDetails(Yii::app()->user->id, TRUE);
            
            // Kiem tra ky tu cua mat khau moi
            if (strlen($newpassword) < 6)
                $msg['error'][] = 'Mật khẩu phải ít nhất 6 ký tự';
            
            if ($newpassword !== $newpassword_confirm)
                $msg['error'][] = 'Xác nhận mật khẩu không chính xác';
                
            // Kiem tra da nhap mạt khau cap 1 chưa
            if ($password == NULL)
                $msg['error'][] = 'Bạn chưa nhập mật khẩu cấp 1';

            // Kiem tra xem mat khau cap 1 cu co trung voi mat khau nhap vao ko
            if (LetIdentityId::encodePassword($password, $user->salt) !== $user->password_old)
                $msg['error'][] = 'Nhập mật khẩu cấp 1 sai';
            
            if (!isset($msg['error']) OR empty($msg['error'])) {
                $user->password_old = LetIdentityId::encodePassword($newpassword, $user->salt);
                $user->save();
                $msg['success'][] = 'Đổi mật khẩu thành công!';
            }
            
//            echo '<pre>';
//            var_dump($msg);
//            echo '</pre>';
        }

        $assign = array();
        $assign['msg'] = $msg;
        $this->render('password1', $assign);
    }

    public function actionPassword2() {
        $this->checkLogin = TRUE;
        $msg = array();
        
        $model = KitAccount::model()->findByPk(Yii::app()->user->id);
        //Nếu chưa có mật khẩu cấp 2
        if(empty($model->password2)){
            $this->redirect(array('makePassword2'));
        }
        
        if($model){
            $model->setScenario('changePass2');
            $currentPassword2 = $model->password2;
            $model->unsetAttributes(array('password2', 'new_password2', 'new_password2_confirm'));
        }

        if(isset($_POST['KitAccount'])){
            $model->attributes = $_POST['KitAccount'];
            $model->email_code = $_POST['KitAccount']['email_code'];
            $model->otp_code = $_POST['KitAccount']['otp_code'];
            
            if($model->validate()){
                //Check current password2
                $password2 = LetIdentityId::encodePassword($model->password2, $model->salt);
                if ($password2 !== $currentPassword2)
                    $msg['error'][] = 'Mật khẩu cấp 2 không chính xác.';
                else{
                    //Check confirm email code
                    $emailCode = KitAccountOtp::model()->findByAttributes(array('type'=>'email', 'email'=>$model->email ,'otp' => $model->email_code));
                    if(!$emailCode){
                        $msg['error'][] = 'Mã xác nhận Email không chính xác.';
                    }
                    
                    $phoneCode = KitAccountOtp::model()->findByAttributes(array('type'=>'phone', 'email'=>$model->email ,'otp' => $model->otp_code));
                    if(!$phoneCode){
                        $msg['error'][] = 'Mã OTP không chính xác.';
                    }
                    
                    if($emailCode->id > 0 AND $phoneCode->id > 0){
                        //Change passowrd 2
                        $model->password2 = LetIdentityId::encodePassword($model->new_password2, $model->salt);
                        if($model->update()){
                            $msg['success'][] = 'Đổi mật khẩu cấp 2 thành công.';
                            $model->unsetAttributes(array('password2', 'new_password2', 'new_password2_confirm', 'email_code', 'otp_code'));
                        }
                        
                        //Delete otp row
                        $emailCode->delete();
                        $phoneCode->delete();
                    }
                }
                
            }else{
                //Get errors message
                $msg['error'] = array();
                $errors = $model->getErrors('password2');
                if($errors && is_array($errors)){
                    foreach($errors as $error){
                        $msg['error'][] = $error;
                    }
                }
                $errors = $model->getErrors('new_password2');
                if($errors && is_array($errors)){
                    foreach($errors as $error){
                        $msg['error'][] = $error;
                    }
                }
                $errors = $model->getErrors('new_password2_confirm');
                if($errors && is_array($errors)){
                    foreach($errors as $error){
                        $msg['error'][] = $error;
                    }
                }
                $errors = $model->getErrors('email_code');
                if($errors && is_array($errors)){
                    foreach($errors as $error){
                        $msg['error'][] = $error;
                    }
                }
                $errors = $model->getErrors('otp_code');
                if($errors && is_array($errors)){
                    foreach($errors as $error){
                        $msg['error'][] = $error;
                    }
                }
            }
        }
        
        $assign = array();
        $assign['msg'] = $msg;
        $assign['model'] = $model;
        $this->render('password2', $assign);
    }
    
    public function actionMakePassword2() {
        $this->checkLogin = TRUE;
        $msg = array();
        
        $model = KitAccount::model()->findByPk(Yii::app()->user->id);
        if(!empty($model->password2))
            $this->redirect (array('password2'));
        
        if($model){
            $model->setScenario('makePass2');
            $model->unsetAttributes(array('password2', 'password2_confirm'));
        }

        if(isset($_POST['KitAccount'])){
            $model->attributes = $_POST['KitAccount'];
            
            if($model->validate()){
                //Check current password2
                $password2 = LetIdentityId::encodePassword($model->password2, $model->salt);
                $model->password2 = $password2;
                if($model->update()){
                    $msg['success'][] = "Bạn đã tạo mật khẩu cấp 2 thành công.";
                    $model->unsetAttributes(array('password2', 'password2_confirm'));
                }
            }else{
                $errors = $model->getErrors('password2');
                if($errors && is_array($errors)){
                    foreach($errors as $error){
                        $msg['error'][] = $error;
                    }
                }
                $errors = $model->getErrors('password2_confirm');
                if($errors && is_array($errors)){
                    foreach($errors as $error){
                        $msg['error'][] = $error;
                    }
                }
            }
        }
        
        $assign = array();
        $assign['msg'] = $msg;
        $assign['model'] = $model;
        $this->render('make_password2', $assign);
    }
    
    public function actionAjaxSendMail(){
        $this->checkLogin = TRUE;
        $results = array(
            'success' => FALSE,
            'message' => ''
        );
        if(Yii::app()->request->isAjaxRequest){
            $model = KitAccount::model()->findByPk(Yii::app()->user->id);
            //Make confirm code
            $confirmCode = Common::randomString(8);
            
            //Make params
            $params = array(
                'email_code' => 'send_confirm_code_to_member',
                'to_email' => array($model->email => $model->username),
                'username' => $model->username,
                'confirm_code' => $confirmCode,
            );

            //Send mail to verify email 1
            Yii::import('application.modules.mail.models.*');
            $resultSendMail = KitMailSender::sendMail($params);
            if($resultSendMail){
                //Save email code to account otp table
                $accountOTP = new KitAccountOtp();
                $accountOTP->user_id = $model->id;
                $accountOTP->username = $model->username;
                $accountOTP->email = $model->email;
                $accountOTP->phone = $model->phone;
                $accountOTP->otp = $confirmCode;
                $accountOTP->type = "email";
                $accountOTP->time = date("Y-m-d H:i:s");
                if($accountOTP->save()){
                    $results['message'] = "Đã gửi mã xác nhận email. Hãy kiểm tra hòm thư của bạn để có mã xác nhận Email.";
                }else{
                    $results['message'] = "Xin lỗi, hệ thống tạm thời chưa tạo được mã xác nhận email. Hãy thử lại sau ít phút.";
                }
            }else{
                $results['message'] = "Xin lỗi, hệ thống tạm thời chưa gửi được mã xác nhận vào email của bạn. Hãy thử lại sau ít phút.";
            }
        }
        
        echo json_encode($results);
    }
}
