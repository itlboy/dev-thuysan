<?php
/**
 * Module: account
 * Auth:
 * Date: 2012-03-16 11:23:48
 */
class LevelController extends ControllerBackend
{
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model=KitAccountLevel::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }


	public function actionIndex()
	{
//        $model = new KitAccountLevel();

        $this->dataUpdate();
        $data = KitAccountLevel::model()->findAll('1 = 1 ORDER BY sorder ASC');
        if(!empty($data)){
            $data = KitAccountLevel::treatment($data);
        }
        $this->render('index',array(
            'data' => $data
        ));
	}

    public function dataUpdate(){
        if(isset($_POST['KitAccountLevel']))
        {
            $data = letArray::get($_POST,'KitAccountLevel');

            $id = $data['id'];
            if(!empty($id)){
                $titles = !empty($data['title']) ? $data['title'] : '';
                $sorder = !empty($data['sorder']) ? $data['sorder'] : '';
                $image_link = !empty($data['image_link']) ? $data['image_link'] : '';
                $status = !empty($data['status']) ? $data['status'] : '';
                $configs = !empty($data['config']) ? $data['config'] : '';

                // Dua mang config ve dang mang chuan
                // array('index' => array(
                //      'field' =>
                //      'comparing' =>                  so sanh
                //      'conditions' =>              dieu kien
                //));

                $config = array();
                $i = 0;
                if(!empty($configs)){
                    foreach($configs as $items){
                        $arr = array();
                        foreach($items as $key => $item){
                            for($j = 0; $j < count($item); $j++){
                                foreach($items as $key => $item){
                                    $arr[$j][$key] = $item[$j];
                                }
                            }
                        }
                        $config[$i] = $arr;
                        $i++;
                    }
                }
                $mess = array();
                for($i = 0; $i < count($id); $i++){
                    if(isset($id[$i])){
                        $modelSave = KitAccountLevel::model()->findByPk($id[$i]);
                        $modelSave->title = $titles[$i];
                        $modelSave->sorder = $sorder[$i];
                        $modelSave->image_link = $image_link[$i];
                        $modelSave->status = $status[$i];
                        $modelSave->config = !(empty($config[$i])) ? json_encode($config[$i]) : '';
                        if(!$modelSave->save()){
                            Yii::app()->cache->delete(md5('KitAccountLevel::getDetails_'.$id[$i]));
                            $mess[] = 'ERROR: id = '.$id[$i].' - '.$titles[$i];
                        };
                    }
                }

                if(!empty($mess)){
                    Yii::app()->user->setFlash('error',implode(',',$mess));
                }

            }
        }
    }

    public function actionCreate()
    {
        $model = new KitAccountLevel();
        $model->status = 1;
        if (isset($_POST['KitAccountLevel'])){

            $attributes = letArray::get($_POST,'KitAccountLevel');

            $titles = $attributes['title'];

            if(!empty($titles)){
            $image_links = $attributes['image_link'];
            $sorder = $attributes['sorder'];
            $status = $attributes['status'];

                for($i = 0; $i < count($titles); $i++){
                    $modelSave = new KitAccountLevel();
                    $modelSave->title = $titles[$i];
                    $modelSave->sorder = $sorder[$i];
                    $modelSave->image_link = $image_links[$i];
                    $modelSave->status = $status[$i];
                    $modelSave->save();
                }
                $this->getAjaxStatus('success', 'Data saved');

                Yii::app()->end();

            } else {
                if (!empty($errors)) {
                    $this->getAjaxStatus('error', 'Vui lòng kiểm tra lại thông tin trước khi lưu');
                    Yii::app()->end();
                }
            }
        }

        $this->layout = FALSE;
        $this->render('edit',array(
            'model' => $model,
        ));
    }
    public function actionTest(){
        $id = '35025';
        echo Common::getFolderAvatar('discuz',$id);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete() {

        if(isset($_POST['id']))
        {
            $model=KitAccountLevel::model()->findByPk(letArray::get($_POST,'id'));
            if($model->delete()){
                echo 'true';
            } else {
                echo 'false';
            }
        }
    }

    private function getAjaxStatus($status, $message='', $jrvalue='', $jrvalid='') {
        echo json_encode(array(
            'status' => $status,                // trạng thái success, warning, error
            'message' => $message,              // thông tin đã được lưu
            'jrvalue' => $jrvalue,              // danh sách các ID trả về
            'jrvalid' => $jrvalid,              // thông tin model valid error
        ));
    }
}