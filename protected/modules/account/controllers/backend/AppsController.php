<?php

class AppsController extends ControllerBackend
{
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = KitAccountApplist::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404,'The requested page does not exist.');

        //        $model->groups = KitUserExtra::model()->getMember2Group($model->id);
        return $model;
    }

	public function actionIndex(){
        $model = new KitAccountApplist();
        $model->unsetAttributes();  // clear any default values

        // danh cac bang
        $tables = Yii::app()->db->schema->getTableNames();

        if(isset($_GET['id'])){
            $model = $this->loadModel(letArray::get($_GET,'id'));
        }

        $this->save($model);

        $this->render('index',array(
            'model' => $model,
            'tables' => $tables,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $id = (int) $id;
        $model = $this->loadModel($id);

        $this->save($model);

        $this->render('edit',array(
            'model' => $model,
        ));
    }

    public function actionCreate(){
        $model = new KitAccountApplist();
        $this->save($model);

        $this->render('edit',array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }

    protected function save($model, $groupsOld = NULL) {
        if(isset($_POST['KitAccountApplist'])) {
            $model->attributes = $_POST['KitAccountApplist'];
            if(isset($_POST['config'])){
                $model->config = json_encode($_POST['config']);
            }
            $model->status = 1;
            if ($model->validate()) {
                if($model->save()) {
                    if (Yii::app()->request->getPost('apply') == 0)
                        $this->redirect(array('index'));
                    else $this->redirect(array('update', 'id' => $model->id));
                }
            }
        }
    }


}
