<?php

class LoginController extends ControllerBackend
{
//    public $defaultAction = 'index';

    public function actionIndex()
    {
        $model = new LoginFormMembers;

		if (isset($_POST['LoginFormMembers']))
		{
			$model->attributes = $_POST['LoginFormMembers'];
            $model->checkAdmin = true;
			if ($model->validate() && $model->login()) {
                $this->redirect(array('//cms'));
			}
		}
		$this->layout = '//layouts/login';
        $this->render('login',array('model'=>$model));
    }

}