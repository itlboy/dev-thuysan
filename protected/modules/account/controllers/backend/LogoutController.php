<?php

class LogoutController extends ControllerBackend
{
    public $defaultAction = 'index';
    
    public function actionIndex()
    {
        Yii::app()->user->logout();
        $model = new LoginFormMembers();
        
		$this->layout = '//layouts/login';
        $this->render('/login/login',array('model'=>$model));
        //$this->redirect(Yii::app()->homeUrl);
    }
}