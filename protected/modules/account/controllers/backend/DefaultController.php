<?php

class DefaultController extends ControllerBackend
{
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id) {
		$model = KitAccount::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404,'The requested page does not exist.');
        
//        $model->groups = KitAccountGroup::model()->getMember2Group($model->id);
		return $model;
	}

    public function actionIndex() {
        $model = new KitAccount('search');
        $model->unsetAttributes();  // clear any default values
        
        
        if (isset($_GET['KitAccount'])) {
            $model->attributes = $_GET['KitAccount'];
        }

        // page size drop down changed
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$model = new KitAccount('create');   
//        $model->groups = array();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		$this->save($model);

        $this->render('edit',array(
			'model'=>$model,
		));
	}

	    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $id = (int) $id;
        $model = $this->loadModel($id);
        $groupsOld = $model->groups = KitAccountGroup::model()->getMember2Group($id);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        $this->save($model, $groupsOld);

        $this->render('edit',array(
            'model' => $model,
        ));
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionInfo($id) {
        $model = KitAccount::getDetails($id);
        
		$this->render('info',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id) {
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	protected function save($model, $groupsOld = NULL) {
		if(isset($_POST['KitAccount'])) {

            $model->attributes = $_POST['KitAccount'];
            $groupsNew = $model->groups;
            
            if ($model->validate()) {
                if($model->save()) {                    
                    if (!$model->isNewRecord)
                        KitAccountGroup::deleteMemberGroup($groupsOld, $model->id);
                    KitAccountGroup::createMemberGroup($groupsNew, $model->id);
                    
                    if (Yii::app()->request->getPost('apply') == 0)
                         $this->redirect(array('index'));
                    else $this->redirect(array('update', 'id' => $model->id));
                }
            }
		}
	}
    
	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model) {
		if(isset($_POST['ajax']) && $_POST['ajax']==='forum-ucenter-members-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
