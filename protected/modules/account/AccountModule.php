<?php

class AccountModule extends BaseModule
{    
    public $returnUrl;
    public $defaultLetGroup = 1;
    public $defaultLetActive = 1;

    public $category = FALSE; // Xác định Module có category hay không
    public $option = FALSE; // Xác định Module có option hay không
    public $comment = FALSE; // Xác định Module có dùng comment hay không
    public $media = FALSE; // Xác định Module có dùng media hay không
    
    public $menuList = array(
        array(
            'name' => 'Tạo Account',
            'url' => '/account/default/create',            
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Tạo mới Account',
            'type' => 'direct'
//            'type' => 'dialog',
//            'grid' => 'kit_account_grid',
        ),
        array(
            'name' => 'Quản lý Account',
            'url' => '/account/default/index',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Quản lý danh sách Account',
            'type' => 'direct'
        ),
        array(
            'name' => 'Quản lý Phân quyền',
            'url' => '/srbac/default/index',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Quản lý Phân quyền',
            'type' => 'direct'
        ),
        array(
            'name' => 'Người dùng cho ứng dụng',
            'url' => '/account/apps/index',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Người dùng cho ứng dụng',
            'type' => 'direct'
        ),
        array(
            'name' => 'Quản lý Cấp bậc',
            'url' => '/account/level/index',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Quản lý Cấp bậc',
            'type' => 'direct'
        ),
//        array(
//            'name' => 'Thêm Cấp bậc',
//            'url' => '/account/level/create',
//            'icon' => '/images/icons/small/grey/coverflow.png',
//            'desc' => 'Quản lý Cấp bậc',
//            'type' => 'dialog',
//            'grid' => '',
//        ),

    );
    
    public $image = array(
        'origin' => array(
            'type' => 'move',
            'folder' => 'origin',
        ),
        'large' => array(
            'type' => 'crop',
            'width' => 203,
            'height' => 203,
            'cropWidth' => NULL,
            'cropHeight' => NULL,
            'cropTop' => NULL,
            'cropLeft' => NULL,
            'rotate' => 0, // Xoay anh
            'flip' => NULL, //[integer] Direction. Horizontal = 5, Vertical = 6
            'quality' => 100,
            'folder' => 'large',
        ),
        'medium' => array(
            'type' => 'crop',
            'width' => 40,
            'height' => 40,
            'cropWidth' => NULL,
            'cropHeight' => NULL,
            'cropTop' => NULL,
            'cropLeft' => NULL,
            'rotate' => 0, // Xoay anh
            'flip' => NULL, //[integer] Direction. Horizontal = 5, Vertical = 6
            'quality' => 100,
            'folder' => 'medium',
        ),
        'small' => array(
            'type' => 'crop',
            'width' => 27,
            'height' => 27,
            'cropWidth' => NULL,
            'cropHeight' => NULL,
            'cropTop' => NULL,
            'cropLeft' => NULL,
            'rotate' => 0, // Xoay anh
            'flip' => NULL, //[integer] Direction. Horizontal = 5, Vertical = 6
            'quality' => 100,
            'folder' => 'small',
        ),
    );

    public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'account.models.*',
			'account.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
