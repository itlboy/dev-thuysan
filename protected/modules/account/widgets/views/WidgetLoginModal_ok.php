<style type="text/css">
    .header .link{top:5px;}
    .box-profile li{padding: 2px 0px;}
    .box-profile li ul{ width: 120px;top:14px;padding: 0px;}
    .box-profile li:hover{background-color: #eee;}
    .box-profile li:hover ul{display: block !important;}
    .box-profile li ul li{width: 97%; margin-left: 0px !important;padding-left: 5px;}
</style>
<div style="font-size: 12px;" class="clearfix">
    <div class="fr" style="width:300px; height: 73px;">


        <div style="margin-top: -2px;margin-top: 2px; padding: 2px 0px;" class="fr">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/let_gold_16x16.png" height="15"  width="15" style="vertical-align: top;" /> <a title="Nạp xu" data-placement="bottom" rel="tooltip" data-original-title="Nạp xu" href="http://id.let.vn/currency/pay.let" style="margin-right: 5px;"><?php echo number_format(KitCmsConfig::convertMoneyToGold(KitAccountStats::getField(Yii::app()->user->id,'money_balance'),'default'),0,',','.') ?> <?php echo Yii::t('global','letgold'); ?></a>
        </div>
        <ul class="nav nav-pills box-profile fr">
            <li ><a target="_blank" title="Cấp độ: <?php echo KitAccountLevel::getField(Yii::app()->user->level,'title'); ?>" data-placement="bottom" rel="tooltip" data-original-title="Cấp độ: <?php echo KitAccountLevel::getField(Yii::app()->user->level,'title'); ?>" href="http://id.let.vn/account/level/index.let" style="float: none; display: block;padding: 0px;margin: 0px;display: block;height: 16px;width:16px;"><img style="vertical-align: middle;height: 15px; margin: 3px 2px; float: left;" src="<?php echo KitAccountLevel::getField(Yii::app()->user->level,'image_url') ?>" height="20" /></a></li>
            <li class="dropdown" style="margin-left: 0px !important;">
                <a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button" target="_blank" href="http://forum.let.vn/space-username-<?php echo Yii::app()->user->name; ?>.html" style="margin-right: 5px;font-weight: bold;color: #630D0D;float: right;margin-left: 5px;padding: 0px;"><?php echo Yii::app()->user->name; ?><b class="caret"></b></a>

                <?php $this->widget('bootstrap.widgets.TbMenu', array(
                    'type'=>'list',
                    'htmlOptions'=>array('class'=>'user-info dropdown-menu','role' => 'menu','style' => 'display:none;'),
                    'items'=>array(
                        array('label'=>'Thông tin tài khoản', 'icon'=>'home', 'url'=> 'http://id.let.vn/account/profile/info.let'),
                        array('label'=>'Đổi avatar', 'icon'=>'user', 'url'=> 'http://id.let.vn/account/profile/avatar.let'),
                        array('label'=>'Đổi mật khẩu', 'icon'=>'book', 'url'=> 'http://id.let.vn/account/profile/changepass.let','itemOptions' => array('style' => 'border-bottom:1px solid #e3e3e3;')),
                        array('label'=>'Nạp xu', 'icon'=>'pencil', 'url'=> 'http://id.let.vn/currency/pay.let'),
                        array('label'=>'Lịch sử giao dịch', 'icon'=>'list', 'url'=> 'http://id.let.vn/currency/history.let','itemOptions' => array('style' => 'border-bottom:1px solid #e3e3e3;')),
                        array('label'=>'Thoát', 'icon'=>'off', 'url'=> Yii::app()->createUrl('account/ajax/logout')),
                    )
                )); ?>
            </li>
        </ul>
        <?php if(!Yii::app()->user->isGuest): ?>
        <?php $memberConfig = KitAccountConfig::getField(Yii::app()->user->id,'film_unlimit'); ?>
        <?php if(empty($memberConfig)): ?>
            <?php $message = 'Kích hoạt tài khoản'; ?>
            <?php else: ?>
            <?php $message = 'Gia hạn tài khoản'; ?>
            <?php if(strtotime($memberConfig) > time()): // kiem tra film_unlimit voi thoi gian hien tai ?>
                <?php $day = Yii::t('global', date('l', strtotime ($memberConfig))); ?>
                <div class="fr" style="margin-top: -2px;margin-top: 2px; padding: 2px 0px; text-align: right; color: green; font-weight: bold;" title="Hết hạn lúc <?php echo date('H:i:s d/m/Y', strtotime ($memberConfig)); ?>">
                    <?php echo letDate::fuzzy_span(strtotime($memberConfig)) ?>
                </div>
                <?php else: ?>
                <div class="fr" style="margin-top: -2px;margin-top: 2px; padding: 2px 0px; color: red; text-align: right; font-weight: bold;">Tài khoản hết hạn</div>
                <?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>
                
        <a class="btn btn-success" href="http://id.let.vn/currency/pay/index.let?type=film_unlimit" style="position: absolute;top: 27px;right: 0px;"><?php echo $message; ?></a>
        <p style="font-size: 11px; text-align: center; position: absolute; bottom: 0px; right: 0px;"><strong style="color: #006AA8;"><span style="color: black;font-weight: normal;">Soạn tin:</span> LET <span style="color: green">PHIM</span> <span style="color: #9203C1;"><?php echo (!Yii::app()->user->isGuest) ? Yii::app()->user->id : 'ID'  ?></span> <span style="color: black;font-weight: normal;">gửi</span> <span style="color: red">8771</span></strong></a></p>
    </div>
</div>

