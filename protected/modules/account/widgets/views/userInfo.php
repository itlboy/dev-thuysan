<div class="box-user">
    <div class="box-user-info">
        <p><a href="http://id.let.vn/account/profile/avatar.let" rel="nofollow"><img width="100%" src="<?php echo KitAccount::getAvatar(Yii::app()->user->id,'large'); ?>"/></a></p>
        <p>Chào <strong><?php echo Yii::app()->user->name; ?></strong></p>
        <p class="info-level">Cấp độ: <img style="vertical-align: middle;cursor: pointer"  onclick="return window.open('http://id.let.vn/account/level/index.let');" src="<?php echo KitAccountLevel::getField(Yii::app()->user->level,'image_url') ?>" height="20" />&nbsp;<?php echo KitAccountLevel::getField(Yii::app()->user->level,'title') ?>
            <a title="Thông tin cấp độ" data-placement="bottom" rel="tooltip" data-original-title="Cấp độ: <?php echo KitAccountLevel::getField(Yii::app()->user->level,'title'); ?>" href="http://id.let.vn/account/level/index.let" style="margin-left: 5px ;margin-left: 5px;padding: 0px;" target="_blank" ><img  src="<?php echo Yii::app()->theme->baseUrl; ?>/images/let_help_16x16.png" /></a>
        </p>

        <p>Bạn có: <?php echo number_format(KitCmsConfig::convertMoneyToGold(KitAccountStats::getField(Yii::app()->user->id,'money_balance'),'default'),0,',','.'); ?> <?php echo Yii::t('global','letgold'); ?></p>
        <?php $this->widget('bootstrap.widgets.TbMenu', array(
            'type'=>'list',
            'htmlOptions'=>array('class'=>'user-info'),
            'items'=>array(
                array('label'=>'Thông tin tài khoản', 'icon'=>'home', 'url'=> 'http://id.let.vn/account/profile/info.let'),
                array('label'=>'Đổi avatar', 'icon'=>'user', 'url'=> 'http://id.let.vn/account/profile/avatar.let'),
                array('label'=>'Đổi mật khẩu', 'icon'=>'book', 'url'=> 'http://id.let.vn/account/profile/changepass.let'),
                array('label'=>'Nạp xu', 'icon'=>'pencil', 'url'=> 'http://id.let.vn/currency/pay.let'),
                array('label'=>'Lịch sử giao dịch', 'icon'=>'list', 'url'=> 'http://id.let.vn/currency/history.let'),
                array('label'=>'Thoát', 'icon'=>'off', 'url'=> Yii::app()->createUrl('account/ajax/logout')),
            )
        )); ?>
    </div>
</div>