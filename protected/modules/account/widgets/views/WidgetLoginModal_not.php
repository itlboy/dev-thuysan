<div id="widget-login-modal">
    <ul>
        <li><a href="#loginModal" data-toggle="modal" rel="nofollow">Đăng nhập</a></li>
        <li>| <a href="http://id.let.vn" rel="nofollow" target="_blank">Đăng ký</a></li>
    </ul>
</div>
<!-- Login Modal -->
<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'loginModal')); ?>
<?php /** @var TbActiveForm $form */
$model = new KitAccount();
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'loginFormModal',
    'action' => Yii::app()->createUrl('//account/frontend/ajax/login'),
    'htmlOptions'=>array(
        'class'=>'',
        'onSubmit' => "js:ajaxLoginModal(jQuery(this).attr('id'), jQuery(this).attr('action')); return false"
    ),
)); ?>
<div class="modal-header">
    <a class="close" data-dismiss="modal" rel="nofollow">&times;</a>
    <h3>Đăng nhập</h3>
</div>
<div class="modal-body">
    <?php echo $form->textFieldRow($model, 'username', array('style'=>'width:100%')); ?>
    <?php echo $form->passwordFieldRow($model, 'password', array('style'=>'width:100%')); ?>
</div>
<div class="modal-footer">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'type'=>'primary',
        'icon'=>'ok',
        'label'=>'Đăng nhập',
        'url'=>'javaScript:void(0);',
        'htmlOptions'=>array('rel' => 'nofollow'),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Đóng',
        'url'=>'javaScript:void(0);',
        'htmlOptions'=>array('data-dismiss'=>'modal', 'rel' => 'nofollow'),
    )); ?>
</div>
<?php $this->endWidget(); ?>
<?php $this->endWidget(); ?>
<!-- END Modal -->

<script>
function ajaxLoginModal(form, url) {
    jQuery.ajax({
        'url':url,
        'type':'POST',
        'data':jQuery('#'+form).serialize(),
        'dataType':'JSON',
        'beforeSend':function() {
            
        },
        'success':function(data) {            
            if (data.status == 'success') {
                 window.location.reload();
            } else {
                alert(data.message);
            }
        }
    });
}
</script>