<div id="widget-login-form">
    <div style="background: whitesmoke; padding: 15px;">
        <?php /** @var TbActiveForm $form */
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id'=>'loginForm',
            'type'=>'inline',
            'action' => Yii::app()->createUrl('//account/frontend/ajax/login'),
            'htmlOptions'=>array(
                'class'=>'',
                'onSubmit' => "js:ajaxLoginForm(jQuery(this).attr('id'), jQuery(this).attr('action')); return false"
            ),
        )); ?>
        <?php echo $form->textFieldRow($model, 'username', array('style'=>'width:95%;margin-bottom:5px;')); ?>
        <?php echo $form->passwordFieldRow($model, 'password', array('style'=>'width:95%;margin-bottom:5px;')); ?>
        <?php // echo $form->checkboxRow($model, 'checkbox'); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'icon'=>'ok', 'label'=>'Đăng nhập')); ?>
        <?php $this->endWidget(); ?>
        <div id="widget-login-msg" style="display:none;text-align: center;margin-top: 10px;"></div>
    </div>
<!--    --><?php //$this->render('userInfo'); ?>
</div>

<script>
function ajaxLoginForm(form, url) {
    jQuery.ajax({
        'url':url,
        'type':'POST',
        'data':jQuery('#'+form).serialize(),
        'dataType':'JSON',
        'beforeSend':function() {
            jQuery('#widget-login-msg').html('<img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif" />').show();
        },
        'success':function(data) {
            jQuery('#widget-login-msg').html('').hide();
            if (data.status == 'success') {
                jQuery('#widget-login-form').html(data.contentForm);
                jQuery('#widget-login-modal').html(data.contentModal);
            } else {                
                jQuery('#widget-login-form').html(data.contentForm);
                jQuery('#widget-login-msg').text(data.message).show();
            }
        }
    });
}
</script>
