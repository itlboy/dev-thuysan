<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'FormForGot',
        'type'=>'inline',
        'htmlOptions' => array('class' => 'well'),
    )
);echo "\n";
?>
    <h1 class="title-register" style="border-bottom: 1px dashed #999;" >Lấy lại mật khẩu</h1>
    <label>Tài khoản / Email : </label>
<input class="input-large" placeholder="Nhập username hoặc email..." name="keyword" id="forgot_keyword" type="text" />
    <div class=" btn-forgot" style="text-align: right;margin-top: 5px">
        <button onclick="return forgotPasword();" class="btn btn-primary"><i class="icon-ok icon-white"></i> Gửi yêu cầu</button>
    </div>
    <div id="process_forgot" style="margin-top: 10px;">
        <div class="alert in alert-block fade alert-error" style="display: none">
            <div class="messenger"></div>
        </div>
        <div class="alert in alert-block fade alert-success" style="display: none">
            <div class="messenger"></div>
        </div>
        <span class="process-loadding"></span>
    </div>
<?php $this->endWidget();echo "\n"; ?>
<script type="text/javascript">
    function forgotPasword(){
        $(document).ready(function(){
            var keyword = $('#forgot_keyword').val();
            if(keyword == ''){
                alert('Bạn chưa nhập đủ thông tin');
                return false;
            }
            $.ajax({
                type:'POST',
                url:'<?php echo Yii::app()->createUrl('account/ajax/ajaxforgot'); ?>',
                data:$('#FormForGot').serialize(),
                beforeSend:function(){
                    $('.process-loadding').html('<center style="margin-top: 10px;"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif" /></center>');
                },
                success:function(data){
                    eval('data='+data);
                    if(data.result == 'false'){

                        $('#process_forgot .alert-error .messenger').html(data.mess);
                        $('#process_forgot .alert-error').show();
                        $('#process_forgot .alert-success').hide();
                    } else {
                        $('#process_forgot .alert-success .messenger').html(data.mess);
                        $('#process_forgot .alert-success').show();
                        $('#process_forgot .alert-error').hide();
                    }
                    $('.process-loadding').html('');
                }
            });
        });
        return false;
    }
</script>
