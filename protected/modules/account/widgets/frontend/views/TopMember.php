<?php if($this->tabs !== array()): ?>
<?php $divId = 'result_' . rand(1000000, 9999999); ?>
<script type="text/javascript">
    $('#<?php echo $divId ?>_tabs a.btn').live('click',function(){
        var link = $(this).data("link");
        ajaxLoadFile(link,'<?php echo $divId; ?>');
        $('#<?php echo $divId ?>_tabs a').removeClass('active');
        $(this).addClass('active');
        return false;
    });
</script>
<!--<div class="heading_s1 heading_tabs">--><?php //echo $this->title; ?><!--</div>-->
<div id="<?php echo $divId ?>_tabs">
    <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'buttons'=> $this->tabs,
    )); ?>
</div>
<div id="<?php echo $divId; ?>">
<?php endif; ?>
    <?php if(!empty($data)): ?>

    <div class="other-channels-compact" style="margin-top: 5px;">
        <ul class="channel-summary-list ">
            <?php foreach($data as $item): ?>
            <li class="yt-tile-visible yt-uix-tile">
                <div class="channel-summary clearfix channel-summary-compact" style="position: relative;">
                    <div class="channel-summary-thumb">
                        <span class="video-thumb ux-thumb yt-thumb-square-46 "><span class="yt-thumb-clip"><span class="yt-thumb-clip-inner">
                            <img onerror="this.src='<?php echo Yii::app()->theme->baseUrl ?>/images/no-avatar.png'" src="<?php echo (!empty($item['avatar'])) ? Common::getImageUploaded('account/small/'.$item['avatar']) : 'http://forum.let.vn/uc_server/avatar.php?uid='.$item['id']; ?>" alt="Hình thu nhỏ" data-thumb="//i3.ytimg.com/i/jGStT5Wk2xBYfNYDxY6fXQ/1.jpg?v=4f7bf475" width="46" data-group-key="thumb-group-3">
                        <span class="vertical-align"></span></span></span></span>
                    </div>
                    <div class="channel-summary-info">
<!--                        <h3 class="channel-summary-title" style="height: 20px;">-->
                            <a style="font-weight: bold; font-size: 13px;" href="javascript:;" class="yt-uix-tile-link"><?php echo $item['username']; ?></a>
                            <a rel="nofollow" href="http://id.let.vn/account/level/index.let"><img title="<?php echo $item['title']; ?>" style="vertical-align: middle;height: 25px; position: absolute; top: 0px; right: 0px;" src="<?php echo !empty($item['image_link']) ? (Yii::app()->params['account_level_image_dir'] . '/' . $item['image_link']) : ''; ?>" height="20" /></a>
<!--                        </h3>-->
                        <span  class="subscriber-count">
                                <strong >Bình luận: <?php echo number_format($item['comment'],0,',','.'); ?></strong>

                        </span
                    </div>
                </div>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <?php endif; ?>
<!--        <ul class="" style="margin-top: 10px;">-->
<!---->
<!--                <li>-->
<!--                    <a target="_blank" title="Cấp độ: --><?php //echo $item['title']; ?><!--" data-placement="bottom" rel="tooltip" data-original-title="Cấp độ: --><?php //echo $item['title']; ?><!--" href="http://id.let.vn/account/level/index.let" style="padding: 0px;margin: 0px;height: 16px;width:16px;"><img style="vertical-align: middle;height: 15px; margin: 3px 2px;" src="--><?php //echo Yii::app()->params['account_level_image_dir'] . '/' . $item['image_link'] ?><!--" height="20" /></a>-->
<!--                    <a href="javascript:;" rel="nofollow">--><?php //echo $item['username']; ?><!--</a>-->
<!--                    --><?php //if($item['comment']): ?>
<!--                    <span style="font-size: 11px; font-style: italic;">Bình luận: --><?php //echo KitAccountStats::getField($item['id'],'comment'); ?><!--</span>-->
<!--                    --><?php //endif; ?>
<!--                </li>-->
<!---->
<!--        </ul>-->

<?php if($this->tabs !== array()): ?>
</div>
<?php endif; ?>

