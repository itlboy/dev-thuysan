<?php
if (Common::get_user_browser() == 'ie')
    $type = '';
else
    $type = 'inline';
?>

<?php
$model = new KitAccount();
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'horizontalForm',
        'type' => $type,
        'htmlOptions' => array('class' => 'well'),
    )
);
?>
    <h1 class="title-register" style="border-bottom: 1px dashed #999;" >Đăng ký</h1>
    <?php $style = array('style' => 'width:266px;height:20px;margin-top:6px;');?>
    <?php echo $form->textFieldRow($model, 'fullname',$style); ?>
    <?php echo $form->textFieldRow($model,'username',$style); ?>
    <?php echo $form->passwordFieldRow($model,'password',$style); ?>
<?php echo $form->textFieldRow($model, 'email',$style); ?>
    <div class="ct-birthday">
        <select name="KitAccount[date]" id="birthday-date" class="sl-birthday">
        </select>
        <select name="KitAccount[month]" onchange="return dateChange('birthday-date','birthday-month','birthday-year')" id="birthday-month" class="sl-birthday">
        </select>
        <select name="KitAccount[year]" onchange="return dateChange('birthday-date','birthday-month','birthday-year')" id="birthday-year" class="sl-birthday">
        </select>
    </div>

    <div class="ct-btn btn-submit" >
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'size'=>'small',  'icon'=>'ok white', 'label'=>'Đăng ký với Let')); ?>
    </div>
    <div id="process" style="margin-top: 10px;">
        <div class="alert in alert-block fade alert-error" style="display: none">
            <div class="messenger"></div>
        </div>
        <div class="alert in alert-block fade alert-success" style="display: none">
            <div class="messenger"></div>
        </div>
        <span class="process-loadding"></span>
    </div>
<?php $this->endWidget();echo "\n"; ?>
<script type="text/javascript">
    $('.alert .close2').live('click',function(){
        $(this).parent().hide();
        return false;
    });
    $('.btn-submit .btn').live('click',function(){
        var fullname = $('#KitAccount_fullname').val();
        var username = $('#KitAccount_username').val();
        var pass = $('#KitAccount_password').val();
        var email = $('#KitAccount_email').val();
        if(fullname == '' || username == '' || pass == '' || email == ''){
            alert('Bạn chưa nhập đủ thông tin');
            return false;
        }

        $.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl('account/ajax/AjaxSignUp'); ?>',
            data:$('#horizontalForm').serialize(),
            beforeSend:function(){
                $('#process .process-loadding').html('<center style="margin-top: 10px;"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif" /></center>');
            },
            success:function(data){
                eval('data ='+data);
                if(data.result == 'true'){
                    //window.location.href = '<?php echo Yii::app()->createUrl('account/profile'); ?>'
                    $('.sl-birthday option[value=""]').attr('selected','selected');
                    $('#KitAccount_fullname,#KitAccount_username,#KitAccount_password,#KitAccount_email').val('');
                    $('#process .alert-success .messenger').html(data.mess);
                    $('#process .alert-success').show();
                    $('#process .alert-error').hide();
                } else {
                    $('#process .alert-error .messenger').html(data.mess);
                    $('#process .alert-error').show();
                }
                $('#process .process-loadding').html('');
            }
        });
        return false;
    });

    var date = new Date();
    var year = date.getFullYear() - 10;

    listDateOption(1,31,'Ngày','birthday-date');
    listDateOption(1,12,'Tháng','birthday-month');
    listDateOption(1950,year,'Năm','birthday-year');

    /**
     *
     * @param to  - từ
     * @param from - đến
     * @param title -
     * @param TagId
     */
    function listDateOption(to,from,title,TagId,itemActive){
        var str = '<option selected="selected" value="0">'+title+'</option>';
        for(var i = to; i <= from; i++){
            var selected = '';
            if(itemActive != undefined){
                if(i == itemActive){
                    selected = 'selected="selected"';
                }
            }
            if(i < 10){
                i = '0'+ i;
            }
            str += '<option value="'+i+'" '+selected+'>'+i+'</option>';
        }
        $('#'+TagId).append(str);
    }

</script>