<?php

class TopMember extends Widget{
    public $limit = 10;
    public $view = '';
    public $title = '';
    public $type = 'level'; // level: danh sach cap do dunng dau. new : danh sach thanh vien moi
    public $tabs = array();
    public function init(){
        if($this->view ==''){
            $this->view = __CLASS__;
            Yii::import('application.modules.account.models.KitAccount');
        }
    }

    public function run() {
        $cache_name = md5(__METHOD__ .'_'.$this->type.'_' . $this->limit);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        $timeCache = 1;
        if ($cache === FALSE) {
            $order = 'u.id DESC';
            if(!empty($this->type)){
                switch($this->type){
                    case 'level':
                        $order = 'l.sorder DESC,s.comment DESC';
                        $timeCache = (60*60*12);
                        break;
                    case 'new':
                        $order = 'u.id DESC';
                        $timeCache = (60*60);
                        break;
                }
            }
            $result = Yii::app()->db->createCommand()
                ->select('u.id, u.username, u.avatar, u.level, s.comment, l.title, l.image_link')
                ->from('let_kit_account u')
                ->leftJoin('let_kit_account_stats s', 'u.id=s.user_id')
                ->leftJoin('let_kit_account_level l', 'u.level=l.id')
                ->limit($this->limit)
                ->order($order)
                ->queryAll();
            Yii::app()->cache->set($cache_name, $result, $timeCache); // Set cache
        } else $result = $cache;
        $this->render($this->view,array(
            'data' => $result,
        ));
    }
}