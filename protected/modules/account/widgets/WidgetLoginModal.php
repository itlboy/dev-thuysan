<?php

class WidgetLoginModal extends Widget {
    
    public $view = '';

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
    }

    public function run() {

        if (Yii::app()->user->isGuest) {
            $this->render($this->view . '_not');
        } else {
            $this->render($this->view . '_ok');
        }
    }

}
