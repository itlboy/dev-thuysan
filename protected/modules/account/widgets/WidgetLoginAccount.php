<?php

class WidgetLoginAccount extends Widget {

    public function run() {

        if (Yii::app()->user->isGuest) {
            $this->render('loginAccountNot');
        } else {
            // Kiem tra id user nay co trong data hay ko
            $model = KitAccount::model()->findByPk(Yii::app()->user->id);
            if(!empty($model)){
                $this->render('loginAccountOk');
            } else {
                $this->render('loginAccountNot');
            }
        }
    }

}
