<?php

class WidgetLoginForm extends Widget {

    public $params = array();

    public function init(){
        Yii::import('application.modules.account.models.KitAccountStats');
        Yii::import('application.modules.account.models.KitAccountLevel');
    }
    public function run() {                
        $model = new KitAccount();
        
        if (!Yii::app()->user->isGuest) {
            $data = KitAccount::getDetails(Yii::app()->user->id);
            if(empty($data))
                return FALSE;
            $this->render('userInfo');
        } else if (!empty($this->params)) {            
            $model->attributes = $this->params;           
            
            $modelLogin = new LoginFormMembers();
            $modelLogin->attributes = $this->params;
            $modelLogin->rememberMe = (isset($_POST['KitAccount']['checkbox']) AND $_POST['KitAccount']['checkbox'] == 1) ? true : false;
            if ($modelLogin->validate() && $modelLogin->login()) {
                // Update Level , Update Stats
                KitAccountStats::updateStats(Yii::app()->user->id);
                KitAccountLevel::updateLevel(Yii::app()->user->id);
                Yii::app()->cache->delete('config_unlimit_'.Yii::app()->user->id);
                echo json_encode(array(
                    'status' => 'success',
                    'message' => '',
//                    'contentForm' => $this->render('userPanel', array(), true),
                    'contentModal' => $this->render('WidgetLoginModal_ok', array(), true),
                    'contentForm' => $this->render('userInfo', array(), true),
                ));
            } else {
                echo json_encode(array(
                    'status' => 'error',
                    'message' => 'Sai tên truy cập hoặc mật khẩu',
                    'contentForm' => $this->render('userLogin', array('model' => $model), true)
                ));
            }
            Yii::app()->end();
        } else {
            $this->render('userLogin', array('model' => $model));
        }
    }

}
