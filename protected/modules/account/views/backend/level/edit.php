<?php
$moduleName = 'account';
$modelName = 'KitAccountLevel';
$formID = 'kit_'.$modelName.'_form_box';
$gridID = 'kit_'.$modelName.'_grid_box';

$form = $this->beginWidget('CActiveForm', array(
    'id' => $formID,
    'enableAjaxValidation'=>false,
    'htmlOptions' => array(
        'onSubmit' => 'js:ajaxSaveForm("'.$modelName.'" ,jQuery(this).attr("id"), jQuery(this).attr("action")); return false',
    )
)); ?>
<script type="text/javascript">
    var content = $('.level_list').html();
    function copyHtml(){
//            content = content.replace(/{{i}}/g,count);
//            ++count;
            $('.level_list').append(content);
    }
</script>
<div id="" class="dialog_content">
    <div class="block" style="opacity: 1;">
        <div class="section">
            <h2>Thông tin danh mục</h2>
        </div>

        <div class="columns clearfix level_list" style="width: 900px;" id="container_level">
            <div class="clearfix">
                <div class="col_40">
                    <fieldset>
                        <label>Tên cấp độ</label>
                        <div>
                            <?php echo CHtml::textField('KitAccountLevel[title][]', $model->title, array('class' => 'tooltip autogrow', 'style' => 'padding:5px 2px; border:1px solid #ccc;width:100%','title' => 'Tên cấp độ', 'placeholder' => 'Tên cấp độ')); ?>
                        </div>
                    </fieldset>
                </div>
                <div class="col_20">
                    <fieldset>
                        <label>Link ảnh</label>
                        <div>
                            <?php echo CHtml::textField('KitAccountLevel[image_link][]',$model->image_link, array('class' => 'tooltip autogrow', 'style' => 'padding:5px 2px; border:1px solid #ccc;width:100%','title' => 'Link ảnh', 'placeholder' => 'Link ảnh')); ?>
                        </div>
                    </fieldset>
                </div>
                <div class="col_20">
                    <fieldset>
                        <label>Thứ tự</label>
                        <div>
                            <?php echo CHtml::textField('KitAccountLevel[sorder][]', $model->sorder, array('class' => 'tooltip autogrow', 'style' => 'padding:5px 2px; border:1px solid #ccc;width:100%','title' => 'Số càng nhỏ thì thứ tự càng cao. Mặc định: 500', 'placeholder' => 'Nhập 1 số')); ?>
                        </div>
                    </fieldset>
                </div>
                <div class="col_20">
                    <fieldset>
                        <label>Trạng thái</label>
                        <div>
                            <div class="">
                                <select name="KitAccountLevel[status][]" style="border: 1px solid #ccc;padding: 2px;">
                                    <option selected="selected">Status</option>
                                    <option value="1" <?php echo (!empty($model->status) AND (int)$model->status == 1 ) ? 'selected="selected"' : ''; ?>>Có</option>
                                    <option value="0" <?php echo (!empty($model->status) AND (int)$model->status == 0 ) ? 'selected="selected"' : ''; ?>>Không</option>
                                </select>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>

        </div>
        <div class="detail-level clearfix">
            <a href="javascript:;" onclick="return copyHtml();" style="margin-left: 20px; margin-bottom: 5px">Add <span>Level</span></a>
        </div>
        <div class="button_bar clearfix">
            <button class="dark green close_dialog">
                <div class="ui-icon ui-icon-check"></div>
                <span>Submit</span>
            </button>
        </div>
    </div>
</div>

<?php $this->endWidget(); ?>
<!--
<div id="attributes_stats" style="display:none;">
    <div style="width: 100%;">
        <div class="col_40">
            <fieldset>
                <label>Chọn Field</label>
                <div>
                    <div class="">
                        <?php
                        $attributes = KitAccountStats::model()->attributeNames();
                        if(!empty($attributes)): ?>
                            <select name="KitAccountLevel[config][{{i}}][field]" style="border: 1px solid #ccc;padding: 2px;">
                            <option selected="selected">Chọn Field</option>
                            <?php foreach($attributes as $index => $attribute ): ?>
                                <?php if($attribute != 'user_id'): ?>
                                    <option value="<?php echo $index; ?>"><?php echo $attribute; ?></option>
                                <?php endif ?>
                            <?php endforeach; ?>
                            </select>
                        <?php endif; ?>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col_40">
            <fieldset>
                <label>So sánh</label>
                <div>
                    <div class="">
                        <select name="KitAccountLevel[config][{{i}}][comparing]" style="border: 1px solid #ccc;padding: 2px;">
                            <option selected="selected">Chọn phép tính</option>
                            <option value="="> = </option>
                            <option value="-"> + </option>
                            <option value="<"> < </option>
                            <option value=">"> > </option>
                        </select>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col_20">
            <fieldset>
                <label>Điều kiện</label>
                <div>
                    <div class="">
                        <input name="KitAccountLevel[config][{{i}}][conditions]" style="padding:5px 2px; border:1px solid #ccc;width:100%">
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="clearfix"></div>
    </div>
</div> -->
