<?php
$moduleName = 'Account';
$modelName = 'KitAccountLevel';
$formID = 'kit_'.$modelName.'_form';
$gridID = 'kit_'.$modelName.'_grid';
?>

<?php $attributes = KitAccountStats::model()->attributeNames(); ?>
<div class="grid_16">
    <button class="green small" onclick="js:jQuery('#apply').val(0); document.forms['<?php echo $formID; ?>'].submit();">
        <div class="ui-icon ui-icon-check"></div>
        <span><?php echo Yii::t('global', 'Save'); ?></span>
    </button>
    <button class="blue small" onclick="return sorder();">
        <div class="ui-icon ui-icon-check"></div>
        <span>Sắp xếp</span>
    </button>
    <button class="blue small" onclick="return ajaxDialogForm('', 'Thêm Cấp bậc', '/let/letkit/letadmin@.php/account/level/create.html');;">
        <div class="ui-icon ui-icon-check"></div>
        <span>Thêm cấp bậc</span>
    </button>
</div>
<div class="box grid_16" style="opacity: 1;">

    <h2 class="box_head">Form Elements</h2>
    <a class="grabber" href="#">&nbsp;</a>
    <a class="toggle" href="#">&nbsp;</a>

    <?php
    $form=$this->beginWidget('CActiveForm', array(
        'id' => $formID,
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype'=>'multipart/form-data'),
    ));
    ?>
<script type="text/javascript">

    function copyHtmlConfig(obj){
        var id = $(obj).data('id');
        var content = $('#attributes_stats').html();
        content = content.replace(/{{i}}/g,id);
        $('.level-config-'+id).append(content);
    }

    function deleteLevel(id,obj){
        if(confirm('Bạn muốn xóa level này ?')){
            $.ajax({
                type:'POST',
                url:'<?php echo Yii::app()->createUrl('account/level/delete'); ?>',
                data:{id:id},
                success:function(data){
                    if(data == 'true'){
                        $(obj).parent().parent().parent().parent().parent().parent().remove();
                    }
                }
            });
        }
    }
    function sorder(){
        var i = 1;
        $('.txtsorder').each(function(){
            $(this).val(i+'0');
            ++i;
        });
    }
</script>
    <div class="toggle_container">
        <?php if(Yii::app()->user->hasFlash('error')): ?>
        <div class="alert dismissible alert_red" style="margin: 10px;">
            <img width="24" height="24" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/icons/small/white/alarm_bell.png">
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
        <?php endif; ?>
        <div class="block" style="opacity: 1;">
            <div class="columns clearfix"  id="container_level">

            <?php if(!empty($data)): ?>
            <?php foreach($data as $key => $value): ?>
                <div class="">
                    <p style="height: 10px;background-color: #0085BF;width: 100%"></p>
                    <input type="hidden" name="KitAccountLevel[id][<?php echo $key; ?>]" value="<?php echo $value['id']; ?>">
                    <div class="clearfix">
                        <div class="col_20">
                            <fieldset>
                                <label>Tên cấp độ</label>
                                <div>
                                    <input type="text" name="KitAccountLevel[title][<?php echo $key; ?>]" value="<?php echo $value['title']; ?>" class="tooltip autogrow" title="Tên cấp độ" placeholder="Tên cấp độ" />
                                </div>
                            </fieldset>
                        </div>
                        <div class="col_20">
                            <fieldset>
                                <label>Link ảnh</label>
                                <div>
                                    <input type="text" name="KitAccountLevel[image_link][<?php echo $key; ?>]" value="<?php  echo $value['image_link']; ?>" class="tooltip autogrow" title="Link ảnh" placeholder="Link ảnh" />
                                </div>
                            </fieldset>
                        </div>
                        <div class="col_20">
                            <fieldset>
                                <label>Thứ tự</label>
                                <div>
                                    <input type="text" name="KitAccountLevel[sorder][<?php echo $key ?>]" value="<?php echo $value['sorder']; ?>" class="tooltip autogrow txtsorder" title="Số càng nhỏ thì thứ tự càng cao. Mặc định: 500" placeholder="Nhập 1 số" />
                                </div>
                            </fieldset>
                        </div>
                        <div class="col_20">
                            <fieldset>
                                <label>Trạng thái</label>
                                <div>
                                    <div class="">
                                        <select name="KitAccountLevel[status][<?php echo $key; ?>]" style="border: 1px solid #ccc;padding: 2px;">
                                            <option selected="selected"  value="0">Status</option>
                                            <option value="1" <?php echo (!empty($value['status']) AND (int)$value['status'] == 1 ) ? 'selected="selected"' : ''; ?>>Có</option>
                                            <option value="0" <?php echo (!empty($value['status']) AND (int)$value['status'] == 0 ) ? 'selected="selected"' : ''; ?>>Không</option>
                                        </select>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col_20">
                            <fieldset style="height: 89px;">
                                <label></label>
                                <div>
                                    <div class="">
                                        <a onclick="return copyHtmlConfig(this);" href="javascript:;" data-id="<?php echo $key; ?>"><b>Add Config</b></a>
                                        <a href="javascript:;" onclick="return deleteLevel(<?php echo $value['id']; ?>,this);" style="float: right;color: red;"><b>Xóa Level</b></a>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="level-config level-config-<?php echo $key; ?> columns">
                        <?php if(!empty($value['config'])): ?>
                        <?php $value['config'] = json_decode($value['config'],true); ?>
                        <?php foreach($value['config'] as $item): ; ?>
                        <fieldset class="label_side">
                            <div style="width: 100%; margin: 0px;padding: 0px;border: 0;">
                                <div class="remove_tag tooltip hover left" title="Xóa Config này" onclick="if(confirm('Bạn muốn xóa ? ')){$(this).parent().parent('fieldset').remove();}" style="z-index: 99999" ></div>
                                <div class="col_40">
                                    <fieldset>
                                        <label>Chọn Field</label>
                                        <div>
                                            <div class="">
                                                <?php
                                                if(!empty($attributes)): ?>
                                                    <select name="KitAccountLevel[config][<?php echo $key; ?>][field][]" style="border: 1px solid #ccc;padding: 2px;">
                                                        <?php foreach($attributes as $index => $attribute ): ?>
                                                        <?php if($attribute != 'user_id'): ?>
                                                            <option value="<?php echo $attribute; ?>" <?php echo ($attribute == $item['field']) ? 'selected="selected"' : ''; ?>><?php echo $attribute; ?></option>
                                                            <?php endif ?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <?php endif; ?>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>

                                <div class="col_40">
                                    <fieldset>
                                        <label>So sánh</label>
                                        <div>
                                            <div class="">
                                                <select name="KitAccountLevel[config][<?php echo $key; ?>][comparing][]" style="border: 1px solid #ccc;padding: 2px;">
                                                    <option value=">=" <?php echo ($item['comparing'] == '>=') ? 'selected="selected"' : ''; ?> > >= </option>
                                                    <option value=">" <?php echo ($item['comparing'] == '>') ? 'selected="selected"' : ''; ?> > > </option>
                                                    <option value="<=" <?php echo ($item['comparing'] == '<=') ? 'selected="selected"' : ''; ?> > <= </option>
                                                    <option value="<" <?php echo ($item['comparing'] == '<') ? 'selected="selected"' : ''; ?> > < </option>
                                                    <option value="==" <?php echo ($item['comparing'] == '==') ? 'selected="selected"' : ''; ?> > = </option>
                                                </select>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col_20">
                                    <fieldset style="height: 89px;">
                                        <label>Điều kiện</label>
                                        <div>
                                            <div class="">
                                                <input name="KitAccountLevel[config][<?php echo $key; ?>][conditions][]" value="<?php echo $item['conditions']; ?>" style="padding:5px 2px; border:1px solid #ccc;width:100%">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </fieldset>
                        <?php endforeach; ?>
                        <?php endif; ?>

                    </div>
                </div>
            <?php endforeach; ?>
            <?php endif; ?>
            </div>

        </div>
    </div>
<?php $this->endWidget(); ?>


<div id="attributes_stats" style="display:none;">
    <fieldset class="label_side">
        <div style="width: 100%; margin: 0px;padding: 0px;border: 0;">
            <div class="col_40">
                <fieldset>
                    <label>Chọn Field</label>
                    <div>
                        <div class="">
                            <?php
                            if(!empty($attributes)): ?>
                                <select name="KitAccountLevel[config][{{i}}][field][]" style="border: 1px solid #ccc;padding: 2px;">
                                <?php foreach($attributes as $index => $attribute ): ?>
                                    <?php if($attribute != 'user_id'): ?>
                                        <option value="<?php echo $attribute; ?>"><?php echo $attribute; ?></option>
                                    <?php endif ?>
                                <?php endforeach; ?>
                                </select>
                            <?php endif; ?>
                        </div>
                    </div>
                </fieldset>
            </div>

            <div class="col_40">
                <fieldset>
                    <label>So sánh</label>
                    <div>
                        <div class="">
                            <select name="KitAccountLevel[config][{{i}}][comparing][]" style="border: 1px solid #ccc;padding: 2px;">
                                <option value=">="> >= </option>
                                <option value=">"> > </option>
                                <option value="<="> <= </option>
                                <option value="<"> < </option>
                                <option value="=="> = </option>

                            </select>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="col_20">
                <fieldset style="height: 89px;">
                    <label>Điều kiện</label>
                    <div>
                        <div class="">
                            <input name="KitAccountLevel[config][{{i}}][conditions][]" style="padding:5px 2px; border:1px solid #ccc;width:100%">
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="remove_tag tooltip hover left" title="Xóa Config này" onclick="if(confirm('Bạn muốn xóa ? ')){$(this).parent().parent('fieldset').remove();}" style="z-index: 99999"></div>
            <div class="clearfix"></div>
        </div>
    </fieldset>
</div>