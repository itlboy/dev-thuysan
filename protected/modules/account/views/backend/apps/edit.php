<?php
$moduleName = 'Account';
$modelName = 'KitAccountApplist';
$formID = 'kit_'.$moduleName.'_form';
$gridID = 'kit_'.$moduleName.'_grid';
?>
<?php
if(isset($_GET['id'])){
    $models = array();
    $configs = !empty($model->config) ? $model->config : NULL;
    $configs = json_decode($configs,true);
    if(!empty($configs)){
        foreach($configs as $config){
            if(isset($config['field'])){
                foreach($config['field'] as $fields){
                    $models[$config['table']][$fields['field_account']] = $fields['field_target'];
                }
            }
        }
    }
}

?>


<script type="text/javascript">
    var countTable = <?php echo (!empty($models)) ? count($models) : 0; ?>;
    var countField = 0;
    tableList()

    function copyHtml(TagSource,TagTarget,name,idTag,dataValue){


        var sourceData = {
            "table":"",
            "field_account":"",
            "field_target":""
        };

        if(dataValue !== undefined){
            sourceData['table'] = dataValue;
        }

        if(name == 'table'){
            sourceData['i'] = countTable;
            ++countTable;
        } else if( name == 'field'){
            sourceData['tbl'] = idTag;
            $count = $('#field_'+idTag+' .add-field').size();
            sourceData['i'] = $count;
        }

        data = sourceData;
        var listTable = $('#table-list').html();
        var content = $('#'+TagSource).html();
        content = content.replace(/{{tblList}}/g,listTable);
        content = content.replace(/{{i}}/g, data.i);
        content = content.replace(/{{table}}/g, data.table);
        content = content.replace(/{{field_account}}/g, data.field_account);
        content = content.replace(/{{field_target}}/g, data.field_target);
        content = content.replace(/{{tbl}}/g, data.tbl);

        $('#'+TagTarget).append(content);

    }

    function tableList(){
        $(document).ready(function(){


            $('.table-list').each(function(){
                var listTable = $('#table-list').html();
                var id = $(this).data('id');
                listTable = listTable.replace(/{{i}}/g, id);
                $(this).html(listTable);
            });
        })
    }

    function tableListChange(obj){
        $(document).ready(function(){
            var id = $(obj).data('id');
            $('#table_'+id).val($(obj).val());
        });
    }
</script>
<div class="grid_16">
<!-- Title -->
<div class="flat_area grid_16" style="opacity: 1;">
    <h2>Quản lý Config</h2>

</div>
<!-- END Title -->
<div class="grid_16">
    <button class="green small" onclick="js:jQuery('#apply').val(0); document.forms['<?php echo $formID; ?>'].submit();">
        <div class="ui-icon ui-icon-check"></div>
        <span><?php echo Yii::t('global', 'Save'); ?></span>
    </button>
    <button class="green small" onclick="js:jQuery('#apply').val(1); document.forms['<?php echo $formID; ?>'].submit();">
        <div class="ui-icon ui-icon-check"></div>
        <span><?php echo Yii::t('global', 'Apply'); ?></span>
    </button>
</div>
<div class="box grid_16" style="opacity: 1;">
<h2 class="box_head">Form Elements</h2>
<a class="grabber" href="#">&nbsp;</a>
<a class="toggle" href="#">&nbsp;</a>
<?php
$form=$this->beginWidget('CActiveForm', array(
    'id' => $formID,
    'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype'=>'multipart/form-data'),
));
?>
<input id="apply" type="hidden" name="apply" value="0">
<div class="toggle_container">
    <div class="block" style="opacity: 1;">
        <!--            <h2 class="section">Search Film</h2>-->

        <fieldset class="label_side">
            <label>Ứng dụng:</label>
            <div>
                <?php echo $form->textField($model,'title',array(
//                        'onmousedown' => "getSelText()",
            )); ?>
                <?php echo $form->error($model,'title'); ?>
            </div>
        </fieldset>


        <div class="button_bar clearfix">
            <button type="button" data-name='table' class="blue small div_icon has_text" onclick="return copyHtml('add-table','table_all','table','{{i}}');">
                <div class="ui-icon ui-icon-plusthick"></div>
                <span>Add Table</span>
            </button>
            <div style="float: right;" class="result_ld"></div>
        </div>
    </div>
    <style>
        .element_regex{
            color: #666;
            cursor: pointer;
            padding: 2px;
        }
        .element_regex span{
            color:blue;
        }
        .col_20,.col_50 {
            border: 0px;
        }
        .col_20 fieldset,.col_50 fieldset{
            border: 0px !important;
        }
        .col_20 div.result_ld{
            padding:0px;
            border: 1px solid #e5e5e5;
            border-radius: 2px;
            height: 30px;
            line-height: 30px;
            background: none repeat scroll 0 0 rgba(0, 0, 0, 0.05);
            cursor: pointer;
            padding-left: 7px;
        }
        .col_20 div.result_ld img{margin-top: 3px;}
        .add-field,#table_all,.item_table{
            position: relative;
        }
        #table_all .remove_tag{
            background: url("<?php echo Yii::app()->theme->baseUrl; ?>/images/input_remove.png") repeat scroll right top transparent;
            cursor: pointer;
            display: block;
            height: 40px;
            position: absolute;
            right: 0;
            top: 0;
            width: 40px;
            z-index: 9999;
        }
    </style>
    <div id="table_all">

        <?php if(!empty($models)):
        $i = 0;
        foreach($models as $key => $fields):

            ?>
            <div title="" class="item_table">
                <div title="Xóa" onclick="if(confirm('Bạn muốn xóa ? ')){$(this).parent().remove();}return false;" class="remove_tag hover left"></div>
                <fieldset class="label_side">
                    <label>Table</label>
                    <div>
                        <div class="table-list" data-id="<?php echo $i; ?>">{{tblList}}</div>
                        <input type="text" id="table_<?php echo $i; ?>" value="<?php echo $key; ?>" class="text" name="config[<?php echo $i; ?>][table]" />

                    </div>
                </fieldset>
                <fieldset class="label_side">
                    <label>Field:
                        <button data-name='field' data-table="<?php echo $i; ?>" onclick="return copyHtml('add_field','field_<?php echo $i; ?>','field','<?php echo $i; ?>');" style="margin-top:10px;" type="button" class="orange small div_icon has_text">
                            <div class="ui-icon ui-icon-plusthick"></div>
                            <span style="color: white">Add Field</span>
                        </button>
                    </label>
                    <div class="columns clearfix" id="field_<?php echo $i; ?>">
                        <?php if(isset($fields) AND !empty($fields)):
                        $j = 0;
                        ?>
                        <?php foreach($fields as $field_account => $field_target): ?>
                        <div style="border-bottom: 1px solid #ccc;" class="add-field">
                            <div class="col_50" style="border: 0px">
                                <fieldset>
                                    <label>Field Account</label>
                                    <div>
                                        <input type="text" value="<?php echo $field_account; ?>" class="text" name="config[<?php echo $i ?>][field][<?php echo $j; ?>][field_account]" />
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col_50" style="border: 0px">
                                <fieldset>
                                    <label>Field Target</label>
                                    <div>
                                        <input type="text" value="<?php echo $field_target ?>" class="text" name="config[<?php echo $i ?>][field][<?php echo $j; ?>][field_target]" />
                                    </div>
                                </fieldset>
                            </div>
                            <div class="clearfix"></div>
                            <div title="Xóa" onclick="if(confirm('Bạn muốn xóa ? ')){$(this).parent().remove();}return false;" class="remove_tag hover left"></div>
                        </div>
                        <?php $j++; ?>
                        <?php endforeach; ?>
                        <?php endif; ?>

                    </div>
                </fieldset>
            </div>
            <?php
            $i++;
        endforeach; ?>

        <?php endif; ?>

    </div>
</div>
<?php $this->endWidget(); ?>
<div id="table-list" style="display: none;">
    <select class="table-item" style="border: 1px solid #ccc;padding: 2px;" data-id="{{i}}" onchange="return tableListChange(this);">
        <option selected="selected">Chọn table</option>
        <?php if(!empty($tables)): ?>
        <?php foreach($tables as $table): ?>
            <option value="<?php echo $table; ?>"><?php echo $table; ?></option>
            <?php endforeach; ?>
        <?php endif; ?>
    </select>
</div>
<div id="add-table" class="add-table block" style="display: none;">
    <div title="" class="item_table">
        <div title="Xóa" onclick="if(confirm('Bạn muốn xóa ? ')){$(this).parent().remove();}return false;" class="remove_tag hover left"></div>
        <fieldset class="label_side">
            <label>Table</label>
            <div>
                <div class="table-list" data-id="{{i}}">{{tblList}}</div>
                <input type="text" id="table_{{i}}" value="{{table}}" class="text" name="config[{{i}}][table]" />
            </div>
        </fieldset>
        <fieldset class="label_side">
            <label>Field:
                <button data-name='field' data-table="{{i}}" onclick="return copyHtml('add_field','field_{{i}}','field','{{i}}');" style="margin-top:10px;" type="button" class="orange small div_icon has_text">
                    <div class="ui-icon ui-icon-plusthick"></div>
                    <span style="color: white">Add Field</span>
                </button>
            </label>
            <div class="columns clearfix" id="field_{{i}}">


            </div>
        </fieldset>
    </div>
</div>
<div style="display: none" id="add_field">
    <div style="border-bottom: 1px solid #ccc;" class="add-field">
        <div class="col_50" style="border: 0px">
            <fieldset>
                <label>Field Account</label>
                <div>
                    <input type="text" value="{{field_account}}" class="text" name="config[{{tbl}}][field][{{i}}][field_account]" />
                </div>
            </fieldset>
        </div>
        <div class="col_50" style="border: 0px">
            <fieldset>
                <label>Field Target</label>
                <div>
                    <input type="text" value="{{field_target}}" class="text" name="config[{{tbl}}][field][{{i}}][field_target]" />
                </div>
            </fieldset>
        </div>
        <div class="clearfix"></div>
        <div title="Xóa" onclick="if(confirm('Bạn muốn xóa ? ')){$(this).parent().remove();}return false;" class="remove_tag hover left"></div>
    </div>
</div>