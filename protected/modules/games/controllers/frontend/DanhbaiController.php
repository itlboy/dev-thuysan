<?php
/**
 * Module: games
 * Auth:
 * Date: 2012-11-14 09:54:56
 */
class DanhbaiController extends ControllerFrontend
{
	public function actionIndex()
	{
        if(Yii::app()->user->isGuest){
            $this->render('login');
        } else {
            //Secret code của khách hàng.
            $secret = '3891907fe55cafc234a25281990c77a6';
            $username = Yii::app()->user->name;//'Truyền tên đăng nhập của user trong session vào biến này nhé';
            $avatar = KitAccount::getField(Yii::app()->user->id,'avatar');
            $avatar = (!empty($avatar)) ? Common::getImageUploaded('account/large/'.$avatar) : 'http://forum.let.vn/uc_server/avatar.php?uid="'.Yii::app()->user->id.'"&size=big';;
            $email = '';//'Email của thành viên đang đăng nhập!';		
            $fields_string = '';
            $fields = array(
                    'username'=>urlencode($username),
                    'email'=>urlencode(''),
                    'avatar'=>urlencode($avatar),
                    'secret'=>$secret
                );
                foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                rtrim($fields_string,'&');


                $ch = curl_init(); //khai báo curl				
                curl_setopt($ch, CURLOPT_URL,"http://www.duduto.vn/api/clientlogin/");
                curl_setopt($ch,CURLOPT_POST,count($fields));
                curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $game = curl_exec ($ch); 
            curl_close ($ch); 

            $this->render('index', array(
                'game' => $game,
            ));
        }
	}
}