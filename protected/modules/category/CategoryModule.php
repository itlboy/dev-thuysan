<?php

class CategoryModule extends BaseModule
{    
    public $menuList = array(
        array(
            'name' => 'Tạo danh mục',
            'url' => '/category/default/create',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'type' => 'dialog',
            'grid' => 'kit_category_grid',
        ),
        array(
            'name' => 'Quản lý danh mục',
            'url' => '/category/default/index',
            'icon' => '/images/icons/small/grey/coverflow.png',
            'desc' => 'Quản lý danh mục bài viết',
            'type' => 'direct'
        ),
    );

	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'category.models.*',
			'category.components.*',
		));
	}
}
