<?php

class treeCategory extends Widget
{
    public $module = '';
    public $promotion = NULL;

    public function init()
    {
        Yii::import('category.models.KitCategory');
    }
 
    public function run()
    {
        $tree = KitCategory::getTree($this->module);
        $data = array(array(
            'text' => '<a href="'.Yii::app()->createUrl($this->module).'">'.Yii::t('backend', $this->module).'</a>',
            'children' => $tree,
        ));
        $this->render('treeCategory', array(
            'dataTree' => $data,
        ));
    }
}