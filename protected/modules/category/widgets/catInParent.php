<?php

class catInParent extends Widget
{
    public $module = '';
    public $parent_id = '';

    public function init()
    {
        Yii::import('category.models.KitCategory');
    }
 
    public function run()
    {
        $data = KitCategory::getListInParent($this->module, $this->parent_id);
        $this->render('catInParent', array(
            'data' => $data,
        ));
    }
}