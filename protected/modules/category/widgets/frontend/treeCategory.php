<?php

class treeCategory extends Widget {

    public $title = '';
    public $view = '';
    public $module = 'article';
    public $promotion = NULL;
    public $category = 0;

    public function init() {
        if ($this->view == '')
            $this->view = __CLASS__;
        if ($this->title == '')
            $this->title = 'Thể loại';
        Yii::import('category.models.KitCategory');
    }

    public function run() {
        $cache_name = md5(__METHOD__ . '_' . $this->module . '_' . $this->category . '_' . $this->promotion);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        $cache = false;
        if ($cache === FALSE) {
            $active = KitCategory::getParentCategory($this->module, $this->category);

            if (!empty($active)) {
                $assign['category'] = end($active);
            } else {
                $assign['category'] = $this->category;
            }

            $assign['data'] = KitCategory::getList($this->module, $this->promotion, 'array');
            $assign['data'] = KitCategory::treatment($assign['data']);
            Yii::app()->cache->set($cache_name, $assign, Yii::app()->params['timeCache']); // Set cache
        } else
            $assign = $cache;
        $this->render($this->view, $assign);
    }

}
