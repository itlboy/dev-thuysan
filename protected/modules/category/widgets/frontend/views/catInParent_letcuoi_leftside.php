<?php
$id = intval(Yii::app()->request->getParam('id', NULL));
?>

<ul class="side_left_nav maggin_bottom_15">
<?php foreach ($data as $key => $item): ?>
    <?php
    $class = array();
    if ($item['id'] == $id) $class[] = 'active';
    if ($key == (count($data) - 1)) $class[] = 'last';
    $class = implode(' ', $class);
    ?>
    <li<?php if ($class !== ''): ?> class="<?php echo $class; ?>"<?php endif; ?>><a href="<?php echo $item['url']; ?>"><?php echo $item['name']; ?></a></li>
<?php endforeach; ?>
</ul>
