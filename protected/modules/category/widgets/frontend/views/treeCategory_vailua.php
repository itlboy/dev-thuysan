<?php
$count = count($data);
$numOfColumn = round($count / 2);
?>
		<li class="submenu">
		<a href="/categories/" class="nav">
		Danh mục <span></span></a>&nbsp;&middot;
		<ul id="CategoriesDropdown">
			<li>
            	<span class="SubmenuColumn">
        	<?php foreach ($data as $key => $row): ?>
                	<a href="<?php echo $row['url']; ?>"><?php echo $row['name']; ?></a>
            <?php if (($key % $numOfColumn) == 0 AND $key > 0) ?>
				</span>
            	<span class="SubmenuColumn">
        	<?php endforeach; ?>
				</span>
			</li>
		</ul>
		</li>