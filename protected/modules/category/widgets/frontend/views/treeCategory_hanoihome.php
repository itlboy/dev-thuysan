<?php foreach ($data as $key => $parent): ?>
    <?php if ($parent['parent_id'] == 0) : ?>
					<li class="parent">
						<a href="<?php echo $parent['url']; ?>"><?php echo $parent['name']; ?></a>
        <?php if (isset($data[0])) : ?>
						<ul>
            <?php foreach ($data as $children): ?>
                <?php if ($children['parent_id'] == $parent['id']) : ?>
							<li><a href="<?php echo $children['url']; ?>"><?php echo $children['name']; ?></a></li>
                <?php endif; ?>
            <?php endforeach; ?>
						</ul>
        <?php endif; ?>
					</li>
    <?php endif; ?>
<?php endforeach; ?>
