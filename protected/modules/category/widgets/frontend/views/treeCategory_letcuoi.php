<?php
$id = intval(Yii::app()->request->getParam('id', NULL));
if ($this->module == 'film')
    $searchLabel = 'tim-kiem-phim';
if ($this->module == 'pin')
    $searchLabel = 'tim-kiem-vui-cuoi';
else
    $searchLabel = 'tim-kiem';
?>

        <ul class="side_left_nav">
            <li class="last<?php if (empty($id)): ?> active<?php endif; ?>"><a href="<?php echo Yii::app()->createUrl($searchLabel); ?>">Tất cả thể loại</a></li>
        </ul>
        <ul class="side_left_nav maggin_bottom_15">
        <?php foreach ($data as $key => $item): ?>
			<?php
			$class = array();
			if ($item['id'] == $id) $class[] = 'active';
			if ($key == (count($data) - 1)) $class[] = 'last';
			$class = implode(' ', $class);
            ?>
            <li<?php if ($class !== ''): ?> class="<?php echo $class; ?>"<?php endif; ?>><a href="<?php echo $item['url']; ?>"><?php echo $item['name']; ?></a></li>
        <?php endforeach; ?>
        </ul>
