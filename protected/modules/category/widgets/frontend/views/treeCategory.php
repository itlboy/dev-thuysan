<div id="nav">
    <ul class="tabs">
        <li class="i-menu <?php echo (intval($category) == 0) ? 'current' : ''; ?> first"><a class="pro joc-cate-1" href="<?php echo Yii::app()->getBaseUrl(true); ?>">Trang chủ</a></li>
        <?php foreach ($data as $key => $parent): ?>
            <?php if ($parent['parent_id'] == 0) : ?>
                <li class="i-menu <?php echo (intval($category) == $parent['id']) ? 'current' : ''; ?> item-<?php echo $key; ?>" >
                    <a class="pro joc-cate-1" href="<?php echo $parent['url']; ?>"><?php echo $parent['name']; ?></a>
                    <ul class="joc-<?php echo $parent['id']; ?>">
                        <?php foreach ($data as $children): ?>
                            <?php if ($children['parent_id'] == $parent['id']) : ?>
                                <li><a style="<?php echo (intval($this->category) == $children['id']) ? 'color:#cd000f;;font-weight: bold;' : ''; ?>" href="<?php echo $children['url']; ?>"><?php echo $children['name']; ?></a></li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ul>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#nav ul.tabs li.i-menu').live('click',function(){
            $('#nav ul.tabs li.i-menu').removeClass('current');
            $(this).addClass('current');
        });
        //form-search

        $('#nav ul.tabs li.item-7,#nav ul.tabs li.item-6,#nav ul.tabs li.item-8').hover(function(){
            $('.form-search').hide();
        },function(){
            $('.form-search').show();
        });
        $('#nav ul.tabs li.current').hover(function(){
            $('.form-search').show();
        });
    });
</script>
