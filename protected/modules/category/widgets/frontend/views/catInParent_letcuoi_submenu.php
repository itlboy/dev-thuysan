<?php
$id = intval(Yii::app()->request->getParam('id', NULL));
?>
<ul>
<?php foreach ($data as $key => $item): ?>
    <?php
    $class = array();
    if ($item['id'] == $id OR $item['id'] == $this->active_id) $class[] = 'active';
    if ($key == (count($data) - 1)) $class[] = 'last';
    $class = implode(' ', $class);
    ?>
    <li<?php if ($class !== ''): ?> class="<?php echo $class; ?>"<?php endif; ?>><a href="<?php echo $item['url']; ?>"><?php echo $item['name']; ?></a></li>
<?php endforeach; ?>
</ul>
