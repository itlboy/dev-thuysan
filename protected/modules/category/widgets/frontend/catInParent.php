<?php

class catInParent extends Widget
{
    public $view = '';
    public $module = '';
    public $parent_id = '';
    public $active_id = '';

    public function init()
    {
        if ($this->view == '') $this->view = __CLASS__;
        Yii::import('category.models.KitCategory');
    }
 
    public function run()
    {
        $assign['data'] = KitCategory::getListInParent($this->module, $this->parent_id);
        $assign['data'] = KitCategory::treatment($assign['data']);
        $this->render($this->view, $assign);
    }
}