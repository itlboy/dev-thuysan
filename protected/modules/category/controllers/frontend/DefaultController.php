<?php

/**
 * Module: category
 * Auth:
 * Date: 2012-03-16 11:24:17
 */
class DefaultController extends ControllerFrontend {

    public function actionIndex() {
        $assign['category'] = letArray::get($_GET, 'id', NULL);
        $assign['details'] = KitCategory::getDetails($assign['category']);
        $assign['details'] = KitCategory::treatment($assign['details']);
        $view = $assign['details']['module'];

        // phân trang
        $page = '';
        if(isset($_GET['page'])){
            $page = letArray::get($_GET, 'page');
        }
        $limit = 10;
        $link = $assign['details']['alias'].'-c'.$assign['details']['id'];
        if(!empty($page)){
            $limit_wg = $limit.','.$page * $limit;
            $assign['old'] = $link.'-'.($page +1);
            if(($page - 1) != 0){
                $assign['new'] = $link.'-'.($page -1);
            } else {
                $assign['new'] = $link;
            }
            $link .='-'.$page;
        } else {
            $limit_wg = $limit;
            $assign['old'] = $link.'-'.($page +1);
        }

        $assign['limit_wg'] = $limit_wg;
        
        // Children
        $result = KitCategory::getListInParent('article',$assign['category']);
        $assign['category_child'] = KitCategory::treatment($result);
//        CVarDumper::dump($assign,10,true);
//        die();
        $this->render($view, $assign);
    }

}