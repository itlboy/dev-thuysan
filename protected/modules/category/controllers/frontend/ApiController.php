<?php
class ApiController extends ControllerFrontend
{

    /**
     * category/api/list/module/{$module}/parent_id/{$parent_id}/level/{$level}
     *
     */
    public function actionList() {
        $module = Yii::app()->request->getParam('module', 'article');
        $promotion = Yii::app()->request->getParam('promotion', 0);
        $parent_id = (int) Yii::app()->request->getParam('parent_id', NULL);

        // Get list
        $assign['data'] = KitCategory::getList($module, $promotion);
        $assign['data'] = KitCategory::treatment($assign['data']);

        //if (isset($assign['data']['id'])) {
        if (isset($assign['data'])) {
            $result['query'] = 'ok';
            $result['status'] = 'Tải dữ liệu thành công.';
        } else {
            $result['query'] = 'fail';
            $result['status'] = 'Tải dữ liệu thất bại.';
        }
        $result['result'] = $assign['data'];

        echo json_encode($result);
    }
//    public function actionList() {
//        $levelDefault = 1;
//        $module = Yii::app()->request->getParam('module', 'film');
//        $parentId = (int) Yii::app()->request->getParam('parent_id', NULL);
//        $level = (int) Yii::app()->request->getParam('level', $levelDefault);
//        $cache_name = md5(__METHOD__ . $module . '_' . $parentId . '_' . $level);
//        $cache = Yii::app()->cache->get($cache_name); // Get cache
//        if ($cache === FALSE) {
//            $criteria = new CDbCriteria;
//            $criteria->condition = 'status = 1';
//
//            if ($parentId > 0) {
//                $parentArr = array();
//                $parents = KitCategory::getListInParent($module, $parentId);
//                foreach ($parents as $data)
//                    $parentArr[] = $data->id;
//                $criteria->addInCondition('id', $parentArr);
//            } else {
//                $criteria->addCondition('parent_id = 0');
//            }
//            
//            //Cái $level chưa biết xư lý thế nào
//
//            $assign['data'] = NULL;
//            $model = KitCategory::model()->findAll($criteria);
//            if ($model != NULL)
//                $assign['data'] = KitCategory::treatment($model);
//
////            // Get details
////            $assign['data'] = KitFilm::getDetails($id);
////            $assign['data'] = KitFilm::treatment($assign['data']);
//
//            Yii::app()->cache->set($cache_name, $assign); // Set cache
//        } else $assign = $cache;
//
//        //if (isset($assign['data']['id'])) {
//        if (isset($assign['data'])) {
//            $result['query'] = 'ok';
//            $result['status'] = 'Tải dữ liệu thành công.';
//        } else {
//            $result['query'] = 'fail';
//            $result['status'] = 'Tải dữ liệu thất bại.';
//        }
//        $result['result'] = $assign['data'];
//
//        echo json_encode($result);
//    }

}