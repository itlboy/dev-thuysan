<?php

Yii::import('application.modules.category.models.db.BaseKitCategory');

class KitCategory extends BaseKitCategory {

    var $className = __CLASS__;
    var $children = array();

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    public function attributeLabels() {
        return array_merge(parent::attributeLabels(), array(
            'name' => 'Tên danh mục',
            'alias' => 'Bí danh',
            'parent_id' => 'Danh mục cha',
            'description' => 'Mô tả',
            'layout' => 'Giao diện',
            'sorder' => 'Thứ tự',
            'promotion' => 'Tiêu điểm',
            'status' => 'Kích hoạt'
        ));
    }

    public function behaviors() {
        return array(
            'NestedSetBehavior' => array(
                'class' => 'NestedSetBehavior',
                'hasManyRoots' => true,
                'leftAttribute' => 'lft',
                'rightAttribute' => 'rgt',
                'levelAttribute' => 'level',
            )
        );
    }

    protected function beforeValidate() {
        $this->updated_time = Common::getNow();
        if ($this->isNewRecord) {
            $this->created_time = $this->updated_time;
        }

        if (isset($_POST['KitCategory'])) {
            if ($this->alias == NULL) {
                $this->alias = Common::convertStringToUrl($this->name);
            }
        }

        return parent::beforeValidate();
    }

    protected function afterSave() {
        parent::afterSave();
        $this->clearCache();
    }

    protected function beforeDelete() {
        $this->clearCache();
        return parent::beforeDelete();
    }

    private function clearCache() {
        Yii::app()->cache->delete(md5('KitCategory::getList_' . $this->module . '_NULL_array'));
        Yii::app()->cache->delete(md5('KitCategory::getList_' . $this->module . '_0_array'));
        Yii::app()->cache->delete(md5('KitCategory::getList_' . $this->module . '_1_array'));
        Yii::app()->cache->delete(md5('KitCategory::getList_' . $this->module . '_NULL_object'));
        Yii::app()->cache->delete(md5('KitCategory::getList_' . $this->module . '_0_object'));
        Yii::app()->cache->delete(md5('KitCategory::getList_' . $this->module . '_1_object'));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $module = Yii::app()->request->getQuery('module');
        if ($module !== null) {
            $this->module = $module;
        }

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('alias', $this->alias, true);
        $criteria->compare('parent_id', $this->parent_id, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('layout', $this->layout, true);
        $criteria->compare('module', $this->module, true);
        $criteria->compare('lft', $this->lft, true);
        $criteria->compare('rgt', $this->rgt, true);
        $criteria->compare('level', $this->level);
        $criteria->compare('root', $this->root, true);
        $criteria->compare('created_time', $this->created_time, true);
        $criteria->compare('updated_time', $this->updated_time, true);
        $criteria->compare('sorder', $this->sorder, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['pageSize']),
            ),
            'sort' => array(
                'defaultOrder' => 'root ASC, t.lft ASC',
            ),
        ));
    }

    /**
     * Get tree by module
     * @param string $module
     * @return type
     */
    public static function getTree($module = '', $promotion = NULL) {
        $list = self::getList($module, $promotion, 'array');
        $dataTree = self::createTree($module, $list);

        return $dataTree;
    }

    /**
     * Lay danh sach Module su dung category
     * @return array
     */
    public static function getModuleList() {
        $arr = array();
        foreach (Yii::app()->modules as $moduleName => $moduleDetails) {
            if (isset(Yii::app()->getModule($moduleName)->category) AND Yii::app()->getModule($moduleName)->category == TRUE) {
                $arr[$moduleName] = $moduleName;
            }
        }
        return $arr;
    }

    /**
     *
     * @param array $data
     * @param int $parent_id
     * @param array $result
     * @return array
     */
    public static function createTree($module, $data = array(), $parent_id = 0, $result = array()) {
        $arr = array();
        foreach ($data as $key => $value) {
            if ($value['parent_id'] == $parent_id) {
                $url = Yii::app()->createUrl($module . '/default/index', array(
                    'category_id' => $value['id'],
                ));
                $row = array(
                    'text' => '<a href="' . $url . '">' . $value['name'] . '</a>',
                );
                $row['children'] = self::createTree($module, $data, $value['id'], $result);
                $arr[] = $row;
            }
        }
        return $arr;
    }

    /**
     * Get Details
     * @param int $id
     * @return object
     */
    public static function getDetails($id) {
        $id = intval($id);
        $cache_name = md5(__METHOD__ . '_' . $id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $result = self::model()->findByPk($id);
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else
            return $cache;
        return $result;
    }

    /**
     * Get list by module
     * @param string $module
     * @param string $type
     * @return mixed
     */
    public static function getList($module = '', $promotion = NULL, $type = 'object') {
        $promotionStr = ($promotion == NULL) ? 'NULL' : $promotion;
        $cache_name = md5(__METHOD__ . '_' . $module . '_' . $promotionStr . '_' . $type);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $criteria = new CDbCriteria;
            $criteria->select = implode(',', array_keys(self::model()->getAttributes()));
            $criteria->condition = '';
            if ($module !== '')
                $criteria->condition = letFunction::addWhere($criteria->condition, "module = '" . $module . "'");
            if ($promotion !== NULL)
                $criteria->condition = letFunction::addWhere($criteria->condition, "promotion = '" . $promotion . "'");
            $criteria->order = 'parent_id ASC, sorder ASC, id ASC';
            $list = self::model()->findAll($criteria);

            if ($type == 'json')
                $result = json_encode($list);
            elseif ($type == 'array')
                $result = CJSON::decode((CJSON::encode($list)));
            else
                $result = $list;
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else
            return $cache;
        return $result;
    }

    /**
     * Lay 1 mang bao gom clickstream den category khai bao
     * @param int $category_id
     * @return array
     */
    public static function getBreadcrumb($category_id = NULL) {
        $cache_name = md5(__METHOD__ . '_' . $category_id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $data = self::_getBreadcrumb($category_id);
            krsort($data);
            $result = array();
            foreach ($data as $value) {
                $result[] = $value;
            }
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else
            return $cache;
        return $result;
    }

    /**
     *
     * @param int $category_id
     * @param array $data
     * @return array
     */
    private static function _getBreadcrumb($category_id = NULL, $data = array()) {
        $category_id = intval($category_id);
        $row = self::getDetails($category_id);
        if ($row !== NULL) {
            $row = self::treatment($row);
            $data[] = $row;
            $data = self::_getBreadcrumb($row['parent_id'], $data, FALSE);
        }
        return $data;
    }

    /**
     * Lay danh sach cac danh muc con cap gan nhat
     * @param string $module
     * @param int $parent_id
     * @return object
     */
    public static function getListInParent($module = '', $parent_id = '') {
        $cache_name = md5(__METHOD__ . '_' . $module . '_' . $parent_id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $c = new CDbCriteria;
            $c->select = implode(',', array_keys(self::model()->getAttributes()));
            $where = 'status = 1';
            if ($module !== NULL AND $module !== '')
                $where = Common::addWhere($where, "module = '" . $module . "'");
            if ($parent_id !== NULL) {
                $parent_id = intval($parent_id);
                $where = Common::addWhere($where, "parent_id = " . $parent_id . "");
            }

            $c->condition = $where;
            $c->order = 'sorder ASC, id ASC';
            $list = self::model()->findAll($c);
            Yii::app()->cache->set($cache_name, $list, Yii::app()->params['timeCache']); // Set cache
        } else
            return $cache;
        return $list;
    }

    /**
     * Lay danh sach bao gom danh muc cha va cac danh muc con tat ca cac cap
     * @param string $module
     * @param int $parent_id
     * @result array $result
     * @return string
     */
    public static function getListInParentAll($module = '', $parent_id = '', $result = array(), $level = 0) {
        $cache_name = md5(__METHOD__ . '_' . $module . '_' . $parent_id);
        $cache = Yii::app()->cache->get($cache_name); // Get cache
        if ($cache === FALSE) {
            $level++; // Cung cap thong tin level
            if (!in_array($parent_id, $result))
                $result[] = $parent_id; // Neu ko co $parent_id trong $result thi gan vao

            $list = self::getListInParent($module, $parent_id);
            foreach ($list as $key => $value) {
                //            echo $value->id;
                if (!in_array($value->id, $result))
                    $result[] = $value->id;
                $result = self::getListInParentAll($module, $value->id, $result, $level);
            }
            Yii::app()->cache->set($cache_name, $result); // Set cache
        } else
            return $cache;
        return $result;
    }

    /**
     *
     * @param array $data
     * @return array
     */
    public static function treatment($data) {
        $data = CJSON::decode((CJSON::encode($data))); // Neu la object thi chuyen thanh array
        if (!is_array($data) OR $data == array()) return array();
        if (!isset($data[0]))
            $data = self::treatmentRow($data);
        else {
            foreach ($data as $key => $value) {
                $data[$key] = self::treatmentRow($value);
            }
        }
        return $data;
    }

    public static function treatmentRow($row) {
//        var_dump($row); die;
        $urlTitle = Common::convertStringToUrl($row['name']);
        $row['url'] = Yii::app()->createUrl($urlTitle . '-c' . $row['id']);
        $row['name'] = htmlentities($row['name'], ENT_QUOTES);
        $row['alias'] = $urlTitle;
        return $row;
    }

    /**
     * get layout's class name
     * @return string
     */
    public function getLayoutClassName() {
        $layout = $this->layout ? ucfirst($this->layout) : "Default";
        return "application.widgets.layouts." . $layout . "Layout";
    }

    /**
     * get all category and dump it into an array
     * @return Array Of kitCategory
     * Each item is an instance of KitCategory onject, include fields as follow:
     *  - id: id of category
     *  - name: name of category
     *  - children: array of children category, each item is an instance of KitCategory object (include id and name field)
     */
    public static function dumpAllCategoryToArray($module = NULL, $parentId = 0, $maxLevel = 0) {
        $tree = array();
        $criteria = new CDbCriteria;
        $criteria->select = "id, name, alias, sorder";
        $criteria->condition = "parent_id=$parentId";
        $criteria->order = "sorder, id";

        if ($module !== NULL)
            $criteria->addSearchCondition ('module', $module);

        $categories = self::model()->findAll($criteria);
        foreach ($categories as $category) {
            self::dumpCategoryChildren($category, 1, $maxLevel);
            array_push($tree, $category);
        }

        return $tree;
    }
    public static function dumpCategoryChildren(&$parent, $level = 1, $maxLevel = 0) {
        $criteria = new CDbCriteria;
        $criteria->select = "id, name, alias";
        $criteria->condition = "parent_id=" . $parent->id;
        $criteria->order = "sorder, id";

        if (!$maxLevel || ($maxLevel && $level < $maxLevel)) {
            $children = self::model()->findAll($criteria);
            if ($children) {
                $parent->children = array();
                foreach ($children as $child) {
                    self::dumpCategoryChildren($child, $level + 1, $maxLevel);
                    array_push($parent->children, $child);
                }
            }
        }
    }

    public function getSortIcon($action, $grid, $id) {
        $url = Yii::app()->createUrl('//' . Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/' . $action);

        $imageUp = '<img src="' . Yii::app()->theme->baseUrl . '/images/icons/system/up_16.png" height="16" width="16" title="' . Yii::t('default', 'Up') . '">';
        $imageDown = '<img src="' . Yii::app()->theme->baseUrl . '/images/icons/system/down_16.png" height="16" width="16" title="' . Yii::t('default', 'Down') . '">';

        $html = CHtml::link($imageUp, 'javascript:;', array('onclick' => "ajaxGridSort('{$grid}', '{$id}', '{$url}', 'up')"));
        $html .= ' ';
        $html .= CHtml::link($imageDown, 'javascript:;', array('onclick' => "ajaxGridSort('{$grid}', '{$id}', '{$url}', 'down')"));
        return $html;
    }

    public function getCategoryOptions($module, $parentId = 0) {
        $criteria = new CDbCriteria;
        $criteria->select = 'id, CONCAT(REPEAT("-- ", level - 1), name) AS name';
        $criteria->order = 'root ASC, lft ASC';
        $criteria->condition = 'level<=:level AND status=1 AND module=:module';
        $criteria->params = array(
            ':level' => 5,
            ':module' => $module
        );
        if ($parentId != 0) {
            $model = self::model()->findByPk((int) $parentId);
            
            $descendants = array();
            if ($model == NULL) {
                $descendants = $model->descendants()->findAll(array(
                    'select' => 'id',
                    'condition' => 'status!=0'
                ));
            }
            $children = array();
            $children[] = $parentId;
            foreach ($descendants as $child)
                $children[] = $child->id;

            $criteria->addNotInCondition('id', $children);
        }

        return self::model()->findAll($criteria);
//        return CHtml::listData(self::model()->findAll($criteria), 'id', 'name');
    }
    
    public function getPinCategoryOptions($module, $parentId = 0) {
        $criteria = new CDbCriteria;
        $criteria->select = 'id, CONCAT(REPEAT("-- ", level - 2), name) AS name';
        $criteria->order = 'root ASC, lft ASC';
        $criteria->condition = 'level<=:level AND status=1 AND module=:module';
        $criteria->params = array(
            ':level' => 5,
            ':module' => $module
        );
        if ($parentId != 0) {
            $model = self::model()->findByPk((int) $parentId);
            
            $descendants = array();
            if ($model != NULL) {
                $descendants = $model->descendants()->findAll(array(
                    'select' => 'id',
                    'condition' => 'status!=0'
                ));
            }
            $children = array();
            $children[] = $parentId;
            foreach ($descendants as $child)
                $children[] = $child->id;

            $criteria->addCondition('id != ' . $parentId);
            $criteria->addInCondition('id', $children);
        }

        return self::model()->findAll($criteria);
    }

    public function getLayoutList() {
        $dir = Yii::getPathOfAlias('application.widgets.layouts');
        $dh = opendir($dir);
        $files = array();
        while (($file = readdir($dh)) !== false) {
            if ($file !== '.' && $file !== '..' && !is_dir($dir . DIRECTORY_SEPARATOR . $file)) {
                $filename = substr($file, 0, -10);
                $files[$filename] = $filename;
            }
        }
        return $files;
    }
    /** Đưa các Id cate về 1 mảng
     * @static
     * @param string $module
     * @param string $parent_id
     * @return array
     */
    public static function arrCategory($module = '', $parent_id = '')
    {
        $arr = self::getListInParent($module,$parent_id);
        $arrCategoryId = array();
        $arrCategoryId[] = $parent_id;
        if(count($arr) > 0){
            foreach($arr as $item)
            {
                $arrCategoryId[] = $item->attributes['id'] ;
            }
        }
        return $arrCategoryId;
    }
    public static function getParentCategory($module = '', $category_id, &$array = array())
    {
        $data = self::model()->findByPk($category_id);
        if($data !== NULL)
        {
            $data = KitCategory::treatment($data);

            if((int)$data['parent_id'] !== 0)
            {
                $array[] = $data['parent_id'];
                self::getParentCategory($module, $data['parent_id'], $array);
            }
        }
        return $array;
    }
}