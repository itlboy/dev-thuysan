<?php

Yii::import('application.modules.category.models.db.BaseKitCategoryBlock');
class KitCategoryBlock extends BaseKitCategoryBlock
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function encodeParams($params) {
        if(!$params) $params = $this->params;
        return serialize($params);
    }
    
    public function decodeParams($params=array()) {
        if(!$params) $params = $this->params;
        if(!$params) return array();
        $params = unserialize($params);
        if(!is_array($params)) return array();
        return $params;
    }
}