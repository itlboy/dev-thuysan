﻿<?php
$moduleName = 'crawler';
$modelName = 'KitCrawlerConfig';
$formID = 'kit_'.$moduleName.'_form';
$gridID = 'kit_'.$moduleName.'_grid';
?>

<?php
$i = 0;
$arr = array();
if(!empty($config)){
    foreach($config as $key => $value){
        $value['i'] = $i;
        $arr[] = $value;
        $i++;
    };
}
$arr = json_encode($arr);
?>

<script type="text/javascript">
    var countConfig = 0;
    $(document).ready(function(){
        $('#result_config').sortable();
        var data = '<?php echo $arr; ?>';
        data = eval(data);
        $.each(data,function(key,value){

            copyHtml('source_'+value.type,'result_config', value);
            ++countConfig;
        });
        changeColorAll();
    });

    function copyHtml(sourceId, resultId, data){
        var sourceData = {
            "i":countConfig,
            "key":"",
            "object":"",
            "startstring":"",
            "endstring":"",
            "tag":"",
            "index":"",
            "attr":"",
            "field":"",
            "check_exists":"",
            "is_replace":"",
            "replace_text":"",
            "action_url":"0"
        };
        if(data !== undefined){
            $.each(data,function(key,value){
                if((key == 'check_exists' || key == 'is_replace') && value == 1){
                    value = 'checked';
                }
                sourceData[key] = value;
            });
        }
        data = sourceData;

        var contentHtml = $('#'+sourceId).html();

        contentHtml = contentHtml.replace(/{{i}}/g, data.i);
        contentHtml = contentHtml.replace(/{{key}}/g, data.key);
        contentHtml = contentHtml.replace(/{{object}}/g, data.object);
        contentHtml = contentHtml.replace(/{{startstring}}/g, data.startstring);
        contentHtml = contentHtml.replace(/{{endstring}}/g, data.endstring);
        contentHtml = contentHtml.replace(/{{tag}}/g, data.tag);
        contentHtml = contentHtml.replace(/{{index}}/g, data.index);
        contentHtml = contentHtml.replace(/{{attr}}/g, data.attr);
        contentHtml = contentHtml.replace(/{{field}}/g, data.field);
        contentHtml = contentHtml.replace(/{{check_exists}}/g, data.check_exists);
        contentHtml = contentHtml.replace(/{{replace_text}}/g, data.replace_text);
        contentHtml = contentHtml.replace(/{{is_replace}}/g, data.is_replace);
        contentHtml = contentHtml.replace(/{{action_url}}/g, data.action_url);
        $('#'+resultId).append(contentHtml);


    }
    function testLink(){
        $(document).ready(function(){
            $('.result_all').html('');
            $.ajax({
                type: "POST",
                url: '<?php echo Yii::app()->createUrl('//crawler/ajax/testlink'); ?>',
                data: $("#kit_crawler_form").serialize(), // serializes the form's elements.
                beforeSend:function(){
                    $('.result_ld').html('<img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/ajax-loader.gif" />');
                },
                success: function(data)
                {
//                    eval('data =' +data);
//                    $.each(data,function(k,v){
//                        $.each(v,function(key,value){
//                            testAll(key,value);
//                        });
//                    });
//                    $('.result_ld').html('');
                }
            });
            return false;
        });
    }
    function testAll(object,content){
        $(document).ready(function(){
            var html = $('.ct-rs').html();
            var colLeft = $('.ct-col-l').html();
            colLeft = colLeft.replace(/{{object}}/g,object);
            html = html.replace(/{{content}}/g,content);
            html = html.replace(/</g,'<code><</code>');
            html = '<fieldset class="label_side"><label style="border-right: 0px">'+colLeft+'</label><div style="width: 880px;height: 300px;overflow: auto;">'+html+'</fieldset>';
            $('.result_all').append(html);
        });
    }
    function changeColor(obj){
        $(document).ready(function(){

            var data = obj.val();
            if(data.indexOf('.') == -1 && data != ''){
                obj.parent().parent().parent().parent().parent().css({'background-color':'red'});
            } else if( data != ''){
                obj.parent().parent().parent().parent().parent().css({'background-color':'#E3FCE6'});
            } else {
                obj.parent().parent().parent().parent().parent().css({'background':'none'});
            }
        });
    }

    function changeColorAll(){
        //database_field
        $(document).ready(function(){
            $.each($('.database_field'),function(){
                changeColor($(this));
            });
        });
    }
</script>
<div class="grid_16">
<!-- Title -->
<div class="flat_area grid_16" style="opacity: 1;">
    <h2>Quản lý Config</h2>

</div>
<!-- END Title -->
<div class="box grid_16" style="opacity: 1;">
    <h2 class="box_head">Form Elements</h2>
    <a class="grabber" href="#">&nbsp;</a>
    <a class="toggle" href="#">&nbsp;</a>
    <?php
    $form=$this->beginWidget('CActiveForm', array(
        'id' => $formID,
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype'=>'multipart/form-data'),
    ));
    ?>

    <div class="toggle_container">
        <div class="block" style="opacity: 1;">
            <!--            <h2 class="section">Search Film</h2>-->
            <fieldset class="label_side">
                <label>Item  :</label>
                <div>
                    <?php echo $form->dropDownList($model,'item_id',CHtml::listData(KitCrawler::model()->findAll(),'id','title'), array('prompt'=>'Chọn Crawler')); ?>
                </div>
            </fieldset>
            <fieldset class="label_side">
                <label>Regex :</label>
                <div>
                    <?php echo $form->textField($model,'regex',array(
//                        'onmousedown' => "getSelText()",
                    )); ?>
                    <?php echo $form->error($model,'regex'); ?>
                    <span style="float: left;font-size: 11px;font-style: italic;color: #999;margin-top: 2px; color: #333" >
                        <span class="element_regex">Tất cả các kí tự : <span>(.*)</span></span> |
                        <span class="element_regex">Kí tự dạng số : <span>[0-9]</span></span> |
                        <span class="element_regex">Kí tự dạng chữ : <span>[a-z]</span></span> |
                        <span class="element_regex">Không phân biệt chữ hoa chữ thường : <span>/i</span></span> |
                        <span class="element_regex">Bất kì kí tự số nào : <span>\d</span></span> |
                        <span class="element_regex">Bất kì kí tự nào không phải là số : <span>\D</span></span> |
                        <span class="element_regex">Bất kì kí tự khoảng trắng : <span>\s</span></span> |
                        <span class="element_regex">Bất kì kí tự không phải là khoảng trắng : <span>\S</span></span> |
                    </span>
                </div>
            </fieldset>
            <fieldset class="label_side">
                <label>Test :</label>
                <div>
                    <?php echo $form->textField($model,'link_test'); ?>
                    <?php echo $form->error($model,'link_test'); ?>
                </div>
            </fieldset>


            <div class="button_bar clearfix">
                <button type="button" class="blue small div_icon has_text" onclick="return copyHtml('source_cutstring','result_config');">
                    <div class="ui-icon ui-icon-plusthick"></div>
                    <span>Cut String</span>
                </button>
                <button type="button" class="blue small div_icon has_text" onclick="return copyHtml('source_getlist','result_config');">
                    <div class="ui-icon ui-icon-plusthick"></div>
                    <span>Get List</span>
                </button>
                <button type="button" class="green small send_right div_icon has_text" onclick="return window.location.href='<?php echo isset($_GET['id']) ? Yii::app()->createUrl('crawler/config/index',array('id'=> $_GET['id'] , 'test' => 1)) : ''; ?>';" >
                    <div class="ui-icon ui-icon-plusthick"></div>
                    <span>Test hoàn chỉnh</span>
                </button>
                <div style="float: right;" class="result_ld"></div>
            </div>
        </div>
        <div class="block" style="opacity: 1;">
            <style>
                .element_regex{
                    color: #666;
                    cursor: pointer;
                    padding: 2px;
                }
                .element_regex span{
                    color:blue;
                }
                .col_20 {
                    border: 0px;
                }
                .col_20 fieldset{
                    border: 0px !important;
                }
                .col_20 div.result_ld{
                    padding:0px;
                    border: 1px solid #e5e5e5;
                    border-radius: 2px;
                    height: 30px;
                    line-height: 30px;
                    background: none repeat scroll 0 0 rgba(0, 0, 0, 0.05);
                    cursor: pointer;
                    padding-left: 7px;
                }
                .col_20 div.result_ld img{margin-top: 3px;}
            </style>
            <div id="result_config"></div>

            <div class="button_bar clearfix">
                <button class="green dark img_icon has_text" id="btn_search" type="submit">
                    <span>Lưu</span>
                </button>
                <span style="margin: 6px 10px 10px;float: left;" class="sp-loadding"></span>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
    <div class="result_all"></div>
    <?php if(isset($_GET['test'])):  ?>
    <div class="rs_config">
        <?php if(count($result) > 0): ?>
            <?php foreach($result as $key => $value ): ?>
        <fieldset class="label_side">
            <label class="ct-col-l" style="border-right: 0px"><?php echo $key; ?></label>
            <div class="ct-rs">
                <?php var_dump($value); ?>
            </div>
        </fieldset>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <?php endif; ?>
    <div id="source_cutstring" style="display: none;">
        <fieldset class="label_side">
            <label>Cut String: {{i}}</label>
            <input type="hidden" name="config[{{i}}][type]" value="cutstring">
            <div class="columns clearfix">
                <div class="col_25" style="border: 0px">
                    <fieldset>
                        <label>Key</label>
                        <div>
                            <input type="text" value="{{key}}" class="text" name="config[{{i}}][key]" />
                        </div>
                    </fieldset>
                </div>
                <div class="col_25"  style="border: 0px;">
                    <fieldset>
                        <label>Object</label>
                        <div>
                            <input type="text" value="{{object}}" class="text" name="config[{{i}}][object]" />
                        </div>
                    </fieldset>
                </div>
                <div class="col_25" style="border: 0px;">
                    <fieldset>
                        <label>Start String</label>
                        <div>
                            <input type="text" value="{{startstring}}" class="text" name="config[{{i}}][startstring]" />
                        </div>
                    </fieldset>
                </div>
                <div class="col_25" style="border: 0px;">
                    <fieldset>
                        <label>End String</label>
                        <div>
                            <input type="text" value="{{endstring}}" class="text" name="config[{{i}}][endstring]" />
                        </div>
                    </fieldset>
                </div>
                <div class="col_20" style="border: 0px;">
                    <fieldset>
                        <label>Database Field</label>
                        <div>
                            <input type="text" value="{{field}}" class="text database_field" name="config[{{i}}][field]" onchange="return changeColor($(this));" />
                        </div>
                    </fieldset>
                </div>
                <div class="col_20">
                    <fieldset>
                        <label></label>
                        <div>
                            <div class="jqui_radios">
                                <input type="radio" name="answer5" id="yes5"/><label for="yes5">Yes</label>
                                <input type="radio" name="answer5" id="no5" checked="checked"/><label for="no5">No</label>																					</div>
                        </div>
                    </fieldset>
                </div>
                <div class="col_20" style="border: 0px">
                    <fieldset>
                        <label>Yêu cầu tồn tại</label>
                        <div>
                            <input type="checkbox" name="config[{{i}}][check_exists]" value="1" {{check_exists}} />
                        </div>
                    </fieldset>
                </div>
                <div class="remove_tag tooltip hover left" title="Xóa Config này" onclick="$(this).parent().parent('fieldset').css('display', 'none');"></div>
            </div>

        </fieldset>
    </div>
    <div id="source_getlist" class="block" style="display: none;">
        <fieldset class="label_side">
            <label>Get List: {{i}}</label>
            <input type="hidden" name="config[{{i}}][type]" value="getlist" />
            <div class="columns clearfix">
                <div class="col_20" style="border: 0px">
                    <fieldset>
                        <label>Key</label>
                        <div>
                            <input type="text" value="{{key}}" class="text" name="config[{{i}}][key]" />
                        </div>
                    </fieldset>
                </div>
                <div class="col_20"  style="border: 0px;">
                    <fieldset>
                        <label>Object</label>
                        <div>
                            <input type="text" value="{{object}}" class="text" name="config[{{i}}][object]" />
                        </div>
                    </fieldset>
                </div>
                <div class="col_20" style="border: 0px;">
                    <fieldset>
                        <label>Tag</label>
                        <div>
                            <input type="text" value="{{tag}}" class="text" name="config[{{i}}][tag]" />
                        </div>
                    </fieldset>
                </div>
                <div class="col_20" style="border: 0px;">
                    <fieldset>
                        <label>Index</label>
                        <div>
                            <input type="text" value="{{index}}" class="text" name="config[{{i}}][index]" />
                        </div>
                    </fieldset>
                </div>
                <div class="col_20" style="border: 0px;">
                    <fieldset>
                        <label>Attributes</label>
                        <div>
                            <input type="text" value="{{attr}}" class="text" name="config[{{i}}][attr]" />
                            <span>outertext, innertext, plaintext ...</span>
                        </div>
                    </fieldset>
                </div>
                <div class="col_20" style="border: 0px;">
                    <fieldset>
                        <label>Database Field</label>
                        <div>
                            <input type="text" value="{{field}}" class="text database_field" name="config[{{i}}][field]" onchange="return changeColor($(this));" />
                        </div>
                    </fieldset>
                </div>
                <div class="col_20" style="border: 0px">
                    <fieldset>
                        <label>Yêu cầu tồn tại</label>
                        <div>
                            <input type="checkbox" name="config[{{i}}][check_exists]" value="1" {{check_exists}} />
                        </div>
                    </fieldset>
                </div>
                <div class="col_20" style="border: 0px">
                    <fieldset>
                        <label>Is Replace</label>
                        <div>
                            <input type="checkbox" name="config[{{i}}][is_replace]" value="1" {{is_replace}} />
                        </div>
                    </fieldset>
                </div>
                <div class="col_20" style="border: 0px">
                    <fieldset>
                        <label></label>
                        <div>
                            <input type="text" name="config[{{i}}][replace_text]" value="{{replace_text}}" />
                        </div>
                    </fieldset>
                </div>
                <div class="col_20" style="border: 0px">
                    <fieldset>
                        <label>Xử lý Url</label>
                        <div>
                            <input type="text" name="config[{{i}}][action_url]" value="{{action_url}}" />
                        </div>
                        <div>0 : Không xử lý</div>
                        <div>1 : Lấy Link gốc</div>
                        <div>2 : Lưu ảnh về server</div>
                    </fieldset>
                </div>
                <div class="remove_tag tooltip hover left" title="Xóa Config này" onclick="$(this).parent().parent('fieldset').remove();"></div>
            </div>
        </fieldset>
    </div>
</div>
<?php $model = new KitCrawlerConfig('search'); ?>
<div class="block" style="margin-top: 10px">
    <?php
    $this->widget('GridView', array(
        'id' => $gridID,
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => array(
            array(
                'class' => 'CCheckBoxColumn',
                'id' => 'chkIds',
                'selectableRows' => 2,
                'htmlOptions' => array('width' => '20px', 'align' => 'center')
            ),
            array(
                'name' => 'id',
                'value' => '$data->id'
            ),
            array(
                'name' => 'regex',
                'value' => '$data->regex'
            ),
            array( // display a column with "view", "update" and "delete" buttons
                'class' => 'CButtonColumn',
                'header' => CHtml::dropDownList('pageSize',
                    Yii::app()->user->getState('pageSize',20),
                    array(2=>2,10=>10,20=>20,30=>30,40=>40,50=>50,100=>100,200=>200,500=>500,1000=>1000),
                    array('onchange'=>"$.fn.yiiGridView.update('".$gridID."',{ data:{pageSize: $(this).val() }})")
                ),
                'template' => '{update} {delete}',
                'buttons' => array(
                    'update' => array(
                        'label' => Yii::t('global', 'Edit'),
                        'url' => 'Yii::app()->createUrl("//'.$moduleName.'/'.$this->id.'/index", array("id"=>$data->id))',
                        'imageUrl' => Yii::app()->theme->baseUrl.'/images/icons/system/edit_16.png',
                        'options' => array('class' => 'btnGridUpdate')
                    ),
                    'delete' => array(
                        'label' => Yii::t('global', 'Delete'),
                        'url' => 'Yii::app()->createUrl("//'.$moduleName.'/'.$this->id.'/delete", array("id"=>$data->id))',
                        'imageUrl' => Yii::app()->theme->baseUrl.'/images/icons/system/delete_16.png',
                    ),
                ),
            )
        ),
    ));
    ?>
</div>




