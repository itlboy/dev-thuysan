<?php
$moduleName = 'crawler';
$modelName = 'KitCrawlerSource';
$formID = 'kit_'.$moduleName.'_form';
$gridID = 'kit_'.$moduleName.'_grid';
?>
<div class="grid_16">
<!-- Title -->
<div class="flat_area grid_16" style="opacity: 1;">
    <h2>Quản lý source</h2>

</div>
<!-- END Title -->
<div class="box grid_16" style="opacity: 1;">
    <h2 class="box_head">Form Elements</h2>
    <a class="grabber" href="#">&nbsp;</a>
    <a class="toggle" href="#">&nbsp;</a>
    <?php
    $form=$this->beginWidget('CActiveForm', array(
        'id' => $formID,
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype'=>'multipart/form-data'),
    ));
    ?>
    <div class="toggle_container">
        <div class="block" style="opacity: 1;">
<!--            <h2 class="section">Search Film</h2>-->
            <fieldset class="label_side">
                <label>Title :</label>
                <div>
                    <?php echo $form->textField($model,'title'); ?>
                    <?php echo $form->error($model,'title'); ?>
                    <span style="float: left;font-size: 11px;font-style: italic;color: #999;margin-top: 2px" ></span>
                </div>
            </fieldset>
            <fieldset class="label_side">
                <label>Link :<span></span></label>
                <div>
                    <?php echo $form->textField($model,'link');  ?>
                    <?php echo $form->error($model,'link');  ?>
                </div>
            </fieldset>
            <fieldset class="label_side">
                <label>Item  :<span></span></label>
                <div>
                    <?php echo $form->dropDownList($model,'item_id',CHtml::listData(KitCrawler::model()->findAll(),'id','title'), array('prompt'=>'Chọn Crawler')); ?>
                </div>
            </fieldset>
            <fieldset class="label_side">
                <label>Category :<span></span></label>
                <div>
                    <?php echo $form->textField($model,'category_id'); ?>
                    <?php echo $form->error($model,'category_id');  ?>
                </div>
            </fieldset>

            <div class="button_bar clearfix">
                <button class="green dark img_icon has_text" id="btn_search" onblur="return submit();">
                    <span>Lưu</span>
                </button>
                <span style="margin: 6px 10px 10px;float: left;" class="sp-loadding"></span>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
    <div class="block" style="margin-top: 10px">
        <?php
        $this->widget('GridView', array(
            'id' => $gridID,
            'dataProvider' => $model->search(),
            'filter' => $model,
            'columns' => array(
                array(
                    'class' => 'CCheckBoxColumn',
                    'id' => 'chkIds',
                    'selectableRows' => 2,
                    'htmlOptions' => array('width' => '20px', 'align' => 'center')
                ),
                array(
                    'name' => 'id',
                    'value' => '$data->id'
                ),
                array(
                    'header' =>'crawler',
                    'type' => 'raw',
                    'value' => '$data->item_id'
                ),
                array(
                    'name' => 'title',
                    'value' => '$data->title'
                ),
                array(
                    'name' => 'link',
                    'value' => '$data->link'
                ),
                array(
                    'filter' => CHtml::activeDropDownList($model, 'promotion', BackendFunctions::getStatusOptions(), array('prompt' => Yii::t('global', 'Select all'))),
                    'type' => 'raw',
                    'name' => 'promotion',
                    'value' => 'BackendFunctions::getBooleanIcon("'.$moduleName.'", "'.$modelName.'", $data->id, "promotion", $data->promotion, "'.$gridID.'")',
                    'htmlOptions' => array('align' => 'center')
                ),
                array(
                    'filter' => CHtml::activeDropDownList($model, 'status', BackendFunctions::getStatusOptions(), array('prompt' => Yii::t('global', 'Select all'))),
                    'type' => 'raw',
                    'name' => 'status',
                    'value' => 'BackendFunctions::getBooleanIcon("'.$moduleName.'", "'.$modelName.'", $data->id, "status", $data->status, "'.$gridID.'")',
                    'htmlOptions' => array('align' => 'center')
                ),
                array( // display a column with "view", "update" and "delete" buttons
                    'class' => 'CButtonColumn',
                    'header' => CHtml::dropDownList('pageSize',
                        Yii::app()->user->getState('pageSize',20),
                        array(2=>2,10=>10,20=>20,30=>30,40=>40,50=>50,100=>100,200=>200,500=>500,1000=>1000),
                        array('onchange'=>"$.fn.yiiGridView.update('".$gridID."',{ data:{pageSize: $(this).val() }})")
                    ),
                    'template' => '{update} {delete}',
                    'buttons' => array(
                        'update' => array(
                            'label' => Yii::t('global', 'Edit'),
                            'url' => 'Yii::app()->createUrl("//'.$moduleName.'/'.$this->id.'/index", array("id"=>$data->id))',
                            'imageUrl' => Yii::app()->theme->baseUrl.'/images/icons/system/edit_16.png',
                            'options' => array('class' => 'btnGridUpdate')
                        ),
                        'delete' => array(
                            'label' => Yii::t('global', 'Delete'),
                            'url' => 'Yii::app()->createUrl("//'.$moduleName.'/'.$this->id.'/delete", array("id"=>$data->id))',
                            'imageUrl' => Yii::app()->theme->baseUrl.'/images/icons/system/delete_16.png',
                        ),
                    ),
                )
            ),
        ));
        ?>
    </div>
</div>




