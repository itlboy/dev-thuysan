<?php
class SourceController extends ControllerBackend
{

    public function actionIndex() {

        $model = new KitCrawlerSource('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['id']))
        {
            $id = letArray::get($_GET,'id');
            $model = $this->loadModel($id);
        }
        if(isset($_POST['KitCrawlerSource'])){
            $model->attributes = $_POST['KitCrawlerSource'];
            $date = date('Y-m-d H:i:s');
            $model->created_time = $date;
            $model->updated_time = $date;
            $uid = Yii::app()->user->id;
            $model->creator = $uid ;
            $model->editor =$uid;
            if($model->validate()){
                if($model->save()){
                    $this->redirect(Yii::app()->createUrl('//crawler/source'));
                }
            }
        }
        // Category
        if (Yii::app()->request->getQuery('category_id') !== NULL) {
            $model->category_id = Yii::app()->request->getQuery('category_id');
        }

        if (isset($_GET['KitCrawler'])) {
            $model->attributes = $_GET['KitCrawler'];
        }

        // page size drop down changed
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
            unset($_GET['pageSize']);
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }
    public function actionDelete($id) {
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));

            echo json_encode(array('status' => 'success'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }
    public function loadModel($id) {
        $model=KitCrawlerSource::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}
