<?php

Yii::import('application.vendors.libs.crawler');
class AjaxController extends ControllerBackend
{
    public function actionIndex() {

    }
    public function actionTestLink(){
        $config = letArray::get($_POST,'config');
        $post = letArray::get($_POST,'KitCrawlerConfig');
        $link_test = $post['link_test'];
        $html = letText::getCurl($link_test);
        foreach($config as $rule){
            // Kiem tra ton tai ket qua theo key = $rule['object'] khong, neu co thi lay gia tri ket qua do, neu khong thi van lay html get ve.
            if (isset($rule['object']) AND $rule['object'] !== '' AND isset($result[$rule['object']]) AND $result[$rule['object']] !== '') {
                $html = $result[$rule['object']];
            }
            // $result[$i][$rule['key']] la ket qua xu ly
            if($rule['type'] == 'getlist'){
                $result[$rule['key']] = crawler::ruleGetList($html,$rule['tag'],$rule['index']);
            } else {
                $result[$rule['key']] = crawler::ruleCutString($html,$rule['tag'],$rule['index']);
            }
        }
        foreach($result as $key => $item){
            if(is_string($item)){
                $result[$key] = highlight_string($item);
            }
        }
        echo json_encode(array(
            'result' => $result,
        ));
        return;
    }


}
