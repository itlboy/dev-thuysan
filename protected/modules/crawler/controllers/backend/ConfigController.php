<?php
Yii::import('application.vendors.libs.crawler');
class ConfigController extends ControllerBackend
{

    public function loadModel($id) {
        $model=KitCrawlerConfig::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    public function actionIndex() {
        $model = new KitCrawlerConfig('search');
        $model->unsetAttributes();  // clear any default values

        $result = array();
        if(isset($_GET['id']))
        {
            $id = letArray::get($_GET,'id');
            $model = $this->loadModel($id);
            $config  = json_decode($model->config,true);

            if(isset($_GET['test'])){
                $result = crawler::crawlerData($config,$model->link_test,$model->item_id);
            }
        }
        if(isset($_POST['KitCrawlerConfig']))
        {
            $model->attributes = $_POST['KitCrawlerConfig'];
            unset($config);
            $post = letArray::get($_POST,'config');
            if(!empty($post)){
                foreach($post as $key => $value){
                    $config[$value['key']] = $value;
                }
                $model->config = json_encode($config);
            }
            //Xoa cache
            Yii::app()->cache->delete(md5('crawler_'.$model->item_id));
            Yii::app()->cache->delete(md5('crawler_source_'.$model->item_id.'_0'));
            Yii::app()->cache->delete(md5('crawler_link_'.$model->item_id));
            if($model->save()){
//                $this->redirect(Yii::app()->createUrl('//crawler/config/index'));
            }
        }


        // Category
        if (Yii::app()->request->getQuery('category_id') !== NULL) {
            $model->category_id = Yii::app()->request->getQuery('category_id');
        }
        if (isset($_GET['KitCrawler'])) {
            $model->attributes = $_GET['KitCrawler'];
        }
        // page size drop down changed
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
            unset($_GET['pageSize']);
        }
//        print_r($config);
        $this->render('index', array(
            'model' => $model,
            'config' => !empty($config) ? $config : '',
            'result' => $result,
        ));
    }

    public function actionDelete($id) {
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));

            echo json_encode(array('status' => 'success'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }


}
