<?php

Yii::import('application.modules.crawler.models.db.BaseKitCrawlerSource');
class KitCrawlerSource extends BaseKitCrawlerSource
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}