<?php

Yii::import('application.modules.crawler.models.db.BaseKitCrawlerConfig');
class KitCrawlerConfig extends BaseKitCrawlerConfig
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}