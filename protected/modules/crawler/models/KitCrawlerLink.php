<?php

Yii::import('application.modules.crawler.models.db.BaseKitCrawlerLink');
class KitCrawlerLink extends BaseKitCrawlerLink
{
    var $className = __CLASS__;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}