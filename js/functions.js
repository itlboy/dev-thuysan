function ajaxLoadFile(url, id)
{
    $(document).ready(function(){
        var aj = $.ajax({
            url: url
        });
        aj.done(function(data) {
            $('#' + id).html(data);
        }).fail(function() {
            alert('Có lỗi trong quá trình xử lý. Xin hảy thử lại !');
        });
    });
}

function getDateInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

//function listDateOption(to,from,title,TagId,itemActive){
//    var str = '<option selected="selected" value="0">'+title+'</option>';
//    for(var i = to; i <= from; i++){
//        var selected = '';
//        if(itemActive != undefined){
//            if(i == itemActive){
//                selected = 'selected="selected"';
//            }
//        }
//        if(i < 10){
//            i = '0'+ i;
//        }
//        str += '<option value="'+i+'" '+selected+'>'+i+'</option>';
//    }
//    $('#'+TagId).append(str);
//}


/**
 *
 * @param IdTagDate
 * @param IdTagMonth
 * @param IdTagYear
 */
function dateChange(IdTagDate,IdTagMonth,IdTagYear){
    var year = 2000;
    var date = parseInt($('#'+IdTagDate).val());
    var month = parseInt($('#'+IdTagMonth).val());
    if(parseInt($('#'+IdTagYear).val()) != 0){
        year = parseInt($('#'+IdTagYear).val());
    }
    var dateInMonth = getDateInMonth(month,year);
    $('#'+IdTagDate).html('');
    listDateOption(1,dateInMonth,'Ngày',IdTagDate,date);
}

